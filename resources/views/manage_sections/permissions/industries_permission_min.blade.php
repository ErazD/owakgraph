{{-- REQUIRE   $brand: marca, $collapsible: boolean --}}
<li class ="brand-permission brand-{{$brand->id}}
	@foreach($brand->canastas as $k=>$canasta)
	 canasta-{{$canasta->id}} categoria-{{$canasta->categoria->id}} vertical-{{$canasta->categoria->vertical->id}}
	@endforeach
">
	<div class="{{($collapsible)?'collapsible-header':''}} title-section user-title">
		<span>
			<span class="stop-propagation"> 
				<a class="white-text" href="{{App\Http\Controllers\MethodsController::getSectionUrl($brand, 'Brand')}}">
					<i class="material-icons">insert_link</i> {{$brand->name}}
				</a>
			</span>
			<span class="white-text">
				({{sizeof($brand->industryRoles)}} grupos)
			</span>
		 </span>
		<span class="info-min white-text stop-propagation">
			@if($isAdmin)			
			<i class="material-icons" onclick='openBrandModal({{$brand->id}}, "{{$brand->name}}")'>add_circle</i>			
			{{--  <i class="material-icons" onclick='removeGroup("{{$brand->id}}", "{{$brand->name}}")'>delete</i> --}}
			@endif
		</span>
	</div>
	<div class="{{($collapsible)?'collapsible-body':''}} container-m">
		<table class="permissions-table">
			<tr>
				<th>Grupo</th> <th>Descripción</th> <th>Nivel</th><th class="w2em"></th><th class="w2em"></th>
			</tr>
			@foreach($brand->industryRoles as $ind=>$gRol)
			<?php $group = $gRol->group; ?>
			<tr>
				<td>{{$group->name}}</td>
				<td> {{$group->description}}</td>
				<td>{{$gRol->permission_value}}</td>
				<td class="w2em"><i class="material-icons pointer" onclick="openUserModal({{$group->id}})">group_add</i></td>
				<td class="w2em"><i class="material-icons pointer" onclick='removePermission("brand,{{$gRol->permission_value}}", "{{$brand->id}}", "{{$brand->name}}", {{$group->id}}, "{{$group->name}}")'>delete</i></td>
			</tr>
			<tr>
				<td colspan="5"  class="white group-users">	
					@foreach( $group->users as $g_user)
					<a class="user-label grey lighten-3" onclick='removeUser("{{$g_user->id}}", "{{$g_user->name}}", {{$group->id}}, "{{$group->name}}")'>{{$g_user->name}}</a>
					@endforeach		
				</td>
			</tr>
			@endforeach
		</table>
	</div>
</li>
