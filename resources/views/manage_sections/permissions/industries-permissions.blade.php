@extends('layouts.app')

@section('scripts')

<script>
	var selectedGroup = false;
	var selectedElements =  {};
	var type = 'User';
	var selectedBrandName = '';

	var elementSelector =  document.getElementById("elementselector");
	var elementsSelected =  document.getElementById("elements-selected");

	var permissionLevel = document.getElementById("add-element-level");

	var userDataList =  document.getElementById("users_list");
	var groupDataList =  document.getElementById("groups_list");
	var elementDataList =  document.getElementById("elements_list"); // datalist con la información seleccionada (para el imput)
	var elementList =  userDataList; // Referencia a la lista de elementos seleccionados (referencia al elemento)


	/**
	* Ventana para agregar un usuario a un grupo.
	* @param {int} g_id: ID del grupo al que se agregará el usuario.
	*/
	function openUserModal(g_id){
		type = 'User';
		selectedGroup = g_id;
		selectedElements = {};
		$('#add-element').modal('open');
		elementList = userDataList;
		elementDataList.innerHTML = elementList.innerHTML;
		selectedElements = {};
		updateSelectedElements();
		$('#add-element-title').html('Agregar usuario');

		$('#parent-element-permission').addClass('hide');	
		$('#only-add-element').removeClass('hide');	
		$('#create-add-group').addClass('hide');	
	}


	function openBrandModal(b_id, brandName=''){
		selectedBrandName = brandName;
		type = 'Group';
		selectedGroup = b_id;
		selectedElements = {};
		$('#add-element').modal('open');
		elementList = groupDataList;
		elementDataList.innerHTML = elementList.innerHTML;
		selectedElements = {};
		updateSelectedElements();
		$('#add-element-title').html('Agregar Grupo');
		$('#parent-element-permission').removeClass('hide');
		$('#only-add-element').removeClass('hide');	
		$('#create-add-group').addClass('hide');	
	}
	/**
	* Abre una ventana para crear un nuevo grupo de permisos y agregarlo a la marca.
	* Precondition: openBrandModal ha sido llamado previamente.
	* Precondition: selectedBrandName ha sido seteada con el nombre de la marca
	* Precondition: selectedGroup ha sido seteado con la id de la marca.
	*/
	function openCreateAddGroup(){
		$('#only-add-element').addClass('hide'); 
		$('#create-add-group').removeClass('hide');	
		$('#create-add-group-title').html('Crear y agregar grupo a ' + selectedBrandName);
		$('#create-add-group-next-url').val('group/add/Brand,<lvl>/<id>/'+selectedGroup+'/agregar');
	}


	/**
	* Muestra un mensaje de confirmación para elminar un usuario de un grupo y actualiza la url para su eliminación.
	* @param {int} u_id: id del usuario a eliminar.
	* @param {string} u_name: nombre del usuario a eliminar.
	* @param {int} g_id: id del grupo del que se va a eliminar.
	* @param {string} g_name: nombre del grupo del que se va a eliminar.
	* Postcondition: la url de eliminción de usuario ha cambiado.
	*/
	function removeUser(u_id, u_name, g_id, g_name){
		$('#txt-eliminar-element').html("¿Seguro que quieres eliminar a " + u_name + " del grupo \"" + g_name+"\"?");
		// var addUserURL = "http://owak.co/graph/public/index.php";
		$('#title-eliminar-element').html('Eliminar usuario');
		var removeUserURL = getUrl('group/add/User', g_id, u_id, 'eliminar');
		$('#url-eliminar-element').attr("href", removeUserURL);
		$('#modal-delete-element').modal('open');
	}

	/**
	* Muestra un mensaje de confirmación para elminar un permiso de un grupo y actualiza la url para su eliminación.
	* @param {string} type: tipo de permiso que se quiere eliminar ('permission', 'brand') concatenado con el nivel de permiso, separados por una coma ','.
	* @param {int} u_id: id o slug del elemento a eliminar.
	* @param {string} u_name: nombre del elemento a eliminar.
	* @param {int} g_id: id del grupo del que se va a eliminar.
	* @param {string} g_name: nombre del grupo del que se va a eliminar.
	* Postcondition: la url de eliminción de usuario ha cambiado.
	*/
	function removePermission(type, e_id, e_name, g_id, g_name){
		$('#txt-eliminar-element').html("¿Seguro que quieres eliminar el permiso de \"" + e_name + "\" del grupo \"" + g_name+"\"?");
		$('#title-eliminar-element').html('Eliminar Permiso');
		var removeUserURL = getUrl('group/add', type, g_id, e_id, 'eliminar');
		$('#url-eliminar-element').attr("href", removeUserURL);
		$('#modal-delete-element').modal('open');
	}

	function removeGroup(g_id, g_name){
		$('#txt-eliminar-group').html("¿Seguro que quieres eliminar el grupo \"" + g_name+"\"?<br>Perderás todos los permisos asignados.");
		var removeUserURL = getUrl('group', g_id);
		$('#delete-group-form').attr("action", removeUserURL);
		$('#modal-delete-group').modal('open');
	}

	/**
	* Busca el usuario seleccionado, si existe lo agrega a los usuarios seleccionados y actualiza los usuarios seleccionados. luego limpia el campo del formulario para poder ingresar otro usuario.
	* Postcondition: si existe, se ha agregado un usuario a la lista.
	* Postcondition: Se ha limpiado el selector de usuarios.
	*/
	function onchangeUser(){
		var sel = elementList.options.namedItem(elementselector.value);
		if(sel){
			selectedElements[sel.value] = sel.label;
			updateSelectedElements();
		}
		elementselector.value = "";
	}

	/**
	* Elimina un usario seleccionado y vuelve a pintar los usuarios seleccionados.
	* Postcondition: se ha eliminado un objeto de la lista de usuarios seleccionados.
	*/
	function deselectuser(key){
		delete selectedElements[key];
		updateSelectedElements();
	}
	/**
	* Actualiza la lista de usuarios seleccionados en el formulario de agregarusuarios.
	* Postcondition: el html que muestra los usuarios seleccionados ha sido actualizado.
	*/
	function updateSelectedElements(){
		var html = "";
		Object.keys(selectedElements).forEach(function(key){
			html += " <span class='filter-tag' onclick='deselectuser(\""+key+"\")'>"+selectedElements[key]+" </span>";
		});
		elementsSelected.innerHTML = html;
	}

	/**
	* Crea la url para agregar los usuarios al adset y redirige a dicha url si hay usuarios seleccionados.
	*/
	function registerElements(){
		var eleList = "";
		Object.keys(selectedElements).forEach(function(key){
			eleList += ((eleList == "")? "": ",") + key;
		});
		if(eleList == "" || selectedGroup===false){
			elementsSelected.innerHTML = "<br><span class='filter-tag' onclick='updateSelectedElements()' style='color:white; background-color:#b71c1c; padding:12px; font-weigth:bolder;'> Selecciona al menos un usuario a agregar. </span>";
		}else{	
			var type_send = type + ','+permissionLevel.value;
			var addUserURL = getUrl('group/add', type_send, selectedGroup, eleList, 'agregar');
			// alert(addUserURL);
			window.location = addUserURL;
		}
	}

	/**
	* Opción cuando se selecciona un filtro.
	* @param {string} category: Categoría del elemento.
	* @param {int} id: id del elemento.
	* Postcondition: se han ocultado las marcas no seleccionadas y se muestran las seleccionadas.
	*/
	function selectFilter(category, id, element){
		var filter = category + '-' +id;
		// muestra las marcas seleccionadas y oculta las que no
		$('.brand-permission').addClass('hide');
		$('.brand-permission.'+filter).removeClass('hide');
		// limpia las opciones del mismo nivel no seleccionadas
		$('.'+category+'-selections .menu-option').removeClass('selected');
		// limpia las opciones de la categoría seleccionada
		$('.all-sub'+category).removeClass('hide');
		$('.all-sub'+category+' .menu-option').removeClass('selected');
		$('.all-sub'+category+' .menu-option').addClass('hide');
		// muestra las opciones de la categoría seleccionada
		$('.sub'+filter+'.menu-option').removeClass('hide');
		// selecciona el elemento
		element.classList.add('selected');		
		if(category == 'vertical'){
			$('.canasta-selections').addClass('hide');
		}
	}
	/**
	* Deselecciona todos los filtros y muestra todas las marcas.
	*/
	function mostratTodasLasMarcas(){
		$('.selected').removeClass('selected');
		$('.brand-permission').removeClass('hide');
	}

</script>
@stop
<style>
	.w2em{
		max-width: 2em !important;
		width: 2em;
		font-size: 1em;
	}
	.pointer{
		cursor: pointer;
	}

	.row.menu-selection{
		display: flex;
	    justify-content: space-around;
	    align-items: center;
	    flex-wrap: wrap;
	    margin: 0px;
	    border-bottom-color: #01579b;
    	border-bottom-style: solid;
    	border-bottom-width: 1px;
	}
	.menu-selection .menu-option{
		display: inline-block;
		padding: 8px;
		text-align: center;
		flex: 1 1;
		cursor: pointer;
	}

	.menu-selection .menu-option.selected{
		background-color: #01579b;
	    color: white;
	    font-weight: bold;
	    font-size: 1.2em;
	    border: solid;
	    border-width: 1px;
	}
	li{
		list-style: none;	
	}
	ul.permissions-username{
		border: none;
		box-shadow: none;
	}
	ul.permissions-username .collapsible-header{
		border: none;
	}
	.user-title, .user-title.collapsible-header{
		background-color: #444753dc;
		display: flex;
		justify-content: space-between;
	}
	.user-title.collapsible-header:hover{
		background-color: #444753fc;
	}
	.permissions-table tr {
		border-bottom-style: solid;
		border-width: 1px;
		border-color: #ccc;
		/*font-size: 13px;*/
		font-family: Arial, san-serif;
	}
	.permissions-table td, .permissions-table th{
		padding: 8px 5px;		
	}
	.permissions-table tr td:first-child{
		max-width: 30%;
		min-width: 150px;
	}
	.title-section{
		padding: 10px 20px;
	}
	.title-section .group-users{
		padding: 8px 0px;
	}
	span.info-min{
		font-size: 10px;
		font-family: helvetica, sans-serif;
		font-weight: 100;
	}
	.modal{
		z-index: 10010 !important;
	}

	#modal-delete-element{
		width: 40%;
		margin: auto;
		height: 200px;
		top: 0px !important;
	}
	#title-eliminar-element{
		font-size: 1.8em;
	}
</style>
@section('content')
<div class="row grey lighten-3 mb-0">
	<div class="col s12 m12 l12 page-title">
		<h2 class="breadcrumbs-title left">
		Permisos de Industria</h2>
	</div>
</div>
<div class="row blue darken-3">
	<div class="col s12 m12 l12 page-title">
		<p class="white-text" style="width: 80%;"> 
			En esta sección se pueden ver los grupos que tienen permisos para acceder a las marcas.
		</p>
	</div>
</div>
<div class="row">
	@if (Session::has('message'))
	<div class="row red darken-4 animated fadeIn">
		<div class="col s12 m12 l12 page-title">
			<p class="white-text">{{ Session::get('message') }} </p>
		</div>
	</div>
	@endif
	
	@if(session('success'))
	<div class="col s12 m12 l12">
		<div class="light-green message animated fadeInLeftBig">
			<p class="white-text"> <i class="material-icons" style="margin-right: 5px;">check</i>{{ session('success') }} </p>
		</div>
	</div>
	@endif
</div>

<div class="brands-menu">
	<div class="row">
		<a class="btn light-blue darken-4 right mr-5" onclick="mostratTodasLasMarcas()">Mostrar todas <i class="material-icons right">replay</i></a>
	</div>
	<div class="row menu-selection vertical-selections">
		@foreach(App\Vertical::all() as $vk => $vertical)
			<div class="menu-option" onclick="selectFilter('vertical', {{$vertical->id}}, this)">
				{{$vertical->name}}
			</div>
		@endforeach
	</div>
	<div class="row menu-selection categoria-selections all-subvertical hide">
		@foreach(App\Categoria::all() as $ck => $categoria)
			<div class="menu-option subvertical-{{$categoria->vertical->id}}" onclick="selectFilter('categoria', {{$categoria->id}}, this)">
				{{$categoria->name}}
			</div>
		@endforeach
	</div>
	<div class="row menu-selection canasta-selections all-subcategoria hide">
		@foreach(App\Canasta::all() as $ck => $canasta)
			<div class="menu-option subcategoria-{{$canasta->categoria->id}}" onclick="selectFilter('canasta', {{$canasta->id}}, this)">
				{{$canasta->name}}
			</div>
		@endforeach
	</div>
</div>



@if($isAdmin)
<ul class="collapsible permissions-username">
	@foreach($brands as $k=>$brand)
	@include('manage_sections.permissions.industries_permission_min', ['brand' => $brand, 'collapsible'=>true, 'isAdmin'=>$isAdmin])
	@endforeach
</ul>
@else	
@endif
<datalist id="users_list">
	@foreach(App\User::get() as $u)
	<option value="{{$u->id}}" label="{{$u->name}}" name="{{$u->id}}"> </option>
	@endforeach
</datalist>

<datalist id="groups_list">
	@foreach(App\Group::get() as $g)
	<option value="{{$g->id}}" label="{{$g->name}}" name="{{$g->id}}"> </option>
	@endforeach
</datalist>

<datalist id="elements_list"></datalist>


<div id="add-element" class="modal" style="z-index: 1000;">
	<h2 class="title dark-blue-graph" id="add-element-title">Agregar</h2>
	<div class="modal-content">


		<div id="only-add-element" class="row">
			<div class="row" id="elements-selected"></div>
			<div class="row">
				<input placeholder="Agrega un elemento" type="text" list="elements_list" multiple id="elementselector" autocomplete="off" onchange="onchangeUser()">
			</div>
			<div class="row">
				<div  class="col s12 m6">
					<div id="parent-element-permission">
						<label for="add-element-level">Nivel de permiso</label>
						<select name="add-element-level" id="add-element-level">
							@foreach($roles['permission_value'] as $slug=>$permission)
							<option value="{{$permission['value']}}"> {{$permission['name']}}</option>
							@endforeach
						</select>
						<button onclick="openCreateAddGroup()">Crear y agregar a grupo</button>
					</div>
				</div>	
				<div class="col s12 m6">
					<a class="btn right" onclick="registerElements()">Agregar</a>
				</div>
			</div>
		</div>	

		<div id="create-add-group" class="row">
			<h4 id="create-add-group-title">Crear Grupo</h4>
			<div >
				<form action="{{route('group.store')}}" method="POST">
					{{ csrf_field() }}
					<label for="name">Nombre del grupo</label>
					<input type="text" name="name" required="true">
					<br>
					<label for="description">Descripción del grupo</label>
					<input type="text" name="description" required="true">
					<br>
					<label for="add-element-level">Nivel de permiso</label>
					<select name="add-element-level">
						@foreach($roles['permission_value'] as $slug=>$permission)
						<option value="{{$permission['value']}}"> {{$permission['name']}}</option>
						@endforeach
					</select>
					<br>
					<input type="hidden" name="nexturl" id="create-add-group-next-url">
					<input type="submit" value="Crear" class="btn btn-primary green" style="width:100%">
				</form>
			</div>
		</div>



	</div>
	<div class="modal-footer">
		<a class="modal-action modal-close waves-effect waves-blue btn-flat">Cerrar</a>
	</div>
</div>

<div id="modal-delete-element" class="modal bottom-sheet">
	<div class="modal-content">
		<h4 id="title-eliminar-element">Eliminar Usuario</h4>
		<p id="txt-eliminar-element">

		</p>
	</div>
	<div class="modal-footer">
		<a class="modal-action modal-close waves-effect waves-green btn-flat" onclick="$('#modal-delete-element').modal('close');"> Cancelar</a>
		<a id="url-eliminar-element" href="#" class="modal-action modal-close waves-effect waves-green btn-flat">Eliminar</a>
	</div>
</div>

<div id="modal-delete-group" class="modal modal-fixed-footer brands">

	<h2 class="title dark-blue-graph">Eliminar Grupo</h2>
	<div class="modal-content">
		<p class="center" id="txt-eliminar-group">¿Seguro que quieres eliminar el grupo?</p>

		{!! Form::open(['url' => 'group', 'method' => 'delete', 'id'=>'delete-group-form']) !!}

		<div class="row">
			<div class="col s12 m8 l8 offset-m2 offset-l2">
				{!!Form::submit('Eliminar', ['class' => 'btn btn-primary red darken-4', 'style' => 'width:100%'] ) !!}
			</div>
		</div>

		{!! Form::close() !!}


	</div>
	<div class="modal-footer">
		<a href="#" class="modal-action modal-close waves-effect waves-green btn-flat">Cerrar</a>
	</div>
</div>


@stop

