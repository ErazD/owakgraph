{{-- REQUIRE   $p_user: usuario, $collapsible: boolean --}}
<li>
<div class="{{($collapsible)?'collapsible-header':''}} title-section user-title">
		<span class="white-text"> 
			{{$p_user->name}}
		</span>
		<span class="info-min white-text">
			{{($p_user->rank == 0)? 'Deshabilitado':'Habilitado'}}
		</span>
</div>

<div class="{{($collapsible)?'collapsible-body':''}} container-m">
	<table class="permissions-table">
		<tr>
			<th>Nombre Permiso</th> <th> Descripción</th> <th>Nivel</th>
			<th>desde</th><th>informacion adicional</th>
		</tr>
		@foreach(App\UserRole::getUserPermissions($p_user)->get() as $ind=>$permission)
		<tr>
			<td>{{$roles['permission'][$permission['permission']]['name']}}</td>
			<td>{{$roles['permission'][$permission['permission']]['description']}}</td>
			<td>{{$permission['permission_value']}}</td>
			<td>{{$permission['element_model']}}</td>
			<td>{{('App\\'.$permission['element_model'])::where('id', $permission['element_id'])->first()->name }}</td>
		</tr>
		@endforeach
	</table>
	
	@if($p_user->twoFactorProfile)
	{{-- Información Two Factor --}}
		<br>
		<table class="permissions-table">
			<tr><th>Rango TwoFactor</th><th>Total posts asignados</th></tr>
			<tr>
				<td>{{$p_user->twoFactorProfile->rank}}</td>
				<td>{{number_format(sizeof($p_user->twoFactorProfile->twofactors), 0 , '.', '.')}}</td>
			</tr>
		</table>
	@endif
	
	{{-- Información permisos de modelos (marcas, verticales, adsets, ...) --}}
	<?php
	$groupRoles = App\GroupIndustryRole::whereHas('group', function($gQuery) use ($p_user){
		$gQuery->whereHas('users', function($uQuery) use ($p_user){
			$uQuery->where('users.id', $p_user->id);
		});
	})->get();
	?>
	<br>
	<table class="permissions-table">
		<tr>
			<th>Permiso a sección</th> <th>Elemento</th> <th>Nombre</th> <th>Nivel</th>
		</tr>
		@foreach($groupRoles as $ind=>$gRol)
		<?php $gRolElement = ('App\\'.$gRol->element_model)::where('id', $gRol->element_id)->first(); ?>
			<tr>
				<td><a href="{{App\Http\Controllers\MethodsController::getSectionUrl($gRolElement, $gRol->element_model)}}"><i class="material-icons">insert_link</i></a></td>
				<td>{{$gRol->element_model}}</td>
				<td> {{$gRolElement->name}}</td>
				<td>{{$gRol->permission_value}}</td>
			</tr>
		@endforeach
	</table>
</div>
</li>
