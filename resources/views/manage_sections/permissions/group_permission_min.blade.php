{{-- REQUIRE   $group: usuario, $collapsible: boolean --}}
<li>
<div class="{{($collapsible)?'collapsible-header':''}} title-section user-title">
		<span class="white-text"> 
			{{$group->name}}
		</span>
		<span class="info-min white-text stop-propagation">
			@if($isAdmin)
			
				 <i class="material-icons" onclick="openGroupModal({{$group->id}})">add_circle</i>
			
				 <i class="material-icons" onclick='removeGroup("{{$group->id}}", "{{$group->name}}")'>delete</i>
		 	@endif
		</span>
</div>

<div class="{{($collapsible)?'collapsible-body':''}} container-m">
	<div class="title-section white">
		<div class="group-users">
			@foreach($group->users as $g_user)
				<a class="user-label grey lighten-3" onclick='removeUser("{{$g_user->id}}", "{{$g_user->name}}", {{$group->id}}, "{{$group->name}}")'>{{$g_user->name}}</a>
			@endforeach
		</div>
		@if($group->description)
		<hr>
		<div class="group-description">
			<p>{{$group->description}}</p>
		</div>
		@endif
	</div>

	<table class="permissions-table">
		<tr>
			<th>Nombre Permiso</th> <th> Descripción</th> <th>Nivel</th>
			<th>desde</th><th>informacion adicional</th><th class="w2em"></th>
		</tr>
		@foreach(App\UserRole::getGroupPermission($group)->get() as $ind=>$permission)
		<tr>
			<td>{{$roles['permission'][$permission['permission']]['name']}}</td>
			<td>{{$roles['permission'][$permission['permission']]['description']}}</td>
			<td>{{$permission['permission_value']}}</td>
			<td>{{$permission['element_model']}}</td>
			<td>{{('App\\'.$permission['element_model'])::where('id', $permission['element_id'])->first()->name }}</td>
			<td class="w2em"><i class="material-icons pointer" onclick="removePermission('permission,{{$permission['permission_value']}}', '{{$permission['permission']}}', '{{$roles['permission'][$permission['permission']]['name']}}', {{$group->id}}, '{{$group->name}}')">delete</i></td>
		</tr>
		@endforeach
	</table>
	
	{{-- Información permisos de modelos (marcas, verticales, adsets, ...) --}}
	
	<br>
	<table class="permissions-table">
		<tr>
			<th>Permiso a sección</th> <th>Elemento</th> <th>Nombre</th> <th>Nivel</th><th class="w2em"></th>
		</tr>
		@foreach($group->industryPremissions as $ind=>$gRol)
		<?php $gRolElement = ('App\\'.$gRol->element_model)::where('id', $gRol->element_id)->first(); ?>
			<tr>
				<td><a href="{{App\Http\Controllers\MethodsController::getSectionUrl($gRolElement, $gRol->element_model)}}"><i class="material-icons">insert_link</i></a></td>
				<td>{{$gRol->element_model}}</td>
				<td> {{$gRolElement->name}}</td>
				<td>{{$gRol->permission_value}}</td>
				<td class="w2em"><i class="material-icons pointer" onclick='removePermission("brand,{{$gRol->permission_value}}", "{{$gRol->element_id}}", "{{$gRolElement->name}}", {{$group->id}}, "{{$group->name}}")'>delete</i></td>
			</tr>
		@endforeach
	</table>
</div>
</li>
