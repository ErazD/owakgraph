@extends('layouts.app')

@section('scripts')

@stop
<style>
	li{
		list-style: none;	
	}
	ul.permissions-username{
		border: none;
		box-shadow: none;
	}
	ul.permissions-username .collapsible-header{
		border: none;
	}
	.user-title, .user-title.collapsible-header{
		background-color: #444753dc;
		display: flex;
		justify-content: space-between;
	}
	.user-title.collapsible-header:hover{
		background-color: #444753fc;
	}
	.permissions-table tr {
		border-bottom-style: solid;
		border-width: 1px;
		border-color: #ccc;
		/*font-size: 13px;*/
		font-family: Arial, san-serif;
	}
	.permissions-table td, .permissions-table th{
		padding: 8px 5px;		
	}
	.permissions-table tr td:first-child{
		max-width: 30%;
		min-width: 150px;
	}
	.title-section{
		padding: 10px 20px;
	}
	span.info-min{
    	font-size: 10px;
	    font-family: helvetica, sans-serif;
	    font-weight: 100;
	}
</style>
@section('content')
<div class="row grey lighten-3 mb-0">
	<div class="col s12 m12 l12 page-title">
		<h2 class="breadcrumbs-title left">
		Permisos</h2>
	</div>
</div>
<div class="row blue darken-3">
	<div class="col s12 m12 l12 page-title">
		<p class="white-text" style="width: 80%;"> 
			En esta sección se pueden ver los permisos.	 
		</p>
	</div>
</div>
<div class="container-m">
	<table class="permissions-table">
		<tr>
			<th>Nombre Permiso</th> <th> Descripción</th>
		</tr>
		@foreach($roles['permission'] as $slug=>$permission)
			<tr>
				<td>{{$permission['name']}}</td> <td>{{$permission['description']}}</td>
			</tr>
		@endforeach
	</table>
</div>

@include('manage_sections.permissions.user_permission_min', ['p_user' => $user, 'collapsible'=>false])

@if($isAdmin)
	<div class="row grey lighten-3 mb-0">
		<div class="col s12 m12 l12 page-title">
			<h2 class="breadcrumbs-title left">
			Usuarios Registrados</h2>
		</div>
	</div>
	<ul class="collapsible permissions-username">
		@foreach(App\User::all() as $k=>$p_user)
			@if($user->id != $p_user->id)
				@include('manage_sections.permissions.user_permission_min', ['p_user' => $p_user, 'collapsible'=>true])
			@endif
		@endforeach
	</ul>
@endif

@stop

