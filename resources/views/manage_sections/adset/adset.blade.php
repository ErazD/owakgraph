@extends('layouts.app')
<?php 
$user = (Auth::check())? Auth::user(): false; 
$user = App\User::where('id',11)->first(); 
$userLevel = ($user)? App\GroupIndustryRole::getUserLevelPermission($user, $adset) : -1;
?>
@section('scripts')

<script src="{{ asset('js/vue.min.js') }}"></script>

<script src="{{ asset('js/appComms.js') }}"></script>
<script src="{{ asset('js/date-selector.js') }}"></script>

<script>
	var formComms=true;
	comms_register = "{{route('two-factor.store')}}";
	dateSelectorApp.since = "{{$since}}";
	dateSelectorApp.until = "{{$until}}";
</script>
@stop
@section('content')
<style>
.row.page-title{
	padding-bottom: 0px !important;
	margin-bottom: 0px;

}
.users-expandible{
	margin:0px;
	border: none;
}

.users-container{
	padding: 18px !important;
	width: 100%;
}

a.users-header{
	color: white;
	float: right;
	background: none;
	border: none;
	padding: 8px;
	padding-right: 20px;
}
.active a.users-header{
	color: #1565C0;
}

.user-label{
	padding: 8px 14px 8px 8px;
	background-color: #ffffff40;
	color: #777;
	border-radius: 8px;
	position: relative;
}

.user-label:after {
	content: 'x';
	color: white;
	font-size: 0.8em;
	position: absolute;
	right: 5px;
	top: 0px;
	text-shadow: 0px 0px 1px #999;
}


#modal-delete-user{
	width: 40%;
	margin: auto;
	height: 200px;
	top: 0px !important;
}
#title-eliminar-usuario{
	font-size: 1.8em;
}

</style>

<div class="page" id="adset">
	<div class="row grey lighten-5 mb-0">
		<div class="col s12 m12 l12 page-title">

			<div class="header-content left">
				<h2>{{ $adset->name }}</h2>
				
			</div>

			<div class="right">

				<a class="add-brand right ml-10 modal-trigger tooltipped" data-position="bottom" data-delay="50" data-tooltip="Eliminar" href="#modal-delete-adset">
					<i class="material-icons header">delete</i>
				</a>
				
				@if($adset->public == 0)
				<a class="add-brand right modal-trigger ml-10 tooltipped" data-position="bottom" data-delay="50" data-tooltip="Set privado">
					<i class="material-icons header indigo-text text-lighten-3">lock</i>
				</a>				
				@endif
				
				{{-- <a class="add-brand right modal-trigger ml-10 tooltipped" data-position="bottom" data-delay="50" data-tooltip="Comms Panorama Section" href="{{ url('comms/discovery/'.$adset->id) }}">
					<i class="material-icons header indigo-text">assignment</i>
				</a> --}}
			</div>
		</div>
	</div>

	<div class="row blue darken-3 mb-0">
		<div class="row page-title">
			<p class="white-text">
				@if($adset->description)
				{{$adset->description}} <br>
				@endif
				<strong>{{$posts->total()}} Posts guardados</strong> en "{{$adset->name}}".
			</p>	
		</div>

		{{-- USUARIOS --}}
		<div class="row" style="position: relative; margin: 0px;">
			<ul class="collapsible collapsible-accordion users-expandible">
				<li class="no-padding">
					<a class="collapsible-header users-header"><i class="material-icons">people</i>Usuarios</a>

					<div class="collapsible-body container-m users-container grey lighten-3">
						@if($userLevel == 10)
						<div>
							<a class="add-brand modal-trigger" href="#add-user">
								<i class="material-icons header v-middle">add</i> Agregar Usuario
							</a>
						</div>
						@endif
						@if(sizeof($adset->admins()->users) > 0)
						<div class="admins">
							<h6>Administradores</h6>
							<div class="row">
								@foreach($adset->admins()->users as $user)
								<a class="user-label" @if($userLevel == 10)onclick="removeUser({{$user->id}}, '{{$user->name}}', 'admin')"@endif>{{$user->name}}</a>
								@endforeach
							</div>
						</div>
						@endif
						@if(sizeof($adset->editors()->users) > 0)
						<div class="editors">
							<h6>Pueden agregar posts</h6>
							<div class="row">
								@foreach($adset->editors()->users as $user)
								<a class="user-label" @if($userLevel == 10)onclick="removeUser({{$user->id}}, '{{$user->name}}', 'editor')"@endif>{{$user->name}}</a>
								@endforeach
							</div>
						</div>
						@endif
						@if(sizeof($adset->readers()->users) > 0)
						<div class="readers">
							<h6>Pueden Ver</h6>
							<div class="row">
								@foreach($adset->readers()->users as $user)
								<a class="user-label" @if($userLevel == 10)onclick="removeUser({{$user->id}}, '{{$user->name}}', 'reader')"@endif>{{$user->name}}</a>
								@endforeach
							</div>
						</div>
						@endif

					</div>
				</li>
			</ul>
		</div>

	</div>

	{{-- @include('consults.date-min',  array('route' =>'adset/'.$adset->id)) --}}

	@if (Session::has('message'))
	<div class="row red darken-4 animated fadeIn">
		<div class="col s12 m12 l12 page-title">
			<p class="white-text">{{ Session::get('message') }} </p>
		</div>
	</div>
	@endif
	
	@if(session('success'))
	<div class="col s12 m12 l12">
		<div class="light-green message animated fadeInLeftBig">
			<p class="white-text"> <i class="material-icons" style="margin-right: 5px;">check</i>{{ session('success') }} </p>
		</div>
	</div>
	@endif

</div>




<div class="container-m posts">	
	<div class="row">
		@foreach($posts as $key => $post)
		@include('posts.post-min', ['user' => $user])
		@endforeach 
	</div>
	<div class="row">
		<div class="col s12 m12 l12">
			{{ $posts->links() }}
		</div>
	</div>
</div>






<div id="add-user" class="modal" style="z-index: 1000;">

	<h2 class="title dark-blue-graph">Agrega un usuario al AdSet</h2>
	<div class="modal-content">
		<div class="row">
			<div class="row">
				<input id="role-admin" type="radio" name="role" value="admin"> 
				<label for="role-admin">Administrar</label>
				<input id="role-editor" type="radio" name="role" value="editor">
				<label for="role-editor">Editar</label>
				<input id="role-reader" type="radio" name="role" value="reader" checked="true">
				<label for="role-reader">Ver</label>
			</div>
			<div class="row" id="users-selected">
				
			</div>
			<div class="row">
				<input type="text" list="userslist" multiple id="userselector" autocomplete="off" onchange="onchangeUser()">
				<datalist id="userslist" >
					@foreach(App\User::get() as $u)
					<option value="{{$u->id}}" label="{{$u->name}}" name="{{$u->id}}"> </option>
					@endforeach
				</datalist>
				<a class="btn" onclick="registerUsers()">Agregar</a>
			</div>
		</div>		
	</div>
	<div class="modal-footer">
		<a class="modal-action modal-close waves-effect waves-blue btn-flat">Cerrar</a>
	</div>
</div>



<div id="modal-delete-user" class="modal bottom-sheet">
	<div class="modal-content">
		<h4 id="title-eliminar-usuario">Eliminar Usuario</h4>
		<p id="txt-eliminar-usuario">

		</p>
	</div>
	<div class="modal-footer">
		<a class="modal-action modal-close waves-effect waves-green btn-flat" onclick="$('#modal-delete-user').modal('close');"> Cancelar</a>
		<a id="url-eliminar-usuario" href="#" class="modal-action modal-close waves-effect waves-green btn-flat">Eliminar</a>
	</div>
</div>


<div id="modal-delete-adset" class="modal modal-fixed-footer brands">
	<h2 class="title dark-blue-graph">Eliminar AdSet</h2>
	<div class="modal-content">
		<p class="center">Si eliminas el AdSet "{{$adset->name}}" se eliminará su asociación con posts y usuarios.</p>

		{!! Form::open(['method' => 'DELETE', 'route' => ['adset.destroy',  $adset->id]]) !!}

		<div class="row">
			<div class="col s12 m8 l8 offset-m2 offset-l2">
				{!!Form::submit('Eliminar', ['class' => 'btn btn-primary red darken-4', 'style' => 'width:100%'] ) !!}
			</div>
		</div>

		{!! Form::close() !!}


	</div>
	<div class="modal-footer">
		<a href="#" class="modal-action modal-close waves-effect waves-green btn-flat">Cerrar</a>
	</div>
</div>

<script>
	var selectedUsers =  {};
	var userSelector =  document.getElementById("userselector");
	var userList =  document.getElementById("userslist");
	var userSelected =  document.getElementById("users-selected");

	/**
	* Busca el usuario seleccionado, si existe lo agrega a los usuarios seleccionados y actualiza los usuarios seleccionados. luego limpia el campo del formulario para poder ingresar otro usuario.
	* Postcondition: si existe, se ha agregado un usuario a la lista.
	* Postcondition: Se ha limpiado el selector de usuarios.
	*/
	function onchangeUser(){
		var sel = userList.options.namedItem(userselector.value);
		if(sel){
			selectedUsers[sel.value] = sel.label;
			updateSelectedUsers();
		}
		userselector.value = "";
	}

	/**
	* Elimina un usario seleccionado y vuelve a pintar los usuarios seleccionados.
	* Postcondition: se ha eliminado un objeto de la lista de usuarios seleccionados.
	*/
	function deselectuser(key){
		delete selectedUsers[key];
		updateSelectedUsers();
	}
	/**
	* Actualiza la lista de usuarios seleccionados en el formulario de agregarusuarios.
	* Postcondition: el html que muestra los usuarios seleccionados ha sido actualizado.
	*/
	function updateSelectedUsers(){
		var html= "";
		Object.keys(selectedUsers).forEach(function(key){
			html += " <span class='filter-tag' onclick='deselectuser("+key+")'>"+selectedUsers[key]+" </span>";
		});
		userSelected.innerHTML = html;
	}

	/**
	* Crea la url para agregar los usuarios al adset y redirige a dicha url si hay usuarios seleccionados.
	*/
	function registerUsers(){
		var usersList = "";
		Object.keys(selectedUsers).forEach(function(key){
			usersList += ((usersList == "")? "": ",") + key;
		});
		if(usersList == ""){
			userSelected.innerHTML = "<br><span class='filter-tag' onclick='updateSelectedUsers()' style='color:white; background-color:#b71c1c; padding:12px; font-weigth:bolder;'> Selecciona al menos un usuario a agregar. </span>";
		}else{	
			var type = $("input[name=role]:checked").val();
			// var addUserURL = "http://owak.co/graph/public/index.php";
			var addUserURL = getUrl('adset/add/{{$adset->id}}', type, usersList, 'agregar');
			// var addUserURL = window.location.protocol+"//"+window.location.host+"/adset/add/{$adset->id}}/"+type+"/"+usersList+"/agregar";
			// alert(addUserURL);
			window.location = addUserURL;
		}
	}

	/**
	* Muestra un mensaje de confirmación para elminar un usuario del adset y actualiza la url para su eliminación.
	* @param {int} id: id del usuario a eliminar.
	* @param {string} name: nombre del usuario a eliminar.
	* @param {string} type: nomre del grupo a eliminar (admin, editor, reader)
	* Postcondition: la url de eliminción de usuario ha cambiado.
	*/
	function removeUser(id, name, type){
		$('#txt-eliminar-usuario').html("Seguro que quieres eliminar a " + name + " del grupo \"" + type+"\".");
		// var addUserURL = "http://owak.co/graph/public/index.php";
		$('#title-eliminar-usuario').html('Eliminar '+type);
		var removeUserURL = "{{url('adset/add')}}/{{$adset->id}}/"+type+"/"+id+"/eliminar";
		$('#url-eliminar-usuario').attr("href", removeUserURL);
		$('#modal-delete-user').modal('open');
	}

	// function removeAdset(){
	// 	$('#txt-eliminar-usuario').html("Seguro que quieres eliminar el AdSet \"{ {$adset->name}}\".");
	// 	$('#title-eliminar-usuario').html('Eliminar AdSet');
	// 	var removeAdSetURL = "{ { route('adset.destroy', $adset->id) }} ";
	// 	$('#url-eliminar-usuario').attr("href", removeAdSetURL);
	// 	$('#url-eliminar-usuario').attr("data-method", 'delete');
	// 	$('#modal-delete-user').modal('open');
	// }
</script>

@endsection