
<?php $layout= 'layouts.default'; ?>
@if(isset($issection) && $issection===true)
<?php $layout= 'layouts.app'; ?>
@extends($layout)
@section('content')
<div class="row">
    <h2 class="title dark-blue-graph">Añadir Adset </h2>
</div>
<div class="row">
 @if (Session::has('message'))
 <div class="row red darken-4 animated fadeIn">
    <div class="col s12 m12 l12 page-title">

        <p class="white-text">{{ Session::get('message') }} </p>
    </div>

</div>
@endif
@if(session('success'))
<div class="col s12 m12 l12">
    <div class="light-green message animated fadeInLeftBig">
        <p class="white-text"> <i class="material-icons" style="margin-right: 5px;">check</i>{{ session('success') }} </p>
    </div>
</div>
@endif
@if(session('error'))
<div class="col s12 m12 l12">
    <div class="light-green message animated fadeInLeftBig">
        <p class="white-text"> <i class="material-icons" style="margin-right: 5px;">clear</i>{{ session('error') }} </p>
    </div>
</div>
@endif
</div>

<div class="row">
    <div class="col s12 m8 l8 offset-m2 offset-l2">

        @endif

        <?php 
        $user =(isset($user) && $user != null)? $user: App\User::where('id',11)->first();
    // $user =(isset($user) && $user != null)? $user: ((Auth::check())? Auth::user(): false);
    // UserRole::addBasicPermissions($user);
    // $user = (Auth::check())? Auth::user(): false; 
        ?>

        <div class="adset-form">
           <form action="{{route('adset.store')}}" method="POST">
             {{ csrf_field() }}
             <input type="hidden" name="user_id" value="{{$user->id}}">
             <label for="name">Nombre de Adset</label>
             <input type="text" name="name">
             <br>
             <label for="description">Descripción</label>
             <input type="text" name="description">
             <br>
             <input type="submit" value="Crear" class="btn btn-primary green" style="width:100%">
         </form>
     </div>


     @if(isset($issection) && $issection===true)
 </div>
</div>
@endsection
@endif