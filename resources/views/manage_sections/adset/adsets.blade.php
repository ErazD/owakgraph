@extends('layouts.app')

@section('content')

<div class="row grey lighten-3 mb-0">
  <div class="col s12 m12 l12 page-title">
   <h2 class="breadcrumbs-title left">Adsets</h2>
   <a class="add-brand right modal-trigger" href="#add">
    <i class="material-icons header">add_circle</i>
  </a>
</div>
</div>

<div class="row blue darken-3 mb-0">
  <div class="col s12 m12 l12 page-title">

    <p class="white-text">Actualmente perteneces a <strong>{{ sizeof($adsets) }}</strong> adset(s) en <strong>OWAK Graph</strong></p>

  </div>

</div>

@if (Session::has('message'))
<div class="row red darken-4 mb-0 animated fadeIn">
  <div class="col s12 m12 l12 page-title">

    <p class="white-text">{{ Session::get('message') }} </p>
  </div>

</div>
@endif


<!-- Modal Structure -->
<div id="add" class="modal modal-fixed-footer adsets">

  <h2 class="title dark-blue-graph">Añadir AdSet</h2>
  <div class="modal-content">
    <div class="row">
     @include('manage_sections.adset.create-adset')
   </div>
 </div>
 <div class="modal-footer">
  <a href="#" class="modal-action modal-close waves-effect waves-blue btn-flat">Cerrar</a>
</div>
</div>

<div class="container-m">
  <div class="row" id="brands">

    @if(session('success'))
    <div class="col s12 m12 l12">
      <div class="light-green message animated fadeInLeftBig">
        <p class="white-text"> <i class="material-icons" style="margin-right: 5px;">check</i>{{ session('success') }} </p>
      </div>
    </div>
    @endif


    @foreach($adsets as $adset)
    @include('manage_sections.adset.adset-card')
    @endforeach

  </div>

</div>


@endsection