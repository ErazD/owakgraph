  {{-- Rquires: $user, $post     | Optional: $userAdsets --}}

@if(isset($user) && $user != null)
      <?php
      $userAdsets = (isset($userAdsets))? $userAdsets : ((isset($user))? App\AdSet::getAdsetsFromUserId($user->id, 1): null);
      $postAdSets = App\AdSet::getAdsetsUserPost($user->id, $post->id, clone $userAdsets);
      $noPostAdsets =  App\AdSet::getAdsetsUserNotPost($user->id, $post->id, clone $userAdsets);
      // dd($noPostAdsets, $userAdsets->get());
      $allPostAdSets = App\AdSet::getAdsetsUserPost($user->id, $post->id, App\AdSet::getAdsetsFromUserId($user->id, 0));
      $strPostAdsets = "";
      foreach ($allPostAdSets as $key => $value) { 
        $strPostAdsets .= (($strPostAdsets == "")?"":", ").$value->name;
      }
      ?>

    <h5 class="stop-propagation">
      <a class="dropdown-button" href="#" data-activates='dropdown-{{$post->id}}' data-beloworigin="true" data-constrainWidth="true">

        @if(sizeof($allPostAdSets))
          <i class="white-text material-icons tooltipped" aria-hidden="true" data-position="right" data-enable-html="true" data-delay="50" data-tooltip="{{$strPostAdsets}}">bookmark</i>
        @else
          <i class="white-text material-icons" aria-hidden="true">bookmark_border</i>
        @endif

      </a>
    </h5>

    <ul id='dropdown-{{$post->id}}' class='dropdown-content adset-menu'>
      @if(sizeof($noPostAdsets) > 0)
        <li class="adset-menu-item"><a href="#" class="dropdown-button" data-activates='dropdown-add-{{$post->id}}' data-hover="hover" data-alignment="right" data-constrainWidth="true"><i class="material-icons">add</i>Agregar a</a></li>
      @endif
      @if(sizeof($postAdSets) > 0)
        <li class="adset-menu-item"><a href="#" class="dropdown-button" data-activates='dropdown-remove-{{$post->id}}' data-hover="hover" data-alignment="right" data-constrainWidth="true"><i class="material-icons">remove</i>Eliminar de</a></li>
      @endif
    </ul>

    <ul id='dropdown-add-{{$post->id}}' class='dropdown-content adset-submenu adset-add'>
      @foreach($noPostAdsets as $uAdset)
      <li class="adset-submenu-item"><a href="{{ url('adset/add/'.$uAdset->id.'/post/'.$post->id.'/agregar') }}">{{$uAdset->name}}</a></li>
      @endforeach
    </ul>

    <ul id='dropdown-remove-{{$post->id}}' class='dropdown-content adset-submenu adset-remove'>
      @foreach($postAdSets as $pAdset)
      <li class="adset-submenu-item"><a href="{{ url('adset/add/'.$pAdset->id.'/post/'.$post->id.'/eliminar') }}">{{$pAdset->name}}</a></li>
      @endforeach
    </ul>
    @endif