    <div class="col s12 m3">
      <div class="card white">
        <div class="card-content grey-text">
          <span class="card-title">{{ $adset->name }}</span>
          <p>"{{ $adset->description }}".</p>
          <p><strong class="blue-text">{{ $adset->posts->count() }}</strong> Posts guardados</p>
        </div>
        <div class="card-action">
          <a href="{{ url('/adset/'.$adset->id) }}" class="btn btn-default">Ver más</a>
        </div>
      </div>
    </div>