@if(Auth::check())
<li class="bold">
	<?php  
	$userFront = (Auth::check())? Auth::user():false;  
	?>
	<p class='no-margin waves-effect waves-teal text-center white-text' style='font-size:14px'>Bienvenid@, {{$userFront}}</p>
</li>
@endif
<li class="bold">
	<ul class="collapsible collapsible-accordion">
		<li>
			<a class="collapsible-header white-text"><i class="fa fa-cog white-text material-icons" aria-hidden="true"></i> Admin</a>
			<div class="collapsible-body">
				<ul>
				    <li><a href="{{ url('permissions/groups/manual') }}">Asignar Permisos de Grupo</a></li>
				    <li><a href="{{ url('two-factor') }}">Asignar Two Factor</a></li>
					<li><a href=" {{ url('vertical/create') }} ">Agregar Industria</a></li>
					<li><a href=" {{ url('categoria/create') }} ">Agregar Sector</a></li>
					<li><a href="{{ url('canasta/create') }}">Agregar Categorías</a></li>
					<li><a href="{{ url('segmento/create') }}">Agregar Segmentos</a></li>
				</ul>
			</div>
		</li>
	</ul>
</li>
<li class="bold">
	<a href="{{ url('/') }}" class="waves-effect waves-teal white-text">
		<i class="material-icons white-text">copyright</i> Marcas
	</a>
</li>
<li class="bold">
	<a href="{{ url('/vertical') }}" class="waves-effect waves-teal white-text">
		<i class="material-icons white-text">sort</i> Industria
	</a>
</li>
<li class="bold">
	<ul class="collapsible collapsible-accordion">
		<li>
			<a class="collapsible-header white-text"><i class="material-icons white-text">assignment</i> Discovery</a>
			<div class="collapsible-body">
				<ul>
					<li><a href="{{ url('/comms/discovery/Facebook/all') }}" >Facebook</a></li>
					<li><a href="{{ url('/comms/discovery/Youtube/all') }}" >Youtube</a></li> 
					<li><a href="{{ url('/comms/discovery/Instagram/all') }}" >Instagram</a></li> 
				</ul>
			</div>
		</li>
	</ul>
</li>
<li class="bold">
	<ul class="collapsible collapsible-accordion">
		<li>
			<a class="collapsible-header white-text"><i class="fa fa-eye white-text" aria-hidden="true"></i> Consultas</a>
			<div class="collapsible-body">
				<ul>
					<li><a href=" {{ url('comms') }} ">Gráficas Comms</a></li>
					<li><a href="{{ url('comms/consult') }}">Tasas Comms</a></li>
					<li><a href="{{ url('comms/sharesof') }}">Share of Rates</a></li>
					<li><a href="{{ url('comms/consult/posts') }}">Top of Posts</a></li>
					<li><a href="{{ url('comms/heatmap') }}">HeatMap</a></li>
				</ul>
			</div>
		</li>
	</ul>
</li>
<li class="bold">
	<ul class="collapsible collapsible-accordion">
		<li>
			<a class="collapsible-header white-text"><i class="material-icons white-text">chat_bubble_outline</i> Publicaciones</a>
			<div class="collapsible-body">
				<ul>
					<li><a href=" {{ url('latest-posts') }} ">Más recientes</a></li>
					<li><a href="{{ url('hot-posts') }}">Más activas</a></li>
				</ul>
			</div>
		</li>
	</ul>
</li>

<li class="bold">
	<ul class="collapsible collapsible-accordion">
		<li>
			<a class="collapsible-header white-text"><i class="material-icons white-text">insert_chart</i> Reporte rápido</a>
			<div class="collapsible-body">
				<ul>
					<li><a href="{{ url('quickly-report/fanpage') }}">Publicaciones</a></li>
					<li><a href=" {{ url('quickly-report/posts') }} ">Comentarios</a></li>
				</ul>
			</div>
		</li>
	</ul>
</li>
@if (!Auth::check())
<li class="bold active"><a href="{{ url('login') }}" class="waves-effect waves-teal white-text"><i class="material-icons white-text">verified_user</i> Iniciar sesión</a></li>
@else
<li class="bold">
	<a href="{{ url('token') }}" class="waves-effect waves-teal white-text">
		<i class="material-icons white-text">memory</i> Token
	</a>
</li>

<li class="bold active">

	<a class="waves-effect waves-teal white-text" href="{{ route('logout') }}"
	onclick="event.preventDefault();
	document.getElementById('logout-form').submit();">
	<i class="material-icons white-text">power_settings_new</i> Cerrar sesión
</a>


<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
	{{ csrf_field() }}
</form>

</li>
@endif