@extends('layouts.app')
<?php $user = (Auth::check())? Auth::user(): false; ?>
@section('scripts')
<script src="{{ asset('js/vue.min.js') }}"></script>
<script src="{{ asset('js/appComms.js') }}"></script>
<script src="{{ asset('js/date-selector.js') }}"></script>
{{-- <script src="{{ asset('js/main.js') }}"></script> --}}

<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.6.0/Chart.min.js"></script>
<script src="https://www.amcharts.com/lib/3/amcharts.js"></script>
<script src="https://www.amcharts.com/lib/3/serial.js"></script>
<script src="https://www.amcharts.com/lib/3/plugins/export/export.min.js"></script>
<link rel="stylesheet" href="https://www.amcharts.com/lib/3/plugins/export/export.css" type="text/css" media="all" />
<script src='{{ asset('js/subs-count.js') }}'></script>

@if(isset($since))

<script>
	comms_register = "{{ route('comms_register')}}";
	dateSelectorApp.since = "{{$since}}";
	dateSelectorApp.until = "{{$until}}";
</script>
@endif
@stop 
@section('content')
<style>
	.amcharts-export-menu .export-main>a, .amcharts-export-menu .export-drawing>a, .amcharts-export-menu .export-delayed-capturing>a {
		width: 25px !important;
		height: 25px !important;
	}
	.no-show{
		display:none;
	}
</style>
<div class="page" id="fanpage">
	<div class="row grey lighten-3 mb-0">
		<div class="col s12 m12 l12 page-title">

			<div class="header-content left">
				<h2>{{$channel->brand->name}} {{ $channel->name }}</h2>
				<h6><a href="{{ $channel->url }}">{{ $channel->url }}</a></h6>
				<div class="btn btn-primary red darken-3" style="font-size: .8em; line-height: 22px; height: 22px;">YouTube</div>
			</div>

			<div class="right">
				<a class="add-brand right modal-trigger" href="#add-post">
					<i class="material-icons header">add</i>
				</a>

				<a class="add-brand right modal-trigger ml-10 tooltipped" data-position="bottom" data-delay="50" data-tooltip="Buscar publicaciones" href="{{ url('channel/'.$channel->id.'/scrap') }}">
					<i class="material-icons header green-text">loop</i>
				</a>

				<a class="add-brand right modal-trigger ml-10 tooltipped" data-position="bottom" data-delay="50" data-tooltip="Hot Posts" href="{{ url('channel/'.$channel->id.'/hot-posts') }}">
					<i class="material-icons header red-text">whatshot</i>
				</a>

			</div>
		</div>
	</div>

	<!-- Modal Structure -->
	<div id="add-post" class="modal modal-fixed-footer fanpage">

		<h2 class="title dark-blue-graph">Añadir Publicación</h2>
		<div class="modal-content">

			{!! Form::open(['route' => 'add_video', 'onSubmit' =>'submitForm()']) !!}
			<div class="row">
				<div class="col s12 m10 l10 offset-m1 offset-l1">
				<p>Existen publicaciones ocultas. Con esta funcionalidad podrás añadir una publicación manualmente. Ten presente solo añadir las publicaciónes de <a>{{$channel->url}}</a></p>

					<label for="name" class="col-md-4 control-label">URL de la publicación</label>

					{!! Form::url('video_url', 'https://www.youtube.com/watch?v=3lNxZOQF_lU', array('class' => 'mb-5')) !!}
					{!! Form::hidden('channel_id', $channel->id, array('class' => 'm-0')) !!}

					@if ($errors->has('url'))
					@foreach($errors->get('url') as $error)
					<p style="color:#b71c1c;">{{ $error }}</p>
					@endforeach
					@endif

					{!!Form::submit('Agregar', ['class' => 'find btn btn-primary green m-0', 'style' => 'width:100%'] ) !!}
					<div id="scrap">
						<div class="progress mt-5">
							<div class="indeterminate"></div>
						</div>
					</div>

				</div>
			</div>

			<script type=text/javascript>
				function submitForm()
				{	
					$('.btn.green.find').hide();
					$('.progress').show();
				}

			</script>


			{!! Form::close() !!}


		</div>
		<div class="modal-footer">
			<a href="#!" class="modal-action modal-close waves-effect waves-blue btn-flat">Cerrar</a>
		</div>
	</div>
	<div id="fan-state">
	<div class="fans-state col s12 m12 l12 white" v-for='data in allData'>
		<div  v-bind:id='"fancount-chart-" + data["name"]' class="fan-state-graph"></div>
	</div>
</div>
	@include('utils.sections.scrapt-menu', ['element'=> $channel, 'modelName'=>'YoutubeChannel', 'isPost'=>false])
	<div class="row red darken-3 mb-0">
		<div class="col s12 m12 l12 page-title">

			<p class="white-text">Hemos encontrado <strong>{{$posts->total()}} publicaciones</strong> en nuestra base de datos.</p>

			<div class="search">
				{!! Form::open(['route' => ['search_video', $channel->id], 'method' => 'GET']) !!}
				
				<div class="row mb-0">
					<div class="col s12 m10 l10">

						{!!Form::text('query', null, ['class' => 'white-text mb-0', 'placeholder' => 'Buscar publicación...']) !!}
					</div>
					<div class="col s12 m2 l2">
						{!!Form::submit('Ir', ['class' => 'btn btn-primary green', 'style' => 'width:100%; margin-top: 3px;'] ) !!}
					</div>

				</div>

				{!! Form::close() !!}
			</div>

			
		</div>

	</div>
		@if(isset($since))
			<div id="date-form" class="row formDateContainer" style="padding: 8px;">
						<div class="col s12 m6 l3">
							<input v-model="since" type="text" placeholder="Inicio: Y-m-d">	
						</div>
						<div class="col s12 m6 l3">
							<input v-model="until" type="text" placeholder="Fin: Y-m-d">
						</div>
						<div class="col s12 m6 l3">
							<select v-model="interval" v-on:change="setDate(interval)" class="browser-default"  name="interval"   id="">
								<optgroup label="" >
									<option value="all">Todo</option>
								</optgroup>
								<optgroup label="" >
									<option value="hoy">hoy</option>
									<option value="day">El mismo Día</option>
								</optgroup>
								<optgroup label="">
									<option value="6">Semana anterior</option>
									<option value="28">28 días anteriores</option>
									<option value="30">30 días anteriores</option>
								</optgroup>
								<optgroup label="">
									<option value="90">trimestre anterior (90 días)</option>
									<option value="182">semestre anterior (182 días)</option>
									<option value="365">año anterior</option>
								</optgroup>
							</select>
						</div>	

						<div class="col s12 m6 l3">
							<a v-bind:href=" '{{ url('channel/'.$channel->id )}}' + getDates" class="btn deep-orange darken-4 tooltipped" data-position="bottom" data-delay="50" data-tooltip="Para filtrar por fecha es importante la fecha final">
								Filtrar por fecha
							</a>
						</div>				
					</div>
				@endif



	@if (Session::has('message'))
<div class="row red darken-4 animated fadeIn">
  <div class="col s12 m12 l12 page-title">

    <p class="white-text">{{ Session::get('message') }} </p>
  </div>

</div>
@endif

</div>


<div class="container-m posts">
	
<div class="row">
		@foreach($posts as $key => $post)
		@include('youtube.videos.video-min', ['post'=>$post])
		@endforeach 
</div>


	<div class="modal modal-fixed-footer" id="addContest">
	<h2 class="title dark-blue-graph">Agregar Concurso</h2>
	<div class="modal-content">

		{!! Form::open(['route' => 'contest.store', 'onSubmit' =>'submitForm()']) !!}
		<div class="row">
			<div class="col s12 m10 l10 offset-m1 offset-l1">
				<label for="name" class="col-md-4 control-label">Nombre</label>
				{!! Form::text('name', 'Nombre del Concurso', array('class' => 'mb-5')) !!}
				<label for="text" class="col-md-4 control-label">Mecánica</label>
				{!! Form::textarea('mechanics', 'Mecánica del Concurso', array('class' => 'mb-5')) !!}
				<label for="text" class="col-md-4 control-label">Premio</label>
				{!! Form::text('prize', 'Premio del Concurso', array('class' => 'mb-5')) !!}
				<label for="date" class="col-md-4 control-label">Desde</label>
				{!! Form::date('from-date', \Carbon\Carbon::now()); !!}
				<label for="date" class="col-md-4 control-label">Hasta</label>
				{!! Form::date('to-date', \Carbon\Carbon::now()); !!}
				{!! Form::hidden('brand_id', $channel->brand->id) !!}
				@if ($errors->has('url'))
				@foreach($errors->get('url') as $error)
				<p style="color:#b71c1c;">{{ $error }}</p>
				@endforeach
				@endif

				{!!Form::submit('Agregar', ['class' => 'find btn btn-primary green m-0', 'style' => 'width:100%'] ) !!}
				<div id="scrap">
					<div class="progress mt-5">
						<div class="indeterminate"></div>
					</div>
				</div>
			</div>
		</div>
		<script type=text/javascript>
			function submitForm()
			{	
				$('.btn.green.find').hide();
				$('.progress').show();
			}

		</script>
		{!! Form::close() !!}


	</div>
	<div class="modal-footer">
		<a href="#!" class="modal-action modal-close waves-effect waves-blue btn-flat">Cerrar</a>
	</div>
</div>	


<div id="add-adset" class="modal modal-fixed-footer adsets">

	<h2 class="title dark-blue-graph">Añadir AdSet</h2>
	<div class="modal-content">
		<div class="row">
			@include('manage_sections.adset.create-adset')
		</div>
	</div>
	<div class="modal-footer">
		<a href="#" class="modal-action modal-close waves-effect waves-blue btn-flat">Cerrar</a>
	</div>
</div>

	<div class="row">
		<div class="col s12 m12 l12">
			{{ $posts->links() }}
		</div>
	</div>

</div>
</div>


@endsection