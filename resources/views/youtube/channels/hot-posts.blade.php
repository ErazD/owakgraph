@extends('layouts.app')

@section('content')

<div class="page" id="fanpage">
	<div class="row grey lighten-3 mb-0">
		<div class="col s12 m12 l12 page-title">

			<div class="header-content left">
				<h2>{{ $channel->brand->name }} {{ $channel->name }}</h2>
			</div>

		</div>
	</div>

	<div class="row blue darken-3 mb-0">
	<div class="col s12 m12 l12 page-title">

		<p class="white-text">En esta sección encontrarás las <strong>publicaciones</strong> más activas del día.</p>
	</div>

</div>

</div>


<div class="container-m posts">
	<div class="row">
		
		@if($hot_videos->isEmpty())
		No hay interacciones en ninguna de las publicaciones de esta Fan Page.<br/><br/>

		<a href="{{ url('/channel/'.$channel->id)}}">Regresar</a>
		@else
		@foreach($hot_videos as $key => $metric)

		<div class="col s12 m4 l4">
			<div class="card">

				<!-- Dropdown Structure -->
				<ul id='dropdown{{$key}}' class='dropdown-content'>
					<li><a href="{{ url('youtube-video/'.$metric->id) }}"><i class="material-icons left">insert_chart</i> Métricas ({{ count($metric->metrics) }})</a></li>
					<li><a href="{{ $metric->permalink_url }}" target="_blank"><i class="material-icons left">link</i> Visitar</a></li>
				</ul>
				<a href="{{ url('youtube-video/'.$metric->id) }}">
					<div class="attachment" style="background-image: url({{$metric->attachment}})">

						@if($metric->type == 'photo')
						<div class="waves-effect waves-light btn green circle-icon"><i class="material-icons center">image</i></div>
						@endif
						@if($metric->type == 'video')
						<div class="waves-effect waves-light btn red circle-icon"><i class="material-icons center">ondemand_video</i></div>
						@endif
						@if($metric->type == 'status')
						<div class="waves-effect waves-light btn green circle-icon"><i class="material-icons center">format_quote</i></div>
						@endif
						@if($metric->type == 'link')
						<div class="waves-effect waves-light btn blue circle-icon"><i class="material-icons center">insert_link</i></div>
						@endif


					</div>
				</a>


				<div class="content youtube-change">

					<div class="date valign-wrapper">
						<a href="{{ url('youtube-video/'.$metric->id) }}">{{ $metric->published }}</a>
						<div>
						
						<span class="new badge">{{ $metric->count }}</span>
						<a class='dropdown-button' href='#' data-activates='dropdown{{$key}}'><i class="material-icons">more_horiz</i></a>
						</div>
					</div>

					@if(!is_null($metric->content))
					<b>{{$metric->title}}</b> <br>
					<p class="no-margin">{{ $metric->content}}</p>
					@else
					<p class="no-margin">Esta publicación no tiene una descripción.</p>
					@endif
				</div>
                
				<a class="cta flex" href="{{ url('youtube-video/'.$metric->id) }}">
					<div class="shares flex">
						<i class="material-icons">visibility</i> &nbsp {{$metric->views}} 
					</div>

					<div class="reactions flex">
						<i class="material-icons">thumb_up</i> &nbsp {{ $metric->likes }}
					</div>

					<div class="comments flex">
						<i class="material-icons">thumb_down</i> &nbsp {{ $metric->dislikes }}
					</div>

					<div class="shares flex">
						<i class="material-icons">mode_comment</i> &nbsp {{ $metric->comments }} 
					</div>
				</a>
				
			</div>
		</div>
		@endforeach 
		@endif



	</div>


</div>

<div class="row">
		<div class="col s12 m12 l12">
			{{ $hot_videos->links() }}
		</div>
	</div>

</div>


@endsection