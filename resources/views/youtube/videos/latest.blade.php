@extends('layouts.app')

@section('content')

<div class="page" id="fanpage">
	<div class="row grey lighten-3 mb-0">
		<div class="col s12 m12 l12 page-title">

			<div class="header-content left">
				<h2>Publicaciones más recientes</h2>
			</div>

		</div>
	</div>

	<div class="row blue darken-3 mb-0">
	<div class="col s12 m12 l12 page-title">

		<p class="white-text">En esta sección encontrarás las <strong>10 publicaciones</strong> más recientes.</p>

	</div>

</div>

</div>


<div class="container-m posts">
	<div class="row">

		@foreach($latest_posts as $key => $post)
		<div class="col s12 m4 l4">
			<div class="card">

				<!-- Dropdown Structure -->
				<ul id='dropdown{{$key}}' class='dropdown-content'>
					<li><a href="{{ url('posts/'.$post->id) }}"><i class="material-icons left">insert_chart</i> Métricas ({{ count($post->metrics) }})</a></li>
					<li><a href="{{ $post->permalink_url }}" target="_blank"><i class="material-icons left">link</i> Visitar</a></li>
				</ul>
				<a href="{{ url('posts/'.$post->id) }}">
					<div class="attachment" style="background-image: url({{$post->attachment}})">

						@if($post->type == 'photo')
						<div class="waves-effect waves-light btn green circle-icon"><i class="material-icons center">image</i></div>
						@endif
						@if($post->type == 'video')
						<div class="waves-effect waves-light btn red circle-icon"><i class="material-icons center">ondemand_video</i></div>
						@endif
						@if($post->type == 'status')
						<div class="waves-effect waves-light btn green circle-icon"><i class="material-icons center">format_quote</i></div>
						@endif
						@if($post->type == 'link')
						<div class="waves-effect waves-light btn blue circle-icon"><i class="material-icons center">insert_link</i></div>
						@endif


					</div>
				</a>


				<div class="content">
					<div class="date valign-wrapper">
						<a href="{{ url('posts/'.$post->id) }}">{{ $post->published }}</a>
						<a class='dropdown-button' href='#' data-activates='dropdown{{$key}}'><i class="material-icons">more_horiz</i></a>
					</div>

					@if(!is_null($post->content))
					<p class="no-margin">{{ $post->content}}</p>
					@else
					<p class="no-margin">Esta publicación no tiene una descripción.</p>
					@endif
				</div>

				<a class="cta flex" href="{{ url('posts/'.$post->id) }}">
					<div class="reactions flex">
						<img src="{{ asset('images/reactions.png') }}" alt="Reacciones"> {{ $post->latestMetric->reactions }}
					</div>

					<div class="comments flex">{{ $post->latestMetric->comments }} comentarios</div>

					<div class="shares flex">{{ $post->latestMetric->shares }} compartidos</div>
				</a>
				
			</div>
		</div>
		@endforeach 



	</div>


</div>
</div>


@endsection