@extends('layouts.app')
@php 
	$user = (Auth::check())? Auth::user(): false;
	$user = App\User::where('id',11)->first();
	$hasUser = isset($user) && $video->hasUser($user);
	$isJudge = isset($user) && $user->twoFactorProfile && $user->twoFactorProfile->rank > 0;
	$isIncomplete = $hasUser && $video->twofactors->where('profile_id',$user->twoFactorProfile->id)->first()->status < 2;
	$isAdmin = App\UserRole::userHasPermissions($user, 'admin')
@endphp
@section('scripts')
<script src="{{ asset('js/vue.min.js') }}"></script>
<script src="{{ asset('js/moment.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.6.0/Chart.min.js"></script>
<script src="https://www.amcharts.com/lib/3/amcharts.js"></script>
<script src="https://www.amcharts.com/lib/3/serial.js"></script>
<script src="{{ asset('js/appComms.js') }}"></script>
<script src="{{ asset('js/youtube-chart.js') }}"></script>
{{-- <script src="{{ asset('js/main.js') }}"></script> --}}
@stop
@section('content')

<div class="row grey lighten-3 mb-0">
	<div class="col s12 m6 l6 page-title">
		<h2 class="breadcrumbs-title left">
			Estadísticas</h2>
		</div>
		<div class="col s12 m6 l6" >
		<h5 class="right">		
			<br>
			<a class="add-brand right modal-trigger ml-10 tooltipped" data-position="bottom" data-tooltip="Comms Panorama" href="#comms-post" ><i class="fa fa-info-circle" aria-hidden="true"></i></a>

			@if($isAdmin)			
			<a class="add-brand right modal-trigger ml-10 tooltipped" data-position="bottom" data-tooltip="Agregar Métrica" href="#add-metric" ><i class="fa fa-plus-circle" aria-hidden="true"></i></a>

			<a class="add-brand right modal-trigger ml-10 tooltipped" data-position="bottom" data-tooltip="Editar Comms" href="#add-comms" ><i class="fa fa-cog" aria-hidden="true"></i></a>
			@endif
		</h5>
	</div>
	</div>

	<div class="row red darken-3">
		<div class="col s12 m12 l12 page-title">

			<p class="white-text" style="width: 80%;"> 
			@if(!is_null($video->content))
			{{ $video->content }}
			@else
			Esta publicación no tiene una descripción
			@endif
			</p>
			<div class="btn-group">
			<a href="{{ $video->permalink_url }}" target="_blank" class="btn btn-primary green tooltipped mr-5" style="margin-left: 20px;" data-position="bottom" data-delay="50" data-tooltip="Abrir en Facebook"><i class="material-icons">insert_link</i></a>

			<a href="{{ url('youtube-video/'.$video->id.'/export') }}" target="_blank" class="btn btn-primary green tooltipped" data-position="bottom" data-delay="50" data-tooltip="Descargar"><i class="material-icons">file_download</i></a>
			</div>

		</div>

	</div>



{{-- Modal Comms --}}
<div id="comms-post" class="modal" style="overflow-y: auto !important;">
	<h2 id="comms-title" class="title dark-blue-graph">Two Factor: Comms Panorama</h2>
	<div class="modal-content">	
		<div style="position: relative; padding-bottom: 30px;">
			@if($isIncomplete && !$isJudge)
			@include('comms.two-factor.register', ['post'=>$video, 'user'=>$user,'isJudge'=>$isJudge, 'hasUser'=>$hasUser, 'social_network'=>strtolower($video->social_network()), 'social_network_name'=>$video->social_network()])
			@else
			@include('comms.comms-post', ['post'=>$video, 'user'=>$user,'isJudge'=>$isJudge, 'hasUser'=>$hasUser,  'social_network'=>strtolower($video->social_network()), 'social_network_name'=>$video->social_network(), 'isIncomplete'=>$isIncomplete])
			@endif
		</div>
	</div>
	<div class="modal-footer">
		<a  class="modal-action modal-close waves-effect waves-blue btn-flat">Cerrar</a>
	</div>
</div>


@if($isAdmin)
{{-- Modal Comms --}}
<div id="add-comms" class="modal" style="overflow-y: auto !important;">
	<h2 id="comms-title" class="title dark-blue-graph">Editar valores comms</h2>
	<div class="modal-content">	
		<div style="position: relative; padding-bottom: 30px;">
			@include('comms.form-comms', ['post'=>$video, 'user'=>$user])
		</div>
	</div>
	<div class="modal-footer">
		<a  class="modal-action modal-close waves-effect waves-blue btn-flat">Cerrar</a>
	</div>
</div>

{{-- Modal Metrics --}}
<div id="add-metric" class="modal" style="overflow-y: auto !important;">

	<h2 id="comms-title" class="title dark-blue-graph">Actualizar métricas</h2>
	<div class="modal-content">	
		<div style="position: relative; padding-bottom: 30px;">
			@include('utils.posts.update-metrics', ['post'=>$video])
		</div>
	</div>

	<div class="modal-footer">
		<a  class="modal-action modal-close waves-effect waves-blue btn-flat">Cerrar</a>
	</div>
</div>
{{-- Modal contests --}}
<div class="modal modal-fixed-footer" id="addContest">
	<h2 class="title dark-blue-graph">Agregar Concurso</h2>
	<div class="modal-content">

		{!! Form::open(['route' => 'contest.store', 'onSubmit' =>'submitForm()']) !!}
		<div class="row">
			<div class="col s12 m10 l10 offset-m1 offset-l1">
				<label for="name" class="col-md-4 control-label">Nombre</label>
				{!! Form::text('name', 'Nombre del Concurso', array('class' => 'mb-5')) !!}
				<label for="text" class="col-md-4 control-label">Mecánica</label>
				{!! Form::textarea('mechanics', 'Mecánica del Concurso', array('class' => 'mb-5')) !!}
				<label for="text" class="col-md-4 control-label">Premio</label>
				{!! Form::text('prize', 'Premio del Concurso', array('class' => 'mb-5')) !!}
				<label for="date" class="col-md-4 control-label">Desde</label>
				{!! Form::date('from-date', \Carbon\Carbon::now()); !!}
				<label for="date" class="col-md-4 control-label">Hasta</label>
				{!! Form::date('to-date', \Carbon\Carbon::now()); !!}
				{!! Form::hidden('brand_id', $video->brand()->id) !!}
				@if ($errors->has('url'))
				@foreach($errors->get('url') as $error)
				<p style="color:#b71c1c;">{{ $error }}</p>
				@endforeach
				@endif

				{!!Form::submit('Agregar', ['class' => 'find btn btn-primary green m-0', 'style' => 'width:100%'] ) !!}
				<div id="scrap">
					<div class="progress mt-5">
						<div class="indeterminate"></div>
					</div>
				</div>
			</div>
		</div>
		<script type=text/javascript>
			function submitForm()
			{	
				$('.btn.green.find').hide();
				$('.progress').show();
			}
		</script>
		{!! Form::close() !!}
	</div>
	<div class="modal-footer">
		<a href="#" class="modal-action modal-close waves-effect waves-blue btn-flat">Cerrar</a>
	</div>
</div>

{{-- End Modals Admin --}}
@endif

	@include('utils.sections.scrapt-menu', ['element'=> $video, 'modelName'=>'YoutubeVideo', 'isPost'=>true])
	<div class="container-m" id="stats">
		
		<div class="row">
			<div class="col s12 m12 l12">
				<div class="title green">
					<h2>Estadísticas de hoy</h2>
					{{-- <ul class="tabs">
						<li class="tab" @click="metricsByHour('reactions')"><a href="#reactions" class="active">Reacciones</a></li>
						<li class="tab" @click="metricsByHour('comments')"><a href="#comments">Comentarios</a></li>
						<li class="tab" @click="metricsByHour('shares')"><a href="#shares">Shares</a></li>
					</ul> --}}

				</div>
				<div class="card white" id="hoursChartdiv">
					{{-- <canvas id="hoursChart"></canvas> --}}
					<div id="hoursChart" class="postChart">
						
					</div>
				</div>
			</div>

		</div>


		<div class="row">
			<div class="col s12 m12 l12">
				<div class="title blue">
					<h2>Estadísticas por día</h2>
					{{-- <ul class="tabs">
						<li class="tab" @click="metricsByDay('reactions')"><a href="#reactions" class="active">Reacciones</a></li>
						<li class="tab" @click="metricsByDay('comments')"><a href="#comments">Comentarios</a></li>
						<li class="tab" @click="metricsByDay('shares')"><a href="#shares">Shares</a></li>
					</ul> --}}

				</div>
				<div class="card white" id="daysChartdiv">
					{{-- <canvas id="daysChart"></canvas> --}}
					<div class="row formDateContainer">
						<div class="col s12 m6 l3">
							<input v-model="since" type="text" placeholder="Y-m-d">	
						</div>
						<div class="col s12 m6 l3">
							<input v-model="until" type="text" placeholder="Y-m-d">
						</div>
						<div class="col s12 m6 l3">
							<button  @click="zoomChart(chartMD)">Actualizar Gráfica</button>
						</div>
						<div class="col s12 m6 l3">
							<select v-model="interval" v-on:change="setDate(interval)" class="browser-default"  name="interval"   id="">
								<optgroup label="" >
									<option value="all">Todo</option>
								</optgroup>
								<optgroup label="" >
									<option value="hoy">hoy</option>
									<option value="day">El mismo Día</option>
								</optgroup>
								<optgroup label="">
									<option value="6">Semana anterior</option>
									<option value="28">28 días anteriores</option>
									<option value="30">30 días anteriores</option>
								</optgroup>
								<optgroup label="">
									<option value="90">trimestre anterior (90 días)</option>
									<option value="182">semestre anterior (182 días)</option>
									<option value="365">año anterior</option>
								</optgroup>
							</select>
						</div>					
					</div>
					<div id="daysChart" class="postChart">
						
					</div>
				</div>
			</div>

		</div>




	</div>
	@stop