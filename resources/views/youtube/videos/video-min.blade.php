<div class="col s6 m6 l6 flex post-card">
	<div class="card-summary col s11 m11 l11">
			<div class="card row">
				<!-- Dropdown Structure -->
				<ul id='dropdown{{$key}}' class='dropdown-content'>
					<li><a href="{{ url('youtube-video/'.$post->id) }}"><i class="material-icons left">insert_chart</i> Métricas ({{ count($post->metrics) }})</a></li>
					<li><a href="{{ $post->permalink_url }}" target="_blank"><i class="material-icons left">link</i> Visitar</a></li>
				</ul>
				<a href="{{ url('youtube-video/'.$post->id) }}">
					<div class="attachment" style="background-image: url({{$post->attachment}})">
						@if($post->type == 'photo')
						<div class="waves-effect waves-light btn green circle-icon"><i class="material-icons center">image</i></div>
						@endif
						@if($post->type == 'video')
						<div class="waves-effect waves-light btn red circle-icon"><i class="material-icons center">ondemand_video</i></div>
						@endif
						@if($post->type == 'status')
						<div class="waves-effect waves-light btn green circle-icon"><i class="material-icons center">format_quote</i></div>
						@endif
						@if($post->type == 'link')
						<div class="waves-effect waves-light btn blue circle-icon"><i class="material-icons center">insert_link</i></div>
						@endif
					</div>
				</a>
				<div class="content youtube-change">
					<div class="date valign-wrapper">
						<a href="{{ url('youtube-video/'.$post->id) }}">{{ $post->published }}</a>
						<a class='dropdown-button' href='#' data-activates='dropdown{{$key}}'><i class="material-icons">more_horiz</i></a>
					</div>

					@if(!is_null($post->content))
					<b>{{$post->title}}</b> <br>
					@php($len = 270 - strlen($post->title) / 45 * 55)
					<p class="no-margin">{{substr($post->content,0, $len)}}{{strlen($post->content) > $len? "...":""}}</p>
					@else
					<p class="no-margin">Esta publicación no tiene una descripción.</p>
					@endif
				</div>

				<a class="cta flex" href="{{ url('youtube-video/'.$post->id) }}">
					<div class="shares flex">
						<i class="material-icons">visibility</i> &nbsp {{ number_format($post->latestMetric->views, 0, '.', '.')}} 
					</div>

					<div class="reactions flex">
						<i class="material-icons">thumb_up</i> &nbsp {{  number_format($post->latestMetric->likes, 0, '.', '.') }}
					</div>

					<div class="comments flex">
						<i class="material-icons">thumb_down</i> &nbsp {{  number_format($post->latestMetric->dislikes, 0, '.', '.') }}
					</div>

					<div class="shares flex">
						<i class="material-icons">mode_comment</i> &nbsp {{  number_format($post->latestMetric->comments, 0, '.', '.') }} 
					</div>					
				</a>				
			</div>
			</div>
				@include('utils.posts.aditional-post-info', ['post'=>$post, 'social_network'=>'YouTube'])
		</div>