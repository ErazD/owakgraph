{{-- Variables requeridas: $post --}}

<div class="form-metrics">
	<form action="{{route('update_post_metrics')}}" id="metrics" method="POST">
		{{ csrf_field() }}
		<input type="hidden" name="social_network" value="{{$post->social_network()}}">
		<input type="hidden" name="post_id" value="{{$post->id}}">


		<div class="row" style="text-align: center;">
			<div class="col l6 m6 ">
					
					<input id="crear-metrica" type="radio" name="info_metrics" value="create" required onchange="setinfoMetrics('create')">		
					<label for="crear-metrica">Crear Métrica</label>
				</div>
				<div class="col l6 m6 ">
					<input id="actualizar-metrica" type="radio" name="info_metrics" value="update" required onchange="setinfoMetrics('update')">		
					<label for="actualizar-metrica">Actualizar Métricas</label>
				</div>
				<div class="col m12">
					<p id="info-form" style="text-align: center; color: #999;">	
					Selecciona una opción entre crear y actualizar métrica.				
					</p>
				</div>
		</div>

		<div class="row">
			<div id="since-metrics-container" class="col l6 m12 ">
				<label for="since-metrics">Fecha inicial</label>
				<input id="since-metrics" type="text" class="datepicker" name="since" placeholder="Desde">		
			</div>
			<div id="until-metrics-container" class="col l6 m12 ">
				<label for="until-metrics">Fecha Principal*</label>
				<input id="until-metrics" type="text" class="datepicker" name="until" placeholder="Hasta" required selected>		
			</div>
		</div>
		
		
		<div class="row">
			@foreach(Config::get('constants.metrics')[strtolower($post->social_network())] as $metric)
				<div class="col l4 m6 s12 ">
					<label for="{{$metric}}-metrics">{{$metric}}</label>
					<input id="{{$metric}}-metrics" type="number" name="{{$metric}}" placeholder="{{$metric}}">
				</div>
			@endforeach
		</div>

		<input type="submit" class="find btn btn-primary green m-0" value="Actualizar">
	</form>
</div>

<script>
	var info = document.getElementById("info-form");
	var metricsSince = document.getElementById("since-metrics-container");
	var metricsUntil = document.getElementById("until-metrics-container");
	var infoDescriptions = [
	'Se actualizarán los valores seleccionados para todas las métricas que se encuentren en el rango de fecha.',
	'Se creará una nueva métrica para la fecha seleccionada.'
	];
	function setinfoMetrics(type='create'){
		if(type == 'update'){
			metricsSince.classList.remove("hide");
			metricsUntil.classList.remove('l12');
			info.innerHTML = infoDescriptions[0];

		}else {
			info.innerHTML = infoDescriptions[1];
			metricsSince.value = "";
			metricsSince.classList.add("hide");
			metricsUntil.classList.add('l12');
		}
	}
</script>