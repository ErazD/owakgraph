@extends('layouts.app')

@section('scripts')
<script src="{{ asset('js/vue.min.js') }}"></script>
<script src="{{ asset('js/appComms.js') }}"></script>
<script src="{{ asset('js/date-selector.js') }}"></script>
<script>
	// variables para comms-chart.js
	// var type = "fanpage";
	// var pedido_id = @{{$fanpage->id}};
	var formComms=true;
	comms_register = "{{route('two-factor.store')}}";
</script>
@stop

@section('content')
<div class="page" id="fanpage">
	<div class="row grey lighten-3 mb-0">
		<div class="col s12 m12 l12 page-title">

			<div class="header-content left">
				<h2>Publicaciones Filtradas</h2>
			</div>

		</div>
	</div>
	<div class="row blue darken-3 mb-0">
		<div class="col s12 m12 l12 page-title">
			<p class="white-text">Se han encontrado <strong>{{$count}} publicaciones</strong>.</p>
		</div>
	</div>
</div>
<div class="container-m posts">
	<div class="row">
		@foreach($posts as $key => $post)
			@if($post->social_network() == 'Facebook')
					@include('posts.post-min', ['user' => $user, 'post'=>$post])
			@elseif($post->social_network() == 'YouTube')
					@include('youtube.videos.video-min', ['user' => $user, 'post'=>$post])
			@elseif($post->social_network() == 'Instagram')
					@include('instagram.posts.post-min', ['user' => $user, 'post'=>$post])
			@endif
		@endforeach 
	</div>
</div>
<div class="row">
	<div class="col s12 m12 l12">
		{{ $posts->links() }}
	</div>
</div>


<div class="modal modal-fixed-footer" id="addContest">
	<h2 class="title dark-blue-graph">Agregar Concurso</h2>
	<div class="modal-content">

		{!! Form::open(['route' => 'contest.store', 'onSubmit' =>'submitForm()']) !!}
		<div class="row">
			<div class="col s12 m10 l10 offset-m1 offset-l1">
				<?php 
					$brands_ids =[];
					$brns = App\GroupIndustryRole::getUserPermissionsElement($user, 'Brand')->get();
					foreach ($brns as $key => $brn) {
						$brands_ids[$brn->id] = $brn->name;
					}
				 ?>
				 	<label for="brand_id" class="col-md-4 control-label">Selecciona una marca</label>
					{!! Form::select('brand_id', $brands_ids, null, ['placeholder' => 'Selecciona una marca', 'required'=>'required']) !!}
				<label for="name" class="col-md-4 control-label">Nombre</label>
				{!! Form::text('name', 'Nombre del Concurso', array('class' => 'mb-5')) !!}
				<label for="text" class="col-md-4 control-label">Mecánica</label>
				{!! Form::textarea('mechanics', 'Mecánica del Concurso', array('class' => 'mb-5')) !!}
				<label for="text" class="col-md-4 control-label">Premio</label>
				{!! Form::text('prize', 'Premio del Concurso', array('class' => 'mb-5')) !!}
				<label for="from-date" class="col-md-4 control-label">Desde</label>
				{!! Form::date('from-date', \Carbon\Carbon::now()); !!}
				<label for="to-date" class="col-md-4 control-label">Hasta</label>
				{!! Form::date('to-date', \Carbon\Carbon::now()); !!}


				@if ($errors->has('url'))
				@foreach($errors->get('url') as $error)
				<p style="color:#b71c1c;">{{ $error }}</p>
				@endforeach
				@endif

				{!!Form::submit('Agregar', ['class' => 'find btn btn-primary green m-0', 'style' => 'width:100%'] ) !!}
				<div id="scrap">
					<div class="progress mt-5">
						<div class="indeterminate"></div>
					</div>
				</div>
			</div>
		</div>
		<script type=text/javascript>
			function submitForm()
			{	
				$('.btn.green.find').hide();
				$('.progress').show();
			}

		</script>
		{!! Form::close() !!}

	<br>
	<br>
	</div>
	<div class="modal-footer">
		<a class="modal-action modal-close waves-effect waves-blue btn-flat">Cerrar</a>
	</div>
</div>	

@endsection