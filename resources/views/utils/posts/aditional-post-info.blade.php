<?php
	// Requires $socialNetwork
	$user = ($user)? $user: App\User::where('id',11)->first();
	$hasUser = isset($user) && $post->hasUser($user);
	$isJudge = isset($user) && $user->twoFactorProfile && $user->twoFactorProfile->rank > 0;
	$isIncomplete = $hasUser && $post->twofactors->where('profile_id',$user->twoFactorProfile->id)->first()->status < 2;
 
?>
<div class="col s1 m1 l1  card-coms flex 
		@if($isIncomplete)
			two-factor-0
		@else
			two-factor-{{$post->twoFactorStatus}}
		@endif">
		<div class="coms-info-icon" onClick="showCommsFanpage('{{$post->id}}', 1)">
			{{-- TODO Realizar el menú de bookmark --}}		
			@include('manage_sections.adset.adset-post-menu', ['post'=>$post, 'user'=>$user])		
			<h4 class="white-text"><i class="fa fa-file-text-o" aria-hidden="true"></i> <span class="text-xs">Comms</span></h4>
			@if($hasUser)
				<h5 class="white-text"><i class="fa fa-user-circle" aria-hidden="true"></i></h5>
				@if($isIncomplete)
					<h5 class="white-text" align="center"><i class="fa fa-edit" aria-hidden="true"></i></h5>
				@endif
			@endif
		</div>
</div>

<div class="col s6 m6 l6 comms-summary">
	<i class="material-icons close-btn">close</i>	
	<div id="content{{$post->id}}" class="comms-summary-show"></div>
	<div id="section{{$post->id}}_1" class="hide">
		@if($isIncomplete && !$isJudge)
			@include('comms.two-factor.register', ['post'=>$post, 'user'=>$user,'isJudge'=>$isJudge, 'hasUser'=>$hasUser, 'social_network'=>$social_network])
		@else
			@include('comms.comms-post', ['post'=>$post, 'user'=>$user,'isJudge'=>$isJudge, 'hasUser'=>$hasUser,  'social_network'=>$social_network, 'isIncomplete'=>$isIncomplete])
		@endif
	</div>
</div>