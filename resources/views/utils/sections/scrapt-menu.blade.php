{{-- Variables requeridas: $element, $modelName, $isPost  --}}
<style>
.scrap-menu{
	display: flex;
	justify-content: space-evenly;
	align-items: center;
	padding: 8px;
}
.scrap-menu h6{
	font-weight: bolder;
	color: #616161;
}
.scrap-info{

}
.scrap-menu-item{
	
}
@media screen and (max-width: 680px){
	.scrap-menu{
		flex-direction: column;
		align-items: stretch;
	}
	.scrap-menu-item{
		text-align: center;
		margin: 0 16px;
	}
	.scrap-info{
		border-bottom-style: solid;
		border-top-style: solid;
		border-color: #61616161;
		border-width: 1px; 
	}
}
</style>
<div class="scrap-menu">
	<?php $scrap_types = Config::get('constants.scrap_types'); ?>
	<div class="tooltipped scrap-menu-item" data-position="bottom" data-tooltip="Frecuencia mínima para pedir métricas de los posts."><h6>Frecuencia de scrap <sup>?</sup></h6></div>
	
	<div class="scrap-menu-item scrap-info">	
		@if($isPost)
			<p>
				<b> Frecuencia Automática:</b>
				@if($element->scrap_type >= -1)
				{{$scrap_types[$element->scrap_type]['name']}}.
				@else
				Bloqueado por no poder acceder al post.
				@endif
			</p>
			<p>
				<b class="tooltipped" data-position="bottom" data-tooltip="límita la frecuencia automática, solo es necesaria en algunos posts"> Frecuencia Personalizada:<sup>?</sup></b>
				@if($element->selected_scrap_type !== null)
					{{$scrap_types[$element->scrap_type]['name']}}. 
				@else
					No definida. (Frecuencia Base: {{$scrap_types[$element->selectedScrap()]['name']}}) 
				@endif
			</p>
		@else
			@if($element->scrap_type !== null)
				<p>
					<b> {{$scrap_types[$element->scrap_type]['name']}}:</b>
					{{$scrap_types[$element->scrap_type]['description']}}.
				</p>
			@else
				No definida. (Frecuencia Base: {{$scrap_types[$element->selectedScrap()]['name']}})
			@endif
		@endif
	</div>
	<div class="scrap-menu-item">
		<select name="" id="" onchange="if(this.value != '{{$element->scrap_type}}') window.location.href = '{{url('brand/scrap_type/'.$modelName.'/'.$element->id)}}/'+this.value;">
			<option disabled selected value="">Cambiar frecuencia</option>
			@foreach($scrap_types as $v=>$scrp)
			<option value="{{$v}}">{{$scrp['name']}}</option>
			@endforeach
		</select>
	</div>
</div>