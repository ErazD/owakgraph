@extends('layouts.app')

@section('scripts')
<script src="{{asset('js/jscolor.js')}}"></script>
<script src="{{ asset('js/vue.min.js') }}"></script>
<script src="{{ asset('js/moment.js') }}"></script>
<script>
	setDataSelect('info-from', 'from');
	setDataSelect('all', 'to');
	setDataSelect('all', 'table');
	var modifiedColors = $('#modified-colors');

	function setColor(element, id='unique-color', concat=false) {
		var color = element.value;
		color = (color[0] == '#')? color:'#' + color;
		// $('#'+id).css('background-color',color);
		$('#'+id).val(color);
		if(concat)
			concatModifiedColor(element.name);
		
	}
	function selectColor(element, id="color-text", concat=false){
		var color = element.value;
		var elementInput = $('#'+id);
		elementInput.val(color);
		if(concat)
			concatModifiedColor(elementInput.name);

	}
	function concatModifiedColor(elementName){
		if(!modifiedColors)
			modifiedColors = $('#modified-colors');
		var val = modifiedColors.val();
		if(val.indexOf(elementName)<0){
			modifiedColors.val(elementName + ' ' + val);
		}
	}
	function setDataSelect(dataID='all', selectID='to'){
		var data = document.getElementById(dataID).innerHTML;
		document.getElementById(selectID).innerHTML = data;
		if(dataID=='all')
			document.getElementById('element').value = 'none';
		else
			document.getElementById('element').value = '';
	}
</script>
@stop
<style>
.color{
	display: inline-block;
	/*width: 10px;	*/
	padding: 0px !important;
	border:none !important;
	flex: 0 1 30px;
	height: 70px;
}
ul#color-section{
	display: flex;
	justify-content: start;
	align-items: center;
	flex-wrap: wrap;
}
ul#color-section{
	width: 100%;
	padding: 20px;
}
.conte-color{
	display: flex;
	align-items: center;
}
div.conte-color .color{
	flex: 1 1;
	margin: 10px;
	height: 190px;
	text-align:center;
}
.mini{
	font-size:.5em !important;
	text-align:center;
}
div.conte-color .info-color{
	flex: 10 1;
}
ul#color-section li.conte-color{

	margin: 10px 20px;
}
ul#color-section li.conte-color .info{
	display: flex;	
	flex-direction: column;
	justify-content: center;
	align-content: center;
}

ul#color-section li.conte-color .info *{
	margin: 2px;
}
ul#color-section li.conte-color .info h6{
	font-size: 1em;
	font-weight: 700;
	font-family: sans-serif;
}
</style>
@section('content')
<div class="row grey lighten-3 mb-0">
	<div class="col s12 m12 l12 page-title">
		<h2 class="breadcrumbs-title left">
		Colores</h2>
	</div>
</div>
<div class="row blue darken-3">
	<div class="col s12 m12 l12 page-title">
		<p class="white-text" style="width: 80%;"> 
			En esta sección se pueden actualizar los colores.
	 
		</p>
	</div>
</div>
<div class="container-m">

	<form method="GET">
		<datalist id="info-from">
			<option  selected disabled required>Selecciona el nivel de búsqueda</option>
			<option value="all">Todos</option>
			@foreach($allOptions as $kOpt => $opt)
			<option value="{{$kOpt}}">{{$opt['name']}}</option>
			@endforeach

		</datalist>
		<datalist id="all">
			<option  selected disabled required>Selecciona que elemento buscar </option>
			@foreach($allOptions as $kOpt => $opt)
		
			<option value="{{$kOpt}}">{{$opt['name']}}</option>
			@endforeach
		</datalist>
		<datalist id="listElements"></datalist>

		@foreach($allOptions as $kOpt => $opt)
		<datalist id="{{$kOpt}}">
			@foreach($opt['list'] as $k => $o)
			<option value="{{$o->id}}">{{$o->name}}</option>
			@endforeach
		</datalist>
		@endforeach


		<label for="from">Buscar en</label>
		<select id="from" name="from" class="browser-default" onchange="setDataSelect(this.value, 'listElements')"> </select> {{-- onchange="setDataSelect(this.value)" --}}
		<label for="element">Id del elemento a buscar</label>
		<input id="element" name="element" list="listElements" autocomplete="off">
		<label for="to">Buscar elemento</label>
		<select id="to" name="to" class="browser-default"> </select>
		<input type="hidden" name="type" value="table">
		<input type="hidden" name="multiple" value="true">
		<input type="submit" value="buscar">
	</form>
	<form action="{{route('color.store')}}" method="POST">
		{{ csrf_field() }}
		<input type="hidden" name="multiple" value="{{$multiple}}">
		<input type="hidden" name="modified-colors" id="modified-colors" value="">
		@if($multiple)
		<ul id="color-section">
			@foreach($elements as $key => $ele)
			<li class="conte-color">
							
				<?php $actColor = App\Http\Controllers\ColorController::getColor($table, $ele->id); 
				$actColor = ($actColor == 'undefined')? '' : $actColor; ?>
				<input  onchange="selectColor(this, 'color-{{$type}}-{{$table}}-{{$ele->id}}', true)" id="unique-color-{{$ele->id}}" class="jscolor color mini" value="{{$actColor}}">

				<div class="info">
					<h6>{{$ele->name}}</h6>
					<input id="color-{{$type}}-{{$table}}-{{$ele->id}}" type="text" name="color-{{$type}}-{{$table}}-{{$ele->id}}" placeholder="color" onchange="setColor(this, 'unique-color-{{$ele->id}}', true)" value="{{$actColor}}">
				</div>
			</li>	
			@endforeach
		</ul>
		@else
		<datalist id="oneElement"></datalist>

		<h4>Asigna color a un elemento</h4>
		<div class="conte-color">
			{{-- <div id="unique-color" class="color"> </div> --}}
			<input id="unique-color" class="jscolor color"  onchange="selectColor(this)">
			<div class="info-color">
				<label for="table">Tipo de elemento</label>
				<select id="table" name="table" class="browser-default" onchange="setDataSelect(this.value, 'oneElement')"> </select>
				{{-- <input type="text" name="table" placeholder="Nombre de la tabla"> --}}
				<label for="id">ID del elemento a asignar color.</label>
				<input type="text" name="id" placeholder="id del elemento a colorear" list="oneElement" autocomplete="off">
				<input id="color-text" type="text" name="color" placeholder="color" onchange="setColor(this)">			
			</div>			
		</div>
		
		
		@endif
		<input type="submit" value="guardar">
	</form>
</div>
@stop