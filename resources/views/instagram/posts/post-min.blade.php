{{-- Necesita: $user, $post  | Opcional: $userAdsets --}}
		<div class="col s10 m6 l6 flex post-card">
			<div class="card-summary col s11 m11 l11">
			<div class="card row">
				<!-- Dropdown Structure -->
				<ul id='dropdown{{$key}}' class='dropdown-content'>
					<li><a href="{{ url('instagram-post/'.$post->id) }}"><i class="material-icons left">insert_chart</i> Métricas ({{ count($post->metrics) }})</a></li>
					<li><a href="{{ $post->permalink_url }}" target="_blank"><i class="material-icons left">link</i> Visitar</a></li>
				</ul>
				<a href="{{ url('instagram-post/'.$post->id) }}">
					<div class="attachment" style="background-image: url({{$post->attachment}})">

						@if($post->type == 'photo')
						<div class="waves-effect waves-light btn green circle-icon"><i class="material-icons center">image</i></div>
						@endif
						@if($post->type == 'video')
						<div class="waves-effect waves-light btn red circle-icon"><i class="material-icons center">ondemand_video</i></div>
						@endif
						@if($post->type == 'status')
						<div class="waves-effect waves-light btn green circle-icon"><i class="material-icons center">format_quote</i></div>
						@endif
						@if($post->type == 'link')
						<div class="waves-effect waves-light btn blue circle-icon"><i class="material-icons center">insert_link</i></div>
						@endif
					</div>
				</a>
				<div class="content">
					<div class="date valign-wrapper">
						<a href="{{ url('instagram-post/'.$post->id) }}">{{ $post->published }}</a>
						<a class='dropdown-button' href='#' data-activates='dropdown{{$key}}'><i class="material-icons">more_horiz</i></a>
					</div>

					@if(!is_null($post->content))
					<p class="insta-content no-margin">{{ $post->content}}</p>
					@else
					<p class="no-margin insta-content">Esta publicación no tiene una descripción.</p>
					@endif
				</div>

			<a class="cta flex" href="{{ url('instagram-post/'.$post->id) }}">
				@if($post->latestMetric)
					@if($post->latestMetric->views != 0)
					<div class="shares flex">
						<i class="material-icons">visibility</i> &nbsp {{ number_format($post->latestMetric->views, 0, '.', '.')}} 
					</div>
					@endif
					@if($post->latestMetric->likes != 0)
					<div class="reactions flex">
						<i class="material-icons">favorite</i> &nbsp {{  number_format($post->latestMetric->likes, 0, '.', '.') }}
					</div>
					@endif
					<div class="shares flex">
						<i class="material-icons">mode_comment</i> &nbsp {{  number_format($post->latestMetric->comments, 0, '.', '.') }} 
					</div>
				@else
					<p>No hay métricas registradas</p>
				@endif
				</a> 
			</div>
			</div>
				@include('utils.posts.aditional-post-info', ['post'=>$post, 'social_network'=>'Instagram'])
		</div>