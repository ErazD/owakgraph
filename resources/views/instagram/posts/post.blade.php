@extends('layouts.app')

@section('scripts')
<script src="{{ asset('js/vue.min.js') }}"></script>
<script src="{{ asset('js/moment.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.6.0/Chart.min.js"></script>
<script src="https://www.amcharts.com/lib/3/amcharts.js"></script>
<script src="https://www.amcharts.com/lib/3/serial.js"></script>
<script src="{{ asset('js/instagram-chart.js') }}"></script>
@stop
@section('content')

<div class="row grey lighten-3 mb-0">
	<div class="col s12 m12 l12 page-title">
		<h2 class="breadcrumbs-title left">
			Estadísticas</h2>
		</div>
	</div>

	<div class="row blue darken-3">
		<div class="col s12 m12 l12 page-title">

			<p class="white-text" style="width: 80%;"> 
			@if(!is_null($post->content))
			{{ $post->content }}
			@else
			Esta publicación no tiene una descripción
			@endif
			</p>
			<div class="btn-group">
			<a href="{{ $post->permalink_url }}" target="_blank" class="btn btn-primary green tooltipped mr-5" style="margin-left: 20px;" data-position="bottom" data-delay="50" data-tooltip="Abrir en Instagram"><i class="material-icons">insert_link</i></a>

			{{-- <a href="{{ url('instagram-post/'.$post->id.'/export') }}" target="_blank" class="btn btn-primary green tooltipped" data-position="bottom" data-delay="50" data-tooltip="Descargar"><i class="material-icons">file_download</i></a> --}}
			</div>

		</div>

	</div>

	<div class="container-m" id="stats">
		
		<div class="row">
			<div class="col s12 m12 l12">
				<div class="title green">
					<h2>Estadísticas de hoy</h2>
					{{-- <ul class="tabs">
						<li class="tab" @click="metricsByHour('reactions')"><a href="#reactions" class="active">Reacciones</a></li>
						<li class="tab" @click="metricsByHour('comments')"><a href="#comments">Comentarios</a></li>
						<li class="tab" @click="metricsByHour('shares')"><a href="#shares">Shares</a></li>
					</ul> --}}

				</div>
				<div class="card white" id="hoursChartdiv">
					{{-- <canvas id="hoursChart"></canvas> --}}
					<div id="hoursChart" class="postChart">
						
					</div>
				</div>
			</div>

		</div>


		<div class="row">
			<div class="col s12 m12 l12">
				<div class="title blue">
					<h2>Estadísticas por día</h2>
					{{-- <ul class="tabs">
						<li class="tab" @click="metricsByDay('reactions')"><a href="#reactions" class="active">Reacciones</a></li>
						<li class="tab" @click="metricsByDay('comments')"><a href="#comments">Comentarios</a></li>
						<li class="tab" @click="metricsByDay('shares')"><a href="#shares">Shares</a></li>
					</ul> --}}

				</div>
				<div class="card white" id="daysChartdiv">
					{{-- <canvas id="daysChart"></canvas> --}}
					<div class="row formDateContainer">
						<div class="col s12 m6 l3">
							<input v-model="since" type="text" placeholder="Y-m-d">	
						</div>
						<div class="col s12 m6 l3">
							<input v-model="until" type="text" placeholder="Y-m-d">
						</div>
						<div class="col s12 m6 l3">
							<button  @click="zoomChart(chartMD)">Actualizar Gráfica</button>
						</div>
						<div class="col s12 m6 l3">
							<select v-model="interval" v-on:change="setDate(interval)" class="browser-default"  name="interval"   id="">
								<optgroup label="" >
									<option value="all">Todo</option>
								</optgroup>
								<optgroup label="" >
									<option value="hoy">hoy</option>
									<option value="day">El mismo Día</option>
								</optgroup>
								<optgroup label="">
									<option value="6">Semana anterior</option>
									<option value="28">28 días anteriores</option>
									<option value="30">30 días anteriores</option>
								</optgroup>
								<optgroup label="">
									<option value="90">trimestre anterior (90 días)</option>
									<option value="182">semestre anterior (182 días)</option>
									<option value="365">año anterior</option>
								</optgroup>
							</select>
						</div>					
					</div>
					<div id="daysChart" class="postChart">
						
					</div>
				</div>
			</div>

		</div>




	</div>
	@stop