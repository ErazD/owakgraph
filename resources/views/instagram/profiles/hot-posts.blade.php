@extends('layouts.app')

@section('content')

<div class="page" id="fanpage">
	<div class="row grey lighten-3 mb-0">
		<div class="col s12 m12 l12 page-title">

			<div class="header-content left">
				<h2>{{ $profile->brand->name }} {{ $profile->name }}</h2>
			</div>

		</div>
	</div>

	<div class="row orange darken-4 mb-0">
	<div class="col s12 m12 l12 page-title">

		<p class="white-text">En esta sección encontrarás las <strong>publicaciones</strong> más activas del día.</p>
	</div>

</div>

</div>


<div class="container-m posts">
	<div class="row">
		
		@if($hot_posts->isEmpty())
		No hay interacciones en ninguna de las publicaciones de este Perfil.<br/><br/>

		<a href="{{ url('/profile/'.$profile->id)}}">Regresar</a>
		@else
		@foreach($hot_posts as $key => $metric)

		<div class="col s12 m4 l4">
			<div class="card">

				<!-- Dropdown Structure -->
				<ul id='dropdown{{$key}}' class='dropdown-content'>
					<li><a href="{{ url('instagram-prost/'.$metric->id) }}"><i class="material-icons left">insert_chart</i> Métricas ({{ $metric->count }})</a></li>
					<li><a href="{{ $metric->permalink_url }}" target="_blank"><i class="material-icons left">link</i> Visitar</a></li>
				</ul>
				<a href="{{ url('instagram-post/'.$metric->id) }}">
					<div class="attachment" style="background-image: url({{$metric->attachment}})">

						@if($metric->type == 'photo')
						<div class="waves-effect waves-light btn green circle-icon"><i class="material-icons center">image</i></div>
						@endif
						@if($metric->type == 'video')
						<div class="waves-effect waves-light btn red circle-icon"><i class="material-icons center">ondemand_video</i></div>
						@endif
						@if($metric->type == 'status')
						<div class="waves-effect waves-light btn green circle-icon"><i class="material-icons center">format_quote</i></div>
						@endif
						@if($metric->type == 'link')
						<div class="waves-effect waves-light btn blue circle-icon"><i class="material-icons center">insert_link</i></div>
						@endif


					</div>
				</a>


				<div class="content youtube-change">

					<div class="date valign-wrapper">
						<a href="{{ url('instagram-post/'.$metric->id) }}">{{ $metric->published }}</a>
						<div>
						
						<span class="new badge">{{ $metric->count }}</span>
						<a class='dropdown-button' href='#' data-activates='dropdown{{$key}}'><i class="material-icons">more_horiz</i></a>
						</div>
					</div>

					@if(!is_null($metric->content))
					<p class="no-margin">{{ $metric->content}}</p>
					@else
					<p class="no-margin">Esta publicación no tiene una descripción.</p>
					@endif
				</div>
                
				<a class="cta flex" href="{{ url('instagram-post/'.$metric->id) }}">
				  @if($metric->views != 0)
					<div class="shares flex">
						<i class="material-icons">visibility</i> &nbsp {{$metric->views}} 
					</div>
                  @endif
                  @if($metric->likes != 0)
					<div class="reactions flex">
						<i class="material-icons">favorite</i> &nbsp {{ $metric->likes }}
					</div>
                  @endif
					<div class="shares flex">
						<i class="material-icons">mode_comment</i> &nbsp {{ $metric->comments }} 
					</div>
				</a>
				
			</div>
		</div>
		@endforeach 
		@endif



	</div>


</div>

<div class="row">
		<div class="col s12 m12 l12">
			{{ $hot_posts->links() }}
		</div>
	</div>

</div>


@endsection