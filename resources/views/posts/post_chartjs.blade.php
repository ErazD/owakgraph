@extends('layouts.app')

@section('scripts')
<script src="{{ asset('js/vue.min.js') }}"></script>
<script src="{{ asset('js/moment.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.6.0/Chart.min.js"></script>
<script src="{{ asset('js/app.js') }}"></script>
{{-- <script src="{{ asset('js/main.js') }}"></script> --}}
@stop
@section('content')

<div class="row grey lighten-3 mb-0">
	<div class="col s12 m12 l12 page-title">
		<h2 class="breadcrumbs-title left">
			Estadísticas</h2>
		</div>
	</div>

	<div class="row blue darken-3">
		<div class="col s12 m12 l12 page-title">

			<p class="white-text" style="width: 80%;"> 
			@if(!is_null($post->content))
			{{ $post->content }}
			@else
			Esta publicación no tiene una descripción
			@endif
			</p>
			<div class="btn-group">
			<a href="{{ $post->permalink_url }}" target="_blank" class="btn btn-primary green tooltipped mr-5" style="margin-left: 20px;" data-position="bottom" data-delay="50" data-tooltip="Abrir en Facebook"><i class="material-icons">insert_link</i></a>

			<a href="{{ url('posts/'.$post->id.'/export') }}" target="_blank" class="btn btn-primary green tooltipped" data-position="bottom" data-delay="50" data-tooltip="Descargar"><i class="material-icons">file_download</i></a>
			</div>

		</div>

	</div>

	<div class="container-m" id="stats">
		
		<div class="row">
			<div class="col s12 m12 l12">
				<div class="title green">
					<h2>Estadísticas de hoy</h2>
					<ul class="tabs">
						<li class="tab" @click="metricsByHour('reactions')"><a href="#reactions" class="active">Reacciones</a></li>
						<li class="tab" @click="metricsByHour('comments')"><a href="#comments">Comentarios</a></li>
						<li class="tab" @click="metricsByHour('shares')"><a href="#shares">Shares</a></li>
					</ul>

				</div>
				<div class="card white" id="hoursChartdiv">
					<canvas id="hoursChart"></canvas>
				</div>
			</div>

		</div>


		<div class="row">
			<div class="col s12 m12 l12">
				<div class="title blue">
					<h2>Estadísticas por día</h2>
					<ul class="tabs">
						<li class="tab" @click="metricsByDay('reactions')"><a href="#reactions" class="active">Reacciones</a></li>
						<li class="tab" @click="metricsByDay('comments')"><a href="#comments">Comentarios</a></li>
						<li class="tab" @click="metricsByDay('shares')"><a href="#shares">Shares</a></li>
					</ul>

				</div>
				<div class="card white" id="daysChartdiv">
					<canvas id="daysChart"></canvas>
				</div>
			</div>

		</div>


	</div>


	@stop