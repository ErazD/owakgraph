{{-- Necesita: $user, $post  | Opcional: $userAdsets --}}

<?php 
$user = App\User::where('id',11)->first();
$hasUser = isset($user) && $post->hasUser($user);
$isJudge = isset($user) && $user->twoFactorProfile && $user->twoFactorProfile->rank > 0;
$isIncomplete = $hasUser && $post->twofactors->where('profile_id',$user->twoFactorProfile->id)->first()->status < 2;
$views = number_format($post->latestMetric->views, 0, '.', '.');
$reactions = number_format($post->latestMetric->reactions, 0, '.', '.');
$comments = number_format($post->latestMetric->comments, 0, '.', '.');
$shares =number_format($post->latestMetric->shares, 0, '.', '.');
?>

<style>
</style>
 
<div class="post-card col s6 m6 l6 flex  @foreach($post->fanpage->brand->canastas as $canasta)
 canasta-{{$canasta->id}} categoria-{{$canasta->categoria->id}} vertical-{{$canasta->categoria->vertical->id}}
@endforeach  marca-{{$post->fanpage->brand->id}} @foreach($post->adsets as $adset) adset-{{$adset->id}} @endforeach fanpage-{{$post->fanpage->id}}">

	<div class="card-summary col s11 m11 l11">
		<div class="card row">
			<!-- Dropdown Structure -->
			<ul id='dropdown{{$key}}' class='dropdown-content'>
				<li><a href="{{ url('posts/'.$post->id) }}"><i class="material-icons left">insert_chart</i> Métricas ({{ count($post->metrics) }})</a></li>
				<li><a href="{{ $post->permalink_url }}" target="_blank"><i class="material-icons left">link</i> Visitar</a></li>
				<li><a class='confirm-delete' href="{{url('posts/delete/'. $post->id)}}"><i class="fa fa-trash-o" aria-hidden="true"></i> Eliminar</a></li>
			</ul>   
			<div class="col s12 m12 l12 no-padding">
				<a href="{{ url('posts/'.$post->id) }}">
					<div class="attachment" style="background-image: url({{$post->attachment}})">
                       
						@if($post->type == 'photo')
						<div class="waves-effect waves-light btn green circle-icon"><i class="material-icons center">image</i></div>
						@endif
						@if($post->type == 'video')
						<div class="waves-effect waves-light btn red circle-icon"><i class="material-icons center">ondemand_video</i></div>
						@endif
						@if($post->type == 'status')
						<div class="waves-effect waves-light btn green circle-icon"><i class="material-icons center">format_quote</i></div>
						@endif
						@if($post->type == 'link')
						<div class="waves-effect waves-light btn blue circle-icon"><i class="material-icons center">insert_link</i></div>
						@endif
					</div>
				</a>
			</div>
			<div class="col s12 m12 l12 no-padding">
				<div class="content">
					<div class="date valign-wrapper">
						<a href="{{ url('posts/'.$post->id) }}">{{ $post->published }}</a>
						<a class='dropdown-button' href='#' data-activates='dropdown{{$key}}'><i class="material-icons">more_horiz</i></a>
					</div>
					<div class="post-text">
						@if(!is_null($post->content))
						<p class="no-margin">{{ $post->content}}</p>
						@else
						<p class="no-margin">Esta publicación no tiene una descripción.</p>
						@endif
					</div>
				</div>
				<a class="cta flex" href="{{ url('posts/'.$post->id) }}">
					@if($post->type == 'video' && $post->latestMetric->views != 0)
					<div class="shares flex">
						<i class="material-icons">visibility</i>   {{$views}} 
					</div>
					@elseif($post->type == 'video' && $post->latestMetric->views == 0)
					<div class="shares flex">
						<i class="material-icons">visibility</i> &nbsp {{ number_format( (($post->maxMetric()===0)?0:$post->maxMetric()->views), 0, '.', '.') }} 
					</div>
					@endif
					<div class="reactions flex">
						<img src="{{ asset('images/reactions.png') }}" alt="Reacciones"> {{ $reactions }}
					</div>

					<div class="comments flex">{{ $comments }} comentarios</div>

					<div class="shares flex">{{ $shares }} compartidos</div>
				</a>
			</div>
		</div>
	</div>
	@include('utils.posts.aditional-post-info', ['post'=>$post, 'social_network'=>'Facebook'])
</div>
