@extends('layouts.app')

@section('scripts')
<script>
	var type_fan = 'fanpage';	
	var modifiedColors = $('#modified-colors');

	function setColor(element, id='unique-color', concat=false) {
		var color = element.value;
		color = (color[0] == '#')? color:'#' + color;
		// $('#'+id).css('background-color',color);
		$('#'+id).val(color);
		if(concat)
			concatModifiedColor(element.name);
	}
	function selectColor(element, id="color-text", concat=false){
		var color = element.value;
		var elementInput = $('#'+id);
		elementInput.val(color);
		if(concat)
			concatModifiedColor(elementInput.name);

	}
	function concatModifiedColor(elementName){
		if(!modifiedColors)
			modifiedColors = $('#modified-colors');
		var val = modifiedColors.val();
		if(val.indexOf(elementName)<0){
			modifiedColors.val(elementName + ' ' + val);
			coonsole.log('working');
		}
	}

</script>
<script src="{{asset('js/jscolor.js')}}"></script>
<script src="{{ asset('js/vue.min.js') }}"></script>
<script src="{{ asset('js/moment.js') }}"></script>
<script>
	$(document).ready(function(){
		$('.collapsible').collapsible();
	});
	const productFilter = new Vue({
		el:"#product-list-container",
		delimiters:["<%", "%>"],
		data:{
			productos:[],
			segmentos:[],
		},
		methods:{
			filterProducts: function(event){
				var input, filter, container, list;
				input = document.getElementById("filter-products");
    	    filter = input.value.toUpperCase(); // valor a filtrar
    	    container = document.getElementById('product-list-container');
    	    list = container.getElementsByClassName("product-name");

    	    for (var i = 0; i < list.length; i++) {
    	    	var elem = list[i];	 
    	 	var val = elem.value.toUpperCase(); // Valor de la opción
    	 	// console.log(val);
    	 	// console.log(filter);
    	 	if(val.indexOf(filter) > -1 && event.key != 'Enter'){
    	 		elem.parentElement.parentElement.style.display = "block";
    	 	}else{
    	 		elem.parentElement.parentElement.style.display = "none";
    	 	}
    	 }
    	},
    	confirmDelete(productName, productId){
    		let result = confirm("Esta seguro que desea eliminar el producto " + productName + '?');
            if(result){
            	// https://owak.co/graph/public/index.php/product/edit
            	// https://owak.co/graph/public/index.php/product/delete/453
              let deleteProduct = 'http://graph.test:8888/product/delete/' + productId;
              var xhttp = new XMLHttpRequest();
              xhttp.open("GET", deleteProduct, true);
              xhttp.send();
              location.reload();
            }else{
             
            }
    	}
    },
    created:function(){
    	this.productos = <?php echo $products?> ;
    	this.segmentos = <?php echo json_encode($segments)?> ;
    	// console.log(this.productos);
    	// console.log(this.segmentos);
    }
});
</script>  
@endsection

@section('content')
<?php $scrap_type = Config::get('constants.scrap_types'); ?>
<style>
	.color{
		display: inline-block;
		/*width: 10px;	*/
		padding: 0px !important;
		border:none !important;
		flex: 0 1 30px;
		height: 70px;
	}
	.card ul{
		margin-top: 0px;
	}
	#fan-state h5{
		margin-top:3%;
	}
	.fans-state{
		margin-top:10px;
		padding-top:15px !important;
	}
	.fans-state .amcharts-chart-div{
		padding:2% !important;
	}
	.fans-state .fan-state-graph{
		width:100%;
		height:400px;
	}
	.amcharts-export-menu .export-main>a, .amcharts-export-menu .export-drawing>a, .amcharts-export-menu .export-delayed-capturing>a {
		width: 25px !important;
		height: 25px !important;
	}
	.no-show{
		display:none;
	}
	#filter-products{
		padding: 10px 10px;
		background-color: #fff;
		width: 95%;
		margin: 0 auto;
		display: block;
	}
	@media all and (min-width: 1480px){
		.fans-state{
			width:50% !important;
			margin-top:10px;
		}
	}
	.assign-color{
		padding: 10px 0 0px 0 !important; 
	}
	.assign-color h4{
		text-align: center;
		font-size: 1.3em;
		margin: 0 0 20px 0;
	}
	.assign-color form{
		padding: 20px 20px 0px 20px;
	} 
	.assign-color .conte-color{
		margin-bottom:20px;
		display: inline-block;
		width: 59%;
	}
	.assign-color #unique-color{
		width: 98%;
		display: inline-block;
		text-align: center;
		border-radius: 0 !important;
	}
	.assign-color .info-color{
		display:inline-block;
		vertical-align:text-bottom;
	}
	.assign-color .info-color input{
		display:inline-block;
		margin-bottom:3px;
	}

	.assign-scrap{
		padding: 10px 20px !important; 
	}
	.assign-scrap h4{
		font-size: 1.3em;
		font-weight: bolder;
	}

	.fanpage-scrap.selected .selec-scrap{
		height: 50px;
		width: 100%;
		overflow: initial;
	}
	.selec-scrap{
		height: 0px;
		width: 0px;
		transition: height 0.2s, width 0.2s;
		overflow: hidden;
	}

</style>

<div class="page">
	<div class="row grey lighten-3 mb-0">
		<div class="col s12 m12 l12 page-title">

			<div class="header-content left">
				<h2>{{ $brand->name }}</h2>
				<h3>{{ $brand->slug }}</h3>
			</div>

			<div class="right">
				<a class="add-brand right modal-trigger" href="#delete">
					<i class="material-icons header">delete</i>
				</a>
			</div>
		</div>
	</div>

	<div class="row blue darken-3">
		<div class="col s12 m12 l12 page-title">

			<p class="white-text">Hemos encontrado <strong>{{$brand->fanpages->count()}} fanpages</strong> y <strong>{{$brand->channels->count()}} canales de YouTube</strong> de {{ $brand->name }}.</p>
		</div>
	</div>
	{{-- Modal eliminar Marca --}}
	<div id="delete" class="modal modal-fixed-footer brands">

		<h2 class="title dark-blue-graph">Eliminar marca</h2>
		<div class="modal-content">
			<p class="center">Si eliminas esta marca se perderá toda la información de las fanpages asociadas con <br/> sus respectivas publicaciones.</p>

			{!! Form::open(['route' => ['delete_brand',  $brand->id]]) !!}

			<div class="row">
				<div class="col s12 m8 l8 offset-m2 offset-l2">
					{!!Form::submit('Eliminar', ['class' => 'btn btn-primary red darken-4', 'style' => 'width:100%'] ) !!}
				</div>
			</div>

			{!! Form::close() !!}


		</div>
		<div class="modal-footer">
			<a href="#" class="modal-action modal-close waves-effect waves-green btn-flat">Cerrar</a>
		</div>
	</div>
	{{-- Modal agregar concurso --}}
	<div class="modal modal-fixed-footer" id="addContest">
		<h2 class="title dark-blue-graph">Agregar Concurso</h2>
		<div class="modal-content">

			{!! Form::open(['route' => 'contest.store', 'onSubmit' =>'submitForm()']) !!}
			<div class="row">
				<div class="col s12 m10 l10 offset-m1 offset-l1">
					<label for="name" class="col-md-4 control-label">Nombre</label>
					{!! Form::text('name', 'Nombre del Concurso', array('class' => 'mb-5')) !!}
					<label for="text" class="col-md-4 control-label">Mecánica</label>
					{!! Form::textarea('mechanics', 'Mecánica del Concurso', array('class' => 'mb-5')) !!}
					<label for="text" class="col-md-4 control-label">Premio</label>
					{!! Form::text('prize', 'Premio del Concurso', array('class' => 'mb-5')) !!}
					<label for="date" class="col-md-4 control-label">Desde</label>
					{!! Form::date('from-date', \Carbon\Carbon::now()); !!}
					<label for="date" class="col-md-4 control-label">Hasta</label>
					{!! Form::date('to-date', \Carbon\Carbon::now()); !!}
					{!! Form::hidden('brand_id', $brand->id) !!}
					@if ($errors->has('url'))
					@foreach($errors->get('url') as $error)
					<p style="color:#b71c1c;">{{ $error }}</p>
					@endforeach
					@endif

					{!!Form::submit('Agregar', ['class' => 'find btn btn-primary green m-0', 'style' => 'width:100%'] ) !!}
					<div id="scrap">
						<div class="progress mt-5">
							<div class="indeterminate"></div>
						</div>
					</div>
				</div>
			</div>
			<script type=text/javascript>
				function submitForm()
				{	
					$('.btn.green.find').hide();
					$('.progress').show();
				}

			</script>
			{!! Form::close() !!}


		</div>
		<div class="modal-footer">
			<a href="#" class="modal-action modal-close waves-effect waves-blue btn-flat">Cerrar</a>
		</div>
	</div>
	{{-- Fin Modals --}}

	<div class="container-m" id="other-pages">
		<div class="row">

			@if(session('success'))
			<div class="col s12 m12 l12">
				<div class="light-green message animated fadeInLeftBig">
					<p class="white-text"> <i class="material-icons" style="margin-right: 5px;">check</i>{{ session('success') }} </p>
				</div>
			</div>
			@endif

			@if(session('error'))
			<div class="col s12 m12 l12">
				<div class="red message animated fadeInLeftBig">
					<p class="white-text"> <i class="material-icons" style="margin-right: 5px;">clear</i>{{ session('error') }} </p>
				</div>
			</div>
			@endif

			<div class="col s12 m7 l7 border-radius">
				{{-- <div class="title blue darken-3">Fanpages</div> --}}
				<div class="card white">
					<ul class="collapsible" data-collapsible="accordion">
						@if(!$brand->fanpages->isEmpty())
						<li> <div class="collapsible-header"><i class="fa fa-facebook-square" aria-hidden="true"></i> <h3 class="grey-text">Facebook</h3></div> 
							<div class="collapsible-body">
								<ul class="fanpage-nav">
									@foreach($brand->fanpages as $fanpage)
									<li class="fanpage-item">

										<div class="analytics-info grey-text" >
											<h3><a class="grey-text" href="{{ url('/fanpage/'.$fanpage->id)}}">{{ $fanpage->name }}</a></h3>
											<div class="url-fix">
												<h4>
													<a href="#">{{ $fanpage->url }}</a></h4>
												</div>
												

												<div class="fanpage-scrap" id="fb-scrap-{{$fanpage->id}}" onclick="$('#fb-scrap-{{$fanpage->id}}').toggleClass('selected');">
													<h4 class="grey-text">Frecuencia de Scrap: {{($fanpage->scrap_type!==null)? $scrap_type[$fanpage->scrap_type]['name'] : 'No definida'}}</h4>
													<div class="selec-scrap">
														<select onchange="if(this.value!=={{($fanpage->scrap_type!==null)?$fanpage->scrap_type:"'null'"}})window.location.href='{{url('brand/scrap_type/Fanpage/'.$fanpage->id)}}/'+this.value;">

															<option disabled selected value="">Cambiar frecuencia</option>
															@foreach($scrap_type as $v=>$scrp)
															<option value="{{$v}}">{{$scrp['name']}}</option>
															@endforeach
														</select>
													</div>
												</div>


												<a href="{{ url('/fanpage/'.$fanpage->id)}}" class="waves-effect waves-light btn" style="text-transform: initial; margin-top: 10px; background-color: #444753;"><i class="material-icons left">remove_red_eye</i>Publicaciones analizadas: <strong>{{ $fanpage->posts->count() }}</strong></a>

											</div>

											<div class="cta">
												<a href="{{ url('/fanpage/'.$fanpage->id)}}"><i class="material-icons left" style="    font-size: 2.3em; color:#444753;">pageview</i></a>
											</div>
										</li>
										@endforeach
									</ul>
								</div>
							</li>
							@endif
							@if(!$brand->channels->isEmpty())
							<li> 
								<div class="collapsible-header"><i class="fa fa-youtube-play" aria-hidden="true"></i> <h3 class="grey-text">Youtube</h3></div> 
								<div class="collapsible-body">
									<ul class="fanpage-nav">	
										@foreach($brand->channels as $channel)
										<li class="fanpage-item">
											<div class="analytics-info grey-text" >
												<h3><a class="grey-text" href="{{ url('/channel/'.$channel->id)}}">{{ $channel->name }}</a></h3>
												<div class="urlfix">

													<h4 class='adapt-youtube'>
														<a href="#">{{ $channel->url}}</a></h4>
													</div>

													<div class="fanpage-scrap" id="yt-scrap-{{$channel->id}}" onclick="$('#yt-scrap-{{$channel->id}}').toggleClass('selected');">
														<h4 class="grey-text">Frecuencia de Scrap: {{($channel->scrap_type!==null)? $scrap_type[$channel->scrap_type]['name'] : 'No definida'}}</h4>
														<div class="selec-scrap">
															<select onchange="if(this.value!=={{($channel->scrap_type!==null)?$channel->scrap_type:"'null'"}})window.location.href='{{url('brand/scrap_type/YoutubeChannel/'.$channel->id)}}/'+this.value;">

																<option disabled selected value="">Cambiar frecuencia</option>
																@foreach($scrap_type as $v=>$scrp)
																<option value="{{$v}}">{{$scrp['name']}}</option>
																@endforeach
															</select>
														</div>
													</div>

													<a href="{{ url('/channel/'.$channel->id)}}" class="waves-effect waves-light btn" style="text-transform: initial; margin-top: 10px; background-color: #444753;"><i class="material-icons left">remove_red_eye</i>Publicaciones analizadas: <strong>{{ $channel->posts->count() }}</strong></a>
												</div>
												<div class="cta">
													<a href="{{ url('/channel/'.$channel->id)}}"><i class="material-icons left" style="    font-size: 2.3em; color:#444753;">pageview</i></a>
												</div>
											</li>
											@endforeach
										</ul>
									</div>
								</li>
								@endif
								@if(!$brand->profiles->isEmpty())
								<li> 
									<div class="collapsible-header"><i class="fa fa-instagram" aria-hidden="true"></i> <h3 class="grey-text">Instagram</h3></div> 
									<div class="collapsible-body">
										<ul class="fanpage-nav">	
											@foreach($brand->profiles as $profile)
											<li class="fanpage-item">
												<div class="analytics-info grey-text" >
													<h3><a class="grey-text" href="{{ url('/profile/'.$profile->id)}}">{{ $profile->name }}</a></h3>
													<div class="urlfix">
														<h4 class='adapt-youtube'>
															<a href="#">{{ $profile->url}}</a></h4>
														</div>
														<a href="{{ url('/profile/'.$profile->id)}}" class="waves-effect waves-light btn" style="text-transform: initial; margin-top: 10px; background-color: #444753;"><i class="material-icons left">remove_red_eye</i>Publicaciones analizadas: <strong>{{ $profile->posts->count() }}</strong></a>
													</div>
													<div class="cta">
														<a href="{{ url('/profile/'.$profile->id)}}"><i class="material-icons left" style="    font-size: 2.3em; color:#444753;">pageview</i></a>
													</div>
												</li>
												@endforeach
											</ul>
										</div>
									</li>
									@endif
								</ul>
							</div>
							{{-- 	<div id="fan-state">
							<h5>Seguidores por fanpage</h5>
							<div class="fans-state col s12 m12 l12 white" v-for='data in allData'>
								<div  v-bind:id='"fancount-chart-" + data["name"]' class="fan-state-graph"></div>
							</div>
						</div> --}}
					</div>
					<div class="col s12 m5 l5 border-radius form">
						<div class="title dark-blue-graph">Frecuencia de scrap</div>
						<div class="card white assign-scrap ">
							<h4> {{$scrap_type[$brand->scrap_type]['name']}}:</h4>
							<p>{{$scrap_type[$brand->scrap_type]['description']}}.</p>
							<select name="" id="" onchange="if(this.value != {{$brand->scrap_type}}) window.location.href = '{{url('brand/scrap_type/Brand/'.$brand->id)}}/'+this.value;">
								<option disabled selected value="">Cambiar frecuencia</option>
								@foreach($scrap_type as $v=>$scrp)
								<option value="{{$v}}">{{$scrp['name']}}</option>
								@endforeach
							</select>
						</div>

						<div class="title dark-blue-graph">Color de marca</div>
						<div class="card white assign-color ">
							@if(!$color)
							<h4>Aún no hay un color asignado, ¡asigna uno!</h4>
							@endif
							{!! Form::open(['route' => 'assign_brand_color']) !!}
							{!!Form::hidden('element_id', $brand->id) !!}
							{!!Form::hidden('table', 'brands') !!}
							<div class="conte-color">
								{{-- <div id="unique-color" class="color"> </div> --}}
								@if($color)
								<input id="unique-color" value='{{$color->color}}' class="jscolor color browser-default" onchange="selectColor(this)">
								@else
								<input id="unique-color" value='e7e7e7' class="jscolor color browser-default" onchange="selectColor(this)">
								@endif
								<div class="info-color">
									@if($color)
									{!!Form::hidden('color', $color->color, ['class' => 'alter-input', 'onchange' => "setColor(this)", 'id' => 'color-text']) !!}
									{{-- 	<input id="color-text" type="text" name="color" value='{{$color->color}}' placeholder="color" onchange="setColor(this)">	 --}}	
									@else
									{!!Form::hidden('color', " ", ['class' => 'alter-input', 'onchange' => "setColor(this)", 'id' => 'color-text']) !!}
									{{-- 	<input id="color-text" type="text" name="color" placeholder="color" onchange="setColor(this)">	 --}}
									@endif	
								</div>			
							</div>
							@if($color)
							{!!Form::submit('Modificar', ['class' => 'btn btn-primary green', 'style' => 'width:39%'] ) !!}
							@else
							{!!Form::submit('Asignar', ['class' => 'btn btn-primary green', 'style' => 'width:39%'] ) !!}
							@endif
							{!! Form::close() !!}
						</div>
						<div class="title dark-blue-graph">Agregar Fanpage</div>
						<div class="card white">

							{!! Form::open(['route' => 'redirect_create_fanpage']) !!}

							<div class="row">
								<div class="col s12 m12 l12">

									{!!Form::hidden('brand_id', $brand->id) !!}

									<label for="name" class="col m12 l12">Nombre del Fanpage</label>

									{{-- {!!Form::text('name') !!} --}}

									
									{!! Form::select('name',
									[
									'Argentina' => 'Argentina',
									'Australia' => 'Australia',
									'Bolivia' => 'Bolivia',
									'Brasil' => 'Brasil',
									'Canadá' => 'Canadá',
									'Centro América' => 'Centro América',
									'Chile' => 'Chile',
									'Colombia' => 'Colombia',
									'Ecuador' => 'Ecuador',
									'España' => 'España',
									'India' => 'India',
									'Jamaica' => 'Jamaica',
									'Latino América' => 'Latino América',
									'México' => 'México',
									'Nueva Zelanda' => 'Nueva Zelanda',
									'Paraguay' => 'Paraguay',
									'Perú' => 'Perú',
									'Puerto Rico' => 'Puerto Rico',
									'Reino Unido' => 'Reino Unido',
									'Uruguay' => 'Uruguay',
									'USA' => 'USA',
									'Venezuela' => 'Venezuela'], 'Colombia') !!}
									<label for="url" class="col m12 l12">Url de la fanpage</label>

									{!!Form::url('url') !!}

									<p class="grey-text small" style="display: flex; justify-content: center; align-items: center; margin-top: 0px;"><i style="margin-right: 10px;" class="material-icons green-text">message</i>Recuerda incluir en el nombre alguna distinción para categorizar fanpages por país.</p>
									{!!Form::submit('Agregar', ['class' => 'btn btn-primary green', 'style' => 'width:100%'] ) !!}
								</div>
							</div>
							{!! Form::close() !!}
						</div>
						<div class="title  dark-blue-graph">Agregar Producto</div>
						<div class="card white">
							<div class="row">
								<div class="col s12 m12 l12">
									{!! Form::open(['route' => 'create_product']) !!}
									{!!Form::hidden('brand_id', $brand->id) !!}
									<label for="name" class="col m12 l12">Nombre del producto</label>
									{!!Form::text('name') !!} 
									<label for="segment" class="col m12 l12">Segmento del producto</label>
                                     <select name="segment">
                                     	@foreach($segments as $segment)
                                     	<option value="{{$segment['id']}}">{{$segment['segmento']}}</option>
                                     	@endforeach
                                     </select>
									 
									{!!Form::submit('Agregar', ['class' => 'btn btn-primary green', 'style' => 'width:100%'] ) !!}
									{!! Form::close() !!}
								</div>	
							</div>
						</div>
						<ul class="collapsible" data-collapsible="accordion">
							<li>
								<div class="collapsible-header active">
									Productos ({{count($products)}} productos listados)
								</div>
								<div class="collapsible-body" id='product-list-container'> 
									<input id="filter-products" type="text" class='product-input' placeholder="Filtrar productos" @keyup="filterProducts($event)">
									<div class="product-container" v-for='producto in productos'>
										<form method='POST' action="http://graph.test:8888/product/edit">
										     {{ csrf_field() }}
											<input type="hidden" name='product_id' :value = 'producto.id'>
											<input type="text" name='name' class='alter-input product-name' :value='producto.name' :placeholder='producto.name'>
											<div v-if='producto.name !== "Todos" && producto.name !== "N/A"'>
												<select name="segmento">
													<option v-if='producto.segment.length != 0' :value='segmentos[producto.segment].id'  selected><% segmentos[producto.segment].segmento %></option>
													<option v-else disabled selected>Selecciona un Segmento</option>
													<option v-for='(segmento, index) in segmentos' v-if='segmento.id != producto.segment' :value="segmento.id"><% segmento.segmento %></option>
												</select>
											</div>
											<div v-if='producto.name != "Todos" && producto.name != "N/A"' class="product-options">
												{!! Form::button('<i class="fa fa-floppy-o" aria-hidden="true"></i>', array('type' => 'submit', 'class' => 'white-text btn btn-primary green')) !!}
												<a class='btn btn-primary red white-text' @click="confirmDelete(producto.name, producto.id)"  href="javascript:void(0)">
													<i class="white-text fa fa-trash-o" aria-hidden="true"></i>
												</a>		
											</div>	
										</form>	
									</div>
								</div>
							</li>
						</ul>
						<div class="title  dark-blue-graph">Concursos </div>
						<div class="card white" align="CENTER">								
							<a href="#addContest" class='modal-trigger btn btn-primary green' style="width: 90%">AGREGAR</a>
						</div>
						@php($contests = App\Contest::where('brand_id',$brand->id)->get())
						<ul class="collapsible" data-collapsible="accordion">
							<li>
								<div class="collapsible-header active">
									Concursos ({{count($contests)}} concursos listados)
								</div>
								<div class="collapsible-body">

									@foreach($contests as $contest) 
									{!! Form::open(['route' => ['contest.update', $contest->id] , 'method' => 'put']) !!}
									<div class="product-container contest-container contest-{{$contest->id}}">
										{!!Form::hidden('contest_id', $contest->id) !!}
										{!!Form::text('name', $contest->title, ['class' => 'alter-input']) !!}
										{!!Form::textarea('mechanics', $contest->content, ['class' => 'alter-input']) !!}
										{!!Form::text('prize', $contest->prize, ['class' => 'alter-input']) !!}
										{!! Form::date('from-date', explode(' ', $contest->since)[0]); !!}
										{!! Form::date('to-date', explode(' ', $contest->until)[0]); !!}
										<div class="product-options contest-options">
											{!! Form::button('<i class="fa fa-check" aria-hidden="true"></i>', array('type' => 'submit', 'class' => 'white-text btn btn-primary green')) !!}			
											{{-- {!!Form::submit(' ', ['class' => 'white-text btn btn-primary green fa fa-pencil-square-o'] ) !!} --}}
											{!! Form::close() !!}
											<a class='btn btn-primary red white-text'  href="{{route('contest.destroy' , $contest->id)}}">
												<i class="white-text fa fa-trash-o" aria-hidden="true"></i>
											</a>	
										</div>
									</div>
									@endforeach
								</div>
							</li>
						</ul>


					</div>
				</div>
			</div>
		</div>
		@endsection