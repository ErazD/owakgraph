@extends('layouts.app')

@section('content')

<div class="row grey lighten-3 mb-0">
  <div class="col s12 m12 l12 page-title">
   <h2 class="breadcrumbs-title left">Marcas</h2>
   <a class="add-brand right modal-trigger" href="#add">
    <i class="material-icons header">add_circle</i>
  </a>
</div>
</div>

<div class="row blue darken-3 mb-0">
  <div class="col s12 m12 l12 page-title">

    <p class="white-text">Actualmente se ha(n) agregado <strong>{{ $brands->count() }}</strong> marca(s) en <strong>OWAK Graph</strong></p>

    <div class="search">
        {!! Form::open(['route' => 'search_brand', 'method' => 'GET']) !!}
        
        <div class="row mb-0">
          <div class="col s12 m10 l10">

            {!!Form::text('query', null, ['class' => 'white-text mb-0', 'placeholder' => 'Buscar marca...']) !!}
          </div>
          <div class="col s12 m2 l2">
            {!!Form::submit('Ir', ['class' => 'btn btn-primary green', 'style' => 'width:100%; margin-top: 3px;'] ) !!}
          </div>

        </div>
        {!! Form::close() !!}
      </div>

  </div>

</div>

@if (Session::has('message'))
<div class="row red darken-4 mb-0 animated fadeIn">
  <div class="col s12 m12 l12 page-title">

    <p class="white-text">{{ Session::get('message') }} </p>
  </div>

</div>
@endif


<!-- Modal Structure -->
<div id="add" class="modal modal-fixed-footer brands">

  <h2 class="title dark-blue-graph">Añadir Marca</h2>
  <div class="modal-content">

    {!! Form::open(['route' => 'create_brand']) !!}
    
    <div class="row">
      <div class="col s12 m8 l8 offset-m2 offset-l2">
        <label for="name" class="col-md-4 control-label">Nombre de la marca</label>

        {!!Form::text('name') !!}

        {!!Form::submit('Agregar', ['class' => 'btn btn-primary green', 'style' => 'width:100%'] ) !!}

      </div>
    </div>

    {!! Form::close() !!}


  </div>
  <div class="modal-footer">
    <a href="#!" class="modal-action modal-close waves-effect waves-blue btn-flat">Cerrar</a>
  </div>
</div>

<div class="container-m">
  <div class="row" id="brands">

    @if(session('success'))
    <div class="col s12 m12 l12">
      <div class="light-green message animated fadeInLeftBig">
        <p class="white-text"> <i class="material-icons" style="margin-right: 5px;">check</i>{{ session('success') }} </p>
      </div>
    </div>
    @endif
    
    @foreach($brands as $brand)

    @include('brands.card')
    @endforeach

  </div>

</div>


@endsection