@extends('layouts.app')

@section('content')



<div class="row grey lighten-3 mb-0">
    <div class="col s12 m12 l12 page-title">
        <h2 class="breadcrumbs-title left">Iniciar sesión</h2>
    </div>
</div>

<div class="row blue darken-3 mb-0">
    <div class="col s12 m12 l12 page-title">

        <p class="white-text">Es necesario que inicies sesión con Facebook para utilizar los servicios de <strong>OWAK Graph</strong></p>

    </div>

</div>

<div class="login-row">
    <div class="container">

        <div class="row card-row">

            <div class="col s12 m12 l12 white parent">
 
                    {!! Form::open(['route' => 'login'])!!}
                    {{-- <div class="hidden-form col s12 m12 l12{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" type='hidden' class="col-md-4 control-label">Correo electrónico</label>

                            {!!Form::email('email', old('email'), ['required' => true, 'autofocus' => true]) !!}

                            @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                            @endif
                        </div>

                        <div class="hidden-form col s12 m12 l12{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Contraseña</label>

                            {!!Form::password('password', null, ['required' => false]) !!}

                            @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                            @endif
                        </div>

                      
                            <div class="col s12 m2 l2 hidden-form">

                                {!!Form::submit('Ok', ['class' => 'btn btn-primary teal', 'style' => 'width:100%'] ) !!}
                            </div> --}}
                           <div class="container">
                           <p style='text-align:center'>Inicia sesión con Facebook para utilizar <b>Graph</b></p>
                            <div class="button-facebook btn btn-primary">
                                <a href="{{ $login_url }}" type="submit" style="font-size: 1.1em;">
                                    <i class="fa fa-facebook-official" aria-hidden="true"></i>
                                    Continuar con Facebook
                                </a>
                            </div>
                            <p style='text-align:center; padding:10px; '><a href="#">Política de privacidad</a> y <a href="#">Condiciones del servicio</a></p>
                    </div>

                    {!! Form::close() !!} 
 
            </div>
        </div>
   </div>
</div>
@endsection
