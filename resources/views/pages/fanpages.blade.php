@extends('layouts.app')
{{-- @section('scripts')
<script src="{{ asset('js/vue.min.js') }}"></script>
 <script src="{{ asset('js/report_select.js') }}"></script>
 @stop --}}
 @section('content')
 <div class="row grey lighten-3 mb-0">
 	<div class="col s12 m12 l12 page-title">
 		<h2 class="breadcrumbs-title left">Reportes</h2>
 	</div>
 </div>	
 <div class="row blue darken-3 mb-0">
 	<div class="col s12 m12 l12 page-title">
 		<p class="white-text">Con esta herramienta es posible obtener la información de muchas publicaciones en tan solo unos pasos. Enjoy!</p>
 	</div>
 </div>
 <div class="row yellow lighten-5 animated fadeInLeft">
 	<div class="col s12 m12 l12 page-title" style="padding: 3px 3px 3px 29px !important;">
 		<p style="color:#f57f17; font-size: .9em;">Para una mejor experencia, selecciona un <strong>rango de fechas inferiores a 60 días.</strong></p>
 	</div>
 </div>
 <div class="row" id="scrap">	
 	<div class="col s12 m8 l8 offset-m2 offset-l2 report-selector">
 	{{--  <div class="title green">
 			<h2>Selecciona una red</h2>
 			<ul class="tabs">
 				<li class="tab"><a class="active" onClick="changeForm('facebook')">Facebook</a></li>
 				<li class="tab"><a onClick="changeForm('youtube')">Youtube</a></li>
 			</ul>
 		</div>  --}}
 		<div class="card">
 			{!! Form::open(['route' => 'redirect_report', 'onSubmit' =>'submitForm()']) !!}
 			<div class="row">
 				<div class="col s12 m12 l12">
 					<label for="url">Url Fanpage</label>
 					{!! Form::url('url', ' ' ,['placeholder' => 'Introduce una fanpage de Facebook o Youtube']) !!}
 				</div>

 				<div class="col s12 m6 l6">
 					<label for="url">Desde</label>
 					{!! Form::date('date_since', null, array('class'=>' datepicker')); !!}
 				</div>

 				<div class="col s12 m6 l6">
 					<label for="url">Hasta</label>
 					{!! Form::date('date_to', \Carbon\Carbon::now(), array('class'=>' datepicker')); !!}
 				</div>

 				<div class="col s12 m12 l12">
 					@if($errors)
 					@foreach ($errors->all() as $error)
 					<div class="red-text" style="display: flex;margin: 5px 0px 3px 0px;"><i class="material-icons">warning</i> {{ $error }}</div>
 					@endforeach
 					@endif
               
 					{!! Form::submit('Analizar', ['class' => 'btn btn-primary green white-text', 'style' => 'width:100%;margin-top: 10px;']) !!}
 					<div class="progress">
 						<div class="indeterminate"></div>
 					</div>
 				</div>
 			</div>
 		</div>

 		{!! Form::close() !!}

 		{{-- <div class="card youtube-card no-display">
 			{!! Form::open(['route' => 'channel_report', 'onSubmit' =>'submitForm()']) !!}

 			<div class="row">
 				<div class="col s12 m12 l12">
 					<label for="url">Url Canal Youtube</label>
 					{!! Form::url('url', 'https://www.youtube.com/user/ListerineCO' ,['placeholder' => 'https://www.youtube.com/user/ListerineCO']) !!}
 				</div>

 				<div class="col s12 m6 l6">
 					<label for="url">Desde</label>
 					{!! Form::date('date_since', null, array('class'=>' datepicker')); !!}
 				</div>

 				<div class="col s12 m6 l6">
 					<label for="url">Hasta</label>
 					{!! Form::date('date_to', \Carbon\Carbon::now(), array('class'=>' datepicker')); !!}
 				</div>
 				<div class="col s12 m12 l12">
 					@if($errors)
 					@foreach ($errors->all() as $error)
 					<div class="red-text" style="display: flex;margin: 5px 0px 3px 0px;"><i class="material-icons">warning</i> {{ $error }}</div>
 					@endforeach
 					@endif
 					{!! Form::submit('Analizar', ['class' => 'btn btn-primary green white-text', 'style' => 'width:100%;margin-top: 10px;']) !!}
 					<div class="progress">
 						<div class="indeterminate"></div>
 					</div>
 				</div>
 			</div>
 		</div>
 		{!! Form::close() !!} --}}
 		<script type=text/javascript>
 			function submitForm()
 			{	
 				$('.btn.green').hide();
 				$('.progress').show();
 			}
            // function changeForm(networkId)
            // {
            //      $('.card').hide();
            //      $('.'+networkId+'-card').show();
            // }
 			 
 		</script>
 	</div>
 </div>


 @stop