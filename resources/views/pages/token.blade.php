@extends('layouts.app')

@section('content')

<div class="row grey lighten-3 mb-0">
	<div class="col s12 m12 l12 page-title">
		<h2 class="breadcrumbs-title left">Token</h2>
	</div>
</div>

<div class="row blue darken-3 mb-0">
	<div class="col s12 m12 l12 page-title">
		<p class="white-text">Un token es una cadena de dígitos que identifica a un usuario, una aplicación o una página, y que la aplicación puede utilizar para realizar llamadas a Facebook.</p>
	</div>
</div>
<div class="row yellow lighten-5 animated fadeInLeft">
	<div class="col s12 m12 l12 page-title" style="padding: 3px 3px 3px 29px !important;">

		<p style="color:#f57f17; font-size: .9em;">Este token se actualizará de manera automática cada vez que alguien inicie sesión con Facebook.</p>

	</div>
</div>

<div class="container">
	<div class="card white row mb-0" style="margin-top: 45px;padding: 10px;    box-sizing: border-box;">

		<div class="col s12 m12 l12">
			<div class="row mb-0">
				<form class="col s12">
					<div class="row mb-0">
						<div class="input-field col s12">
							<textarea disabled id="token" class="materialize-textarea">{{ $token->token }}</textarea>
							<label for="token">Token</label>
						</div>
					</div>
				</form>
			</div>

		</div>
	</div>

</div>

@stop