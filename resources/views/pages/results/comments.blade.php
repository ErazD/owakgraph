

@extends('layouts.app')

@section('content')

<div class="row grey lighten-3 mb-0">
	<div class="col s12 m12 l12 page-title">
		<h2 class="breadcrumbs-title left">
			Comentarios de la publicación </a><br/>
			<a href="https://www.facebook.com/{{ $request->id }}" target="_blank">{{ $request->id }}</a></h2>
		</div>
	</div>

	<div class="row blue darken-3">
		<div class="col s12 m12 l12 page-title">

			<p class="white-text">Hemos encontrado <strong><u>{{ count($totalComments) }}</u></strong> comentario(s).</p>
			
			@if(is_null($request->attach))
			<a class="btn btn-primary green" onclick="selectElementContents(document.getElementById('posts') );"><i class="material-icons">content_copy</i></a>
			@endif

		</div>

	</div>

	<div class="row">
		<div class="col s12 m12 l12">
			<div class="wrap">
				<table class="bordered centered responsive-table" id="posts">
					<thead>
						<tr>
							<th>#</th>
							<th>Nombre</th>
							<th>Url</th>
							<th>Mensaje</th>
							<th>Tipo mensaje</th>
							@if(!is_null($request->attach))<th>Adjunto</th>@endif
							<th>Creación</th>
						</tr>
					</thead>

					<tbody>
						@foreach($totalComments as $key => $post)

						<tr>
							<td> {{ $key + 1 }}</td>
							{{-- <td> <a href="https://www.facebook.com/{{ $post['from']['id'] }}" target="_blank"> {{ $post['from']['name'] }}</a></td> --}}
							<td> <a href="{{$post['permalink_url'] }}" target="_blank"> <i class="material-icons">insert_link</i></a></td>
							<td> {{ $post['message'] }} </td>
							<td>@if(isset($post['attachment']))
							{{ $post['attachment']['type'] }}
							@else
							text
							@endif</td>
							@if(!is_null($request->attach))
							<td style="position: relative;"> @if(isset($post['attachment'])) 

								@if($post['attachment']['type'] == 'video_inline')
								<a href="{{ $post['attachment']['media']['image']['src'] }}" target="_blank" class="zoom-in-icon">
									<img class="responsive-img" src="{{ $post['attachment']['media']['image']['src'] }}" alt="">
								</a>
								<i class="material-icons video-fb zoom-in-icon">ondemand_video</i>
								@endif

								@if($post['attachment']['type'] == 'photo')
								<a href="{{ $post['attachment']['media']['image']['src'] }}" target="_blank" class="zoom-in-icon">
									<img class="responsive-img" src="{{ $post['attachment']['media']['image']['src'] }}" alt="">
								</a>
								@endif

								@endif
							</td>
							@endif
							<td> {{ $post['created_time']->format('m/d/Y H:i') }}</td>
						</tr>

					@endforeach

				</tbody>
			</table>

		</div>

	</div>

	<script>
		function selectElementContents(el) {
			var body = document.body, range, sel;
			if (document.createRange && window.getSelection) {
				range = document.createRange();
				sel = window.getSelection();
				sel.removeAllRanges();
				try {
					range.selectNodeContents(el);
					sel.addRange(range);
				} catch (e) {
					range.selectNode(el);
					sel.addRange(range);
				}
			} else if (body.createTextRange) {
				range = body.createTextRange();
				range.moveToElementText(el);
				range.select();
			}
		}


	</script>

</div>


@stop

