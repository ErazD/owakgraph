

@extends('layouts.app')

@section('content')

<div class="row grey lighten-3 mb-0">
	<div class="col s12 m12 l12 page-title">
		<h2 class="breadcrumbs-title left">
			Comentarios del Video </a><br/>
			<a href="https://www.youtube.com/watch?v={{ $request->id }}" target="_blank">{{ $request->id }}</a></h2>
		</div>
	</div>

	<div class="row blue darken-3">
		<div class="col s12 m12 l12 page-title">

			<p class="white-text">Hemos encontrado <strong><u>{{ count($totalComments) }}</u></strong> comentario(s).</p>
			
			
			<a class="btn btn-primary green" onclick="selectElementContents(document.getElementById('posts') );"><i class="material-icons">content_copy</i></a>
			

		</div>

	</div>

	<div class="row">
		<div class="col s12 m12 l12">
			<div class="wrap">
				<table class="bordered centered responsive-table" id="posts">
					<thead>
						<tr>
							<th>#</th>
							<th>Nombre</th>
							<th>Url</th>
							<th>Mensaje</th>
							<th>Likes</th>
							<th>Creación</th>
						</tr>
					</thead>
					<tbody>
						@foreach($totalComments as $key => $comment)
						<tr>
							<td> {{ $key + 1 }}</td>
							<td> <a href="{{ $comment['userchannel'] }}" target="_blank"> {{ $comment['username'] }}</a></td>
							<td> <a href="{{ $comment['url'] }}" target="_blank"> <i class="material-icons">insert_link</i></a></td>
							<td> {{ $comment['message'] }} </td>
							<td> {{ $comment['likes'] }} </td>							
							<td> {{ $comment['published'] }}</td>
						</tr>
					@endforeach
				</tbody>
			</table>

		</div>

	</div>

	<script>
		function selectElementContents(el) {
			var body = document.body, range, sel;
			if (document.createRange && window.getSelection) {
				range = document.createRange();
				sel = window.getSelection();
				sel.removeAllRanges();
				try {
					range.selectNodeContents(el);
					sel.addRange(range);
				} catch (e) {
					range.selectNode(el);
					sel.addRange(range);
				}
			} else if (body.createTextRange) {
				range = body.createTextRange();
				range.moveToElementText(el);
				range.select();
			}
		}


	</script>

</div>


@stop

