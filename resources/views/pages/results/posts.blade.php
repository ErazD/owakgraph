

@extends('layouts.app')

@section('content')

<div class="row grey lighten-3 mb-0">
	<div class="col s12 m12 l12 page-title">
		<h2 class="breadcrumbs-title left">Fan pages: Resultados</h2>
	</div>
</div>

<div class="row blue darken-3">
	<div class="col s12 m12 l12 page-title">

		<p class="white-text">Hemos encontrado <strong><u>{{ count($totalPosts) }}</u></strong> publicación(es) en <a href="{{ $request->url }}" class="white-text"><strong>{{ $fanpage_name[1] }}</strong></a><br/> desde <strong>{{ $request->date_since }}</strong> hasta <strong>{{ $request->date_to }}</strong></p>
		<div class="btn-group">
		<a class="btn btn-primary green mr-5 tooltipped" onclick="selectElementContents(document.getElementById('posts') );" data-position="bottom" data-delay="50" data-tooltip="Copiar"><i class="material-icons">content_copy</i></a>
			<a class="btn btn-primary green tooltipped" href="{{ url('/download/report/'.$unique_id) }}" data-position="bottom" data-delay="50" data-tooltip="Descargar"><i class="material-icons">file_download</i></a>

		</div>

	</div>

</div>

<div class="row">
	<div class="col s12 m12 l12">
		<div class="wrap">
			<table class="bordered centered responsive-table" id="posts">
				<thead>
					<tr>
						<th>#</th>
						<th>Contenido</th>
						<th>Tipo</th>
						<th>Comentarios</th>
						<th>Reacciones</th>
						<th>Compartidos</th>
						<th>Publicado</th>
						<th>Url</th>
					</tr>
				</thead>

				<tbody>
					@foreach($totalPosts as $key => $post)

					<tr>
						<td>{{ $key+1 }}</td>
						<td> @if(isset($post['message'])) {{ $post['message'] }} @else  @endif</td>
						<td> {{ $post['type'] }} </td>
						<td> {{ $post['comments']->getTotalCount() }} </td>
						<td> {{ $post['reactions']->getTotalCount() }} </td>
						<td> @if(isset($post['shares']['count'])) {{ $post['shares']['count'] }} @else 0 @endif</td>
						<td> {{ $post['created_time']->format('Y/m/d H:i:s') }} </td>
						@if(array_key_exists('permalink_url', {{$post['permalink_url']}}))
						@if( empty($post['permalink_url'] ))
						<td> <a href="#" target="_blank"><i class="material-icons">insert_link</i></a> </td>
						@else
						<td> <a href="{{ $post['permalink_url'] }}" target="_blank"><i class="material-icons">insert_link</i></a> </td>
						@endif
					</tr>

					@endforeach

				</tbody>
			</table>

		</div>

	</div>

	<script>
		function selectElementContents(el) {
			var body = document.body, range, sel;
			if (document.createRange && window.getSelection) {
				range = document.createRange();
				sel = window.getSelection();
				sel.removeAllRanges();
				try {
					range.selectNodeContents(el);
					sel.addRange(range);
				} catch (e) {
					range.selectNode(el);
					sel.addRange(range);
				}
			} else if (body.createTextRange) {
				range = body.createTextRange();
				range.moveToElementText(el);
				range.select();
			}
		}


	</script>

</div>


@stop

