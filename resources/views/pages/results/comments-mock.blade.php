

@extends('layouts.app')

@section('content')
<?php

 
// dd($egh->data);
 //dd($datos);
$totalComments = $datos;
 
?>

<div class="row blue darken-3">
		<div class="col s12 m12 l12 page-title">

		 
			<a class="btn btn-primary green" onclick="selectElementContents(document.getElementById('posts') );"><i class="material-icons">content_copy</i></a>
			 

		</div>

	</div>
 	<div class="row">
		<div class="col s12 m12 l12">
			<div class="wrap">
				<table class="bordered centered responsive-table" id="posts">
					<thead>
						<tr>
							<th>#</th>
							<th>Nombre</th>
							<th>Url</th>
							<th>Mensaje</th>
							<th>Tipo mensaje</th>
							<th>Creación</th>
						</tr>
					</thead>

					<tbody>
						@foreach($totalComments as $key => $post)

						<tr>
							<td> {{ $key + 1 }}</td>
							<td> <a href="https://www.facebook.com/{{ $post['from']['id'] }}" target="_blank"> {{ $post['from']['name'] }}</a></td>
							<td> <a href="{{$post['permalink_url'] }}" target="_blank"> <i class="material-icons">insert_link</i></a></td>
							<td> {{ $post['message'] }} </td>
							<td>@if(isset($post['attachment']))
							{{ $post['attachment']['type'] }}
							@else
							text
							@endif</td>
							@if(false)
							<td style="position: relative;"> @if(isset($post['attachment'])) 

								@if($post['attachment']['type'] == 'video_inline')
								<a href="{{ $post['attachment']['media']['image']['src'] }}" target="_blank" class="zoom-in-icon">
									<img class="responsive-img" src="{{ $post['attachment']['media']['image']['src'] }}" alt="">
								</a>
								<i class="material-icons video-fb zoom-in-icon">ondemand_video</i>
								@endif

								@if($post['attachment']['type'] == 'photo')
								<a href="{{ $post['attachment']['media']['image']['src'] }}" target="_blank" class="zoom-in-icon">
									<img class="responsive-img" src="{{ $post['attachment']['media']['image']['src'] }}" alt="">
								</a>
								@endif
								@endif
							</td>
							@endif
							<td> {{ date($post['created_time']) }}</td>
						</tr>

					@endforeach

				</tbody>
			</table>

		</div>

	</div>

	<script>
		function selectElementContents(el) {
			var body = document.body, range, sel;
			if (document.createRange && window.getSelection) {
				range = document.createRange();
				sel = window.getSelection();
				sel.removeAllRanges();
				try {
					range.selectNodeContents(el);
					sel.addRange(range);
				} catch (e) {
					range.selectNode(el);
					sel.addRange(range);
				}
			} else if (body.createTextRange) {
				range = body.createTextRange();
				range.moveToElementText(el);
				range.select();
			}
		}


	</script>

</div>


@stop

