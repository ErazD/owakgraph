@extends('layouts.app')

@section('content')

<style>

span.info {
    /*border-bottom: 1px dashed;*/
    text-decoration: none;
    margin: 0em 1em .7em 0;
    color: #ccc;
    font-size:1.2em; 
    float: right;
}

span.info:hover {
    cursor: help;
    position: relative
}
span.info span {
    display: none
}
span.info:hover span {
    border: #c0c0c0 1px dotted;
    border-radius: 5px;
    padding: 5px 20px 5px 5px;
    display: block;
    z-index: 100;
    background: #f0f0f0 no-repeat 100% 5%;
    right: -50px;
    margin: 10px;
    position: absolute;
    top: 18px;
    text-decoration: none;
	min-width: 250px;
    color: black #333;
}

span.info:hover span {

    color: black;
}

</style>

<div class="row grey lighten-3 mb-0">
	<div class="col s12 m12 l12 page-title">
		<h2 class="breadcrumbs-title left">Comentarios</h2>
	</div>
</div>

<div class="row blue darken-3 mb-0">
	<div class="col s12 m12 l12 page-title">
		<p class="white-text">Con esta herramienta es posible obtener el <strong>total de comentarios</strong> de una publicación con solo saber su <a href="http://findpostid.com/" class="white-text" target="_blank"><u>ID</u></a>.</p>
	</div>
</div>

<div class="row yellow lighten-5 animated fadeInLeft">
	<div class="col s12 m12 l12 page-title" style="padding: 3px 3px 3px 29px !important;">

		<p style="color:#f57f17; font-size: .9em;">Para una mejor experencia, utiliza el filtro solo sí tu publicación supera los 2000 comentarios.</p>

	</div>
</div>

<div class="row" id="scrap">
	
	<div class="col s12 m8 l8 offset-m2 offset-l2">
		
		<div class="card">

			{!! Form::open(['route' => 'redirect_comments', 'onSubmit' =>'submitForm()']) !!}

			<div class="row">
				<div class="col s12 m12 l12">
					<label for="url">ID publicación   </label> 
					 
					<span class="info "><i class="fa fa-youtube-play" aria-hidden="true"></i><span>https://www.youtube.com/watch?v=<b>gjS5MOcxjyU</b> &t=652</span></span>
					<span class="info "><i class="fa fa-facebook-square" aria-hidden="true"></i><span>https://www.facebook.com/LubridermCO/posts/<b>345696075855242</b></span></span>
					<span class="info"><i class="fa fa-question-circle" aria-hidden="true"></i><span>La ID se encuentra en la url, es la parte que se muestra en negrilla en los siguientes ejemplos.</span></span>
					 

					{!! Form::text('id', '253958271675711' ,['placeholder' => '253958271675711']) !!}
				</div>
				<div class="col s12 m12 l12">
					<label for="url">Red Social</label>
				{!! Form::select('social_network',
								['Facebook' => 'Facebook',
								'YouTube' => 'YouTube'],
								 'Facebook') !!}

				</div>
				<div class="col s12 m6 l6">
					<label for="url">Desde</label>
					{!! Form::date('date_since', null, array('class'=>' datepicker')); !!}
				</div>

				<div class="col s12 m6 l6">
					<label for="url">Hasta</label>
					{!! Form::date('date_to', \Carbon\Carbon::now(), array('class'=>' datepicker')); !!}
				</div>

				<div class="col s12 m6 l6">

					{!! Form::checkbox('attach', 1, false, ['id' => 'attach']); !!}
					<label for="attach">¿Mostrar adjuntos?</label>

				</div>

				<div class="col s12 m6 l6">

					{!! Form::checkbox('date', 1, true, ['id' => 'date']); !!}
					<label for="date">¿Utilizar el filtro de fecha?</label>

				</div>


				<div class="col s12 m12 l12">

					@if($errors)
					@foreach ($errors->all() as $error)
					<div class="red-text" style="display: flex;margin: 5px 0px 3px 0px;"><i class="material-icons">warning</i> {{ $error }}</div>
					@endforeach
					@endif

					{!! Form::submit('Obtener', ['class' => 'btn btn-primary green white-text', 'style' => 'width:100%; margin-top: 15px;']) !!}
					<div class="progress mt-5">
						<div class="indeterminate"></div>
					</div>
				</div>
			</div>
		</div>

		{!! Form::close() !!}

		<script type=text/javascript>
			function submitForm()
			{	
				$('.btn.green').hide();
				$('.progress').show();
			}

		</script>
	</div>
</div>


@stop