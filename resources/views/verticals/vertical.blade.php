@extends('layouts.app')

@section('scripts')
  <script>
    $(document).ready(function(){
      $('ul.tabs').tabs();
      $('#tabs-verticals').tabs({'swipeable':'true'});
      $('#verticals-menu-content').mouseenter( openMenu ).mouseleave( closeMenu );
      
    });

    function openMenu(){
      $('#verticals-menu').collapsible('open', 0);
      setMenu();
    }
    function closeMenu(){
      $('#verticals-menu').collapsible('close', 0);
    }

    function setMenu(tab){
       $('#tabs-verticals').tabs('select_tab', tab);
    }
  </script>
@endsection
@section('content')

<div class="row grey lighten-3 mb-0">
  <div class="col s12 m12 l12 page-title">
   <h2 class="breadcrumbs-title left">{{$vertical->name}}</h2>
   <a class="add-brand right modal-trigger" href="#add">
    <i class="material-icons header">add_circle</i>
  </a>
</div>
</div>

<div class="row blue darken-3 mb-0">
  <div class="col s12 m12 l12 page-title">

    <p class="white-text">Actualmente se ha(n) agregado <strong>{{ sizeof($vertical->usercategories($user)->get()) }}</strong> Categorías en la vertical <strong>{{$vertical->name}}</strong></p>

    <div class="search">
      {!! Form::open(['route' => 'search_brand', 'method' => 'GET']) !!}

      <div class="row mb-0">
        <div class="col s12 m10 l10">
          {!!Form::text('query', null, ['class' => 'white-text mb-0', 'placeholder' => 'Buscar marca...']) !!}
        </div>
        <div class="col s12 m2 l2">
          {!!Form::submit('Ir', ['class' => 'btn btn-primary green', 'style' => 'width:100%; margin-top: 3px;'] ) !!}
        </div>

      </div>
      {!! Form::close() !!}
    </div>
  </div>

</div>

<div id="verticals-menu-content" class="row grey lighten-3 mb-0">


  <div class="col s12 m12 l12">
   <ul id="tabs-verticals" class="tabs tabs-fixed-width ">
    
    <li class="tab col s3"><a href="#{{$vertical->slug}}" onmouseover="setMenu('{{$vertical->slug}}')" >{{$vertical->name}}</a></li>
   
  </ul>
  <ul id="verticals-menu" class="collapsible" data-collapsible="expandable">
    <li>
      <div class="collapsible-header" style="display: none;"></div>
      <div class="collapsible-body white" align="center" >
        {{-- <div id="{{$vertical->slug}}" class="col s12 green">{{$vertical->name}}</div> --}}
        <div id="{{$vertical->slug}}" class="col s12 white vertical-content">
          @foreach($vertical->usercategories($user)->get() as $cate)
          <div class="col s12 m6 l3 categories-content">
            <a class="category-link" href="{{url('/categoria')}}/{{$cate->id}}">{{$cate->name}} ({{$cate->usercanastas($user)->count()}}) </a>
            @foreach($cate->usercanastas($user)->get() as $canasta)
            <a class="canasta-link" href="{{url('/canasta')}}/{{$canasta->id}}">{{$canasta->name}} ({{$canasta->userbrands($user)->count()}})</a>
            @endforeach   
          </div>          
          @endforeach
        </div> 
      </div>
    </li>
  </ul>
</div>  
</div>


@if (Session::has('message'))
<div class="row red darken-4 mb-0 animated fadeIn">
  <div class="col s12 m12 l12 page-title">
    <p class="white-text">{{ Session::get('message') }} </p>
  </div>
</div>
@endif


<!-- Modal Structure -->
<div id="add" class="modal modal-fixed-footer brands">

  <h2 class="title dark-blue-graph">Añadir Marca</h2>
  <div class="modal-content">

    {!! Form::open(['route' => 'vertical.store']) !!}
    <div class="row">
      <div class="col s12 m8 l8 offset-m2 offset-l2">
        <label for="name" class="col-md-4 control-label">Nombre de la Vertical</label>
        {!!Form::text('name') !!}
        {!!Form::text('description') !!}
        {!!Form::submit('Agregar', ['class' => 'btn btn-primary green', 'style' => 'width:100%'] ) !!}
      </div>
    </div>
    {!! Form::close() !!}


  </div>
  <div class="modal-footer">
    <a href="#!" class="modal-action modal-close waves-effect waves-blue btn-flat">Cerrar</a>
  </div>
</div>

<div class="container-m">
  <div class="row" id="brands">

    @if(session('success'))
    <div class="col s12 m12 l12">
      <div class="light-green message animated fadeInLeftBig">
        <p class="white-text"> <i class="material-icons" style="margin-right: 5px;">check</i>{{ session('success') }} </p>
      </div>
    </div>
    @endif

    @foreach($brands as $brand)
   {{--  <h1>{{$key}}</h1> --}}
    
    {{-- @foreach($cate as $brand) --}}
    @include('verticals.brand_card')
    
    {{-- @endforeach --}}
    @endforeach
  </div>
</div>
@endsection
