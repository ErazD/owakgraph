    <div class="col s12 m3">
      <div class="card white">
        <div class="card-content grey-text">
          <span class="card-title">{{ $brand->name }}</span>
          <p>Tiene <strong class="blue-text">{{ $brand->fanpages->count() }}</strong> Fanpages asociadas</p>
        </div>
        <div class="card-action">
          <a href="{{ url('/brand/'.$brand->id) }}" class="btn btn-default">Ver más</a>
        </div>
      </div>
    </div>