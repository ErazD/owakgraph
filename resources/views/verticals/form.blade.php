@extends('layouts.app')

@section('content')

<h2 class="title dark-blue-graph">Añadir {{$titulo}} </h2>

@if (Session::has('message'))
<div class="row red darken-4 animated fadeIn">
    <div class="col s12 m12 l12 page-title">

        <p class="white-text">{{ Session::get('message') }} </p>
    </div>

</div>
@endif
@if(session('success'))
<div class="col s12 m12 l12">
    <div class="light-green message animated fadeInLeftBig">
        <p class="white-text"> <i class="material-icons" style="margin-right: 5px;">check</i>{{ session('success') }} </p>
    </div>
</div>
@endif
@if(session('error'))
<div class="col s12 m12 l12">
    <div class="light-green message animated fadeInLeftBig">
        <p class="white-text"> <i class="material-icons" style="margin-right: 5px;">clear</i>{{ session('error') }} </p>
    </div>
</div>
@endif


<form action="{{route($route)}}" method="POST">
   {{ csrf_field() }}
   <div class="row">
      <div class="col s12 m8 l8 offset-m2 offset-l2">
        <label for="name" class="col-md-4 control-label">Nombre de la {{$titulo}} </label>
        <input type="text" name="name">

        <label for="description" class="col-md-4 control-label">Descripción </label>
        <input type="text" name="description">

        @if($titulo != 'Industria')
        <label for="parent" class="col-md-4 control-label">Id de <span id="parent-name"> de la categoría a la que pertenece</span> </label>
        <input id="parent" list="parents" name="parent" onfocusout="changeParentValue()" autocomplete="off" required>
        <datalist id="parents">
            @foreach($parents as $k=>$p)
            <option id="child_{{$p->id}}" value="{{$p->id}}" label="{{$p->name}}" name="{{$p->name}}">
                @endforeach
            </datalist>
            <script>
                var parent = document.getElementById("parent");
                var parentName = document.getElementById("parent-name");
                function changeParentValue(){
                    var val = document.getElementById("child_" + parent.value)
                    if(val!=null){
                        parentName.innerHTML = val.label;
                    }else{
                        parent.value = null
                        parentName.innerHTML = "de la categoría a la que pertenece";
                    }

                }
            </script>
            @endif
            <input type="submit" class="btn btn-primary green" style="width:100%" value="Agregar">

        </div>
    </div>

</form>

@if(isset($segmento_producto))

<h2 class="title dark-blue-graph">Asignar productos</h2>

{{-- <form > --}}
{{ csrf_field() }}
<div class="row">
  <div class="col s12 m8 l8 offset-m2 offset-l2">
    
   
    
    
    <div class="col s12 m6"> 
        <label for="segment" class="col-md-4 control-label">Categoría </label><br>
        <div class="col s4 m6">
           <input id="segment" list="segments" name="segment" onfocusout="changeSegmentProductValue(this.value, true )" required>
           <datalist id="segments">
                @foreach($segmento_producto['segments'] as $k=>$p)
                    <option id="seg_{{$p->id}}" value="{{$p->id}}" label="{{$p->name}}" name="{{$p->name}}">
                @endforeach
            </datalist>
        </div>
        <div class="col s8 m6">
            <span id="segment-name">Elegir categoría</span>
        </div>
    </div>    

    <div class="col s12 m6"> 
        <label for="segment" class="col-md-4 control-label">Producto </label><br>
        <div class="col s4 m6">
           <input id="product" list="products" name="product" onfocusout="changeSegmentProductValue(this.value, false)" required>
           <datalist id="products">
            @foreach($segmento_producto['products'] as $k=>$p)
            <option id="prod_{{$p->id}}" value="{{$p->id}}" label="{{$p->name}} @if($p->segmento) ({{$p->segmento->name}})@endif" name="{{$p->name}}"></option>
            @endforeach
        </datalist>
    </div>
    <div class="col s8 m6">
        <span id="product-name">Elegir producto</span>
    </div>

</div>


<script>
    var url = window.location.protocol+"//"+window.location.host;
    var segment = document.getElementById("segment");
    var segmentName = document.getElementById("segment-name");
    var product = document.getElementById("product");
    var productName = document.getElementById("product-name");

    function changeSegmentProductValue(val, isSegment){
        var pre = (isSegment)? 'seg_' : 'prod_';
        var par = (isSegment)? segment: product;
        var parName = (isSegment)? segmentName: productName;
        var val = document.getElementById(pre + par.value);

        if(val!=null){
            parName.innerHTML = val.label;
        }else{
            par.value = null
            parName.innerHTML = "Elegir elemento";
        }

    }

    function attachProductSegment(){
                        // alert("aaa");
                        // alert(url+"/segment-add-product/"+segment.value+"/"+product.value);
                        window.location.href = url+"/segment-add-product/"+segment.value+"/"+product.value;
                    }
                </script>
                
                <button  onclick="attachProductSegment()" class="btn btn-primary green" style="width:100%" >Asignar Producto </button> 


            </div>
        </div>

        {{-- </form> --}}
        @endif

        @if(isset($canasta_marca))
        
        <h2 class="title dark-blue-graph">Asignar Marcas</h2>

        {{-- <form > --}}
        {{ csrf_field() }}
        <div class="row">

          <div class="col s12 m8 l8 offset-m2 offset-l2">
            
            <div class="col s12 m6"> 
                <label for="basket" class="col-md-4 control-label">Seleccionar Canasta</label><br>
                <div class="col s4 m6">
                   <input id="basket" list="baskets" name="basket" onfocusout="changeBrandProductValue(this.value, true )" required>
                   <datalist id="baskets">
                    @foreach($canasta_marca['basket'] as $k=>$p)
                    <option id="basket_{{$p->id}}" value="{{$p->id}}" label="{{$p->name}}" name="{{$p->name}}">
                        @endforeach
                    </datalist>
                </div>
                <div class="col s8 m6">
                    <span id="basket-name">Elegir Canasta</span>
                </div>
            </div>   

            <div class="col s12 m6"> 
                <label for="segment" class="col-md-4 control-label">Seleccionar Marca</label><br>
                <div class="col s4 m6">
                   <input id="brand" list="brands" name="brands" onfocusout="changeBrandProductValue(this.value, false)" required>
                   <datalist id="brands">
                    @foreach($canasta_marca['brand'] as $k=>$p)
                    <option id="brand_{{$p->id}}" value="{{$p->id}}" label="{{$p->name}} @if($p->segmento) ({{$p->segmento->name}})@endif" name="{{$p->name}}"></option>
                    @endforeach
                </datalist>
            </div>
            <div class="col s8 m6">
                <span id="brand-name">Elegir Marca</span>
            </div>

        </div>
        <script>
            var url = window.location.protocol+"//"+window.location.host;
            var basket = document.getElementById("basket");
            var basketName = document.getElementById("basket-name");
            var brand = document.getElementById("brand");
            var brandName = document.getElementById("brand-name");
            
            function changeBrandProductValue(val, isSegment){
                var pre = (isSegment)? 'basket_' : 'brand_';
                var par = (isSegment)? basket: brand;
                var parName = (isSegment)? basketName: brandName;
                var val = document.getElementById(pre + par.value);

                if(val!=null){
                    parName.innerHTML = val.label;
                }else{
                    par.value = null
                    parName.innerHTML = "Elegir elemento";
                }

            }

            function attachBrandBasket(){
                        // alert("aaa");
                         //alert(url+"/canasta-add-brand/"+basket.value+"/"+brand.value);
                         window.location.href = url+"/canasta-add-brand/"+basket.value+"/"+brand.value;
                     }
                 </script>
                 
                 <button  onclick="attachBrandBasket()" class="btn btn-primary green" style="width:100%" >Asignar Marca </button> 


             </div>
         </div>

         {{-- </form> --}}
         @endif

         @endsection