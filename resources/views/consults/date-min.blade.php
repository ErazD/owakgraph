<div id="date-form" class="row formDateContainer" style="padding: 8px; margin-bottom:10px">
	<div class="col s12 m6 l3">
		<input v-model="since" type="text" placeholder="Y-m-d">	
	</div>
	<div class="col s12 m6 l3">
	   <input v-model="until" type="text" placeholder="Y-m-d">
	</div>
	<div class="col s12 m6 l3">
		<select v-model="interval" v-on:change="setDate(interval)" class="browser-default"  name="interval"   id="">
			<optgroup label="" >
				<option value="all">Todo</option>
			</optgroup>
			<optgroup label="" >
				<option value="hoy">hoy</option>
				<option value="day">El mismo Día</option>
			</optgroup>
			<optgroup label="">
				<option value="6">Semana anterior</option>
				<option value="28">28 días anteriores</option>
				<option value="30">30 días anteriores</option>
			</optgroup>
			<optgroup label="">
				<option value="90">trimestre anterior (90 días)</option>
				<option value="182">semestre anterior (182 días)</option>
				<option value="365">año anterior</option>
			</optgroup>
		</select>
	</div>	

	<div class="col s12 m6 l3">
		<a v-bind:href=" '{{ url($route) }}' + getDates" class="btn indigo tooltipped" data-position="bottom" data-delay="50" data-tooltip="Para filtrar por fecha es importante la fecha final">
			Filtrar por fecha
		</a>
	</div>				
</div>

{{-- url('fanpage/'.$fanpage->id ) --}}
