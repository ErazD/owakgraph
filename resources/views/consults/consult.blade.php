@extends('layouts.app')

@section('scripts')
<script src="{{ asset('js/vue.min.js') }}"></script>
<script src="{{ asset('js/moment.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.6.0/Chart.min.js"></script>
<script src="https://www.amcharts.com/lib/3/amcharts.js"></script>
<script src="https://www.amcharts.com/lib/3/serial.js"></script>
<script src="{{ asset('js/consult-chart.js') }}"></script>
@stop
@section('content')

<div class="row grey lighten-3 mb-0">
	<div class="col s12 m12 l12 page-title">
		<h2 class="breadcrumbs-title left">
		Consulta</h2>
	</div>
</div>

<div class="row blue darken-3">
	<div class="col s12 m12 l12 page-title">

		<p class="white-text" style="width: 80%;"> 
		{{-- 	@if(!is_null($video->content))
			{{ $video->content }}
			@else
			Esta publicación no tiene una descripción
			@endif --}}
		</p>
		<div class="btn-group">
			{{-- <a href="{{ $video->permalink_url }}" target="_blank" class="btn btn-primary green tooltipped mr-5" style="margin-left: 20px;" data-position="bottom" data-delay="50" data-tooltip="Abrir en Facebook"><i class="material-icons">insert_link</i></a>

			<a href="{{ url('youtube-video/'.$video->id.'/export') }}" target="_blank" class="btn btn-primary green tooltipped" data-position="bottom" data-delay="50" data-tooltip="Descargar"><i class="material-icons">file_download</i></a> --}}
		</div>

	</div>

</div>

<div class="container-m" id="stats">

	<input v-model="since" type="text" placeholder="Y-m-d" @click="updateStatus()">
	<input v-model="until" type="text" placeholder="Y-m-d" @click="updateStatus()">
	<select class="browser-default" v-model="interval"  name="interval"   id="">
		<option value="1 day">Diario</option>
		<option value="1 week">Semanal</option>
		<option value="1 month">Mensual</option>
	</select>
	<select class="browser-default" v-model="typeSel" name="type" id="" @click="updateStatus()">
		<option value="vertical">Vertical</option>
		<option value="categoria">Categoría</option>
		<option value="canasta">Canasta</option>
		<option value="marca">Marca</option>
	</select>

	<input v-model="slug" type="text" placeholder="slug de la búsqueda" >
	<button @click="globalMetrics()">Consultar</button>
	<span><%since%> <%until%> <%typeSel%> <%slug%></span>
	<br>
	<div v-bind:class="{ hide: hidedivResult }" align="center">
		<b>Fechas en los que se alcanzaron los valores máximos</b>
		
		<table v-if="!hidedivResult">
			<tr>
				<th></th><th>FB_Reactions</th><th>FB_Comments</th><th>FB_Shares</th>

				<th>YT_Views</th><th>YT_Comments</th><th>YT_Likes</th><th>YT_Dislikes</th>
			</tr>
			<tr>
				<th>Fecha</th><td><%maxResults.max_facebook_reactions.date_i%> a <%maxResults.max_facebook_reactions.date_f%></td><td><%maxResults.max_facebook_comments.date_i%> a <%maxResults.max_facebook_comments.date_f%></td><td><%maxResults.max_facebook_shares.date_i%> a <%maxResults.max_facebook_shares.date_f%></td>

				<td><%maxResults.max_youtube_views.date_i%> a <%maxResults.max_youtube_views.date_f%></td><td><%maxResults.max_youtube_comments.date_i%> a <%maxResults.max_youtube_comments.date_f%></td><td><%maxResults.max_youtube_likes.date_i%> a <%maxResults.max_youtube_likes.date_f%></td><td><%maxResults.max_youtube_dislikes.date_i%> a <%maxResults.max_youtube_dislikes.date_f%></td>
			</tr>
			<tr>
				<th>Cantidad</th><td><%maxResults.max_facebook_reactions.facebook_reactions%></td><td><%maxResults.max_facebook_comments.facebook_comments%></td>

				<td><%maxResults.max_facebook_shares.facebook_shares%></td><td><%maxResults.max_youtube_views.youtube_views%></td><td><%maxResults.max_youtube_comments.youtube_comments%></td><td><%maxResults.max_youtube_likes.youtube_likes%></td><td><%maxResults.max_youtube_dislikes.youtube_dislikes%></td>
			</tr>
		</table>

		<div class="row">
			<div class="col s12 m12 l12">
				<div class="title green">
					<h2>Valores totales</h2>
				</div>
				<div class="card white" id="hoursChartdiv">
					<div id="consult-chart" class="postChart"></div>
				</div>
			</div>
		</div>
		
	</div>

	
	{{-- 
	<div class="row">
		<div class="col s12 m12 l12">
			<div class="title blue">
				<h2>Estadísticas por día</h2>
			</div>
			<div class="card white" id="daysChartdiv">
				<div id="daysChart" class="postChart">
				</div>
			</div>
		</div>
	</div> --}}


</div>
@stop