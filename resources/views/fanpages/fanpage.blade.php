@extends('layouts.app')
<?php $user = (Auth::check())? Auth::user(): false; ?>
@section('scripts')
<script>
	// variables para comms-chart.js
	var type = "fanpage";
	var pedido_id = {{$fanpage->id}};
	var formComms=true;
	// $(document).ready(function(){
 //    	$('ul.tabs').tabs();
 //  	});
</script>
<script src="{{ asset('js/vue.min.js') }}"></script>

{{-- <script src="https://www.amcharts.com/lib/3/amcharts.js"></script>
<script src="https://www.amcharts.com/lib/3/pie.js"></script>
<script src="https://www.amcharts.com/lib/3/plugins/export/export.min.js"></script>
<link rel="stylesheet" href="https://www.amcharts.com/lib/3/plugins/export/export.css" type="text/css" media="all" />
<script src="{{ asset('js/comms-chart.js') }}"></script> --}}
{{-- ////////////// --}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.6.0/Chart.min.js"></script>
<script src="https://www.amcharts.com/lib/3/amcharts.js"></script>
<script src="https://www.amcharts.com/lib/3/serial.js"></script>
<script src="https://www.amcharts.com/lib/3/plugins/export/export.min.js"></script>
<link rel="stylesheet" href="https://www.amcharts.com/lib/3/plugins/export/export.css" type="text/css" media="all" />
<script src='{{ asset('js/fan-count.js') }}'></script>
{{-- //////////// --}}
<script src="{{ asset('js/appComms.js') }}"></script>
<script src="{{ asset('js/date-selector.js') }}"></script>

{{-- <script src="{{ asset('js/main.js') }}"></script> --}}

@if(isset($since))
<script>
		//comms_register = "{ route('comms_register')}}";
		comms_register = "{{route('two-factor.store')}}";
		dateSelectorApp.since = "{{$since}}";
		dateSelectorApp.until = "{{$until}}";
	</script>
	@endif

	@stop
	@section('content')
	<style>
	.amcharts-export-menu .export-main>a, .amcharts-export-menu .export-drawing>a, .amcharts-export-menu .export-delayed-capturing>a {
		width: 25px !important;
		height: 25px !important;
	}
	.no-show{
		display:none;
	}
    </style>
<div class="page" id="fanpage">
	<div class="row grey lighten-3 mb-0">
		<div class="col s12 m12 l12 page-title">

			<div class="header-content left">
				<h2>{{$fanpage->brand->name}} {{ $fanpage->name }}</h2>
				<h6><a href="{{ $fanpage->url }}">{{ $fanpage->url }}</a></h6>
				<div class="btn btn-primary blue darken-3" style="font-size: .8em; line-height: 22px; height: 22px;">Facebook</div>
				<div class="color-signs">
					<ul>
						<li class="card red darken-2">Post(s) sin analizar</li>
						<li class="card orange lighten-1">Post(s) por autenticar</li>
						<li class="card  cyan darken-1">Post(s) clasificados</li>
					</ul>
				</div>
			</div>

			<div class="right">
				<a class="add-brand right modal-trigger" href="#add-post">
					<i class="material-icons header">add</i>
				</a>
				<a class="add-brand right modal-trigger ml-10 tooltipped" data-position="bottom" data-delay="50" data-tooltip="Buscar publicaciones" href="{{ url('fanpage/'.$fanpage->id.'/scrap') }}">
					<i class="material-icons header green-text">loop</i>
				</a>
				<a class="add-brand right modal-trigger ml-10 tooltipped" data-position="bottom" data-delay="50" data-tooltip="Hot Posts" href="{{ url('fanpage/'.$fanpage->id.'/hot-posts') }}">
					<i class="material-icons header red-text">whatshot</i>
				</a>
				{{-- <a class="add-brand right modal-trigger ml-10 tooltipped" data-position="bottom" data-delay="50" data-tooltip="Comms panorama" href="#comms-panorama">
					<i class="material-icons header">pie_chart</i>
				</a> --}}
				<a class="add-brand right modal-trigger ml-10 tooltipped" data-position="bottom" data-delay="50" data-tooltip="Comms Panorama Section" href="{{ url('comms/discovery/Facebook/'.$fanpage->id) }}">
					<i class="material-icons header indigo-text">assignment</i>
				</a>

				<a class="add-brand right modal-trigger tooltipped" data-position="bottom" data-delay="50" data-tooltip="Agregar AdSet" href="#add-adset">
					+<i class="material-icons header v-middle">developer_board</i>
				</a>
			</div>
		</div>
	</div>

	<!-- Modal Structure -->
	<div id="add-post" class="modal modal-fixed-footer fanpage">

		<h2 class="title dark-blue-graph">Añadir Publicación</h2>
		<div class="modal-content">

			{!! Form::open(['route' => 'add_post', 'onSubmit' =>'submitForm()']) !!}

			<div class="row">


				<div class="col s12 m10 l10 offset-m1 offset-l1">
					<p>Existen publicaciones ocultas. Con esta funcionalidad podrás añadir una publicación manualmente. Ten presente solo añadir las publicaciónes de <a>{{$fanpage->url}}</a></p>

					<label for="name" class="col-md-4 control-label">URL de la publicación</label>

					{!! Form::url('url', 'https://www.facebook.com/BabyDoveMamaReal/posts/1825580551088282', array('class' => 'mb-5')) !!}
					{!! Form::hidden('fanpage_url', $fanpage->url, array('class' => 'm-0')) !!}

					@if ($errors->has('url'))
					@foreach($errors->get('url') as $error)
					<p style="color:#b71c1c;">{{ $error }}</p>
					@endforeach
					@endif

					{!!Form::submit('Agregar', ['class' => 'find btn btn-primary green m-0', 'style' => 'width:100%'] ) !!}
					<div id="scrap">
						<div class="progress mt-5">
							<div class="indeterminate"></div>
						</div>
					</div>

				</div>
			</div>

			<script type=text/javascript>
				function submitForm()
				{	
					$('.btn.green.find').hide();
					$('.progress').show();
				}

			</script>


			{!! Form::close() !!}


		</div>
		<div class="modal-footer">
			<a class="modal-action modal-close waves-effect waves-blue btn-flat">Cerrar</a>
		</div>
	</div>
	<style>
	.comms-chart{
		height: 300px;
	}
</style>
{{-- Modal Comms --}}
{{-- <div id="comms-panorama" class="modal" style="z-index: 1000;">

	<h2 class="title dark-blue-graph">Comms Panorama  (últimos 28 días)</h2>
	<div class="modal-content">
		<div class="row">
			<div class="col s12">
				<ul class="tabs tabs-fixed-width">
					<li class="tab"><a class="active" href="#comms_tab1">Tipo</a></li>
					<li class="tab"><a href="#comms_tab2">Balance</a></li>
					<li class="tab"><a href="#comms_tab3">Tareas</a></li>
					<li class="tab"><a href="#comms_tab4">Productos</a></li>
					<li class="tab"><a href="#comms_tab5">Temas</a></li>
					<li class="tab"><a href="#comms_tab6">Concursos</a></li>
				</ul>
			</div>
			<div id="comms_tab1" class="col s12" align="middle">
			
				<div class="comms-chart" id="comms-type"></div>
			</div>

			<div id="comms_tab2" class="col s12" align="middle">
				<h5> Balance del post </h5>
				<div class="comms-chart" id="comms-balance"></div>
			</div>

			<div id="comms_tab3" class="col s12" align="middle">
			
				<div class="comms-chart" id="comms-task"></div>
			</div>

			<div id="comms_tab4" class="col s12" align="middle">
				
				<div class="comms-chart" id="comms-product"></div>
			</div>

			<div id="comms_tab5" class="col s12" align="middle">
				
				<div class="comms-chart" id="comms-theme"></div>
			</div>

			<div id="comms_tab6" class="col s12" align="middle">
				
				<div class="comms-chart" id="comms-contest"></div>
			</div>
		</div>
		<br>
		
	</div>
	<div class="modal-footer">
		<a href="#!" class="modal-action modal-close waves-effect waves-blue btn-flat">Cerrar</a>
	</div>
</div> --}}
<div id="fan-state">
	<div class="fans-state col s12 m12 l12 white" v-for='data in allData'>
		<div  v-bind:id='"fancount-chart-" + data["name"]' class="fan-state-graph"></div>
	</div>
</div>
@include('utils.sections.scrapt-menu', ['element'=> $fanpage, 'modelName'=>'Fanpage', 'isPost'=>false])
<div class="row blue darken-3 mb-0">
	<div class="col s12 m12 l12 page-title">
		<p class="white-text">Hemos encontrado <strong>{{$posts->total()}} publicaciones</strong> en nuestra base de datos.</p>
		<div class="search">
			{!! Form::open(['route' => ['search_posts', $fanpage->id], 'method' => 'GET']) !!}
			<div class="row mb-0">
				<div class="col s12 m10 l10">
					{!!Form::text('query', null, ['class' => 'white-text mb-0', 'placeholder' => 'Buscar publicación...']) !!}
				</div>
				<div class="col s12 m2 l2">
					{!!Form::submit('Ir', ['class' => 'btn btn-primary green', 'style' => 'width:100%; margin-top: 3px;'] ) !!}
				</div>
			</div>
			{!! Form::close() !!}
		</div>			
	</div>
</div>


{{-- <div id="date-form" class="row formDateContainer" style="padding: 8px;">
	<div class="col s12 m6 l3">
		<input v-model="since" type="text" placeholder="Y-m-d">	
	</div>
	<div class="col s12 m6 l3">
		<input v-model="until" type="text" placeholder="Y-m-d">
	</div>
	<div class="col s12 m6 l3">
		<select v-model="interval" v-on:change="setDate(interval)" class="browser-default"  name="interval"   id="">
			<optgroup label="" >
				<option value="all">Todo</option>
			</optgroup>
			<optgroup label="" >
				<option value="hoy">hoy</option>
				<option value="day">El mismo Día</option>
			</optgroup>
			<optgroup label="">
				<option value="6">Semana anterior</option>
				<option value="28">28 días anteriores</option>
				<option value="30">30 días anteriores</option>
			</optgroup>
			<optgroup label="">
				<option value="90">trimestre anterior (90 días)</option>
				<option value="182">semestre anterior (182 días)</option>
				<option value="365">año anterior</option>
			</optgroup>
		</select>
	</div>	

	<div class="col s12 m6 l3">
		<a v-bind:href=" '{{ url('fanpage/'.$fanpage->id )}}' + getDates" class="btn indigo tooltipped" data-position="bottom" data-delay="50" data-tooltip="Para filtrar por fecha es importante la fecha final">
			Filtrar por fecha
		</a>
	</div>				
</div> --}}

@if(isset($since))
@include('consults.date-min',  array('route' =>'fanpage/'.$fanpage->id))
@endif

@if (Session::has('message'))
<div class="row red darken-4 animated fadeIn">
	<div class="col s12 m12 l12 page-title">

		<p class="white-text">{{ Session::get('message') }} </p>
	</div>

</div>
@endif
@if(session('success'))
<div class="col s12 m12 l12">
	<div class="light-green message animated fadeInLeftBig">
		<p class="white-text"> <i class="material-icons" style="margin-right: 5px;">check</i>{{ session('success') }} </p>
	</div>
</div>
@endif

</div>


<div class="container-m posts">
	
	<div class="row">
		@foreach($posts as $key => $post)
		
		@include('posts.post-min', ['user' => $user])
		{{-- <div class="post-card col s6 m6 l6">
			<div class="card-summary col s11 m11 l11">
				<div class="card row">

					<!- Dropdown Structure ->
					<ul id='dropdown{{$key}}' class='dropdown-content'>
						<li><a href="{{ url('posts/'.$post->id) }}"><i class="material-icons left">insert_chart</i> Métricas ({{ count($post->metrics) }})</a></li>
						<li><a href="{{ $post->permalink_url }}" target="_blank"><i class="material-icons left">link</i> Visitar</a></li>
					</ul> 
					<div class="col s12 m12 l12 no-padding">
						<a href="{{ url('posts/'.$post->id) }}">
							<div class="attachment" style="background-image: url({{$post->attachment}})">

								@if($post->type == 'photo')
								<div class="waves-effect waves-light btn green circle-icon"><i class="material-icons center">image</i></div>
								@endif
								@if($post->type == 'video')
								<div class="waves-effect waves-light btn red circle-icon"><i class="material-icons center">ondemand_video</i></div>
								@endif
								@if($post->type == 'status')
								<div class="waves-effect waves-light btn green circle-icon"><i class="material-icons center">format_quote</i></div>
								@endif
								@if($post->type == 'link')
								<div class="waves-effect waves-light btn blue circle-icon"><i class="material-icons center">insert_link</i></div>
								@endif


							</div>
						</a>
					</div>
					<div class="col s12 m12 l12 no-padding">
						<div class="content">
							<div class="date valign-wrapper">
								<a href="{{ url('posts/'.$post->id) }}">{{ $post->published }}</a>
								<a class='dropdown-button' href='#' data-activates='dropdown{{$key}}'><i class="material-icons">more_horiz</i></a>
							</div>

							@if(!is_null($post->content))
							<p class="no-margin">{{ $post->content}}</p>
							@else
							<p class="no-margin">Esta publicación no tiene una descripción.</p>
							@endif
						</div>
						<a class="cta flex" href="{{ url('posts/'.$post->id) }}">
							<div class="reactions flex">
								<img src="{{ asset('images/reactions.png') }}" alt="Reacciones"> {{ $post->latestMetric->reactions }}
							</div>

							<div class="comments flex">{{ $post->latestMetric->comments }} comentarios</div>

							<div class="shares flex">{{ $post->latestMetric->shares }} compartidos</div>
						</a>
					</div>
{{- 				<div class="cta">
					<a class="waves-effect waves-light btn dark-blue-graph metrics" href="{{ url('posts/'.$post->id) }}"><i class="material-icons left">insert_chart</i>Métricas ({{ count($post->metrics) }})</a>

					<a href="{{ $post->permalink_url }}" class="waves-effect waves-light btn blue darken-3" target="_blank"><i class="material-icons left">link</i>Visitar</a>

					@if($post->type == 'photo')
					<div class="waves-effect waves-light btn green circle-icon"><i class="material-icons center">image</i></div>
					@endif
					@if($post->type == 'video')
					<div class="waves-effect waves-light btn red circle-icon"><i class="material-icons center">ondemand_video</i></div>
					@endif
					@if($post->type == 'status')
					<div class="waves-effect waves-light btn green circle-icon"><i class="material-icons center">format_quote</i></div>
					@endif
					@if($post->type == 'link')
					<div class="waves-effect waves-light btn blue circle-icon"><i class="material-icons center">insert_link</i></div>
					@endif
				</div> -}}
			</div>
		</div>
		<div class="col s1 m1 l1  card-coms flex">
			<div class="coms-info-icon blue darken-3" onClick="showCommsFanpage('{{$post->id}}', 1)">
				<h4 class="white-text"><i class="fa fa-file-text-o" aria-hidden="true"></i> <span class="text-xs">Comms</span></h4>
			</div>
		</div>

		<div class="col s6 m6 l6 comms-summary">
			<i class="material-icons close-btn">close</i>	
			<div id="content{{$post->id}}" class="comms-summary-show">
			</div>
			<div id="section{{$post->id}}_1" class="hide ">
				<h2 class="center-align">Formulario Comms</h2>
				<div class="content-comms col s12 row">
					<form id="form-{{$post->id}}" action="{{ route('comms_register')}}" method="POST">
						{{ csrf_field() }}
						<input type="hidden" name="post_id" value="{{$post->id}}">
						<div class="input-comms col s6">
							<label for="tipo">Tipo de Post</label>
							<select name="type"   class='browser-default'>
								@if($post->type && $post->type!="")
								<option disabled selected value=""> {{$post->type}}</option>
								@else
								<option disabled selected value=""> Selecciona el tipo de Post</option>
								@endif
								@foreach(Config::get('constants.comms.type.facebook') as $id => $value) 
								<option value="{{$id}}"> {{$value}} </option>
								@endforeach
							</select>
						</div>
						<div class="input-comms col s6">
							<label for="balance">Balance E/F</label>
							<select name="balance" class='browser-default'>
								@if($post->balance && $post->balance!="")
								<option disabled selected value=""> {{$post->balance}}</option>
								@else
								<option disabled selected value=""> Selecciona el balance</option>
								@endif
								@foreach(Config::get('constants.comms.balance') as $id => $value) 
								<option value="{{$id}}"> {{$value}} </option>
								@endforeach
							</select>
						</div>
						<div class="input-comms col s6">
							<label for="tareas">Tareas de Comunicación</label>
							<select name="comunication_task"   class='browser-default'>
								@if($post->comunication && $post->comunication!="")
								<option disabled selected value=""> {{$post->comunication}}</option>
								@else
								<option disabled selected value=""> Selecciona la tarea</option>
								@endif
								@foreach(Config::get('constants.comms.comunication_task') as $id => $value) 
								<option value="{{$id}}"> {{$value}} </option>
								@endforeach
							</select>
						</div>
						<div class="input-comms col s6">  
							<label for="themes">Temas de Comunicación</label>
							<select name="comunication_theme"  onchange="showContest({{$post->id}})" class='browser-default' id='theme-{{$post->id}}' >
								@if($post->theme && $post->theme!="")
								<option disabled selected value=""> {{$post->theme}}</option>
								@else
								<option disabled selected value=""> Selecciona el tema</option>
								@endif
								@foreach(Config::get('constants.comms.comunication_theme') as $id => $value) 
								<option value="{{$id}}"> {{$value}} </option>
								@endforeach
							</select>
						</div>
						
						<div class="input-comms contest col s12 hide"  id='concursos-{{$post->id}}'>
							<div class="contest-select">
								
								<label for="themes">Concursos</label>
								<select name="contest"   class='browser-default'>

									@if($post->contests()->get()->count()>0)
									<option disabled selected value=""> {{$post->contests()->get()[0]->title}} </option>
									@else
									<option disabled selected value="">Selecciona el concurso</option>
									@endif

									@foreach($contests as $contest)
									<option value="{{$contest->id}}">{{$contest->title}}</option>
									@endforeach

									
								</select>
							</div>
							<a href="#addContest" class='modal-trigger'><span class='tooltipped' data-tooltip="Agregar Concurso"><i class="fa fa-plus-circle" aria-hidden="true"></i></span></a>
						</div>	
						<div class="input-comms col s12">  
							<label for="themes">Productos</label>
							<select name="product"   class='browser-default'>
								@if($post->product)
								<option disabled selected value=""> {{$post->product->name}} </option>
								@else
								<option disabled selected value=""> Selecciona el Producto</option>	
								@endif
								

								@foreach($products as $product)
								<option value="{{$product->id}}">{{$product->name}}</option>
								@endforeach

							</select>
						</div>

						{{- <input type="submit" value="Actualizar"> -}}
					</form>
					<div class="comms-submit col s6">
						<button class="find btn btn-primary green m-0" style="width:100%;" 
						onclick ="updateComms('{{$post->id}}')" >Actualizar</button>

						{{- {!!Form::submit('Agregar', ['class' => 'find btn btn-primary green m-0', 'style' => 'width:100%'] ) !!} -}}
					</div>	
				</div>
			</div>
			{{- <div id="section{{$post->id}}_2" class="hide">
				<h2 class="center-align">Balance Emocional/Funcional</h2>
			</div>
			<div id="section{{$post->id}}_3" class="hide">
				<h2 class="center-align">Tarea de Comunicación</h2>
			</div>
			<div id="section{{$post->id}}_4" class="hide">
				<h2 class="center-align">Tema</h2>
			</div>
			<div id="section{{$post->id}}_5" class="hide">
				<h2 class="center-align">Segmento/Nombre de Producto</h2>
			</div>
			<div id="section{{$post->id}}_6" class="hide">
				<h2 class="center-align">Camapaña</h2>
			</div> -}}
		</div>
	</div> --}}
	@endforeach 
</div>

<div class="modal modal-fixed-footer" id="addContest">
	<h2 class="title dark-blue-graph">Agregar Concurso</h2>
	<div class="modal-content">

		{!! Form::open(['route' => 'contest.store', 'onSubmit' =>'submitForm()']) !!}
		<div class="row">
			<div class="col s12 m10 l10 offset-m1 offset-l1">
				<label for="name" class="col-md-4 control-label">Nombre</label>
				{!! Form::text('name', 'Nombre del Concurso', array('class' => 'mb-5')) !!}
				<label for="text" class="col-md-4 control-label">Mecánica</label>
				{!! Form::textarea('mechanics', 'Mecánica del Concurso', array('class' => 'mb-5')) !!}
				<label for="text" class="col-md-4 control-label">Premio</label>
				{!! Form::text('prize', 'Premio del Concurso', array('class' => 'mb-5')) !!}
				<label for="date" class="col-md-4 control-label">Desde</label>
				{!! Form::date('from-date', \Carbon\Carbon::now()); !!}
				<label for="date" class="col-md-4 control-label">Hasta</label>
				{!! Form::date('to-date', \Carbon\Carbon::now()); !!}
				{!! Form::hidden('brand_id', $fanpage->brand->id) !!}
				@if ($errors->has('url'))
				@foreach($errors->get('url') as $error)
				<p style="color:#b71c1c;">{{ $error }}</p>
				@endforeach
				@endif

				{!!Form::submit('Agregar', ['class' => 'find btn btn-primary green m-0', 'style' => 'width:100%'] ) !!}
				<div id="scrap">
					<div class="progress mt-5">
						<div class="indeterminate"></div>
					</div>
				</div>
			</div>
		</div>
		<script type=text/javascript>
			function submitForm()
			{	
				$('.btn.green.find').hide();
				$('.progress').show();
			}

		</script>
		{!! Form::close() !!}


	</div>
	<div class="modal-footer">
		<a href="#" class="modal-action modal-close waves-effect waves-blue btn-flat">Cerrar</a>
	</div>
</div>	


<div id="add-adset" class="modal modal-fixed-footer adsets">

	<h2 class="title dark-blue-graph">Añadir AdSet</h2>
	<div class="modal-content">
		<div class="row">
			@include('manage_sections.adset.create-adset')
		</div>
	</div>
	<div class="modal-footer">
		<a href="#" class="modal-action modal-close waves-effect waves-blue btn-flat">Cerrar</a>
	</div>
</div>


<div class="row">
	<div class="col s12 m12 l12">
		{{ $posts->links() }}
	</div>
</div>

</div>


@endsection