@extends('layouts.app')
 <?php $user = (Auth::check())? Auth::user(): false; ?>
@section('content')

<div class="page" id="fanpage">
	<div class="row grey lighten-3 mb-0">
		<div class="col s12 m12 l12 page-title">

			<div class="header-content left">
				<h2>{{ $fanpage->brand->name }} {{ $fanpage->name }}</h2>
			</div>

		</div>
	</div>

	<div class="row blue darken-3 mb-0">
	<div class="col s12 m12 l12 page-title">

		<p class="white-text">En esta sección encontrarás las <strong>publicaciones</strong> más activas del día.</p>
	</div>

</div>

</div>


<div class="container-m posts">
	<div class="row">
		
		@if($hot_posts->isEmpty())
		No hay interacciones en ninguna de las publicaciones de esta Fan Page.<br/><br/>

		<a href="{{ url('/fanpage/'.$fanpage->id)}}">Regresar</a>
		@else
		@foreach($hot_posts as $key => $metric)

		<div class="col s12 m4 l4">
			<div class="card">

				<!-- Dropdown Structure -->
				<ul id='dropdown{{$key}}' class='dropdown-content'>
					<li><a href="{{ url('posts/'.$metric->post->id) }}"><i class="material-icons left">insert_chart</i> Métricas ({{ count($metric->post->metrics) }})</a></li>
					<li><a href="{{ $metric->post->permalink_url }}" target="_blank"><i class="material-icons left">link</i> Visitar</a></li>
				</ul>
				<a href="{{ url('posts/'.$metric->post->id) }}">
					<div class="attachment" style="background-image: url({{$metric->post->attachment}})">

						@if($metric->post->type == 'photo')
						<div class="waves-effect waves-light btn green circle-icon"><i class="material-icons center">image</i></div>
						@endif
						@if($metric->post->type == 'video')
						<div class="waves-effect waves-light btn red circle-icon"><i class="material-icons center">ondemand_video</i></div>
						@endif
						@if($metric->post->type == 'status')
						<div class="waves-effect waves-light btn green circle-icon"><i class="material-icons center">format_quote</i></div>
						@endif
						@if($metric->post->type == 'link')
						<div class="waves-effect waves-light btn blue circle-icon"><i class="material-icons center">insert_link</i></div>
						@endif


					</div>
				</a>


				<div class="content">

					<div class="date valign-wrapper">
						<a href="{{ url('posts/'.$metric->post->id) }}">{{ $metric->post->published }}</a>
						<div>
						
						<span class="new badge">{{ $metric->count }}</span>
						<a class='dropdown-button' href='#' data-activates='dropdown{{$key}}'><i class="material-icons">more_horiz</i></a>
						</div>
					</div>
                   <div class="post-text">
                   	@if(!is_null($metric->post->content))
					<p class="no-margin">{{ $metric->post->content}}</p>
					@else
					<p class="no-margin">Esta publicación no tiene una descripción.</p>
					@endif
                   </div>
				</div>

				<a class="cta flex" href="{{ url('posts/'.$metric->post->id) }}">
					<div class="reactions flex">
						<img src="{{ asset('images/reactions.png') }}" alt="Reacciones"> {{ $metric->post->latestMetric->reactions }}
					</div>

					<div class="comments flex">{{ $metric->post->latestMetric->comments }} comentarios</div>

					<div class="shares flex">{{ $metric->post->latestMetric->shares }} compartidos</div>
				</a>
				
			</div>
		</div>
		@endforeach 
		@endif



	</div>


</div>

<div class="row">
		<div class="col s12 m12 l12">
			{{ $hot_posts->links() }}
		</div>
	</div>

</div>


@endsection