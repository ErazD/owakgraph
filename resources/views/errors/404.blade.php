@extends('layouts.app')

@section('content')

<div class="row grey lighten-3 mb-0">
	<div class="col s12 m12 l12 page-title">
		<h2 class="breadcrumbs-title left"><i>Error</i> 404</h2>
	</div>
</div>

<div class="row red darken-4 mb-0">
	<div class="col s12 m12 l12 page-title">

		<p class="white-text flex" style="display:flex;"><i class="material-icons mr-5">error_outline</i> La página que buscas no existe o ha cambiado su ubicación.</p>

	</div>

</div>

@stop