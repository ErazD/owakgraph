{{-- atributes required
	string $nameVueApp: nombre de vue.js
	--}}
	<?php 
	$infoCategories = [
	'type' => ['slug'=>'type', 'name' => 'Tipo', 'route'=>'facebook'],
	'balance' => ['slug'=>'balance', 'name' => 'Balance', 'route'=>false],
	'comunication_task' => ['slug'=>'comunication_task', 'name' => 'Tarea de comunicación', 'route'=>false],
	'comunication_theme' => ['slug'=>'comunication_theme', 'name' => 'Tema de comunicación', 'route'=>false],
	'google_micro_moment' => ['slug'=>'google_micro_moment', 'name' => 'Micro Momento', 'route'=>false],
	'business-objective' => ['slug'=>'business-objective', 'name' => 'Objetivo de negocio', 'route'=>false],
	'contest' => ['slug'=>'contest', 'name' => 'Concurso', 'route'=>false],
	'segment' => ['slug'=>'segment', 'name' => 'Segmento', 'route'=>false],
	'product' => ['slug'=>'product', 'name' => 'Producto', 'route'=>false],
	'canasta' => ['slug'=>'canasta', 'name' => 'Categoría', 'route'=>false],
	'influencer' => ['slug'=>'influencer', 'name' => 'Influencer', 'route'=>false],
	'pauta' => ['slug'=>'pauta', 'name' => 'Pauta', 'route'=>false]
	];
	?>

	<script src="{{ asset('js/filter-comms-selector.js') }}"></script>
	
	<script>

		window.addEventListener('load', loadInitialize);
		function loadInitialize(){
			initializeFilter({{$nameVueApp}});
			initializeCommsOptions();
		}
		function initializeCommsOptions(){		
			var optLists ={};
		// Tipo, Balance, Tarea de comunicación, Tema de comunicación, Micro Momento, Objetivo de negocio
		@foreach(Config::get('constants.comms') as $type => $options) 
		optLists ={};
		<?php $list  = ($infoCategories[$type]['route'])? $options[$infoCategories[$type]['route']] : $options;?>
		@foreach( $list as $id => $value) 
		optLists = addOptionToModelList('{{$value['name']}}', '{{$value['slug']}}', optLists);
		@endforeach
		optLists = addOptionToModelList(' No definido', 'null', optLists);
		// TODO: // optLists = addOptionToModelList('No definido', 'null', optLists);
		addCategoryToCommsFilter('{{$infoCategories[$type]['name']}}', '{{$infoCategories[$type]['slug']}}', optLists);
		@endforeach
		
		// Concurso
		{{--  optLists ={};
		@foreach(App\Contest::get() as $id => $value) 
		optLists = addOptionToModelList('{{$value['title']}}', '{{$value['id']}}', optLists);
		@endforeach 
		addCategoryToCommsFilter('{{$infoCategories['contest']['name']}}', '{{$infoCategories['contest']['slug']}}', optLists);  --}}

		// Producto
		{{-- optLists ={};
		@foreach(App\Product::get() as $id => $value) 
		optLists = addOptionToModelList('{{$value->getSegementProductname()}}', '{{$value['id']}}', optLists);
		@endforeach
		addCategoryToCommsFilter('{{$infoCategories['product']['name']}}', '{{$infoCategories['product']['slug']}}', optLists);--}}
		
		// Canasta
		optLists ={};
		@foreach(App\Canasta::get() as $id => $value) 
		optLists = addOptionToModelList('{{$value['name']}}', '{{$value['id']}}', optLists);
		@endforeach
		optLists = addOptionToModelList(' No definido', 'null', optLists);
		addCategoryToCommsFilter('{{$infoCategories['canasta']['name']}}', '{{$infoCategories['canasta']['slug']}}', optLists);
		// Influencer

		// Pauta
		{{-- optLists ={};
		optLists = addOptionToModelList('Con Pauta', '1', optLists);
		optLists = addOptionToModelList('Sin Pauta', '0', optLists);
		addCategoryToCommsFilter('{{$infoCategories['pauta']['name']}}', '{{$infoCategories['pauta']['slug']}}', optLists);		--}}

		loadSelectedFilters();
		}

		/**
		* Carga los filtros desde url
		* Postcondition: los valores de filtros seleccionados se han actualizado
		*/
	function loadSelectedFilters() {
		@if(isset($_GET) && isset($_GET['filtro_comms']))
		<?php
		$isConjuction = (isset($_GET['filtro_comms_operation']))? $_GET['filtro_comms_operation']: '';
		$allFilter = $_GET['filtro_comms'];
		$arrFilter = (strpos($allFilter, ';'))? explode(';', $allFilter) : [$allFilter];

		?>
		var conj = "{{$isConjuction}}";

		{{$nameVueApp}}.extra.commsFilter.operation = (conj != "")? conj:'conjunction';

		@foreach($arrFilter as $index => $values)
		var infFilter = '{{$values}}'.split(":");
		var categorySlug = infFilter[0];
		var values = infFilter[1];
		values = (values.includes(","))? values.split(","): [values];
		$.each(values, function(key, optionSlug){
			{{$nameVueApp}}.addSelectedCommsFilter(categorySlug, optionSlug);
		});
		@endforeach
		@endif
	}

	{{-- FILTROS VARIABLES (PRODUCTOS Y CONCURSOS)  --}}
	/** 
	* Recorre las listas de concursos y productos desde php y crea en javascript los diccionarios que contienen su informacion en una estructura que se pueda filtrar. 
	*/
<?php		
			// PRODUCTOS
		    $allProducts = []; 
		    foreach (App\Product::get() as $prdk => $product) {
		        $brand = $product->brand;
		        $canastas = ($brand)? $brand->canastas: null;
		        $canastas = ($canastas)? $canastas: [];
		        $strCanastas = "";
		        $strCategorias = "";
		        $strVerticals = "";
		        foreach ($canastas as $cank => $canasta) {
		            $categoria = $canasta->categoria;
		            $vertical = ($categoria)?$categoria->vertical: null;
		            $strCanastas .= ' '.$canasta->id;
		            $strCategorias .= ($categoria)? ' '.$categoria->id: '';
		            $strVerticals .= ($vertical)? ' '.$vertical->id: '';
		        }
		        $slug = "".$product->id;
		        $infoProducto = [
		            	'name' => $product->getSegementProductname(),
		            	'slug' => $slug,
		            	'vertical' => ($strVerticals != "")? ' '.$strVerticals.' ': 'none',
		                'category' => ($strCategorias != "")? ' '.$strCategorias.' ': 'none',
		                'basket' => ($strCanastas != "")? ' '.$strCanastas.' ': 'none',
		                'brand' => ($brand)? ' '.$brand->id.' ': 'none',
		                
		                'Vertical' => ($strVerticals != "")? ' '.$strVerticals.' ': 'none',
		                'Categoria' => ($strCategorias != "")? ' '.$strCategorias.' ': 'none',
		                'Canasta' => ($strCanastas != "")? ' '.$strCanastas.' ': 'none',
		                'Brand' => ($brand)? ' '.$brand->id.' ': 'none'
		            ];
		            $allProducts[$slug] = $infoProducto;
		    }
		    
		    // uasort($allProducts, function($a, $b) {
		    // 	// dd($a, $b);
		    // 	return strcmp($a['name'], $b['name']);
		    // });
		    $allProducts['null'] = ['name' => 'No Definido', 'slug' => 'null'];
		   	echo 'allProducts = ';
		    echo json_encode($allProducts); 
		    echo ';';

		    // SEGMENTOS
		    $allSegments = []; 
		    foreach (App\Segmento::get() as $segk => $segment) {
		        $canasta = $segment->canasta;
	            $categoria = ($canasta)?$canasta->categoria: null;
	            $vertical = ($categoria)?$categoria->vertical: null;
	            $strCanastas = ' '.$canasta->id.' ';
	            $strCategorias = ($categoria)? ' '.$categoria->id: '';
	            $strVerticals = ($vertical)? ' '.$vertical->id: '';
		        $strBrands = '';
		        foreach ($segment->canasta->brands as $brndk => $brand) {
					$strBrands .= ' '.$brand->id.' ';
				}

		        $slug = "".$segment->id;
		        $infoSegmento = [
		            	'name' => $segment->name,
		            	'slug' => $slug,
		            	'vertical' => ($strVerticals != "")? ' '.$strVerticals.' ': 'none',
		                'category' => ($strCategorias != "")? ' '.$strCategorias.' ': 'none',
		                'basket' => ($strCanastas != "")? ' '.$strCanastas.' ': 'none',
		                'brand' => ($strBrands != "")? ' '.$strBrands.' ': 'none',
		                
		                'Vertical' => ($strVerticals != "")? ' '.$strVerticals.' ': 'none',
		                'Categoria' => ($strCategorias != "")? ' '.$strCategorias.' ': 'none',
		                'Canasta' => ($strCanastas != "")? ' '.$strCanastas.' ': 'none',
		                'Brand' => ($strBrands != "")? ' '.$strBrands.' ': 'none'
		            ];
		            $allSegments[$slug] = $infoSegmento;
		    }
		    
		    // uasort($allSegments, function($a, $b) {
		    // 	// dd($a, $b);
		    // 	return strcmp($a['name'], $b['name']);
		    // });
		    $allSegments['null'] = ['name' => 'No Definido', 'slug' => 'null'];
		   	echo 'allSegments = ';
		    echo json_encode($allSegments); 
		    echo ';';


		    // CONCURSOS 
		    $allContests = []; 
		    foreach (App\Contest::get() as $cnk => $contest) {
		        $brand = $contest->brand;
		        $canastas = ($brand)? $brand->canastas: null;
		        $canastas = ($canastas)? $canastas: [];
		       	$strCanastas = "";
		        $strCategorias = "";
		        $strVerticals = "";
		        foreach ($canastas as $cank => $canasta) {
			            $categoria = $canasta->categoria;
			            $vertical = ($categoria)?$categoria->vertical: null;
			            $strCanastas .= ' '.$canasta->id;
			            $strCategorias .= ($categoria)? ' '.$categoria->id: '';
			            $strVerticals .= ($vertical)? ' '.$vertical->id: '';
		        }
		        $slug = ''.$contest->id;
		            $infoContest = [
		            	'name' => $contest->title,
		            	'slug' => $slug,
		            	'vertical' => ($strVerticals != "")? ' '.$strVerticals: 'none',
		                'category' => ($strCategorias != "")? ' '.$strCategorias: 'none',
		                'basket' => ($strCanastas != "")? ' '.$strCanastas: 'none',
		                'brand' => ($brand)? ' '.$brand->id: 'none',
		                'Vertical' => ($strVerticals != "")? ' '.$strVerticals: 'none',
		                'Categoria' => ($strCategorias != "")? ' '.$strCategorias: 'none',
		                'Canasta' => ($strCanastas != "")? ' '.$strCanastas: 'none',
		                'Brand' => ($brand)? ' '.$brand->id: 'none'
		            ];		            
		            $allContests[$slug] = $infoContest;
		        
		    }
		    echo 'allContests = ';
		    echo json_encode($allContests); 
		    echo ';';
		?>
{{--  FILTROS SELECCIONADOS  --}}

	/**
	* Oculta los elementos del tipo de comms seleccionado que no se encuentran dentro del filtro.
	* @param {string} typeElement: slug del elemento comms a filtrar.
	* Postcondition: Se han ocultado en el html los elementos que no cumplen con el valor filtro para el tipo de comms seleccionado.
	*/
	function filterFunction(typeElement){
		var input, filter, container, list;
    	input = document.getElementById("filter-"+typeElement);
    	filter = input.value.toUpperCase(); // valor a filtrar
    	container = document.getElementById("expand-"+typeElement);
    	list = container.getElementsByTagName("label"); // Lista de las opciones para el valor de comms
    	for (var i = 0; i < list.length; i++) {
    		var elem = list[i];
    		var val = elem.getElementsByClassName("option-name")[0].innerHTML.toUpperCase(); // Valor de la opción
    		if(val.indexOf(filter) > -1){
    			elem.style.display = "";
    		}else{
    			elem.style.display = "none";
    		}
    	}
	}

</script>

<div class="row" v-if="extra.commsFilter">
	<div class="row">
		<div class="col s12 m12">
			<h6>Filtros Comms {{-- < %extra.commsFilter%> --}}</h6>
			<h6 style="display:none"><%extra.commsFilter.paramsURL%></h6>
		</div>

		<ul class="filter-screen col s12 m6 l3 xl3" v-for='type in extra.commsFilter.all'>
			<li>
			<div class="collapsible-header" v-bind:id="'header-'+type.slug" @click='showFilterContent(type.slug)'><%type.name%> <i class="fa fa-caret-down" aria-hidden="true"></i></div>
			<div class="collapsible-body" v-bind:id="'expand-'+type.slug">
					<input v-bind:id="'filter-'+type.slug" type="text" v-bind:placeholder="'Filtrar '+type.name+'...'" @keyup="filterFunction(type.slug)">
					<label v-for="element in getOrderObjectListByField(type.list)" v-bind:name="'expand-'+type.slug" class='container-check'><span class="option-name"><%element.name%></span>
						<input type="checkbox" v-bind:id="'filter-'+element.slug+type.slug" v-bind:value='type.slug' @click='selectCommsFilter(element.slug, type.slug)'>
						<span class="checkmark"></span>	
					</label>	
				</div>	 
			</li>
		</ul>
	{{-- 	<div class="col s12 m6 l3 xl3">
			<label for="filter-operation">Producto</label>
			
		</div> --}}
		<div class="col s12 m6 l3 xl3">
			<label for="filter-operation">Operación:</label>
			<select class="browser-default" name="filter-operation" id="filter-operation" v-model="extra.commsFilter.operation" v-on:change="updateParamsUrl()">
				<option value="conjunction">Conjunción (Los resultados deberán coincidir con todos los filtros)</option>
				<option value="disjunction">Disyunción (Los resultados deberán coincidir con al menos un filtro)</option>
			</select>
		</div>
	</div>

	<div class="row">
		<div class="col s12 m12">
			<h6>Opciones seleccionadas:</h6>
		</div>
		<div class="col s12 m12" v-for="(options, type_slug) in extra.commsFilter.selected">
			<h6><%extra.commsFilter.all[type_slug].name%>:</h6>
			<div class="row">
				<span class="filter-tag" v-for="(opt, opt_slug) in options" v-on:click="deselectCommsFilter(type_slug, opt_slug)"> <%opt.name%></span>
			</div>
		</div>
	</div>
</div>