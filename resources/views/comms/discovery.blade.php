@extends('layouts.app')
<?php  $user = (Auth::check())? Auth::user(): false;  ?>
@section('scripts')

<script src="{{ asset('js/vue.min.js') }}"></script>
<script src="{{ asset('js/appComms.js') }}"></script>
<script src="{{ asset('js/date-selector.js') }}"></script>
{{-- <script src="{{ asset('js/main.js') }}"></script> --}}
<style>

	.select-wrapper+label {
		position: relative;
		top:0px;
	}
	
	/*COUNTER*/
	.performance-u{
		width: 100%;
		padding: 15px 30px 15px 30px !important;
		margin: -35px 0px 0px;
	}
	.performance-u ul{
		display:none;
	}
	.performance-u .performance-title:hover{
		cursor:pointer;
	}


	.performance-u .performance-title h2{
		font-size:1.5em !important;
		margin:15px 0px 15px 0px;
	}
	.performance-u .performance-title p{
		margin-top:10px;
		display:none;
	}
	.performance-u li{
		position:relative;
	}  
	.performance-u li .detail:hover{
		cursor:pointer;
	}
	.performance-u .expand-list{
		display:none;
	}

	.performance-u .expand-list p{
		font-size: .8em !important;
	}
	.performance-u li{
		display:inline-block;
	}


	table.result-table{
		border-style: solid;
		border-width: 2px;
        margin-top: -10px;
		margin-bottom: : 30px;
	}

	table.result-table th{
		color: white;
		background-color: #0277bd; 
	}
	table.result-table th, table.result-table td{
		text-align: center;
		border-style: solid;
		border-width: 1px;
		border-color: #ffffff99;
		padding: 8px;
	} 
	table.result-table th.selected{
		background-color: #b0bec5;
		color: #37474f;
	}
	table.result-table td.selected{
		background-color: #eceff1;
	}
</style>
<script>
    @if($allElements)
    function getAllElements(){
		var all = [];
		var temps = [];		
		@foreach($allElements['fanpages'] as $fanpage)
		var dict = {'id': {{$fanpage->id}}, 'name': '{{html_entity_decode($fanpage->brand->name, ENT_QUOTES | ENT_XML1, 'UTF-8')}}  {{$fanpage->name}}', 'slug': '{{$fanpage->id}}'};
		temps.push(dict);
		@endforeach
		all['fanpage'] = temps;
	
		temps = [];		
		@foreach($allElements['brands'] as $brand)
		var dict = {'id': {{$brand->id}}, 'name': '{{html_entity_decode($brand->name, ENT_QUOTES | ENT_XML1, 'UTF-8')}}', 'slug': '{{$brand->slug}}'};
		temps.push(dict);
		@endforeach
		all['marca'] = temps;
		
		temps = [];		
		@foreach($allElements['baskets'] as $basket)
		var dict = {'id': {{$basket->id}}, 'name': '{{$basket->name}}', 'slug': '{{$basket->slug}}'};
		temps.push(dict);
		@endforeach
		all['canasta'] = temps;
		
		temps = [];		
		@foreach($allElements['adsets'] as $adset)
		var dict = {'id': {{$adset->id}}, 'name': '{{$adset->name}}', 'slug': '{{$adset->slug}}'};
		temps.push(dict);
		@endforeach
		all['adsets'] = temps;

		return all;
	}
   
    filterForm.allElements = getAllElements();

    @endif 

	var formComms=true;
	//comms_register = "{ route('comms_register')}}";
	comms_register = "{{route('two-factor.store')}}";
	dateSelectorApp.since = "{{$since}}";
	dateSelectorApp.until = "{{$until}}";

	var tab = window.location.href;
	var tb = "no-comms";

	if(tab.includes("&")){
		tab = tab.split("&");
		ind = tab.length-1;
		tab = tab[ind].split("=")[0];
	}

	if(tab=="pendientes"){
		tb = "no-comms";
	}else if(tab=="completos"){
		tb = "comms";
	}


	/*
	}
	comms
	no-comms
	two-factor
	*/
	$(document).ready(function(){
		$('ul.tabs').tabs('select_tab', tb);
		// var counter = 0;
		// var container = $('.performance-u');
		// var containerB = $('.expand-list');

		// $('.performance-title').on('click', function(){
		// 	$('.performance-title p').css('display', 'block');
		// 	$('.performance-u ul').css('display', 'block');
		// 	counter++;
		// 	console.log(counter);
		// 	if(counter == 2){
		// 		$('.performance-title p').css('display', 'none');
		// 		$('.performance-u ul').css('display', 'none');
		// 		counter = 0;
		// 		console.log(counter);	
		// 	}
		// });
		// $(document).mouseup(function (e){
		// 	if (!container.is(e.target) && container.has(e.target).length === 0) 
		// 	{
		// 		$('.performance-title p').css('display', 'none');
		// 		$('.performance-u ul').css('display', 'none');
				
		// 		counter = 0;
		// 	}
		// 	if (!containerB.is(e.target) && containerB.has(e.target).length === 0) 
		// 	{
		// 		containerB.css('display', 'none');
		// 	}
		// });
		// $('.detail').click(function(){
		// 	$(this).parent('li').find('.expand-list').toggle();
		// });
	});

</script>
@stop

@section('content')
<style>
	.select-wrapper+label {
		position: relative;
		top:0px;
	}
</style>
@php
$postBlade = '';
$postSocial = strtolower($social_network);

switch($postSocial){
	case 'facebook':
	$postBlade = 'posts.post-min';
	break;
	case 'youtube':
	$postBlade = 'youtube.videos.video-min';
	break;
	case 'instagram':
    $postBlade = 'instagram.posts.post-min';  
	break;
}

@endphp

<div class="page" id="fanpage">
	<div class="row grey lighten-3 mb-0">
		<div class="col s12 m12 l12 page-title">
			<div class="header-content left">
				@if($fanpage != "all")
				<a href="{{ url('fanpage/'.$fanpage->id) }}"><h2>{{$title}} ({{$posts["no_comms"]->total()}} Posts pendientes)</h2></a>
				<h6><a href="{{ $fanpage->url }}">{{ $fanpage->url }}</a></h6>
				<div class="btn btn-primary blue darken-3" style="font-size: .8em; line-height: 22px; height: 22px;">Facebook</div>
				@else
				<h2>Comms Discovery ({{ $posts["no_comms"]->total() }} Posts pendientes)</h2>

				@endif
				<div class="color-signs">
					<ul>
						<li class="card red darken-2">Post(s) sin analizar</li>
						<li class="card orange lighten-1">Post(s) por autenticar</li>
						<li class="card  cyan darken-1">Post(s) clasificados y aprobados</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="right">
			{{-- @if($fanpage != "all")
			
			@endif --}}
		</div>

		@if(isset($userPoints))
		<div class="performance-u col s12 m12 l12">
			<div class="performance-title">
				<h2>Rendimiento por usuario</h2>
				<p>(El puntaje mostrado representa el rendimiento total del usuario calficiando las tareas de comunicación)</p>	
			</div>
			<hr>
			<table class='result-table'>
				<tr>
					<th colspan="2"> General </th>
					<th colspan="8"> Discrepancias </th>
				</tr>
				<tr>
					<th>Nombre</th> 
					<th>Posts calificados</th> 
					<th>Total</th> 
					<th>Tipo</th>
					<th>Balance E/F </th>
					<th>Tema de Comunicación </th>
					<th>Tarea de Comunicación </th>
					<th>Producto </th>
					<th>Concurso </th>
					<th>Influencer </th>
				</tr>	
				@foreach($userPoints as $userPoint)
				<tr>
					<td>{{$userPoint['name']}}</td>
					<td>{{$userPoint['complete_posts']}}</td>
					<td>{{number_format(70000 - $userPoint['total'],0,".",".") }}</td>
					<td>{{number_format(10000 -$userPoint['ratings_type'],0,".",".")}}</td>
					<td>{{number_format(10000 -$userPoint['ratings_balance'],0,".",".")}}</td>
					<td>{{number_format(10000 -$userPoint['ratings_communication_theme'],0,".",".")}}</td>
					<td>{{number_format(10000 -$userPoint['ratings_communication_task'],0,".",".")}}</td>
					<td>{{number_format(10000 -$userPoint['ratings_product'],0,".",".")}}</td>
					<td>{{number_format(10000 -$userPoint['ratings_contest'],0,".",".")}}</td>
					<td>{{number_format(10000 -$userPoint['ratings_influencer'],0,".",".")}}</td>
				</tr>
				@endforeach	
			</table>
		</div>
		@endif

	</div>
	<div class="row blue darken-3 mb-0">
		<div class="col s12 m12 l12 page-title">
			<p class="white-text">En esta sección se consulta el <strong>estado de los comms</strong> de los posts.</p>
		</div>
	</div>
	{{-- <div id="date-form" class="row formDateContainer m-0" style="padding: 8px;">
	<div class="col s12 m6 l3">
		<input v-model="since" type="text" placeholder="Y-m-d" class="m-0">	
	</div>
	<div class="col s12 m6 l3">
		<input v-model="until" type="text" placeholder="Y-m-d" class="m-0">
	</div>
	<div class="col s12 m6 l3">
		<select v-model="interval" v-on:change="setDate(interval)" class="browser-default"  name="interval"   id="">
			<optgroup label="" >
				<option value="all">Todo</option>
			</optgroup>
			<optgroup label="" >
				<option value="hoy">hoy</option>
				<option value="day">El mismo Día</option>
			</optgroup>
			<optgroup label="">
				<option value="6">Semana anterior</option>
				<option value="28">28 días anteriores</option>
				<option value="30">30 días anteriores</option>
			</optgroup>
			<optgroup label="">
				<option value="90">trimestre anterior (90 días)</option>
				<option value="182">semestre anterior (182 días)</option>
				<option value="365">año anterior</option>
			</optgroup>
		</select>
	</div>	

	<div class="col s12 m6 l3">
		<a v-bind:href=" '{{ url('comms/discovery/'.(($fanpage != "all")? $fanpage->id:$fanpage) )}}' + getDates" class="btn indigo tooltipped" data-position="bottom" data-delay="50" data-tooltip="Para filtrar por fecha es importante la fecha final">
			Filtrar por fecha
		</a>
	</div>				
</div> --}}
@include('consults.date-min', array('route' =>'comms/discovery/'.(($fanpage == 'all')? $fanpage: $fanpage->id)))
</div>
 @if($allElements)
<div class="filtros-graph comms-filter row">
	<div class="col s4">
<label for="first_name">Filtrar Publicaciones (Aplica para los elementos visibles en la pagina actual)</label>
	<select class="browser-default" v-model="filterType" name="filterType" id="model-type">
		{{-- <option value="Vertical">Vertical</option>
		<option value="Categoria">Categoría</option> --}}
		<option value="canasta">Categoría</option>
		<option value="marca">Marca</option>
		<option value="fanpage">Fanpage</option>
		<option value="adset">Adset</option>
	</select>
	
</div>
<div class="col s4">
	<select class='adjust-list browser-default' v-model="pedido_id" list="lista_ids" name="pedido_id" placeholder="Id del elemento a buscar" id="model-id" @change='filterPosts()'>
	    <option value='todos'>Mostrar todo</option>
		<option v-for="option in allElements[filterType]" v-bind:value='option.id'><%option.name%></option>
	</select>
</div>
{{-- <div class="col s4">
<button class='consult-btn btn'  v-on:click="filterPosts()">Consultar</button>
</div>	 --}}	
</div>		
@endif

<ul id="tabs-swipe-demo" class="tabs tabs-fixed-width">
	<li class="tab col s3"><a href="#no-comms">Comms Pendientes</a></li>
	<li class="tab col s3"><a href="#comms">Comms Completos</a></li>    
	{{-- <li class="tab col s3"><a href="#two-factor">Two factor Authentification</a></li> --}}
</ul>

<div id="comms" class="col s12">
	<div class="container-m posts">
		<div class="row">		
			@if($posts["comms"]->isEmpty())
			No hay interacciones en ninguna de las publicaciones de esta Fan Page.<br/><br/>
			<a href="{{ url(($fanpage != 'all')? '/fanpage/'.$fanpage->id : '/')}}">Regresar</a>
			@else
				@foreach($posts["comms"] as $key => $post)
					@include($postBlade, ['user' => $user])						
				@endforeach
			@endif
		</div>
	</div>
	<div class="row">
		<div class="col s12 m12 l12">
			{{ $posts["comms"]->appends(['pendientes' => $posts["no_comms"]->currentPage()])->links() }}
		</div>
	</div>
</div>
<div id="no-comms" class="col s12">
	<div class="container-m posts">
		<div class="row">		
			@if($posts["no_comms"]->isEmpty())
			No hay interacciones en ninguna de las publicaciones de esta Fan Page.<br/><br/>
			<a href="{{ url(($fanpage != 'all')? '/fanpage/'.$fanpage->id : '/')}}">Regresar</a>
			@else
				@foreach($posts["no_comms"] as $key => $post)
					@include($postBlade)												
				@endforeach
			@endif
		</div>
	</div>

	<div class="row">
		<div class="col s12 m12 l12">
			{{ $posts["no_comms"]->appends(['completos' => $posts["comms"]->currentPage()])->links() }}
		</div>
	</div>
</div>

<div id="two-factor" class="col s12 green"></div>




</div>



<div class="modal modal-fixed-footer" id="addContest">
	<h2 class="title dark-blue-graph">Agregar Concurso</h2>
	<div class="modal-content">

		{!! Form::open(['route' => 'contest.store', 'onSubmit' =>'submitForm()']) !!}
		<div class="row">
			<div class="col s12 m10 l10 offset-m1 offset-l1">
				@if($fanpage == 'all')
				<?php 
				$brands_ids =[];
				$brns = App\Brand::get();
				foreach ($brns as $key => $brn) {
					$brands_ids[$brn->id] = $brn->name;
				}
				?>
				<label for="brand_id" class="col-md-4 control-label">Selecciona una marca</label>
				{!! Form::select('brand_id', $brands_ids, null, ['placeholder' => 'Selecciona una marca', 'required'=>'required']) !!}
				@else	
				{!! Form::hidden('brand_id', $fanpage->brand->id) !!}
				@endif

				<label for="name" class="col-md-4 control-label">Nombre</label>
				{!! Form::text('name', 'Nombre del Concurso', array('class' => 'mb-5')) !!}
				<label for="text" class="col-md-4 control-label">Mecánica</label>
				{!! Form::textarea('mechanics', 'Mecánica del Concurso', array('class' => 'mb-5')) !!}
				<label for="text" class="col-md-4 control-label">Premio</label>
				{!! Form::text('prize', 'Premio del Concurso', array('class' => 'mb-5')) !!}
				<label for="from-date" class="col-md-4 control-label">Desde</label>
				{!! Form::date('from-date', \Carbon\Carbon::now()); !!}
				<label for="to-date" class="col-md-4 control-label">Hasta</label>
				{!! Form::date('to-date', \Carbon\Carbon::now()); !!}


				@if ($errors->has('url'))
				@foreach($errors->get('url') as $error)
				<p style="color:#b71c1c;">{{ $error }}</p>
				@endforeach
				@endif

				{!!Form::submit('Agregar', ['class' => 'find btn btn-primary green m-0', 'style' => 'width:100%'] ) !!}
				<div id="scrap">
					<div class="progress mt-5">
						<div class="indeterminate"></div>
					</div>
				</div>
			</div>
		</div>
		<script type=text/javascript>
			function submitForm()
			{	
				$('.btn.green.find').hide();
				$('.progress').show();
			}

		</script>
		{!! Form::close() !!}

		<br>
		<br>
	</div>
	<div class="modal-footer">
		<a class="modal-action modal-close waves-effect waves-blue btn-flat">Cerrar</a>
	</div>
</div>	



@endsection