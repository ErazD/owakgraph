<div class="content-comms col s12 row">
<div style="position: relative; height: 100%; min-height: 430px;"> 
  <div class="input-comms col s6">
    <label for="tipo">Tipo de Post</label>
    <h6>
      @if($post->type && $post->type!="")
      {{$post->type}}
      @else
      No han habido coincidencias para este valor.
      @endif
    </h6>
  </div>

  <div class="input-comms col s6">
    <label for="balance">Balance E/F</label>
    <h6>
      @if($post->balance && $post->balance!="")
      {{$post->balance}}
      @else
      No han habido coincidencias para este valor.
      @endif
    </h6>
  </div>

   <div class="input-comms col s6">
    <label for="micro_moment">Google Micro Moment</label>
    <h6>
      @if($post->micro_moment && $post->micro_moment!="")
      {{$post->micro_moment}}
      @else
      No han habido coincidencias para este valor.
      @endif
    </h6>
  </div>

  <div class="input-comms col s6">
    <label for="business_objective">Business Objective</label>
    <h6>
      @if($post->business_objective && $post->business_objective!="")
      {{$post->business_objective}}
      @else
      No han habido coincidencias para este valor.
      @endif
    </h6>
  </div>

  <div class="input-comms col s6">
    <label for="tareas">Tareas de Comunicación</label>
    <h6>
      @if($post->comunication && $post->comunication!="")
      {{$post->comunication}}
      @else
      No han habido coincidencias para este valor.
      @endif
    </h6>
  </div>

  <div class="input-comms col s6">  
    <label for="themes">Temas de Comunicación</label>
    <h6>
      @if($post->theme && $post->theme!="")
      {{$post->theme}}
      @else
      No han habido coincidencias para este valor.
      @endif
    </h6>
  </div>

  @if($post->theme == 'concurso')
  <div class="input-comms col s12">
    <label for="themes">Concursos</label>
    <h6>
      @if($post->contests()->get()->count()>0)
      {{$post->contests()->get()[0]->title}}
      @else
      No han habido coincidencias de concurso.
      @endif
    </h6>   
  </div>  
  @endif
 
  <div class="input-comms col s12">  
    <label for="product">Productos</label>
    <h6>
      @if($post->product)
      {{$post->product->name}}
      @else
      No han habido coincidencias para este valor.
      @endif
    </h6>
  </div>

  <div class="input-comms col s6">  
    <label for="influencer">Influencer</label>
    <h6>
      @if($post->influencer && $post->influencer!="")
      {{$post->influencer}}
      @else
      No hay influencer.
      @endif
    </h6>
  </div>

  <div class="input-comms col s6">  
    <label for="pauta">Pauta</label>
    <h6>
      {{($post->pauta)?'PAUTA':'NO PAUTA'}}
    </h6>
  </div>  

  @if($hasUser || $isJudge)
  <div class="col s12" style="position: absolute; bottom: 5px;">
    @if($isJudge)
    <a class="grey-text text-lighten-1 tooltipped" target="_blank" href="{{url('two-factor/create/'.$social_network.'/'.$post->id.'/principal')}}" data-position="bottom" data-delay="50" data-tooltip="Sección principal de Two Factor Authentication"> <i class="material-icons">settings</i></a> 
    @endif
    <?php 
    $twos=App\TwoFactorAuthentication::where('social_network',$social_network)->where('post_id',$post->id)->get();
    $usersComms = "";
    foreach ($twos as $key => $value) { 
      $usersComms .= (($usersComms == "")?"":", ").$value->user->name;
    }          
    ?>
    @if($usersComms!="")
    <i class="grey-text text-lighten-1 tooltipped material-icons" data-position="bottom" data-enable-html="true" data-delay="50" data-tooltip="Usuarios que han registrado comms: {{$usersComms}}">people</i>
    @endif
  </div>  
  @endif

   @if($isIncomplete && $hasUser && $isJudge)
   <a href="{{url('two-factor/create/'.$social_network.'/'.$post->id.'/principal')}}" target="_blank">
    <div class="comms-submit col s6">
    <button class="find btn btn-primary green m-0" style="width:100%;">Revisar</button>
  </div>  
  </a>
  @endif 

</div>

</div>