@extends('layouts.app')

@section('scripts')
<script>
	// variables para comms-chart.js
	var type = "basket";
	var pedido_id = "2";
	var formComms=true;
	var allElements = getAllElements();
	function getAllElements(){
		var all = [];
		var temps = [];		
		@foreach($allElements['fanpages'] as $fanpage)
		var dict = {'id': {{$fanpage->id}}, 'name': '{{html_entity_decode($fanpage->brand->name, ENT_QUOTES | ENT_XML1, 'UTF-8')}}  {{$fanpage->name}}'};
		temps.push(dict);
		@endforeach
		all['fanpage'] = temps;
		temps = [];		
		@foreach($allElements['brands'] as $brand)
		var dict = {'id': {{$brand->id}}, 'name': '{{html_entity_decode($brand->name, ENT_QUOTES | ENT_XML1, 'UTF-8')}}'};
		temps.push(dict);
		@endforeach
		all['brand'] = temps;
		temps = [];		
		@foreach($allElements['baskets'] as $basket)
		var dict = {'id': {{$basket->id}}, 'name': '{{$basket->name}}'};
		temps.push(dict);
		@endforeach
		all['basket'] = temps;

		temps = [];		
		@foreach($allElements['adsets'] as $adset)
		var dict = {'id': {{$adset->id}}, 'name': '{{$adset->name}}'};
		temps.push(dict);
		@endforeach
		all['adset'] = temps;

		console.log(all);
		return all;
	}
</script>
<script src="{{ asset('js/vue.min.js') }}"></script>
<script src="{{ asset('js/moment.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.6.0/Chart.min.js"></script>
<script src="https://www.amcharts.com/lib/3/amcharts.js"></script>
<script src="https://www.amcharts.com/lib/3/plugins/export/export.min.js"></script>
<script src="https://www.amcharts.com/lib/3/serial.js"></script>
{{-- <script src="https://www.amcharts.com/lib/3/pie.js"></script> --}}
<link rel="stylesheet" href="https://www.amcharts.com/lib/3/plugins/export/export.css" type="text/css" media="all" />
<script src="{{ asset('js/comms-rates-chart.js') }}"></script>

<script>
function formatNumber(num) {
    return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.");
}
function formatSelected(num, type){
	if("cant reactions prom_reactions shares prom_shares comments prom_comments views".includes(type)){
		return formatNumber(num);
	}else if("amplification_rate participation_rate reaction_rate".includes(type)){
		return num.toFixed(2)+'%';
	}else{
		return num;
	}
}
</script>

<script>
	@if($_GET)
		<?php 
			$type = $_GET['type'];
			$pedido_id = $_GET['pedido_id'];
			$country = $_GET['country'];
			$to = $_GET['to'];
			$since = (empty($_GET['since']))? "": $_GET['since'];
			$until = (empty($_GET['until']))? "": $_GET['until'];
		?>
			// var formComms = "undefined";
			commsApp.type = "{{$type}}";
			commsApp.pedido_id = "{{$pedido_id}}";
			commsApp.country = "{{$country}}";
			commsApp.to = "{{$to}}";
			commsApp.since = "{{$since}}";
			commsApp.until = "{{$until}}";			
			$.each(datepickers, function(key, value){
				value.updateValue();
			});
			// commsApp.getConsult();
	@endif

</script>
@stop
@section('content')

<div class="row grey lighten-3 mb-0">
	<div class="col s12 m12 l12 page-title">
		<h2 class="breadcrumbs-title left">
		Tasas Comms</h2>
	</div>
</div>
<div class="row blue darken-3">
	<div class="col s12 m12 l12 page-title">
		<p class="white-text" style="width: 80%;"> 
		{{-- 	@if(!is_null($video->content))
			{{ $video->content }}
			@else
			Esta publicación no tiene una descripción
			@endif --}}
			En esta sección pueden consultarse diferentes tasas de actividad de una marca o una fanpage en un período determinado de tiempo. (Para una mejor visualización de los datos hacer consultas de máximo 60 días).
		</p>
		<div class="btn-group">
			{{-- <a href="{{ $video->permalink_url }}" target="_blank" class="btn btn-primary green tooltipped mr-5" style="margin-left: 20px;" data-position="bottom" data-delay="50" data-tooltip="Abrir en Facebook"><i class="material-icons">insert_link</i></a>

			<a href="{{ url('youtube-video/'.$video->id.'/export') }}" target="_blank" class="btn btn-primary green tooltipped" data-position="bottom" data-delay="50" data-tooltip="Descargar"><i class="material-icons">file_download</i></a> --}}
		</div>
	</div>
</div>
<style>
	body, html{
		overflow: initial;
	}
	.full-widht{
		width: 100%;
	}

	optgroup{
		border-color: red;
		border-style: solid;
		border-width: 3px;
		background-color: blue;
	}

	table{
		border-style: solid;
	    border-width: 1px;
	    margin-top: 30px;
	    margin-bottom: : 30px;
	}

	table th{
		color: white;
		background-color: #0277bd; 
	}
	table th, table td{
		text-align: center;
		border-style: solid;
	    border-width: 1px;
	    border-color: #ffffff99;
	    padding: 8px;
	} 
	table th.selected{
		background-color: #b0bec5;
		color: #37474f;
	}
	 table td.selected{
		background-color: #eceff1;
	}
	.pointer{
		cursor: pointer;
	}
</style>

<div class="container-m">
	
	<div id="comms-charts" class='row'>
		<h5>Consultar tasas:</h5>
		<div class="input-field col s6">
			<input id="since" v-model="since" name="since"  is="datepicker" class='datepicker' type="text" placeholder="Y-m-d">
			<label for="since">Fecha Inicial:</label>
		</div>  
		<div class="input-field col s6">
			<input id="until" v-model="until" name="until"  is="datepicker" class='datepicker' type="text" placeholder="Y-m-d">
			<label for="until">Fecha Final:</label>
		</div> 
		<div class="filtros-graph row">
			<div class="col s6">
				<label for="first_name">Filtrar Fecha:</label>
				<select v-model="interval" v-on:change="setDate(interval)" class="browser-default"  name="interval"   id="">
					<optgroup label="" >
						<option value="all">Todo</option>
					</optgroup>
					<optgroup label="" >
						<option value="hoy">hoy</option>
						<option value="day">El mismo Día</option>
					</optgroup>
					<optgroup label="">
						<option value="6">Semana anterior</option>
						<option value="28">28 días anteriores</option>
						<option value="30">30 días anteriores</option>
					</optgroup>
					<optgroup label="">
						<option value="90">trimestre anterior (90 días)</option>
						<option value="182">semestre anterior (182 días)</option>
						<option value="365">año anterior</option>
					</optgroup>
				</select>
			</div>
			{{-- <button style="display: none;" @click="setDate()">Actualizar Fecha</button> --}}
			<div class="col s6">
				<label for="first_name">Filtrar Ubicación:</label>
				<select class="browser-default" v-model="country" name="country" id="">
					<option value="all"> Todos los países </option>
					<option v-for="option in allCountries"><%option%></option>
				</select>
			</div>
		</div>
		<div class="filtros-graph row">
			<div class="col s6">
				<label for="first_name">Filtrar Scope: (seleccionar el alcance de la consulta y la opción deseada)</label>
				<select class="browser-default" v-model="type" name="type" id="model-type">
		{{-- <option value="vertical">Vertical</option>
		<option value="category">Categoría</option> --}}
		<option value="basket">Categoría</option>
		<option value="brand">Marca</option>
		<option value="adset">AdSet</option>
		{{-- <option value="fanpage">Fanpage</option> --}}
	</select>
</div>
<div class="col s6">
	<select class='adjust-list browser-default' v-model="pedido_id" list="lista_ids" name="pedido_id" placeholder="Id del elemento a buscar" id="model-id">
		<option v-for="option in allElements[type]" v-bind:value='option.id'><%option.name%></option>
	</select>
</div>
</div>

{{-- FILTROS DE COMMS --}}
<div class="row">
	@include('comms.filter-comms-selector', array('nameVueApp'=>'commsApp'))
</div>



{{-- <input v-model="pedido_id" list="lista_ids" name="pedido_id" placeholder="Id del elemento a buscar" >
<datalist id="lista_ids">
	<option v-for="option in allElements[type]" v-bind:value="option.id" v-bind:label="option.name"></option>
</datalist> --}}
{{-- <button  @click="getCommsChart()">Consultar</button> --}}
<br>
<button class='consult-btn btn'  @click="getConsult()">Ver Tasas</button>
<div class="row url-box hide">
	<a class="btn" @click="getConsultURL()">Copiar Consulta</a>	
	<input type="text" id="share-url" class="hide">
</div>
<br>
<button style="display: none;" @click="generateChartsComms(to)"> DATA</button>

<br>

<div class="row comms-display">
	<h4>Datos obtenidos</h4>
	<select class="browser-default total-filter" v-model="to" name="to" id="" > {{-- v-on:change="generateChartsComms(to)"> --}}
		{{-- <option disabled value="vertical">Vertical</option>
		<option disabled value="category">Categoría</option> --}}
		<option disabled="disabled">Mostrar:</option>
		<option value="basket">Categoría</option>
		<option selected value="brand">Marca</option>
		<option value="adset">AdSet</option>
	</select>

	<div class="row">

<div id="posiciones" v-if="globalPositions[0]">

	<h4><%formatNumber(allDataMetrics.metrics.cant)%> posts analizados durante el periodo (<%since%> - <%until%>)</h4>

	<h6><b>Vistas:</b> <%formatNumber(allDataMetrics.metrics.views)%> <b>| Reacciones:</b> <%formatNumber(allDataMetrics.metrics.reactions)%> y <%formatNumber(allDataMetrics.metrics.reactions_n)%> <b>| Comentarios:</b> <%formatNumber(allDataMetrics.metrics.comments)%> y <%formatNumber(allDataMetrics.metrics.comments_n)%> <b>| Compartidos:</b> <%formatNumber(allDataMetrics.metrics.shares)%> y <%formatNumber(allDataMetrics.metrics.shares_n)%></h6>

	<table>
		<tr><th>#</th> <th>Marca</th> <th class="blue lighten-3 blue-grey-text text-darken-3" v-if="globalPositionsKey"><%globalPositionsKey.name%></th>
			<th class="sort-selectable sort-selectable-cant_posts pointer" class="pointer" @click="sortGlobalPositions('cant_posts', 'Posts')">Posts</th> 
			<th class="sort-selectable sort-selectable-reactions pointer" class="pointer" @click="sortGlobalPositions('reactions', 'Reacciones')">Reacciones</th> 
			<th class="sort-selectable sort-selectable-prom_reactions pointer" class="pointer" @click="sortGlobalPositions('prom_reactions', 'Reacciones Promedio')">Reacciones Promedio</th> 
			<th class="sort-selectable sort-selectable-shares pointer" class="pointer" @click="sortGlobalPositions('shares', 'Compartidos')">Compartidos</th> 
			<th class="sort-selectable sort-selectable-prom_shares pointer" class="pointer" @click="sortGlobalPositions('prom_shares', 'Compartidos Promedio')">Compartidos Promedio</th> 
			<th class="sort-selectable sort-selectable-comments pointer" class="pointer" @click="sortGlobalPositions('comments', 'Comentarios')">Comentarios</th> 
			<th class="sort-selectable sort-selectable-prom_comments pointer" class="pointer" @click="sortGlobalPositions('prom_comments', 'Comentarios Promedio')">Comentarios Promedio</th> 
			<th class="sort-selectable sort-selectable-views pointer" class="pointer tooltipped" @click="sortGlobalPositions('views', 'Vistas')" data-position="top" data-delay="50" data-tooltip="Solo incluye publicaciones de video">Vistas*</th> 
			<th class="sort-selectable sort-selectable-amplification_rate pointer" class="pointer" @click="sortGlobalPositions('amplification_rate', 'Tasa de amplificación')">Tasa de amplificación</th> 
			<th class="sort-selectable sort-selectable-participation_rate pointer" class="pointer" @click="sortGlobalPositions('participation_rate', 'Tasa de participación')">Tasa de participación</th> 
			<th class="sort-selectable sort-selectable-reaction_rate pointer" class="pointer tooltipped" @click="sortGlobalPositions('reaction_rate', 'Tasa de reacción')" data-position="top" data-delay="50" data-tooltip="Solo incluye publicaciones de video">Tasa de reacción* </th>
		</tr>
		<tr v-for="(ele, key) in globalPositions">
			<th><%key+1%></th> <td><%ele.name%></td> <td class="blue lighten-5" v-if="globalPositionsKey"><%ele[globalPositionsKey.key]%></td>			
			<td class="sort-selectable sort-selectable-cant_posts"><%ele.cant_posts%></td> 
			<td class="sort-selectable sort-selectable-reactions"><%ele.reactions%></td>
			<td class="sort-selectable sort-selectable-prom_reactions"><%ele.prom_reactions%></td> 
			<td class="sort-selectable sort-selectable-shares"><%ele.shares%></td>
			<td class="sort-selectable sort-selectable-prom_shares"><%ele.prom_shares%></td> 
			<td class="sort-selectable sort-selectable-comments"><%ele.comments%></td>
			<td class="sort-selectable sort-selectable-prom_comments"><%ele.prom_comments%></td> 
			<td class="sort-selectable sort-selectable-views"><%ele.views%></td> 
			<td class="sort-selectable sort-selectable-amplification_rate"><%ele.amplification_rate%>%</td> 
			<td class="sort-selectable sort-selectable-participation_rate"><%ele.participation_rate%>%</td> 
			<td class="sort-selectable sort-selectable-reaction_rate"><%ele.reaction_rate%>%</td>
		</tr>
	</table>
</div> <br>

		<div id="grafica-total-rates" class="full-widht">
			
		</div>
		<br>
		<div id="grafica-total-metrics" class="full-widht">
			
		</div>
	</div>

{{--		 
	DEPRECATED: sección para mostrar las gráficas de las tasas de tareas de comunicación, temas de comunicación y productos de cada marca o fanpage (elemento to).
	<div class="row">
		<div v-for="crt in comms_rates_types">
			<br><hr>
			<h5><%crt.name%></h5>
		<div v-for="brnd in commsData">
			<h5><%brnd.name%></h5>
			<div v-for="fpg in brnd.fanpages">

		<div v-bind:id="'section-'+crt.slug+'-'+fpg.facebook_id+'-rates'"></div>		
		</div>
		</div>
		</div>
	</div>
--}}

	<input id="mostrarDatosCrudos" type="checkbox" v-model="mostrarDatosCrudos">
	<label for="mostrarDatosCrudos">Mostrar Datos Crudos</label>

	<div class="row" v-if="mostrarDatosCrudos">

		<div v-for="fpgs in commsData">
			<h5><%fpgs['name']%></h5>
			<p>
				<b>Vistas: </b> <% formatNumber(fpgs['metrics']['views'])%>, 
				<b>Reacciones: </b> <% formatNumber(fpgs['metrics']['reactions'])%>, 
				<b>Comentarios: </b> <% formatNumber(fpgs['metrics']['comments'])%>, 
				<b>Compartidos: </b> <% formatNumber(fpgs['metrics']['shares'])%> <br>

				<b>Tasa Amplificación promedio: </b> <%parseFloat(fpgs['metrics']['totals']['amplification_rate']).toFixed(2)%>% <br>
				<b>Tasa Participación promedio: </b> <%parseFloat(fpgs['metrics']['totals']['participation_rate']).toFixed(2)%>% <br>
				<b>Tasa de Reacción promedio: </b> <%parseFloat(fpgs['metrics']['totals']['reaction_rate']).toFixed(2)%>% <br>
			</p>
			<div v-for="fpg in fpgs['fanpages']">
				<b><%fpg['name']%>:</b> <a v-bind:href="'{{url('fanpage')}}/'+fpg['fanpage_id']" ><i class="material-icons">insert_link</i></a> <br>

				<b>Vistas: </b> <%formatNumber(fpg['metrics']['views'])%>, 
				<b>Reacciones: </b> <%formatNumber(fpg['metrics']['reactions'])%>, 
				<b>Comentarios: </b> <%formatNumber(fpg['metrics']['comments'])%>, 
				<b>Compartidos: </b> <%formatNumber(fpg['metrics']['shares'])%> <br>


				<div v-for="(rates, type) in fpg['metrics']['rates']">
					<%type%>:
					<div v-if="type == 'totals'">
						<b>Tasa Amplificación promedio: </b> <%parseFloat(rates['amplification_rate']).toFixed(2)%>% <br>
						<b>Tasa Participación promedio: </b> <%parseFloat(rates['participation_rate']).toFixed(2)%>% <br>
						<b>Tasa de Reacción promedio: </b> <%parseFloat(rates['reaction_rate']).toFixed(2)%>% <br>
						<b>Cantidad posts: </b> <%rates['cant']%> <br>	
					</div>
					<div v-else v-for="(rts, name) in rates"> <%name%>: <br>
						<b>Tasa Amplificación promedio: </b> <%parseFloat(rts['amplification_rate']).toFixed(2)%>% <br>
						<b>Tasa Participación promedio: </b> <%parseFloat(rts['participation_rate']).toFixed(2)%>% <br>
						<b>Tasa de Reacción promedio: </b> <%parseFloat(rts['reaction_rate']).toFixed(2)%>% <br>
						<b>Cantidad posts: </b> <%rts['cant']%> <br>	
					</div>
					<hr>
				</div>
				<div>
					<table>
						<tr><th>URL</th><th>METRICAS</th><th>Tasas Post</th><th>Tasas FANPAGE</th><th>Producto</th><th>Tarea</th><th>Tema</th><th>Tipo</th><th>Balance</th><th>Influencer</th><th>Pauta</th></tr>
						<tr v-for="post in fpg['posts']">
							<td><a v-bind:href="'{{url('posts')}}/'+post['id']"><i class="material-icons">insert_link</i></a></td>
							<td><b>R:</b> <%formatNumber(post['reactions'])%> <br><b>S:</b> <%formatNumber(post['shares'])%><br><b>C:</b> <%formatNumber(post['comments'])%><br><b>V:</b> <%formatNumber(post['views'])%>    </td>

							<td><b>amplificación:</b> <%parseFloat(post['own_amplification_rate']).toFixed(2)%>% <br><b>Participación:</b> <%parseFloat(post['own_participation_rate']).toFixed(2)%>%<br><b>Reacción:</b> <%parseFloat(post['own_reaction_rate']).toFixed(2)%>%</td>
							<td><b>amplificación:</b> <%parseFloat(post['amplification_rate']).toFixed(2)%>% <br><b>Participación:</b> <%parseFloat(post['participation_rate']).toFixed(2)%>%<br><b>Reacción:</b> <%parseFloat(post['reaction_rate']).toFixed(2)%>%</td>
							<td><%post['product']%></td><td><%post['comunication_task']%></td><td><%post['comunication_theme']%></td>
							<td><%post['type']%></td><td><%post['balance']%></td><td><%post['influencer']%></td><td><%post['pauta']%></td>

						</tr>
					</table>
				</div>
			</div>
		</div>
	</div>


</div>
</div>
</div>
</div>
</div>
</div>
@stop