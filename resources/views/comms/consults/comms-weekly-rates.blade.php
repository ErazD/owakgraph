@extends('comms.consults.base-consult')

@section('consultscripts')
<script src="{{ asset('js/consults/activity-chart.js') }}"></script>
@stop 

@section('consultdescription')
<div class="row grey lighten-3 mb-0">
	<div class="col s12 m12 l12 page-title">
		<h2 class="breadcrumbs-title left">
		Frecuencia de Actividad</h2>
	</div>
</div>
<div class="row blue darken-3">
	<div class="col s12 m12 l12 page-title">
		<p class="white-text" style="width: 80%;"> 

			En esta sección pueden consultarse la frecuencia de actividad de las marcas en diferentes rangos de tiempo
		</p>
	</div>
</div>
@stop

@section('consultcontent')
<h6 v-if="allData.metrics"><b>Vistas:</b> <%formatNumber(allData.metrics.views)%> <b>| Reacciones:</b> <%formatNumber(allData.metrics.reactions)%> <b>| Comentarios:</b> <%formatNumber(allData.metrics.comments)%> <b>| Compartidos:</b> <%formatNumber(allData.metrics.shares)%></h6> 
<consult-activity name="activity-rate" v-bind:class='(mostrarResultados === true)? "show": "hide"'></consult-activity>
<h6 v-if='mostrarResultados === false'>No hay datos para mostrar</h6>
@stop

@section('consulttemplate')
<template id="consult-activity">
<div class="sharesof row">
	<div class="chart-container col s12 m12 l4 xl3 flex"> 
		<div class="activity-chart" id="grafica-activity-reactions"></div>
	</div>
	<div class="chart-container col s12 m12 l4 xl3 flex"> 
		<div class="activity-chart" id="grafica-activity-comments"></div>
	</div>
	<div class="chart-container col s12 m12 l4 xl3 flex">
		<div class="activity-chart" id="grafica-activity-shares"></div>
	</div>
	<div class="chart-container col s12 m12 l4 xl3 flex"> 
		<div class="activity-chart" id="grafica-activity-views"></div>
	</div>
</div>
</template>
@stop


@section('consultstyles')
<style>
optgroup{
	border-color: red;
	border-style: solid;
	border-width: 3px;
	background-color: blue;
}

body, html{
	overflow: initial;
}
#sharesof-app h5{
	margin-left: 9px;
}
.activity-chart{
	width: 100%;
	display:block !important;
	height: 650px;
	margin:10px auto 20px !important; 
}
.sharesof-display{
	margin-top:1%;
	padding:15px;
}
.sharesof-display h4{
	display:inline-block;
	vertical-align:middle;
}
.sharesof-display select{
	display:inline-block;
	vertical-align:middle;
	width:40%;
	margin-left:20px;
	float:right;
	margin:8px 0 10px 0;
}
.txt-info{
	font-size: 15px;
	font-weight: 100;
}
h5.no-data{
	text-align: center;
	font-weight: 900;
}
.center-content{
	text-align:center;
}
.sharesof{
	margin:6% 0 0 0;
}
@media only screen and (max-width: 1360px) {
	.sharesof .chart-container{
		width:100% !important;
	}
}

@media only screen and (min-width: 1360px) {
	.sharesof .chart-container{
		width:33% !important;
	}
}
.consult-info{
	font-size:1.5em;
}
.consult-btn{
	margin-left: 14px !important;
}
</style>
@stop
