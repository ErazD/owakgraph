@extends('comms.consults.base-consult')

@section('consultscripts')
<script src="{{ asset('js/consults/comms-rates-chart.js') }}"></script>
<script>
	function formatSelected(num, type){
		if("cant reactions prom_reactions shares prom_shares comments prom_comments views".includes(type)){
			return formatNumber(num);
		}else if("amplification_rate participation_rate reaction_rate".includes(type)){
			return num.toFixed(2)+'%';
		}else{
			return num;
		}
	}
</script>
@stop

@section('consultdescription')
<div class="row grey lighten-3 mb-0">
	<div class="col s12 m12 l12 page-title">
		<h2 class="breadcrumbs-title left">
		Tasas Comms</h2>
	</div>
</div>
<div class="row blue darken-3">
	<div class="col s12 m12 l12 page-title">
		<p class="white-text" style="width: 80%;">
			En esta sección pueden consultarse diferentes tasas de actividad de una marca o una fanpage en un período determinado de tiempo. (Para una mejor visualización de los datos hacer consultas de máximo 60 días).
		</p>
	</div>
</div>
@stop

@section('consultcontent')
<div v-if="allData.metrics">
	<h4><%formatNumber(allData.metrics.cant)%> posts analizados durante el periodo (<%since%> - <%until%>)</h4>

	<h6 v-if='parentApp.social_network == "Facebook"'><b>Vistas:</b> <%formatNumber(allData.metrics.fb_views)%> <b>| Reacciones:</b> <%formatNumber(allData.metrics.fb_reactions)%> y <%formatNumber(allData.metrics.fb_reactions_n)%> <b>| Comentarios:</b> <%formatNumber(allData.metrics.fb_comments)%> y <%formatNumber(allData.metrics.fb_comments_n)%> <b>| Compartidos:</b> <%formatNumber(allData.metrics.fb_shares)%> y <%formatNumber(allData.metrics.fb_shares_n)%></h6>
	<h6 v-if='parentApp.social_network == "YouTube"'><b>Vistas:</b> <%formatNumber(allData.metrics.yt_views)%> <b>| Likes:</b> <%formatNumber(allData.metrics.yt_likes)%> y <%formatNumber(allData.metrics.yt_likes_n)%> <b>| Comentarios:</b> <%formatNumber(allData.metrics.yt_comments)%> y <%formatNumber(allData.metrics.yt_comments_n)%> <b>| Dislikes:</b> <%formatNumber(allData.metrics.yt_dislikes)%> y <%formatNumber(allData.metrics.yt_dislikes_n)%></h6>
	<h6 v-if='parentApp.social_network == "Instagram"'><b>Vistas:</b> <%formatNumber(allData.metrics.insta_views)%> <b>| Likes:</b> <%formatNumber(allData.metrics.insta_likes)%> y <%formatNumber(allData.metrics.insta_reactions_n)%> <b>| Comentarios:</b> <%formatNumber(allData.metrics.insta_comments)%> y <%formatNumber(allData.metrics.insta_comments_n)%></h6>
</div>
<consult-metrics name="consult-metrics" v-bind:class='(mostrarResultados === true)? "show": "hide"'></consult-metrics>
<h6 v-if='mostrarResultados === false' >No hay datos para mostrar</h6>
@stop

@section('consulttemplate')
<template id="consult-metrics">
	<div class="row">
		<div id="posiciones" v-if="globalPositions[0]">
			<table class="top-table">
				<tr><th>#</th> <th>Marca</th> <th class="blue lighten-3 blue-grey-text text-darken-3" v-if="globalPositionsKey"><%globalPositionsKey.name%></th>
					<th class="sort-selectable sort-selectable-cant_posts pointer" class="pointer" @click="sortGlobalPositions('cant_posts', 'Posts')">Posts</th> 
					
					<th v-if="parentApp.social_network=='Facebook'" class="sort-selectable sort-selectable-reactions pointer" class="pointer" @click="sortGlobalPositions('reactions', 'Reacciones')">Reacciones</th> 
					<th v-if="parentApp.social_network=='Facebook'" class="sort-selectable sort-selectable-prom_reactions pointer" class="pointer" @click="sortGlobalPositions('prom_reactions', 'Reacciones Promedio')">Reacciones Promedio</th> 
					<th v-if="parentApp.social_network=='Facebook'" class="sort-selectable sort-selectable-shares pointer" class="pointer" @click="sortGlobalPositions('shares', 'Compartidos')">Compartidos</th> 
					<th v-if="parentApp.social_network=='Facebook'" class="sort-selectable sort-selectable-prom_shares pointer" class="pointer" @click="sortGlobalPositions('prom_shares', 'Compartidos Promedio')">Compartidos Promedio</th> 
					<th v-if="parentApp.social_network=='Facebook'" class="sort-selectable sort-selectable-comments pointer" class="pointer" @click="sortGlobalPositions('comments', 'Comentarios')">Comentarios</th> 
					<th v-if="parentApp.social_network=='Facebook'" class="sort-selectable sort-selectable-prom_comments pointer" class="pointer" @click="sortGlobalPositions('prom_comments', 'Comentarios Promedio')">Comentarios Promedio</th> 
					<th v-if="parentApp.social_network=='Facebook'" class="sort-selectable sort-selectable-views pointer" class="pointer tooltipped" @click="sortGlobalPositions('views', 'Vistas')" data-position="top" data-delay="50" data-tooltip="Solo incluye publicaciones de video">Vistas*</th> 

                    <th v-if="parentApp.social_network=='YouTube' && parentApp.social_network=='Instagram'" class="sort-selectable sort-selectable-reactions pointer" class="pointer" @click="sortGlobalPositions('views', 'Views')">Vistas</th> 
					<th v-if="parentApp.social_network=='YouTube' && parentApp.social_network=='Instagram'" class="sort-selectable sort-selectable-prom_reactions pointer" class="pointer" @click="sortGlobalPositions('prom_views', 'Vistas Promedio')">Vistas Promedio</th> 
					<th v-if="parentApp.social_network=='YouTube' && parentApp.social_network=='Instagram'" class="sort-selectable sort-selectable-shares pointer" class="pointer" @click="sortGlobalPositions('likes', 'Likes')">Likes</th> 
					<th v-if="parentApp.social_network=='YouTube' && parentApp.social_network=='Instagram'" class="sort-selectable sort-selectable-prom_shares pointer" class="pointer" @click="sortGlobalPositions('prom_likes', 'Likes Promedio')">Likes Promedio</th> 
					<th v-if="parentApp.social_network=='YouTube' && parentApp.social_network=='Instagram'" class="sort-selectable sort-selectable-comments pointer" class="pointer" @click="sortGlobalPositions('comments', 'Comentarios')">Comentarios</th> 
					<th v-if="parentApp.social_network=='YouTube' && parentApp.social_network=='Instagram'" class="sort-selectable sort-selectable-prom_comments pointer" class="pointer" @click="sortGlobalPositions('prom_comments', 'Comentarios Promedio')">Comentarios Promedio</th> 
					<th v-if="parentApp.social_network=='YouTube'" class="sort-selectable sort-selectable-views pointer" class="pointer" @click="sortGlobalPositions('dislikes', 'Dislikes')">Dislikes</th>
					<th v-if="parentApp.social_network=='YouTube'" class="sort-selectable sort-selectable-views pointer" class="pointer" @click="sortGlobalPositions('prom_dislikes', 'Dislikes Promedio')">Dislikes Promedio</th>




					<th class="sort-selectable sort-selectable-amplification_rate pointer" class="pointer" @click="sortGlobalPositions('amplification_rate', 'Tasa de amplificación')">Tasa de amplificación</th> 
					<th class="sort-selectable sort-selectable-participation_rate pointer" class="pointer" @click="sortGlobalPositions('participation_rate', 'Tasa de participación')">Tasa de participación</th> 
					<th class="sort-selectable sort-selectable-reaction_rate pointer" class="pointer tooltipped" @click="sortGlobalPositions('reaction_rate', 'Tasa de reacción')" data-position="top" data-delay="50" data-tooltip="Solo incluye publicaciones de video">Tasa de reacción* </th>
				</tr>
				<tr v-for="(ele, key) in globalPositions">
					<th><%key+1%></th> <td><%ele.name%></td> <td class="blue lighten-5" v-if="globalPositionsKey"><%ele[globalPositionsKey.key]%></td>			
					<td class="sort-selectable sort-selectable-cant_posts"><%ele.cant_posts%></td> 
					<td v-if="parentApp.social_network=='Facebook'" class="sort-selectable sort-selectable-reactions"><%ele.reactions%></td>
					<td v-if="parentApp.social_network=='Facebook'" class="sort-selectable sort-selectable-prom_reactions"><%ele.prom_reactions%></td> 
					<td v-if="parentApp.social_network=='Facebook'" class="sort-selectable sort-selectable-shares"><%ele.shares%></td>
					<td v-if="parentApp.social_network=='Facebook'" class="sort-selectable sort-selectable-prom_shares"><%ele.prom_shares%></td> 
					<td v-if="parentApp.social_network=='Facebook'" class="sort-selectable sort-selectable-comments"><%ele.comments%></td>
					<td v-if="parentApp.social_network=='Facebook'" class="sort-selectable sort-selectable-prom_comments"><%ele.prom_comments%></td> 
					<td v-if="parentApp.social_network=='Facebook'" class="sort-selectable sort-selectable-views"><%ele.views%></td>

					<td v-if="parentApp.social_network=='YouTube' && parentApp.social_network=='Instagram'" class="sort-selectable sort-selectable-reactions"><%ele.views%></td>
					<td v-if="parentApp.social_network=='YouTube' && parentApp.social_network=='Instagram'" class="sort-selectable sort-selectable-prom_reactions"><%ele.prom_views%></td> 
					<td v-if="parentApp.social_network=='YouTube'" class="sort-selectable sort-selectable-shares"><%ele.likes%></td>
					<td v-if="parentApp.social_network=='YouTube' && parentApp.social_network=='Instagram'" class="sort-selectable sort-selectable-prom_shares"><%ele.prom_likes%></td> 
					<td v-if="parentApp.social_network=='YouTube' && parentApp.social_network=='Instagram'" class="sort-selectable sort-selectable-comments"><%ele.comments%></td>
					<td v-if="parentApp.social_network=='YouTube' && parentApp.social_network=='Instagram'" class="sort-selectable sort-selectable-prom_comments"><%ele.prom_comments%></td> 
					<td v-if="parentApp.social_network=='YouTube'" class="sort-selectable sort-selectable-views"><%ele.dislikes%></td>
					<td v-if="parentApp.social_network=='YouTube'" class="sort-selectable sort-selectable-views"><%ele.prom_dislikes%></td>    
					<td class="sort-selectable sort-selectable-amplification_rate"><%ele.amplification_rate%>%</td> 
					<td class="sort-selectable sort-selectable-participation_rate"><%ele.participation_rate%>%</td> 
					<td class="sort-selectable sort-selectable-reaction_rate"><%ele.reaction_rate%>%</td>
				</tr>
			</table>
		</div>
		<div id="grafica-total-rates" class="full-widht"></div>
		<div id="grafica-total-metrics" class="full-widht"></div>
{{--		 
	DEPRECATED: sección para mostrar las gráficas de las tasas de tareas de comunicación, temas de comunicación y productos de cada marca o fanpage (elemento to).
	<div class="row">
		<div v-for="crt in comms_rates_types">
			<br><hr>
			<h5><%crt.name%></h5>
		<div v-for="brnd in commsData">
			<h5><%brnd.name%></h5>
			<div v-for="fpg in brnd.fanpages">

		<div v-bind:id="'section-'+crt.slug+'-'+fpg.facebook_id+'-rates'"></div>		
		</div>
		</div>
		</div>
	</div>
	--}}

	<hr>
	<div class="row">
		<input id="mostrarDatosCrudos" type="checkbox" v-model="mostrarDatosCrudos">
		<label for="mostrarDatosCrudos">Mostrar Datos Crudos</label>
	</div>
	<div class="row" v-if="mostrarDatosCrudos">

		<div v-for="fpgs in parentApp.commsData">
			<h5><%fpgs['name']%></h5>
			<p>
				<b>Vistas: </b> <% formatNumber(fpgs['metrics']['fb_views'])%>, 
				<b>Reacciones: </b> <% formatNumber(fpgs['metrics']['fb_reactions'])%>, 
				<b>Comentarios: </b> <% formatNumber(fpgs['metrics']['fb_comments'])%>, 
				<b>Compartidos: </b> <% formatNumber(fpgs['metrics']['fb_shares'])%> <br>

				<b>Tasa Amplificación promedio: </b> <%parseFloat(fpgs['metrics']['totals']['amplification_rate']).toFixed(2)%>% <br>
				<b>Tasa Participación promedio: </b> <%parseFloat(fpgs['metrics']['totals']['participation_rate']).toFixed(2)%>% <br>
				<b>Tasa de Reacción promedio: </b> <%parseFloat(fpgs['metrics']['totals']['reaction_rate']).toFixed(2)%>% <br>
			</p>
			<div v-for="fpg in fpgs['fanpages']">
				<b><%fpg['name']%>:</b> <a v-bind:href="'{{url('fanpage')}}/'+fpg['fanpage_id']" ><i class="material-icons">insert_link</i></a> <br>

				<b>Vistas: </b> <%formatNumber(fpg['metrics']['fb_views'])%>, 
				<b>Reacciones: </b> <%formatNumber(fpg['metrics']['fb_reactions'])%>, 
				<b>Comentarios: </b> <%formatNumber(fpg['metrics']['fb_comments'])%>, 
				<b>Compartidos: </b> <%formatNumber(fpg['metrics']['fb_shares'])%> <br>


				<div v-for="(rates, type) in fpg['metrics']['rates']">
					<%type%>:
					<div v-if="type == 'totals'">
						<b>Tasa Amplificación promedio: </b> <%parseFloat(rates['amplification_rate']).toFixed(2)%>% <br>
						<b>Tasa Participación promedio: </b> <%parseFloat(rates['participation_rate']).toFixed(2)%>% <br>
						<b>Tasa de Reacción promedio: </b> <%parseFloat(rates['reaction_rate']).toFixed(2)%>% <br>
						<b>Cantidad posts: </b> <%rates['cant_posts']%> <br>	
					</div>
					<div v-else v-for="(rts, name) in rates"> <%name%>: <br>
						<b>Tasa Amplificación promedio: </b> <%parseFloat(rts['amplification_rate']).toFixed(2)%>% <br>
						<b>Tasa Participación promedio: </b> <%parseFloat(rts['participation_rate']).toFixed(2)%>% <br>
						<b>Tasa de Reacción promedio: </b> <%parseFloat(rts['reaction_rate']).toFixed(2)%>% <br>
						<b>Cantidad posts: </b> <%rts['cant_posts']%> <br>	
					</div>
					<hr>
				</div>
				<div>
					<table>
						<tr><th>URL</th><th>METRICAS</th><th>Tasas Post</th><th>Tasas FANPAGE</th><th>Producto</th><th>Tarea</th><th>Tema</th><th>Tipo</th><th>Balance</th><th>Influencer</th><th>Pauta</th></tr>
						<tr v-for="post in fpg['posts']">
							<td><a v-bind:href="'{{url('posts')}}/'+post['id']"><i class="material-icons">insert_link</i></a></td>
							<td><b>R:</b> <%formatNumber(post['fb_reactions'])%> <br><b>S:</b> <%formatNumber(post['fb_shares'])%><br><b>C:</b> <%formatNumber(post['fb_comments'])%><br><b>V:</b> <%formatNumber(post['fb_views'])%>    </td>

							<td><b>amplificación:</b> <%parseFloat(post['own_amplification_rate']).toFixed(2)%>% <br><b>Participación:</b> <%parseFloat(post['own_participation_rate']).toFixed(2)%>%<br><b>Reacción:</b> <%parseFloat(post['own_reaction_rate']).toFixed(2)%>%</td>
							<td><b>amplificación:</b> <%parseFloat(post['fb_amplification_rate']).toFixed(2)%>% <br><b>Participación:</b> <%parseFloat(post['fb_participation_rate']).toFixed(2)%>%<br><b>Reacción:</b> <%parseFloat(post['fb_reaction_rate']).toFixed(2)%>%</td>
							<td><%post['product']%></td><td><%post['comunication_task']%></td><td><%post['comunication_theme']%></td>
							<td><%post['type']%></td><td><%post['balance']%></td><td><%post['influencer']%></td><td><%post['pauta']%></td>

						</tr>
					</table>
				</div>
			</div>
		</div>
	</div>
	</div>
</template>
@stop

@section('consultstyles')
<style>
body, html{
	overflow: initial;
}
.full-widht{
	width: 100%;
}

optgroup{
	border-color: red;
	border-style: solid;
	border-width: 3px;
	background-color: blue;
}

table.top-table{
	border-style: solid;
	border-width: 1px;
	margin-top: 30px;
	margin-bottom: : 30px;
}

table.top-table th{
	color: white;
	background-color: #0277bd; 
}
table.top-table th, table.top-table td{
	text-align: center;
	border-style: solid;
	border-width: 1px;
	border-color: #ffffff99;
	padding: 8px;
} 
table.top-table th.selected{
	background-color: #b0bec5;
	color: #37474f;
}
table.top-table td.selected{
	background-color: #eceff1;
}
.pointer{
	cursor: pointer;
}
</style>
@stop