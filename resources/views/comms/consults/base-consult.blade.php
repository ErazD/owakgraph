@extends('layouts.app')
@section('scripts')
<script>
	var allColors = <?php echo json_encode(App\Http\Controllers\ColorController::getColorList('products', 'segmentos')); ?>;
	console.log(allColors);
	
	function getAllElements(){
		var all = [];
		
		var temps = [];		
		@foreach($allElements['fanpages']->sortBy(function($fpg){
			return $fpg->brand->name." ".$fpg->name;
		}) as $fanpage)
		var dict = {'id': {{$fanpage->id}}, 'name': '{{html_entity_decode($fanpage->brand->name, ENT_QUOTES | ENT_XML1, 'UTF-8')}}  {{$fanpage->name}}', 'slug': '{{$fanpage->id}}'};
		temps.push(dict);
		@endforeach
		all['Fanpage'] = temps;

		var temps = [];	 
		@foreach($allElements['channels']->sortBy(function($ch){
			return $ch->brand->name." ".$ch->name;
		}) as $channel)
		var dict = {'id': {{$channel->id}}, 'name': '{{html_entity_decode($channel->brand->name, ENT_QUOTES | ENT_XML1, 'UTF-8')}}  {{$channel->name}}', 'slug': '{{$channel->id}}'};
		temps.push(dict);
		@endforeach

		all['YoutubeChannel'] = temps;

		var temps = [];	 
		@foreach($allElements['profiles']->sortBy(function($pf){
			return $pf->brand->name." ".$pf->name;
		}) as $profile)
		var dict = {'id': {{$profile->id}}, 'name': '{{html_entity_decode($profile->brand->name, ENT_QUOTES | ENT_XML1, 'UTF-8')}}  {{$profile->name}}', 'slug': '{{$profile->id}}'};
		temps.push(dict);
		@endforeach

		all['InstaProfile'] = temps;
		
		temps = [];		
		@foreach($allElements['brands']->sortBy('name') as $brand)
		var dict = {'id': {{$brand->id}}, 'name': '{{html_entity_decode($brand->name, ENT_QUOTES | ENT_XML1, 'UTF-8')}}', 'slug': '{{$brand->slug}}'};
		temps.push(dict);
		@endforeach
		all['Brand'] = temps;
		
		temps = [];		
		@foreach($allElements['baskets']->sortBy('name') as $basket)
		var dict = {'id': {{$basket->id}}, 'name': '{{$basket->name}}', 'slug': '{{$basket->slug}}'};
		temps.push(dict);
		@endforeach
		all['Canasta'] = temps;
		
		temps = [];		
		@foreach($allElements['adsets']->sortBy('name') as $adset)
		var dict = {'id': {{$adset->id}}, 'name': '{{$adset->name}}', 'slug': '{{$adset->slug}}'};
		temps.push(dict);
		@endforeach
		all['AdSet'] = temps;
		

		return all;
	}

</script>

<script src="{{ asset('js/vue.min.js') }}"></script>
<script src="{{ asset('js/moment.js') }}"></script>
<script src="https://www.amcharts.com/lib/3/amcharts.js"></script>
<script src="https://www.amcharts.com/lib/3/plugins/export/export.min.js"></script>
<script src="https://www.amcharts.com/lib/3/serial.js"></script>
<script src="https://www.amcharts.com/lib/3/pie.js"></script>
<link rel="stylesheet" href="https://www.amcharts.com/lib/3/plugins/export/export.css" type="text/css" media="all" />

<script src="{{ asset('js/components/datepicker.js') }}"></script>	
<script src="{{ asset('js/consults/consult-component.js') }}"></script>
@yield('consultscripts')

<script>
	
	parentApp.allElements = getAllElements();
	@if($_GET)
	<?php 
	$type = $_GET['type'];
	$pedido_id = $_GET['pedido_id'];
	$country = $_GET['country'];
	$to = $_GET['to'];
	$since = (empty($_GET['since']))? "": $_GET['since'];
	$until = (empty($_GET['until']))? "": $_GET['until'];
	$social_network = (isset($_GET['social_network']))? $_GET['social_network'] : 'all';
	?>
	var formComms = "undefined";
	parentApp.type = "{{$type}}";
	parentApp.pedido_id = "{{$pedido_id}}";
	parentApp.country = "{{$country}}";
	parentApp.to = "{{$to}}";
	parentApp.since = "{{$since}}";
	parentApp.until = "{{$until}}";
	parentApp.social_network = "{{$social_network}}";

	@endif

/**
	* Oculta los elementos del tipo de comms seleccionado que no se encuentran dentro del filtro.
	* @param {string} typeElement: slug del elemento comms a filtrar.
	* Postcondition: Se han ocultado en el html los elementos que no cumplen con el valor filtro para el tipo de comms seleccionado.
	*/

	var applyOutside = false;
	document.onclick = function(e){
		
		if(applyOutside){
			if(e.target.className !== 'categories-container' || e.target.className !== 'category-input'){
					banishItems();
			}
		}
        applyOutside = false;
    }

	function filterCategories(event, typeElement){
		var input, filter, container, list;
      
    	input = document.getElementById("filter-"+typeElement);
    	filter = input.value.toUpperCase(); // valor a filtrar
    	container = document.getElementById('model-id');
    	list = container.getElementsByClassName("option-category"); // Lista de las opciones para el valor de comms
     
    	for (var i = 0; i < list.length; i++) {
    	 	var elem = list[i];	 
    	 	var val = elem.innerHTML.toUpperCase(); // Valor de la opción	
    	 	if(val.indexOf(filter) > -1 && event.key != 'Enter'){
    	 		elem.style.display = "block";
    	 		applyOutside = true;
    	 	}else{
    	 		elem.style.display = "none";
    	 	}
    	 	if(val == filter){
    	 		console.log(elem.value);
                parentApp.pedido_id = elem.value;
                parentApp.updateProductsContestsFilter();
                banishItems();
    	 	}
    	 }
	}
	function banishItems(){
	let	container = document.getElementById('model-id');
    let	list = container.getElementsByClassName("option-category"); // Lista de las opciones para el valor de comms
	   for(var i = 0; i< list.length; i++){
	   	list[i].style.display = 'none';
	   }
	}
    	function keyMonitor(event){
		// console.log(event, type);
		let container = document.getElementById('model-id');
		let list = container.getElementsByClassName('option-category');
		let input =  document.getElementById('filter-' + parentApp.type);
		let selected = '';
		if(event.key == "ArrowDown"){	
			parentApp.counterKey++;
			let previous = (parentApp.counterKey == 0)? 0 : parentApp.counterKey;
			if(previous > 0){
				list[previous -= 1].style.backgroundColor = '#fff';
			}
			list[parentApp.counterKey].style.backgroundColor = "#3EA6E2";
		}
		if(event.key == "ArrowUp"){
			if(parentApp.counterKey == 0){
				parentApp.counterKey = 0
			}else{
				parentApp.counterKey--;
				let next = (parentApp.counterKey == 0)? 0 : parentApp.counterKey;
				list[next += 1].style.backgroundColor = '#fff';
				list[parentApp.counterKey].style.backgroundColor = "#3EA6E2";
			}
		}
		if(event.key == "Enter"){
			parentApp.pedido_id = list[parentApp.counterKey].getAttribute('value');
			for (var i = 0; i < list.length; i++) {
				list[i].style.display = "none";
			}
			input.value = list[parentApp.counterKey].innerHTML;
		}
	}
	function emptyCell(type) {
		if(type != ''){
			let inputCell = document.getElementsByClassName('category-input')[0];
			console.log(type);
            let category = ''
			switch(type){
				case 'Canasta':
                 category = 'Categoria';
				break;
				case 'Brand':
                  category = 'Marca';
				break;
				case 'Fanpage':
                  category = 'Fanpage';
				break;
				case 'YoutubeChannel':
				  category = 'YoutubeChannel';
				break;
				case 'InstaProfile':
				  category = 'InstaProfile';
				break;
			}
			inputCell.placeholder = "Filtrar " + category ;
			inputCell.value = null;
		}
	}

		function selectCategoryFilter(type, optionSlug, categoryValue){
		parentApp.pedido_id = categoryValue;
		parentApp.updateProductsContestsFilter();
		console.log(type + "-------" + parentApp.pedido_id + " ---- " +  optionSlug);
		
		let input = document.getElementById("filter-"+type);
        input.value = optionSlug;
        banishItems();
	}
</script>
@stop
@section('content')
<style>
	optgroup{
		border-color: red;
		border-style: solid;
		border-width: 3px;
		background-color: blue;
	}
</style>
@yield('consultdescription')


<div class="container-m" >

	<div id="comms-charts" class="row">

		<h5>Realizar consulta:</h5>

		<div class="input-field col s6">
			{{-- <datepicker id="since" v-model="since" name="since" class='datepicker' type="text" placeholder="Y-m-d" v-on:set-date="setDate" v-bind:date="since"> --}}
			<input id="since" type="date" v-model="since"  placeholder="Fecha inicial: Y-m-d" v-on:set-date="setDate" v-bind:date="since">
			 
			</div>  

			<div class="input-field col s6">
			{{-- 	<datepicker id="until" v-model="until" name="until" class='datepicker' type="text" placeholder="Y-m-d" v-on:set-date="setDate" v-bind:date="until"> --}}
			<input id="until" type="date" v-model="until"   placeholder="Fecha final: Y-m-d" v-on:set-date="setDate" v-bind:date="until">
		 
				</div> 


				<div class="filtros-graph row">
					<div class="col s6">
						<label for="first_name">Filtrar Fecha:</label>
						<select v-model="interval" v-on:change="setDate(interval)" class="browser-default"  name="interval"   id="">
							<optgroup label="" >
								<option value="all">Todo</option>
								<option value="manual">Seleccionar fechas</option>
							</optgroup>
							<optgroup label="" >
								<option value="hoy">hoy</option>
								<option value="day">El mismo Día</option>
							</optgroup>
							<optgroup label="">
								<option value="6">Semana anterior</option>
								<option value="28">28 días anteriores</option>
								<option value="30">30 días anteriores</option>
							</optgroup>
							<optgroup label="">
								<option value="90">trimestre anterior (90 días)</option>
								<option value="182">semestre anterior (182 días)</option>
								<option value="365">año anterior</option>
							</optgroup>
						</select>

					</div>
					<div class="col s6">
						<label for="first_name">Filtrar Ubicación:</label>
						<select class="browser-default" v-model="country" name="country" id="">
							<option value="all"> Todos los países </option>
							<option v-for="option in allCountries"><%option%></option>
						</select>
					</div>
				</div> 
				<div class="filtros-graph row">
					<div class="col s6">
						<label for="first_name">Filtrar Scope: (seleccionar el alcance de la consulta y la opción deseada)</label>
						<select class="browser-default" v-model="type" name="type" id="model-type" @change="emptyCell(type)">
							{{-- <option value="Vertical">Vertical</option>
							<option value="Categoria">Categoría</option> --}}
							<option value="Canasta">Canasta</option>
							<option value="Brand">Marca</option>
							<option value="Fanpage">Fanpage</option>
							<option value="YoutubeChannel">Channel</option>
							<option value="InstaProfile">Profile</option>
							<option value="AdSet">AdSet</option>
						</select>

					</div>

					<div class="col s6">
						<div class='adjust-list browser-default' id="model-id">
							<input v-bind:id="'filter-'+type" list='lista_ids' name='pedido_id' type="text"  class='category-input' v-bind:placeholder="'Filtrar ...'" @keyup="filterCategories($event, type)" @keydown="keyMonitor($event)" >
							<div class="categories-container">
								<option v-for="option in allElements[type]" class='option-category' v-bind:value="option.id"  @click="selectCategoryFilter(type, option.name, option.id)"><%option.name%></option>
							</div>
						</div>
					</div>	
					<div class="col s6 adjust-list">
						<label for="first_name">Filtrar Red Social:</label>
						<select class='browser-default' v-model="social_network" placeholder="Red social" id="model-id">
							{{}}
							@if(strpos($_SERVER['REQUEST_URI'], '/comms/consult') === false)
							<option value="all">Todos</option>
							@endif    
							<option value="Facebook">Facebook</option>
							<option value="YouTube">Youtube</option>
							<option value="Instagram">Instagram</option>
						</select>

					</div>
					<div class="col s6 adjust-list">
						<label for="first_name">Filtra actividad:</label>
						<select class='browser-default' v-model="activity" placeholder="Actividad" id="active-id">
							 
							@if(strpos($_SERVER['REQUEST_URI'], '/comms/consult') === false)
							<option value="published">Fecha de Publicación</option>
							@endif    
							<option value="active">Actividad de Post</option>
						</select>

					</div>		
				</div>		

				{{-- FILTROS DE COMMS --}}
				<div class="row" >
					@include('comms.filter-comms-selector', array('nameVueApp'=>'parentApp'))
				</div>

				<br>
				<button class='consult-btn btn'  @click="getConsult()">Consultar</button>
				<br>
				<span style="display:  none;"> <%type%> <%pedido_id%> <%since%> -- <%until%></span>
				<br>
				<button style="display: none;" @click="getArraysOfData(to)"> DATA</button>

				<br>	
				<div class="row comms-display">
					<h4>Resultado de la Consulta  
						{{-- <a class="btn btn-primary green tooltipped" v-bind:href="'{{url('excel-comms/download')}}/'+type+'/'+pedido_id+'/'+country+'/'+since+'/'+until" data-position="bottom" data-delay="50" data-tooltip="Descargar"  style="margin-left: 30px;"><i class="material-icons">file_download</i></a> --}}
					</h4>
					<div class="row url-box super-align">
						<a class="btn " @click="getConsultURL()">Copiar Consulta</a>	
						<input type="text" id="share-url" class="hide">
					</div>



					<select v-if="mostrarTo===true" class="browser-default total-filter" v-model="to" name="to" id="">
						{{-- v-on:change="getArraysOfData(to)" --}}
						<option disabled="disabled">Mostrar:</option>
						{{-- <option value="Vertical">Vertical</option>
						<option value="Categoria">Categoría</option> --}}
						<option value="Canasta">Canasta</option>
						<option value="Brand">Marca</option>
						<option v-if='social_network == "Facebook"' value="Fanpage">Fanpage</option>
						<option v-if='social_network == "YouTube"' value="YoutubeChannel">Channel</option>
						<option v-if='social_network == "Instagram"' value="InstaProfile">Profile</option>
						<option value="AdSet">AdSet</option>
					</select>
				</div>

				@yield('consultcontent')
			</div>
		</div>
		@yield('consulttemplate')
		@yield('consultstyles')

		@stop