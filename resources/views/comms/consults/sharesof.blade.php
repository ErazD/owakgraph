@extends('comms.consults.base-consult')

@section('consultscripts')
<script src="{{ asset('js/consults/sharesof-chart.js') }}"></script>
@stop

@section('consultdescription')
<div class="row grey lighten-3 mb-0">
	<div class="col s12 m12 l12 page-title">
		<h2 class="breadcrumbs-title left">
		Share of Rates</h2>
	</div>
</div>
<div class="row blue darken-3">
	<div class="col s12 m12 l12 page-title">
		<p class="white-text" style="width: 80%;"> 

			En esta sección pueden consultarse los valores acumulados absolutos de reacciones, comentarios, compartidos y views que han tenido las marcas dentro de un rango definido de tiempo.
		</p>
	</div>
</div>
@stop

@section('consultcontent')
<h6 v-if="allData.metrics" >
	<span v-if="social_network=='all' || social_network=='Facebook'">
		<b>FACEBOOK:</b> 
	<b>Vistas:</b> <%formatNumber(allData.metrics.fb_views)%> <b>| Reacciones:</b> <%formatNumber(allData.metrics.fb_reactions)%> y <%formatNumber(allData.metrics.fb_reactions_n)%> <b>| Comentarios:</b> <%formatNumber(allData.metrics.fb_comments)%> y <%formatNumber(allData.metrics.fb_comments_n)%> <b>| Compartidos:</b> <%formatNumber(allData.metrics.fb_shares)%> y <%formatNumber(allData.metrics.fb_shares_n)%>
	</span><br>
	
</h6>
<h6 v-if="allData.metrics" >
  <span v-if="social_network=='all' || social_network=='YouTube'">
	<b>YOUTUBE:</b> 
	<b>Vistas:</b> <%formatNumber(allData.metrics.yt_views)%> <b>| Likes:</b> <%formatNumber(allData.metrics.yt_likes)%> y <%formatNumber(allData.metrics.yt_likes_n)%> <b>| Dislikes:</b> <%formatNumber(allData.metrics.yt_dislikes)%> y <%formatNumber(allData.metrics.yt_dislikes_n)%><b>| Comentarios:</b> <%formatNumber(allData.metrics.yt_comments)%> y <%formatNumber(allData.metrics.yt_comments_n)%> 
	</span>
</h6>
<h6 v-if="allData.metrics" >
  <span v-if="social_network=='all' || social_network=='Instagram'">
	<b>INSTAGRAM:</b> 
	<b>Vistas:</b> <%formatNumber(allData.metrics.insta_views)%> <b>| Likes:</b> <%formatNumber(allData.metrics.insta_likes)%> y <%formatNumber(allData.metrics.insta_likes_n)%> <b>| Comentarios:</b> <%formatNumber(allData.metrics.insta_comments)%> y <%formatNumber(allData.metrics.insta_comments_n)%> 
	</span>
</h6>  
<consult-sharesof name="sharesof-comms" v-bind:class='(mostrarResultados === true)? "show": "hide"'></consult-sharesof>
<h6 v-if='mostrarResultados === false'>No hay datos para mostrar</h6>
@stop

@section('consulttemplate')
<template id="consult-sharesof">
<div class="sharesof row">
	<div class="row" v-if="parentApp.social_graphs=='all' || parentApp.social_graphs=='Facebook'">
	<div class="chart-container col s12 m12 l4 xl3 flex"> 
		<div class="sharesof-chart" id="grafica-shares-of-fb-reactions"></div>
	</div>
	<div class="chart-container col s12 m12 l4 xl3 flex"> 
		<div class="sharesof-chart" id="grafica-shares-of-fb-comments"></div>
	</div>
	<div class="chart-container col s12 m12 l4 xl3 flex">
		<div class="sharesof-chart" id="grafica-shares-of-fb-shares"></div>
	</div>
	<div class="chart-container col s12 m12 l4 xl3 flex"> 
		<div class="sharesof-chart" id="grafica-shares-of-fb-views"></div>
	</div>
	</div>
	
	<div class="row" v-if="parentApp.social_graphs=='all' || parentApp.social_graphs=='YouTube'">
	<div class="chart-container col s12 m12 l4 xl3 flex"> 
		<div class="sharesof-chart" id="grafica-shares-of-yt-likes"></div>
	</div>
	<div class="chart-container col s12 m12 l4 xl3 flex">
		<div class="sharesof-chart" id="grafica-shares-of-yt-dislikes"></div>
	</div>
	<div class="chart-container col s12 m12 l4 xl3 flex"> 
		<div class="sharesof-chart" id="grafica-shares-of-yt-comments"></div>
	</div>
	<div class="chart-container col s12 m12 l4 xl3 flex"> 
		<div class="sharesof-chart" id="grafica-shares-of-yt-views"></div>
	</div>
	</div>

	<div class="row" v-if="parentApp.social_graphs=='all' || parentApp.social_graphs=='Instagram'">
	<div class="chart-container col s12 m12 l4 xl3 flex"> 
		<div class="sharesof-chart" id="grafica-shares-of-insta-likes"></div>
	</div>
	<div class="chart-container col s12 m12 l4 xl3 flex"> 
		<div class="sharesof-chart" id="grafica-shares-of-insta-comments"></div>
	</div>
	<div class="chart-container col s12 m12 l4 xl3 flex"> 
		<div class="sharesof-chart" id="grafica-shares-of-insta-views"></div>
	</div>
	</div>

</div>
</template>
@stop


@section('consultstyles')
<style>
optgroup{
	border-color: red;
	border-style: solid;
	border-width: 3px;
	background-color: blue;
}

body, html{
	overflow: initial;
}
#sharesof-app h5{
	margin-left: 9px;
}
.sharesof-chart{
	width: 100%;
	display:block !important;
	height: 650px;
	margin:10px auto 20px !important; 
}
.sharesof-display{
	margin-top:1%;
	padding:15px;
}
.sharesof-display h4{
	display:inline-block;
	vertical-align:middle;
}
.sharesof-display select{
	display:inline-block;
	vertical-align:middle;
	width:40%;
	margin-left:20px;
	float:right;
	margin:8px 0 10px 0;
}
.txt-info{
	font-size: 15px;
	font-weight: 100;
}
h5.no-data{
	text-align: center;
	font-weight: 900;
}
.center-content{
	text-align:center;
}
.sharesof{
	margin:6% 0 0 0;
}
 
@media only screen and (max-width: 1360px) {
	.sharesof .chart-container{
		width:100% !important;
	}
}

@media only screen and (min-width: 1360px) {
	.sharesof .chart-container{
		width:33% !important;
	}
}
.consult-info{
	font-size:1.5em;
}
.consult-btn{
	margin-left: 14px !important;
}
</style>
@stop
