@extends('comms.consults.base-consult')

@section('consultscripts')
	<script src="https://cdn.anychart.com/releases/8.2.1/js/anychart-base.min.js"></script>
	<script src="https://cdn.anychart.com/releases/8.2.1/js/anychart-ui.min.js"></script>
	<script src="https://cdn.anychart.com/releases/8.2.1/js/anychart-exports.min.js"></script>
	<script src="https://cdn.anychart.com/releases/8.2.1/js/anychart-heatmap.min.js"></script>
	<script src="https://cdn.anychart.com/releases/8.2.1/js/anychart-data-adapter.min.js"></script>
	<link rel="stylesheet" href="https://cdn.anychart.com/releases/8.2.1/css/anychart-ui.min.css" />
	<link rel="stylesheet" href="https://cdn.anychart.com/releases/8.2.1/fonts/css/anychart-font.min.css" />
	<script src="{{ asset('js/consults/heatmap-chart.js') }}"></script>
@stop

@section('consultdescription')
	<div class="row grey lighten-3 mb-0">
		<div class="col s12 m12 l12 page-title">
			<h2 class="breadcrumbs-title left">
				Heatmap</h2>
		</div>
	</div>
	<div class="row blue darken-3">
		<div class="col s12 m12 l12 page-title">
			<p class="white-text" style="width: 80%;"> 
				En esta sección puede consultarse la intensidad hora a hora de la actividad para una marca.
			</p>
		</div>
	</div>
@stop

@section('consultcontent')
	<h6 v-if="allData.info"><%formatNumber(allData.cant)%> posts analizados para <% actual_type %>  durante el período <span class="txt-info">(<% since %> - <% until%>)</span>   
	</h6>
	<consult-heatmap name="consult-heatmap" v-bind:class='(mostrarResultados === true)? "show": "hide"'></consult-heatmap>
@stop

@section('consulttemplate')
	<template id="consult-heatmap">
	<div class="intensity row">
			<div v-if='parentApp.social_network == "Facebook" || parentApp.social_network == "all"' class="chart-container col s12 m12 l12 xl6 flex"> 
				<div class="intensity-chart" id="container-fb_reactions"></div>
			</div>
			<div v-if='parentApp.social_network == "Facebook" || parentApp.social_network == "all"' class="chart-container col s12 m12 l12 xl6 flex"> 
				<div class="intensity-chart" id="container-fb_comments"></div>
			</div>
			<div v-if='parentApp.social_network == "Facebook" || parentApp.social_network == "all"' class="chart-container col s12 m12 l12 xl6 flex">
				<div class="intensity-chart" id="container-fb_shares"></div>
			</div>
			<div v-if='parentApp.social_network == "Facebook" || parentApp.social_network == "all"' class="chart-container col s12 m12 l12 xl6 flex"> 
				<div class="intensity-chart" id="container-fb_views"></div>
			</div>
		 
			<div v-if='parentApp.social_network == "YouTube" || parentApp.social_network == "all"' class="chart-container col s12 m12 l12 xl6 flex"> 
				<div class="intensity-chart" id="container-yt_views"></div>
			</div>
			<div v-if='parentApp.social_network == "YouTube" || parentApp.social_network == "all"' class="chart-container col s12 m12 l12 xl6 flex"> 
				<div class="intensity-chart" id="container-yt_likes"></div>
			</div>
			<div v-if='parentApp.social_network == "YouTube" || parentApp.social_network == "all"' class="chart-container col s12 m12 l12 xl6 flex">
				<div class="intensity-chart" id="container-yt_comments"></div>
			</div>
			<div v-if='parentApp.social_network == "YouTube" || parentApp.social_network == "all"' class="chart-container col s12 m12 l12 xl6 flex"> 
				<div class="intensity-chart" id="container-yt_dislikes"></div>
			</div>

			<div v-if='parentApp.social_network == "Instagram" || parentApp.social_network == "all"' class="chart-container col s12 m12 l12 xl6 flex"> 
				<div class="intensity-chart" id="container-insta_likes"></div>
			</div>
			<div v-if='parentApp.social_network == "Instagram" || parentApp.social_network == "all"' class="chart-container col s12 m12 l12 xl6 flex">
				<div class="intensity-chart" id="container-insta_comments"></div>
			</div>
			<div v-if='parentApp.social_network == "Instagram" || parentApp.social_network == "all"' class="chart-container col s12 m12 l12 xl6 flex"> 
				<div class="intensity-chart" id="container-insta_views"></div>
			</div>
		</div>
	</template>
@stop

@section('consultstyles')
	<style>
		optgroup{
			border-color: red;
			border-style: solid;
			border-width: 3px;
			background-color: blue;
		}

		body, html{
			overflow: initial;
		}
		#sharesof-app h5{
			margin-left: 9px;
		}
		.intensity-chart{
			width: 100%;
			display:block !important;
			height: 350px;
			margin:10px auto 20px !important; 
		}
		.intensity-display{
			margin-top:1%;
			padding:15px;
		}
		.intensity-display h4{
			display:inline-block;
			vertical-align:middle;
		}
		.intensity-display select{
			display:inline-block;
			vertical-align:middle;
			width:40%;
			margin-left:20px;
			float:right;
			margin:8px 0 10px 0;
		}
		.txt-info{
			font-size: 15px;
			font-weight: 100;
		}
		h5.no-data{
			text-align: center;
			font-weight: 900;
		}
		.center-content{
			text-align:center;
		}
		.sharesof{
			margin:6% 0 0 0;
		}
		@media only screen and (max-width: 1360px) {
			.intensity .chart-container{
				width:100% !important;
			}
		}

		@media only screen and (min-width: 1360px) {
			.intensity .chart-container{
				width:49% !important;
			}
		}
		.consult-info{
			font-size:1.5em;
		}
		.consult-btn{
			margin-left: 14px !important;
		}
</style>
@stop