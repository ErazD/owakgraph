@extends('comms.consults.base-consult')

@section('consultscripts')
<script src="{{ asset('js/consults/comms-rates-by-post.js') }}"></script>

<script>
	function formatSelected(num, type){
		if("reactions shares comments views".includes(type)){
			return formatNumber(num);
		}else if("total_amplification_rate total_participation_rate total_reaction_rate".includes(type)){
			return num.toFixed(2)+'%';
		}else{
			return num;
		}
	}
</script>

<script>
		function selectElementContents(el) {
			var body = document.body, range, sel;
			if (document.createRange && window.getSelection) {
				range = document.createRange();
				sel = window.getSelection();
				sel.removeAllRanges();
				try {
					range.selectNodeContents(el);
					sel.addRange(range);
				} catch (e) {
					range.selectNode(el);
					sel.addRange(range);
				}
			} else if (body.createTextRange) {
				range = body.createTextRange();
				range.moveToElementText(el);
				range.select();
			}
		}


	</script>
@stop

@section('consultdescription')
<div class="row grey lighten-3 mb-0">
	<div class="col s12 m12 l12 page-title">
		<h2 class="breadcrumbs-title left">
		Top Posts</h2>
	</div>
</div>
<div class="row blue darken-3">
	<div class="col s12 m12 l12 page-title">
		<p class="white-text" style="width: 80%;">
			En esta sección pueden consultarse diferentes tasas de actividad de posts  en un período determinado de tiempo. (Para una mejor visualización de los datos hacer consultas de máximo 60 días).
		</p>
	</div>
</div>
@stop

@section('consultcontent')
<div v-if="allData.metrics">
	<h4 v-if="social_network=='Facebook && Instagram'"><%formatNumber(allData.metrics.cant)%> posts analizados durante el periodo (<%since%> - <%until%>)</h4>
	<h6 v-if="social_network=='Facebook'"><b>Vistas:</b> <%formatNumber(allData.metrics.fb_views)%> <b>| Reacciones:</b> <%formatNumber(allData.metrics.fb_reactions)%> y <%formatNumber(allData.metrics.fb_reactions_n)%> <b>| Comentarios:</b> <%formatNumber(allData.metrics.fb_comments)%> y <%formatNumber(allData.metrics.fb_comments_n)%> <b>| Compartidos:</b> <%formatNumber(allData.metrics.fb_shares)%> y <%formatNumber(allData.metrics.fb_shares_n)%></h6>
	<h4 v-if="social_network=='YouTube'"><%formatNumber(allData.metrics.cant)%> videos analizados durante el periodo (<%since%> - <%until%>)</h4>
	<h6 v-if="social_network=='YouTube'">
	<b>Vistas:</b> <%formatNumber(allData.metrics.yt_views)%> <b>| Likes:</b> <%formatNumber(allData.metrics.yt_likes)%> y <%formatNumber(allData.metrics.yt_likes_n)%> <b>| Dislikes:</b> <%formatNumber(allData.metrics.yt_dislikes)%> y <%formatNumber(allData.metrics.yt_dislikes_n)%><b>| Comentarios:</b> <%formatNumber(allData.metrics.yt_comments)%> y <%formatNumber(allData.metrics.yt_comments_n)%> </h6> 
	<h6 v-if="social_network=='Instagram'">
	<b>Vistas:</b> <%formatNumber(allData.metrics.insta_views)%> <b>| Likes:</b> <%formatNumber(allData.metrics.insta_likes)%> y <%formatNumber(allData.metrics.insta_likes_n)%> <b>| Comentarios:</b> <%formatNumber(allData.metrics.insta_comments)%> y <%formatNumber(allData.metrics.insta_comments_n)%> </h6> 
</div>


<a class="btn btn-primary green" onclick="selectElementContents(document.getElementById('result-top-posts') );"><i class="material-icons">content_copy</i></a>

<consult-top-posts name="consult-top-posts" v-bind:class='(mostrarResultados === true)? "show": "hide"'></consult-top-posts>
<h6 v-if='mostrarResultados === false' >No hay datos para mostrar</h6>
@stop

@section('consulttemplate')
<template id="consult-top-posts">
	<div id="posiciones" v-if="globalPositions[0]">
		<table class="top-posts" id="result-top-posts">
			<tr><th>#</th> <th v-if="parentApp.social_network=='Facebook'">FB</th>
            <th v-if="parentApp.social_network=='YouTube'">YT</th>
            <th v-if="parentApp.social_network=='Instagram'">INSTGR</th>
			 <th>Metrics</th> 
			 <th class="blue lighten-3 blue-grey-text text-darken-3" v-if="globalPositionsKey"><%globalPositionsKey.name%></th>
				<th v-if="parentApp.social_network=='Facebook'" class="sort-selectable sort-selectable-name pointer" @click="sortGlobalPositions('name', 'Fanpage')">Fanpage</th> 

				<th v-if="parentApp.social_network=='YouTube'" class="sort-selectable sort-selectable-name pointer" @click="sortGlobalPositions('name', 'YoutubeChannel')">Canal</th> 
				<th v-if="parentApp.social_network=='Instagram'" class="sort-selectable sort-selectable-name pointer" @click="sortGlobalPositions('name', 'InstaProfile')">Perfil</th> 
				
				<th class="sort-selectable sort-selectable-published pointer" @click="sortGlobalPositions('published', 'Published')">Publicación</th>
                
				<th  v-if="parentApp.social_network=='Facebook'" class="sort-selectable sort-selectable-reactions pointer" @click="sortGlobalPositions('fb_reactions', 'Reacciones')">Reacciones</th> 
				<th v-if="parentApp.social_network=='Facebook'" class="sort-selectable sort-selectable-shares pointer" @click="sortGlobalPositions('fb_shares', 'Compartidos')">Compartidos</th> 
				<th v-if="parentApp.social_network=='Facebook'" class="sort-selectable sort-selectable-comments pointer" @click="sortGlobalPositions('fb_comments', 'Comentarios')">Comentarios</th> 
				<th v-if="parentApp.social_network=='Facebook'" class="sort-selectable sort-selectable-views pointer tooltipped" @click="sortGlobalPositions('fb_views', 'Vistas')" data-position="top" data-delay="50" data-tooltip="Solo incluye publicaciones de video">Vistas*</th>
                 
                <th v-if="parentApp.social_network=='YouTube'" class="sort-selectable sort-selectable-reactions pointer" @click="sortGlobalPositions('yt_views', 'Vistas')">Vistas</th> 
				<th v-if="parentApp.social_network=='YouTube'" class="sort-selectable sort-selectable-shares pointer" @click="sortGlobalPositions('yt_likes', 'Likes')">Likes</th> 
				<th v-if="parentApp.social_network=='YouTube'" class="sort-selectable sort-selectable-comments pointer" @click="sortGlobalPositions('yt_comments', 'Comentarios')">Comentarios</th> 
				<th v-if="parentApp.social_network=='YouTube'" class="sort-selectable sort-selectable-views pointer" @click="sortGlobalPositions('yt_dislikes', 'Dislikes')" >Dislikes</th>

				 <th v-if="parentApp.social_network=='Instagram'" class="sort-selectable sort-selectable-reactions pointer" @click="sortGlobalPositions('insta_views', 'Vistas')">Vistas</th> 
				<th v-if="parentApp.social_network=='Instagram'" class="sort-selectable sort-selectable-shares pointer" @click="sortGlobalPositions('insta_likes', 'Likes')">Likes</th> 
				<th v-if="parentApp.social_network=='Instagram'" class="sort-selectable sort-selectable-comments pointer" @click="sortGlobalPositions('insta_comments', 'Comentarios')">Comentarios</th> 
                
				<th class="sort-selectable sort-selectable-segment pointer" @click="sortGlobalPositions('segment', 'Segmento')">Segmento</th>
				<th class="sort-selectable sort-selectable-product pointer" @click="sortGlobalPositions('product', 'Producto')">Producto</th>
				<th class="sort-selectable sort-selectable-comunication_task pointer" @click="sortGlobalPositions('comunication_task', 'Tarea de comunicación')">Tarea de comunicación</th> 
					
              
				<th v-if="parentApp.social_network=='Facebook'" class="sort-selectable sort-selectable-total_amplification_rate pointer" @click="sortGlobalPositions('total_amplification_rate', 'Tasa de amplificación')">Tasa de amplificación</th> 
				<th v-if="parentApp.social_network=='Facebook'" class="sort-selectable sort-selectable-total_participation_rate pointer" @click="sortGlobalPositions('total_participation_rate', 'Tasa de participación')">Tasa de participación</th> 
				<th v-if="parentApp.social_network=='Facebook'" class="sort-selectable sort-selectable-total_reaction_rate pointer tooltipped" @click="sortGlobalPositions('total_reaction_rate', 'Tasa de reacción')" data-position="top" data-delay="50" data-tooltip="Solo incluye publicaciones de video">Tasa de reacción* </th>

				<th v-if="parentApp.social_network=='YouTube'" class="sort-selectable sort-selectable-total_amplification_rate pointer" @click="sortGlobalPositions('yt_amplification_rate', 'Tasa de amplificación')">Tasa de amplificación</th> 
				<th v-if="parentApp.social_network=='YouTube'" class="sort-selectable sort-selectable-total_participation_rate pointer" @click="sortGlobalPositions('yt_participation_rate', 'Tasa de participación')">Tasa de participación</th> 
				<th v-if="parentApp.social_network=='YouTube'" class="sort-selectable sort-selectable-total_reaction_rate pointer tooltipped" @click="sortGlobalPositions('yt_reaction_rate', 'Tasa de reacción')" data-position="top" data-delay="50" data-tooltip="Solo incluye publicaciones de video">Tasa de reacción* </th>

				<th v-if="parentApp.social_network=='Instagram'" class="sort-selectable sort-selectable-total_amplification_rate pointer" @click="sortGlobalPositions('insta_amplification_rate', 'Tasa de amplificación')">Tasa de amplificación</th> 
				<th v-if="parentApp.social_network=='Instagram'" class="sort-selectable sort-selectable-total_participation_rate pointer" @click="sortGlobalPositions('insta_participation_rate', 'Tasa de participación')">Tasa de participación</th> 
				<th v-if="parentApp.social_network=='Instagram'" class="sort-selectable sort-selectable-total_reaction_rate pointer tooltipped" @click="sortGlobalPositions('insta_reaction_rate', 'Tasa de reacción')" data-position="top" data-delay="50" data-tooltip="Solo incluye publicaciones de video">Tasa de reacción* </th>
			</tr>
			<tr v-for="(ele, key) in globalPositions">
				<th><%key+1%></th>
				<td v-if="parentApp.social_network=='Facebook'"><a v-bind:href="'https://www.facebook.com/'+ele['facebook_id']" target="_blank"><i class="fa fa-facebook-square social-icon"></i></a></td>

				<td v-if="parentApp.social_network=='Facebook'"><a v-bind:href="'{{url('posts')}}/'+ele['id']" target="_blank"><i class="material-icons">insert_link</i></a></td>
               
               	<td v-if="parentApp.social_network=='YouTube'"><a v-bind:href="'https://www.youtube.com/watch?v='+ele['youtube_id']" target="_blank"><i class="fa fa-youtube-play social-icon"></i></a></td>

				<td v-if="parentApp.social_network=='YouTube'"><a v-bind:href="'{{url('youtube-video')}}/'+ele['id']" target="_blank"><i class="material-icons">insert_link</i></a></td>

				<td v-if="parentApp.social_network=='Instagram'"><a v-bind:href="'https://www.instagram.com/p/'+ele['instagram_id']" target="_blank"><i class="fa fa-instagram social-icon"></i></a></td>

				<td v-if="parentApp.social_network=='Instagram'"><a v-bind:href="'{{url('instagram-post')}}/'+ele['id']" target="_blank"><i class="material-icons">insert_link</i></a></td>

				<td class="blue lighten-5" v-if="globalPositionsKey"><% formatNumber(formatSelected(ele[globalPositionsKey.key], globalPositionsKey.key))%></td>

				<td class="sort-selectable sort-selectable-name"><%ele.name%></td> 
				<td class="sort-selectable sort-selectable-published"><%ele.published%></td> 

				<td  v-if="parentApp.social_network=='Facebook'" class="sort-selectable sort-selectable-reactions"><%formatNumber(ele.fb_reactions)%></td> 
				<td v-if="parentApp.social_network=='Facebook'" class="sort-selectable sort-selectable-shares"><%formatNumber(ele.fb_shares)%></td> 
				<td v-if="parentApp.social_network=='Facebook'" class="sort-selectable sort-selectable-comments"><%formatNumber(ele.fb_comments)%></td> 
				<td v-if="parentApp.social_network=='Facebook'" class="sort-selectable sort-selectable-views"><%formatNumber(ele.fb_views)%></td> 

				<td  v-if="parentApp.social_network=='YouTube'" class="sort-selectable sort-selectable-reactions"><%formatNumber(ele.yt_views)%></td> 
				<td v-if="parentApp.social_network=='YouTube'" class="sort-selectable sort-selectable-shares"><%formatNumber(ele.yt_likes)%></td> 
				<td v-if="parentApp.social_network=='YouTube'" class="sort-selectable sort-selectable-comments"><%formatNumber(ele.yt_comments)%></td> 
				<td v-if="parentApp.social_network=='YouTube'" class="sort-selectable sort-selectable-views"><%formatNumber(ele.yt_dislikes)%></td> 

				<td  v-if="parentApp.social_network=='Instagram'" class="sort-selectable sort-selectable-reactions"><%formatNumber(ele.insta_views)%></td> 
				<td v-if="parentApp.social_network=='Instagram'" class="sort-selectable sort-selectable-shares"><%formatNumber(ele.insta_likes)%></td> 
				<td v-if="parentApp.social_network=='Instagram'" class="sort-selectable sort-selectable-comments"><%formatNumber(ele.insta_comments)%></td> 

				<td class="sort-selectable sort-selectable-segment"><%ele.segment%></td>
				<td class="sort-selectable sort-selectable-product"><%ele.product%></td>
				<td class="sort-selectable sort-selectable-comunication_task"><%ele.comunication_task%></td>

				<td v-if="parentApp.social_network=='Facebook'" class="sort-selectable sort-selectable-total_amplification_rate"><%ele.total_amplification_rate.toFixed(2)%>%</td> 
				<td v-if="parentApp.social_network=='Facebook'" class="sort-selectable sort-selectable-total_participation_rate"><%ele.total_participation_rate.toFixed(2)%>%</td> 
				<td v-if="parentApp.social_network=='Facebook'" class="sort-selectable sort-selectable-total_reaction_rate"><%ele.total_reaction_rate.toFixed(2)%>%</td>


				<td v-if="parentApp.social_network=='YouTube'" class="sort-selectable sort-selectable-total_amplification_rate"><%ele.yt_amplification_rate%></td> 
				<td v-if="parentApp.social_network=='YouTube'"  class="sort-selectable sort-selectable-total_participation_rate"><%ele.yt_participation_rate.toFixed(2)%>%</td> 
				<td v-if="parentApp.social_network=='YouTube'" class="sort-selectable sort-selectable-total_reaction_rate"><%ele.yt_reaction_rate.toFixed(2)%>%</td>

				<td v-if="parentApp.social_network=='Instagram'" class="sort-selectable sort-selectable-total_amplification_rate"><%ele.insta_amplification_rate%></td> 
				<td v-if="parentApp.social_network=='Instagram'"  class="sort-selectable sort-selectable-total_participation_rate"><%ele.insta_participation_rate.toFixed(2)%>%</td> 
				<td v-if="parentApp.social_network=='Instagram'" class="sort-selectable sort-selectable-total_reaction_rate"><%ele.insta_reaction_rate.toFixed(2)%>%</td>

				 
			</tr>
		</table>
	</div> 
</template>
@stop

@section('consultstyles')
<style>
optgroup{
	border-color: red;
	border-style: solid;
	border-width: 3px;
	background-color: blue;
}

table.top-posts{
	border-style: solid;
	border-width: 1px;
	margin-top: 30px;
	margin-bottom: : 30px;
}

table.top-posts  th{
	color: white;
	background-color: #0277bd; 
}
/*table.top-posts.yt-top th{
	color: white;
	background-color: #af052d; 
}*/
table.top-posts th, table.top-posts td{
	text-align: center;
	border-style: solid;
	border-width: 1px;
	border-color: #ffffff99;
	padding: 8px;
} 
table.top-posts th.selected{
	background-color: #b0bec5;
	color: #37474f;
}
table.top-posts td.selected{
	background-color: #eceff1;
}
.pointer{
	cursor: pointer;
}

.social-icon{
	font-size: 2em;
}
</style>
@stop