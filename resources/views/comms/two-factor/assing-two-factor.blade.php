@extends('layouts.app')

@section('scripts')

<script>
	var allColors = <?php echo json_encode(App\Http\Controllers\ColorController::getColorList('products', 'segmentos')); ?>;
	console.log(allColors);

	function getAllElements(){
		var all = [];		
		var temps = [];		
		@foreach($allElements['fanpages'] as $fanpage)
		var dict = {'id': {{$fanpage->id}}, 'name': '{{html_entity_decode($fanpage->brand->name, ENT_QUOTES | ENT_XML1, 'UTF-8')}}  {{$fanpage->name}}', 'slug': '{{$fanpage->id}}'};
		temps.push(dict);
		@endforeach
		all['Fanpage'] = temps;

		temps = [];		
		@foreach($allElements['brands'] as $brand)
		var dict = {'id': {{$brand->id}}, 'name': '{{html_entity_decode($brand->name, ENT_QUOTES | ENT_XML1, 'UTF-8')}}', 'slug': '{{$brand->slug}}'};
		temps.push(dict);
		@endforeach
		all['Brand'] = temps;

		temps = [];		
		@foreach($allElements['baskets'] as $basket)
		var dict = {'id': {{$basket->id}}, 'name': '{{$basket->name}}', 'slug': '{{$basket->slug}}'};
		temps.push(dict);
		@endforeach
		all['Canasta'] = temps;

		temps = [];		
		@foreach($allElements['adsets'] as $adset)
		var dict = {'id': {{$adset->id}}, 'name': '{{$adset->name}}', 'slug': '{{$adset->slug}}'};
		temps.push(dict);
		@endforeach
		all['AdSet'] = temps;

		console.log(all);
		return all;
	}

</script>

<script src="{{ asset('js/vue.min.js') }}"></script>
<script src="{{ asset('js/moment.js') }}"></script>
<script src="{{ asset('js/components/datepicker.js') }}"></script>	
<script src="{{ asset('js/assing-two-factor.js') }}"></script>
<script>
	assingTWApp.allElements = getAllElements();
	@if($_GET)
	<?php 
	$type = $_GET['type'];
	$pedido_id = $_GET['pedido_id'];
	$country = $_GET['country'];
	$since = (empty($_GET['since']))? "": $_GET['since'];
	$until = (empty($_GET['until']))? "": $_GET['until'];
	?>
	var formComms = "undefined";
	assingTWApp.type = "{{$type}}";
	assingTWApp.pedido_id = "{{$pedido_id}}";
	assingTWApp.country = "{{$country}}";
	assingTWApp.since = "{{$since}}";
	assingTWApp.until = "{{$until}}";
	@endif
</script>	
@stop

@section('content')

<style>
.tw-element.card .card-content {
	padding: 6px 24px;
}
.tw-element.card .row {
	margin-bottom: 0px;
}
.contained-element{
	margin: 0.5em;
	line-height: 2;
	border-radius: 3px;
	border-width: 1px;
	border-style: solid;
	border-color: #b0bec526;
	background-color: #eceff1;
	padding: 3px;
}
.flex.flex-tw-users{

}
.flex.flex-tw-users span{
	flex: 1 1;
}
.flex.flex-tw-users a{
	flex: 0 1;
	padding: 0 0.7em;
	flex-basis: auto;
	font-weight: lighter;
	line-height: 2;
	vertical-align: middle;
	font-family: sans-serif;
	font-size: 0.6em;
}
.flex.flex-tw-users a i{
	vertical-align: middle;
	margin-right: 0.2em;
}

ul#assignation-result-list li{
	margin-top: 1em;
	margin-left: 1em;
}
</style>

<div class="row grey lighten-3 mb-0">
	<div class="col s12 m12 l12 page-title">
		<h2 class="breadcrumbs-title left">
		Two Factor</h2>
	</div>
</div>
<div class="row blue darken-3">
	<div class="col s12 m12 l12 page-title">
		<p class="white-text" style="width: 80%;"> 
			En esta sección se pueden ver a los usuarios que califican los comms panorama de los posts.
		</p>
	</div>
</div>
<div class="row">
	@if (Session::has('message'))
	<div class="row red darken-4 animated fadeIn">
		<div class="col s12 m12 l12 page-title">
			<p class="white-text">{{ Session::get('message') }} </p>
		</div>
	</div>
	@endif

	@if(session('success'))
	<div class="col s12 m12 l12">
		<div class="light-green message animated fadeInLeftBig">
			<p class="white-text"> <i class="material-icons" style="margin-right: 5px;">check</i>{{ session('success') }} </p>
		</div>
	</div>
	@endif
</div>

<div >
	<div class="row posts-selection">
		<div id="tw-app" class="col s12 m12">
			<div class="row">
				<p><b>Filtro de posts:</b> Posts seleccionados para asignar los usuarios a calificar.</p>
				<div class="input-field col s6">
					{{-- <label for="since">Fecha Inicial:</label>
					<datepicker id="since" v-model="since" name="since" class='datepicker' type="text" placeholder="Y-m-d" v-on:set-date="setDate" v-bind:date="since"> --}}
				<input id="since" type="date" v-model="since"  placeholder="Fecha inicial: Y-m-d" v-on:set-date="setDate" v-bind:date="since">
				</div>  

				<div class="input-field col s6">
				{{-- 	<label for="until">Fecha Final:</label>
					<datepicker id="until" v-model="until" name="until" class='datepicker' type="text" placeholder="Y-m-d" v-on:set-date="setDate" v-bind:date="until"> --}}
				<input id="until" type="date" v-model="until"   placeholder="Fecha final: Y-m-d" v-on:set-date="setDate" v-bind:date="until">
				</div> 
				<div class="filtros-graph row">
					<div class="col s6">
						<label for="interval">Filtrar Fecha:</label>
						<select v-model="interval" v-on:change="setDate(interval)" class="browser-default"  name="interval"   id="">
							<optgroup label="" >
								<option value="all">Todo</option>
								<option value="manual">Seleccionar fechas</option>
							</optgroup>
							<optgroup label="" >
								<option value="hoy">hoy</option>
								<option value="day">El mismo Día</option>
							</optgroup>
							<optgroup label="">
								<option value="6">Semana anterior</option>
								<option value="28">28 días anteriores</option>
								<option value="30">30 días anteriores</option>
							</optgroup>
							<optgroup label="">
								<option value="90">trimestre anterior (90 días)</option>
								<option value="182">semestre anterior (182 días)</option>
								<option value="365">año anterior</option>
							</optgroup>
						</select>
					</div>
					<div class="col s6">
						<label for="first_name">Filtrar Ubicación:</label>
						<select class="browser-default" v-model="country" name="country" id="">
							<option value="all"> Todos los países </option>
							<option v-for="option in allCountries"><%option%></option>
						</select>
					</div>
				</div> 
				<div class="filtros-graph row">
					<div class="col s6">
						<label for="first_name">Filtrar Scope: (seleccionar el alcance de la consulta y la opción deseada)</label>
						<select class="browser-default" v-model="type" name="type" id="model-type">
							{{-- <option value="Vertical">Vertical</option>
							<option value="Categoria">Categoría</option> --}}
							<option value="Canasta">Canasta</option>
							<option value="Brand">Marca</option>
							<option value="Fanpage">Fanpage</option>
							<option value="AdSet">AdSet</option>
						</select>
					</div>
					<div class="col s6">
						<select class='adjust-list browser-default' v-model="pedido_id" list="lista_ids" name="pedido_id" placeholder="Id del elemento a buscar" id="model-id">
							<option v-for="option in allElements[type]" v-bind:value='option.id'><%option.name%></option>
						</select>
					</div>	
				</div>
				<div class="filtros-graph row">
					<div class="col s12">
						<label for="social_network">Red Social</label>
						<select class="browser-default" v-model="social_network" name="social_network" id="model-social-network">
							<option value="all">Todas</option>
							<option value="Facebook">Facebook</option>
							<option value="YouTube">YouTube</option>
							<option value="Instagram">Instagram</option>
						</select>
					</div>
				</div>
				<a v-on:click="sendAssignmentRequest" class="btn">Asignar posts a calificar</a>
				<a v-bind:href="'{{url('posts-list')}}/' + social_network + '/' + type + '/' + pedido_id + '/' + country + '/' + (since?since:'none') + '/' + (until?until:'none') + '?tw_profiles=-1'" class="btn" target="_blank">Ver Posts</a>
			</div>
		</div>
	</div>
	<div class="row tw-users-selection">
		<div class="col s12 m12">
			<p><b>Usuarios Seleccionados:</b> Usuarios elegidos para calificar los posts.</p>
			<div id="tw-users"></div>
		</div>
	</div>
	<div id="assignation-result" class="row result-info hide">
		<div class="col s12 m12">
			<p><b>Resultado Asignaciones:</b> Resultado de las asignaciones realizadas.</p>
			<ul id="assignation-result-list">
				
			</ul>
		</div>
	</div>
	<div class="row judge-info">
		<div class="col s12 m12">
			<p><b>Jueces Activos:</b> Usuarios a los que se asignarán los posts que hayan tenido diferencias en su calificación.<br>(Permisos de Juez para Two Factor nivel 1)</p>
			<div id="tw-juges">
				@foreach($activesJudge as $k=>$ajd)
					<span class="contained-element">{{$ajd->name}}</span>
				@endforeach
			</div>
		</div>
	</div>
<div class="row">
	<div class="col s12 m12"><hr></div>
	<div class="col s12 m6 l6">
		<h4 class="center">Calificadores</h4>
		@foreach($twGroups as $k => $twG )
		<div class="card tw-element">
			<div class="card-content group-users">
				@foreach($twG->element->users as $k => $u)
				<span class="contained-element">{{$u->name}}</span>
				@endforeach
			</div>
			<div class="card-content">
				<div class="flex flex-tw-users">
					<span class="card-title">{{$twG->element->name}}</span>
					<a class="waves-effect waves-light btn red center" onclick='selectUsers(
						@foreach($twG->element->users as $ind=>$usr) @if($ind != 0), @endif {name:"{{$usr->name}}", id:{{$usr->id}}} @endforeach
						)'> <i class="material-icons">group</i> Seleccionar todos</a>
					</div>
				</div>
				<div class="card-action">
					<div class="row">
						<div class="col s6 m6">
							<span>{{$twG->element_model}}</span>
						</div>
						<div class="col s6 m6">
							<span>Nivel: {{$twG->permission_value}}</span>
						</div>
					</div>
				</div>
			</div>
			@endforeach

			@foreach($twUsers as $k => $twU )
			<div class="card tw-element">
				<div class="card-content">
					<div class="flex flex-tw-users">
						<span class="card-title">{{$twU->element->name}}</span>
						<a class="waves-effect waves-light btn red center" onclick='selectUsers({name:"{{$twU->element->name}}", id:{{$twU->element->id}}})'> <i class="material-icons">group</i> Seleccionar</a>
					</div>
				</div>
				<div class="card-action">
					<div class="row">
						<div class="col s6 m6">
							<span>{{$twU->element_model}}</span>
						</div>
						<div class="col s6 m6">
							<span>Nivel: {{$twU->permission_value}}</span>
						</div>
					</div>
				</div>
			</div>
			@endforeach
		</div>
		<div class="col s12 m6 l6">
			<h4 class="center">Jueces</h4>
			@foreach($judgeGroups as $k => $jdG )
			<div class="card tw-element">
				<div class="card-content group-users">
					@foreach($jdG->element->users as $k => $u)
					<span class="contained-element">{{$u->name}}</span>
					@endforeach
				</div>
				<div class="card-content">
					<span class="card-title">{{$jdG->element->name}}</span>
				</div>
				<div class="card-action">
					<div class="row">
						<div class="col s6 m6">
							<span>{{$jdG->element_model}}</span>
						</div>
						<div class="col s6 m6">
							<span>Nivel: {{$jdG->permission_value}}</span>
						</div>
					</div>
				</div>
			</div>
			@endforeach

			@foreach($judgeUsers as $k => $jdU )
			<div class="card tw-element">
				<div class="card-content">
					<span class="card-title">{{$jdU->element->name}}</span>
				</div>
				<div class="card-action">
					<div class="row">
						<div class="col s6 m6">
							<span>{{$jdU->element_model}}</span>
						</div>
						<div class="col s6 m6">
							<span>Nivel: {{$jdU->permission_value}}</span>
						</div>
					</div>
				</div>
			</div>
			@endforeach
		</div>
	</div>
</div>

@stop

