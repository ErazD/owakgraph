<script>
  
  function changeParentValue(){
   var parent = document.getElementById("parent");
   var parentName = document.getElementById("parent-name");
  var val = document.getElementById("child_" + parent.value);
   
    if(val!=null){
      parentName.innerHTML = val.label;
    }else{
      parent.value = null
      parentName.innerHTML = "de la categoría a la que pertenece";
    }
  }
</script>

<style>
  body, html{
   height: auto; 
 }
 .content-comms{

 }
 .m-0 {
  margin: 0px!important;
}
.p-8{
  padding: 8px;
}
.input-comms{

 margin:8px;
 border:solid 1px #B3B0B0;
 background-color:#fff;
}
.input-comms h4{
  font-size:1.2em;
  font-weight:500;
  margin: 10px 10px 25px 0px;
}
.input-comms .comms-options{
  border:2px solid #000;

  padding:10px;
}
.input-comms ul{
  list-style: none;
  font-size: 12px;
  font-family: sans-serif;
  padding: 0px 15px;
}
.mismatch-field{
  padding:10px;
}
.mismatch-field h3{
  font-size:1.5em;
  margin:5px 5px 15px 5px;
}
.mismatch-field .other-choice{
  width:90%;
  display:block;
  margin:0px 10px 10px 10px;
  border:2px solid #000;
  border-top:0px;
  padding:20px 10px 20px 10px;
}
.mismatch-field .other-choice label{
  display: inline-block;
  width: 20%;
}
.mismatch-field .other-choice .line-up-other{
  width: 45%;
  display: inline-block;
}
.send-form{
  padding: 3%;
  margin: 2%;
  clear: both;
}
.match{
  display:none;
  /*padding: 15px !important;
  border-bottom-color: #b2dfdb;
  border-bottom-style: solid;
  border-bottom-width: 2px;*/
}

.no-match{
  padding: 15px !important;
  border-bottom-color: red;
  border-bottom-style: solid;
  border-bottom-width: 2px;
}
</style>

{{-- <h2 class="center-align">Formulario Comms</h2> --}}

<div class="content-comms col s12 row">

  <div class="p-8" align="right">
    <div class="post-preview col s12">
     <div class="post-thumbnail col s5">
      <img src="{{$post->attachment}}" alt="">
     </div>
        <div class="post-summary col s7">
           <p>{{$post->content}}</p>
             <a class='btn btn-primary red lighten-1' target='_blank' href="{{ url($post->permalink_url) }}">Ver el post</a>
        </div>
    </div>
  </div>
  <form id="form-{{$post->id}}" action="{{route('two-factor.store.p')}}" method="POST">
    {{ csrf_field() }}
    <?php 
    $twos=App\TwoFactorAuthentication::where('social_network',$social_network_name)->where('post_id',$post->id)->where('principal',false)->get();
    $tw_type = 'none';
    $tw_balance = 'none';
    $tw_micro = 'none';
    $tw_business='none';
    $tw_task = 'none';
    $tw_theme = 'none';
    $tw_contest = 'none';
    $tw_product = 'none';
    $tw_canasta = 'none';
    $tw_influencer = 'none';
    $tw_pauta = 'none';
    $mostarConcurso = false;
    ?>
    <input type="hidden" name="post_id" value="{{$post->id}}">
    <input type="hidden" name="social_network" value="{{$social_network_name}}">
    {{-- <a href="two-factor/create/{{$social_network}}/{{$post->id}}/principal"> Principal</a><br> --}}
    @if(Auth::check())
    <input type="hidden" name="user_id" value="{{Auth::user()->id}}">
    @else
    <input type="text" name="user_id" placeholder="ID del usuario">
    @endif

    <div class="input-comms col s12">

 @if(count($twos)<=0)
 <?php
     $tw_type = false;
     $tw_balance = false;
     $tw_micro = false;
     $tw_business=false;
     $tw_task = false;
     $tw_theme = false;
     $tw_contest = false;
     $tw_product = false;
     $tw_canasta = false;
     $tw_influencer = false;
     $tw_pauta = false;

    ?>
  @else
     @foreach($twos as $tw)
       <div class="col s6">
         <h4>{{$tw->user->name}}</h4>
       </div>

       <?php
           if($tw_type == 'none'){
            $tw_type = ($tw->type)?$tw->type:false;
          }else if($tw_type != false && $tw_type != $tw->type){
            $tw_type = false;
          }
          
          if($tw_balance == 'none'){
            $tw_balance = ($tw->balance)?$tw->balance:false;
          }else if($tw_balance != false && $tw_balance != $tw->balance){
            $tw_balance = false;
          }

          if($tw_micro == 'none'){
            $tw_micro = ($tw->micro_moment)?$tw->micro_moment:false;
          }else if($tw_micro != false && $tw_micro != $tw->micro_moment){
            $tw_micro = false;
          }

          if($tw_business == 'none'){
            $tw_business = ($tw->business_objective)?$tw->business_objective:false;
          }else if($tw_business != false && $tw_business != $tw->business_objective){
            $tw_business = false;
          }

          if($tw_task == 'none'){
            $tw_task = ($tw->communication_task)?$tw->communication_task:false;
          }else if($tw_task != false && $tw_task != $tw->communication_task){
            $tw_task = false;
          }

          if($tw_theme == 'none'){
            $tw_theme = ($tw->communication_theme)?$tw->communication_theme:false;
          }else if($tw_theme != false && $tw_theme != $tw->communication_theme){
            $tw_theme = false;
          }
          $mostarConcurso |= ($tw->communication_theme != null && $tw->communication_theme == 'concurso');

          if($tw_contest == 'none'){
            $tw_contest = ($tw->contest)?$tw->contest:false;
          }else if($tw_contest != false && $tw_contest != $tw->contest){
            $tw_contest = false;
          }
          
          if($tw_product == 'none'){
            $tw_product = ($tw->product)?$tw->product:false;
          }else if($tw_product != false && $tw_product != $tw->product){
            $tw_product = false;
          }

          if($tw_canasta == 'none'){
            $tw_canasta = ($tw->canasta)?$tw->canasta:false;
          }else if($tw_canasta != false && $tw_canasta != $tw->canasta){
            $tw_canasta = false;
          }

          if($tw_influencer == 'none'){
            $tw_influencer = $tw->influencer;
          }else if($tw_influencer != false && $tw_influencer != $tw->influencer){
            $tw_influencer = false;
          }
          
          if($tw_pauta == 'none'){
            $tw_pauta = ($tw->pauta)?$tw->pauta:false;
          }else if($tw_pauta != false && $tw_pauta != $tw->pauta){
            $tw_pauta = false;
          }
      ?>  

     @endforeach    
@endif




  {{-- TIPO DE POST --}}
    <div class="mismatch-field col s12 {{($tw_type)?'match':'no-match'}}">
      <h3>Tipo de Post</h3>
      @foreach($twos as $tw)
      <div class="col s6 comms-options">
       <h4>{{$tw->user->name}}</h4>
       <p class='comms-option'>
        <input type="radio" name='type-check' value='{{$tw->type}}' id='check-type-{{$tw->user->id}}'>
        <label for='check-type-{{$tw->user->id}}'>{{$tw->type}}</label>
      </p>
    </div>
    @endforeach
    <div class="other-choice col s12">
     <input type="radio" name='type-check' value='otro' id='check-type-otro'>
     <label for="check-type-otro">Selecciona otro tipo</label>
     <select name="type" class='browser-default line-up-other'>     
      @if($tw_type)    
      <option  selected value="{{$tw_type}}"> {{$tw_type}}</option>
      @else
      <option disabled selected value=""> Selecciona el tipo de Post</option>
      @endif     
      @foreach(Config::get('constants.comms.type.facebook') as $id => $value) 
      <option value="{{$value['slug']}}"> {{$value['name']}} </option>
      @endforeach
    </select>
  </div>
</div>   


{{-- BALANCE E/F --}}
<div class="mismatch-field col s12 {{($tw_balance)?'match':'no-match'}}">
  <h3>Balance E/F</h3>
  @foreach($twos as $tw)
  <div class="col s6 comms-options"> 
    <h4>{{$tw->user->name}}</h4>
    <p class='comms-option'>
      <input type="radio" name='type-ef' value='{{$tw->balance}}' id='check-balance-{{$tw->user->id}}'>
      <label for='check-balance-{{$tw->user->id}}'>{{$tw->balance}}</label>
    </p>
  </div>
  @endforeach
</div>   

{{-- GOOGLE MICRO MOMENT --}}
<div class="mismatch-field col s12 {{($tw_micro)?'match':'no-match'}}">
  <h3>Google Micro Moment</h3>
  @foreach($twos as $tw)
  <div class="col s6 comms-options"> 
    <h4>{{$tw->user->name}}</h4>
    <p class='comms-option'>
      <input type="radio" name='type-micro' value='{{$tw->micro_moment}}' id='check-micro-{{$tw->user->id}}'>
      <label for='check-micro-{{$tw->user->id}}'>{{$tw->micro_moment}}</label>
    </p>
  </div>
  @endforeach
  <div class="other-choice col s12">
    <input type='radio' name="type-micro" value='otro' id='check-micro-otro'>
    <label for="check-micro-otro">Selecciona otro Micro Moment</label>
    <select name="micro_moment"  class='browser-default line-up-other'>
      @if($tw_micro)
      <option  selected value="{{$tw_micro}}"> {{$tw_micro}}</option>
      @else
      <option disabled selected value=""> Selecciona un Micro moment</option>
      @endif
      @foreach(Config::get('constants.comms.google_micro_moment') as $id => $value) 
      <option value="{{$value['slug']}}"> {{$value['name']}} ({{$value['description']}})</option>
      @endforeach
    </select>
  </div>
</div>  

{{-- BUSINESS OBJECTIVE --}}
<div class="mismatch-field col s12 {{($tw_business)?'match':'no-match'}}">
  <h3>Business Objective</h3>
  @foreach($twos as $tw)
  <div class="col s6 comms-options"> 
    <h4>{{$tw->user->name}}</h4>
    <p class='comms-option'>
      <input type="radio" name='type-business' value='{{$tw->business_objective}}' id='check-business-{{$tw->user->id}}'>
      <label for='check-business-{{$tw->user->id}}'>{{$tw->business_objective}}</label>
    </p>
  </div>
  @endforeach
  <div class="other-choice col s12">
    <input type='radio' name="type-business" value='otro' id='check-business-otro'>
    <label for="check-task-otro">Selecciona otro Business Objective</label>
    <select name="business_objective"  class='browser-default line-up-other'>
      @if($tw_business)
      <option  selected value="{{$tw_business}}"> {{$tw_business}}</option>
      @else
      <option disabled selected value=""> Selecciona un Business objective</option>
      @endif
      @foreach(Config::get('constants.comms.business-objective') as $id => $value) 
      <option value="{{$value['slug']}}"> {{$value['name']}} ({{$value['description']}}) </option>
      @endforeach
    </select>
  </div>
</div>  

{{-- TAREA DE COMUNICACION --}}
<div class="mismatch-field col s12 {{($tw_task)?'match':'no-match'}}">
  <h3>Tareas de Comunicación</h3>
  @foreach($twos as $tw)
  <div class="col s6 comms-options"> 
    <h4>{{$tw->user->name}}</h4>
    <p class='comms-option'>
      <input type="radio" name='type-task' value='{{$tw->communication_task}}' id='check-task-{{$tw->user->id}}'>
      <label for='check-task-{{$tw->user->id}}'>{{$tw->communication_task}}</label>
    </p>
  </div>
  @endforeach
  <div class="other-choice col s12">
    <input type='radio' name="type-task" value='otro' id='check-task-otro'>
    <label for="check-task-otro">Selecciona otra tarea</label>
    <select name="comunication_task"  class='browser-default line-up-other'>
      @if($tw_task)
      <option  selected value="{{$tw_task}}"> {{$tw_task}}</option>
      @else
      <option disabled selected value=""> Selecciona la tarea</option>
      @endif
      @foreach(Config::get('constants.comms.comunication_task') as $id => $value) 
      <option value="{{$value['slug']}}"> {{$value['name']}} ({{$value['description']}})</option>
      @endforeach
    </select>
  </div>
</div>   


{{-- TEMAS DE COMUNICACIÓN --}}    
<div class="mismatch-field col s12 {{($tw_theme)?'match':'no-match'}}">
  <h3>Temas de Comunicación</h3>
  @foreach($twos as $tw)
  <div class="col s6 comms-options"> 
   <h4>{{$tw->user->name}}</h4>
    <p class='comms-option'>
      <input type="radio" name='type-theme' value='{{$tw->communication_theme}}' id='check-communicate-{{$tw->user->id}}'>
      <label for='check-communicate-{{$tw->user->id}}'>{{$tw->communication_theme}}</label>
    </p>
  </div>
  @endforeach
  <div class="other-choice col s12">
    <input type='radio' name="type-theme" value='otro' id='check-theme-otro'>
    <label  for="check-theme-otro">Selecciona otro tema</label>
    <select name="comunication_theme" onchange="showContest({{$post->id}})" class='browser-default line-up-other' id='theme-{{$post->id}}' >
      @if($tw_theme)
        <option  selected value="{{$tw_theme}}"> {{$tw_theme}}</option>
      @else
        <option disabled selected value=""> Selecciona el tema</option>
      @endif
      @foreach(Config::get('constants.comms.comunication_theme') as $id => $value) 
        <option value="{{$value['slug']}}"> {{$value['name']}} </option>
      @endforeach
    </select>
  </div>
</div>  

{{-- CONCURSO --}}      
<div class="mismatch-field col s12 {{($mostarConcurso)?'':'hide'}} {{($tw_contest)?'match':'no-match'}}" id='concursos-{{$post->id}}'>
  <h3>Concurso</h3>
  @foreach($twos as $tw)
  <div class="col s6 comms-options"> 
   <h4>{{$tw->user->name}}</h4>
    <p class='comms-option'>
      <input type="radio" name='type-contest' value='{{($tw->contest)?$tw->contest->id :''}}' id='check-contest-{{$tw->user->id}}'>
      <label for='check-contest-{{$tw->user->id}}'>{{($tw->contest)?$tw->contest->title:''}}</label>
    </p>    
  </div>
  @endforeach
  <div class="other-choice col s12">
    <input type="radio" name='type-contest' value='otro' id='check-contest-otro'>
    <label for="comunication_contest-otro">Selecciona otro concurso</label>
    <select name="contest"   class='browser-default line-up-other'>
      @if($tw_contest)
      <option selected value="{{$tw_contest->id}}"> {{$tw_contest->title}} </option>
      @else
      <option disabled selected value="">Selecciona el concurso</option>
      @endif
      <?php $contests = App\Contest::where('brand_id',$post->brand()->id)->get(); ?>
      @foreach($contests as $contest)
      <option value="{{$contest->id}}">{{$contest->title}}</option>
      @endforeach         
    </select>
    <a href="#addContest" class='modal-trigger'><span class='tooltipped' data-tooltip="Agregar Concurso"><i class="fa fa-plus-circle" aria-hidden="true"></i></span></a>
  </div>

</div>  

{{-- PRODUCTOS --}}     
<div class="mismatch-field col s12 {{($tw_product)?'match':'no-match'}}">
  <h3>Productos</h3>
  @foreach($twos as $tw)
  <div class="col s6 comms-options">
   <h4>{{$tw->user->name}}</h4> 
    <p class='comms-option'>
      <input type="radio" name='type-product' value='{{$tw->product_id}}' id='check-product-{{$tw->user->id}}'>
      <label for='check-product-{{$tw->user->id}}'>{{($tw->product)?$tw->product->name:''}} </label>
    </p>
  </div>
  @endforeach
  <div class="other-choice col s12 products-selector">
   <input type="radio" name='type-product' value='otro' id='check-product-otro'>
   <label for="check-product-otro">Selecciona otro producto</label>
   <input id="parent" list="parents" name="product" onfocusout="changeParentValue()" autocomplete="off">
       <?php $products = App\Product::where('brand_id',$post->brand()->id)->where(
          function($query){
            $query->orwhere('created_at', '>', '2018-02-12')->orwhere('group_product', 'na')->orwhere('group_product', 'todos');
          })->get();?>  
      <datalist id="parents">
        @foreach($products as $k=>$p)
        <option id="child_{{$p->id}}" value="{{$p->id}}" label="{{$p->name}}" name="{{$p->name}}">
          @endforeach
        </datalist>
       <label for="parent" class="col-md-5 control-label">Id <span id="parent-name"> de la categoría a la que pertenece</span> </label>
</div>
</div>  


{{-- CANASTAS --}}     
<div class="mismatch-field col s12 {{($tw_canasta)?'match':'no-match'}}">
  <h3>Canastas</h3>
  @foreach($twos as $tw)
  <div class="col s6 comms-options">
   <h4>{{$tw->user->name}}</h4> 
    <p class='comms-option'>
      <input type="radio" name='type-canasta' value='{{$tw->canasta_id}}' id='check-canasta-{{$tw->user->id}}'>
      <label for='check-canasta-{{$tw->user->id}}'>{{($tw->canasta)?$tw->canasta->name:''}} </label>
    </p>
  </div>
  @endforeach
  <div class="other-choice col s12 canastas-selector">
   <input type="radio" name='type-canasta' value='otro' id='check-canasta-otro'>
   <label for="check-canasta-otro">Selecciona otro canasta</label>


  <select name="canasta"   class='browser-default line-up-other'>
      @if($tw_canasta)
      <option selected value="{{$tw_canasta->id}}"> {{$tw_canasta->name}} </option>
      @else
      <option disabled selected value="">Selecciona la canasta</option>
      @endif
      <?php $canastas = $post->brand()->canastas; ?>  
      @foreach($canastas as $canasta)
        <option value="{{$canasta->id}}">{{$canasta->name}}</option>
      @endforeach             
    </select>

  </div>
</div>  


{{-- INFLUENCER --}}

<div class="mismatch-field col s12 {{($tw_influencer!==false)?'match':'no-match'}} ">
  <h3>Infuencer</h3>
  @foreach($twos as $tw)
  <div class="col s6 comms-options">
   <h4>{{$tw->user->name}}</h4> 
    <p class='comms-option'>
      <input type="radio" name='type-influencer' value='{{$tw->influencer}}' id='check-influencer-{{$tw->user->id}}'>
      <label for='check-influencer-{{$tw->user->id}}'>{{$tw->influencer}}</label>
    </p>       
  </div>
  @endforeach
  <div class="other-choice col s12">
    <input type="radio" name='type-influencer' value='otro' id='check-influencer-otro'>
    <label for="check-influencer-otro">Menciona otro Influencer</label>
    <input class="m-0 line-up-other" type="text" name="influencer" placeholder="Nombre del influencer" value="{{$tw_influencer}}">
  </div>
</div>  

{{-- PAUTA/NO PAUTA --}}   
<div class="mismatch-field col s12 {{($tw_pauta)?'match':'no-match'}}">
  <h3>Pauta</h3>
  @foreach($twos as $tw)
  <div class="col s6 comms-options">
   <h4>{{$tw->user->name}}</h4> 
    <p class='comms-option'>
      <input type="radio" name='type-pauta' value='{{$tw->pauta}}' id='check-pauta-{{$tw->user->id}}'>
      <label for='check-pauta-{{$tw->user->id}}'>{{$tw->pauta}}</label>
    </p>
  </div>
  @endforeach
</div>   

<div class="send-form">
    {{-- <div class="col s6">
      <input id="update_post" type="checkbox" name="update_post" value="update"/><label for="update_post">Confirmar la actualización del Post</label>
    </div> --}}
    <div class="col s12 m6 offset-m6">
      <input type="submit" value="Actualizar" class="find btn btn-primary green m-0" style="width:100%;">
    </div>   
  </div>
</div>
</form> 
  {{-- <div class="comms-submit col s6">
    <button class="find btn btn-primary green m-0" style="width:100%;" 
    onclick ="updateComms('{{$post->id}}')" >Actualizar</button>
  </div>   --}}
</div>