{{-- <h2 class="center-align">Formulario Comms</h2> --}}
<script>
  
  function changeParentValue(id){
   var parent = document.getElementById("parent" + id);
   var parentName = document.getElementById("parent-name" + id);
   var val = document.getElementById("child_" + parent.value);
   if(val!=null){
    parentName.innerHTML = val.value;
  }else{
    parent.value = null
    parentName.innerHTML = "de la categoría a la que pertenece";
  }
}
</script>

<div class="content-comms col s12 row">
  <form id="form-{{$post->id}}" action="{{route('two-factor.store')}}" method="POST">
    {{ csrf_field() }}

    <input type="hidden" name="post_id" value="{{$post->id}}">
    <input type="hidden" name="social_network" value="{{$social_network}}">
    <input type="hidden" name='brand_id' value="{{$post->brand()->id}}">

    @if(Auth::check())
    <input type="hidden" name="user_id" value="{{Auth::user()->id}}">
    @else
    <input type="text" name="user_id" placeholder="ID del usuario">
    @endif

    <div class="input-comms col s6">
      <label for="tipo">Tipo de Post*</label>
      <select name="type" class='browser-default' required>
        @if($post->type && $post->type!="")
        <option disabled selected value="{{$post->type}}"> {{$post->type}}</option>
        @else
        <option disabled selected value=""> Selecciona el tipo de Post</option>
        @endif
        @foreach(Config::get('constants.comms.type.facebook') as $id => $value) 
        <option value="{{$value['slug']}}"> {{$value['name']}} </option>
        @endforeach
      </select>
    </div>

    <div class="input-comms col s6">
      <label for="balance">Balance E/F*</label>
      <select name="balance" class='browser-default' required>
        @if($post->balance && $post->balance!="")
        <option disabled selected value="{{$post->balance}}"> {{$post->balance}}</option>
        @else
        <option disabled selected value=""> Selecciona el balance</option>
        @endif
        @foreach(Config::get('constants.comms.balance') as $id => $value) 
        <option value="{{$value['slug']}}"> {{$value['name']}}</option>
        @endforeach
      </select>
    </div>
    <div class="input-comms col s6">
      <label for="micro_moment">Google Micro Moment*</label>
      <select name="micro_moment" class='browser-default' required>
        @if($post->micro_moment && $post->micro_moment!="")
        <option disabled selected value="{{$post->micro_moment}}"> {{$post->micro_moment}}</option>
        @else
        <option disabled selected value=""> Selecciona el Micro Moment</option>
        @endif
        @foreach(Config::get('constants.comms.google_micro_moment') as $id => $value) 
        <option value="{{$value['slug']}}"> {{$value['name']}} ({{$value['description']}}) </option>
        @endforeach
      </select>
    </div>
    <div class="input-comms col s6">
      <label for="business_objective">Business Objective*</label>
      <select name="business_objective" class='browser-default' required>
        @if($post->business_objective && $post->business_objective!="")
        <option disabled selected value="{{$post->business_objective}}"> {{$post->business_objective}}</option>
        @else
        <option disabled selected value=""> Selecciona el Business Objective</option>
        @endif
        @foreach(Config::get('constants.comms.business-objective') as $id => $value) 
        <option value="{{$value['slug']}}"> {{$value['name']}} ({{$value['description']}}) </option>
        @endforeach
      </select>
    </div>
    <div class="input-comms col s6">
      <label for="tareas">Tareas de Comunicación*</label>
      <select name="comunication_task"   class='browser-default' required>
        @if($post->comunication && $post->comunication!="")
        <option disabled selected value="{{$post->comunication}}"> {{$post->comunication}}</option>
        @else
        <option disabled selected value=""> Selecciona la tarea </option>
        @endif
        @foreach(Config::get('constants.comms.comunication_task') as $id => $value) 
        <option value="{{$value['slug']}}"> {{$value['name']}} ({{$value['description']}}) </option>
        @endforeach
      </select>
    </div>

    <div class="input-comms col s6">  
      <label for="themes">Temas de Comunicación*</label>
      <select name="comunication_theme"  onchange="showContest({{$post->id}})" class='browser-default' id='theme-{{$post->id}}' required>
        @if($post->theme && $post->theme!="")
        <option disabled selected value="{{$post->theme}}"> {{$post->theme}}</option>
        @else
        <option disabled selected value=""> Selecciona el tema</option>
        @endif
        @foreach(Config::get('constants.comms.comunication_theme') as $id => $value) 
        <option value="{{$value['slug']}}"> {{$value['name']}} </option>
        @endforeach
      </select>
    </div>
    
    <div class="input-comms contest col s12 hide"  id='concursos-{{$post->id}}'>
      <div class="contest-select">

        <label for="themes">Concursos</label>
        <select name="contest"   class='browser-default'>

          @if($post->contests()->get()->count()>0)
          <option disabled selected value="{{$post->contests()->get()[0]->id}}"> {{$post->contests()->get()[0]->title}} </option>
          @else
          <option disabled selected value="">Selecciona el concurso</option>
          @endif
          <?php $contests = App\Contest::where('brand_id',$post->brand()->id)->get(); ?>
          @foreach($contests as $contest)
          <option value="{{$contest->id}}">{{$contest->title}}</option>
          @endforeach          
        </select>
      </div>
      <a href="#addContest" class='modal-trigger'><span class='tooltipped' data-tooltip="Agregar Concurso"><i class="fa fa-plus-circle" aria-hidden="true"></i></span></a>
    </div>   

    
    <div class="row">
      <div class="input-comms col s6 ">  
        <label for="product">Productos*</label>
        <input id="parent{{$post->id}}" list="products_{{$post->brand()->id}}" name="product" onfocusout="changeParentValue({{$post->id}})" required autocomplete="off" style="margin-bottom: 0px;">
        <label for="parent" class="col-md-5 control-label">Id <span id="parent-name{{$post->id}}"> de la categoría a la que pertenece</span> </label>
         <!-- < ?php $products = App\Product::where('brand_id',$post->brand()->id)->where(
            function($query){
              $query->orwhere('created_at', '>', '2018-02-12')->orwhere('group_product', 'na')->orwhere('group_product', 'todos');
            })->get();?>   -->
        <?php $products = App\Product::where('brand_id',$post->brand()->id)->get();?>  
        <datalist id="products_{{$post->brand()->id}}">
          @foreach($products as $k=>$p)
          {{-- <option id="child_{{$p->id}}" value="{{$p->id}}" label="{{$p->name}}" name="{{$p->name}}"> --}}
          <option id="child_{{$p->name}}" value="{{$p->name}}" label="{{$p->id}}" name="{{$p->name}}">
            @endforeach
          </datalist>
      </div>

          
        <?php $canastas = $post->brand()->canastas; ?>
        <div class="input-comms col s6" 
          @if(sizeof($canastas)==1)
            style="display: none;" 
          @endif
        >
      <label for="canasta">Canasta*</label>
        <select name="canasta"   class='browser-default' required>          
          
          @if($post->canasta)
          <option disabled selected value="{{$post->canasta->id}}"> {{$post->canasta->name}} </option>
          @else
            @if(sizeof($canastas)!=1)
              <option disabled selected value="">Selecciona la canasta</option>
            @else
              <option disabled selected value="{{$canastas[0]->id}}"> {{$canastas[0]->name}} </option>
            @endif
          @endif          
          @foreach($canastas as $canasta)
          <option value="{{$canasta->id}}">{{$canasta->name}}</option>
          @endforeach          
        </select>
        <label for="canasta" class="col-md-5 control-label">Canasta a la que pertenence</label>
        </div>
  </div>  


        <div class="input-comms col s6">
          <label for="influencer">Influencer</label>
          <input type="text" name="influencer" placeholder="Nombre del influencer" value="{{$post->influencer}}">
        </div>
        <div class="input-comms col s6">  
          <label for="pauta">Pauta</label>
          <select name="pauta" class='browser-default'>
            <option disabled selected value="{{($post->pauta)?'PAUTA':'NO PAUTA'}}">{{($post->pauta)?'PAUTA':'NO PAUTA'}}</option>
            <option value="NO PAUTA">NO PAUTA</option>
            <option value="PAUTA">PAUTA</option>
          </select><br>
        </div>  
        {{-- <input type="submit" value="Crear"> --}}
      </form> 
      <div class="col s6">
        @if($hasUser || $isJudge)
        @if($isJudge)
        <a class="grey-text text-lighten-1 tooltipped" target="_blank" href="{{url('two-factor/create/'.$social_network.'/'.$post->id.'/principal')}}" data-position="bottom" data-delay="50" data-tooltip="Sección principal de Two Factor Authentication"> <i class="material-icons">settings</i></a> 
        @endif
        <?php 
        $twos=App\TwoFactorAuthentication::where('social_network',$social_network)->where('post_id',$post->id)->get();
        $usersComms = "";
        foreach ($twos as $key => $value) { 
            //dd($value,$value->profile,$value->post);//,$value->profile->user, $value->profile->user->name);
          $usersComms .= (($usersComms == "")?"":", ").$value->user->name;
        }          
        ?>
        @if($usersComms!="")
        <i class="grey-text text-lighten-1 tooltipped material-icons" data-position="bottom" data-enable-html="true" data-delay="50" data-tooltip="Usuarios que han registrado comms: {{$usersComms}}">people</i>
        @endif
        @endif
      </div>
      <div class="comms-submit col s6">
        <button class="find btn btn-primary green m-0" style="width:100%;" 
        onclick ="updateComms('{{$post->id}}')" >Enviar a revisión</button>


      </div>  
    </div>

