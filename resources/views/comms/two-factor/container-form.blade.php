@extends('layouts.app')

@section('scripts')

<script src="{{ asset('js/vue.min.js') }}"></script>
<script src="{{ asset('js/appComms.js') }}"></script>
{{-- <script src="{{ asset('js/main.js') }}"></script> --}}
@stop

@section('content')

@php
 $fanpage = '';
 switch($post->social_network()){
     case 'Facebook':
      $post->fanpage
     break;
     case 'YouTube':
      $post->channel
     break;
     case 'Instagram':
       $post->profile
     break;
 }
 @endphp

<div class="page" id="fanpage">
	<div class="row grey lighten-3 mb-0">
		<div class="col s12 m12 l12 page-title">
			<div class="header-content left">
			<h2>Revisión de Comms Panorama</h2>
				{{-- <a href="{{ url('fanpage/'.$fanpage->id) }}"><h2>{{ $title }}</h2></a>
				<h6><a href="{{ $fanpage->url }}">{{ $fanpage->url }}</a></h6>
			 --}}
			</div>
		</div>
		<div class="right">
			{{-- @if($fanpage != "all")
			
			@endif --}}
		</div>
	</div>
	<div class="row blue darken-3 mb-0">
		<div class="col s12 m12 l12 page-title">
			<p class="white-text">En esta sección puede revisar la información <b>comms</b> de otros usuarios que presentan conflicto. Puede seleccionar el valor del usuario con el que concuerde su análisis. Si considera que ninguno de los valores es correcto, puede elegir una tercera opción para corregir la información.</p>
		</div>
	</div>
	@if($errors)
		@foreach ($errors->all() as $error)
		<div class="red-text" style="display: flex;margin: 5px 0px 3px 0px;"><i class="material-icons">warning</i> {{ $error }}</div>
		@endforeach
 	@endif


	<div class="row">
	
		@include('comms.two-factor.register-principal', ['social_network'=>strtolower($post->social_network()), 'social_network_name'=>$post->social_network()])
	</div>
</div>







<div class="modal modal-fixed-footer" id="addContest">
	<h2 class="title dark-blue-graph">Agregar Concurso</h2>
	<div class="modal-content">

		{!! Form::open(['route' => 'contest.store', 'onSubmit' =>'submitForm()']) !!}
		<div class="row">
			<div class="col s12 m10 l10 offset-m1 offset-l1">
				<label for="name" class="col-md-4 control-label">Nombre</label>
				{!! Form::text('name', 'Nombre del Concurso', array('class' => 'mb-5')) !!}
				<label for="text" class="col-md-4 control-label">Mecánica</label>
				{!! Form::textarea('mechanics', 'Mecánica del Concurso', array('class' => 'mb-5')) !!}
				<label for="text" class="col-md-4 control-label">Premio</label>
				{!! Form::text('prize', 'Premio del Concurso', array('class' => 'mb-5')) !!}
				<label for="date" class="col-md-4 control-label">Desde</label>
				{!! Form::date('from-date', \Carbon\Carbon::now()); !!}
				<label for="date" class="col-md-4 control-label">Hasta</label>
				{!! Form::date('to-date', \Carbon\Carbon::now()); !!}
				{!! Form::hidden('brand_id', $fanpage->brand->id) !!}
				@if ($errors->has('url'))
				@foreach($errors->get('url') as $error)
				<p style="color:#b71c1c;">{{ $error }}</p>
				@endforeach
				@endif

				{!!Form::submit('Agregar', ['class' => 'find btn btn-primary green m-0', 'style' => 'width:100%'] ) !!}
				<div id="scrap">
					<div class="progress mt-5">
						<div class="indeterminate"></div>
					</div>
				</div>
			</div>
		</div>
		<script type=text/javascript>
			function submitForm()
			{	
				$('.btn.green.find').hide();
				$('.progress').show();
			}

		</script>
		{!! Form::close() !!}


	</div>
	<div class="modal-footer">
		<a href="#!" class="modal-action modal-close waves-effect waves-blue btn-flat">Cerrar</a>
	</div>
</div>	

@endsection