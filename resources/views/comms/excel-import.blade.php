@extends('layouts.app')

@section('scripts')
<script>
	function getString(cadena = "NULL"){
		var res = (cadena == "NULL")? "-" : cadena;
		return res;
	}
</script>
@stop
@section('content')

<div class="row grey lighten-3 mb-0">
	<div class="col s12 m12 l12 page-title">
		<h2 class="breadcrumbs-title left">
		Datos cargados previamente de Excel</h2>
	</div>
</div>

<div class="row blue darken-3">
	<div class="col s12 m12 l12 page-title">

		<p class="white-text" style="width: 80%;"> 
			<b>{{$str_estado}}</b>
		{{-- 	@if(!is_null($video->content))
			{{ $video->content }}
			@else
			Esta publicación no tiene una descripción
			@endif --}}
		</p>
		<div class="btn-group">
			{{-- <a href="{{ $video->permalink_url }}" target="_blank" class="btn btn-primary green tooltipped mr-5" style="margin-left: 20px;" data-position="bottom" data-delay="50" data-tooltip="Abrir en Facebook"><i class="material-icons">insert_link</i></a>

			<a href="{{ url('youtube-video/'.$video->id.'/export') }}" target="_blank" class="btn btn-primary green tooltipped" data-position="bottom" data-delay="50" data-tooltip="Descargar"><i class="material-icons">file_download</i></a> --}}
		</div>

	</div>

</div>


	<style type="text/css">
		body{
			background-color:#fcfcfc;
		}
		table, th, td {
			border: 1px solid black;
			border-collapse: collapse;
			padding: 5px;
			text-align:center;
		}
		.tabla{
			overflow-x: scroll;
		}
		th{
			padding: 20px;
			text-align: center;
			background-color: #707070;
			color: white;
			font-family: sans-serif;
			font-weight: 300;
		}
		td.estado{
			color: white;
			font-weigth: bolder;
		}
		td.estado0{
			background-color:#e53935;
		}
		td.estado1{
			background-color:#fb8c00;
		}
		td.estado2{
			background-color:#00897b;
		}
		td.estado3{
			background-color:#00acc1;
		}
		td.estado4{
			background-color:#039be5;
		}
		
</style>
<div class="container-m tabla" id="stats">

		<table >
			<tr>
				<th>ESTADO</th> <th>FB_id</th><th>Post_id</th>
				<th>Producto</th><th>Type</th> <th>Balance</th><th>Comunication Task</th><th>Comunication Theme</th>
				<th>Nombre Concurso</th><th>Mecánica</th><th>Premio</th><th>Duración</th>
				<th>Influencer</th> <th>Pauta</th> <th>URL</th>
			</tr>
			@foreach($imports as $imp)
			<tr>
				<td class="estado estado{{$imp->estado}}">{{$imp->estado}}</td> <td>{{$imp->facebook_id}}</td>
				<td><a href="{{ url('posts/'.$imp->post_id) }}">{{$imp->post_id}}</a></td> 
				<td>{{($imp->product_id!= NULL)?$imp->product->name : "-"}}</td>
				<td>{{$imp->type}}</td> <td>{{$imp->balance}}</td><td>{{$imp->communication_task}}</td><td>{{$imp->communication_theme}}</td>

				<td>{{ ($imp->nombre_del_concurso!= "NULL")?$imp->nombre_del_concurso : "-" }}</td>
				<td>{{ ($imp->mecanica_del_concurso!= "NULL")?$imp->mecanica_del_concurso : "-" }}</td>
				<td>{{ ($imp->premio!= "NULL")?$imp->premio : "-" }}</td>
				<td>{{ ($imp->duracion!= "NULL")?$imp->duracion : "-" }}</td>

				<td>{{ ($imp->influencer > 0)?$imp->nombre_del_influencer : "-" }}</td>
				<td>{{$imp->pauta_vs_no_pauta}}</td>
				<td><a href="{{$imp->url}}">url</a></td>
			</tr>
			@endforeach
			
		</table>	
	
</div>
<div class="row">
		<div class="col s12 m12 l12">
			{{ $imports->links() }}
		</div>
	</div>
@stop