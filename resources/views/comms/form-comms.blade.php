@php
	$comms = Config::get('constants.comms'); 
	$isFirts = isset($isFirts)? $isFirts: True;
@endphp

<div class="content-comms col s12 row">
	<form id="admin-form-post-{{$post->id}}" action="{{ route('comms_register')}}" method="POST">
		{{ csrf_field() }}
		<input type="hidden" name="post_id" value="{{$post->id}}">
		<input type="hidden" name="social_network" value="{{$post->social_network()}}">

		<div class="input-comms col s6">
			<label for="tipo">Tipo de Post</label>
			<select name="type" class='browser-default'>
				<option disabled selected value=""> 
					{{($post->type && $post->type != "")? $post->type : "Selecciona el tipo de Post"}}
				</option>
				@foreach($comms['type'][strtolower($post->social_network())] as $id => $value) 
				<option value="{{$value['slug']}}"> {{$value['name']}} </option>
				@endforeach
			</select>
		</div>

		<div class="input-comms col s6">
			<label for="balance">Balance E/F</label>
			<select name="balance" class='browser-default'>
				<option disabled selected value=""> 
					{{($post->balance && $post->balance!="")? $post->balance: "Selecciona el balance"}}
				</option>
				@foreach($comms['balance'] as $id => $value) 
					<option value="{{$value['slug']}}"> {{$value['name']}} </option>
				@endforeach
			</select>
		</div>

		<div class="input-comms col s6">
	      <label for="micro_moment">Google Micro Moment*</label>
	      <select name="micro_moment" class='browser-default' required>
	        <option disabled selected value=""> 
	        	{{($post->micro_moment && $post->micro_moment!="")? $post->micro_moment: "Selecciona el Micro Moment"}}
	        </option>
	        @foreach($comms['google_micro_moment'] as $id => $value) 
	        	<option value="{{$value['slug']}}"> {{$value['name']}} ({{$value['description']}}) </option>
	        @endforeach
	      </select>
	    </div>

	    <div class="input-comms col s6">
	      <label for="business_objective">Business Objective*</label>
	      <select name="business_objective" class='browser-default' required>
	        <option disabled selected value="">
	        	{{($post->business_objective && $post->business_objective!="")? $post->business_objective: "Selecciona el Business Objective"}}
	     	</option>
	        @foreach($comms['business-objective'] as $id => $value) 
	        	<option value="{{$value['slug']}}"> {{$value['name']}} ({{$value['description']}}) </option>
	        @endforeach
	      </select>
	    </div>

		<div class="input-comms col s6">
			<label for="tareas">Tareas de Comunicación</label>
			<select name="comunication_task"   class='browser-default'>
				<option disabled selected value=""> 
					{{($post->comunication && $post->comunication!="")? $post->comunication: "Selecciona la tarea"}}
				</option>
				@foreach($comms['comunication_task'] as $id => $value) 
					<option value="{{$value['slug']}}"> {{$value['name']}} </option>
				@endforeach
			</select>
		</div>

		<div class="input-comms col s6">  
			<label for="themes">Temas de Comunicación</label>
			<select name="comunication_theme"  onchange="showContest('{{$post->id}}-admin')" class='browser-default' id='theme-{{$post->id}}-admin'>
				<option disabled selected value="">
					{{($post->theme && $post->theme!="")? $post->theme : "Selecciona el tema"}}</option>
				@foreach($comms['comunication_theme'] as $id => $value) 
					<option value="{{$value['slug']}}"> {{$value['name']}} </option>
				@endforeach
			</select>
		</div>
		
		<div class="input-comms contest col s12 hide"  id='concursos-{{$post->id}}-admin'>
			<div class="contest-select">
				<label for="themes">Concursos</label>
				<select name="contest" class='browser-default'>
					<option disabled selected value=""> 
						{{($post->contests()->get()->count()>0)? $post->contests()->get()[0]->title: "Selecciona el concurso"}} 
					</option>
					@php($contests = App\Contest::where('brand_id',$post->brand()->id)->get())
					<option value="no-contest">Publicación sin concurso</option>
					@foreach($contests as $contest)
						<option value="{{$contest->id}}">{{$contest->title}}</option>
					@endforeach
				</select>
			</div>
			<a href="#addContest" class='modal-trigger'><span class='tooltipped' data-tooltip="Agregar Concurso"><i class="fa fa-plus-circle" aria-hidden="true"></i></span></a>
		</div>

		<div class="input-comms col s6">
			<label for="product">Producto: <strong id="admin_selected_product_name_{{$post->id}}">Selecciona un producto</strong></label>
				<input id="admin_selected_product_{{$post->id}}" list="admin_products_{{$post->brand()->id}}" name="product" onfocusout="changeAdminProduct({{$post->id}})" required autocomplete="off" style="margin-bottom: 0px;">

				@php($products = App\Product::where('brand_id',$post->brand()->id)->get())
				<datalist id="admin_products_{{$post->brand()->id}}">
					@foreach($products as $product)
						<option id="admin_product_child_{{$product->id}}" value="{{$product->id}}" label="{{$product->name}}">{{$product->name}}</option>
					@endforeach
				</datalist>
		</div>
		
		
        <div class="input-comms col s6">
      <label for="canasta">Categoría*</label>
        <select name="canasta" class='browser-default'>
        	<option disabled selected value="">
        		{{($post->canasta)? $post->canasta->name: "Selecciona la canasta"}} 
       		</option>
       		@php($canastas = $post->brand()->canastas)
          	@foreach($canastas as $canasta)
          		<option value="{{$canasta->id}}">{{$canasta->name}}</option>
          	@endforeach          
        </select>
        </div>


		<div class="input-comms col s6">  
	    	<label for="influencer">Influencer</label>
			 <input type="text" name="influencer" placeholder="Nombre del influencer" value="{{$post->influencer}}">
		</div>
	    <div class="input-comms col s6">  
	    	<label for="pauta">Pauta</label>
		    <select name="pauta" class='browser-default'>
		    	<option disabled selected value=""> {{($post->pauta)?'PAUTA':'NO PAUTA'}} </option>
		      <option value="NO PAUTA">NO PAUTA</option>
		      <option value="PAUTA">PAUTA</option>
		    </select><br>
		</div>

	</form>
	<div class="comms-submit col s6">
		<button class="find btn btn-primary green m-0" style="width:100%;" 
		onclick ="updateAdminComms('{{$post->id}}')" >Actualizar</button>
	</div>	
</div>

@if($isFirts)
	<script>
		function  updateAdminComms(id){
			var formData = {};
			var comms_register = "{{ route('comms_register')}}";

			$('#admin-form-post-'+id).find("input, select").each(function (index, node) {
			formData[node.name] = node.value;
			});      
		    $.post(comms_register, formData).done(function (data) {
				data = data.json().result;
		    	Materialize.toast(data, 2000, 'teal darken-1');
		    	$('#content'+id).parent().parent().find('.card-coms').css('background-color', '#00acc1');
		    }).fail(function(data){
		  		data = "Se ha presentado aun error y no se ha podido actualizar el post."
		  		Materialize.toast(data, 2000, 'orange darken-1');
		    });
	    }

		function changeAdminProduct(id){
		   var productSelector = document.getElementById("admin_selected_product_" + id);
		   var productName = document.getElementById("admin_selected_product_name_" + id);
		   var val = document.getElementById("admin_product_child_" + productSelector.value);
		   if(val!=null){
		    productName.innerHTML = val.label;
		  }else{
		    productSelector.value = null
		    productName.innerHTML = "Selecciona un producto";
		  }
		}
	</script>
	@php($isFirts = False)
@endif
