@extends('comms.base-consult')

@section('consultscripts')
<script src="{{ asset('js/consults/comms-chart.js') }}"></script>	
@stop

@section('consultdescription')
<div class="row grey lighten-3 mb-0">
	<div class="col s12 m12 l12 page-title">
		<h2 class="breadcrumbs-title left">
		Gráficas Comms</h2>
	</div>
</div>
<div class="row blue darken-3">
	<div class="col s12 m12 l12 page-title">
		<p class="white-text" style="width: 80%;"> 
			En esta sección puede consultarse la información de COMMS panorama dentro de un período determinado de tiempo (Para una mejor visualización hacer consultas de máximo 60 días).
		</p>
	</div>
</div>
@stop

@section('consultcontent')
<consult-comms name="consult-comms" v-if='mostrarResultados === true'></consult-comms>
<h6 v-else >No hay datos para mostrar</h6>
@stop

@section('consulttemplate')
<template id="consult-comms">
	<div>
		<slot> </slot>
		<ul class='comms-item'>
			<li v-for="comms_section in comms_sections"><%comms_section['name']%></li>
		</ul>

		<div v-for="comms_type in comms_types" class='select-graphs col s12 m4 l3'>
			<button class='blue darken-3 btn mt-5' @click="chartSelect(comms_type['slug'])"><%comms_type["name"]%></button>
		</div>
		<div v-for="comms_type in comms_types" class="col s12 row comms-line">

			<div v-bind:id="'section-'+comms_type['slug']">
				<h5><%comms_type["name"]%></h5>
				<div v-bind:id="comms_type['slug']" class="comms-total">
					<%comms_type['slug']%>
				</div>
				<div v-for="comms_section in comms_sections" class='col s4'>

					<div v-bind:id="comms_section['slug']+'-'+comms_type['slug']" class='comms'><%comms_section["slug"]%>-<%comms_type['slug']%></div>
				</div>
			</div>
		</div>
	</div>
</template>
@stop

@section('consultstyles')

<style>

.super-align{
	vertical-align: super;
}
.no-show{
	display:none;
}
.hide-graphs{
	display:none;
}
.consult-btn{
	float: right; 
	margin: 0 12px 0 10px;
}
.filtros-graph{
	margin:15px 0 15px 0;
}
.adjust-list{
	margin:20px 0 0 0 !important;
}

#comms-charts h5{
	margin: 10px 10px 25px 10px;
}
.comms-display{
	clear:both;
}
.comms-display h4{
	display:inline-block;
}
.comms-display h6{
	font-size:1.4em;
	margin:3% 0 0 0;
}
.comms-display .total-filter{
	width: 20%;
	margin-top: 20px;
	float: right;
} 
.comms{
	height: 600px;
	width: 100%;
	border:2px solid;
	padding:10px;
	margin:15px 0 0 0;
}
.comms-total{
	width:600px;
	height:600px;
	margin:10px auto 10px;
}
.comms-line{
	margin:10px 0 10px 0;
}
.comms-item li{
	display:inline-block;
	border:2px solid #F3AD08;
	margin:5px 5px 5px 5px;
	padding:5px;
}
.amcharts-pie-label{
	fill: #333;
}
.select-graphs{
	/*padding: 0rem !important;*/
}
.select-graphs button{
	width: 100%;
	padding: 5px 5px 5px 5px !important;
	height: 100% !important;
	font-size: .8em;
}
.select-graphs button:focus{
	background-color:#1e88e5 !important;
}
.amcharts-export-menu .export-main>a, .amcharts-export-menu .export-drawing>a, .amcharts-export-menu .export-delayed-capturing>a {
	margin: 8px 10px 0 0px !important;
}
</style>
@stop