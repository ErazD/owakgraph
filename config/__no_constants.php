<?php
//Config::get('constants.comms');
return [

 /*
    |--------------------------------------------------------------------------
    | Valores fijos para Commons panorama 
    |--------------------------------------------------------------------------
    |
    | Aquí se especifican los valores 
    |
    */
    'comms' =>[
        'type' => [
            'facebook' => [
                'photopost' => ['slug'=>'photopost', 'name' => 'Photopost', 'description'=>'', 'color' => 'undefined'],
                'evento' => ['slug'=>'evento', 'name' => 'Evento', 'description'=>'', 'color' => 'undefined'],
                'linkad' => ['slug'=>'linkad', 'name' => 'Linkad', 'description'=>'', 'color' => 'undefined'],
                'carrousel' => ['slug'=>'carrousel', 'name' => 'Carrousel', 'description'=>'', 'color' => 'undefined'],
                'gif' => ['slug'=>'gif', 'name' => 'Gif', 'description'=>'', 'color' => 'undefined'],
                'video' => ['slug'=>'video', 'name' => 'Video', 'description'=>'', 'color' => 'undefined'],
                '360' => ['slug'=>'360', 'name' => '360°', 'description'=>'', 'color' => 'undefined'],
                'nota' => ['slug'=>'nota', 'name' => 'Nota', 'description'=>'', 'color' => 'undefined'],
                'estado' => ['slug'=>'estado', 'name' => 'Estado', 'description'=>'', 'color' => 'undefined'],
                're_post' => ['slug'=>'re_post', 'name' => 'Repost', 'description'=>'', 'color' => 'undefined'],
                'live_video' => ['slug'=>'live_video', 'name' => 'Live Video', 'description'=>'', 'color' => 'undefined'],
                'album' => ['slug'=>'album', 'name' => 'Álbum', 'description'=>'', 'color' => 'undefined'],
                'canvas' => ['slug'=>'canvas', 'name' => 'Canvas', 'description'=>'', 'color' => 'undefined'],
            ],
            'youtube' => [
                'video' => ['slug'=>'video', 'name' => 'Video', 'description'=>'', 'color' => 'undefined'],
                'live_video' => ['slug'=>'live_video', 'name' => 'Live Video', 'description'=>'', 'color' => 'undefined'],
            ],
        ],
        'balance' =>[
            'emocional' => ['slug'=>'emocional', 'name' => 'Emocional', 'description'=>'', 'color' => 'undefined'],
            'funcional' => ['slug'=>'funcional', 'name' => 'Funcional', 'description'=>'', 'color' => 'undefined'],
        ],
        'comunication_task' =>[
            'awareness' => ['slug'=>'awareness', 'name' => 'Awareness', 'description'=>'Recordación (Quiero usar el producto)', 'color' => 'undefined'],
            'desire' => ['slug'=>'desire', 'name' => 'Desire', 'description'=>'Deseo (Quiero usar el producto)', 'color' => 'undefined'],
            'seek_more_knowledge' => ['slug'=>'seek_more_knowledge', 'name' => 'Knowledge', 'description'=>'Conocimiento (Lo Aprendí de la Marca)', 'color' => 'undefined'],
            'transaction' => ['slug'=>'transaction', 'name' => 'Transaction', 'description'=>'Transacción (Lo Voy A Comprar)', 'color' => 'undefined'],
            'involvement' => ['slug'=>'involvement', 'name' => 'Involvement', 'description'=>'Involucramiento (Esta Marca Me Invita a Hacer Cosas Que Me Gustan)', 'color' => 'undefined'],
            'personality' => ['slug'=>'personality', 'name' => 'Personality', 'description'=>'Personalidad (Esta Marca Me Divierte - Me Identifico Con)', 'color' => 'undefined'],
            'recommendation' => ['slug'=>'recommendation', 'name' => 'Recommendation', 'description'=>'Recomendación (Si otros Lo consumen, yo lo consumiré)', 'color' => 'undefined'],
        ],
        'comunication_theme' => [
'fechas_especiales' => 'Fechas Especiales',
'tips' => 'Tips',
'institucional' => 'Institucional',
'producto' => 'Producto',
'responsabilidad_social' => 'Responsabilidad Social',
'promociones' => 'Promociones',
'motivacional' => 'Motivacional',
'brand_love' => 'Brand Love',
'actividades' => 'Actividades',
'programa_de_lealtad' => 'Programa de Lealtad',
'concurso' => 'Concurso',
'otros' => 'Otros',
        ],
        'google_micro_moment' => [
            'i-want-to-know' => ['slug'=>'i-want-to-know', 'name' => 'I wnat to know', 'description'=>'Qué quiero saber', 'color' => 'undefined'],
            'i-want-to-do' => ['slug'=>'i-want-to-do', 'name' => 'I wnat to do', 'description'=>'Qué quiero hacer', 'color' => 'undefined'],
            'i-want-to-buy' => ['slug'=>'i-want-to-buy', 'name' => 'I wnat to buy', 'description'=>'Qué quiero comprar', 'color' => 'undefined'],
            'i-want-to-go' => ['slug'=>'i-want-to-go', 'name' => 'I wnat to go', 'description'=>'Dónde quiero ir', 'color' => 'undefined'],
        ],
        'business-objetive' => [
            'brand_love' => ['slug'=>'brand_love', 'name' => 'Brand Love', 'description'=>'Construir, Fortalecer y Posicionar marcas', 'color' => 'undefined'],
            'lead_acquisition ' => ['slug'=>'lead_acquisition', 'name' => 'Lead Acquisition', 'description'=>'Atraer clientes potenciales', 'color' => 'undefined'],
            'product_launch' => ['slug'=>'product_launch', 'name' => 'Product Launch', 'description'=>'Lanzar productos al mercado', 'color' => 'undefined'],
            'portafolio_navigation' => ['slug'=>'portafolio_navigation', 'name' => 'Portafolio Navigation', 'description'=>'Navegar el portafolio (productos y/o servicios)', 'color' => 'undefined'],
            'myth_debunk' => ['slug'=>'myth_debunk', 'name' => 'MYTH Debunk', 'description'=>'Romper mitos del consumidor', 'color' => 'undefined'],
            'increase_penetration' => ['slug'=>'increase_penetration', 'name' => 'Increase Penetration', 'description'=>'Incrementar Penetración', 'color' => 'undefined'],
            'increase_frequency' => ['slug'=>'increase_frequency', 'name' => 'Increase Frequency', 'description'=>'Aumentar la frecuencia de uso', 'color' => 'undefined'],
            'brand_expertise' => ['slug'=>'brand_expertise', 'name' => 'Brand Expertise', 'description'=>'Educar a los consumidores', 'color' => 'undefined'],
            'product_superiority' => ['slug'=>'product_superiority', 'name' => 'Product Superiority', 'description'=>'Demuestra la superioridad de producto', 'color' => 'undefined'],
        ],
    ],

    'social_networks' => [
        'facebook' => ['slug' => 'facebook', 'name' => 'Facebook', ],
        'youtube' => ['slug' => 'youtube', 'name' => 'YouTube', ],
        'instragram' => ['slug' => 'instragram', 'name' => 'Instagram', ],
        'twitter' => ['slug' => 'twitter', 'name' => 'Twitter', ],
    ],
];