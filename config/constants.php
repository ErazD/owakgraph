<?php
//Config::get('constants.comms');
return [

 /*
    |--------------------------------------------------------------------------
    | Valores fijos para Commons panorama 
    |--------------------------------------------------------------------------
    |
    | Aquí se especifican los valores 
    |
    */
    'comms' =>[
        'type' => [
            'facebook' => [
                'photopost' => ['slug'=>'photopost', 'name' => 'Photopost', 'description'=>'', 'color' => 'undefined'],
                'evento' => ['slug'=>'evento', 'name' => 'Evento', 'description'=>'', 'color' => 'undefined'],
                'linkad' => ['slug'=>'linkad', 'name' => 'Linkad', 'description'=>'', 'color' => 'undefined'],
                'carrousel' => ['slug'=>'carrousel', 'name' => 'Carrousel', 'description'=>'', 'color' => 'undefined'],
                'gif' => ['slug'=>'gif', 'name' => 'Gif', 'description'=>'', 'color' => 'undefined'],
                'video' => ['slug'=>'video', 'name' => 'Video', 'description'=>'', 'color' => 'undefined'],
                '360' => ['slug'=>'360', 'name' => '360°', 'description'=>'', 'color' => 'undefined'],
                'nota' => ['slug'=>'nota', 'name' => 'Nota', 'description'=>'', 'color' => 'undefined'],
                'estado' => ['slug'=>'estado', 'name' => 'Estado', 'description'=>'', 'color' => 'undefined'],
                're_post' => ['slug'=>'re_post', 'name' => 'Repost', 'description'=>'', 'color' => 'undefined'],
                'live_video' => ['slug'=>'live_video', 'name' => 'Live Video', 'description'=>'', 'color' => 'undefined'],
                'album' => ['slug'=>'album', 'name' => 'Álbum', 'description'=>'', 'color' => 'undefined'],
                'canvas' => ['slug'=>'canvas', 'name' => 'Canvas', 'description'=>'', 'color' => 'undefined'],
            ],
            'youtube' => [
                'video' => ['slug'=>'video', 'name' => 'Video', 'description'=>'', 'color' => 'undefined'],
                'live_video' => ['slug'=>'live_video', 'name' => 'Live Video', 'description'=>'', 'color' => 'undefined'],
            ],
        ],
        'balance' =>[
            'emocional' => ['slug'=>'emocional', 'name' => 'Emocional', 'description'=>'', 'color' => 'undefined'],
            'funcional' => ['slug'=>'funcional', 'name' => 'Funcional', 'description'=>'', 'color' => 'undefined'],
        ],
        'comunication_task' =>[
            'awareness' => ['slug'=>'awareness', 'name' => 'Awareness', 'description'=>'Recordación (Quiero usar el producto)', 'color' => 'undefined'],
            'desire' => ['slug'=>'desire', 'name' => 'Desire', 'description'=>'Deseo (Quiero usar el producto)', 'color' => 'undefined'],
            'seek_more_knowledge' => ['slug'=>'seek_more_knowledge', 'name' => 'Knowledge', 'description'=>'Conocimiento (Lo Aprendí de la Marca)', 'color' => 'undefined'],
            'transaction' => ['slug'=>'transaction', 'name' => 'Transaction', 'description'=>'Transacción (Lo Voy A Comprar)', 'color' => 'undefined'],
            'involvement' => ['slug'=>'involvement', 'name' => 'Involvement', 'description'=>'Involucramiento (Esta Marca Me Invita a Hacer Cosas Que Me Gustan)', 'color' => 'undefined'],
            'personality' => ['slug'=>'personality', 'name' => 'Personality', 'description'=>'Personalidad (Esta Marca Me Divierte - Me Identifico Con)', 'color' => 'undefined'],
            'recommendation' => ['slug'=>'recommendation', 'name' => 'Recommendation', 'description'=>'Recomendación (Si otros Lo consumen, yo lo consumiré)', 'color' => 'undefined'],
        ],
        'comunication_theme' => [
            'fechas_especiales' => ['slug'=>'fechas_especiales', 'name' => 'Fechas Especiales', 'description'=>' ', 'color' => 'undefined'],
            'tips' => ['slug'=>'tips', 'name' => 'Tips', 'description'=>' ', 'color' => 'undefined'],
            'producto' => ['slug'=>'producto', 'name' => 'Producto', 'description'=>' ', 'color' => 'undefined'],
            'responsabilidad_social'=> ['slug'=>'responsabilidad_social', 'name' => 'Responsabilidad Social', 'description'=>' ', 'color' => 'undefined'],
            'promociones' => ['slug'=>'promociones', 'name' => 'Promociones', 'description'=>' ', 'color' => 'undefined'],
            'motivacional' => ['slug'=>'motivacional', 'name' => 'Motivacional', 'description'=>' ', 'color' => 'undefined'],
            'brand_love' => ['slug'=>'brand_love', 'name' => 'Brand Love', 'description'=>' ', 'color' => 'undefined'],
            'actividades' => ['slug'=>'actividades', 'name' => 'Actividades', 'description'=>' ', 'color' => 'undefined'],
            'programa_de_lealtad' => ['slug'=>'programa_de_lealtad', 'name' => 'Programa de Lealtad', 'description'=>' ', 'color' => 'undefined'],
            'concurso' => ['slug'=>'concurso', 'name' => 'Concurso', 'description'=>' ', 'color' => 'undefined'],
            'institucional' => ['slug'=>'institucional', 'name' => 'Institucional', 'description'=>' ', 'color' => 'undefined'],
            'otros' => ['slug'=>'otros', 'name' => 'Awareness', 'description'=>' ', 'color' => 'undefined'],
        ],
        'google_micro_moment' => [
            'i-want-to-know' => ['slug'=>'i-want-to-know', 'name' => 'I want to know', 'description'=>'Qué quiero saber', 'color' => 'undefined'],
            'i-want-to-do' => ['slug'=>'i-want-to-do', 'name' => 'I want to do', 'description'=>'Qué quiero hacer', 'color' => 'undefined'],
            'i-want-to-buy' => ['slug'=>'i-want-to-buy', 'name' => 'I want to buy', 'description'=>'Qué quiero comprar', 'color' => 'undefined'],
            'i-want-to-go' => ['slug'=>'i-want-to-go', 'name' => 'I want to go', 'description'=>'Dónde quiero ir', 'color' => 'undefined'],
        ],
        'business-objective' => [
            'brand_love' => ['slug'=>'brand_love', 'name' => 'Brand Love', 'description'=>'Construir, Fortalecer y Posicionar marcas', 'color' => 'undefined'],
            'lead_acquisition ' => ['slug'=>'lead_acquisition', 'name' => 'Lead Acquisition', 'description'=>'Atraer clientes potenciales', 'color' => 'undefined'],
            'product_launch' => ['slug'=>'product_launch', 'name' => 'Product Launch', 'description'=>'Lanzar productos al mercado', 'color' => 'undefined'],
            'portafolio_navigation' => ['slug'=>'portafolio_navigation', 'name' => 'Portafolio Navigation', 'description'=>'Navegar el portafolio (productos y/o servicios)', 'color' => 'undefined'],
            'myth_debunk' => ['slug'=>'myth_debunk', 'name' => 'MYTH Debunk', 'description'=>'Romper mitos del consumidor', 'color' => 'undefined'],
            'increase_penetration' => ['slug'=>'increase_penetration', 'name' => 'Increase Penetration', 'description'=>'Incrementar Penetración', 'color' => 'undefined'],
            'increase_frequency' => ['slug'=>'increase_frequency', 'name' => 'Increase Frequency', 'description'=>'Aumentar la frecuencia de uso', 'color' => 'undefined'],
            'brand_expertise' => ['slug'=>'brand_expertise', 'name' => 'Brand Expertise', 'description'=>'Educar a los consumidores', 'color' => 'undefined'],
            'product_superiority' => ['slug'=>'product_superiority', 'name' => 'Product Superiority', 'description'=>'Demuestra la superioridad de producto', 'color' => 'undefined'],
        ],
    ],
    'social_networks' => [
        'facebook' => ['slug' => 'facebook', 'name' => 'Facebook', 'color' => 'undefined', 'models' => ['page' => 'App\Fanpage', 'post' => 'App\Post', 'metric' => 'App\Metric', 'page_id' => 'fanpage_id', 'post_id' => 'post_id'] ],
        'youtube' => ['slug' => 'youtube', 'name' => 'YouTube', 'color' => 'undefined', 'models' => ['page' => 'App\YoutubeChannel', 'post' => 'App\YoutubeVideo', 'metric' => 'App\YoutubeMetrics', 'page_id' => 'youtube_channel_id', 'post_id' => 'post_id'] ],

        'instagram' => ['slug' => 'instagram', 'name' => 'Instagram', 'color' => 'undefined', 'models' => ['page' => 'App\InstaProfile', 'post' => 'App\InstaPost', 'metric' => 'App\InstaMetrics', 'page_id' => 'profile_id', 'post_id' => 'post_id'] ],
        'twitter' => ['slug' => 'twitter', 'name' => 'Twitter', 'color' => 'undefined', 'models' => ['page' => 'Fanpage', 'post' => 'Post', 'metric' => 'Metric', 'page_id' => 'fanpage_id', 'post_id' => 'post_id'] ],
    ],
    'metrics' => [
        'facebook' => ['reactions', 'comments', 'shares', 'views'],
        'youtube' => ['views', 'likes', 'dislikes', 'comments'],
        'instagram' => ['views', 'likes', 'comments'],
    ],
    'user_roles' => [
        'permission' => [
            'user' => ['slug' => 'user', 'name' => 'Usuario Básico', 'description' => 'Permiso Básico'],
            'owakean' => ['slug' => 'owakean', 'name' => 'Owakeans', 'description' => 'Permiso Para miembros de Owak'],
            'two_factor' => ['slug' => 'two_factor', 'name' => 'Permisos de Two Factor', 'description' => 'Permiso para calificar commsPanorama.'],
            'two_factor_principal' => ['slug' => 'two_factor_principal', 'name' => 'Permisos de Juez para Two Factor', 'description' => 'Permiso para calificar commsPanorama.'],
            'admin' => ['slug' => 'admin', 'name' => 'Permisos de administrador', 'description' => 'Permisos completos.'],
        ],

        'permission_value' => [
            0 => ['value' => 0, 'name' => 'Sólo Lectura', 'description' => 'Solo se puede vizualizar el contenido'],
            1 => ['value' => 1, 'name' => 'Escritura', 'description' => 'Puede ver y editar el contenido'],

            10 => ['value' => 10, 'name' => 'Administrador', 'description' => 'Administrador con permisos para modificar el contenido'],
        ],
    ],

    'scrap_types' => [
        'null' => ['value' => 'null', 'name' => 'Default', 'description' => 'Valor por defecto'],
        -1 => ['value' => -1, 'name' => 'N/A', 'description' => 'No se realiza scrap'],
        0 => ['value' => 0, 'name' => 'Hora', 'description' => 'El llamado a scrap de la lista se realiza cada dos horas'],
        1 => ['value' => 1, 'name' => 'Diario', 'description' => 'El llamado a scrap de la lista se realiza cada día'],
        2 => ['value' => 2, 'name' => 'Semanal', 'description' => 'El llamado a scrap de la lista se realiza cada Semana'],
        3 => ['value' => 3, 'name' => 'Mensual', 'description' => 'El llamado a scrap de la lista se realiza cada mes'],
        4 => ['value' => 4, 'name' => 'Anual', 'description' => 'El llamado a scrap de la lista se realiza cada año'],
    ],
        
];