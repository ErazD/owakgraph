// var colorReactions="#FF6600";
// var colorComments="#FCD202";
// var colorShares="#B0DE09";
var url = window.location.protocol+"//"+window.location.host;

let app = new Vue({
	el: '#stats',
	data: {
		colorReactions:"#FF6600",
		colorComments:"#FCD202",
		colorShares:"#B0DE09",
		colorViews:"#F72C2C",
		type_post:"post",
		since:'',
		until:'',
		interval:28,
		chartMH:undefined,
		chartMD:undefined,
	},
	methods: {
		setDate: function(interval = this.interval, update=true){
			var d = new Date(this.until);
			switch (interval){
				case "hoy":
				d = new Date();
				this.since = d.toISOString().split("T")[0];
				this.until = d.toISOString().split("T")[0];
				console.log("hoy");
				break;
				case "day":
				this.since = d.toISOString().split("T")[0];
				this.until = d.toISOString().split("T")[0];
				console.log("day");
				break;
				case "all":
				this.since = "";
				this.until = "";
				console.log("all");
				break;
				default:	
				var firstDate = new Date(d);
				firstDate.setDate(d.getDate() - parseInt(this.interval));
				this.until = d.toISOString().split("T")[0];
				this.since = firstDate.toISOString().split("T")[0];
				console.log(interval);
				break;
			}
			if(update){
				this.zoomChart(this.chartMD);
			}
		},
		metricsByHour: function(filter){
			//fetch(url+"/posts/"+this.postID+"/allmetrics/hours")
			fetch(url+"/posts/"+this.postID+"/allmetrics/hours")
			.then(r => r.json()).then(json => {
				this.chartMH = this.createChart(json, 'hoursChart', '#4CAF50');
			});
		},
		metricsByDay: function(filter){
			
			//fetch(url+"/posts/"+this.postID+"/allmetrics/day")
			fetch(url+"/posts/"+this.postID+"/allmetrics/day")
			.then(r => r.json()).then(json => {
				console.log("CHARTTTTTTTTTT");
				this.chartMD =  this.createChart(json, 'daysChart', '#2196f3');
				
				console.log(this.chartMD);
				this.setDate("hoy", false);
				this.interval=28;
				this.setDate(28, false);
				
				this.zoomChart(this.chartMD);
				
			});
			
		},

		getAxes:function(){
			var axes =	[{
				"id":"v1",
				"axisColor": this.colorReactions,
				"axisThickness": 2,
				"axisAlpha": 1,
				"position": "left",
				"precision": "0"
			}, {
				"id":"v2",
				"axisColor": this.colorComments,
				"axisThickness": 2,
				"axisAlpha": 1,
				"position": "right",
				"precision": "0"
			}, {
				"id":"v3",
				"axisColor": this.colorShares,
				"axisThickness": 2,
				"gridAlpha": 0,
				"offset": 50,
				"axisAlpha": 1,
				"position": "right",
				"precision": "0"
			}];
			if(this.type_post == "video"){
				axes.push( {
					"id":"v4",
					"axisColor": this.colorViews,
					"axisThickness": 2,
					"gridAlpha": 0,
					"offset": 50,
					"axisAlpha": 1,
					"position": "left",
					"precision": "0"
				});
			}
			return axes;
		}, 
		getGraphs:function(){
			var graphs = [{
				"valueAxis": "v1",
				"lineColor": this.colorReactions,
				"bullet": "round",
				"bulletBorderThickness": 1,
				"hideBulletsCount": 30,
				"title": "Reacciones",
				"valueField": "reactions",
				"fillAlphas": 0
			}, {
				"valueAxis": "v2",
				"lineColor": this.colorComments,
				"bullet": "square",
				"bulletBorderThickness": 1,
				"hideBulletsCount": 30,
				"title": "Comentarios",
				"valueField": "comments",
				"fillAlphas": 0
			}, {
				"valueAxis": "v3",
				"lineColor": this.colorShares,
				"bullet": "triangleUp",
				"bulletBorderThickness": 1,
				"hideBulletsCount": 30,
				"title": "Shares",
				"valueField": "shares",
				"fillAlphas": 0
			}];
			if(this.type_post == "video"){
				graphs.push({
					"valueAxis": "v4",
					"lineColor": this.colorViews,
					"bullet": "triangleDown",
					"bulletBorderThickness": 1,
					"hideBulletsCount": 30,
					"title": "Vistas",
					"valueField": "views",
					"fillAlphas": 0
				});	
			}
			return graphs;
		},
		createChart: function(data, element, color){		
			console.log(element);
			console.log(data);
			var chart = AmCharts.makeChart(element, {
				"type": "serial",
				"theme": "light",
				"legend": {
					"valueAlign":"left",
					"useGraphSettings": true
				},
				"dataProvider": data,
				"synchronizeGrid":true,
				"valueAxes": this.getAxes(),
				"graphs": this.getGraphs(),
				"chartScrollbar": {},
				"categoryField": "date",
				"categoryAxis": {
					"parseDates": true,
					"minPeriod": "mm",
					"axisColor": "#DADADA"
				},
				"export": {
					"enabled": true,
					"position": "bottom-right"
				},
				"chartCursor": {
					"valueBalloonsEnabled": true,
					"fullWidth": true,
					"cursorAlpha": 0.1,
					"valueLineBalloonEnabled": true,
					"valueLineEnabled": true,
					"valueLineAlpha": 0.5
				}
			});
			console.log(chart);
			return chart;
		},
		zoomChart:function(chart){
			if(this.since!="" && this.until!=""){
				chart.zoomToDates(new Date(this.since), new Date(this.until));
			}else{
				chart.zoomOut();
			}

		}

	},
	created: function(){
		this.metricsByHour('reactions');
		this.metricsByDay('reactions');
		this.type_post = (type_post) ? type_post:this.type_post;
	},
	computed: {
		postID: function(){
			var route = window.location.href;
			var route_array = route.split("//");
			var route_values = route_array[1].split("/");
			return post_id = route_values[2];
			// return post_id = route_values[5];

		}
	}
});
