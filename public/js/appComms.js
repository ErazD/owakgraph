var comms_register ="#";
function showCommsFanpage(idPost,numSection){
  console.log(idPost + numSection);
  var content = $("#content"+idPost);
  var section = $("#section" + idPost + "_" + numSection);
  content.html(section.html());
  $('.tooltipped').tooltip({delay: 50});
  
}
function showContest(id, socialNetwork="fb"){
  var selected = document.getElementById("theme-" + id).value;
  var concurso = $('#concursos-' + id);
  if(selected == 'concurso'){
    concurso.removeClass('hide');
  }else{
    concurso.addClass('hide');
  }
}
 
function  updateComms(id){
      //alert(id);
      var formData = {};
      var completo = true;

      $('#form-'+id).find("input, select").each(function (index, node) {
        formData[node.name] = node.value;
        completo &= !node.required || (node.required && node.value!="");
      });
      console.log(formData);
      console.log( comms_register);
     
      if(completo){
            $.post(comms_register, formData).done(function (data) {
               Materialize.toast(data, 2000, 'teal darken-1');
               $('#content'+id).parent().parent().find('.card-coms').css('background-color', '#00acc1');
            }).fail(function(data){
              data = "Se ha presentado aun error, revisa si ya has iniciado sesión."
              Materialize.toast(data, 2000, 'orange darken-1');
            });
          }else{
            data = "Revisa que hayas llenado los campos obligatorios: \n Tipo, Balance, tarea de comunicación, Tema de comunicación y Producto."
            Materialize.toast(data, 2000, 'orange darken-1');
          }
    }
    $('.coms-info-item').click(function(){
      $('.coms-info-item').removeClass('active');
      $('.post-card').css('width', '49%');
      $('.card-summary').css('width', '91.6666666667%');
      $('.comms-summary').removeClass('show-view');
      $(this).addClass('active');
      $(this).parent(".card-coms").parent('.post-card').css('width', '100%');
      $(this).parent(".card-coms").parent().find('.card-summary').css('width', '41.6666666667%');
      $(this).parent(".card-coms").parent().find('.comms-summary').addClass('show-view');
    });
    
    $('.close-btn').click(function(){
      $('.coms-info-item').removeClass('active');
      $('.post-card').css('width', '49%');
      $('.card-summary').css('width', '91.6666666667%');
      $('.comms-summary').removeClass('show-view');
    });

//Comms Formulario
$('.coms-info-icon').click(function(){
  $('.coms-info-icon').removeClass('active');
  $('.post-card').css('width', '49%');
  $('.card-summary').css('width', '91.6666666667%');
  $('.comms-summary').removeClass('show-view');
  $(this).addClass('active');
  $(this).parent(".card-coms").parent('.post-card').css('width', '100%');
  $(this).parent(".card-coms").parent().find('.card-summary').css('width', '41.6666666667%');
  $(this).parent(".card-coms").parent().find('.comms-summary').addClass('show-view');
});
$('.close-btn').click(function(){
  $('.coms-info-icon').removeClass('active');
  $('.post-card').css('width', '49%');
  $('.card-summary').css('width', '91.6666666667%');
  $('.comms-summary').removeClass('show-view');
});

// $('.datepicker').pickadate();
let filterForm = new Vue({
el:'.filtros-graph',
delimiters:['<%', '%>'],
data:{
  allElements:'',
  filterType:'',
  pedido_id:'',
},
methods:{
  filterPosts:function(){
   
    var posts = document.querySelectorAll('.post-card');
    var filterCriteria = this.filterType+'-'+this.pedido_id; 
  
    for(var i = 0; i< posts.length; i++){
        if(this.pedido_id != 'todos'){
        if(posts[i].className.indexOf(filterCriteria) == -1){
            posts[i].style.display = 'none';
       }else{
         posts[i].style.display = 'flex';
       }
      }else{
         posts[i].style.display = 'flex';
      }
    }
  },
}
});


let appCommsForm= new Vue({
  el:'.content-comms',
  delimiters:['<%', '%>'],

  data:{
    selectType:'',
    selectBalance:'',
    selectTask:'',
    selectTheme:'',
    selectProduct:'',
    selectContest:'',
    optionType:[],
    optionBalance:[],
    optionTasks:[],
    optionTemas:[],
    optionProduct:[],
    optionContest:[],
  },
  methods:{
    saveComms:function(){
      console.log(this.selectType);
    },
    checkId:function(id){
      var tema = $('#theme-'+ id);
      var concurso = $('#concursos-' + id);
      if(this.selectTheme == 'Concurso'){
        concurso.removeClass('hide');
      }else{
        concurso.addClass('hide');
      }
    },
    checkConcurso:function(concurso){

    },
  }
});