let dateSelectorApp = new Vue({
	el: '#date-form',
	delimiters: ['<%', '%>'],
	data: {
		since:'',
		until:'',
		interval:28,
	},
	methods: {
		setDate: function(interval = this.interval){
			if(this.until.length == 0 ){
			    var dd = new Date();
			    this.until = dd.toISOString().split('T')[0];
			}
			var d = new Date(this.until);
			console.log(d);
			switch (interval){
				case "hoy":
				d = new Date();
				this.since = d.toISOString().split("T")[0];
				this.until = d.toISOString().split("T")[0];
				console.log("hoy");
				break;
				case "day":
				console.log('del dia' + d.toISOString().split('T')[0]);
				this.since = d.toISOString().split("T")[0];
				this.until = d.toISOString().split("T")[0];
				console.log("day");
				break;
				case "all":
				console.log('del todo' + d.toISOString().split('T')[0]);
				this.since = "";
				this.until = "";
				console.log("all");
				break;
				default:	
				var firstDate = new Date(d);
				firstDate.setDate(d.getDate() - parseInt(this.interval));
				this.until = d.toISOString().split("T")[0];
				console.log('fecha hasta default: ' + this.until);
				this.since = firstDate.toISOString().split("T")[0];
				console.log('fecha desde default:' + this.since);
				console.log(interval);
				break;
			}
		}

	},
	created: function(){
		   // var dd = new Date();
     //       var defaultDate = dd.toISOString().split('T')[0];
     //       this.until = defaultDate; 
     //       console.log(this.until);
	},
	computed: {
		getDates:function(){
					dates="";
					if(this.until != "" ){
						if(this.since != ""){
						 	dates = "/" + this.since + "/" + this.until;
						}else{
							dates = "/" + this.until + "/" + this.until;
						}
					}
				return dates;
			}
	}
});
