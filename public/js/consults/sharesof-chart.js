Vue.component('consult-sharesof', {
	template: "#consult-sharesof",
	delimiters: ['<%', '%>'],
	data: function(){
		return {
		}
	},
	methods: {	
		/**
		* Si existen datos obtenidos, vuelve a dar formato a los datos y genera la gráfica.
		*/
		reloadChart: function(){
			if(parentApp.allData.metrics){
				parentApp.commsData = parentApp.getArraysOfData(parentApp.to);
				this.createSpecificChart('grafica-shares-of', parentApp.commsData);
			}
		},
		/**
		* Llama la creación de las gráficas.
		* @param {string} id: id usado para su creación en el html.
		* @param {array} data: información para crear la gráfica.
		*/
		createSpecificChart(id, data){
			var i = 0;
			var f = (parentApp.social_network == 'all')? 8 :0;
			if(parentApp.social_network == 'Facebook'){
				i = 0;
				f = 4;
			}else if(parentApp.social_network == 'YouTube'){
				i = 4;
				f = 8;
			}
			else if(parentApp.social_network == 'Instagram'){
				i = 8;
				f = 11;
			}
			for (var x = i; x < f; x++) {
				this.setupChart(x, id, data);
			}
			
		},
		/**
		* Genera los datos recibidos por la gráfica
		* @param {string} type: tipo de la gráfica ('reactions', 'comments', 'shares', 'views')
		* @param {dict} commsData: información base para organizar
		* @param {string} selected: elemento seleccionado en la gráfica
		* @return {array} lista con la información que recibe la gráfica
		*/
		generateChartData: function(type, commsData = parentApp.commsData, selected = undefined){
			var chartData = [];
			var total = parentApp.allData.metrics[type];
			  console.log(commsData);
			if(total == 0)
				return false;

			$.each(commsData, function(key,value){

				var val = parseInt(value.metrics[type]);
				var per = (val / parseInt(total) * 100).toFixed(2);
				if(val > 0 ){
					chartData.push({
						name: value.name,
						value: val,
						percent: per+"%",
						color: value.color,
 						// color: sharesofApp.getColorData(value.name),
 					});
				}
			});			
			// console.log(chartData);
			// console.log(commsData);
			return chartData;
		},
		/**
		* Define el gráfico a crear según el índice dado
		* @param {number} type: índice del tipo a crear.
		* @param {string} id: id usado para su creación en el html.
		* @param {array} commsData: información para crear la gráfica.
		*/
		setupChart: function(type, id="", commsData=parentApp.commsData){
			if(id!=""){
				id = convertToSlug(id) + "-";
			}
			var totals = parentApp.allData.metrics;
			console.log('Tipo de grafica:' + type);
			switch(type){
				case 0:				
				this.createChart("fb_reactions", id+"fb-reactions", commsData, "Facebook Reacciones: "+ formatNumber(parentApp.allData.metrics['fb_reactions']));
				break;
				case 1:				
				this.createChart("fb_comments", id+"fb-comments", commsData, "Facebook Comentarios: "+ formatNumber(parentApp.allData.metrics['fb_comments']));
				break;
				case 2:				
				this.createChart("fb_shares", id+"fb-shares", commsData, "Facebook Compartidos: "+ formatNumber(parentApp.allData.metrics['fb_shares']));
				break;
				case 3:				
				this.createChart("fb_views", id+"fb-views", commsData, "Facebook Views: "+ formatNumber(parentApp.allData.metrics['fb_views']));
				break;

				case 4:				
				this.createChart("yt_likes", id+"yt-likes", commsData, "Youtube Likes: "+ formatNumber(parentApp.allData.metrics['yt_likes']));
				break;
				case 5:				
				this.createChart("yt_dislikes", id+"yt-dislikes", commsData, "Youtube Dislikes: "+ formatNumber(parentApp.allData.metrics['yt_dislikes']));
				break;
				case 6:
				this.createChart("yt_comments", id+"yt-comments", commsData, "Youtube Comentarios: "+ formatNumber(parentApp.allData.metrics['yt_comments']));
				break;
				case 7:				
				this.createChart("yt_views", id+"yt-views", commsData, "Youtube Views: "+ formatNumber(parentApp.allData.metrics['yt_views']));
				break;

				case 8:				
				this.createChart("insta_likes", id+"insta-likes", commsData, "Instagram Likes: "+ formatNumber(parentApp.allData.metrics['insta_likes']));
				break;
				case 9:
				this.createChart("insta_comments", id+"insta-comments", commsData, "Instagram Comentarios: "+ formatNumber(parentApp.allData.metrics['insta_comments']));
				break;
				case 10:				
				this.createChart("insta_views", id+"insta-views", commsData, "Instagram Views: "+ formatNumber(parentApp.allData.metrics['insta_views']));
				break;
			}
		},

		/**
		* Crea una nueva gráfica
		* @param {string} type: tipo de gáfica a crear.
		* @param {string} element: id del elemento html donde se creará la gráfica.
		* @param {array} commsData: información para crear la gráfica.
		* @param {string} title: Título de la gráfica a crear.
		* Postcondition: Se ha creado una gráfica en el html, en el div con id elemento.
		*/
		createChart: function( type, element, commsData=parentApp.commsData, title=""){		
			console.log("AAAAAAAAA " + element);
			var chartData = this.generateChartData(type, commsData);
			var elemento = document.getElementById(element);
			if(chartData == false){ 
				// elemento.parentNode.classList.add('no-show');  
				$('#'+element).html('<br><h5 class="no-data">No hay '+type+'</h5><br>');
			}else{
				// sharesofApp.ocultar = true;
				// elemento.parentNode.classList.remove('no-show');
				if(title==""){ title = type; }
				AmCharts.makeChart(element, {
					"type": "pie",
					"theme": "light",
					"dataProvider": chartData,
					"labelText": "[[percent]]",
					"labelRadius": 20,
					"labelTickAlpha":3,
					"balloonText": "[[name]]: [[value]]",
					"titleField": "name",
					"valueField": "value",
					"outlineColor": "transparent",
					"outlineAlpha": 0.8,
					"outlineThickness": 2,
					"colorField": "color",
					"pulledField": "pulled",
					"titles": [{
						"text": title
					}],
					"legend":{
						"position":"bottom",
						"valueText":"",
						"valueAlign":"center",
						"autoMargins":true
					},
					"listeners": [{
						"event": "clickSlice",
						"method": function(event) {
							if(type == "product"){
								var chart = event.chart;
								var selected = undefined;
								if (event.dataItem.dataContext.type != undefined) {
									selected = event.dataItem.dataContext.type;
								} else {
									selected = undefined;
								}						
								chart.dataProvider = this.generateChartData(type, commsData, selected);
								chart.validateData();
								// chart.animateAgain();
							}
						},
					}],
					"export": {
						"enabled": true
					}	
				});
			}
		},
	},
	created: function(){
		parentApp.route = "comms/sharesof";
		parentApp.consultRoute = "comms/consult";
		var vm = this;
		parentApp.consultMethod = function(json){
			vm.reloadChart();
		};
	},
});








