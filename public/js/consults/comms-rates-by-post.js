 Vue.component('consult-top-posts', {
 	template: '#consult-top-posts',
 	delimiters: ['<%', '%>'],
 	data: function(){
 		return {
 			globalPositions:[],
 			globalPositionsKey: false,
 		}
 	},
 	methods: {
		/**
		* Función principal para organizar los datos obtenidos del servidor.
		* Obtiene Los valores de los datos hasta el nivel de posts, y les agrega los campos necesarios a mostrar.
		* @return {array} lista de posts de la consulta.
		*/
		generateMetricsReport: function(){
			var vm = this;
			to = '';
            
			if(parentApp.to == 'Page'){
			to = 'Page';
			}else{
             switch(parentApp.social_network){
            	case 'Facebook':
            	to = 'Fanpage';
            	break;
            	case 'YouTube':
                 to = 'YoutubeChannel';
            	break;
            	case 'Instagram':
            	to = 'InstaProfile';
            	break; 
            }
			}
			// Obtiene los datos hasta el nivel de Fanpage
			var actualData = parentApp.getArraysOfData(to,parentApp.allData);	
			// var allFanpages = actualData;
			// recorre los posts para gregarles la información del elemento al cual pertenece y terminar el cálculo de sus tasas
			var allPosts = [];
			 $.each(actualData, function(key, data){
					vm.addBrandInfoToPost(data['posts'], data);
					allPosts = allPosts.concat(data['posts']);
			});	

			parentApp.commsData = allPosts;
			return allPosts;
		},
		/**
		* Agrega al post la información del elemento al que pertenece (nombre y color), y las tasas globales.
		* @param {array} postsList: lista con la información de los posts.
		* @param {dict} parentData: información del elemento al que pertenece (parentApp.to)
		* @param {dict} maxes: métricas totales de la consulta.
		*/
		addBrandInfoToPost: function(postsList, parentData, maxes=parentApp.allData.metrics){
			console.log('Lista de publicaciones: ');
			console.log(postsList);
			$.each(postsList, function(key, post){
				post["name"] = parentData['name'];
				post["color"] = parentData['color'];
				if(post['fb_views'] > 0){
					var total = maxes['fb_views'];
					post['total_amplification_rate'] = 100 * post['fb_shares'] / total;
					post['total_participation_rate'] = 100 * post['fb_comments'] / total;
					post['total_reaction_rate'] = 100 * post['fb_reactions'] / total;
				}else if(maxes['fb_reactions'] > 0){
					var total = maxes['fb_reactions'];
					post['total_amplification_rate'] = 100 * post['fb_shares'] / total;
					post['total_participation_rate'] = 100 * post['fb_comments'] / total;
					post['total_reaction_rate'] = 0;
				}else{
					post['total_amplification_rate'] = 0;
					post['total_participation_rate'] = 0;
					post['total_reaction_rate'] = 0;
				}

			});

		},
		/**
		* Ordena una columna de la tabla de posiciones por columna dada.
		* @param {string} key: key de la columna a organizar.
		* @param {string} name: nombre de la columan a mostrar.
		* postcondition: el orden de this.globalPositions ha sido actualizado.
		*/
		sortGlobalPositions: function(key=false, name = ""){
			if(key == false || this.globalPositionsKey.key == key){
				this.globalPositionsKey = false;
				this.selectSortColumn(false);
				return;
			}

			this.globalPositionsKey ={}
			this.globalPositionsKey.key = key;
			this.globalPositionsKey.name = name;
			
			this.selectSortColumn(this.globalPositionsKey.key);
			  // Re-order the data provider
           
			this.globalPositions.sort( function( a, b ) {
			  	if ( a[key] > b[key] ) {
			  		return -1;
			  	} else if ( a[key] < b[key] ) {
			  		return 1;
			  	}
			  	return 0;
			  });
		},
		/**
		* Agrega o quita la clase "selected" a los elementos html
		* @param {string} key: nombre de la columna a seleccionar
		* Postcondition: las clases de las columnas del html han cambiado
		*/
		selectSortColumn: function(key=false){
			$('.sort-selectable').removeClass('selected');
			if(key){
				$('.sort-selectable-' + key).addClass('selected');
			}
		}
	},
	created: function(){
		parentApp.route = "comms/consult/posts";
		parentApp.consultRoute = "comms/consult";
		var vm = this;

		parentApp.consultMethod = function(json){
			vm.globalPositions = vm.generateMetricsReport();			
			var funct = function(){
				$('.tooltipped').tooltip({delay: 50});
			};
			parentApp.updateFunctions.push(funct);
		}
		parentApp.mostrarTo = false;
	}
});
