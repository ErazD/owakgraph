Vue.component('consult-activity', {
	template: '#consult-activity',
	delimiters:['<%', '%>'],
	data:function(){
		return{
			DAYS:["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
			SOCIAL_NETWORK_PRE:{'Facebook':'fb_', 'Youtube':'yt_', 'Instagram':'ins_'},
			METRICS_BY_PRE:{'fb_':['reactions', 'comments', 'shares', 'views'], 'yt_':['views', 'likes', 'dislikes', 'comments']},
			emptyCharts:'',
			intervalLength:0,
			dataSets:{},
		}
	},
	methods:{
/* Si existen datos obtenidos, vuelve a dar formato a los datos y genera la gráfica.
*/
reloadChart: function(){
	this.dataSets = parentApp.allData.intervals;
	console.log(this.dataSets);
	var result = [];
	if(this.dataSets){
		this.intervalLength = this.dataSets.length;
		for(var i = 0; i< this.intervalLength; i++){
			var type = this.dataSets[i].subtype;
			var arraysOfData={};
			arraysOfData = parentApp.getArraysOfData(parentApp.to, this.dataSets[i][type], type);
			result.push({'fecha_i':this.dataSets[i]['date_i'], 'fecha_f':this.dataSets[i]['date_f'], 'data': arraysOfData[i]});
			// result.push(this.dataSets[i]['date_f']);
			// result.push(parentApp.getArraysOfData(parentApp.to, this.dataSets[i][type], type));
			parentApp.commsData = result;
		}
	
		// parentApp.commsData = result;	
		this.createSpecificChart('grafica-activity', parentApp.commsData);
	}
},
createSpecificChart(id, data){
	console.log(data);
	for (var i = 0; i < 4; i++) {
		this.setupChart(i, id);
	}
},
/**
* Genera los datos recibidos por la gráfica
* @param {string} type: tipo de la gráfica ('reactions', 'comments', 'shares', 'views')
* @param {dict} commsData: información base para organizar
* @param {string} selected: elemento seleccionado en la gráfica
* @return {array} lista con la información que recibe la gráfica
*/
generateChartData: function(type, commsData = parentApp.commsData, selected = undefined){
	var chartData = [];
	var total = parentApp.allData.metrics[type];
	if(total == 0)
		return false;

	$.each(commsData, function(key,value){
		var val = parseInt(value.data.metrics[type]);
		var per = (val / parseInt(total) * 100).toFixed(2);
		if(val >= 0 ){
			chartData.push({
				'name':'week ' + key,
				'interval': value.fecha_i + '/' + value.fecha_f,
				'value.name': val,
				'color': value.color,
});
		}
	});			
// console.log(chartData);
// console.log(commsData);
return chartData;
},
/**
* Define el gráfico a crear según el índice dado
* @param {number} type: índice del tipo a crear.
* @param {string} id: id usado para su creación en el html.
* @param {array} commsData: información para crear la gráfica.
*/
setupChart: function(type, id="", commsData=parentApp.commsData){
	if(id!=""){
		id = convertToSlug(id) + "-";
	}
	// var totals = parentApp.allData.metrics;
	switch(type){
		case 0:				
		this.createChart("reactions", id+"reactions", commsData);
		break;
		case 1:				
		this.createChart("comments", id+"comments", commsData);
		break;
		case 2:				
		this.createChart("shares", id+"shares", commsData);
		break;
		case 3:				
		this.createChart("views", id+"views", commsData);
		break;
	}
},
/**
* Crea una nueva gráfica
* @param {string} type: tipo de gáfica a crear.
* @param {string} element: id del elemento html donde se creará la gráfica.
* @param {array} commsData: información para crear la gráfica.
* @param {string} title: Título de la gráfica a crear.
* Postcondition: Se ha creado una gráfica en el html, en el div con id elemento.
*/
createChart: function( type, element, commsData=parentApp.commsData, title=""){		
	console.log("AAAAAAAAA " + element);
	var chartData = this.generateChartData(type, commsData);
	
	console.log(chartData);

	var elemento = document.getElementById(element);
	if(chartData == false){ 
// elemento.parentNode.classList.add('no-show');  
$('#'+element).html('<br><h5 class="no-data">No hay '+type+'</h5><br>');
}else{
// sharesofApp.ocultar = true;
// elemento.parentNode.classList.remove('no-show');
if(title==""){ title = type; }
AmCharts.makeChart(element, {
	"type": "serial",
	"theme": "light",
	"legend":{
		'horizontalGap': 10,
		'maxColumns': 1,
		'position': 'right', 
		'useGraphSettings': true,
		'markerSize':10
	},
	"dataProvider": chartData,
	"valueAxes":[{
        'stackType': 'regular',
        'axisAlpha': 0.3,
        'gridAlpha': 0
	}],
	"graphs":[{
       "balloonText":"<b>[[name]]</b><span style='font-size:14px'>[[interval]]:<b>[[value]]</b></span> " ,
       "fillAlphas":0.8,
       "labelText":"[[value]]",
       "lineAlpha":0.3,
       "title": "[[name]] <br> [[interval]]",
       'type': 'column',
       'color': '#000',
       'valueField': '[[value]]'
	}],
	"titles": [{
		"text": element
	}],
	"categoryField": "name",
    "categoryAxis": {
        "gridPosition": "start",
        "axisAlpha": 0,
        "gridAlpha": 0,
        "position": "left"
    },
	"export": {
		"enabled": true
	}	
});

}
},
},
created:function(){
	parentApp.route = "comms/weekly";
	parentApp.consultRoute = "comms/weekly";
	var vm = this;
	parentApp.consultMethod = function(json){
		console.log('NEW JSON ' + json);
		vm.reloadChart();
	}
},
});