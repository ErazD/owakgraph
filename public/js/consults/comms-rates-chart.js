/**
* Esta función calcula el porcentaje perteneciente a un elemento comparable de una gráfica de barras.
* @param sectArray {diccionario} con los los valores actuales del objeto a comparar.
* @param maxArray {diccionario} con los los valores máximos a comparar.
* @param key {string} índice para buscar los valores en los diccionarios.
* @return {número} 0 a 100 si el valor es positivo, de -0 a -10 si el valor es negativo. (si el valor es menor a -10, pasa a ser -10 por organización de la gráfica)
*/
function getRelativesPercent(sectArray, maxArray, key){
	var k = key.includes('_n')? key.split('_')[0]:key;
	var relativePercent = 100 * sectArray[key] / ((maxArray[k]!=0)?maxArray[k]:1);	
	relativePercent = Math.max(relativePercent, -10);
	return relativePercent;
}

Vue.component('consult-metrics', {
	template: '#consult-metrics',
	delimiters: ['<%', '%>'],
	data: function(){
		return {		
			mostrarDatosCrudos: false,
			updateFunctions:[],
			globalPositions:[],
			globalPositionsKey: false,
		}
	},
	methods: {	
		/**
		* Función principal para organizar los datos obtenidos del servidor.
		* Agrupa y suma la información de las fanpages dependiendo del nivel de dado.
		* @param {string} to: nivel hasta el cual se agruparán los datos
		* Postcondition: parentApp.commsData ha sido actualizado.
		*/
		generateMetricsReport: function(to='Fanpage'){
			var vm = this;
			var actualData = parentApp.getArraysOfData(to,parentApp.allData);
			console.log('actual data: ');
			console.log(actualData);
			var arrayName = ((to.endsWith("s"))?to :to+"s").toLowerCase() ;
			var prevName = vm.getPreviusLevelName(to);
			var allFanpages = [];
			$.each(actualData, function(key, actData){
				var ob = {};
				ob[arrayName] = actData;
				var fanpage = parentApp.getArraysOfData(to=='Page'?'Page':'Fanpage',ob,prevName);
				var fpgMetricsRates ={ totals : {participation_rate:0, amplification_rate:0, reaction_rate:0, cant:0} };
				$.each(fanpage, function(key, valueFanpage){
					var name =valueFanpage['name'];
					var totals = valueFanpage['metrics'];
					var metricsRates ={
						totals : {participation_rate:0, amplification_rate:0, reaction_rate:0, cant:0},
						comunication_theme : {},
						comunication_task : {},
						product : {}
					};
					$.each(valueFanpage['posts'], function(key, post){
						vm.sumMetricsRates(post, metricsRates,['totals','comunication_task', 'comunication_theme', 'product'], 'fb_');
					});
					vm.promMetrics(metricsRates,['totals','comunication_task', 'comunication_theme', 'product']);
					console.log(fanpage, valueFanpage, metricsRates);
					valueFanpage['metrics']['rates'] = metricsRates;
					vm.sumMetricsRates(valueFanpage['metrics']['rates']['totals'], fpgMetricsRates, ['totals']);
				});
				vm.promMetrics(fpgMetricsRates,['totals']);
				var fp = {fanpages:fanpage, name:actData['name'], cant_posts: actData['metrics']['cant'], metrics:actData['metrics'], type:actData['type']};
				fp['metrics']['totals'] = fpgMetricsRates['totals'];
				allFanpages.push(fp);
			});
			console.log("--- FPG MetricsRates: ---");
			console.log(allFanpages);
			parentApp.commsData = allFanpages;
			return allFanpages;
		},
		/**
		* suma las métricas de un post a un resultdo acumulado.
		* Las tasas necesitan ser promediadas con el método promMetrics.
		* @param {dict} $post: diccionario con la información a sumar.
		* @param {dict} allRates: diccionario con la información sumada.
		* @param [array<string>] categories: arreglo con los índices a sumar.
		* @param {string} sn_pre: prefijo para la red social a la que pertenece
		*/
		sumMetricsRates: function(post, allRates, categories, sn_pre=""){
			$.each(categories, function(key, category){
				var rates = (category == 'totals')? allRates:allRates[category];
				var rate = (category == 'totals')? 'totals':post[category];
				var pr = post[sn_pre + 'participation_rate'];
				var ar = post[sn_pre + 'amplification_rate'];
				var vr = post[sn_pre + 'reaction_rate'];
				vr = (vr!='N/A')?vr:0;
				if(pr > 0 || ar > 0 || vr > 0){
					if(rates[rate] == undefined){
						rates[rate] = {participation_rate:pr, amplification_rate:ar, reaction_rate:vr, cant:1};
					}else{
						rates[rate]['participation_rate'] += pr;
						rates[rate]['amplification_rate'] += ar;
						rates[rate]['reaction_rate'] += vr;
						rates[rate]['cant'] +=  1;
					}
				}
			});
		},
		/**
		* Promedia las metricas dadas en el diccionario dividiendolo por su cantidad.
		* @param {dict} metricsRates: diccionario con las métricas a promediar.
		* @param {array<string>} categories: arreglo con los índices a promediar.
		*/
		promMetrics: function(metricsRates, categories){
			$.each(categories, function(key, dir){
				if(dir == 'totals'){
					cant = Math.max(1,metricsRates['totals']['cant']);
					metricsRates['totals']['participation_rate'] /= cant;
					metricsRates['totals']['amplification_rate'] /= cant;
					metricsRates['totals']['reaction_rate'] /= cant;
				}else{
					var metricsR = metricsRates[dir];
					$.each(metricsR, function(k, rates){
						cant = Math.max(1,rates['cant']);
						rates['participation_rate'] /= cant;
						rates['amplification_rate'] /= cant;
						rates['reaction_rate'] /= cant;
					});
				}
			});
		},
		/**
		* Retorna el nivel superior al nivel dado.
		* @param {string} to: nombre del nivel del qe se quiere conocer su nivel superior.
		* @return {string} nombre el nivel superior encontrado.
		*/
		getPreviusLevelName:function(to){
			var resp = 'Canasta';
			switch(to){
				case "Vertical":
					resp = 'all';
					break;
				case "Categoria":				
					resp = 'Vertical';
					break;
				case "Canasta":
					resp = 'Categoria';				
					break;
				case "Brand":
					resp = 'Canasta';
					break;
				case "Fanpage":
				case "Channel":
				case "InstaProfile":
					resp = 'Brand';
					break;
				case "Page":
					resp = 'Page_List';
					break;

				case "YoutubeChannel":
				resp = 'Brand';
				break;

				case "InstaProfile":
                resp = 'Brand';
				break;
			}
			return resp;
		},
		/**
		* Retorna los axes para las gráficas
		* @return {array}: lista con las configuraciones para los axes
		*/
		getAxes:function(){
			var axes =	[
			{
				"id": "v1",
				"axisThickness": 2,
				"axisAlpha": 1,
				"position": "bottom",
				"precision": 2,
				"autoGridCount": false,
				"gridCount": 10,
				"inside": true,
				"title": "Porcentajes",
				"color": "#00000000",
			}
			];
			return axes;
		}, 
		/**
		* Retorna los elementos a mostrar en la gráfica
		* @return {array}: lista con las configuraciones para la gráfica.
		*/
		getGraphs:function(type = 'rates'){
			var graphs = []

			if(type == 'rates'){
				graphs = [ {
					// "valueAxis": "v1",
					"balloonText": "Tasa de amplificación: [[amplification_rate]] %",
					"labelText": "[[amplification_rate]] %",
					"fillAlphas": 0.8,
					"lineAlpha": 0.2,
					"type": "column",
					"title": "Tasa de amplificación",
					"valueField": "relative_percent_amplification_rate",
					"lineColor": parentApp.getColorData('amplification_rate')
				},{
					// "valueAxis": "v2",
					"balloonText": "Tasa de participación: [[participation_rate]] %",
					"labelText": "[[participation_rate]] %",
					"fillAlphas": 0.8,
					"lineAlpha": 0.2,
					"type": "column",
					"title": "Tasa de participación",
					"valueField": "relative_percent_participation_rate",
					"lineColor": parentApp.getColorData('participation_rate')
				},{
					// "valueAxis": "v3",
					"balloonText": "Tasa de reacción: [[reaction_rate]] %",
					"labelText": "[[reaction_rate]] %",
					"fillAlphas": 0.8,
					"lineAlpha": 0.2,
					"type": "column",
					"title": "Tasa de reacción",
					"valueField": "relative_percent_reaction_rate",
					"lineColor": parentApp.getColorData('reaction_rate')
				}];
			}else if(type == 'metrics'){
			if(parentApp.social_network == 'Facebook'){
				graphs = [{
					// "valueAxis": "v4",
					"balloonText": "Reacciones: [[reactions]]",
					"labelText": "[[reactions]]",
					"fillAlphas": 0.8,
					"lineAlpha": 0.2,
					"type": "column",
					"title": "Reacciones",
					"valueField": "relative_percent_reactions",
					"lineColor": parentApp.getColorData('reactions')
				}, {
					// "valueAxis": "v4",
					"balloonText": "Reacciones Eliminadas: [[reactions_n]]",
					"labelText": "[[reactions_n]]",
					"fillAlphas": 0.8,
					"lineAlpha": 0.2,
					"type": "column",
					"title": "Reacciones Eliminadas",
					"valueField": "relative_percent_reactions_n",
					"lineColor": parentApp.getColorData('reactions')
				}, {
					// "valueAxis": "v5",
					"balloonText": "Comentarios: [[comments]]",
					"labelText": "[[comments]]",
					"fillAlphas": 0.8,
					"lineAlpha": 0.2,
					"type": "column",
					"title": "Comentarios",
					"valueField": "relative_percent_comments",
					"lineColor": parentApp.getColorData('comments')
				}, {
					// "valueAxis": "v5",
					"balloonText": "Comentarios eliminados: [[comments_n]]",
					"labelText": "[[comments_n]]",
					"fillAlphas": 0.8,
					"lineAlpha": 0.2,
					"type": "column",
					"title": "Comentarios eliminados",
					"valueField": "relative_percent_comments_n",
					"lineColor": parentApp.getColorData('comments')
				}, {
					// "valueAxis": "v6",
					"balloonText": "Compartidos: [[shares]]",
					"labelText": "[[shares]]",
					"fillAlphas": 0.8,
					"lineAlpha": 0.2,
					"type": "column",
					"title": "Compartidos",
					"valueField": "relative_percent_shares",
					"lineColor": parentApp.getColorData('shares')
				}, {
					// "valueAxis": "v6",
					"balloonText": "Compartidos Eliminados: [[shares_n]]",
					"labelText": "[[shares_n]]",
					"fillAlphas": 0.8,
					"lineAlpha": 0.2,
					"type": "column",
					"title": "Compartidos Eliminados",
					"valueField": "relative_percent_shares_n",
					"lineColor": parentApp.getColorData('shares')
				}, {
					// "valueAxis": "v7",
					"balloonText": "Vistas: [[views]]",
					"labelText": "[[views]]",
					"fillAlphas": 0.8,
					"lineAlpha": 0.2,
					"type": "column",
					"title": "Vistas",
					"valueField": "relative_percent_views",
					"lineColor": parentApp.getColorData('views')
				}];
			  }
			  if(parentApp.social_network == 'YouTube' && parentApp.social_network == 'Instagram'){
                 graphs = [{
					// "valueAxis": "v4",
					"balloonText": "Views: [[views]]",
					"labelText": "[[views]]",
					"fillAlphas": 0.8,
					"lineAlpha": 0.2,
					"type": "column",
					"title": "Views",
					"valueField": "relative_percent_views",
					"lineColor": parentApp.getColorData('views')
				}, {
					// "valueAxis": "v5",
					"balloonText": "Likes: [[likes]]",
					"labelText": "[[likes]]",
					"fillAlphas": 0.8,
					"lineAlpha": 0.2,
					"type": "column",
					"title": "Likes",
					"valueField": "relative_percent_likes",
					"lineColor": parentApp.getColorData('likes')
				}, {
					// "valueAxis": "v5",
					"balloonText": "Likes eliminados: [[likes_n]]",
					"labelText": "[[likes_n]]",
					"fillAlphas": 0.8,
					"lineAlpha": 0.2,
					"type": "column",
					"title": "Likes eliminados",
					"valueField": "relative_percent_likes_n",
					"lineColor": parentApp.getColorData('likes')
				}, {
					// "valueAxis": "v6",
					"balloonText": "Comentarios: [[comments]]",
					"labelText": "[[comments]]",
					"fillAlphas": 0.8,
					"lineAlpha": 0.2,
					"type": "column",
					"title": "Comentarios",
					"valueField": "relative_percent_comments",
					"lineColor": parentApp.getColorData('comments')
				}, {
					// "valueAxis": "v6",
					"balloonText": "Comentarios Eliminados: [[comments_n]]",
					"labelText": "[[comments_n]]",
					"fillAlphas": 0.8,
					"lineAlpha": 0.2,
					"type": "column",
					"title": "Comentarios Eliminados",
					"valueField": "relative_percent_comments_n",
					"lineColor": parentApp.getColorData('comments')
				}, {
					// "valueAxis": "v6",
					"balloonText": "Dislikes: [[dislikes]]",
					"labelText": "[[dislikes]]",
					"fillAlphas": 0.8,
					"lineAlpha": 0.2,
					"type": "column",
					"title": "Dislikes",
					"valueField": "relative_percent_comments",
					"lineColor": parentApp.getColorData('dislikes')
				}, {
					// "valueAxis": "v6",
					"balloonText": "Dislikes Eliminados: [[dislikes_n]]",
					"labelText": "[[dislikes_n]]",
					"fillAlphas": 0.8,
					"lineAlpha": 0.2,
					"type": "column",
					"title": "Dislikes Eliminados",
					"valueField": "relative_percent_comments_n",
					"lineColor": parentApp.getColorData('dislikes')
				}];
			  }	
			}
			return graphs;
		},
		/**
		* Genera los daos para las gráficas.
		* @param {dict} commsData: información base para organizar
		* @params {string} section: puede tener uno de los siguentes valores: comunicatin_task, comunication_theme, product, totals.
		*/
		generateChartData: function(commsData=parentApp.commsData, section='totals'){
			var chartData = [];
			var maxes = {};
			
			$.each(commsData, function(key, value){
				if(section=='totals'){
					console.log('VALUE METRICS');
					console.log(value['metrics']);
					console.log('//////////////');
					var metrics = value['metrics'];
					var rates = metrics[section];
					console.log('METRICS SECTION');
			        console.log(metrics[section]);
			        console.log('//////////////');
					maxes['amplification_rate'] = Math.max(rates['amplification_rate'], ((maxes['amplification_rate'])?maxes['amplification_rate']:0));
					maxes['participation_rate'] = Math.max(rates['participation_rate'], ((maxes['participation_rate'])?maxes['participation_rate']:0));
					maxes['reaction_rate'] = Math.max(rates['reaction_rate'], ((maxes['reaction_rate'])?maxes['reaction_rate']:0));
					
					if(parentApp.social_network == 'Facebook' || parentApp.social_network == 'all'){
					maxes['fb_comments'] = Math.max(metrics['fb_comments'], ((maxes['fb_comments'])?maxes['fb_comments']:0));
					maxes['fb_reactions'] = Math.max(metrics['fb_reactions'], ((maxes['fb_reactions'])?maxes['fb_reactions']:0));
					maxes['fb_shares'] = Math.max(metrics['fb_shares'], ((maxes['fb_shares'])?maxes['fb_shares']:0));
					maxes['fb_views'] = Math.max(metrics['fb_views'], ((maxes['fb_views'])?maxes['fb_views']:0));
					}
					
					if(parentApp.social_network == 'YouTube' || parentApp.social_network == 'all'){
					maxes['yt_views'] = Math.max(metrics['yt_views'], ((maxes['yt_views'])?maxes['yt_views']:0));
					maxes['yt_likes'] = Math.max(metrics['yt_likes'], ((maxes['yt_likes'])?maxes['yt_likes']:0));
					maxes['yt_comments'] = Math.max(metrics['yt_comments'], ((maxes['yt_comments'])?maxes['yt_comments']:0));
					maxes['yt_dislikes'] = Math.max(metrics['yt_dislikes'], ((maxes['yt_dislikes'])?maxes['yt_dislikes']:0));
				    }

				    if(parentApp.social_network == 'Instagram' || parentApp.social_network == 'all'){
					maxes['insta_views'] = Math.max(metrics['insta_views'], ((maxes['insta_views'])?maxes['insta_views']:0));
					maxes['insta_likes'] = Math.max(metrics['insta_likes'], ((maxes['insta_likes'])?maxes['insta_likes']:0));
					maxes['insta_comments'] = Math.max(metrics['insta_comments'], ((maxes['insta_comments'])?maxes['insta_comments']:0));
				    }
				    console.log('Contador de maximos');
				    console.log(maxes['insta_views']);

				}else{}
			});
			$.each(commsData, function(key, value){
				var info = {name: value['name']};
				if(section=='totals'){
					var metrics = value['metrics'];
					var rates = metrics[section];
					info['amplification_rate'] = parseFloat(rates['amplification_rate']).toFixed(2);
					info['participation_rate'] = parseFloat(rates['participation_rate']).toFixed(2);
					info['reaction_rate'] = parseFloat(rates['reaction_rate']).toFixed(2);
					info['cant'] = rates['cant'];
					
					var cant = value['cant_posts'];
					info['cant_posts'] = cant;

                    if(parentApp.social_network == 'Facebook' || parentApp.social_network == 'all'){
                    info['comments'] = formatNumber(metrics['fb_comments']);
					info['reactions'] = formatNumber(metrics['fb_reactions']);
					info['shares'] = formatNumber(metrics['fb_shares']);
					info['views'] = formatNumber(metrics['fb_views']);
						
					info['prom_comments'] = (cant > 0)? formatNumber((metrics['fb_comments'] / cant).toFixed(0)): 0;
					info['prom_reactions'] = (cant > 0)? formatNumber((metrics['fb_reactions'] / cant).toFixed(0)): 0;
					info['prom_shares'] = (cant > 0)? formatNumber((metrics['fb_shares'] / cant).toFixed(0)): 0;
					info['prom_views'] = (cant > 0)? formatNumber((metrics['fb_views'] / cant).toFixed(0)): 0;

					info['comments_n'] = metrics['fb_comments_n'];
					info['reactions_n'] = metrics['fb_reactions_n'];
					info['shares_n'] = metrics['fb_shares_n'];
					info['views_n'] = metrics['fb_views_n'];
                    }
                    if(parentApp.social_network == 'YouTube' || parentApp.social_network == 'all'){
					info['views'] = formatNumber(metrics['yt_views']);
					info['likes'] = formatNumber(metrics['yt_likes']);
					info['comments'] = formatNumber(metrics['yt_comments']);
					info['dislikes'] = formatNumber(metrics['yt_dislikes']);

					info['prom_views'] = (cant > 0)? formatNumber((metrics['yt_views'] / cant).toFixed(0)): 0;
					info['prom_likes'] = (cant > 0)? formatNumber((metrics['yt_likes'] / cant).toFixed(0)): 0;
					info['prom_comments'] = (cant > 0)? formatNumber((metrics['yt_comments'] / cant).toFixed(0)): 0;
					info['prom_dislikes'] = (cant > 0)? formatNumber((metrics['yt_dislikes'] / cant).toFixed(0)): 0;
                    
                    info['views_n'] = metrics['yt_views_n'];
					info['likes_n'] = metrics['yt_likes_n'];
					info['comments_n'] = metrics['yt_comments_n'];
					info['dislikes_n'] = metrics['yt_dislikes_n'];
                    }

                    if(parentApp.social_network == 'Instagram' || parentApp.social_network == 'all'){
					info['views'] = formatNumber(metrics['insta_views']);
					info['likes'] = formatNumber(metrics['insta_likes']);
					info['comments'] = formatNumber(metrics['insta_comments']);

					info['prom_views'] = (cant > 0)? formatNumber((metrics['insta_views'] / cant).toFixed(0)): 0;
					info['prom_likes'] = (cant > 0)? formatNumber((metrics['insta_likes'] / cant).toFixed(0)): 0;
					info['prom_comments'] = (cant > 0)? formatNumber((metrics['insta_comments'] / cant).toFixed(0)): 0;
					
                    info['views_n'] = metrics['insta_views_n'];
					info['likes_n'] = metrics['insta_likes_n'];
					info['comments_n'] = metrics['insta_comments_n'];
					
                    }

                    info['relative_percent_amplification_rate'] = getRelativesPercent(rates, maxes, 'amplification_rate');
					info['relative_percent_participation_rate'] = getRelativesPercent(rates, maxes, 'participation_rate');
					info['relative_percent_reaction_rate'] = getRelativesPercent(rates, maxes, 'reaction_rate');

                    if(parentApp.social_network == 'Facebook' || parentApp.social_network == 'all'){
					
					info['relative_percent_comments'] = getRelativesPercent(metrics, maxes, 'fb_comments');
					info['relative_percent_reactions'] = getRelativesPercent(metrics, maxes, 'fb_reactions');
					info['relative_percent_shares'] = getRelativesPercent(metrics, maxes, 'fb_shares');
					info['relative_percent_views'] = getRelativesPercent(metrics, maxes, 'fb_views');
                    
                    info['relative_percent_comments_n'] = getRelativesPercent(metrics, maxes, 'fb_comments_n');
					info['relative_percent_reactions_n'] = getRelativesPercent(metrics, maxes, 'fb_reactions_n');
					info['relative_percent_shares_n'] = getRelativesPercent(metrics, maxes, 'fb_shares_n');
					info['relative_percent_views_n'] = getRelativesPercent(metrics, maxes, 'fb_views_n');
				    }

                    if(parentApp.social_network == 'YouTube' || parentApp.social_network == 'all'){
					
					info['relative_percent_views'] = getRelativesPercent(metrics, maxes, 'yt_views');
					info['relative_percent_likes'] = getRelativesPercent(metrics, maxes, 'yt_likes');
					info['relative_percent_comments'] = getRelativesPercent(metrics, maxes, 'yt_comments');
					info['relative_percent_dislikes'] = getRelativesPercent(metrics, maxes, 'yt_dislikes');

					info['relative_percent_views_n'] = getRelativesPercent(metrics, maxes, 'yt_views_n');
					info['relative_percent_likes_n'] = getRelativesPercent(metrics, maxes, 'yt_likes_n');
					info['relative_percent_comments_n'] = getRelativesPercent(metrics, maxes, 'yt_comments_n');
					info['relative_percent_dislikes_n'] = getRelativesPercent(metrics, maxes, 'yt_dislikes_n');
				    }

				     if(parentApp.social_network == 'Instagram' || parentApp.social_network == 'all'){
					
					info['relative_percent_views'] = getRelativesPercent(metrics, maxes, 'insta_views');
					info['relative_percent_likes'] = getRelativesPercent(metrics, maxes, 'insta_likes');
					info['relative_percent_comments'] = getRelativesPercent(metrics, maxes, 'insta_comments');

					info['relative_percent_views_n'] = getRelativesPercent(metrics, maxes, 'insta_views_n');
					info['relative_percent_likes_n'] = getRelativesPercent(metrics, maxes, 'insta_likes_n');
					info['relative_percent_comments_n'] = getRelativesPercent(metrics, maxes, 'insta_comments_n');
					
				    }

				}else{
					var metrics = value['metrics'];
					var rates = metrics['rates'][section];
				}
				chartData.push(info);				  		
			});
			console.log("DATAAAAAAA");
			console.log(chartData);
			return chartData;
		},
		/**
		* Crea la gráfica
		* @param {string} type: tipo de gráfica ('metrics', 'rates')
		* @param {string} element: id del elemtneto html donde se cerará la gráfica.
		* @param {dict} commsData: información base para obtener la gráfica.
		* @param {string} title: t;itulo de la gráfica.
		* @param {bool} generateData: indica si debe generar la data, o ya tiene el formato que necesita
		* Postcondition: se ha creado la gráfica en el html.
		*/
		createChart: function( type, element, commsData=parentApp.commsData, title="", generateData=true){
			$('#'+element).height(200+100*commsData.length);		
			if(title==""){ title = type; }
			AmCharts.makeChart(element, {
				"type": "serial",
				"categoryField": "name",
				"synchronizeGrid":true,
				"theme": "light",
				"rotate": true,
				"dataProvider":  (generateData)?this.generateChartData(commsData):commsData,
				"valueAxes": this.getAxes(),
				"gridAboveGraphs": true,
				"startDuration": 1,
				"graphs": this.getGraphs(type),
				"titles": [{
					"text": title
				}],
				"legend":{
					"position":"bottom",
					"valueAlign":"left",
					"autoMargins":true
				},
				"categoryAxis": {
					"gridPosition": "start",
					"gridAlpha": 0,
					"tickPosition": "start",
					"tickLength": 20
				},
				"export": {
					"enabled": true
				}
				
			});
		}, 
		/**
		* Ordena una columna de la tabla de posiciones por columna dada.
		* @param {string} key: key de la columna a organizar.
		* @param {string} name: nombre de la columan a mostrar.
		* postcondition: el orden de this.globalPositions ha sido actualizado.
		*/
		sortGlobalPositions: function(key=false, name = ""){
			if(key == false || this.globalPositionsKey.key == key){
				this.globalPositionsKey = false;
				this.selectSortColumn(false);
				return;
			}
			this.globalPositionsKey ={}
			this.globalPositionsKey.key = key;
			this.globalPositionsKey.name = name;
			this.selectSortColumn(this.globalPositionsKey.key);
			  // Re-order the data provider
			  this.globalPositions.sort( function( a, b ) {
			  	if ( a[key] > b[key] ) {
			  		return -1;
			  	} else if ( a[key] < b[key] ) {
			  		return 1;
			  	}
			  	return 0;
			  } );

			},
			/**
			* Agrega o quita la clase "selected" a los elementos html
			* @param {string} key: nombre de la columna a seleccionar
			* Postcondition: las clases de las columnas del html han cambiado
			*/
			selectSortColumn: function(key=false){
				$('.sort-selectable').removeClass('selected');
				if(key){
					$('.sort-selectable-' + key).addClass('selected');
				}
			}
		},
		created: function(){			
			parentApp.route = "comms/consult";
			parentApp.consultRoute = "comms/consult";
			var vm = this;
			parentApp.consultMethod = function(json){
				console.log(json);	
				console.log('TO: ' + parentApp.to);			
				vm.generateMetricsReport(parentApp.to);
				var dt = vm.generateChartData(parentApp.commsData);

				vm.globalPositions = dt;
				// console.log('LAKSJDLKASJFLK');
				// console.log(vm.globalPositions);
				vm.createChart('rates', "grafica-total-rates",dt,"Tasas Totales", false);
				vm.createChart('metrics', "grafica-total-metrics",dt,"Métricas Totales", false);
				var funct = function(){
					$('.tooltipped').tooltip({delay: 50});
				};
				parentApp.updateFunctions.push(funct);
				
			}
	},
	
});
