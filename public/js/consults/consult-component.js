/**
* Convierte un texto en un slug, convirtiendolo en minúsculas, reemplazando los espacios por guiones(-), y eliminando sus carácteres especiales.
* @param {String} text: texto a convertir.
* @return {String}: texto con el formato de slug.
*/
function convertToSlug(Text)
{
	return Text
	.toLowerCase()
	.replace(/ /g,'-')
	.replace(/[^\w-]+/i,'');
}

/**
* Convierte un número en string con separador de miles
* @param {number} num: número a convertir.
* @return {string}: texto con separadores de miles que representa el número.
*/
function formatNumber(num) {
	if(!num)
    return 0;		
	return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.");
}

// Vue.component('datepicker',{
// 	template: "<input />",
// 	props: ["date"],
// 	data: function(){
// 		return {
// 			internalDate: this.date
// 		}
// 	},
// 	mounted: function(e) {
// 		let vm = this;
// 		$(this.$el).pickadate({
// 			today: 'Hoy',
// 			clear: 'Limpiar',
// 			close: 'Ok',
// 			selectMonths: true, // Creates a dropdown to control month
// 			selectYears: 15, // Creates a dropdown of 15 years to control year,
// 			closeOnSelect: true, // Close upon selecting a date,
// 			monthsFull: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
// 			monthsShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
// 			weekdaysFull: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
// 			weekdaysShort: ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab'],
// 			weekdaysLetter: ['D', 'L', 'M', 'M', 'J', 'V', 'S'],
// 			format: 'yyyy-mm-dd',
// 			onSet: function(){
// 				// if(vm.$el.name == 'since'){
// 				// 	parentApp.since = vm.$el.value;
// 				// }else if(vm.$el.name == 'until'){
// 				// 	parentApp.until = vm.$el.value;
// 				// }
// 				vm.internalDate = vm.$el.value;
// 				vm.$emit('set-date', 'manual');
// 				// parentApp.setDate("manual");
// 			}
// 		});
// 	},
// 	methods:{
// 		updateValue: function(){
// 			//let vm = this;
// 			// if(vm.$el.name == 'since'){
// 			// 	vm.$el.value = parentApp.since;
// 			// }else if(vm.$el.name == 'until'){
// 			// 	vm.$el.value = parentApp.until;
// 			// }
// 			// alert(this.date);
// 			// this.$el.value = this.internalDate;
// 		}, 
// 	},
// 	watch:{
// 		internalDate(dat){
// 			this.$emit("input", dat);
// 		},
// 		date(dat){
// 			this.$el.value = dat;
// 		}
// 	},
// 	created:function(){
// 		if(datepickers != undefined)
// 			datepickers.push(this);
// 	}
// });






let parentApp = new Vue({
	el: '#comms-charts',
	delimiters: ['<%', '%>'],
	data: {
		route: "comms", // reuta base de la sección
		consultRoute: "comms", // reuta base de la consulta
		allData:{}, // datos recibidos del servidor
		commsData:{}, // datos luego del formato para las gráficas
		since:'', // filtro fecha dese
		until:'', // filtro fecha hasta
		interval:28, // filtro de fecha, intervalo de tiempo para elegir la fecha inicial tomando la fecha final de referencia.
		social_network: 'Facebook',
		social_graphs: 'Facebook',
		country:'all', // filtro principal, país
		activity:'published', //Filtro para las publicaciones activas
		allCountries:['Argentina', 'Bolivia', 'Chile', 'Colombia', 'Centro América', 'Ecuador', 'Latino América', 'México', 'Paraguay', 'Perú', 'Uruguay', 'Venezuela'], // opciones filtro principal país
		pedido_id:2, // filtro principal id elemento
		allElements:[], // opciones para los filtros donde se elige el elemento de la categoría seleccionada
		type:'Canasta', // filtro principal, categoría del elemento a buscar 
		actual_type:'Canasta', // Categoría del elemento del que se realizó la última búsqueda
		to:"Brand", // filtro principal, categoría con el que se muestran los resultados, debe ser menor o igual que la categoría de tipo.
		mostrarTo:true,
		mostrarResultados: false,
		timeSplit:'1 week',
		counterKey:0,
		updateFunctions:[], // lista de funciones a ejecutar luego de que vue haga modificaciones
		shareUrl:'', // URL para compartir la consulta
		extraURLParamters:{}, // valores extra para la url
		extra:{}, // atributos extra para la instancia de vue
	},
	methods: {
		consultMethod: function(json){}, // metodo para ejecutar luego de recibir los datos del servidor
		/**
		* METODO FILTRO FECHAS
		* Calcula la fecha inicail dependiendo del intervalo y la fecha final.
		* @param interval: String que contiene un rango definidio como día actual ("hoy"), el mismo día ("day"), y todo ("all") o un número que representa en cantidad de días la diferencia entre la fecha inicial y la final.
		* Postcondition: this.since ha sido modificado. 
		* Postcondition: this.until ha sido modificado a la fecha actual si interval == "hoy", o se ha seleccionado un intervalo y thisuntil == "".
		*/
		setDate: function(interval = this.interval){
			this.interval = interval;
			var d = (this.until != "")? new Date(this.until) : new Date();
			switch (interval){
				case "hoy":
				d = new Date();
				this.since = d.toISOString().split("T")[0];
				this.until = d.toISOString().split("T")[0];
				console.log("hoy");
				break;
				case "day":
				this.since = d.toISOString().split("T")[0];
				this.until = d.toISOString().split("T")[0];
				console.log("day");
				break;
				case "all":
				this.since = "";
				this.until = "";
				console.log("all");
				break;
				case "manual":
				// No hacer nada
				break;
				default:	
				var firstDate = new Date(d);
				firstDate.setDate(d.getDate() - parseInt(this.interval));
				this.until = d.toISOString().split("T")[0];
				this.since = firstDate.toISOString().split("T")[0];
				console.log(interval);
				break;
			}
			// $.each(datepickers, function(key, value){
			// 	value.updateValue();
			// });
		},
		/**
		* Crea la parte de la url que contiene parámetros adicionales.
		* @return {string}: Sección de la url con los parámetros adicionales a la sección principal.
		*/
		getExtraUrlParameters: function(){
			var extraUrl = "";
			if(Object.keys(this.extraURLParamters).length >0){
				var conector = "";
				$.each(this.extraURLParamters, function(key, value){
					extraUrl += conector+value;
					conector = "&";
				});
			}			
			return extraUrl;
		},
        
      
		/**
		* Crea la url para compartir la consulta actual.
		* @param {bool} createUrl: indica si se quiere crear la url o se quiere mostrar la url de compartir.
		* @param  {string} route: ruta base de la consulta
		* Postcondition: si createUrl = true, this.shareUrl se actuliza
		* @return {string}: url para compartir la consulta.
		*/
		getConsultURL:function(createUrl=false, route=this.route){
		    
			var shareUrl = this.shareUrl;
	
			if(createUrl || this.shareUrl == ""){
				shareUrl = getUrl(route) + "?type=" + this.type + "&pedido_id=" + this.pedido_id + "&country=" + this.country +"&social_network="+ this.social_network + "&to=" + this.to;
				if(this.since != "" && this.until != "" ){
					shareUrl +="&since=" + this.since + "&until=" + this.until;
				}
				shareUrl += (shareUrl.includes("?"))? "&": "?";	
				shareUrl += this.getExtraUrlParameters();
				this.shareUrl == shareUrl;
			}
			if(!createUrl){ 
				var shURL = $('#share-url');
				shURL.val(shareUrl);
				shURL.removeClass('hide');
				shURL.select();
				document.execCommand('copy');
				Materialize.toast('Se ha copiado la URL', 2000);
			}
			return shareUrl;
		},

		/**
		* MÉTODO SECCION PRINCIPAL, OBTENER DATOS
		* Pide los datos de la sección principal del servidor, y llama el método para oraganizar los datos.
		* @param {string} route: ruta para pedir datos al servidor.
		* Postcondition: this.allData = datos obtenidos
		* Postcondition: this.actual_type = commsData.type
		*/
		getConsult: function(route=this.consultRoute){
			var theSection = $('.url-box');
			theSection.removeClass('hide');	
			this.mostrarResultados = true;
			fecha = "";
			if(this.since != "" && this.until != "" ){
				fecha = "/" + this.since + "/" + this.until;
			}

		    if(this.type == 'Fanpage'){
                this.social_network = 'Facebook';
                this.to = 'Fanpage';
		    }
		    if(this.social_network == 'all'){
                this.to = 'Brand';
		    }
            if(this.type == 'YoutubeChannel'){
            	this.social_network = 'YouTube';
            	this.to = 'YoutubeChannel';
            }
            if(this.type == 'InstaProfile'){
            	this.social_network = 'Instagram';
            	this.to = 'InstaProfile';
            }

            this.social_graphs = this.social_network;
	   //        intervalo = "";
				// if(route == 'comms/weekly'){
	   //             console.log('estoy en comms weekly');
	   //             intervalo = "/" + this.timeSplit;
				// }
			var extraUrl = "?"+this.getExtraUrlParameters();
			console.log(getUrl(route, this.social_network, this.type, this.pedido_id ,this.country) + '/' + this.activity + fecha + extraUrl);
			fetch(getUrl(route, this.social_network, this.type, this.pedido_id ,this.country) + '/' + this.activity + fecha + extraUrl)
			.then(r => r.json()).then(json => {
				this.allData = json;
				this.actual_type = this.type;
				// console.log('QUE ONDA PASA AQUI');
				// console.log(this.type);
				// console.log(this.actual_type);

				this.consultMethod(json);
			});	
		// Actualiza la url de la consulta.
		this.getConsultURL(true);
		},

		/**
		* Recorre un arreglo de elementos para crear un arreglo de elementos que se encuentre en un nivel menor al nivel inicial.
		* @param {string} to: nombre del nivel del que se quiere obtener los elementos.
		* @param {array} allData: arreglo con los elementos del nivel inicial.
		* @param {string} from: nivel inicial para la búsqueda de elementos.
		* @return {array}: arreglo con los elementos del nivel final.
		*/
		getArraysOfData: function(to='Fanpage', allData=this.allData, from=this.actual_type){			
			/*
			LEVELS -> next
			vertical -> category -> basket -> brand -> (fanpage channel all)
			*/
			 
			actual = from; // elemento desde donde se van a comenzar la consulta, (to es el elemento del que se agrupan los datos a mostrar)
			finish = false; // indica si ha llegado al final de los niveles
			actualData = [allData]; // lista con los datos agrupados por el modelo elegido en "to";
			var safeBreak = 0; // para evitar un bucle infinito se detiene el proceso cuando trata recorrer más de 100 niveles 
			while(!finish && actual != to && safeBreak<100){ // recorre los niveles parra llegar desde "from" a "to"
				newData = []; // arreglo con los elementos del siguiente nivel a recorrer
				finish = true; // comienza en true, para comparar la finalización de la búsqueda
				next_actual = ""; // nombre del siguiente nivel a recorrer
				$.each(actualData, function(key, value){ // recorre todos los elementos de la categoría actual
					infoLevel = parentApp.getNextLevel(actual, to, value);					
					infoData = infoLevel['next']; // lista de elementos del siguiente nivel.
					// console.log([infoLevel, value, infoData]);
					newData = newData.concat(infoData);
					console.log(newData);
					next_actual = infoLevel['level'];
					finish &= infoLevel['finish']; // compara si debe finalizar la búsqueda.
				});	
				console.log("------- NEW DATA --------");
				console.log(newData);
				actualData = newData; // luego de recorrer el nivel actual, asigna los elementos del siguiente nivel
				actual = next_actual; // asigna el nombre del siguiente nivel
				safeBreak++;
			}
			if(safeBreak >= 100){
				alert("Hubo un problema al recorrer la información desde " + from + " hasta " + to);
			}
			//console.log("FINNNNNN");
			// console.log(actualData);
			return actualData;
		},

		/**
		* obtiene la infromacion del siguiente nivel para los datos buscados.
		* @param {string} from: nivel inicial.
		* @param {string} to: nivel final.
		* @param {dict} data: diccionario con los elementos del siguiente nivel a acceder.
		* @return {dict}: {many:bool, next:array, level:string, finish:bool}
		*		many: indica si son muchos elemnteos los que se agregan.
		*		next: contiene los elementos del próximo nivel.
		*		level: nombre del próximo nivel.
		*		finish: indica si ya llegó al nivel buscado o al último nivel posible.
		*/
		getNextLevel:function(from, to="all", data){
			console.log(from+" -> "+to);
			console.log(data);

			var keys={'Fanpage':'fanpages', 'YoutubeChannel':'channels', 'InstaProfile':'profiles'}; // corrección temporal para acceder a las fanpages
			resp = [];
			resp['many'] = false;
			sig = from;
			switch(from){
				case "Vertical":
				case "vertical":
					sig = 'Categoria';
					resp['next']=data['categories'];
					break;
				case "Categoria":
				case "category":
					sig = 'Canasta';
					resp['next']=data['baskets'];
					break;
				case "Canasta":
				case "basket":
					sig = 'Brand';
					resp['next']=data['brands'];
					break;
				case "Brand":
				case "brand":
					if(to== "all"){
						resp['next']=data['fanpages'].concat(data['channels']).concat(data['profiles']);
					}else{
						resp['next']=data[keys[to]];
					}
					resp['many'] = true;
					sig = to;
					break;

				case "Page_List":
				case "page_list" :
					sig = 'Page';
					// sig = 'Fanpage';
					resp['next']=data['pages'];
					break;
				case "Fanpage":
				case "YoutubeChannel":
				case "InstaProfile":
				case "AdSet":
				case "fanpage":
				case "channel":
				case "profile":
				case "adset":
				case "Page":
					resp['finish'] =true;			
					break;
			}
			resp['finish'] = sig == to;
			resp['level'] = sig;
			return resp;
		},
		/**
		* Busca el color asignado a un elemento y lo retorna.
		* @param {string} key: identificador del color (nombre en utf-8).
		* @param {string} type: nombre de la tabla si el elemento pertenece a uno, o undefined en caso contrario.
		* @return {string}: valor para el color del elemento si fué encontrado, o iundefined en caso contrario.
		*/
		getColorData:function (key, type=undefined){
			var color = undefined;
			if(type){
				var col = allColors[type];
				if(col != undefined)
					col = col[key];
				if(col != undefined)
					color = col;
			}

			if(color == undefined){

				switch(key){
					case "photo":
					case "photopost":
					color='#5D9BD3';
					break;
					case "involvement":
					color='#e37e7e';
					break;
					case "event":
					case "evento":
					color='#ED7D31';
					break;
					case "link":
					case "linkad":
					color='#A5A5A5';
					break;
					case "carrousel":
					color='#FDC010';
					break;
					case "gif":
					color='#4A72B8';
					break;
					case "video":
					color='#71AD46';
					break;
					case "360°":
					color='#255F92';
					break;
					case "note":
					case "nota":
					color='#9E4A23';
					break;
					case "status":
					case "estado":
					color='#636464';
					break;
					case "repost":
					color='#ba0062';
					break;
					case "re_post":
					color='#ba0062';
					break;
					case "live_video":
					color='#5d35ea';
					break;

					case "album":
					color='#446931';
					break;
					case "canvas":
					break;
					case "emocional":
					color='#ed7d31';
					break;
					case "racional":
					case "funcional":
					color = '#5b9bd5';
					break;
					case "awareness":
					color = '#5a99d4';
					break;
					case "desire":
					color = '#ec7a30';
					break;
					case "seek_more_knowledge":
					color = '#1e5481';
					break;
					case "knowledge":
					color = '#a3a3a3';
					break;
					case "transaction":
					color = '#ffc024';
					break;
					case "personality":
					color = '#70ab44';
					break;
					case "recommendation":
					color = "#4373c2";
					break;
					case "fechas_especiales":
					color = '#255e91';
					break;
					case "tips":
					color = '#a4a4a4';
					break;
					case "institucional":
					color = '#ed7d2e';
					break;
					case "producto":
					color = '#9e480c';
					break;
					case "responsabilidad_social":
					color = '#70ad45';
					break;
					case "promociones":
					color = '#ffc000';
					break;
					case "motivacional":
					break;
					case "brand_love":
					color = '#5b9bd5';
					break;
					case "actividades":
					break;
					case "programa_lealtad":
					color = '#4373c4';
					break;
					case "concurso":
					color = '#997100';
					break;
					case "No definido":
					color = '#636363';
					break;

					case 'Publicación Sin Productos':
					color='#ED2722'
					break;
					case "Todos":
					color='#f74c00';
					break;
					case 'Pañales':
					color='#f9c800';
					break;
					case 'Baño líquido':
					color = '#f98a00';
					break;
					case 'Toallitas Húmedas':
					color = '#31cc00';
					break;
					case 'Kit baby shower / Recién nacido':
					color = '#1b7bc6';
					break;
					case 'Shampoo':
					color = '#a6d900';
					break;
					case 'Sin Productos':
					color='#ED2722'

					case 'Humectación':
					color = '#FD9927';
					break;

					case 'Anti-acné':
					color = '#21D12C';
					break;

					case 'Anti-edad':
					color = '#1555CF';
					break;

					case 'Limpieza':
					color = '#FC6521';
					break;

					case "Todos":
					color='#F54D1D';
					break;

					case "Hidratación":
					color='#B1DC2F';
					break;

					case "Protección Solar":
					color='#1D8ECC';
					break;

					case "Aclarante":
					color='#2A31CD';
					break;

					case "Multibeneficio Arrugas Protección":
					case "Arrugas Protección":
					color='#8936CC';
					break;


					case "Baby Dove":
					color='#8ddbf1';
					break;
					case "JOHNSON'S":
					color='#1265b6';
					break;
					case "Pequeñin":
					color='#fab100';
					break;
					case "Huggies":
					color='#e11619';
					break;
					case "Winny":
					color='#49afe5';
					break;
					case "Babysec":
					color='#000000';
					break;

					case "Lubriderm":
					color='#554899';
					break;
					case "Nivea":
					color='#000c5e';
					break;
					case "L'OCCITANE":
					color='#eea500';
					break;
					case "Cetaphil":
					color='#76ad00';
					break;
					case "Natura":
					color='#583f3c';
					break;
					case "Esika":
					color='#da0029';
					break;
					case "Dove":
					color='#e4c57f';
					break;
					case "Avon":
					color='#000000';
					break;
					case "Ponds":
					color='#b1b1b1';
					break;
					case "Yanbal":
					color='#f75306';
					break;
					case "L'Oréal":
					color='#7d6d50';
					break;
					case "GOICOECHEA":
					color='#501d67';
					break;
					case "Johnsons Adulto":
					color='#06274b';
					break;

					case "amplification_rate":
					color = '#a7ffeb';
					break;
					case "participation_rate":
					color = '#b388ff';
					break;
					case "reaction_rate":
					color = '#8c9eff';
					break;	
					case "reactions":
					color = '#FF6600';
					break;
					case "comments":
					color = '#FCD202';
					break;
					case "shares":
					color = '#B0DE09';
					break;	
					case "views":
					color = '#67b7dc';
					break;
				}
			}
			return color;
		},
	},
	created: function(){

	// $.each(datepickers, function(key, value){
	// 	value.updateValue();
	// });
	},
	updated:function(){
		// Ejecuta las funciones que estén en la lista
		for (var i = this.updateFunctions.length - 1; i >= 0; i--) {
			var funct =	this.updateFunctions.shift();
			funct();
		}
	},
});
