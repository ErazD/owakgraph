

var url = window.location.protocol+"//"+window.location.host;
// var url = "http://owak.co/graph/public/index.php";
var datepickers =[];


/**
* DEPRECATED
* Función para crear las gráficas específicas por fanpage
*/
var createAllSpecificCharts = function(){
	$.each(commsApp.comms_rates_types, function(kcrt, crt){
		$.each(commsApp.commsData, function(kbrnd, brnd){
			$.each(brnd.fanpages, function(kfpg, fpg){
				var dt = commsApp.generateSectionChartData(fpg,crt.slug);
				commsApp.createChart('rates', "section-"+crt.slug+"-"+fpg.facebook_id+"-rates", dt, fpg.name+": Tasas "+crt.name, false);
			});
		});
	});
}

function convertToSlug(Text)
{
	return Text
	.toLowerCase()
	.replace(/ /g,'-')
	.replace(/[^\w-]+/i,'');
}

function getChartData(type){
	// console.log(commsApp);
	// console.log(type);
	return commsApp.generateChartData(type);
}

/**
* Esta función calcula el porcentaje perteneciente a un elemento comparable de una gráfica de barras.
* @param sectArray {diccionario} con los los valores actuales del objeto a comparar.
* @param maxArray {diccionario} con los los valores máximos a comparar.
* @param key {string} índice para buscar los valores en los diccionarios.
* @return {número} 0 a 100 si el valor es positivo, de -0 a -10 si el valor es negativo. (si el valor es menor a -10, pasa a ser -10 por organización de la gráfica)
*/
function getRelativesPercent(sectArray, maxArray, key){
	var k = key.includes('_n')? key.split('_')[0]:key;
	var relativePercent = 100 * sectArray[key] / ((maxArray[k]!=0)?maxArray[k]:1);	
	relativePercent = Math.max(relativePercent, -10);
	return relativePercent;
}


Vue.component('datepicker',{
	template: "<input />",
	mounted: function(e) {
		let vm = this;
		$(this.$el).pickadate({
			today: 'Hoy',
			clear: 'Limpiar',
			close: 'Ok',
	    selectMonths: true, // Creates a dropdown to control month
	    selectYears: 15, // Creates a dropdown of 15 years to control year,
	    closeOnSelect: true, // Close upon selecting a date,
	    monthsFull: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
	    monthsShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
	    weekdaysFull: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
	    weekdaysShort: ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab'],
	    weekdaysLetter: ['D', 'L', 'M', 'M', 'J', 'V', 'S'],
	    format: 'yyyy-mm-dd',
	    onSet: function(){
	    	if(vm.$el.name == 'since'){
	    		commsApp.since = vm.$el.value;
	    	}else if(vm.$el.name == 'until'){
	    		commsApp.until = vm.$el.value;
	    	}
	    }
	});
	}, methods:{
		updateValue: function(){
			let vm = this;
			if(vm.$el.name == 'since'){
				vm.$el.value = commsApp.since;
			}else if(vm.$el.name == 'until'){
				vm.$el.value = commsApp.until;
			}
		}
	}, created:function(){
		datepickers.push(this);
	}
});

let commsApp = new Vue({
	el: '#comms-charts',
	delimiters: ['<%', '%>'],
	data: {
		chart:"",
		legend:"",
		selected:"",
		commsData:[],
		allDataMetrics:{},
		since:'',
		until:'',
		country:'all',
		allCountries:['Argentina', 'Bolivia', 'Chile', 'Colombia', 'Centro América', 'Ecuador', 'Latino América', 'México', 'Paraguay', 'Perú', 'Uruguay', 'Venezuela'],
		pedido_id:2,
		allElements:[],
		type:'basket',
		actual_type:'basket',
		comms_rates_types:[
		{name:"Tarea de comunicación", slug:"comunication_task",},
		{name:"Tema de comunicación", slug:"comunication_theme",},
		{name:"Producto", slug:"product",}],
		comms_sections:[],
		to:"brand",
		interval:28,
		mostrarDatosCrudos: false,
		globalPositions:[],
		globalPositionsKey: false,
		updateFunctions:[],
		shareUrl:'',
		extraURLParamters:{},
		extra:{},
	},
	methods: {
		setDate: function(interval = this.interval){
			this.interval = interval;
			var d = new Date(this.until);
			switch (interval){
				case "hoy":
				d = new Date();
				this.since = d.toISOString().split("T")[0];
				this.until = d.toISOString().split("T")[0];
				console.log("hoy");
				break;
				case "day":
				this.since = d.toISOString().split("T")[0];
				this.until = d.toISOString().split("T")[0];
				console.log("day");
				break;
				case "all":
				this.since = "";
				this.until = "";
				console.log("all");
				break;
				default:
				var firstDate = new Date(d);
				firstDate.setDate(d.getDate() - parseInt(this.interval));
				this.until = d.toISOString().split("T")[0];
				this.since = firstDate.toISOString().split("T")[0];
				console.log(interval);
				break;
			}
			$.each(datepickers, function(key, value){
				value.updateValue();
			});
		},

		getExtraUrlParameters: function(){
			var extraUrl = "";
			if(Object.keys(this.extraURLParamters).length >0){
				var conector = "";
				$.each(this.extraURLParamters, function(key, value){
					extraUrl += conector+value;
					conector = "&";
				});
			}			
			return extraUrl;
		},

		getConsultURL:function(){
			var shareUrl =url+"/comms/consult?type=" + this.type + "&pedido_id=" + this.pedido_id + "&country=" + this.country + "&to=" + this.to;
			if(this.since != "" && this.until != "" ){
				shareUrl +="&since=" + this.since + "&until=" + this.until;
			}

			shareUrl += (shareUrl.includes("?"))? "&": "?";	
			shareUrl += this.getExtraUrlParameters();

			var shURL = $('#share-url');
			shURL.val(shareUrl);
			shURL.removeClass('hide');
			shURL.select();
			document.execCommand('copy');
			Materialize.toast('Se ha copiado la URL', 2000);
		},
		getConsult: function(){
			var theSection = $('.url-box');
			theSection.removeClass('hide');	
			fecha = "";
			if(this.since != "" && this.until != "" ){
				fecha = "/" + this.since + "/" + this.until;
			}
			var extraUrl = "?"+this.getExtraUrlParameters();

			fetch(url+"/comms/consult/" + this.type + "/" + this.pedido_id + "/" + this.country + fecha + extraUrl)
			.then(r => r.json()).then(json => {
				//this.commsData = json.comms;
				this.allDataMetrics = json;
				this.actual_type = this.type;
				console.log(json);				
				commsApp.generateMetricsReport(commsApp.to);
				var dt = this.generateChartData(type, this.commsData);
				commsApp.globalPositions = dt;
				console.log("DTTTTTT");
				console.log(dt);
				this.createChart('rates', "grafica-total-rates",dt,"Tasas Totales", false);
				this.createChart('metrics', "grafica-total-metrics",dt,"Métricas Totales", false);
				commsApp.updateFunctions.push(createAllSpecificCharts);	// DEPRECATED
				var funct = function(){
					$('.tooltipped').tooltip({delay: 50});
				};
				commsApp.updateFunctions.push(funct);
			});	

		},	
		/**
		* Función principal para organizar los datos obtenidos del servidor.
		*/
		generateMetricsReport: function(to='fanpages'){
			var actualData = this.getArraysOfData(to,this.allDataMetrics);
			console.log('actual data: ');
			console.log(actualData);
			var arrayName = (to.endsWith("s"))?to :to+"s" ;
			var prevName = this.getPreviusLevelName(to);
			var allFanpages = [];
			$.each(actualData, function(key, actData){
				var ob = {};
				ob[arrayName] = actData;
				var fanpage;
				if(to == 'adset')
					fanpage = ob ; //commsApp.getArraysOfData('adsets',ob, 'adset');
				else
					fanpage = commsApp.getArraysOfData('fanpages',ob,prevName);
				
				var fpgMetricsRates ={ totals : {participation_rate:0, amplification_rate:0, reaction_rate:0, cant:0} };
				$.each(fanpage, function(key, valueFanpage){
					var name =valueFanpage['name'];
					var totals = valueFanpage['metrics'];
					var metricsRates ={
						totals : {participation_rate:0, amplification_rate:0, reaction_rate:0, cant:0},
						comunication_theme : {},
						comunication_task : {},
						product : {}
					};
					$.each(valueFanpage['posts'], function(key, post){
						commsApp.sumMetricsRates(post, metricsRates,['totals','comunication_task', 'comunication_theme', 'product']);
					});
					commsApp.promMetrics(metricsRates,['totals','comunication_task', 'comunication_theme', 'product']);
					valueFanpage['metrics']['rates'] = metricsRates;
					commsApp.sumMetricsRates(valueFanpage['metrics']['rates']['totals'], fpgMetricsRates,['totals']);
				});
				commsApp.promMetrics(fpgMetricsRates,['totals']);
				var fp = {fanpages:fanpage, name:actData['name'], cant_posts: actData['metrics']['cant'],  metrics:actData['metrics'], type:actData['type']};
				fp['metrics']['totals'] = fpgMetricsRates['totals'];
				allFanpages.push(fp);
			});
			console.log("------------------------------ FPG MetricsRates: ----------------------------");
			console.log(allFanpages);
			commsApp.commsData = allFanpages;
			return allFanpages;
		},
		sumMetricsRates: function(post, allRates, categories){
			$.each(categories, function(key, category){
				var rates = (category == 'totals')? allRates:allRates[category];
				var rate = (category == 'totals')? 'totals':post[category];
				var pr = post['participation_rate'];
				var ar = post['amplification_rate'];
				var vr = post['reaction_rate'];
				vr = (vr!='N/A')?vr:0;
				if(pr > 0 || ar > 0 || vr > 0){
				if(rates[rate] == undefined){
						rates[rate] = {participation_rate:pr, amplification_rate:ar, reaction_rate:vr, cant:1};
				}else{
						rates[rate]['participation_rate'] += pr;
						rates[rate]['amplification_rate'] += ar;
						rates[rate]['reaction_rate'] += vr;
						rates[rate]['cant'] +=  1;
					}
				}
			});
		},
		promMetrics: function(metricsRates, categories){
			$.each(categories, function(key, dir){
				if(dir == 'totals'){
					cant = (metricsRates['totals']['cant'] == 0)? 1 : metricsRates['totals']['cant'];
					metricsRates['totals']['participation_rate'] /= cant;
					metricsRates['totals']['amplification_rate'] /= cant;
					metricsRates['totals']['reaction_rate'] /= cant;
				}else{
					var metricsR = metricsRates[dir];
					$.each(metricsR, function(k, rates){
						cant = (rates['cant'] == 0)? 1 :rates['cant'];
						rates['participation_rate'] /= cant;
						rates['amplification_rate'] /= cant;
						rates['reaction_rate'] /= cant;
					});
				}
			});
		},
		getArraysOfData: function(to='fanpages', allData=this.allDataMetrics, from=this.actual_type){			
			/** 
			LEVELS -> next
			vertical -> category -> basket -> brand -> (fanpage channel all)
			*/
			actual = from;
			data = [];
			queue = [];
			finish = false;
			actualData = [allData];
			while(!finish && actual != to){
				newData = [];
				end = true;
				// console.log(actual);
				next_actual = "";
				$.each(actualData, function(key, value){
					infoLevel = commsApp.getNextLevel(actual, to, value);					
					infoData = infoLevel['next'];
					// console.log([infoLevel, value, infoData]);
					newData = newData.concat(infoData);
					next_actual = infoLevel['level'];
					end &= infoLevel['finish'];
				});	
				// console.log("------- NEW DATA --------");
				// console.log(newData);
				finish = end;
				actualData = newData;	
				actual = next_actual;	
			}
			return actualData;
		},
		getPreviusLevelName:function(to){
			var resp = 'basket';
			switch(to){
				case "vertical":
				resp = 'all';
				break;
				case "category":				
				resp = 'vertical';
				break;
				case "basket":
				resp = 'category';				
				break;
				case "brand":
				resp = 'basket';
				break;
				case "fanpage":
				case "channel":	
				resp = 'brand';
				break;
			}
			return resp;
		},
		getNextLevel:function(from, to="all", data){
			console.log(from+" -> "+to);
			console.log(data);
			resp = [];
			resp['many'] = false;
			resp['finish'] = false;
			sig = from;
			switch(from){
				case "vertical":
				sig = 'category';
				resp['next']=data['categories'];
				break;
				case "category":
				sig = 'basket';
				resp['next']=data['baskets'];
				break;
				case "basket":
				sig = 'brand';
				resp['next']=data['brands'];
				break;
				case "brand":
				if(to == "all"){
					resp['next']=data['fanpages'].concat(data['channels']);
				}else{
					resp['next']=data[to];
				}
				resp['many'] = true;
				sig = to;
				break;
				case "fanpage":
				case "channel":	
				case "adset":
				resp['finish'] =true;			
				break;
			}
			resp['finish'] |= sig == to;
			resp['level'] = sig;
			return resp;
		},	
		getColorData:function (key){
			//console.log(key);
			var color = undefined;
			switch(key){
				case "photo":
				case "photopost":
				color='#5D9BD3';
				break;
				case "event":
				case "evento":
				color='#ED7D31';
				break;
				case "link":
				case "linkad":
				color='#A5A5A5';
				break;
				case "carrousel":
				color='#FDC010';
				break;
				case "gif":
				color='#4A72B8';
				break;
				case "video":
				color='#71AD46';
				break;
				case "360°":
				color='#255F92';
				break;
				case "note":
				case "nota":
				color='#9E4A23';
				break;
				case "status":
				case "estado":
				color='#636464';
				break;
				case "repost":
				color='#98752B';
				break;
				case "live_video":
				color='#264579';
				break;
				case "album":
				color='#446931';
				break;
				case "canvas":
				break;
				case "emocional":
				color='#ed7d31';
				break;
				case "racional":
				case "funcional":
				color = '#5b9bd5';
				break;
				case "awareness":
				color = '#5a99d4';
				break;
				case "desire":
				color = '#ec7a30';
				break;
				case "seek_more_knowledge":
				color = '#1e5481';
				break;
				case "knowledge":
				color = '#a3a3a3';
				break;
				case "transaction":
				color = '#ffc024';
				break;
				case "personality":
				color = '#70ab44';
				break;
				case "recommendation":
				color = "#4373c2";
				break;
				case "fechas_especiales":
				color = '#255e91';
				break;
				case "tips":
				color = '#a4a4a4';
				break;
				case "institucional":
				color = '#ed7d2e';
				break;
				case "producto":
				color = '#9e480c';
				break;
				case "responsabilidad_social":
				color = '#70ad45';
				break;
				case "promociones":
				color = '#ffc000';
				break;
				case "motivacional":
				break;
				case "brand_love":
				color = '#5b9bd5';
				break;
				case "actividades":
				break;
				case "programa_lealtad":
				color = '#4373c4';
				break;
				case "concurso":
				color = '#997100';
				break;
				case "No definido":
				color = '#636363';
				break;
				case "amplification_rate":
				color = '#a7ffeb';
				break;
				case "participation_rate":
				color = '#b388ff';
				break;
				case "reaction_rate":
				color = '#8c9eff';
				break;	

				case "reactions":
				color = '#FF6600';
				break;
				case "comments":
				color = '#FCD202';
				break;
				case "shares":
				color = '#B0DE09';
				break;	
				case "views":
				color = '#67b7dc';
				break;			
			}
			return color;
		},
		getAxes:function(){
			var axes =	[
			{
				"id":"v1",
				"axisThickness": 2,
				"axisAlpha": 1,
				"position": "bottom",
				"precision": 2,
				"autoGridCount": false,
				"gridCount": 10,
				"inside": true,
				"title": "Porcentajes",
				"color": "#00000000",
			}
			// {
			// 	"id":"v1",
			// 	"axisColor": commsApp.getColorData('amplification_rate'),
			// 	"axisThickness": 2,
			// 	"axisAlpha": 1,
			// 	"position": "bottom",
			// 	"precision": 2,
			// }, {
			// 	"id":"v2",
			// 	"axisColor": commsApp.getColorData('participation_rate'),
			// 	"axisThickness": 2,
			// 	"axisAlpha": 1,
			// 	"position": "bottom",
			// 	"precision": 2,
			// 	"offset": 30,
				
			// }, {
			// 	"id":"v3",
			// 	"axisColor": commsApp.getColorData('reaction_rate'),
			// 	"axisThickness": 2,
			// 	"gridAlpha": 0,
			// 	"axisAlpha": 1,
			// 	"position": "bottom",
			// 	"precision": 2,
			// 	"offset": 60,
			// }, {
			// 	"id":"v4",
			// 	"axisColor": commsApp.getColorData('reactions'),
			// 	"axisThickness": 2,
			// 	"axisAlpha": 1,
			// 	"position": "bottom",
			// 	"precision": "0",
				

			// }, {
			// 	"id":"v5",
			// 	"axisColor": commsApp.getColorData('comments'),
			// 	"axisThickness": 2,
			// 	"axisAlpha": 1,
			// 	"position": "bottom",
			// 	"precision": "0",
			// 	"offset": 30,
			// }, {
			// 	"id":"v6",
			// 	"axisColor": commsApp.getColorData('shares'),
			// 	"axisThickness": 2,
			// 	"gridAlpha": 0,
			// 	"axisAlpha": 1,
			// 	"position": "bottom",
			// 	"precision": "0",
			// 	"offset": 60,
				
			// }, {
			// 	"id":"v7",
			// 	"axisColor": commsApp.getColorData('views'),
			// 	"axisThickness": 2,
			// 	"gridAlpha": 0,
			// 	"axisAlpha": 1,
			// 	"position": "bottom",
			// 	"precision": "0",
			// 	"offset": 90,
			// }
			];
			return axes;
		}, 
		getGraphs:function(type = 'rates'){
			var graphs = []

			if(type == 'rates'){
				graphs = [ {
					// "valueAxis": "v1",
					"balloonText": "Tasa de amplificación: [[amplification_rate]] %",
					"labelText": "[[amplification_rate]] %",
					"fillAlphas": 0.8,
					"lineAlpha": 0.2,
					"type": "column",
					"title": "Tasa de amplificación",
					"valueField": "relative_percent_amplification_rate",
					"lineColor": commsApp.getColorData('amplification_rate')
				},{
					// "valueAxis": "v2",
					"balloonText": "Tasa de participación: [[participation_rate]] %",
					"labelText": "[[participation_rate]] %",
					"fillAlphas": 0.8,
					"lineAlpha": 0.2,
					"type": "column",
					"title": "Tasa de participación",
					"valueField": "relative_percent_participation_rate",
					"lineColor": commsApp.getColorData('participation_rate')
				},{
					// "valueAxis": "v3",
					"balloonText": "Tasa de reacción: [[reaction_rate]] %",
					"labelText": "[[reaction_rate]] %",
					"fillAlphas": 0.8,
					"lineAlpha": 0.2,
					"type": "column",
					"title": "Tasa de reacción",
					"valueField": "relative_percent_reaction_rate",
					"lineColor": commsApp.getColorData('reaction_rate')
				}];
			}else if(type == 'metrics'){
				graphs = [ {
					// "valueAxis": "v4",
					"balloonText": "Reacciones: [[reactions]]",
					"labelText": "[[reactions]]",
					"fillAlphas": 0.8,
					"lineAlpha": 0.2,
					"type": "column",
					"title": "Reacciones",
					"valueField": "relative_percent_reactions",
					"lineColor": commsApp.getColorData('reactions')
				}, {
					// "valueAxis": "v4",
					"balloonText": "Reacciones Eliminadas: [[reactions_n]]",
					"labelText": "[[reactions_n]]",
					"fillAlphas": 0.8,
					"lineAlpha": 0.2,
					"type": "column",
					"title": "Reacciones Eliminadas",
					"valueField": "relative_percent_reactions_n",
					"lineColor": commsApp.getColorData('reactions')
				}, {
					// "valueAxis": "v5",
					"balloonText": "Comentarios: [[comments]]",
					"labelText": "[[comments]]",
					"fillAlphas": 0.8,
					"lineAlpha": 0.2,
					"type": "column",
					"title": "Comentarios",
					"valueField": "relative_percent_comments",
					"lineColor": commsApp.getColorData('comments')
				}, {
					// "valueAxis": "v5",
					"balloonText": "Comentarios eliminados: [[comments_n]]",
					"labelText": "[[comments_n]]",
					"fillAlphas": 0.8,
					"lineAlpha": 0.2,
					"type": "column",
					"title": "Comentarios eliminados",
					"valueField": "relative_percent_comments_n",
					"lineColor": commsApp.getColorData('comments')
				}, {
					// "valueAxis": "v6",
					"balloonText": "Compartidos: [[shares]]",
					"labelText": "[[shares]]",
					"fillAlphas": 0.8,
					"lineAlpha": 0.2,
					"type": "column",
					"title": "Compartidos",
					"valueField": "relative_percent_shares",
					"lineColor": commsApp.getColorData('shares')
				}, {
					// "valueAxis": "v6",
					"balloonText": "Compartidos Eliminados: [[shares_n]]",
					"labelText": "[[shares_n]]",
					"fillAlphas": 0.8,
					"lineAlpha": 0.2,
					"type": "column",
					"title": "Compartidos Eliminados",
					"valueField": "relative_percent_shares_n",
					"lineColor": commsApp.getColorData('shares')
				}, {
					// "valueAxis": "v7",
					"balloonText": "Vistas: [[views]]",
					"labelText": "[[views]]",
					"fillAlphas": 0.8,
					"lineAlpha": 0.2,
					"type": "column",
					"title": "Vistas",
					"valueField": "relative_percent_views",
					"lineColor": commsApp.getColorData('views')
				}];	
			}
			return graphs;
		},
		/**
		* Para cuando no se usan totales, solo es un post y se recorren las subcategorías
		*
		*/
		generateSectionChartData: function(commsData, section){
			var chartData = [];
			var rates = commsData['metrics']['rates'][section];
			var maxes = {};

			$.each(rates,function(key, sect){
			 		maxes['amplification_rate'] = Math.max(sect['amplification_rate'], ((maxes['amplification_rate'])?maxes['amplification_rate']:0));
					maxes['participation_rate'] = Math.max(sect['participation_rate'], ((maxes['participation_rate'])?maxes['participation_rate']:0));
					maxes['reaction_rate'] = Math.max(sect['reaction_rate'], ((maxes['reaction_rate'])?maxes['reaction_rate']:0));
			});
			$.each(rates,function(key, sect){
				var info = {name: key};
				info['amplification_rate'] = parseFloat(sect['amplification_rate']).toFixed(2);
				info['participation_rate'] = parseFloat(sect['participation_rate']).toFixed(2);
				info['reaction_rate'] = parseFloat(sect['reaction_rate']).toFixed(2);
				info['cant'] = sect['cant'];	
				
				info['relative_percent_amplification_rate'] = getRelativesPercent(sect, maxes, 'amplification_rate');
				info['relative_percent_participation_rate'] = getRelativesPercent(sect, maxes, 'participation_rate');
				info['relative_percent_reaction_rate'] = getRelativesPercent(sect, maxes, 'reaction_rate');

				chartData.push(info);				  		
			});
			return chartData;			
		},


		/**
		*
		* section = [comunicatin_task, comunication_theme, product, totals];
		*/
		generateChartData: function(type, commsData=this.commsData, section='totals'){
			var chartData = [];
			var maxes = {};
			$.each(commsData, function(key, value){
				if(section=='totals'){
					var metrics = value['metrics'];
			 			var rates = metrics[section];
				 		maxes['amplification_rate'] = Math.max(rates['amplification_rate'], ((maxes['amplification_rate'])?maxes['amplification_rate']:0));
						maxes['participation_rate'] = Math.max(rates['participation_rate'], ((maxes['participation_rate'])?maxes['participation_rate']:0));
						maxes['reaction_rate'] = Math.max(rates['reaction_rate'], ((maxes['reaction_rate'])?maxes['reaction_rate']:0));
						maxes['comments'] = Math.max(metrics['comments'], ((maxes['comments'])?maxes['comments']:0));
						maxes['reactions'] = Math.max(metrics['reactions'], ((maxes['reactions'])?maxes['reactions']:0));
						maxes['shares'] = Math.max(metrics['shares'], ((maxes['shares'])?maxes['shares']:0));
						maxes['views'] = Math.max(metrics['views'], ((maxes['views'])?maxes['views']:0));
				}else{}
			});
			console.log('MAXESSSS');
			console.log(maxes);
			$.each(commsData, function(key, value){
				var info = {name: value['name']};
				if(section=='totals'){
					var metrics = value['metrics'];
					var rates = metrics[section];
					info['amplification_rate'] = parseFloat(rates['amplification_rate']).toFixed(2);
					info['participation_rate'] = parseFloat(rates['participation_rate']).toFixed(2);
					info['reaction_rate'] = parseFloat(rates['reaction_rate']).toFixed(2);
					info['cant'] = rates['cant'];
					
					var cant = value['cant_posts'];
					info['cant_posts'] = cant;
					info['comments'] = commsApp.formatNumber(metrics['comments']);
					info['reactions'] = commsApp.formatNumber(metrics['reactions']);
					info['shares'] = commsApp.formatNumber(metrics['shares']);
					info['views'] = commsApp.formatNumber(metrics['views']);

					info['prom_comments'] = (cant > 0)? commsApp.formatNumber((metrics['comments'] / cant).toFixed(0)): 0;
					info['prom_reactions'] = (cant > 0)? commsApp.formatNumber((metrics['reactions'] / cant).toFixed(0)): 0;
					info['prom_shares'] = (cant > 0)? commsApp.formatNumber((metrics['shares'] / cant).toFixed(0)): 0;
					info['prom_views'] = (cant > 0)? commsApp.formatNumber((metrics['views'] / cant).toFixed(0)): 0;

					info['comments_n'] = commsApp.formatNumber(metrics['comments_n']);
					info['reactions_n'] = commsApp.formatNumber(metrics['reactions_n']);
					info['shares_n'] = commsApp.formatNumber(metrics['shares_n']);
					info['views_n'] = commsApp.formatNumber(metrics['views_n']);

						info['relative_percent_amplification_rate'] = getRelativesPercent(rates, maxes, 'amplification_rate');
						info['relative_percent_participation_rate'] = getRelativesPercent(rates, maxes, 'participation_rate');
						info['relative_percent_reaction_rate'] = getRelativesPercent(rates, maxes, 'reaction_rate');
						
						info['relative_percent_comments'] = getRelativesPercent(metrics, maxes, 'comments');
						info['relative_percent_reactions'] = getRelativesPercent(metrics, maxes, 'reactions');
						info['relative_percent_shares'] = getRelativesPercent(metrics, maxes, 'shares');
						info['relative_percent_views'] = getRelativesPercent(metrics, maxes, 'views');

						info['relative_percent_comments_n'] = getRelativesPercent(metrics, maxes, 'comments_n');
						info['relative_percent_reactions_n'] = getRelativesPercent(metrics, maxes, 'reactions_n');
						info['relative_percent_shares_n'] = getRelativesPercent(metrics, maxes, 'shares_n');
						info['relative_percent_views_n'] = getRelativesPercent(metrics, maxes, 'views_n');
					}else{
						var metrics = value['metrics'];
						var rates = metrics['rates'][section];
					}
					chartData.push(info);				  		
				});
			console.log("DATAAAAAAA");
			console.log(chartData);
			return chartData;
		},
		formatNumber:function(num) {
			return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.");
		},
		createChart: function( type, element, commsData=this.commsData, title="", generateData=true){	
			$('#'+element).height(200+100*commsData.length);
			if(title==""){ title = type; }
			AmCharts.makeChart(element, {
				"type": "serial",
				"categoryField": "name",
				"synchronizeGrid":true,
				"theme": "light",
				"rotate": true,
				"dataProvider":  (generateData)?this.generateChartData(type, commsData):commsData,
				"valueAxes": this.getAxes(),
				"gridAboveGraphs": true,
				"startDuration": 1,
				"graphs": this.getGraphs(type),
				"titles": [{
					"text": title
				}],
				"legend":{
					"position":"bottom",
					"valueAlign":"left",
					"autoMargins":true
				},

				"listeners": [{
					"event": "clickSlice",
					"method": function(event) {
							// var chart = event.chart;
							// if (event.dataItem.dataContext.id != undefined) {
							// 	selected = event.dataItem.dataContext.id;
							// } else {
							// 	selected = undefined;
							// }						
						// chart.dataProvider = this.generateChartData(type);
						// chart.validateData();
					},
				}], 
				"categoryAxis": {
					"gridPosition": "start",
					"gridAlpha": 0,
					"tickPosition": "start",
					"tickLength": 20,
					"autoWrap": true,
					"inside": true,
					"boldLabels": true
				},
				"export": {
					"enabled": true
				}
				
			});
		}, 
		sortGlobalPositions: function(key=false, name = ""){
			if(key == false || commsApp.globalPositionsKey.key == key){
				commsApp.globalPositionsKey = false;
				commsApp.selectSortColumn(false);
					return;
				}
			commsApp.globalPositionsKey ={}
			commsApp.globalPositionsKey.key = key;
			commsApp.globalPositionsKey.name = name;
			commsApp.selectSortColumn(commsApp.globalPositionsKey.key);
			  // Re-order the data provider
  				commsApp.globalPositions.sort( function( a, b ) {
				    if ( a[key] > b[key] ) {
      					return -1;
    				} else if ( a[key] < b[key] ) {
      					return 1;
    				}
    					return 0;
  				} );

		},
			selectSortColumn: function(key=false){
				$('.sort-selectable').removeClass('selected');
				if(key){
					$('.sort-selectable-' + key).addClass('selected');
				}
			}

	},
	created: function(){
		this.type = (type)? type :this.type;
		this.pedido_id = (pedido_id)? pedido_id :this.pedido_id;
		//console.log("SE HA CREADO CON: \n"+this.type+"  "+ this.pedido_id);
		if( typeof formComms != "undefined"){
			this.setDate("hoy");
			this.setDate(28);
		}
		if( typeof allElements != "undefined"){
			this.allElements = allElements;
		}

		var funct = function(){
			$.each(datepickers, function(key, value){
				value.updateValue();
			});
		};

		this.updateFunctions.push(funct);
		//this.getCommsChart();
	},
	computed: {
		
	}, 
	updated:function(){
		//console.log(commsApp.updateFunctions);
		for (var i = commsApp.updateFunctions.length - 1; i >= 0; i--) {
			var funct =	commsApp.updateFunctions.shift();
			funct();
		}
	},
});
