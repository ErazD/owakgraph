 Vue.component('consult-heatmap', {
 	template: '#consult-heatmap',
 	delimiters: ['<%', '%>'],
 	data: function(){
 		return {
 			DAYS:["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
 			SOCIAL_NETWORK_PRE:{'Facebook':'fb_', 'Youtube':'yt_', 'Instagram':'insta_'},
 			METRICS_BY_PRE:{'fb_':['reactions', 'comments', 'shares', 'views'], 'yt_':['views', 'likes', 'dislikes', 'comments'], 'insta_':['likes', 'comments', 'views']},
 			emptyCharts:''
 		}
 	},
 	methods: {
 		/**
 		* Recorre los tipos de gráficas posibles para crearlas.
 		*/
 		loopData: function(data, social_network){
 			var vm = this;
 			if(social_network == 'Facebook' || social_network == 'all'){
 			$.each(vm.METRICS_BY_PRE[vm.SOCIAL_NETWORK_PRE['Facebook']], function(index, value){
	             vm.loadData(data, vm.SOCIAL_NETWORK_PRE['Facebook'] + value);
	         });
 		    }
 		    if(social_network == 'YouTube' || social_network == 'all'){
 		    $.each(vm.METRICS_BY_PRE[vm.SOCIAL_NETWORK_PRE['Youtube']], function(index, value){
	             vm.loadData(data, vm.SOCIAL_NETWORK_PRE['Youtube'] + value);
	         });	
 		    }
 		    if(social_network == 'Instagram' || social_network == 'all'){
 		    $.each(vm.METRICS_BY_PRE[vm.SOCIAL_NETWORK_PRE['Instagram']], function(index, value){
	             vm.loadData(data, vm.SOCIAL_NETWORK_PRE['Instagram'] + value);
	         });	
 		    }
 		},
 		/**
 		* Método principal para crea la gráfica y definir sus propiedades.
 		* @param {dict} data: información base para la creación de la gráfica.
 		* @param {string} key: identificador del tipo de gráfica a crear. la id del elemento en html donde se creará la gráfica es: 'container-'+key
 		* Postcondition: la gráfica se ha creado en el html.
 		*/
 		loadData:function(data, key){
			// console.log('DATAAAAA:' + data);
			// console.log('key de graficas:' + key);
			parentApp.commsData = data.info;
			for(var i = 0 ; i < parentApp.commsData.length ; i++){
				if(parentApp.commsData[i][key] < 0){
					parentApp.commsData[i][key] = 0;
				}
			}
			var dataSet =  anychart.data.set(parentApp.commsData);
			var heatMapData = dataSet.mapAs({
				'x':'hour',
				'y':'day',
				'heat': key
			});
			
			var chart = anychart.heatMap(heatMapData);
			var colorRange = [];
			var range = {};
			var maxRange = 0;
			
			switch(key){
				//Facebook//
				case 'fb_reactions':
				range = this.createRanges('fb_reactions');
				maxRange = Math.max(...range);
				chart.title('Facebook Actividad Reacciones');
				colorRange.push(0, maxRange/8, maxRange/4, maxRange/2, maxRange);
				break;

				case 'fb_comments':
				range = this.createRanges('fb_comments');
				maxRange = Math.max(...range);
				chart.title('Facebook Actividad Comentarios');
				colorRange.push(0, maxRange/8 , maxRange/4, maxRange/2, maxRange);
				break;
				
				case 'fb_shares':
				range = this.createRanges('fb_shares');
				maxRange = Math.max(...range);
				chart.title('Facebook Actividad Compartidos');
				colorRange.push(0, maxRange/8, maxRange/4, maxRange/2, maxRange);
				break; 

				case 'fb_views':
				range = this.createRanges('fb_views');
				maxRange = Math.max(...range);
				chart.title('Facebook Actividad Views');
				colorRange.push(0, maxRange/8, maxRange/4, maxRange/2, maxRange);
				break;

				////Youtube////
				case 'yt_views':
				range = this.createRanges('yt_views');
				maxRange = Math.max(...range);
				chart.title('Youtube Actividad Views');
				colorRange.push(0, maxRange/8, maxRange/4, maxRange/2, maxRange);
				break;

				case 'yt_likes':
				range = this.createRanges('yt_likes');
				maxRange = Math.max(...range);
				chart.title('Youtube Actividad Likes');
				colorRange.push(0, maxRange/8 , maxRange/4, maxRange/2, maxRange);
				break;
				
				case 'yt_comments':
				range = this.createRanges('yt_comments');
				maxRange = Math.max(...range);
				chart.title('Youtube Actividad Comentarios');
				colorRange.push(0, maxRange/8, maxRange/4, maxRange/2, maxRange);
				break; 

				case 'yt_dislikes':
				range = this.createRanges('yt_dislikes');
				maxRange = Math.max(...range);
				chart.title('Youtube Actividad Dislikes');
				colorRange.push(0, maxRange/8, maxRange/4, maxRange/2, maxRange);
				break;

				/////Instagram//
				case 'insta_likes':
				console.log('estoy en likes');
				range = this.createRanges('insta_likes');
				maxRange = Math.max(...range);
				chart.title('Instagram Actividad likes');
				colorRange.push(0, maxRange/8, maxRange/4, maxRange/2, maxRange);
				break;
				case 'insta_comments':
				console.log('estoy en comments');
				range = this.createRanges('insta_comments');
				maxRange = Math.max(...range);
				chart.title('Instagram Actividad comentarios');
				colorRange.push(0, maxRange/8, maxRange/4, maxRange/2, maxRange);
				break;
				case 'insta_views':
				console.log('estoy en views');
                range = this.createRanges('insta_views');
				maxRange = Math.max(...range);
				chart.title('Instagram Actividad vistas');
				colorRange.push(0, maxRange/8, maxRange/4, maxRange/2, maxRange);
				break;
			} 
			chart.labelsDisplayMode('drop');
			var colorScale = chart.colorScale();
			// for(var i = 0; this.colorRange.length; i++){
			// 	console.log('Rangos Establecidos: ', this.colorRange[i]);
			// }
			colorScale.ranges([
			{
				from:colorRange[0],
				to:colorRange[1],
				name:'Muy Baja',
				color:'#E6FAFF'
			},
			{
				from:colorRange[1],
				to:colorRange[2],
				name:'Baja',
				color: '#93D7F9'
			},
			{
				from: colorRange[2],
				to: colorRange[3],
				name: 'Normal',
				color: '#69bce8'
			},
			{
				from: colorRange[3],
				to: colorRange[4],
				name: 'Alta',
				color: '#3085be'
			}
			]);
			chart.colorScale(colorScale);
			chart.labels().enabled(true).maxFontSize(12)
			chart.stroke('#fff');

			chart.background().fill('transparent');
			
			chart.hovered().stroke('2 #fff').fill('#545f69').labels({'fontColor': '#fff'});
			chart.legend()
			.enabled(true)
			.align('center')
			.position('center-top')
			.itemsLayout('horizontal');

			// Sets Axes TextFormatter
			chart.yAxis().stroke(null);
			chart.yAxis().labels()
			.padding([0, 15, 0, 0])
			.format(function() {
				//console.log(this['tickValue']);
				return this['tickValue'];
			});
			chart.xAxis().stroke(null);
			chart.xAxis().labels().format(function() {
				return this['tickValue'];
			});

			// Turns off ticks for axes
			chart.yAxis().ticks(true);
			chart.xAxis().ticks(true);

			// Turns tooltips off
			chart.tooltip(true);

			// set container id for the chart
			console.log('container-'+key);
			chart.container('container-'+key);

			// initiate chart drawing
			chart.draw();

		},
		/**
		* Crea un arreglo con los valores posibles para el tipo de gráfica indicada.
		* @param: {string} key: identificador del tipo de gráfica del que se quiere obtener los posibles valores.
		* @return {array} lista con los posibles valores.
		*/
		createRanges: function(key){
			var dataRange = [];
			for(var i = 0 ; i< parentApp.allData.info.length; i++){
				dataRange.push(parentApp.allData.info[i][key]);
			}
			return dataRange;
		},
		/**
		* Elimina los gráficos del html.
		* Postcondition: las gráficasse han eliminado del html.
		*/
		cleanChart: function(){
			console.log('llegue hasta aqui');
			this.emptyCharts = document.querySelectorAll('.intensity-chart');
			for(var i = 0 ; i<this.emptyCharts.length;i++){
				console.log(this.emptyCharts.length);
				this.emptyCharts[i].innerHTML="";
			}
		},
	},
	created: function(){
		parentApp.route = "comms/heatmap";
		parentApp.consultRoute = "comms/heatmap";
		var vm = this;
		parentApp.consultMethod = function(json){
			if(parentApp.allData.info)
				vm.cleanChart();	
			console.log('NEW JSON ' + parentApp.social_network);
			console.log(json);
			vm.loopData(json, parentApp.social_network);
		}
		parentApp.mostrarTo = false;
	},
});