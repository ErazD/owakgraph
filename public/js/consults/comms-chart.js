Vue.component('consult-comms', {	
	template: "#consult-comms",
	delimiters: ['<%', '%>'],
	data: function(){
		return{
			clasify:'',
			showChart:'',
			comms_types:[ // elementos especificos de la sección para organizar los resultados de la consulta por categorías.
			{name:"Tipo", slug:"comms-type",},
			{name:"Balance", slug:"comms-balance",},
			{name:"Tarea de comunicación", slug:"comms-task",},
			{name:"Producto", slug:"comms-product",},
			{name:"Tema de comunicación", slug:"comms-theme",},
			{name:"Concurso", slug:"comms-contest"},
			],
			comms_sections:[], // elementos específicos de la sección para organizar los resultados de la consulta por elementos buscados (fanpages, marcas, ...).

		}
	},
	methods:{
		/**
		* TODO: AGREGAR NUEVAS GRÁFICAS DE COMMS PANORAMA (micro moment, bussiness objetive)
		* Llama la creación de las gráficas para un elemento.
		* @param {string} id: id usado para su creación en el html.
		* @param {array} data: información para crear la gráfica.
		*/
		createSpecificChart(id, data){
			for (var i = 0; i < 7; i++) {
				this.setupChart(i, id, data);
			}
		},
		/**
		* Crea el arreglo con la información que reciben las gráficas
		* @param {string} type: tipo de gráfica
		* @param {array} commsData: contiene la información a organizar
		* @param {string} selected: nombre del segmento de productos seleccionado para mostrar sus productos.
		* @return {array}: arreglo con el formato pedido por la gráfica.
		*/
		generateChartData: function(type, commsData=parentApp.commsData, selected = undefined){
			var chartData = [];
			// console.log(type, commsData);
			// console.log(this.commsData[type]);
			var total = commsData[type]['count'];
			if(total != 0){
				if(type == "product"){
					commsD = {};
					$.each(commsData[type], function(key, value){
						if(key != "count" ){ 
							var k = (key != "No definido")?key.split(" - "): ["No definido", "no definido"];
							var cmmd = commsD[k[0]];
							if(cmmd != undefined){ // si existen más valores los suma
								var per = (parseInt(value) / parseInt(total) * 100).toFixed(2);
								var p = cmmd['products'];
								p.push({
									type: k[1],
									value: formatNumber(value),
									percent: per+"%",
									color: parentApp.getColorData(k[1], 'products'),
									pulled: true,
								});
								var v2 = cmmd['value'] + parseInt(value);
								per = (parseInt(v2) / parseInt(total) * 100).toFixed(2);
								cmmd['value'] = v2;
								cmmd['percent'] = per;
							}else{ // si no existen los valores los crea
								var per = (parseInt(value) / parseInt(total) * 100).toFixed(2);
								var p = [];
								p.push({
									type: k[1],
									value: formatNumber(value),
									percent: per+"%",
									color: parentApp.getColorData(k[1], 'products'),
									pulled: true,
								});
								commsD[k[0]] = {
									type: k[0],
									value: parseInt(value),
									percent: per+"%",
									color: parentApp.getColorData(k[0], 'segmentos'),
									products: p,
								};
							}			 					
						}
					});
					// console.log(commsD);
					$.each(commsD, function(key, cData){
						if(key != "count"){ 
							if (key == selected) {
								$.each(cData.products, function(k, dtp){
									chartData.push(dtp);
								});
							} else {
								cData.pulled = false;
								chartData.push(cData);
							}		
						}	  
					});
				}else{
					$.each(commsData[type], function(key, value){
						if(key != "count"){
							var per = (parseInt(value) / parseInt(total) * 100).toFixed(2);
							chartData.push({
								type: key,
								value: value,
								percent: per+"%",
								color: parentApp.getColorData(key),
							});
						}	  
					});	
				}
				console.log(chartData);
				return chartData;
			}else{
				console.log(chartData);
				return false;
			}
		},
		/**
		* Define el gráfico a crear según el índice dado
		* @param {number} type: índice del tipo a crear.
		* @param {string} id: id usado para su creación en el html.
		* @param {array} data: información para crear la gráfica.
		*/
		setupChart(type, id="", commsData=parentApp.commsData){
			name = "";
			if(id!=""){
				name = id;
				id = convertToSlug(id) + "-";
			}else{
			}
			switch(type){
				case 0:				
				this.createChart("type", id+"comms-type", commsData, "Tipos de post: "+name);
				break;
				case 1:				
				this.createChart("balance", id+"comms-balance", commsData, "Balance: "+name);
				break;
				case 2:				
				this.createChart("comunication_task", id+"comms-task", commsData, "Tareas de comunicación: "+name);
				break;
				case 3:				
				this.createChart("product", id+"comms-product", commsData, "Productos: "+name);
				break;
				case 5:				
				this.createChart("comunication_theme", id+"comms-theme", commsData, "Temas de comunicación: "+name);
				break;
				case 6:				
				this.createChart("contest", id+"comms-contest", commsData, "Concursos: "+name);
				break;
			}
		},

		/**
		* Crea una nueva gráfica
		* @param {string} type: tipo de gáfica a crear.
		* @param {string} element: id del elemento html donde se creará la gráfica.
		* @param {array} data: información para crear la gráfica.
		* @param {string} title: Título de la gráfica a crear.
		* Postcondition: Se ha creado una gráfica en el html, en el div con id elemento.
		*/
		createChart: function( type, element, commsData=parentApp.commsData, title=""){		
			console.log("AAAAAAAAA " + element);
			var vm = this;
			var chartData = this.generateChartData(type, commsData);
			var elemento = document.getElementById(element);
			this.clasify = type;
			$('.comms-line').attr('chartype', type);
			elemento.setAttribute('chartype', this.clasify);
			if(chartData == false){ 
				console.log('JJAJAJAAJ ' + elemento);
				elemento.parentNode.classList.add('no-show');  
			}else{
				parentApp.mostrarResultados = true;
				elemento.parentNode.classList.remove('no-show');
				if(title==""){ title = type; }
				AmCharts.makeChart(element, {
					"type": "pie",
					"theme": "light",
					"dataProvider": chartData,
					"labelText": "[[percent]]",
					"labelRadius": 20,
					"labelTickAlpha":1,
					//"color": "#FFFFFF",
					//"labelText": "",
					"balloonText": "[[title]]: [[value]]",
					"titleField": "type",
					"valueField": "value",
					"outlineColor": "#ccc",
					"outlineAlpha": 0.8,
					"outlineThickness": 2,
					"colorField": "color",
					"pulledField": "pulled",
					"titles": [{
						"text": title
					}],
					"legend":{
						"position":"bottom",
						"valueText":"[[percent]]",
						"valueAlign":"left",
						"autoMargins":true
					},
					"listeners": [{
						"event": "clickSlice",
						"method": function(event) {
							if(type == "product"){
								var chart = event.chart;
								var selected = undefined;
								if (event.dataItem.dataContext.type != undefined) {
									selected = event.dataItem.dataContext.type;
								} else {
									selected = undefined;
								}						
								chart.dataProvider = vm.generateChartData(type, commsData, selected);
								chart.validateData();
								// chart.animateAgain();
							}
						},
					}],
					"export": {
						"enabled": true
					}	
				});
			}
		},
		/**
		* Selecciona un tipo de gráfica a mostrar y oculta las demás en el html
		* @param {string} tipo: tipo de las gráficas a mostrar.
		* Poscondition: se han ocultado las graficas de tipo diferente al especificado.
		* Postcondition: se muestran las gráficas del tipo especificado.
		*/
		chartSelect:function(tipo){
			$.each(this.comms_types, function(index, type){
				if(type['slug'] == tipo ){
					console.log(true);
					$('#section-'+tipo).parent().removeClass('hide-graphs');
				}else{
					console.log(false);
					$('#section-' + type['slug']).parent().addClass('hide-graphs');
				}
			});
			// var commsTotal = $('.comms-total[chartype!='+tipo+']');
			// var comms = $('.comms[chartype!='+tipo+']');
			//          	  commsTotal.addClass('no-show');
			//          	  comms.addClass('no-show');
			// console.log('post-tipo:' + comms.length + "total-tipo:" + commsTotal.length);
			// var egh = document.querySelectorAll('[chartype='+tipo+']');
			//          if(egh != 0){
			//             console.log('FUNCIONA');  	
			//          }
		}   
	},
	created: function(){
		parentApp.route = "comms";
		parentApp.consultRoute = "comms";
		var vm = this;
		parentApp.consultMethod = function(json){
			parentApp.commsData = json.comms;							
			for (var i = 0; i < 7; i++) {
				vm.setupChart(i);
			}
			var actualData = parentApp.getArraysOfData(parentApp.to);

			// Agrega las nuevas secciones donde se mostrarán las gráficas
			vm.comms_sections = [];
			$.each(actualData, function(key, value){
				var dict = {name:value['name'], slug:convertToSlug(value['name'])};
				vm.comms_sections.push(dict);
			});
			// se agrega a la lista de métodos laa gráficas que se actualizarán.
			var funct = function(){				
				$.each(actualData, function(key, value){
					vm.createSpecificChart(value['name'], value['comms']);
				});				
			};
			parentApp.updateFunctions.push(funct);
		};
	}
});