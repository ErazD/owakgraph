var route = window.location.href;
var route_array = route.split("//");
var route_values = route_array[1].split("/");
var post_id = route_values[2];
var url = window.location.protocol+"//"+window.location.host;
//Day
$.ajax({
    // url: "http://graph.dev:8888/posts/"+ post_id +"/metrics/reactions/hours",
    url: url+"/posts/"+ post_id +"/metrics/reactions/day",
    method: "GET",
    contentType: "application/json",
    dataType: "json",
    success: function(data) {

        new Chart(document.getElementById("chartjs-0"),
        {
            "type":"line",
            "data":
            {"labels": data[0],
            "datasets":[
            {
                "label":data[1],
                "data":data[2],
                "fill":false,
                "borderColor":"#4CAF50",
                "lineTension":0.1}]
            },
            "options":{
                responsive: true,
                maintainAspectRatio: false,
            }
        });


    }

});


