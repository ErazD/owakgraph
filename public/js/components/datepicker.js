
// <datepicker id="since" class="datepicker" name="since" type="text" placeholder="Y-m-d" 
// v-model="ParentSince" v-bind:date="ParentSince" v-on:set-date="ParentSetDate">

Vue.component('datepicker', {
	name: "datepicker",
	template: "<input />",
	props: ["date"],
	data: function(){
		return {
			internalDate: this.date
		}
	},
	mounted: function(e) {
		let vm = this;
		$(this.$el).pickadate({
			today: 'Hoy',
			clear: 'Limpiar',
			close: 'Ok',
			selectMonths: true, // Creates a dropdown to control month
			selectYears: 15, // Creates a dropdown of 15 years to control year,
			closeOnSelect: true, // Close upon selecting a date,
			monthsFull: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
			monthsShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
			weekdaysFull: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
			weekdaysShort: ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab'],
			weekdaysLetter: ['D', 'L', 'M', 'M', 'J', 'V', 'S'],
			format: 'yyyy-mm-dd',
			onSet: function(){
				// if(vm.$el.name == 'since'){
				// 	parentApp.since = vm.$el.value;
				// }else if(vm.$el.name == 'until'){
				// 	parentApp.until = vm.$el.value;
				// }
				vm.internalDate = vm.$el.value;
				vm.$emit('set-date', 'manual');
				// parentApp.setDate("manual");
			}
		});
	},
	methods:{
		updateValue: function(){
			//let vm = this;
			// if(vm.$el.name == 'since'){
			// 	vm.$el.value = parentApp.since;
			// }else if(vm.$el.name == 'until'){
			// 	vm.$el.value = parentApp.until;
			// }
			// alert(this.date);
			// this.$el.value = this.internalDate;
		}, 
	},
	watch:{
		internalDate(dat){
			this.$emit("input", dat);
		},
		date(dat){
			this.$el.value = dat;
		}
	},
	created:function(){
		// if(datepickers != undefined)
		// 	datepickers.push(this);
	}
});
