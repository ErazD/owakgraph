var url = window.location.protocol+"//"+window.location.host;

var datepickers = [];

let app = new Vue({
	el: '#fan-state',
	delimiters: ['<%', '%>'],
	data: {
		colorReactions:"#FF6600",
		chart:"",
		legend:"",
		selected:"",
		followersData:{},
		allData:{},
		country:'all',
		type_fan:"insta_profiles",
		to:'insta_profiles',
		since:'',
		until:'',
		interval:28,
		followers_chart:[],
		updateFunctions:[],
	},
	methods: {
		setDate: function(interval = this.interval, update=true){
			var d = new Date(this.until);
			switch (interval){
				case "hoy":
				d = new Date();
				this.since = d.toISOString().split("T")[0];
				this.until = d.toISOString().split("T")[0];
				console.log("hoy");
				break;
				case "day":
				this.since = d.toISOString().split("T")[0];
				this.until = d.toISOString().split("T")[0];
				console.log("day");
				break;
				case "all":
				this.since = "";
				this.until = "";
				console.log("all");
				break;
				default:	
				var firstDate = new Date(d);
				firstDate.setDate(d.getDate() - parseInt(this.interval));
				this.until = d.toISOString().split("T")[0];
				this.since = firstDate.toISOString().split("T")[0];
				console.log(interval);
				break;
			}
		},
		metricsByDay: function(){
			fetch(url+"/get-followers-count/"+this.channelID+"/allmetrics/day")
			.then(r => r.json()).then(json => {
				console.log("CHARTTTTTTTTTT");
				this.allData = json;
				// console.log(json);
				this.setDate("hoy", false);
				this.interval=28;
				this.setDate(28, false);
				app.getArraysOfData(app.to);
				// this.zoomChart(this.chartMD);
			});
		},
		getArraysOfData: function(to='insta_profiles'){
			var datas  = this.allData;
             // console.log(datas);
			 var funct = function(){	
				$.each(datas, function(key, value){
					app.createChart(value['name'] , value['data'], value['color']);
				});				
			 };
		     app.updateFunctions.push(funct);
			return app.allData;
		},
		getAxes:function(){
			var axes =	[{
				"id":"v1",
				"axisColor": this.colorReactions,
				"axisThickness": 2,
				"axisAlpha": 1,
				"position": "left",
				"precision": "0"
			} 
			];
			return axes;
		}, 
		getGraphs:function(){
			var graphs = [{
				"valueAxis": "v1",
				"lineColor": this.colorReactions,
				"bullet": "round",
				"bulletBorderThickness": 1,
				"hideBulletsCount": 30,
				"title": "Seguidores",
				"valueField": "followers",
				"fillAlphas": 0
			}];
			return graphs;
		},
		createChart: function(element, data, color){		
			console.log('fanpage ' + element);
			console.log(data);
			var title ='';
            var chartData = data;
            var chartName = 'fancount-chart-'+element; 
			var elemento = document.getElementById('fancount-chart-' + element);
			console.log('AJDKLFASJKLDF ' + chartData[0]['followers']);
			if(chartData[0]['followers'] == 'undefined'){
				// console.log(chartData[0]['followers']);
				// console.log(elemento);
				elemento.parentNode.classList.add('no-show'); 
			}else{
				
			if(title==""){ title = element; }

			var chart = AmCharts.makeChart(chartName, {
				"type": "serial",
				"theme": "light",
				"legend": {
					"valueAlign":"left",
					"useGraphSettings": true
				},
				"dataProvider": chartData,
				"synchronizeGrid":true,
				"valueAxes": this.getAxes(),
				"graphs": this.getGraphs(),
				"titleField": "element",
				"valueFiled":"",
				"chartScrollbar": {},
				"categoryField": "date",
				"categoryAxis": {
					"parseDates": true,
					"minPeriod": "mm",
					"axisColor": "#DADADA"
				},
				"titles": [{
						"text": title
				}],
				"export": {
					"enabled": true,
					"position": "top-right"
				},
				"chartCursor": {
					"valueBalloonsEnabled": true,
					"fullWidth": true,
					"cursorAlpha": 0.1,
					"valueLineBalloonEnabled": true,
					"valueLineEnabled": true,
					"valueLineAlpha": 0.5
				}
			});
			console.log(chart);
			return chart;
		}
		},
		zoomChart:function(chart){
			if(this.since!="" && this.until!=""){
				chart.zoomToDates(new Date(this.since), new Date(this.until));
			}else{
				chart.zoomOut();
			}

		}

	},
	created: function(){
		this.metricsByDay('reactions');
		//this.type_fan= (type_fan)? type_fan:this.type_fan;
	},
	computed: {
		channelID: function(){
			var route = window.location.href;
			var route_array = route.split("//");
			var route_values = route_array[1].split("/");
			return post_id = route_values[2];
			// return post_id = route_values[5];

		}
	},
	updated:function(){
       for (var i = app.updateFunctions.length - 1; i >= 0; i--) {
			var funct =	app.updateFunctions.shift();
			funct();
		}
	},
});
