
var url = window.location.protocol+"//"+window.location.host;
//var url = "http://owak.co/graph/public/index.php";

var datepickers =[];

function convertToSlug(Text)
{
	return Text
	.toLowerCase()
	.replace(/ /g,'-')
	.replace(/[^\w-]+/i,'');
}

function getChartData(type){
	// console.log(commsApp);
	// console.log(type);
	return commsApp.generateChartData(type);
}

function formatNumber(num) {
	return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.");
}

Vue.component('datepicker',{
	template: "<input />",
	mounted: function(e) {
		let vm = this;
		$(this.$el).pickadate({
			today: 'Hoy',
			clear: 'Limpiar',
			close: 'Ok',
	    selectMonths: true, // Creates a dropdown to control month
	    selectYears: 15, // Creates a dropdown of 15 years to control year,
	    closeOnSelect: false, // Close upon selecting a date,
	    monthsFull: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
	    monthsShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
	    weekdaysFull: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
	    weekdaysShort: ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab'],
	    weekdaysLetter: ['D', 'L', 'M', 'M', 'J', 'V', 'S'],
	    format: 'yyyy-mm-dd',
	    onSet: function(){
	    	if(vm.$el.name == 'since'){
	    		commsApp.since = vm.$el.value;
	    	}else if(vm.$el.name == 'until'){
	    		commsApp.until = vm.$el.value;
	    	}
	    }
	});
	}, methods:{
		updateValue: function(){
			let vm = this;
			if(vm.$el.name == 'since'){
				vm.$el.value = commsApp.since;
			}else if(vm.$el.name == 'until'){
				vm.$el.value = commsApp.until;
			}
		}
	}, created:function(){
		datepickers.push(this);
	}
});

let commsApp = new Vue({
	el: '#comms-charts',
	delimiters: ['<%', '%>'],
	data: {
		// colorLikes:"#B0DE09",
		// colorDislikes:"#FF6600",
		// colorComments:"#67b7dc",
		// colorViews:"#FCD202",
		commsData:{}, // datos luego del formato para las gráficas
		allData:{}, // datos recibidos del servidor		
		since:'', // filtro fecha dese
		until:'', // filtro fecha hasta
		country:'all', // filtro principal, país
		allCountries:['Argentina', 'Bolivia', 'Chile', 'Colombia', 'Centro América', 'Ecuador', 'Latino América', 'México', 'Paraguay', 'Perú', 'Uruguay', 'Venezuela'], // opciones filtro principal país
		pedido_id:2, // filtro principal id elemento
		allElements:[], // opciones para los filtros donde se elige el elemento de la categoría seleccionada
		type:'basket', // filtro principal, categoría del elemento a buscar 
		actual_type:'basket', // Categoría del elemento del que se realizó la última búsqueda
		ocultar: false,
		clasify:'',
		showChart:'',
		comms_types:[ // elementos especificos de la sección para organizar los resultados de la consulta por categorías.
		{name:"Tipo", slug:"comms-type",},
		{name:"Balance", slug:"comms-balance",},
		{name:"Tarea de comunicación", slug:"comms-task",},
		{name:"Producto", slug:"comms-product",},
		{name:"Tema de comunicación", slug:"comms-theme",},
		{name:"Concurso", slug:"comms-contest"},
		],
		comms_sections:[], // elementos específicos de la sección para organizar los resultados de la consulta por elementos buscados (fanpages, marcas, ...).
		to:"fanpages", // filtro principal, categoría con el que se muestran los resultados, debe ser menor o igual que la categoría de tipo.
		interval:28, // filtro de fecha, intervalo de tiempo para elegir la fecha inicial tomando la fecha final de referencia.
		updateFunctions:[], // lista de funciones a ejecutar luego de que vue haga modificaciones
		shareUrl:'', // URL para compartir la consulta
		extraURLParamters:{}, // valores extra para la url
		extra:{}, // atributos extra para la instancia de vue
	},
	methods: {
		/**
		* METODO FILTRO FECHAS
		* Calcula la fecha inicail dependiendo del intervalo y la fecha final.
		* @param interval: String que contiene un rango definidio como día actual ("hoy"), el mismo día ("day"), y todo ("all") o un número que representa en cantidad de días la diferencia entre la fecha inicial y la final.
		* Postcondition: commsApp.since ha sido modificado. 
		* Postcondition: commsApp.until ha sido modificado a la fecha actual si interval == "hoy", o se ha seleccionado un intervalo y commsAppuntil == "".
		*/
		setDate: function(interval = this.interval){
			this.interval = interval;
			var d = (this.until != "")? new Date(this.until) : new Date();
			switch (interval){
				case "hoy":
				d = new Date();
				this.since = d.toISOString().split("T")[0];
				this.until = d.toISOString().split("T")[0];
				console.log("hoy");
				break;
				case "day":
				this.since = d.toISOString().split("T")[0];
				this.until = d.toISOString().split("T")[0];
				console.log("day");
				break;
				case "all":
				this.since = "";
				this.until = "";
				console.log("all");
				break;
				default:	
				var firstDate = new Date(d);
				firstDate.setDate(d.getDate() - parseInt(this.interval));
				this.until = d.toISOString().split("T")[0];
				this.since = firstDate.toISOString().split("T")[0];
				console.log(interval);
				break;
			}
			$.each(datepickers, function(key, value){
				value.updateValue();
			});
		},

		getExtraUrlParameters: function(){
			var extraUrl = "";
			if(Object.keys(this.extraURLParamters).length >0){
				var conector = "";
				$.each(this.extraURLParamters, function(key, value){
					extraUrl += conector+value;
					conector = "&";
				});
			}			
			return extraUrl;
		},

		getConsultURL:function(createUrl=false, route="/comms"){

			if(createUrl || commsApp.shareUrl == ""){
				var shareUrl = url + route + "?type=" + this.type + "&pedido_id=" + this.pedido_id + "&country=" + this.country + "&to=" + this.to;
				if(this.since != "" && this.until != "" ){
					shareUrl +="&since=" + this.since + "&until=" + this.until;
				}
				shareUrl += (shareUrl.includes("?"))? "&": "?";	
				shareUrl += this.getExtraUrlParameters();

				commsApp.shareUrl == shareUrl;
			}
			if(!createUrl){ 
				var shURL = $('#share-url');
				shURL.val(shareUrl);
				shURL.removeClass('hide');
				shURL.select();
				document.execCommand('copy');
				Materialize.toast('Se ha copiado la URL', 2000);
			}
			return shareUrl;
		},

		/**
		* MÉTODO SECCIO PRINCIPAL, OBTENER DATOS
		* Pide los datos de la sección principal del servidor, y llama el método para oraganizar los datos.
		* Postcondition: commsApp.allData = datos obtenidos
		* Postcondition: commsApp.actual_type = commsData.type
		* (Se debería asignar la url de la consulta para que sus atributos no cambien)
		*/
		getConsult: function(){
			var theSection = $('.url-box');
			theSection.removeClass('hide');	
			this.ocultar = false;
			fecha = "";
			if(this.since != "" && this.until != "" ){
				fecha = "/" + this.since + "/" + this.until;
			}
			var extraUrl = "?"+this.getExtraUrlParameters();
			fetch(url+"/comms/" + this.type + "/" + this.pedido_id + "/" + this.country + fecha + extraUrl)
			.then(r => r.json()).then(json => {
				this.commsData = json.comms;
				this.allData = json;
				this.actual_type = this.type;
				
				for (var i = 0; i < 7; i++) {
					this.setupChart(i);
				}
				commsApp.getArraysOfData(commsApp.to);

				//this.createChart(json, 'hoursChart', '#4CAF50');
			});	
			// Actualiza la url de la consulta.
			commsApp.getConsultURL(true);
		},

		/**
		* MÉTODO SECCION PRINCIPAL, GENERAR DATOS PARA GRÁFICAS
		* Recorre un arreglo de elementos para crear un arreglo de elementos que se encuentre en un nivel menor al nivel inicial.
		* @param {string} to: nombre del nivel del que se quiere obtener los elementos.
		* @param {array} allData: arreglo con los elementos del nivel inicial.
		* @param {string} from: nivel inicial para la búsqueda de elementos.
		* @return {array}: arreglo con los elementos del nivel final.
		*/
		getArraysOfData: function(to='fanpages', allData=this.allData, from=this.actual_type){			
			/*
			LEVELS -> next
			vertical -> category -> basket -> brand -> (fanpage channel all)
			*/
			actual = from; // elemento desde donde se van a comenzar la consulta, (to es el elemento del que se agrupan los datos a mostrar)
			finish = false; // indica si ha llegado al final de los niveles
			actualData = [allData]; // lista con los datos agrupados por el modelo elegido en "to";
			while(!finish && actual != to){ // recorre los niveles parra llegar desde "from" a "to"
				newData = []; // arreglo con los elementos del siguiente nivel a recorrer
				finish = true; // comienza en true, para comparar la finalización de la búsqueda
				next_actual = ""; // nombre del siguiente nivel a recorrer
				$.each(actualData, function(key, value){ // recorre todos los elementos de la categoría actual
					infoLevel = commsApp.getNextLevel(actual, to, value);					
					infoData = infoLevel['next']; // lista de elementos del siguiente nivel.
					// console.log([infoLevel, value, infoData]);
					newData = newData.concat(infoData);
					next_actual = infoLevel['level'];
					finish &= infoLevel['finish']; // compara si debe finalizar la búsqueda.
				});	
				console.log("------- NEW DATA --------");
				console.log(newData);
				actualData = newData; // luego de recorrer el nivel actual, asigna los elementos del siguiente nivel
				actual = next_actual; // asigna el nombre del siguiente nivel
			}
			//console.log("FINNNNNN");
			// console.log(actualData);


			//FUNCIONES ADICIONALES EN EL MÉTODO
			// Agrega las nuevas secciones donde se mostrarán las gráficas
			commsApp.comms_sections = [];
			$.each(actualData, function(key, value){
				var dict = {name:value['name'], slug:convertToSlug(value['name'])};
				commsApp.comms_sections.push(dict);
			});
			// se agrega a la lista de métodos laa gráficas que se actualizarán.
			var funct = function(){				
				$.each(actualData, function(key, value){
					commsApp.createSpecificChart(value['name'], value['comms']);
				});				
			};
			commsApp.updateFunctions.push(funct);
			// FIN DE FUNCIONESADICIONALES DEL MÉTODO

			return actualData;
		},

		/**
		* MÉTODO SECCION PRINCIPAL, RETORNA DATOS SIGUIENTE NIVEL
		* obtiene la infromacion del siguiente nivel para los datos buscados.
		* @param {string} from: nivel inicial.
		* @param {string} to: nivel final.
		* @param {dict} data: diccionario con los elementos del siguiente nivel a acceder.
		* @return {dict}: {many:bool, next:array, level:string, finish:bool}
		*		many: indica si son muchos elemnteos los que se agregan.
		*		next: contiene los elementos del próximo nivel.
		*		level: nombre del próximo nivel.
		*		finish: indica si ya llegó al nivel buscado o al último nivel posible.
		*/
		getNextLevel:function(from, to="all", data){
			console.log(from+" -> "+to);
			resp = [];
			resp['many'] = false;
			sig = from;
			switch(from){
				case "vertical":
				sig = 'category';
				resp['next']=data['categories'];
				break;
				case "category":
				sig = 'basket';
				resp['next']=data['baskets'];
				break;
				case "basket":
				sig = 'brand';
				resp['next']=data['brands'];
				break;
				case "brand":
				if(to!= "all"){
					resp['next']=data['fanpages'].concat(data['channels']);
				}else{
					resp['next']=data[to];
				}
				resp['many'] = true;
				sig = to;
				break;
				case "fanpage":
				case "channel":
				case "adset":	
				resp['finish'] =true;			
				break;
			}
			resp['finish'] = sig == to;
			resp['level'] = sig;
			return resp;
		},
		reloadChart: function(type, id){
			this.type = type;
			this.pedido_id = id;
			this.metricsByHour();
		},
		createSpecificChart(id, data){
			for (var i = 0; i < 7; i++) {
				this.setupChart(i, id, data);
			}
			
		},
		/**
		* MÉTODO SECCIÓN PRINCIPAL, RETORNA EL COLOR
		* Busca el color asignado a un elemento y lo retorna.
		* @param {string} key: identificador del color (nombre en utf-8).
		* @param {string} type: nombre de la tabla si el elemento pertenece a uno, o undefined en caso contrario.
		* @return {string}: valor para el color del elemento si fué encontrado, o iundefined en caso contrario.
		*/
		getColorData:function (key, type=undefined){
			//console.log(key);
			var color = undefined;
			if(type){
				var col = allColors[type];
				if(col != undefined)
					col = col[key];
				if(col != undefined)
					color = col;
			}
			return color;
		},
		/** 
		* MÉTODO SECUNDARIO, ORGANIZA DATOS PARA GRÁFICAS
		* Recorre los datos generados y crea el arreglo que necesita una gráfica específica de la sección actual.
		* @param {string} type: 
		*/
		generateChartData: function(type, commsData=this.commsData, selected = undefined){
			var chartData = [];
			// console.log(type);
			// console.log(this.commsData[type]);
			var total = commsData[type]['count'];
			if(total != 0){
				if(type == "product"){
					commsD = {};
					$.each(commsData[type], function(key, value){
						if(key != "count" ){ 
							var k = (key != "No definido")?key.split(" - "): ["No definido", "no definido"];
							var cmmd = commsD[k[0]];
							if(cmmd != undefined){ // si existen más valores los suma
								var per = (parseInt(value) / parseInt(total) * 100).toFixed(2);
								var p = cmmd['products'];
								p.push({
									type: k[1],
									value: formatNumber(value),
									percent: per+"%",
									color: commsApp.getColorData(k[1], 'products'),
									pulled: true,
								});
								var v2 = cmmd['value'] + parseInt(value);
								per = (parseInt(v2) / parseInt(total) * 100).toFixed(2);
								cmmd['value'] = v2;
								cmmd['percent'] = per;
							}else{ // si no existen los valores los crea
								var per = (parseInt(value) / parseInt(total) * 100).toFixed(2);
								var p = [];
								p.push({
									type: k[1],
									value: formatNumber(value),
									percent: per+"%",
									color: commsApp.getColorData(k[1], 'products'),
									pulled: true,
								});
								commsD[k[0]] = {
									type: k[0],
									value: parseInt(value),
									percent: per+"%",
									color: commsApp.getColorData(k[0], 'segmentos'),
									products: p,
								};
							}			 					
						}
					});
					// console.log(commsD);
					$.each(commsD, function(key, cData){
						if(key != "count"){ 
							if (key == selected) {
								$.each(cData.products, function(k, dtp){
									chartData.push(dtp);
								});
							} else {
								cData.pulled = false;
								chartData.push(cData);
							}		
						}	  
					});
				}else{
					$.each(commsData[type], function(key, value){
						if(key != "count"){
							var per = (parseInt(value) / parseInt(total) * 100).toFixed(2);
							chartData.push({
								type: key,
								value: value,
								percent: per+"%",
								color: commsApp.getColorData(key),
							});
						}	  
					});	
				}
				console.log(chartData);
				return chartData;
			}else{
				console.log(chartData);
				return false;
			}
		},
		/**
		* MÉTODO SECUNDARIO, DEFINE EL TIPO Y TITULO SEGÚN SU ÍNDICE
		* @param {number} type: númer entre 0 y 6 que representa el tipo de gráfica.
		* @param {string} id: id base del div donde se pondrá la gráfica.
		* @param {array} commsData: datos generados y sin organizar para la gráfica.
		* 
		*/
		setupChart(type, id="", commsData=this.commsData){
			name = "";
			if(id!=""){
				name = id;
				id = convertToSlug(id) + "-";
			}else{

			}
			
			switch(type){
				case 0:				
				this.createChart("type", id+"comms-type", commsData, "Tipos de post: "+name);
				break;
				case 1:				
				this.createChart("balance", id+"comms-balance", commsData, "Balance: "+name);
				break;
				case 2:				
				this.createChart("comunication_task", id+"comms-task", commsData, "Tareas de comunicación: "+name);
				break;
				case 3:				
				this.createChart("product", id+"comms-product", commsData, "Productos: "+name);
				break;
				case 5:				
				this.createChart("comunication_theme", id+"comms-theme", commsData, "Temas de comunicación: "+name);
				break;
				case 6:				
				this.createChart("contest", id+"comms-contest", commsData, "Concursos: "+name);
				break;
			}
		},

		/**
		* MÉTODO SECUNDARIO, CREA UNA GRÁFICA
		* @param {string} type: tipo de la gráfica.
		* @param {string} element: id del div donde se creará la gráfica.
		* @param {array} commsData: datos generados y sin organizar para la gráfica.
		* @param {string} title: título de la gráfica.
		* Postcondition: la gráfica se ha creado.
		*/
		createChart: function(type, element, commsData=this.commsData, title=""){		
				//console.log("AAAAAAAAA " + element);
				var chartData = this.generateChartData(type, commsData);
				var elemento = document.getElementById(element);
				commsApp.clasify = type;
				$('.comms-line').attr('chartype', type);
				elemento.setAttribute('chartype', commsApp.clasify);
				if(chartData == false){ 
					console.log('JJAJAJAAJ ' + elemento);
					elemento.parentNode.classList.add('no-show');  
				}else{
					commsApp.ocultar = true;
					elemento.parentNode.classList.remove('no-show');
					if(title==""){ title = type; }
					AmCharts.makeChart(element, {
						"type": "pie",
						"theme": "light",
						"dataProvider": chartData,
						"labelText": "[[percent]]",
						"labelRadius": 20,
						"labelTickAlpha":1,
					//"color": "#FFFFFF",
					//"labelText": "",
					"balloonText": "[[title]]: [[value]]",
					"titleField": "type",
					"valueField": "value",
					"outlineColor": "#ccc",
					"outlineAlpha": 0.8,
					"outlineThickness": 2,
					"colorField": "color",
					"pulledField": "pulled",
					"titles": [{
						"text": title
					}],
					"legend":{
						"position":"bottom",
						"valueText":"[[percent]]",
						"valueAlign":"left",
						"autoMargins":true
					},
					"listeners": [{
						"event": "clickSlice",
						"method": function(event) {
							if(type == "product"){
								var chart = event.chart;
								var selected = undefined;
								if (event.dataItem.dataContext.type != undefined) {
									selected = event.dataItem.dataContext.type;
								} else {
									selected = undefined;
								}						
								chart.dataProvider = commsApp.generateChartData(type, commsData, selected);
								chart.validateData();
								// chart.animateAgain();
							}
						},
					}],
					"export": {
						"enabled": true
					}	
				});
				}
			},
			/**
			* MÉTODO SECUNDARIO, SELECCIONA UN TIPO DE GRÁFICA
			* Muestra las gráficas del tipo seleccionado, y oculta las gráficas que pertenezcan a otro tipo.
			* @param {string} tipo: slug del tipo de gráfica a mostrar.
			* Postcondition: Se han ocultado las gráficas no seleccionadas y se ha puesto visibles las seleccionadas.
			*/
			chartSelect:function(tipo){
				$.each(this.comms_types, function(index, type){
					if(type['slug'] == tipo ){
						console.log(true);
						$('#section-'+tipo).parent().removeClass('hide-graphs');
					}else{
						console.log(false);
						$('#section-' + type['slug']).parent().addClass('hide-graphs');
					}
				});
			}   
		},
		created: function(){
			this.type = (type)? type :this.type;
			this.pedido_id = (pedido_id)? pedido_id :this.pedido_id;
		//console.log("SE HA CREADO CON: \n"+this.type+"  "+ this.pedido_id);
		if( typeof formComms != "undefined"){
			this.setDate("hoy");
			this.setDate(28);
		}
		if( typeof allElements != "undefined"){
			this.allElements = allElements;
		}

		// this.metricsByHour();
	},
	computed: {
		
	}, 
	updated:function(){
		//console.log(commsApp.updateFunctions);
		for (var i = commsApp.updateFunctions.length - 1; i >= 0; i--) {
			var funct =	commsApp.updateFunctions.shift();
			funct();
		}
	},
});
