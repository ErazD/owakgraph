// var colorYTViews="#FF6600";
// var colorYTLikes="#FCD202";
// var colorYTDislikes="#B0DE09";
var url = window.location.protocol+"//"+window.location.host;

let consultApp = new Vue({
	el: '#stats',
	delimiters: ['<%', '%>'],
	data: {
		colorYTLikes:"#FA8258",
		colorYTDislikes:"#FF0000",
		colorYTComments:"#FACC2E",
		colorYTViews:"#F3E2A9",
		colorFBLikes:"#B0DE09",
		colorFBComments:"#2E9AFE",
		colorFBShares:"#8258FA",
		colorFBViews:"#A9BCF5",
		since:'2017-11-21',
		until:'2017-12-01',
		typeSel:'marca',
		slug:'johnsons',
		interval:'1 day',
		hidedivResult:true,
		maxResults:{},
		days:{},
		category:"",
	},
	methods: {
		globalMetrics: function(){
			fetch(url+"/consulta/"+this.since+"/"+this.until+"/"+this.typeSel+"/"+this.slug+"/"+this.interval)
			.then(r => r.json()).then(json => {
				console.log(json);
				this.hidedivResult = false;
				this.maxResults = json.max_dates;
				this.days = json.days;
				this.category = json.category;
				this.createChart("consult-chart");
			});
		},
		updateStatus: function(){
			console.log(this.typeSel);
			console.log(this.since);
		},
		setChart: function(){
		},
		createChart: function(element){		
			// console.log(element);
			// console.log(data);
			AmCharts.makeChart(element, {
				"type": "serial",
				"theme": "light",
				"legend": {
					"useGraphSettings": true
				},
				"dataProvider": this.generateChartData(),
				"synchronizeGrid":true,
				"valueAxes": [{
					"id":"v1",
					"axisColor": this.colorYTLikes,
					"axisThickness": 2,
					"offset": 50,
					"axisAlpha": 1,
					"position": "left",
					"precision": "0"
				}, {
					"id":"v2",
					"axisColor": this.colorYTDislikes,
					"axisThickness": 2,
					"gridAlpha": 0,
					"axisAlpha": 1,
					"position": "left",
					"precision": "0"
				}, {
					"id":"v3",
					"axisColor": this.colorYTComments,
					"axisThickness": 2,
					"gridAlpha": 0,
					"axisAlpha": 1,
					"position": "right",
					"precision": "0"
				}, {
					"id":"v4",
					"axisColor": this.colorYTViews,
					"axisThickness": 2,
					"offset": 50,
					"axisAlpha": 1,
					"position": "right",
					"precision": "0"
				}],
				"graphs": [{
					"valueAxis": "v1",
					"lineColor": this.colorYTLikes,
					"bullet": "triangleUp",
					"bulletBorderThickness": 1,
					"hideBulletsCount": 30,
					"title": "Youtube Likes: ",
					"valueField": "youtube_likes",
					"fillAlphas": 0
				}, {
					"valueAxis": "v2",
					"lineColor": this.colorYTDislikes,
					"bullet": "triangleDown",
					"bulletBorderThickness": 1,
					"hideBulletsCount": 30,
					"title": "Youtube Dislikes: ",
					"valueField": "youtube_dislikes",
					"fillAlphas": 0
				}, {
					"valueAxis": "v3",
					"lineColor": this.colorYTComments,
					"bullet": "square",
					"bulletBorderThickness": 1,
					"hideBulletsCount": 30,
					"title": "Youtube Comentarios: ",
					"valueField": "youtube_comments",
					"fillAlphas": 0
				}, {
					"valueAxis": "v4",
					"lineColor": this.colorYTViews,
					"bullet": "round",
					"bulletBorderThickness": 1,
					"hideBulletsCount": 30,
					"title": "Youtube Vistas: ",
					"valueField": "youtube_views",
					"fillAlphas": 0
				}, {
					"valueAxis": "v5",
					"lineColor": this.colorFBLikes,
					"bullet": "triangleUp",
					"bulletBorderThickness": 1,
					"hideBulletsCount": 30,
					"title": "Facebook Likes: ",
					"valueField": "facebook_reactions",
					"fillAlphas": 0
				}, {
					"valueAxis": "v6",
					"lineColor": this.colorFBComments,
					"bullet": "square",
					"bulletBorderThickness": 1,
					"hideBulletsCount": 30,
					"title": "Facebook Comentarios: ",
					"valueField": "facebook_comments",
					"fillAlphas": 0
				}, {
					"valueAxis": "v7",
					"lineColor": this.colorFBShares,
					"bullet": "round",
					"bulletBorderThickness": 1,
					"hideBulletsCount": 30,
					"title": "Facebook Vistas: ",
					"valueField": "facebook_shares",
					"fillAlphas": 0
				}],
				"chartScrollbar": {},
				"categoryField": "date",
				"categoryAxis": {
					"parseDates": true,
					"minPeriod": "mm",
					"axisColor": "#DADADA"
				},
				"export": {
					"enabled": true,
					"position": "bottom-right"
				},
				"chartCursor": {
					"valueBalloonsEnabled": true,
					"fullWidth": true,
					"cursorAlpha": 0.1,
					"valueLineBalloonEnabled": true,
					"valueLineEnabled": true,
					"valueLineAlpha": 0.5
				}
			});
		}, 
		generateChartData:function(){
			var chartData = [];
			$.each(this.days, function(key,day){
				chartData.push({
					date: day.date_f,
					facebook_comments: day.total.facebook_comments,  
					facebook_reactions: day.total.facebook_reactions,  
					facebook_shares: day.total.facebook_shares,  
					youtube_comments: day.total.youtube_comments,  
					youtube_dislikes: day.total.youtube_dislikes,  
					youtube_likes: day.total.youtube_likes,  
					youtube_views: day.total.youtube_views
				});
			});
			console.log(chartData);
			return chartData;
		}
	},
	created: function(){
		// this.globalMetrics();
		// this.specificMetrics();
	},
	computed: {
		videoID: function(){
			var route = window.location.href;
			var route_array = route.split("//");
			var route_values = route_array[1].split("/");
			return post_id = route_values[2];
			// return post_id = route_values[5];

		}
	}
});
