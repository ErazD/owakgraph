var url = window.location.protocol+"//"+window.location.host;

let app = new Vue({
	el: '#stats',
	data: {},
	methods: {

		metricsByHour: function(filter){
			fetch(url+"/posts/"+this.postID+"/metrics/"+filter+"/hours")
			.then(r => r.json()).then(json => {
				this.createChart(json, 'hoursChart', '#4CAF50');
			});
			
			
		},

		metricsByDay: function(filter){
			fetch(url+"/posts/"+this.postID+"/metrics/"+filter+"/day")
			.then(r => r.json()).then(json => {
				this.createChart(json, 'daysChart', '#2196f3');
			});
		},

		createChart: function(data, element, color){
			console.log(data);
			if(element=="hoursChart"){
				$('#hoursChartdiv').html('<canvas id="hoursChart"></canvas>');
			}else if(element=="daysChart"){
				$('#daysChartdiv').html('<canvas id="daysChart"></canvas>');
			}

			new Chart(document.getElementById(element),
			{
				"type": "line",
				"data": {
					"labels": data[0],
					"datasets":[
					{
						"label":data[1],
						"data":data[2],
						"fill":false,
						"borderColor": color,
						"lineTension":0.1
					}]
				},
				options: {
					responsive: true,
					maintainAspectRatio: false,
				}
			});
		}
	}, 
	created: function(){
		this.metricsByHour('reactions');
		this.metricsByDay('reactions');
	},
	computed: {
		postID: function(){
			var route = window.location.href;
			var route_array = route.split("//");
			var route_values = route_array[1].split("/");
			return post_id = route_values[2];
			// return post_id = route_values[5];

		}
	}
});


function removeData(chart) {
    chart.data.labels.pop();
    chart.data.datasets.forEach((dataset) => {
        dataset.data.pop();
    });
    chart.update();
}