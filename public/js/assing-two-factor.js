var selectedUsers =  {};
var userSelected = document.getElementById("tw-users");
var resultLogs = document.getElementById("assignation-result-list");
/**
* Agrega usuarios a la lista de usuarios seleccionados
* @param {...Object} arrayTWUsers: lista de usuarios para seleccionar.
* Postcondition: se han agregado usuarios a la lista deseleccionados.
*/
function selectUsers(...arrayTWUsers){
	for(ind in arrayTWUsers){
		twU = arrayTWUsers[ind];
		selectedUsers[twU.id] = twU.name;
		// console.log(twU);
	}
	// console.log(selectedUsers);
	updateSelectedUsers();
}

/**
* Elimina un usario seleccionado y vuelve a pintar los usuarios seleccionados.
* Postcondition: se ha eliminado un objeto de la lista de usuarios seleccionados.
*/
function deselectuser(key){
	delete selectedUsers[key];
	updateSelectedUsers();
}

/**
* Actualiza la lista de usuarios seleccionados en el formulario de agregarusuarios.
* Postcondition: el html que muestra los usuarios seleccionados ha sido actualizado.
*/
function updateSelectedUsers(){
	var html= "";
	Object.keys(selectedUsers).forEach(function(key){
		html += " <span class='filter-tag' onclick='deselectuser("+key+")'>"+selectedUsers[key]+" </span>";
	});
	userSelected.innerHTML = html;
}
/**
* Obtiene la lista de IDs de los usuarios seleccionados y los separa con comas (',').
* @return {string}: lista con las id de usuarios.
* Precondition: selectedUsers debe estra inicializado.
*/
function getUsersIds(){
	var usersList = "";
	Object.keys(selectedUsers).forEach(function(key){
			usersList += ((usersList == "")? "": ",") + key;
		});
	return usersList;
}


let assingTWApp = new Vue({
	el: '#tw-app',
	delimiters: ['<%', '%>'],
	data: {
		since:'', // filtro fecha dese
		until:'', // filtro fecha hasta
		interval:28, // filtro de fecha, intervalo de tiempo para elegir la fecha inicial tomando la fecha final de referencia.
		country:'all', // filtro principal, país
		allCountries:['Argentina', 'Bolivia', 'Chile', 'Colombia', 'Centro América', 'Ecuador', 'Latino América', 'México', 'Paraguay', 'Perú', 'Uruguay', 'Venezuela'], // opciones filtro principal país
		pedido_id:2, // filtro principal id elemento
		allElements:[], // opciones para los filtros donde se elige el elemento de la categoría seleccionada
		type:'Canasta', // filtro principal, categoría del elemento a buscar 
		actual_type:'Canasta', // Categoría del elemento del que se realizó la última búsqueda
		social_network: 'Facebook',
		to:"Fanpage", // filtro principal, categoría con el que se muestran los resultados, debe ser menor o igual que la categoría de tipo.
		mostrarTo:true,
		mostrarResultados: false,

		updateFunctions:[], // lista de funciones a ejecutar luego de que vue haga modificaciones

		shareUrl:'', // URL para compartir la consulta
		extraURLParamters:{}, // valores extra para la url
		extra:{}, // atributos extra para la instancia de vue
	},
	methods:{
		/**
		* METODO FILTRO FECHAS
		* Calcula la fecha inicail dependiendo del intervalo y la fecha final.
		* @param interval: String que contiene un rango definidio como día actual ("hoy"), el mismo día ("day"), y todo ("all") o un número que representa en cantidad de días la diferencia entre la fecha inicial y la final.
		* Postcondition: this.since ha sido modificado. 
		* Postcondition: this.until ha sido modificado a la fecha actual si interval == "hoy", o se ha seleccionado un intervalo y thisuntil == "".
		*/
		setDate: function(interval = this.interval){
			this.interval = interval;
			var d = (this.until != "")? new Date(this.until) : new Date();
			switch (interval){
				case "hoy":
				d = new Date();
				this.since = d.toISOString().split("T")[0];
				this.until = d.toISOString().split("T")[0];
				console.log("hoy");
				break;
				case "day":
				this.since = d.toISOString().split("T")[0];
				this.until = d.toISOString().split("T")[0];
				console.log("day");
				break;
				case "all":
				this.since = "";
				this.until = "";
				console.log("all");
				break;
				case "manual":
				// No hacer nada
				break;
				default:	
				var firstDate = new Date(d);
				firstDate.setDate(d.getDate() - parseInt(this.interval));
				this.until = d.toISOString().split("T")[0];
				this.since = firstDate.toISOString().split("T")[0];
				console.log(interval);
				break;
			}
			// $.each(datepickers, function(key, value){
			// 	value.updateValue();
			// });
		},
		/**
		* Asigna los posts y usuarios a calificar por medio de ajax.
		* Postcondition: actualiza el la sección en html que muestra los resultados.
		*/
		sendAssignmentRequest: function(){
			var ids = getUsersIds();
			if(ids == ''){
				Materialize.toast('Selecciona usuarios para calificar.', 2000, 'orange darken-1');
				return;
			}
			var assingUrl = getUrl('/assing-tw/') + ids + '/' + this.social_network + '/' + this.type + '/' + this.pedido_id + '/' + this.country + '/' + (this.since?this.since:'none') + '/' + (this.until?this.until:'none') + '?tw_profiles=-1';

			fetch(assingUrl)
			.then(r => r.json()).then(json => {
				console.log(json);
				if(json.error){
					Materialize.toast(json.error, 2000, 'red darken-1');
				}else{
					var node = document.createElement("LI");
					var brandName ='';
					var arr = assingTWApp.allElements[assingTWApp.type];
					for (var ind = 0; ind < arr.length && brandName == ''; ind++) {
						var brnd = arr[ind];
						if(brnd.id == assingTWApp.pedido_id){
							brandName = brnd.name
						}
					}
					var log =  assingTWApp.type + ": " + brandName + " (" + ((assingTWApp.since && assingTWApp.until)? (assingTWApp.since + " - " + assingTWApp.until): 'sin retricción de fecha') + ")" + ", Red social: " + assingTWApp.social_network + ", \"país\" " + assingTWApp.country + "<br>";
					log += json.message;
					node.innerHTML = log;	    			
					resultLogs.appendChild(node);
					$('#assignation-result').removeClass('hide');
				}
			});

		},
	},
	updated:function(){
		// Ejecuta las funciones que estén en la lista
		for (var i = this.updateFunctions.length - 1; i >= 0; i--) {
			var funct =	this.updateFunctions.shift();
			funct();
		}
	},
	});



