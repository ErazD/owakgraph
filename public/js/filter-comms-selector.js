/**
* Agrega a la aplicación principal de vue las funciones para crear una url con filtros de las opciones del comms panorama.
*/

/** Aplicación principal de vue */
var vueApp;
/** Diccionario con todos los productos registrados { slug:{name:Nombre, slug:idProducto, Vertical:idVertical, Categoria:idCategoria, Canasta:idCanasta, Brand:idMarca }, ...} */
let allProducts =[];
let allSegments =[];
/** Diccionario con todos los concursos registrados { slug:{name:Nombre, slug:idConcurso, Vertical:idVertical, Categoria:idCategoria, Canasta:idCanasta, Brand:idMarca }, ...} */
let allContests=[];

/**
* Inicializa las propiedades para los filtros  de comms.
* @param {Vue} vApp: elemento de vue al que se le agregarán las propiedades.
* PostCondition: vApp ha sido actualizado con nuevos atributos y métodos.
*/
function initializeFilter(vApp){
	vueApp = vApp;
	createFilterData();
	createFilterFunctions();
	setupVariableFilters();
}

/**
* Agrega a vApp los atributos que se usarán para los filtros de comms.
* Precondition: vApp != null.
* Postcondition: se ha creado nuevos atributos a vApp.
*/
function createFilterData(){
	var extra = {};
	extra.commsFilter = {};
	// {'type' : {'name': '', 'slug': '', 'list': [{'name':'', 'slug':''}, {'name':'', 'slug':''}]}
	extra.commsFilter.all = {}; 
	extra.commsFilter.selected = {};
	extra.commsFilter.paramsURL = '';
	extra.commsFilter.operation = 'conjunction';
	vueApp.$set(vueApp, 'extra', extra);
	console.log(vueApp.extra);
	// vueApp.extra.commsFilter.all['balance'] = {'name': 'Balance', 'slug': 'balance', 'list':[{'name':'Emocional', 'slug':'emocional'}, {'name':'Funcional', 'slug':'funcional'}]};
	// vueApp.extra.commsFilter.all['type'] = {'name': 'Tipo', 'slug': 'type', 'list':[{'name':'Photopost', 'slug':'photopost'}, {'name':'Video', 'slug':'video'}, {'name':'Linkad', 'slug':'linkad'}]};
}

/**
* Agrega a vApp las funciones que se usarán para los filtros de comms.
* Precondition: vApp != null.
* Postcondition: se ha creado nuevas funciones a vApp.
*/
function createFilterFunctions(){
	/**
	* Selecciona un valor para filtrar. Se usa cuando se cargan los valores seleccionados desde una consulta compartida
	* @param {string} categorySlug: nombre del tipo de comms que se quiere agregar (tipo, tarea de comunicación, producto, ...).
	* @param {string} optionSlug: nombre del valor a agregar, nombre o id de elemento a agregar. Ejemplo optionSlug = 'emocional' (categorySlug = balance)
	*/
	vueApp.addSelectedCommsFilter = function(categorySlug, optionSlug){
		if(!vueApp.extra.commsFilter.selected[categorySlug])
			vueApp.extra.commsFilter.selected[categorySlug] = {};
		vueApp.extra.commsFilter.selected[categorySlug][optionSlug] = vueApp.extra.commsFilter.all[categorySlug]['list'][optionSlug];
		vueApp.updateParamsUrl();
	}
	/**
	* Muestra u oculta el menú para seleccionar los filtros de las categoría.
	* @param {string} categorySlug: nombre de la categoría a mostrar
	*/
	vueApp.showFilterContent = function(categorySlug){
		var optionSlug = $('#expand-'+categorySlug);
		var headerSlug = $('#header-'+categorySlug);
		optionSlug.toggle();
		headerSlug.css('background-color', '#1565C0');
		headerSlug.css('color', '#fff');
		$(document).mouseup(function(e) {
	   		// if the target of the click isn't the container nor a descendant of the container
	   		if (!optionSlug.is(e.target) && optionSlug.has(e.target).length === 0) 
	   		{
	   			optionSlug.hide();
	   			headerSlug.css('background-color', '#FFF');
	   			headerSlug.css('color', '#333');
	   		}
	   	});
	}



	/**
	* Selecciona o desecciona un valor para filtrar.
	* @param {string} categorySlug: nombre del tipo de comms que se quiere agregar (tipo, tarea de comunicación, producto, ...).
	* @param {string} optionSlug: nombre del valor a agregar, nombre o id de elemento a agregar. Ejemplo optionSlug = 'emocional' (categorySlug = balance)
	* Postcondition: se ha actualizado el valor de los filtros seleccionados
	*/
	vueApp.selectCommsFilter = function(optionSlug, categorySlug){
		console.log(categorySlug + " ---- " +  optionSlug);
		if(!vueApp.extra.commsFilter.selected[categorySlug])
			vueApp.extra.commsFilter.selected[categorySlug] = {};
		// var element = vueApp.extra.commsFilter.all[categorySlug]['list'][optionSlug];
		if(vueApp.extra.commsFilter.selected[categorySlug][optionSlug] != undefined){
			vueApp.deselectCommsFilter(categorySlug, optionSlug);   
		}else{
			vueApp.extra.commsFilter.selected[categorySlug][optionSlug] = vueApp.extra.commsFilter.all[categorySlug]['list'][optionSlug];
		}
		vueApp.updateParamsUrl();
	}

	/**
	* Elimina un valor de la lista de filtros.
	* @param {string} categorySlug: nombre del tipo de comms que se quiere eliminar (tipo, tarea de comunicación, producto, ...).
	* @param {string} optionSlug: nombre del valor a eliminar, nombre o id de elemento a eliminar. Ejemplo optionSlug = 'emocional' (categorySlug = balance)
	* Postcondition: se ha actualizado el valor de los filtros seleccionados
	*/
	vueApp.deselectCommsFilter = function(categorySlug, optionSlug){
		// console.log(categorySlug +"  "+ optionSlug);
		delete vueApp.extra.commsFilter.selected[categorySlug][optionSlug];
		$('#filter-'+optionSlug+categorySlug).prop('checked', false);
		if(Object.keys(vueApp.extra.commsFilter.selected[categorySlug]).length <= 0)
			delete vueApp.extra.commsFilter.selected[categorySlug];
		vueApp.updateParamsUrl();
	}

	/**
	* Actualiza la sección de url que contiene los parámetros estra.
	* Precondition: vApp != null.
	* Postcondition: se ha modificado la url de filtros en extraURLParamters.
	*/
	vueApp.updateParamsUrl = function(){
		var pUrl = (Object.keys(vueApp.extra.commsFilter.selected).length > 0)? "filtro_comms=": "";
		var separator = "";
		$.each(vueApp.extra.commsFilter.selected, function(slug_type, options){
			pUrl += separator + slug_type + ":";
			separator = "";
			$.each(options, function(slug_opt, opt){
				pUrl += separator + slug_opt;
				if(separator == "")
					separator = ",";
			});
			separator = ";";
		});
		if(pUrl == ""){
			delete vueApp.extraURLParamters['filtro_comms'];
		}else{
			pUrl += "&filtro_comms_operation=" + vueApp.extra.commsFilter.operation;
			vueApp.extraURLParamters['filtro_comms'] = pUrl;
		}
		vueApp.$set(vueApp.extra.commsFilter, 'paramsURL', pUrl);
	}
}

/**
* Agrega a vApp las funciones que se usarán para filtrar los productos y concursos dependiendo de la categoría elegida.
* Precondition: vApp != null.
* Postcondition: se han modificado los productos y concursos del filtro.
*/
function setupVariableFilters(){
	vueApp.updateProductsContestsFilter = function(){
		
		var nProducts = {'name': 'Producto', 'slug': 'product', 'list': filterByAttr(vueApp.type, vueApp.pedido_id, allProducts, true)};
		vueApp.$set(vueApp.extra.commsFilter.all, 'product', nProducts);
		
		var nSegment = {'name': 'Segmento', 'slug': 'segment', 'list': filterByAttr(vueApp.type, vueApp.pedido_id, allSegments)};
		vueApp.$set(vueApp.extra.commsFilter.all, 'segment', nSegment);

		var nContests = {'name': 'Concurso', 'slug': 'contest', 'list': filterByAttr(vueApp.type, vueApp.pedido_id, allContests)};
		vueApp.$set(vueApp.extra.commsFilter.all, 'contest', nContests);
	};
	
	// let getProductOptions =  document.getElementsByClassName('option-category');
	// for(let i = 0; i<getProductOptions.length; i++){
	// 	getProductOptions[i].addEventListener('click', vueApp.updateProductsContestsFilter);
	// }
	document.getElementById('model-type').addEventListener('change', vueApp.updateProductsContestsFilter);
	document.getElementById('model-id').addEventListener('change', vueApp.updateProductsContestsFilter);
	var firtUpdateProductsContests = function(){
		vueApp.updateProductsContestsFilter();	
	};
	vueApp.updateFunctions.push(firtUpdateProductsContests);
}
/**
* Filtra de un diccionario los elementos que tengan un atributo key y contenga el valor value.
* Ejemplo: key = canasta, value = 1, retorna todos los elementos que pertenescan a la canasta 1.
* @param {string} key: atributo que se usará para filtrar.
* @param {string} value: valor que se buscará en el filtro.
* @param {dict} list: lista con los elementos a filtrar.
* @return {dict}: diccionario con los elementos de list que hayan pasado el filtro.
*/
function filterByAttr(key, value, list = allProducts, addNull=false){
	function belongs(element){
		return  element[key]!=undefined && element[key].includes(' ' + value + ' ');
	}
	// return list.filter(belongs);
	// var newList = addNull? {'null': allProducts['null']}: {};
	var initial = addNull? {'null': allProducts['null']}: {};
	var newList = Object.keys(list).reduce((obj, k) => {var v = list[k]; if(belongs(v)){obj[k]=v;}return obj;}, initial);
	/*$.each(list, function(k, v){
		// console.log("IMPRIMEEEE", key, value, k, v, (k!=null));
		if(belongs(v))
			newList[k] = v;
	}); */
	return newList;
}

/**
* Agrega una opción a una lista, usado para llenar las opciones de las listas de filtros.
* @param {string} name: nombre de la opción.
* @param {string} slug: slug de la opción.
* @param {array} lista donde se agrega la nueva opción.
* @return arreglo con la nueva opción agregada.
*/
function addOptionToModelList(name, slug, list={}){
	if(list == undefined) list={};
	list[slug] = {'name': name, 'slug': slug};
	return list;
}

/**
* Agrega un nuevo filtro con sus opciones, las opciones tienen el formato retornado por el método addOptionToModelList.
* @param {string} name: nombre de la categoría.
* @param {string} slug: slug de la categoría.
* @param {array} lista con las opciones de la categoría.
* Postcondition: se agregado un nuevo filtro con sus opciones a la lista.
*/
function addCategoryToCommsFilter(name, slug, list={}){
	vueApp.extra.commsFilter.all[slug] = {'name': name, 'slug': slug, 'list': list};
}
/**
* Retorna los elementos de una lista con indices ordenados por un atributo y como un arreglo.
* @param {object} object: lista a ordenar. {index1:{attr:value, ...}, index2:{attr:value, ...}, ...}.
* @param {string} attr: Nombre del atributo por el que se filtrará.
* @return {Array}: lista ordenada por el atributo. [{attr:value, ...}, {attr:value, ...}, ...]
*/
function getOrderObjectListByField(object, attr="name"){
	// console.log(object, attr);
	return Object.values(object).sort((a,b)=>a[attr].localeCompare(b[attr]));
}





