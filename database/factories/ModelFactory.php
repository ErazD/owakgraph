<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
    'name' => $faker->name,
    'email' => $faker->unique()->safeEmail,
    'password' => $password ?: $password = bcrypt('secret'),
    'remember_token' => str_random(10),
    ];
});


$factory->define(App\Brand::class, function (Faker\Generator $faker) {

    return [
    'name' => $faker->name(),
    'slug' => $faker->slug(),
    'created_at' => $faker->dateTimeThisDecade,
    'updated_at' => $faker->dateTimeThisDecade,
    ];
});

$factory->define(App\Fanpage::class, function (Faker\Generator $faker) {

    return [
    'name' => $faker->name(),
    'url' => $faker->url(),
    'social_network' => 'Facebook',
    ];
});

$factory->define(App\Post::class, function (Faker\Generator $faker) {
    return [
    'content' => $faker->words(random_int(80, 200), true),
    'type' => 'linkad',
    'facebook_id' => random_int(1000, 1000000000000),
    'published' => $faker->dateTimeThisDecade,
    ];

});

$factory->define(App\Metric::class, function (Faker\Generator $faker) {

    return[
    'post_id' => 15,
    'reactions' => random_int(20, 10000),
    'comments' => random_int(10, 1000),
    'shares' => random_int(0, 200),
    'created_at' => $faker->dateTimeThisMonth,
    'updated_at' => $faker->dateTimeThisMonth,
    ];

});