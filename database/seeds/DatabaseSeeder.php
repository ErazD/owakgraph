<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$brands = factory(App\Brand::class, 10)->create();

    	$brands->each(function(App\Brand $brand) use ($brands){

    		$fanpages = factory(App\Fanpage::class, 3)->create([
    			'brand_id' => $brand->id,
              ]);

            $fanpages->each(function(App\Fanpage $fanpage) use ($fanpages){

                $posts = factory(App\Post::class, random_int(20, 100))->create([
                    'fanpage_id' => $fanpage->id,
                    ]);
            }); 

        });

         $metrics = factory(App\Metric::class, random_int(50, 100))->create();
    }
}
