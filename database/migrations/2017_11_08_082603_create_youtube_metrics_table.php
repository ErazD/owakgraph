<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateYoutubeMetricsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('youtube_metrics', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('youtube_video_id')->unsigned();

            $table->integer('views')->nullable()->default(0);
            $table->integer('likes')->nullable()->default(0);
            $table->integer('dislikes')->nullable()->default(0);
            $table->integer('comments')->nullable()->default(0);

            $table->foreign('youtube_video_id')->references('id')->on('youtube_videos');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('youtube_metrics');
    }
}
