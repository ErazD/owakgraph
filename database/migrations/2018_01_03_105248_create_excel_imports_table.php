<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExcelImportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('excel_imports', function (Blueprint $table) {
            $table->increments('id');
            $table->string('url');
            $table->string('facebook_id')->nullable();
            $table->integer('post_id')->unsigned()->nullable();
            $table->string('marca')->nullable();
            $table->string('type')->nullable();
            $table->string('communication_task')->nullable();
            $table->string('balance')->nullable();
            $table->integer('product_id')->unsigned()->nullable();
            $table->string('communication_theme')->nullable();
            $table->string('nombre_del_concurso')->nullable();
            $table->string('mecanica_del_concurso')->nullable();
            $table->string('premio')->nullable();
            $table->string('duracion')->nullable();
            $table->boolean('influencer')->default(false);
            $table->string('nombre_del_influencer')->nullable();
            $table->string('pauta_vs_no_pauta')->nullable();
            $table->integer('reproducciones')->default(0);
            $table->integer('estado')->default(0);
            $table->timestamps();
            $table->foreign('post_id')->references('id')->on('posts')->onDelete('set null')->onUpdate('cascade');
            $table->foreign('product_id')->references('id')->on('products')->onDelete('set null')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('excel_imports');
    }
}
