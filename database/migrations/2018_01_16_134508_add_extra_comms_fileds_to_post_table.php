<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddExtraCommsFiledsToPostTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('posts', function(Blueprint $table){
            $table->string('influencer')->nullable();
            $table->boolean('pauta')->default(false);
            $table->integer('scrap_state')->default('0');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::table('posts', function(Blueprint $table){
              $table->dropColumn('influencer');
              $table->dropColumn('pauta');
              $table->dropColumn('scrap_state');
        });
    }
}
