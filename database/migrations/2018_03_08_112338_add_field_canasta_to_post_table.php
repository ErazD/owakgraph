<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldCanastaToPostTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('posts', function (Blueprint $table) {
            $table->integer('canasta_id')->unsigned()->nullable();
            $table->foreign('canasta_id')->references('id')->on('canastas')->onDelete('set null')->onUpdate('cascade');
            });

        Schema::table('youtube_videos', function (Blueprint $table) {
            $table->integer('canasta_id')->unsigned()->nullable();
            $table->foreign('canasta_id')->references('id')->on('canastas')->onDelete('set null')->onUpdate('cascade');
            });
        Schema::table('two_factor_authentications', function (Blueprint $table) {
            $table->integer('canasta_id')->unsigned()->nullable();
            $table->foreign('canasta_id')->references('id')->on('canastas')->onDelete('set null')->onUpdate('cascade');
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('posts', function(Blueprint $table){
            $table->dropColumn('canasta_id');
        });

        Schema::table('youtube_videos', function(Blueprint $table){
            $table->dropColumn('canasta_id');
        });

        Schema::table('two_factor_authentications', function(Blueprint $table){
            $table->dropColumn('canasta_id');
        });
    }
}
