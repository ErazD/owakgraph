<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddYoutubeMissingAtributesTwoFactor extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::table('contest_post', function (Blueprint $table) {
            $table->string('social_network')->after('post_id');
        });
         Schema::table('youtube_videos', function(Blueprint $table){
            $table->string('influencer')->nullable();
            $table->boolean('pauta')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contest_post', function(Blueprint $table){
            $table->dropColumn('social_network');
        });
        Schema::table('youtube_videos', function(Blueprint $table){
              $table->dropColumn('influencer');
              $table->dropColumn('pauta');
        });
    }
}
