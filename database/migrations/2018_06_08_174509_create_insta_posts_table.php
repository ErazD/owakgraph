<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInstaPostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('insta_posts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('insta_profile_id')->unsigned();
            $table->string('post_id')->nullable();
            $table->text('permalink_url')->nullable();
            $table->text('content')->nullable();
            $table->string('type')->nullable();
            $table->text('attachment')->nullable();
            $table->dateTime('published');
            
            $table->string('balance')->nullable();
            $table->integer('product_id')->unsigned()->nullable();
            $table->string('communication')->nullable();
            $table->string('theme')->nullable();
            $table->string('campaign')->nullable();
            $table->string('influencer')->nullable();
            $table->string('micro_moment')->nullable();
            $table->string('business_objective')->nullable();
            $table->boolean('pauta')->default(false);
           
            $table->integer('canasta_id')->unsigned()->nullable();
            $table->integer('scrap_type')->nullable()->default(0);
            $table->integer('scrap_state')->default(0);

            $table->foreign('insta_profile_id')->references('id')->on('insta_profiles');
            $table->foreign('canasta_id')->references('id')->on('canastas')->onDelete('set null')->onUpdate('cascade');
            $table->foreign('product_id')->references('id')->on('products')->onDelete('set null')->onUpdate('cascade');

            $table->timestamps();
            
           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('insta_posts');
    }
}
