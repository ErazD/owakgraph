<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateYoutubeScrapsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('youtube_scraps', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('youtube_channel_id')->unsigned();
            $table->timestamps();
            $table->foreign('youtube_channel_id')->references('id')->on('youtube_channels');
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('youtube_scraps');
    }
}
