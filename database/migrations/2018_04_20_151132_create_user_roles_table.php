<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_roles', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('element_id')->unsigned()->nullable();
            $table->string('element_model')->default('User'); // nombre del modelo al que se relaciona (usuario, grupo, perfil, etc... )
            $table->timestamps();
            $table->string('permission'); // slug del permiso
            $table->integer('permission_value')->default(0)->nullable();
            // $table->foreign('element_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_roles');
    }
}
