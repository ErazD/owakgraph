<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldScrapTypeToFanpageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('fanpages', function (Blueprint $table) {
            $table->integer('scrap_type')->nullable();
        });
        Schema::table('youtube_channels', function (Blueprint $table) {
            $table->integer('scrap_type')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('fanpages', function(Blueprint $table){
            $table->dropColumn('scrap_type');
        });
        Schema::table('youtube_channels', function(Blueprint $table){
            $table->dropColumn('scrap_type');
        });
    }
}
