<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

// Genera Tabla para crear los reportes de youtube

class CreateYoutubeReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('youtube_reports', function (Blueprint $table) {
            $table->increments('id');
            $table->text('title')->nullable();
            $table->text('content')->nullable();
            $table->string('report_id');
            $table->integer('likes');
            $table->integer('dislikes');
            $table->integer('comments')->nullable();
            $table->integer('views');
            $table->string('type');
            $table->text('url');
            $table->timestamp('published');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('youtube_reports');
    }
}
