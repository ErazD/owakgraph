<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateYoutubeVideosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('youtube_videos', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('youtube_channel_id')->unsigned();
            $table->string('youtube_id');
            $table->timestamp('published');
            $table->string('title');
            $table->text('content')->nullable();
            $table->string('type')->default("video");
            $table->text('permalink_url')->nullable();
            $table->text('attachment')->nullable();

            $table->string('balance')->nullable();
            $table->integer('product_id')->unsigned()->nullable();
            $table->string('comunication')->nullable();
            $table->string('theme')->nullable();
            $table->string('campaign')->nullable();
            
            // indicador para saber cada cuanto se hace scrap, 0:hora, 1:diario, 2:semanal, 3:mensual(publicaciones de más de un año), 4:anual, -1:no se hace.
            $table->integer('scrap_type')->default(0); 

            #$table->foreign('product_id')->references('id')->on('products');
            $table->foreign('youtube_channel_id')->references('id')->on('youtube_channels');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('youtube_videos');
    }
}
