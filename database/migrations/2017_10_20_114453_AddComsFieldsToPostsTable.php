<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddComsFieldsToPostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('posts', function (Blueprint $table) {
            $table->string('balance')->nullable();
            $table->integer('product_id')->unsigned()->nullable();
            $table->string('comunication')->nullable();
            $table->string('theme')->nullable();
            $table->string('campaign')->nullable();
            $table->foreign('product_id')->references('id')->on('products')->onDelete('set null')->onUpdate('cascade');
// $table->string('product_id')->nullable();
              // indicador para saber cada cuanto se hace scrap, 0:hora, 1:diario, 2:semanal, 3:mensual(publicaciones de más de un año), 4:anual, -1:no se hace.
            $table->integer('scrap_type')->default(0); 
            $table->string('influencer')->nullable();
            $table->boolean('pauta')->default(false);
            $table->integer('scrap_state')->default('0');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('posts', function (Blueprint $table) {
            //
        });
    }
}
