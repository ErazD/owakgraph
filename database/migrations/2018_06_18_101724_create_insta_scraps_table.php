<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInstaScrapsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('insta_scraps', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('insta_profile_id')->unsigned();
            $table->timestamps();
            $table->foreign('insta_profile_id')->references('id')->on('insta_profiles');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('insta_scraps');
    }
}
