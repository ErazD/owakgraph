<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTwoFactorProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('two_factor_profiles', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('rank')->default(0); // 0:calificador, 1:juez 
            $table->integer('total_posts')->default(0);
            $table->string('ratings_type')->default(10000);
            $table->string('ratings_communication_task')->default(10000);
            $table->string('ratings_balance')->default(10000);
            $table->integer('ratings_product')->default(10000);
            $table->string('ratings_communication_theme')->default(10000);
            $table->integer('ratings_contest')->default(10000);            
            $table->string('ratings_influencer')->default(10000);
            $table->string('ratings_pauta')->default(10000);
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('two_factor_profiles');
    }
}
