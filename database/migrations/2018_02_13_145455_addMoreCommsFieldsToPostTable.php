<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMoreCommsFieldsToPostTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('posts', function (Blueprint $table) {
            $table->string('micro_moment')->nullable();
            $table->string('business_objective')->nullable();
            });

        Schema::table('youtube_videos', function (Blueprint $table) {
            $table->string('micro_moment')->nullable();
            $table->string('business_objective')->nullable();

            });
        Schema::table('two_factor_authentications', function (Blueprint $table) {
            $table->string('micro_moment')->nullable();
            $table->string('business_objective')->nullable();

            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('posts', function(Blueprint $table){
            $table->dropColumn('micro_moment');
            $table->dropColumn('business_objective');
        });

        Schema::table('youtube_videos', function(Blueprint $table){
            $table->dropColumn('micro_moment');
            $table->dropColumn('business_objective');
        });

        Schema::table('two_factor_authentications', function(Blueprint $table){
            $table->dropColumn('micro_moment');
            $table->dropColumn('business_objective');
        });
    }
}
