<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGroupIndustryRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('group_industry_roles', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('group_id')->unsigned();
            $table->integer('element_id')->unsigned()->nullable();
            $table->string('element_model')->default('Brand'); // nombre del modelo de Industria al que se relaciona (Vertical, Category, Canasta, Marca, Fanpage)
            $table->integer('permission_value')->default(0)->nullable(); // 0: ver, 1:modificar, 2:administrar(crear-eliminar)
            $table->timestamps();
            $table->foreign('group_id')->references('id')->on('groups')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('group_industry_roles');
    }
}
