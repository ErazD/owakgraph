<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOnlyOwakFieldToCanastasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('canastas', function (Blueprint $table) {
        $table->boolean('only_owak')->default(false);
    });
 }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('canastas', function(Blueprint $table){
            $table->dropColumn('only_owak');
        });
    }
}
