<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contests', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->text('content')->nullable();
            $table->string('prize')->nullable();
            $table->timestamp('since');
            $table->timestamp('until');
            $table->integer('brand_id')->unsigned()->nullable();
            $table->timestamps();
            $table->foreign('brand_id')->references('id')->on('brands');
        });

        Schema::create('contest_post', function(Blueprint $table){
            $table->increments('id');
            $table->integer('contest_id')->unsigned();
            $table->integer('post_id')->unsigned();
            $table->foreign('contest_id')->references('id')->on('contests')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('post_id')->references('id')->on('posts')->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contests');
        Schema::dropIfExists('contest_post');
    }
}
