<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTwoFactorAuthenticationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('two_factor_authentications', function (Blueprint $table) {
            $table->increments('id');
            $table->string('social_network');
            $table->integer('profile_id')->unsigned()->nullable();
            $table->integer('post_id')->unsigned();
            $table->boolean('principal')->default(false); #identifica la autentificación final

            $table->string('type')->nullable();
            $table->string('communication_task')->nullable();
            $table->string('balance')->nullable();
            $table->integer('product_id')->unsigned()->nullable();
            $table->string('communication_theme')->nullable();
            $table->integer('contest_id')->unsigned()->nullable();
            
            $table->string('influencer')->nullable();
            $table->string('pauta')->nullable();

            $table->timestamps();

            $table->foreign('profile_id')->references('id')->on('two_factor_profiles')->onDelete('set null')->onUpdate('cascade');
            $table->foreign('post_id')->references('id')->on('posts')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('product_id')->references('id')->on('products')->onDelete('set null')->onUpdate('cascade');
            $table->foreign('contest_id')->references('id')->on('contests')->onDelete('set null')->onUpdate('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('two_factor_authentications');
    }
}
