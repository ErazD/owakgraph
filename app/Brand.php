<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    protected $guarded = [];

    /*-------------------------------------------------------*
    * Obtener relación entre Marca y Fanpages
    * @return collection
    *-------------------------------------------------------*/
    public function fanpages()
    {
        return $this->hasMany(Fanpage::class);
    }

    /**
    * Obtiene el query de los posts pertenecientes a todas las fanpages de la marca.
    * @param {string} $country: nombre del páis del que se quieren pedir los posts, si se quieren pedir todos, $country='all'.
    * @return {Object}: Query con la unión de los posts de las fanpages.
    */
    public function posts($country="all"){
        $posts = null;
        $fpgs = $this->fanpages();
        if($country != "all")
            $fpgs = $fpgs->where('name', 'LIKE', '%'.$country.'%');
        foreach ($fpgs->get() as $k => $brnd) {
            $getPosts = $brnd->messyPosts();   
            if(sizeof($getPosts->get()) > 0 ){
            $posts = ($posts)? $posts->unionall($getPosts): $getPosts;
            }
        }
        return $posts;   
    }

    /**
    * Obtiene la lista de posts pertenecientes a la red social.
    * @param {string} $country: nombre del país a filtrar.
    * @param {string} $socialNetwork: nombre de la red social.
    * @param {Object} $filterObj: Es obtenido de MethodController::getFilterPostsObject, y es usuado para los filtros que se aplicarán a los posts.
    * @return {Collection<Mixed>}: Lista con los posts de diferentes redes sociales (No es Query)
    */
    public function snPosts($country="all", $socialNetwork="all", $filterObj=null){
        $snPosts = collect();
        $posts = null;
        if ($socialNetwork == 'all' || strtolower($socialNetwork) == 'facebook') {
            $fpgs = $this->fanpages();
            if($country != "all")
                $fpgs = $fpgs->where('name', 'LIKE', '%'.$country.'%');
            foreach ($fpgs->get() as $k => $fpg) {
                $fpgPosts = $fpg->messyPosts();
                if($filterObj)
                    $fpgPosts = $filterObj->filterPosts($fpgPosts, 'posts');
                $posts = ($posts)? $posts->union($fpgPosts): $fpgPosts;
            }
            if($posts){
                $snPosts = $snPosts->concat($posts->get());
            }
        }
        // dd('BRAND', $posts->get(), $snPosts);
        $posts = null;
        if ($socialNetwork == 'all' || strtolower($socialNetwork) == 'youtube') {
            $chns = $this->channels();
            if($country != "all")
                $chns = $chns->where('name', 'LIKE', '%'.$country.'%');
            foreach ($chns->get() as $k => $chn) {
                $chnPosts = $chn->messyPosts();
                if($filterObj)
                    $chnPosts = $filterObj->filterPosts($chnPosts, 'youtube_videos');
                $posts = ($posts)? $posts->merge($chnPosts): $chnPosts;
            }
            if($posts){     
             $snPosts = $snPosts->merge($posts->get());
         }
     }
      $posts = null;
    
    if ($socialNetwork == 'all' || strtolower($socialNetwork) == 'instagram') {
            $prfls = $this->profiles();
            if($country != "all")
                $prfls = $prfls->where('name', 'LIKE', '%'.$country.'%');
            foreach ($prfls->get() as $k => $prfl) {
                $prflPosts = $prfl->messyPosts();
                if($filterObj)
                    $prflPosts = $filterObj->filterPosts($prflPosts, 'instagram_posts');
                $posts = ($posts)? $posts->merge($prflPosts): $prflPosts;
            }
            if($posts){     
             $snPosts = $snPosts->merge($posts->get());
         }
     }

     return $snPosts;   
 }


    /**
    * Obtiene el query de los posts pertenecientes a las fanpages de una marca dentro de un arreglo.
    * @return {Array<Object>}: arreglo de que contiene los queries de posts.
    */
    public function postsArrayQueries(){
        $posts = [];
        foreach ($this->fanpages as $k => $fpg) {
            $posts =array_merge($posts , $fpg->postsArrayQueries());
        }
        return $posts;   
    }

    /**
    * Retorna el límite asignado para el scrap de los posts
    * @return {int} límite seleccionado del post.
    */
    public function selectedScrap(){
        return $this->scrap_type;
    }

    /*-------------------------------------------------------*
    * Obtener relación entre Marca y canales de YouTube
    * @return collection
    *-------------------------------------------------------*/
    public function channels()
    {
        return $this->hasMany(YoutubeChannel::class);
    }
     public function profiles()
    {
    return $this->hasMany(InstaProfile::class);
    }
    public function products(){    
        return $this->hasMany(Product::class);
    }
    /*-------------------------------------------------------*
    * Obtener relación entre una marca y la canasta a la que pertenece.
    * @return Object
    *-------------------------------------------------------*/
    public function canastas(){
        return $this->belongsToMany(Canasta::class,'canasta_brand','brand_id','canasta_id');
    }
    /**
    * @return {Object}: Relación entre la marca y los permisos que le han sido asignados.
    */
    public function industryRoles(){
        return $this->hasMany(GroupIndustryRole::class, 'element_id', 'id')->where('element_model', 'Brand');
    }

    /*-------------------------------------------------------*
    * Eliminar las fanpages asociadas a una Marca
    * @return Brand
    *-------------------------------------------------------*/
    public function delete()
    {
        $this->fanpages()->delete();
        $this->channels()->delete();
        $this->profiles()->delete();
        return parent::delete();
    }

}
