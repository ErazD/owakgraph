<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExcelImport extends Model
{
    //
     protected $guarded = [];

      public function product(){
        return $this->belongsTo(Product::class);
     }

         public function post(){
        return $this->belongsTo(Post::class);
     }
}
