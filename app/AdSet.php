<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdSet extends Model
{
    protected $guarded = [];

    public function posts(){
    	return $this->belongsToMany(Post::class,'post_ad_set','ad_set_id','post_id');
    }
    public function groups(){
        return $this->belongsToMany(Group::class,'group_ad_set','ad_set_id','group_id');
    }

    /**
    * agrega o elimina un elemento (usuario o post) al adset.
    * @param {string} $tipo: nombre del tipo del elemento a agregar.
    * @param {int} $id: id del elemento a agregar.
    * @param {string} $action: acción a ejecutar, ('agregar', 'eliminar', 'cambiar').
    * @return 0: no se pudo realizar la operación, 1: se ha agregado el elemento, -1: se ha eliminado el elemento.
    */
    public function add($type, $id, $action="agregar"){
        $ids = [$id];
        if(strpos($id, ',') > 0)
            $ids = explode(',', $id);
        $queryId = ($type=='post')? 'posts.id': 'users.id';
        $list = $this->getList($type);
        $statusMessage = "No se realizó la operación";
        if($list){
            foreach ($ids as $key => $idActual) {
                switch ($action) {
                    case 'agregar':
                    if(sizeof((clone $list)->where($queryId, $idActual)->get()) == 0){
                        $list->attach($idActual);
                        $statusMessage = "El ".$type." se ha agregado con éxito";
                    }
                    break;
                    case 'eliminar':
                    $list->detach($idActual);
                    $statusMessage = "El ".$type." se ha eliminado con éxito";
                    break;
                    case 'cambiar':
                    if(sizeof((clone $list)->where($queryId, $idActual)->get())>0){
                       $list->detach($idActual);
                       $statusMessage = "Se ha eliminado el ".$type;
                   }else{
                       $list->attach($idActual);
                       $statusMessage = "Se ha agregado el ".$type;
                   }
                   break;            
                   default:
                   break;
               }
           }
        }else{
            $statusMessage = "No se encuentra una lista de ".$type;
        }
        return $statusMessage;
    }

    /**
    * Obtiene la lista relacionada al identificador dado.
    * @param {string} $type: identificador de los grupos por defecto del AdSet.
    * @return {Object} Lista relacionada al identificador.
    */
    private function getList($type){
        $list = false;
        switch (strtolower($type)) {
            case 'post':
            $list = $this->posts();
            break;
            case 'reader':
            $list = $this->readers()->users();
            break;
            case 'editor':
            $list = $this->editors()->users();
            break;
            case 'admin':
            $list = $this->admins()->users();
            break;
            default:
            $list = false;
            break;
        }
        return $list;
    }
    /**
    * Agrega posts a la lista de posts.
    * @param {...App\Post} $posts: posts a agregar
    * @return {Object} lista de posts del adset.
    * Postcondition: se han agregado nuevos posts a la lista.
    */
    public function addPost(Post ...$posts){
    	$allPosts = $this->posts();
    	foreach ($posts as $k => $post) {
    		$allPosts->attach($post);
    	}
    	return $allPosts;
    }
    /**
    * Elimina posts de la lista de posts.
    * @param {...App\Post} $posts: posts a eliminar.
    * @return {Object} lista de posts del adset.
    * Postcondition: se han eliminado posts a la lista.
    */
    public function removePost(Post ...$posts){
        $allPosts = $this->posts();
        $allPosts->detach($posts);
        return $allPosts;
    }    
    /**
    * Obtiene un grupo de usuarios con el nivel de permiso dado. si no existe, crea uno.
    * @param {int} $levelPermission: nivel de permiso del grupo buscado.
    * @param {string} $levelTitle: Nombre del título para el grupo a crear.
    * @return {App\Group}: grupo con el permiso buscado.
    */
    public function getGroup($levelPermission, $levelTitle=''){
    	$group = $this->groups()->wherehas('industryPremissions', function($query) use ($levelPermission){
    		$query->where('element_model', 'AdSet')->where('element_id', $this->id)->where('permission_value', $levelPermission);
    	})->first();
    	if(!$group){
            $g_title = ($levelTitle =='')? $levelPermission: $levelTitle;
            $group = Group::create([
                'name' => $this->name.' permission '.$g_title,
                'description' => 'Grupo automático para usuarios del AdSet "'.$this->name.'" con nivel de permiso ('.$g_title.').',
            ]);
            $gIR = GroupIndustryRole::firstOrCreate([
                'element_model' => 'AdSet',
                'element_id' => $this->id,
                'group_id' => $group->id,
                'permission_value' => $levelPermission,
            ]);
        }
        if(!$this->groups()->where('groups.id',$group->id)->first())
            $this->groups()->attach($group);
        return $group;
    }
    /**
    * @return {App\Group}: Grupo por defecto para usuarios con permisos de administrar.
    */
    public function admins(){
        return $this->getGroup(10, '10 "Administrar"');
    }
    /**
    * @return {App\Group}: Grupo por defecto para usuarios con permisos de editar.
    */
    public function editors(){
        return $this->getGroup(1, '1 "Editar"');
    }
    /**
    * @return {App\Group}: Grupo por defecto para usuarios con permisos de visualizar.
    */
    public function readers(){
        return $this->getGroup(0, '0 "Ver"');
    }    
    /**
    * Crea un nuevo Adset
    * @param {int} $user_id: id del usuario que crea el AdSet.
    * @param {string} $name: nombre del AdSet.
    * @param {string} $description: descripción del AdSet.
    * Postcondition: se ha creado un nuevo AdSet, y se ha agregado el usuario que lo creó a su lista de administradores.
    */
    public static function createAdset($user_id, $name='Nuevo AdSet', $description=null){
        $vals=['name' => $name];
        if($description)
            $vals['description'] = $description;
        $adset = AdSet::create($vals);
        $adset->admins()->users()->attach($user_id);
    }
     /**
    * Consulta los adsets delusuario dado.
    * @param {App\User} $user: usuario de que se quieren los adsets.
    * @param {int} $level: nivel mínimo a buscar, si es null retorna todos.
    * @return {}: lista de adsets del usuario.
    */
    public static function getAdsetsFromUser(User $user, $level=null){
        return AdSet::getAdsetsFromUserId($user->id, $level)->get();
    }
    /**
    * Consulta los adsets delusuario dado.
    * @param {int} $user_id: id del usuario activo.
    * @param {int} $level: nivel mínimo a buscar, si es null retorna todos.
    * @return {}: lista de adsets del usuario. 
    */
    public static function getAdsetsFromUserId($user_id, $level=null){
        $list = AdSet::whereHas('groups', function($queryG) use ($user_id, $level){
            if($level!==null){
                $queryG = $queryG->whereHas('industryPremissions', function($queryI) use ($level){
                    $queryI->where('permission_value', '>=', $level);
                });
            }
            $queryG->whereHas('users', function($queryU) use($user_id){
                $queryU->where('users.id', $user_id);
            });
        });
        // dd($list->get()->toArray());
        return $list;
    }
    /**
    * Filtra la lista de adsests para saber a cuales pertenece el post dado.
    * @param {int} $user_id: id del usuario activo.
    * @param {int} $post_id: ID del post a comparar dentro de los adsets.
    * @param {} $list: lista de adsets a comparar.
    * @return {}: lista de adsets a los que contienen el post. 
    */
    public static function getAdsetsUserPost($user_id, $post_id, $list=null){
        if($list == null)
            $list = AdSet::getAdsetsFromUserId($user_id);
        $list = $list->whereHas('posts', function($query) use($post_id){
            $query->where('posts.id', $post_id);
        });        
        return $list->get();
    }
    /**
    * Filtra la lista de adsests para saber a cuales no pertenece el post dado.
    * @param {int} $user_id: id del usuario activo.
    * @param {int} $post_id: ID del post a comparar dentro de los adsets.
    * @param {} $list: lista de adsets a comparar.
    * @return {}: lista de adsets a los que el post se puede agregar. 
    */
    public static function getAdsetsUserNotPost($user_id, $post_id, $list=null){
        if($list == null)
            $list = AdSet::getAdsetsFromUserId($user_id);
        $list = $list->whereDoesntHave('posts', function($query) use($post_id){
            $query->where('posts.id', $post_id);
        });        
        return $list->get();
    }
}