<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class YoutubeChannel extends Model
{
	protected $guarded = [];

     /*-------------------------------------------------------*
     * Obtener relación entre el canal de youtube y sus videos
     * @return collection
     *-------------------------------------------------------*/
     public function posts()
     {
     	return $this->hasMany(YoutubeVideo::class)->orderBy('published', 'DESC');
     }

      public function messyPosts()
     {
          return $this->hasMany(YoutubeVideo::class);
     }
     public function snPosts($country="all", $socialNetwork="all", $filterObj=null){
      $snPosts = collect();
      if (($socialNetwork == 'all' || strtolower($socialNetwork) == strtolower($this->social_network)) &&
          ($country == "all" || strtolower($country) == strtolower($this->name)))
          $snPosts = ($filterObj)? $filterObj->filterPosts($this->messyPosts(), 'posts')->get(): $this->messyPosts()->get();
      return $snPosts;
     }


     /*-------------------------------------------------------*
     * Obtener relación entre el canal de youtube y su parent brand.
     * @return Object
     *-------------------------------------------------------*/
     public function brand(){
     	return $this->belongsTo(Brand::class);
     }

      /**
     * Obtiene la Relación con los permisos que han sido asociados a su marca.
     * @return Object
     */
     public function industryRoles(){
        return $this->brand->industryRoles(); 
     }
       /**
      * Retorna el límite asignado para el scrap de los posts
      * @return {int} límite seleccionado del post o límite en el elemento que lo contiene.
      */
      public function selectedScrap(){
          if($this->scrap_type !== null)
               return $this->scrap_type;
          else 
               return $this->brand->selectedScrap();
      }

 }
