<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Canasta extends Model
{
    protected $guarded = [];

  	/*-------------------------------------------------------*
     * Obtener relación entre Canasta y marcas.
     * @return collection
     *-------------------------------------------------------*/
    public function brands()
    {
     return $this->belongsToMany(Brand::class,'canasta_brand','canasta_id','brand_id');
    }
    /**
    * Obtiene la lista de marcas pertenecientes a la canasta a las que un usuario tiene acceso.
    * @param {App\User} $user: usuario del que se comprobará el acceso.
    * @return {Object}: Query con las marcas obtenidas.
    */
     public function userbrands(User $user){
        return GroupIndustryRole::getUserPermissionsElement($user, 'Brand')->whereHas('canastas', function($query){
            $query->where('canastas.id', $this->id);
        });
    }
    /**
    * Obtiene el query de los posts pertenecientes a todas las marcas de una canasta.
    * @param {string} $country: nombre del páis del que se quieren pedir los posts, si se quieren pedir todos, $country='all'.
    * @return {Object}: Query con la unión de los posts de las marcas.
    */
     public function posts($country="all"){

        $posts = null;

        foreach ($this->brands as $k => $brnd) {
        $getPosts = $brnd->posts($country);
        if($getPosts){
            $posts = ($posts)? $posts->union($getPosts): $getPosts;
        }
        }
        return $posts;   
    }

    /**
    * Obtiene la lista de posts pertenecientes a la red social.
    * @param {string} $country: nombre del país a filtrar.
    * @param {string} $socialNetwork: nombre de la red social.
    * @param {Object} $filterObj: Es obtenido de MethodController::getFilterPostsObject, y es usuado para los filtros que se aplicarán a los posts.
    * @return {Collection<Mixed>}: Lista con los posts de diferentes redes sociales (No es Query)
    */
    public function snPosts($country="all", $socialNetwork="all", $filterObj=null){
        $posts = null;
        foreach ($this->brands as $k => $brnd) {
            $brndPosts = $brnd->snPosts($country, $socialNetwork, $filterObj);
            if($brndPosts)
                $posts = ($posts)? $posts->union($brndPosts): $brndPosts;
        }
        return $posts;
    }

   /**
    * Obtiene el query de los posts pertenecientes a las marcas de una canasta dentro de un arreglo.
    * @return {Array<Object>}: arreglo de que contiene los queries de posts.
    */
    public function postsArrayQueries(){
        $posts = [];
        foreach ($this->brands as $k => $brnd) {
            $posts =array_merge($posts , $brnd->postsArrayQueries());
        }
     return $posts;   
    }

    /*-------------------------------------------------------*
     * Obtener relación entre una categoría y sus segmentos.
     * @return collection
     *-------------------------------------------------------*/
    public function segments()
    {
     return $this->hasMany(Segmento::class);
    }
    public function segmentos()
    {
        return $this->segments();
    }

    /*-------------------------------------------------------*
     * Obtener relación entre un segmento y la categoría a la que pertenece.
     * @return Object
     *-------------------------------------------------------*/
     public function category(){
        return $this->belongsTo(Categoria::class);
     }

     public function categoria(){
         return $this->belongsTo(Categoria::class);
     }

     /**
     * Obtiene la Relación entre una canasta y los permisos que han sido asociados a sus marcas.
     * @return Object
     */
     public function industryRoles(){
        $industryRoles = null;
        foreach ($this->brands as $k => $brnd) {
            $industryRoles = ($industryRoles)? $industryRoles->union($brnd->industryRoles()): $brnd->industryRoles();
        }
        return $industryRoles; 
     }
    
}
