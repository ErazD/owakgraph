<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\InstaPost as InstaPost;

class InstaMetrics extends Model
{
    //
	protected $guarded = [];


	public function addMetric($metric){
		 
		$post = InstaPost::where('post_id', $metric->post_id)->first();
		$this->create([
			'insta_post_id' => $post->id,
			'likes' => $metric->likes,
			'views' => $metric->views,
			'comments' => $metric->comments
			]);
	}
}
