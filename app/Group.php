<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    protected $guarded = [];

/*-------------------------------------------------------*
* Obtener relación entre un grupo y sus usuarios.
* @return Object
*-------------------------------------------------------*/
public function users(){
    return $this->belongsToMany(User::class,'group_user','group_id','user_id');
}

/*-------------------------------------------------------*
* Obtener relación entre un grupo y sus permisos.
* @return Object
*-------------------------------------------------------*/
public function permissions(){
    return $this->hasMany(UserRole::class, 'element_id')->where('element_model', 'Group');
    // return UserRole::where('element_model', 'Group')->where('element_id', $this->id);
}

/*-------------------------------------------------------*
* Obtener relación entre un grupo y sus permisos de marca.
* @return Object
*-------------------------------------------------------*/
public function industryPremissions(){
    return $this->hasMany(GroupIndustryRole::class);
}

/**
* Busca si un elemento pertenece a la lista dada (lista de usuarios, permisos o marcas del grupo).
* @param {string} $type: Tipo de elemento que se va a buscar. ('user', 'permission', 'brand').
* @param {string} $slug: Identificador el elemento a buscar. {id o valor del permiso}
* @param {int} $level: Nivel del permiso a buscar.
* @return {boolean}: true si se encuentra al menos un elemento que cumpla las condiciones dadas, false en caso contrario.
*/
public function existElement($type, $slug, $level=0){
    switch (strtolower($type)) {
        case 'user':
        return sizeof($this->users()->where('users.id', $slug)->get()) > 0;
        break;
        case 'permission':
        return sizeof($this->permissions()->where('permission', $slug)->where('value', $level)->get()) > 0;
        break;
        case 'brand':
        return sizeof(GroupIndustryRole::getGroupLevelPermissionListByIDs($this->id, $slug, 'Brand')->where('permission_value', $level)->get()) > 0;        
        break;
    }
    return false;
}

/**
* Agrega un usuaurio al grupo.
* @param {int} $user_id: id del usuario a agregar.
* @return {int}: 0: si el usuario ya pertenecía al grupo, 2: si se creó el usuario.
* Postcondition: se ha agregado un usuario a la lista.
*/
public function addUser($user_id){
    if(!$this->existElement('user', $user_id)){
        $this->users()->attach($user_id);
        return 2;
    }
    return 0;
}

/**
* Elimina un usuaurio del grupo.
* @param {int} $user_id: id del usuario a eliminar.
* @return {int}: 1: indica que el usuario ha sido eliminado.
* Postcondition: se ha eliminado un usuario de la lista.
*/
public function removeUser($user_id){
    $this->users()->detach($user_id);
    return 1;
}

/**
* Elimina un usuaurio del grupo si existe, en caso contrario lo agrega.
* @param {int} $user_id: id del usuario.
* @return {int}: 1: si se ha eliminado el usuario, 2: si se ha agregado el usuario.
* Postcondition: Se ha modificado la lista de usuarios. 
*/
public function changeUser($user_id){
    if($this->existElement('user', $user_id)){
        $this->users()->detach($user_id);
        return 1;
    }else{
        $this->users()->attach($user_id);
        return 2;
    }
}

/**
* Agrega un permiso al grupo.
* @param {string} $permission_slug: slug del permiso a agregar.
* @param {int} $level: nivel del permiso.
* @return {int}: 2: indica que se ha agregado el permiso.
* Postcondition: se ha agregado o modificado un permiso en la lista.
*/
public function addPermission($permission_slug, $level=0){
    UserRole::addPermissions($this, 'Group', [['permission'=>$permission_slug, 'value'=>$level]]);
    return 2;
}

/**
* Elimina un permiso del grupo.
* @param {string} $permission_slug: slug del permiso a eliminar.
* @param {int} $level: nivel del permiso.
* @return {int}: 1: indica que se ha eliminado el permiso.
* Postcondition: se ha agregado o modificado un permiso en la lista.
*/
public function removePermission($permission_slug, $level=0){
    UserRole::removePermissions($this, 'Group', [['permission'=>$permission_slug, 'value'=>$level]]);
    return 1; 
}

/**
* Elimina un permiso del grupo si existe, lo agrega en caso contrario.
* @param {string} $permission_slug: slug del permiso.
* @param {int} $level: nivel del permiso.
* @return {int}: 1: si se ha eliminado el permiso, 2: si se ha agregado el permiso.
* Postcondition: Se ha modificado la lista de permisos.
*/
public function changePermission($permission_slug, $level=0){
    if($this->existElement('permission', $permission_slug, $level)){
        return $this->removePermission($permission_slug, $level);
    }else{
        return $this->addPermission($permission_slug, $level);
    }
}

/**
* Agrega una marca a los permisos del grupo.
* @param {int} $brand_id: id de la marca a agregar.
* @param {int} $level: nivel del permiso.
* @return {int}: 0: si la marca ya pertenecía al grupo, 2: si se creó la marca.
* Postcondition: se ha agregado una marca a la lista.
*/
public function addIndustry($brand_id, $level=0){
    if(!$this->existElement('brand', $brand_id, $level)){
        GroupIndustryRole::addPermissionGroup($this, $brand_id, 'Brand', $level);
        return 2;
    }
    return 0;
}

/**
* Elimina una marca de los permisos del grupo.
* @param {int} $brand_id: id de la marca a eliminar.
* @param {int} $level: nivel del permiso.
* @return {int}: 1: indica que se ha eliminado el permiso.
* Postcondition: se ha elimnado una marca de la lista.
*/
public function removeIndustry($brand_id, $level=0){
    GroupIndustryRole::removePermissionsGroup($this, $brand_id, 'Brand', $level);
    return 1;
}

/**
* Elimina una marca de los permisos del grupo si existe, la agrega en caso contrario.
* @param {int} $brand_id: id de la marca.
* @param {int} $level: nivel del permiso.
* @return {int}: 1: si se ha eliminado la marca, 2: si se ha agregado la marca.
* Postcondition: Se ha modificado la lista de marcas.
*/
public function changeIndustry($brand_id, $level=0){
     if($this->existElement('brand', $brand_id, $level)){
        return $this->removeIndustry($brand_id, $level);
    }else{
        return $this->addIndustry($brand_id, $level);
    }
}

/**
* Busca el método que se debe llamar según el nombre d ela lista a acceder y la acción a realizar.
* @param {string} $listName: nombre de la lista a buscar.
* @param {string} $action: nombre de la acción a realizar.
* @return {string}: nombre del método que realiza la acción para el método, retorna null si no encuentra el método.
*/
public function getMethodName($listName, $action){
    $methodName = '  '; 
    switch (strtolower($action)) {
     case 'agregar':
     $methodName = 'add';
     break;
     case 'eliminar':
     $methodName = 'remove';
     break;
     case 'cambiar':
     $methodName = 'change';
     break;        
     default:
     return null;
     break;
 }
 switch (strtolower($listName)) {
    case 'user':
    $methodName .= 'User';
    break;
    case 'permission':
    $methodName .= 'Permission';
    break;
    case 'brand':
    $methodName .= 'Industry';
    break;
    default:
    return null;
    break;
}
return $methodName;
}

/**
* Agrega un elemento al grupo (permiso, industria o usuario).
* @param {string} $listName: Nombre de la lista a agregar.
* @param {string}/{array<string>} $element: id/slug o lista de id/slug a agregar.
* @param {string} $action: acción a ejecutar, ('agregar', 'eliminar', 'cambiar').
* @param {int} $level: Nivel del permiso.
* @return {int/array}: Respuesta para cada elemento.  -2: no existe la lista, -1: no se puso realizar la operación completa, 0: no se necesita realizar la operación, 1: se ha eliminado, 2: se ha agregado.
*/
public function addElement($listName, $element, $action, $level=0){
    $methodName = $this->getMethodName($listName, $action);
    if($methodName == null)
        return -2;
    $resp = -1;
    if(is_array($element)){
        $resp = [];
        foreach ($element as $key => $ele) {
            $r = $this->$methodName($ele, $level);
            array_push($resp, $r);
        }
    }else{
        $resp = $this->$methodName($element, $level);
    }
    return $resp;
}

}
