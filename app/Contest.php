<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contest extends Model
{
    protected $guarded = [];

    	/*-------------------------------------------------------*
     * Obtener relación entre el concurso y los posts de facebook.
     * @return collection
     *-------------------------------------------------------*/
    public function posts()
    {
     return $this->belongsToMany(Brand::class,'contest_post','contest_id','post_id');
    }

     public function brand()
	 {
	 	return $this->belongsTo(Brand::class);
	 }

}
