<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class YoutubeMetrics extends Model
{

	protected $guarded = [];

	 /*-------------------------------------------------------*
     * Obtener relación entre las métricas y su parent YoutuveVideo.
     * @return Object
     *-------------------------------------------------------*/
	 public function post(){
	 	return $this->belongsTo(YoutubeVideo::class);
	 }

	 /*-------------------------------------------------------*
     * Añadir Métrica.
     *-------------------------------------------------------*/
	 public function addMetric($video, $video_id)
	 {
	 	//Obtener la ultima métrica del video.
	 	$last_metric = $this->where('youtube_video_id', $video_id)->latest()->first();
	 	$last_exist = !is_null($last_metric);
	 	//Si el resultado es nulo o los datos almacenados en la BD son diferentes a los actuales, se crea un nuevo record
	 	$new_metric = null;
	 		if(!$last_exist || $last_metric->views != $video['views'] || $last_metric->comments != $video['comments'] || $last_metric->likes != $video['likes'] || $last_metric->dislikes != $video['dislikes']){
	 			$new_metric = $this->create([
	 				'youtube_video_id' => $video_id,
	 				'views' => $video['views'],
	 				'comments' => $video['comments'],
	 				'likes' => $video['likes'],
	 				'dislikes' => $video['dislikes'],
	 			]);
	 			
	 		}
	 		if($last_exist && !is_null($new_metric)){
	 			return $this->getScrap_type($last_metric, $new_metric);
	 		}else if($last_exist){
	 			// Retornar otro valor de scrapt dependiendo de hace cuanto no hay cambios 
	 			return 0;
	 		}
	 		return 0;
	 }
	 /**
	 * Calcula el tipo de Scrap del post, que es el indicador para filtrar los post que se actualian según el varlo dado. 
	 * 0:hora, 1:diario, 2:semanal, 3:mensual(publicaciones de más de un año), 4:anual, -1:no se actualiza
	 *
	 * @return tipo de scrap
	 */
	 public function getScrap_type($last, $actual){
	 	$varViews = $actual->views - $last->views;
	 	$varLikes = $actual->likes - $last->likes;
	 	$varDislikes = $actual->dislikes - $last->dislikes;
	 	$varComments = $actual->comments - $last->comments;

	 	$scrap_type = 0;
	 	return $scrap_type;

	 }


	}
