<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;
use \SammyK\LaravelFacebookSdk\LaravelFacebookSdk as Facebook;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(Facebook $fb)
    {
        $login_url = $fb->getLoginUrl(['email']);
        View::share('login_url', $login_url);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
