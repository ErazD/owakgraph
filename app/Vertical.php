<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vertical extends Model
{
    protected $guarded = [];

    /*-------------------------------------------------------*
    * Obtener relación entre una vertical y sus categorías.
    * @return collection
    *-------------------------------------------------------*/
    public function categories()
    {
        return $this->hasMany(Categoria::class, 'vertical_id');
    }
    public function categorias()
    {
        return $this->categories();
    }

    /**
    * Obtiene la lista de categorías perrtenecientes a una vertical a las que un usuario tiene acceso.
    * @param {App\User} $user: usuario del que se comprobará el acceso.
    * @return {Object}: Query con las categorías obtenidas.
    */
    public function usercategories(User $user){
        return GroupIndustryRole::getUserPermissionsElement($user, 'Categoria')->where('categorias.vertical_id', $this->id);
    }

    /**
    * Obtiene el query de los posts pertenecientes a todas las categorías de una vertical.
    * @param {string} $country: nombre del páis del que se quieren pedir los posts, si se quieren pedir todos, $country='all'.
    * @return {Object}: Query con la unión de los posts de las categorías.
    */
    public function posts($country="all"){
        $posts = null;
        foreach ($this->categories as $k => $ctg) {
            $posts = ($posts)? $posts->concat($ctg->posts($country)): $ctg->posts($country);
        }
        return $posts;   
    }

    /**
    * Obtiene la lista de posts pertenecientes a la red social.
    * @param {string} $country: nombre del país a filtrar.
    * @param {string} $socialNetwork: nombre de la red social.
    * @return {Collection<Mixed>}: Lista con los posts de diferentes redes sociales (No es Query)
    */
    public function snPosts($country="all", $socialNetwork="all"){
           $posts = null;
        foreach ($this->categories as $k => $ctg) {
            $posts = ($posts)? $posts->concat($ctg->snPosts($country, $socialNetwork)): $ctg->snPosts($country, $socialNetwork);
        }
        return $posts; 
    }

    /**
    * Obtiene el query de los posts pertenecientes a las categorías de una vertical dentro de un arreglo.
    * @return {Array<Object>}: arreglo de que contiene los queries de posts.
    */
    public function postsArrayQueries(){
        $posts = [];
        foreach ($this->categories as $k => $ctg) {
            $posts =array_merge($posts , $ctg->postsArrayQueries());
        }
        return $posts;   
    }

    /**
    * Obtiene la Relación entre una vertical y los permisos que han sido asociados a sus marcas.
    * @return Object
    */
    public function industryRoles(){
        $industryRoles = null;
        foreach ($this->categories as $k => $ctg) {
            $industryRoles = ($industryRoles)? $industryRoles->union($ctg->industryRoles()): $ctg->industryRoles();
        }
        return $industryRoles; 
    }

}
