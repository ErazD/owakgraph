<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Segmento extends Model
{
     protected $guarded = [];
       
  /*-------------------------------------------------------*
     * Obtener relación entre una marca y la canasta a la que pertenece.
     * @return Object
     *-------------------------------------------------------*/
     public function canasta(){
        return $this->belongsTo(Canasta::class);
     }

      public function products()
    {
     return $this->hasMany(Product::class);
    }
    
}
