<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InstaProfile extends Model
{
    //
	protected $guarded = [];

      /*-------------------------------------------------------*
     * Obtener relación entre Fanpage y sus posts
     * @return collection
     *-------------------------------------------------------*/
      public function posts()
      {
      	return $this->hasMany(InstaPost::class)->orderBy('published', 'DESC');
      }

       public function messyPosts()
      {
        return $this->hasMany(InstaPost::class);
      }

         public function snPosts($country="all", $socialNetwork="all", $filterObj=null){
      $snPosts = collect();
      if (($socialNetwork == 'all' || strtolower($socialNetwork) == strtolower($this->social_network)) &&
          ($country == "all" || strtolower($country) == strtolower($this->name)))
          $snPosts = ($filterObj)? $filterObj->filterPosts($this->messyPosts(), 'insta_posts')->get(): $this->messyPosts()->get();
      return $snPosts;
     }

      public function postsArrayQueries()
      {
      	return [$this->posts()];
      }
     /*-------------------------------------------------------*
     * Obtener relación entre Fanpage y su parent brand.
     * @return Object
     *-------------------------------------------------------*/
     public function brand(){
     	return $this->belongsTo(Brand::class);
     }


     /**
     * Obtiene la Relación con los permisos que han sido asociados a su marca.
     * @return Object
     */
     public function industryRoles(){
        return $this->brand->industryRoles(); 
     }

     public  function getFanCount()
    {
      return FanCount::where('table', 'insta_profiles')->where('element_id', $this->id);
    }

    public function getLastFanCount()
    {
      return FanCount::where('table', 'insta_profiles')->where('element_id', $this->id)->latest();
    }
     
      /**
      * Retorna el límite asignado para el scrap de los posts
      * @return {int} límite seleccionado del post o límite en el elemento que lo contiene.
      */
      public function selectedScrap(){
          if($this->scrap_type !== null)
               return $this->scrap_type;
          else 
               return $this->brand->selectedScrap();
      }
 }
