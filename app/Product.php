<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
  protected $guarded = [];

   public function segmento()
	 {
	 	return $this->belongsTo(Segmento::class);
	 }

	 public function brand()
	 {
	 	return $this->belongsTo(Brand::class);
	 }
	 /**
	 * @return {string}: nombre del segmento al que pertenece el producto y el nombre del producto separados por un guión (' - ').
	 */
	 public function getSegementProductname(){
	 	$spName = $this->name;
	 	$seg = $this->segmento;
	 	if($seg){
	 		$spName = $seg->name.' - '.$spName;
	 	}else{
	 		$spName = $spName.' - '.$spName;
	 	}
	 	return $spName;
	 }

}
