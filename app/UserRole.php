<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Config;

class UserRole extends Model
{
    protected $guarded = [];
    
    /**
    * @return {mixed} Elemento al que se le ha asignado el permiso. (App\User o App\Group)
    */
    public function element(){
    	// return $this->element_model::where('id', $this->elment_id)->first();

        return $this->belongsTo('App\\'.$this->element_model, 'element_id');
    }

    /**
    * Agrega los permisos básicos a un elemento que puede ser App\User o App\Group.
    * @param {mixed} $element: instancia del elemento al que se agregará el permiso, puse ser un usuario o un grupo.
    * @param {string} $model: nombre de la clase a la que pertenece el elemento.
    * Postcondition: se han agregado permisos al elemento.
    */
    public static function addBasicPermissions($element, $model='User'){
    	$per = Config::get('constants.user_roles.permission');
    	$permissions = [
    		UserRole::getPermissionInfoAnrray($per['user']['slug']), 
    		// UserRole::getPermissionInfoAnrray($per['two_factor']['slug'])
    	];
    	UserRole::addPermissions($element, $model, $permissions);
    }

    /**
    * Agrega los permisos contenidos en una lista a un objeto seleccionado (Usuario o grupo).
    * @param Model $element: objeto al que se le asignarán los permisos.
    * @param String $model: nombre del modelo al que pertenece element.
    * @param Array<Array<String>> permissions: lista con los slgus de los perimisos que se agregarán. [['permission'=>permiso1, 'value'=>valor1], ['permission'=>permiso2, 'value'=>valor2]])
    */
    public static function addPermissions($element, $model='User', $permissions=[]){
    	foreach ($permissions as $k => $per) {
    		$userRole = UserRole::firstOrCreate([
                'element_model' => $model ,
                'element_id' => $element->id,
                'permission' => $per['permission'],
            ]);
    		$userRole->permission_value = $per['value'];
    		$userRole->save();
    	}
    }

     /**
    * Elimina los permisos contenidos en una lista a un objeto seleccionado (Usuario o grupo).
    * @param Model $element: objeto al que se le removerán los permisos.
    * @param String $model: nombre del modelo al que pertenece element.
    * @param Array<Array<String>> permissions: lista con los slgus de los perimisos a eliminar. [['permission'=>permiso1, 'value'=>valor1], ['permission'=>permiso2, 'value'=>valor2]])
    */
     public static function removePermissions($element, $model='User', $permissions=[]){
        foreach ($permissions as $k => $per) {
            $userRole = UserRole::where('element_model', $model)
            ->where('element_id', $element->id)
            ->where('permission', $per['permission'])
            ->where('permission_value', $per['value'])
            -> first();
            if($userRole)
                $userRole->delete();
        }
    }
    /**
    * Obtiene un arrglo con la información del permiso que se quiere crear.
    * @param {string} $permission: slug del permiso.
    * @param {int} $value: valor del permiso.
    * @return {array} arreglo con la infromación del permiso. 
    */
    public static function getPermissionInfoAnrray($permission, $value=0){
      return ['permission' => $permission, 'value' => $value];
  }

  /**
  * Obtiene todos los permisos asignados a un grupo.
  * @param {App\Group} $group: grupo del que se consultan los permisos.
  * @return {object}: relacion de todos los permisos asignados al grupo.
  */
  public static function getGroupPermission(Group $group){
    return UserRole::where('element_model', 'Group')->where('element_id', $group->id);  
}

  /**
  * Obtiene los permisos asignados a un usuario, y a los grupos a los que pertenece.
  * @param {App\User} $user: usuario al que se consultarán los permisos.
  * @return {object} lista de permisos relacionados al usuario.
  */
  public static function getUserPermissions(User $user){
        // Agregar permisos básicos a todos los usuarios con rank1
        // $users = User::where('rank',1)->get();
        // foreach ($users as $key => $value) {
        // 	    UserRole::addBasicPermissions($value);
        // }
    $perms = UserRole::where(function($query) use ($user){
        $query->where('element_model', 'User')->where('element_id', $user->id);
    });
        // Busca los permisos de los grupos a los que pertenece el usuario
    foreach ($user->groups as $kgroup => $group) {
        $perms = $perms->orWhere(function($query) use ($group){
            $query->where('element_model', 'Group')->where('element_id', $group->id);
        });
    }
    return $perms; 	
}

    /**
    * Este método busca en una lista de permisos si el usuario dado puede acceder al contenido.
    * @param {App/User} $user: usuario a verificar.
    * @param {...String} $permissions: lista de permisos.
    * @return {bool}: true si el usuario es administrador o posee los permisos necesarios, false en caso contrario.
    */
    public static function userHasPermissions(User $user, ...$permissions){
        $havePermission = $user != null;
        if($havePermission){
            $userPermissions = UserRole::getUserPermissions($user);
            $uPermissions = clone $userPermissions;
            if($uPermissions->get()->where('permission', 'admin')->toArray() == []){
                foreach ($permissions as $key => $perm) {
                    $uPermissions = clone $userPermissions;
                    $havePermission &= ($uPermissions->get()->where('permission', $perm)->toArray() != []);
                }
            }
        }
        return $havePermission;
    }

    /**
    * Obtiene una lista del perteneciente a la clase dada, y que tienen asignado directamente el permiso especificado.
    * @param {string} $permission: slug del permiso a buscar.
    * @param {string} $element_model: Nombre de la clase de los elementos a buscar.
    * @param {int} $level: nivel del permiso a buscar.
    * @param {string} $operation: nombre de la operación a usar. (permisos mayores, permisos menores, o permiso igual).
    * @return {Object}: query con los elementos que tienen asignados directamente el rol especificado.
    */
    public static function elementsWithPermission($permission, $element_model='Group', $level=0, $operation='>='){
      return ('App\\'.$element_model)::whereHas('permissions', function($query) use ($permission, $level, $operation){
        $query->where('permission', $permission)->where('permission_value', $operation, $level);
    });
  }

    /**
    * Obtiene todas los roles con el permiso dado.
    * @param {string} $permission: slug del permiso a buscar.
    * @param {string} $element_model: Nombre de la clase de los elementos a buscar.
    * @param {int} $level: nivel del permiso a buscar.
    * @param {string} $operation: nombre de la operación a usar. (permisos mayores, permisos menores, o permiso igual).
    * @return {Object}: query de la lista con rol especificado.
    */
    public static function getUserRolesPermissions($permission, $element_model='Group', $level=0, $operation='>='){
        return UserRole::where('element_model', $element_model)->where('permission', $permission)->where('permission_value', $operation, $level);
    }

    /**
    * Obtiene una lista de usuarios con un permiso específico, teniendo el permiso de forma indirecta, o de forma directa e indirecta.
    * @param {string} $permission: slug del permiso a buscar.
    * @param {bool} $directPermission: true para incluir los permisos aplicados directamente al usuario.
    * @param {int} $level: nivel del permiso a buscar.
    * @param {string} $operation: nombre de la operación a usar. (permisos mayores, permisos menores, o permiso igual).
    * @return {Object}: query con los elementos que tienen asignados directamente el rol especificado.
    */
    public static function getUsersByPermisson($permission, $directPermission=false, $level=0, $operation='>='){
        $users = User::whereHas('groups', function($gr) use($permission, $level, $operation){
            $gr->whereHas('permissions', function($per) use($permission, $level, $operation){
                $per->where('element_model', 'Group')->where('permission', $permission)->where('permission_value', $operation, $level);
            });
        });
        if($directPermission){
            $users = $users->orWhere(function($query) use($permission, $level, $operation){
                $query->whereHas('permissions', function($per) use($permission, $level, $operation){
                    $per->where('element_model', 'User')->where('permission', $permission)->where('permission_value', $operation, $level);
                });
            });
        }
        return $users;
    }

}
