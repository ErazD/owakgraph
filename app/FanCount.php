<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FanCount extends Model
{
	protected $guarded = [];

	public function element(){
		return DB::table($this->table)->where('id', $this->element_id)->first();
	}

	public function addFacebookFanMetric($fancount, $fanpage_id){
		$last_metric = $this->where('element_id', $fanpage_id)->latest()->first();
		if(!is_null($last_metric)){
			if($last_metric->fan_count != $fancount['fan_count']){
				$this->create([
					'element_id' => $fanpage_id,
					'table' => 'fanpages',
					'fan_count' => $fancount['fan_count']
					]);
			}
		}else{
         	//Si la fanpage es nueva o si la metrica es nula
			$this->create([
				'element_id' => $fanpage_id,
				'table' => 'fanpages',
				'fan_count' => $fancount['fan_count']
				]);
		}
	}
	public function addYoutubeFollowerMetrics($subs, $channel_id){
		$last_metric = $this->where('element_id', $channel_id)->latest()->first();
		if(!is_null($last_metric)){
			if($last_metric->fan_count != $subs){
				$this->create([
					'element_id' => $channel_id,
					'table' => 'youtube_channels',
					'fan_count' => $subs
					]);
			}
			}else{
                $this->create([
				'element_id' => $channel_id,
				'table' => 'youtube_channels',
				'fan_count' => $subs
				]);
			}
	}

	public function addInstagramFollowerMetrics($subs, $profile_id){
		$last_metric = $this->where('element_id', $profile_id)->latest()->first();
		if(!is_null($last_metric)){
			if($last_metric->fan_count != $subs){
				$this->create([
					'element_id' => $profile_id,
					'table' => 'insta_profiles',
					'fan_count' => $subs
					]);
			}
			}else{
                $this->create([
				'element_id' => $profile_id,
				'table' => 'insta_profiles',
				'fan_count' => $subs
				]);
			}
	}

}
