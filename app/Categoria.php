<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categoria extends Model
{
     protected $guarded = [];
       
  	 /*-------------------------------------------------------*
     * Obtener relación entre un segmento y sus canastas.
     * @return collection
     *-------------------------------------------------------*/
    public function canastas()
    {
     return $this->hasMany(Canasta::class);
    }

    /*-------------------------------------------------------*
     * Obtener relación entre una categoría y la vertical a la que pertenece.
     * @return Object
     *-------------------------------------------------------*/
     public function vertical(){
        return $this->belongsTo(Vertical::class);
     }

     /**
     * Obtiene la lista de canastas petenecienes a una categoria a las que un usuario tiene acceso.
     * @param {App\User} $user: Usuario del que se comprobará el acceso.
     * @return {Object}: Query con las canastas obtenias.
     */
     public function usercanastas(User $user){
        return GroupIndustryRole::getUserPermissionsElement($user, 'Canasta')->where('canastas.categoria_id', $this->id);
    }

    /**
    * Obtiene el query de los posts pertenecientes a todas las canastas de una categoría.
    * @param {string} $country: nombre del páis del que se quieren pedir los posts, si se quieren pedir todos, $country='all'.
    * @return {Object}: Query con la unión de los posts de las canastas.
    */
     public function posts($country="all"){
        $posts = null;
        foreach ($this->canastas as $k => $cnt) {
            $posts = ($posts)? $posts->concat($cnt->posts($country)): $cnt->posts($country);
        }
        return $posts;   
    }

    /**
    * Obtiene la lista de posts pertenecientes a la red social.
    * @param {string} $country: nombre del país a filtrar.
    * @param {string} $socialNetwork: nombre de la red social.
    * @return {Collection<Mixed>}: Lista con los posts de diferentes redes sociales (No es Query)
    */
    public function snPosts($country="all", $socialNetwork="all"){
        $posts = null;
        foreach ($this->canastas as $k => $cnt) {
            $posts = ($posts)? $posts->concat($cnt->snPosts($country, $socialNetwork)): $cnt->snPosts($country, $socialNetwork);
        }
        return $posts;   
    }

   /**
    * Obtiene el query de los posts pertenecientes a las canastas de una categoría dentro de un arreglo.
    * @return {Array<Object>}: arreglo de que contiene los queries de posts.
    */
    public function postsArrayQueries(){
        $posts = [];
        foreach ($this->canastas as $k => $cnt) {
            $posts =array_merge($posts , $cnt->postsArrayQueries());
        }
     return $posts;   
    }

    /**
     * Obtiene la Relación entre una categoría y los permisos que han sido asociados a sus marcas.
     * @return Object
     */
     public function industryRoles(){
        $industryRoles = null;
        foreach ($this->canastas as $k => $cnt) {
            $industryRoles = ($industryRoles)? $industryRoles->union($cnt->industryRoles()): $cnt->industryRoles();
        }
        return $industryRoles; 
     }

}
