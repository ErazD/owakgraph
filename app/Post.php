<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Metric as Metric;


class Post extends Model
{
	protected $guarded = [];
	protected $appends = ['twoFactorStatus'];
	// protected $dates = ['published'];
	 /*-------------------------------------------------------*
     * Obtener relación entre un Post y su parent fanpage.
     * @return Object
     *-------------------------------------------------------*/
	 public function fanpage()
	 {
	 	return $this->belongsTo(Fanpage::class);
	 }
	 public function brand(){
	 	return $this->fanpage->brand;
	 }
	 public function social_network(){
	 	return $this->fanpage->social_network;
	 }

	 /**
     * Obtiene la Relación entre una canasta y los permisos que han sido asociados a sus marcas.
     * @return Object
     */
     public function industryRoles(){
        return $this->fanpage->industryRoles(); 
     }
     
	 /**
	 * Retorna el límite asignado para el scrap de los posts
	 * @return {int} límite seleccionado del post o límite en el elemento que lo contiene.
	 */
	 public function selectedScrap(){
	 	if($this->selected_scrap_type !== null)
	 		return $this->selected_scrap_type;
	 	else 
	 		return $this->fanpage->selectedScrap();
	 }

	/*-------------------------------------------------------*
     * Obtener relación entre un Post y todas sus métricas.
     * @return Collection
     *-------------------------------------------------------*/
	public function metrics()
	{
		return $this->hasMany(Metric::class);
	}


	/*-------------------------------------------------------*
     * Obtener relación entre un Post y su última métrica.
     * @return Object
     *-------------------------------------------------------*/
	public function latestMetric()
	{
		return $this->hasOne(Metric::class)->latest();

	}
    
    /*-------------------------------------------------------*
     * Obtener post con métrica en caso de que el ultimo dato este vacio.
     * @return Object
     *-------------------------------------------------------*/
	public function maxMetric()
	{
		// return $this->hasOne(Metric::class)->max();
		$v =  $this->hasOne(Metric::class)->where('views', '>' , 0)->latest()->first();
		return ($v)? $v:0;
	}


	/*-------------------------------------------------------*
     * Obtener relación entre un Post y sus métricas el día de hoy.
     * @return Collection
     *-------------------------------------------------------*/

	public function metrics_per_day()
	{
		return $this->hasMany(Metric::class)->where('created_at', '>=', \Carbon\Carbon::today());
	}


	/*-------------------------------------------------------*
     * Obtener relación entre un Post y sus métricas en la semana.
     * @return Collection
     *-------------------------------------------------------*/

	public function metrics_per_week()
	{
		return $this->hasMany(Metric::class)->whereBetween('created_at', [
			\Carbon\Carbon::parse('last monday')->startOfDay(),
			\Carbon\Carbon::parse('next friday')->endOfDay(),
		])->latest();

	}

	/*-------------------------------------------------------*
     * Obtener relación entre un Post y sus métricas en el mes.
     * @return Collection
     *-------------------------------------------------------*/

	public function metrics_per_month()
	{
		return $this->hasMany(Metric::class)->whereBetween('created_at', [
			\Carbon\Carbon::now()->startOfMonth(),
			\Carbon\Carbon::now()->endOfMonth(),
		])->latest();

	}

	/*-------------------------------------------------------*
     * Función que actualiza el attachment del post.
     *-------------------------------------------------------*/

	public function setAttachmentField($src)
	{
		$this->update([
			'attachment' => $src,
		]);
	}

	 	/*-------------------------------------------------------*
     * Obtener relación entre el posts de facebook y los concurso a los que pertenece.
     * @return collection
     *-------------------------------------------------------*/
	 	public function contests()
	 	{
 		return $this->belongsToMany(Contest::class,'contest_post','post_id','contest_id')->where('social_network', 'Facebok');
	 	}

    /*-------------------------------------------------------*
     * Obtener relación entre el post de facebook y el concurso.
     * @return collection
     *-------------------------------------------------------*/
    public function contest()
    {
    	return $this->belongsToMany(Contest::class,'contest_post','post_id','contest_id')->where('social_network', 'Facebok')->first();
    }


	 /*-------------------------------------------------------*
     * Obtener relación entre un Post y su parent fanpage.
     * @return Object
     *-------------------------------------------------------*/
	 public function product()
	 {
	 	return $this->belongsTo(Product::class);
	 }

	 /*-------------------------------------------------------*
     * Obtener relación entre un Post y la canasta a la que pertenece.
     * @return Object
     *-------------------------------------------------------*/
	 public function canasta()
	 {
	 	return $this->belongsTo(Canasta::class);
	 }

	  /*-------------------------------------------------------*
     * Obtener relación entre un Post y el adset al que pertenece.
     * @return Object
     *-------------------------------------------------------*/
	 public function adSets()
	 {
	 	return $this->belongsToMany(AdSet::class, 'post_ad_set','ad_set_id','post_id');
	 }

	 /*-------------------------------------------------------*
     * Obtener relación entre un Post y sus elementos de two factor authentication.
     * @return Collection
     *-------------------------------------------------------*/
	 public function twofactors()
	 {
	 	return $this->hasMany(TwoFactorAuthentication::class, 'post_id')->where('social_network', 'Facebook');
	 }

	 /*-------------------------------------------------------*
     * Verifica el estado de los valores del comms, si están todos los campos obligatorios, si tiene twofactor en proceso, o si no tiene nada asignado. este resultado lo guarda en el triibuto local  'twoFactorStatus'.
     * @return 2 si tiene los campos obligatorios, 1 si tiene al menos dos twofactors iniciados, 0 si no tiene suficientes twoFactors para un match.
     *-------------------------------------------------------*/
	 public function getTwoFactorStatusAttribute()
	 {
	 	$twoFactorStatus = 0;

	 	if(
	 		$this->type != null && 
	 		$this->balance != null && 
	 		$this->comunication != null && 
	 		$this->theme != null && 
	 		($this->contest() != null || $this->theme != 'concurso') && 
	 		$this->product_id != null &&
	 		$this->canasta_id != null &&
	 		$this->micro_moment != null &&
	 		$this->business_objective != null
	 	){
	 		$twoFactorStatus = 2;
	 	}else {
	 		$twfMax = 0;
	 		foreach ($this->twofactors as $key => $twf) {
	 			$twfMax = max($twfMax, $twf->status);
	 		}
	 		if($twfMax >= 2){
	 		 		$twoFactorStatus = 1;
	 		 	}
	 	}
	 	return $twoFactorStatus;
	 }

	  /*-------------------------------------------------------*
     * Consulta si el usuario ha sido asignado para clasificar los comms del post.
     * @param $user: usuario para realiar la consulta.
     * @return true si el usuario existe, false en caso contrario.
     *-------------------------------------------------------*/
	 public function hasUser($user = false){
	 	if(!$user && Auth::check()){
    	  	$user = Auth::user();
    	}
	 	$has = false;
	 	if($user && $user->twoFactorProfile){
	 		$ele = $this->twofactors()->where('profile_id', $user->twoFactorProfile->id)->first();
	 		if($ele){
	 			$has=true;
	 		}
	 	}
	 	return $has;
	 }

	/*-------------------------------------------------------*
     * Añadir Post
     *-------------------------------------------------------*/

	public function add($post, $fanpage_id, $scrap_type=0)
	{	
		//Preguntar si este post ya ha sido añadido
		$saved = $this->where('facebook_id', $post['id'])->first();
		//Si no está
		if(is_null($saved)){
			//Se crea un post
			$post_created = $this->create([
				'fanpage_id' => $fanpage_id,
				'facebook_id' => $post['id'],
				'content' => isset($post['message']) ? $post['message'] : null,
				'published' => $post['created_time']->format('Y/m/d H:i:s'),
				'type' => $post['type'],
				'permalink_url' => isset($post['permalink_url']) ? $post['permalink_url'] : '#',
				'attachment' => isset($post['attachments'][0]['media']['image']['src']) ? $post['attachments'][0]['media']['image']['src'] : null,
				'scrap_type' => $scrap_type
			]);

			if(!empty($post_created->attachment)){
		     $url = 'imgUpload/' . 'fb_img_' .$post_created->id.'.jpg';
    		 file_put_contents(public_path($url), file_get_contents($post_created->attachment));
         	 $post_created->update([
             'attachment' => $url]);
         	}
			//Se crea la métrica inicial de ese post
			Metric::create([
				'post_id' => $post_created->id,
				'reactions' => $post['reactions']->getTotalCount(),
				'comments' => $post['comments']->getTotalCount(),
				'shares' => isset($post['shares']['count']) ? $post['shares']['count'] : 0,
			]);
		}
	}

	/*-------------------------------------------------------*
     * Añadir Post Manualmente, existen DARK Posts, que no son visibles
     * por el scrap de OWAK Graph.
     *-------------------------------------------------------*/

	public function manualAdd($post, $fanpage_id, $scrap_type=0)
	{
		$post_created = $this->create([
			'facebook_id' => $post['id'],
			'fanpage_id' => $fanpage_id,
			'content' => isset($post['message']) ? $post['message'] : null,
			'published' => $post['created_time'],
			'type' => $post['type'],
			'permalink_url' => $post['permalink_url'],
			'attachment' => isset($post['attachments']) ? $post['attachments']['data'][0]['media']['image']['src'] : null,
			'scrap_type' => $scrap_type
		]);

		if(!empty($post_created->attachment)){
		     $url = 'imgUpload/' . 'fb_img_' .$post_created->id.'.jpg';

    		 file_put_contents(public_path($url), file_get_contents($post_created->attachment));
         	 $post_created->update([
             'attachment' => $url]);
         	}

		Metric::create([
			'post_id' => $post_created->id,
			'reactions' => $post['reactions']['summary']['total_count'],
			'comments' => $post['comments']['summary']['total_count'],
			'shares' => isset($post['shares']['count']) ? $post['shares']['count'] : 0,
		]);

		return $post_created;

	}
}
