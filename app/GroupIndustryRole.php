<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GroupIndustryRole extends Model
{
	protected $guarded = [];

	public function group(){
		return $this->belongsTo(Group::class);
	}

	/**
	* (antes se llamaba getGroup)
	* Retorna una lista de los permisos que tienen los grupos asociados a un elemento.
	* @param {Model} $element: Elemento del que se quiere consultar los permisos.
	* @return {Collection<GroupIndustryRole>}: listas con los permisos asociados a un elemento.
	*/
	public static function getGroupIndustryRoles($element){
		$classname = explode('\\', get_class($element));
		$classname = $classname[sizeof($classname)-1];
		$groupRoles = GroupIndustryRole::where('element_model', $classname)
		->where('element_id', $element->id)
		->get();
		if(sizeof($groupRoles) == 0){
			$group = Group::create([
				'name' => $element->name.' Permisos básicos',
				'description' => 'Grupo automático para los permisos básicos de visualización del elemento "'.$element->name.'".',
			]);
			$gIR = GroupIndustryRole::firstOrCreate([
				'element_model' => $classname,
				'element_id' => $element->id,
				'group_id' => $group->id,
				'permission_value' => 0,
			]);
			$groupRoles = GroupIndustryRole::where('element_model', $classname)->where('element_id', $element->id)->get();
		}
		return $groupRoles;
	}

	/**
	* Obtiene la relación entre un usuario y las marcas a las que tiene acceso.
	* @param {App\User} $user: usuario con el que se quiere realizar la consulta.
	* @param {string} $element_classname: nombre de la clase del elemento a pedir.
	* @return {Object}: relación con las marcas a los que pertenecen.
	*/
	public static function getUserPermissionsElement(User $user, $element_classname='Brand'){

		return GroupIndustryRole::getUserPermissionsElementByID($user->id, $element_classname);
	}
	/**
	* Obtiene la relación entre un usuario y las marcas a las que tiene acceso.
	* @param {int} $user_id: ID del usuario con el que se quiere realizar la consulta.
	* @param {string} $element_classname: nombre de la clase del elemento a pedir.
	* @return {Object}: relación con las marcas a los que pertenecen.
	*/
	public static function getUserPermissionsElementByID($user_id, $element_classname='Brand'){
	 
		$gRI = ('App\\'.$element_classname)::whereHas('industryRoles', function($iQuery) use ($user_id){
			$iQuery->whereHas('group', function($gQuery) use ($user_id){
				$gQuery->whereHas('users', function($uQuery) use ($user_id){
					$uQuery->where('users.id', $user_id);
				});
			});
		});
		$subModel = false; // indica que se debe hacer un join con las tablas siguientes en el modelo de industria
		// si el MODELO NO ES BRAND, hay que agregar las tablas de marca al query
		// Verifica si es una VERTICAL
		if(($isElement = $element_classname == 'Vertical') || $subModel){
			$subModel = true;
			$gRI = $gRI->join('categorias', 'verticals.id', '=', 'categorias.vertical_id');
			if($isElement)
				$gRI = $gRI->select('verticals.*')->groupBy('verticals.id');
		}
		// Verifica si es una CATEGORÍA
		if(($isElement = $element_classname == 'Categoria') || $subModel){
			$subModel = true;
			$gRI = $gRI->join('canastas', 'categorias.id', '=', 'canastas.categoria_id');
			if($isElement)
				$gRI = $gRI->select('categorias.*')->groupBy('categorias.id');
		}
		// Verifica si es una CANASTA
		if(($isElement = $element_classname == 'Canasta') || $subModel){
			$subModel = true;
			$gRI = $gRI->join('canasta_brand', 'canastas.id', '=', 'canasta_brand.canasta_id')
			->join('brands', 'brands.id', '=', 'canasta_brand.brand_id');
			if($isElement)
				$gRI = $gRI->select('canastas.*')->groupBy('canastas.id');
		}
		if($subModel)
			return $gRI;

		$subFBModel = false;
		// Verifica si es un POST DE FACEBOOK
		if(($isElement = $element_classname == 'Post') || $subFBModel){
			$subFBModel = true;
			$gRI = $gRI->join('fanpages', 'fanpages.id', '=', 'posts.fanpage_id');
			if($isElement)
				$gRI = $gRI->select('posts.*')->groupBy('posts.id');
		}
		// Verifica si es una FANPAGE
		if(($isElement = $element_classname == 'Fanpage') || $subFBModel){
			$subFBModel = true;
			$gRI = $gRI->join('brands', 'brands.id', '=', 'fanpages.brand_id');
			if($isElement)
				$gRI = $gRI->select('fanpages.*')->groupBy('fanpages.id');
		}
		if($subFBModel)
			return $gRI;

		$subYTModel = false;
		// Verifica si es un VIDEO DE YOUTUBE
		if(($isElement = $element_classname == 'YoutubeVideo') || $subYTModel){
			$subYTModel = true;
			$gRI = $gRI->join('youtube_channels', 'youtube_channels.id', '=', 'youtube_videos.youtube_channel_id');
			if($isElement)
				$gRI = $gRI->select('youtube_videos.*')->groupBy('youtube_videos.id');
		}
		// Verifica si es un CANAL DE YOUTUBE
		if(($isElement = $element_classname == 'YoutubeChannel') || $subYTModel){
			$subYTModel = true;
			$gRI = $gRI->join('brands', 'brands.id', '=', 'youtube_channels.brand_id');
			if($isElement)
				$gRI = $gRI->select('youtube_channels.*')->groupBy('youtube_channels.id');
		}
		if($subYTModel)
			return $gRI;

		$subINSModel = false;

		//Verifica si es un post de Instagram
        if(($isElement = $element_classname == 'InstaPost') || $subINSModel){
			$subINSModel = true;
			$gRI = $gRI->join('insta_profiles', 'insta_profiles.id', '=', 'insta_posts.insta_profile_id');
			if($isElement)
				$gRI = $gRI->select('insta_posts.*')->groupBy('insta_posts.id');
		}
		//Verifica si es un perfil de Instagram
        if(($isElement = $element_classname == 'InstaProfile') || $subINSModel){
			$subINSModel = true;
			$gRI = $gRI->join('brands', 'brands.id', '=', 'insta_profiles.brand_id');
			if($isElement)
				$gRI = $gRI->select('insta_profiles.*')->groupBy('insta_profiles.id');
		}
		if($subINSModel)
			return $gRI;


		return $gRI;
	}

	/**
	* PERMISOS INDIRECTOS
	* Comprueba si el usuario tiene acceso al elemento dado comprobando su relación con las marcas asignadas.
	* @param {User} $user: Usuario que se comprobará.
	* @param {Object} $element: elemento que se quiere comprobar.
	* @return {bool}: true si el usuario tiene acceso a elemento, false en caso contrario.
	*/
	public static function userhasAcces(User $user, $element){
		$classname = explode('\\', get_class($element));
		$classname = $classname[sizeof($classname)-1];
		$elements = GroupIndustryRole::getUserPermissionsElement($user, $classname)->where($element->getTable().'.id', $element->id)->get();
		return sizeof($elements) > 0;
	}

	/**
	* PERMISOS DIRECTOS
	* Obtiene el valor mayor para el permiso del usuario para el elemento dado
	* @param {User} $user: usuario del que se va a pedir el permiso.
	* @param {Model} $element: elemento al que se le quiere pedir los permisos
	* @return {int}: Número mayor a cero que represemta el nivel de permiso que tiene el usuario, si el usuario no tiene permisos retorna -1.  
	*/
	public static function getUserLevelPermission(User $user, $element){
		$classname = explode('\\', get_class($element));
		$classname = $classname[sizeof($classname)-1];
		return GroupIndustryRole::getUserLevelPermissionByIDs($user->id, $element->id, $classname);
	}

	/**
	* PERMISOS DIRECTOS
	* Obtiene el valor mayor para el permiso del usuario para el elemento dado
	* @param {int} $user_id: ID del usuario del que se va a pedir el permiso.
	* @param {int} $element_id: ID del elemento al que se le quiere pedir los permisos.
	* @param {string} $element_classname: Nombre de la clase del elemento al que se le quiere pedir los permisos.
	* @return {int}: Número mayor a cero que represemta el nivel de permiso que tiene el usuario, si el usuario no tiene permisos retorna -1.  
	*/
	public static function getUserLevelPermissionByIDs($user_id, $element_id, $element_classname){
		$groupRole = GroupIndustryRole::where('element_model', $element_classname)
		->where('element_id', $element_id)
		->whereHas('group', function($gQuery) use ($user_id){
			$gQuery->whereHas('users', function($uQuery) use ($user_id){
				$uQuery->where('users.id', $user_id);
			});
		})
		->orderBy('permission_value', 'DESC')->first();

		// dd($groupRole, $user_id, $element_id, $element_classname, $groupRole->permission_value);
		if($groupRole)
			return $groupRole->permission_value;
		return -1;
	}

	/**
	* Obtiene el valor mayor para el permiso del grupo para el elemento dado
	* @param {Group} $group: usuario del que se va a pedir el permiso.
	* @param {Model} $element: elemento al que se le quiere pedir los permisos
	* @return {int}: Número mayor a cero que represemta el nivel de permiso que tiene el usuario, si el usuario no tiene permisos retorna -1.  
	*/
	public static function getGroupLevelPermission(Group $group, $element){
		$classname = explode('\\', get_class($element));
		$classname = $classname[sizeof($classname)-1];
		return GroupIndustryRole::getGroupLevelPermissionByIDs($group->id, $element->id, $classname);
	}

	/**
	* Obtiene el valor mayor para el permiso del usuario para el elemento dado
	* @param {int} $group_id: ID del grupo del que se va a pedir el permiso.
	* @param {int} $element_id: ID del elemento al que se le quiere pedir los permisos.
	* @param {string} $element_classname: Nombre de la clase del elemento al que se le quiere pedir los permisos.
	* @return {int}: Número mayor a cero que represemta el nivel de permiso que tiene el grupo, si el usuario no tiene permisos retorna -1.  
	*/
	public static function getGroupLevelPermissionByIDs($group_id, $element_id, $element_classname){
		$groupRole = GroupIndustryRole::getGroupLevelPermissionListByIDs($group_id, $element_id, $element_classname)->first();
		if($groupRole)
			return $groupRole->levelPermission;
		return -1;
	}

	/**
	* Obtiene la lista de permisos de industria (GroupIndustryRole) de un modelo dado que están asignados a un grupo.
	* @param {App\Group} $group: grupo del que se quieren buscar los permisos.
	* @param {Object} $element: Elemento a buscar.
	* @return {Object}: Relación de los permisos del grupo con el elemento dado.
	*/
	public static function getGroupLevelPermissionList(Group $group, $element){
		$classname = explode('\\', get_class($element));
		$classname = $classname[sizeof($classname)-1];
		return GroupIndustryRole::getGroupLevelPermissionListByIDs($group->id, $element->id, $classname);
	}
	/**
	* Obtiene la lista de permisos de industria (GroupIndustryRole) de un modelo dado que están asignados a un grupo.
	* @param {int} $group_id: id del grupo.
	* @param {int} $element_id: id del element a buscar.
	* @param {string} $element_classname: Nombre de la clase a la que pertenece elelemento.
	* @return {Object}: Relación de los permisos del grupo con el elemento dado.
	*/
	public static function getGroupLevelPermissionListByIDs($group_id, $element_id, $element_classname){
		return GroupIndustryRole::where('element_model', $element_classname)
		->where('element_id', $element_id)
		->whereHas('group', function($gQuery) use ($group_id){
			$gQuery->where('groups.id', $group_id);			
		})
		->orderBy('permission_value', 'DESC');
	}

	/**
    * Agrega un permiso de una marca para un grupo.
    * @param {Group} $grupo al que se agregarán los permisos.
    * @param {String} $element_id: ID del elemento del permisos.
    * @param {String} $element_classname: Nombre de la clase del elemento.
    * @param {int} $level: El valor del permiso.
    * Postcondition: Crea una conexión de la marca y el grupo con el valor dado.
    */
	public static function addPermissionGroup(Group $group, $element_id, $element_classname, $level=0){
		$gIR = GroupIndustryRole::firstOrCreate([
			'element_model' => $element_classname,
			'element_id' => $element_id,
			'group_id' => $group->id,
		]);
		$gIR->permission_value = $level;
		$gIR->save();
	}

	/**
    * Elimina los permisos de una marca para un grupo.
    * @param {Group} $grupo al que se quitaran los permisos.
    * @param {String} $element_id: ID del elemento a eliminar.
    * @param {String} $element_classname: Nombre de la clase del elemento a eliminar.
    * @param {int/string} $level: 'all' si se quieren eliminar todos los permisos, en caso contrario, el valor del permiso a eliminar.
    * Postcondition: se han eliminado las conexiones de la marca y el grupo que cumplan con las condiciones.
    */
	public static function removePermissionsGroup(Group $group, $element_id, $element_classname, $level=0){
		$gIR = GroupIndustryRole::where('element_model', $element_classname)
		->where('element_id', $element_id)
		->where('group_id', $group->id);
		if($level != 'all')
			$gIR = $gIR->where('permission_value', $level);
		$gIR->delete();
	}


}
