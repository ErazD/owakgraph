<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
	protected $guarded = [];

	/*-------------------------------------------------------*
     * Guardar Reporte en la base de datos.
     *-------------------------------------------------------*/
	public function saveReport($report, $id)
	{
		$url = (empty($report['permalink_url'] ))? '#' : $report['permalink_url'];
		$this->create([
			'report_id' => $id,
			'reactions' => $report['reactions']->getTotalCount(),
			'comments' => $report['comments']->getTotalCount(),
			'shares' => isset($report['shares']['count']) ? $report['shares']['count'] : 0,
			'type' => $report['type'],
			'content' => isset($report['message']) ? $report['message'] : null,
			'published' => $report['created_time']->format('Y/m/d H:i:s'),
			'url' => $url
		]);
	}
}
