<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\InstaMetrics as InstaMetrics;

class InstaPost extends Model
{

	protected $guarded = [];
     /*-------------------------------------------------------*
     * Obtener relación entre un Post y su parent perfil.
     * @return Object
     *-------------------------------------------------------*/
    public function profile(){
     	return $this->belongsTo(InstaProfile::class, 'insta_profile_id');
    }
    public function brand(){
      return $this->profile->brand;
    }
    public function social_network(){
      return $this->profile->social_network;
    }

       /**
     * Obtiene la Relación entre una canasta y los permisos que han sido asociados a sus marcas.
     * @return Object
     */
       public function industryRoles(){
        return $this->fanpage->industryRoles(); 
      }

     /*-------------------------------------------------------*
     * Obtener relación entre un Post y todas sus métricas.
     * @return Collection
     *-------------------------------------------------------*/
     public function metrics()
     {
     	return $this->hasMany(InstaMetrics::class);
     }

    /*-------------------------------------------------------*
     * Obtener relación entre un Post y su última métrica.
     * @return Object
     *-------------------------------------------------------*/

    public function latestMetric()
    {
    	return $this->hasOne(InstaMetrics::class)->latest();
    }
    
       /**
   * Retorna el límite asignado para el scrap de los posts
   * @return {int} límite seleccionado del post o límite en el elemento que lo contiene.
   */
       public function selectedScrap(){
        if($this->selected_scrap_type !== null)
          return $this->selected_scrap_type;
        else 
          return $this->profile->selectedScrap();
      }

    /*-------------------------------------------------------*
     * Obtener relación entre un Post y sus métricas el día de hoy.
     * @return Collection
     *-------------------------------------------------------*/

    public function metrics_per_day()
    {
    	return $this->hasMany(InstaMetrics::class)->where('created_at', '>=', \Carbon\Carbon::today());
    }

    /*-------------------------------------------------------*
     * Obtener relación entre un Post y sus métricas en la semana.
     * @return Collection
     *-------------------------------------------------------*/

    public function metrics_per_week()
    {
    	return $this->hasMany(InstaMetrics::class)->whereBetween('created_at', [
    		\Carbon\Carbon::parse('last monday')->startOfDay(),
    		\Carbon\Carbon::parse('next friday')->endOfDay(),
    		])->latest();

    }   
 /*-------------------------------------------------------*
     * Obtener relación entre un Post y sus métricas en el mes.
     * @return Collection
     *-------------------------------------------------------*/
 public function metrics_per_month()
 {
 	return $this->hasMany(InstaMetrics::class)->whereBetween('created_at', [
 		\Carbon\Carbon::now()->startOfMonth(),
 		\Carbon\Carbon::now()->endOfMonth(),
 		])->latest();
 }

	/*-------------------------------------------------------*
     * Función que actualiza el attachment del post.
     *-------------------------------------------------------*/
	public function setAttachmentField($src)
	{
		$this->update([
			'attachment' => $src,
			]);
	}

 	/*-------------------------------------------------------*
     * Obtener relación entre el posts de facebook y los concurso a los que pertenece.
     * @return collection
     *-------------------------------------------------------*/
 	public function contests()
 	{
 		return $this->belongsToMany(Contest::class,'contest_post','post_id','contest_id');
 	}

    /*-------------------------------------------------------*
     * Obtener relación entre el post de facebook y el concurso.
     * @return collection
     *-------------------------------------------------------*/
    public function contest()
    {
    	return $this->belongsToMany(Contest::class,'contest_post','post_id','contest_id')->first();
    }

    public function product()
    {
    	return $this->belongsTo(Product::class);
    }
    public function canasta()
    {
    	return $this->belongsTo(Canasta::class);
    }

       /*-------------------------------------------------------*
     * Obtener relación entre un Post y sus elementos de two factor authentication.
     * @return Collection
     *-------------------------------------------------------*/
   public function twofactors()
   {
    return $this->hasMany(TwoFactorAuthentication::class, 'post_id')->where('social_network', 'Instagram');
   }

   /*-------------------------------------------------------*
     * Verifica el estado de los valores del comms, si están todos los campos obligatorios, si tiene twofactor en proceso, o si no tiene nada asignado. este resultado lo guarda en el triibuto local  'twoFactorStatus'.
     * @return 2 si tiene los campos obligatorios, 1 si tiene al menos dos twofactors iniciados, 0 si no tiene suficientes twoFactors para un match.
     *-------------------------------------------------------*/
   public function getTwoFactorStatusAttribute()
   {
    $twoFactorStatus = 0;

    if(
      $this->type != null && 
      $this->balance != null && 
      $this->comunication != null && 
      $this->theme != null && 
      ($this->contest() != null || $this->theme != 'concurso') && 
      $this->product_id != null &&
      $this->canasta_id != null &&
      $this->micro_moment != null &&
      $this->business_objective != null
    ){
      $twoFactorStatus = 2;
    }else {
      $twfMax = 0;
      foreach ($this->twofactors as $key => $twf) {
        $twfMax = max($twfMax, $twf->status);
      }
      if($twfMax >= 2){
          $twoFactorStatus = 1;
        }
    }
    return $twoFactorStatus;
   }

   /*-------------------------------------------------------*
     * Consulta si el usuario ha sido asignado para clasificar los comms del post.
     * @param $user: usuario para realiar la consulta.
     * @return true si el usuario existe, false en caso contrario.
     *-------------------------------------------------------*/
   public function hasUser($user = false){
    if(!$user && Auth::check()){
          $user = Auth::user();
      }
    $has = false;
    if($user && $user->twoFactorProfile){
      $ele = $this->twofactors()->where('profile_id', $user->twoFactorProfile->id)->first();
      if($ele){
        $has=true;
      }
    }
    return $has;
   }

    	/*-------------------------------------------------------*
     * Añadir Post
     *-------------------------------------------------------*/

    	public function add($post, $profile_id)
    	{	

		//Preguntar si este post ya ha sido añadido
       // $saved = $this->where('insta_profile_id', $post['id'])->first();
        $saved = $this->where('post_id', $post->post_id)->first();
		//Si no está
        if(is_null($saved)){
			//Se crea un post
         $post_created = $this->create([
          'insta_profile_id' => $profile_id,
          'post_id' => $post->post_id,
          'content' => isset($post->content) ? $post->content : null,
          'published' => (isset($post->profile_id))? $post->published : $post->published,
          'type' => $post->type,
          'permalink_url' => isset($post->url) ? $post->url : '#',
          'attachment' => isset($post->attachment)? $post->attachment : null
          ]);

			//Se crea la métrica inicial de ese post
         if(isset($post->profile_id)){

           InstaMetrics::create([
            'insta_post_id' => $post_created->id,
            'likes' => $post->likes,
            'views' => $post->views,
            'comments' => $post->comments,
            ]);

         }else{
          InstaMetrics::create([
           'insta_post_id' => $post_created->id,
           'likes' => $post->likes,
           'views' => ($post_created->type == 'video') ? $post->views : 0,
           'comments' => $post->comments,
           ]);
        }
      }
    }

	/*-------------------------------------------------------*
     * Añadir Post Manualmente, existen DARK Posts, que no son visibles
     * por el scrap de OWAK Graph.
     *-------------------------------------------------------*/

	public function manualAdd($post, $profile_id)
	{    

   $saved = $this->where('post_id', $post->post_id)->first();
   if(is_null($saved)){
    $post_created = $this->create([
     'insta_profile_id' => $profile_id,
     'post_id' => $post->post_id,
     'content' => isset($post->content) ? $post->content : null,
     'published' => $post->published->date,
     'type' => $post->type,
     'permalink_url' => isset($post->url) ? $post->url : '#',
     'attachment' => isset($post->attachment)? $post->attachment : null
     ]);

    InstaMetrics::create([
     'insta_post_id' => $post_created->id,
     'likes' =>  $post->likes,
     'views' => ($post_created->type == 'video') ? $post->views : 0,
     'comments' => $post->comments,
     ]);

    return $post_created;
  }
}

}
