<?php

/*-------------------------------------------------------*
 * Comando para enviar posts recientes de tipo video para hacer scrap la metrica de vistas
*-------------------------------------------------------*/

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DateTime;
use App\Http\Controllers\PostsController;
use App\Http\Controllers\InstaPostController;

class ScrapMetrics extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ScrapMetrics:getmetrics {scrap_type=0}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sends recent video posts to scrap the "views" metric later';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(PostsController $control, InstaPostController $ipc)
    {
        //indicador a los posts para saber cuales se van a scrapear
        $scrap_type = $this->argument('scrap_type');
        if($scrap_type == 0 || $scrap_type == 1){
             $this -> sendScrapRequest($control, 0);
             $this -> sendScrapRequest($control, 1);
        }else{
            $this -> sendScrapRequest($control, $scrap_type);
        }
        $this->sendInstaScrapPosts($ipc, $scrap_type);
    }

    /**
     * @param {Metric} modelo para verificar las metricas.
     * @param {Post} modelo para verificar los posts
     * @param {GuzzRequest} libreria que se utiliza para enviar el request
     *
    */

    protected function sendScrapRequest(PostsController $control, $scrap_type)
    {
        $result = $control -> getVideoMetrics($scrap_type);
        $this -> info(date("Y-m-d H:i:s") . ' Datos enviados a Scraper con éxito ' . 'Datos enviados: ' . $result);
    }

     protected function sendInstaScrapRequest(InstaPostController $ipc, $scrap_type){
       $result = $ipc->sendPostsData($scrap_type);
       $this -> info(date("Y-m-d H:i:s") . ' Datos Insta enviados a Scraper con éxito ' . 'Datos enviados: ' . $result . ' Del tipo: ' . $scrap_type);
    }
}
