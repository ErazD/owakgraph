<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DateTime;
use App\Http\Controllers\InstaProfileController;

class SendInstaProfiles extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'profiles:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send available profiles for scraping';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(InstaProfileController $ipc)
    {
        $this->info(date('Y-m-d H:i:s') . 'Inicio de envio de perfiles');
        $this->sendProfileData($ipc);
    }
    protected function sendProfileData(InstaProfileController $ipc){
        $result =  $ipc->sendProfileInfo();
        $this->info(date("Y-m-d H:i:s") . $result);
    }
}
