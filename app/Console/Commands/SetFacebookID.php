<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Fanpage;
use App\Token;
use \SammyK\LaravelFacebookSdk\LaravelFacebookSdk as Facebook;

class SetFacebookID extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'SetFacebookID:facebookid';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Add Facebook ID to fanpage';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(Fanpage $fanpages, Facebook $fb)
    {
        $fanpages = Fanpage::all();
        $token = Token::first()->token;

        foreach ($fanpages as $key => $fanpage) {
            
            $data = $fb->get($fanpage->url, $token)->getDecodedBody();
            Fanpage::where('id', $fanpage->id)->update([
                'facebook_id' => $data['id']
            ]);



        }
    }
}
