<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Fanpage;
use App\Token;
use App\FanCount;
use \SammyK\LaravelFacebookSdk\LaravelFacebookSdk as Facebook;
use App\YoutubeChannel;
use DateTime;
use DB;
use Carbon\Carbon;
use App\Http\Controllers\YoutubeController;
use App\Http\Controllers\FanpagesController;
use App\Http\Controllers\InstaProfileController;


class GetFansCount extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fancount:getfans';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get number of fans/followers/subscribers from fanpages, channels and more';
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(FanCount $fancount, Fanpage $fanpages, Facebook $fb, FanpagesController $fc, YoutubeController $yt, YoutubeChannel $channels, InstaProfileController $ipc)
    {
        $this->getFacebookFanCountMetrics($fb, $fc, $fanpages, $fancount);
        $this->getYoutubeFollowerMetrics($yt, $channels, $fancount);
        $this->getInstagramFollowers($ipc);
    }

    protected function getFacebookFanCountMetrics(Facebook $fb, FanpagesController $fc, Fanpage $fanpages, FanCount $fancount)
    {   
        $success = 0;
        $token = Token::first()->token;
        $fanpages = Fanpage::where('active', 1)->get();
        foreach($fanpages as $fanpage){
          try{
            preg_match('#https?\://(?:www\.)?facebook\.com/(\d+|[A-Za-z0-9\.]+)/?#', $fanpage->url, $fanpage_name);
            $fan_count = $fb->get($fanpage_name[1].'/?fields=fan_count', $token);
            $fans = $fan_count->getGraphObject();
            $fancount->addFacebookFanMetric($fans, $fanpage->id);
            $success++;

        }catch(\Facebook\Exceptions\FacebookSDKException $e){
          $this->error($e->getMessage());
      }

  }
  $this->info(date("Y-m-d H:i:s") . 'Información de fanpages recuperada con éxito' . ' datos recuperados: ' . $success);
}
protected function getYoutubeFollowerMetrics(YoutubeController $yt, YoutubeChannel $channels, FanCount $fancount){
 $success = 0;
 $channels = YoutubeChannel::where('active', 1)->get();
 $youtube = $yt->getYoutubeService();
 foreach($channels as $channel){
    try{
      $channelsResponse = $youtube->channels->listChannels('statistics',array(
          'id'=> $channel->youtube_id,
          'fields'=>'items(id, statistics/subscriberCount)'
      ));    
    foreach($channelsResponse['items'] as $channelResult){
         $subs = $channelResult['statistics']['subscriberCount'];
         if(!$subs){
            $subs=0;
         }
         $fancount->addYoutubeFollowerMetrics($subs, $channel->id);
         $success++;
    }  
    }catch(\Google_Service_Exception $e){
       $this->error($e->getMessage());
    } catch(\Google_Exception $e){
       $this->error($e->getMessage()); 
    }
 }
 $this->info(date("Y-m-d H:i:s") . 'Información de subscriptores recuperada con éxito' . ' datos recuperados: ' . $success);
}

protected function getInstagramFollowers(InstaProfileController $ipc){
   $addFollowers = $ipc->getProfileFollowers();
   $this->info(date("Y-m-d H:i:s") . 'Informacion de seguidores recuperada con éxito' . 'datos recuperados: ' . $addFollowers);
}

}