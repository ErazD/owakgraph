<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use \SammyK\LaravelFacebookSdk\LaravelFacebookSdk as Facebook;
use App\Post;
use App\Token;
use DateTime;
use App\Http\Controllers\PostsController;

class SetAttachPosts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'SetAttachPosts:setattach';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update Attachment field';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(Post $posts, Facebook $fb)
    {
      $this->info(date("Y-m-d H:i:s") . " Refrescando imagenes de posts");
      $this->refreshImg($posts, $fb);    
  }

  protected function refreshImg(Post $posts, Facebook $fb){

    $posts = Post::all();
    $token = Token::first()->token;
    $works = 0;
    $notWorking = 0;
    $failure=0;
    $limiter = 90;

    foreach ($posts as $key => $post) {
    if(!strpos($post->attachment, 'owak.co')){ 
       try{
        $attachment = $fb->get($post->facebook_id.'?fields=attachments,created_time', $token);
        $appUsage = $attachment->getHeaders()['x-app-usage'];
        $appUsage = json_decode($appUsage, true);

        $attachment = $attachment->getDecodedBody();
        $date = strtotime($attachment['created_time'], time() - 2*60*60);
        $imgPath = isset($attachment['attachments']['data'][0]['media']['image']['src']) ? $attachment['attachments']['data'][0]['media']['image']['src'] : null;
        if(!empty($imgPath)){
            $the_url = app("App\Http\Controllers\PostsController")->saveImg($post, $imgPath);
            $this->info('Imagen: ' .  asset($the_url));
            Post::where('facebook_id', $post->facebook_id)->update([
                'attachment' => $the_url]);
            $works++;
        }
        if($appUsage['call_count'] > $limiter){
         $this->info('Actualizando el post: ' . $post->facebook_id . ' Imagen: ' . $imgPath);
         $this->info(date('Y-m-d H:i:s') . 'Proceso terminado! Posts Actualizados correctamente:'. $works. 'Post eliminados o fallidos:' . $failure);
         return null;
 }
}catch(Facebook\Exceptions\FacebookResponseException $e){
   $failure++;
   $this->error($e->getMessage());
}catch(\Exception $m){
   $failure++;
   $this->error($m->getMessage());
}
}
}
   //  }
}

protected function checkIfExists($url){
   $ch = curl_init();
   curl_setopt($ch, CURLOPT_URL, $url);
   curl_setopt($ch, CURLOPT_HEADER, 1);
   curl_setopt($ch , CURLOPT_RETURNTRANSFER, 1);
   $data = curl_exec($ch);
   $headers = curl_getinfo($ch);
   curl_close($ch);

   return $headers['http_code'];
} 

}
