<?php

/*-------------------------------------------------------*
 * Método que verifica si hay datos nuevos en el scraper y los pide para 
 llenar la metrica de video Graph de los posts en Graph.
*-------------------------------------------------------*/

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DateTime;
use App\Http\Controllers\PostsController;
use App\Http\Controllers\InstaPostController;

class GetScrapMetrics extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'GetScrapMetrics:getmetrics';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Retrieves data from the scraper';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
   public function handle(PostsController $retriever, InstaPostController $ipc)
    {
        $this->info(date("Y-m-d H:i:s") . ' Inicio de recuperacion de datos');
        $this->retrieveData($retriever); 
        $this->retrieveInstagramData($ipc); 
        $this->retrieveInstagramPosts($ipc);  
    }
    protected function retrieveData(PostsController $retriever)
    {
       $result = $retriever->getData();
       $this->info(date("Y-m-d H:i:s") . ' Datos recuperados de Views con exito' . 'datos recuperados: ' . $result);
    }
    protected function retrieveInstagramData(InstaPostController $ipc)
    { 
       $result = $ipc->getInstaMetrics();
       $this->info(date("Y-m-d H:i:s") . ' Datos Insta recuperados con exito' . 'datos recuperados: ' . $result);
    }
    protected function retrieveInstagramPosts(InstaPostController $ipc)
    {
        $result = $ipc->retrieveInstagramPosts();
        $this->info(date("Y-m-d H:i:s") . ' Post insta recuperados con exito ' . ' Posts recuperados: ' . $result[0] . ' Posts nuevos agregados: ' . $result[1]);
    }
}
