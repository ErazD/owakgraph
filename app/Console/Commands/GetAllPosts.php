<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Post;
use App\Fanpage;
use App\Scrap;
use App\Token;
use \SammyK\LaravelFacebookSdk\LaravelFacebookSdk as Facebook;
use App\YoutubeChannel;
use App\YoutubeVideo;
use App\YoutubeScrap;
use DateTime;
use Carbon\Carbon;
use App\Http\Controllers\YoutubeController;
// use Facebook\Exceptions\FacebookResponseException;

class GetAllPosts extends Command
{
/**
* The name and signature of the console command.
*
* @var string
*/
protected $signature = 'GetAllPosts:getposts';

/**
* The console command description.
*
* @var string
*/
protected $description = 'Get all posts and videos in fanpages saved';

/**
* Create a new command instance.
*
* @return void
*/
public function __construct()
{
    parent::__construct();
}

/**
* Execute the console command.
*
* @return mixed
*/
public function handle(Fanpage $fanpage, Facebook $fb, Post $pt, YoutubeChannel $channel, YoutubeController $yt, YoutubeVideo $video)
{

    // $this->getAllVideosYoutube($channel, $yt, $video);
    $this->GetAllPostsFacebook($fanpage, $fb, $pt);


}


protected function GetAllPostsFacebook(Fanpage $fanpage, Facebook $fb, Post $pt)
{
    $appUsage = [];
    $fanpages = Fanpage::orderBy('updated_at')->get();
    $fanpages->load('posts');
    // $token = (string) env('FACEBOOK_TOKEN');
    $token = Token::first()->token;
    foreach ($fanpages as $fanpage) {

        try{

            $scrap_data = Scrap::where('fanpage_id', $fanpage->id)->first();

            (!is_null($scrap_data)) ? $last_cron = $scrap_data->updated_at : $last_cron = (new \DateTime(date("Y")."-01-01")); 

            $since = strtotime($last_cron->format('Y-m-d'));
            $until = strtotime(date('Y-m-d H:i:s')) + 23 * 60 * 60;

            preg_match('#https?\://(?:www\.)?facebook\.com/(\d+|[A-Za-z0-9\.]+)/?#', $fanpage->url, $fanpage_name);

            $posts = $fb->get($fanpage_name[1].'/posts?fields=shares.summary(true),comments.limit(0).summary(true),reactions.limit(0).summary(true),message,type,permalink_url,attachments,created_time&since='.$since.'&until='.$until.'', $token);
            $appUsage = $posts->getHeaders()['x-app-usage'];
            $this->info('Uso de la app: '.$appUsage);
            $appUsage = json_decode($appUsage, true);

            $graphEdge = $posts->getGraphEdge();

            do {
                foreach ($graphEdge as $post) {
                    $pt->add($post, $fanpage->id, $fanpage->brand->scrap_type);
                }
            }while($graphEdge = $fb->next($graphEdge));

            if(is_null($scrap_data)){
                Scrap::create([
                    'fanpage_id' => $fanpage->id,
                    'updated_at' => date('Y-m-d H:i:s'),
                ]);
            }else{
                $scrap_data->update([
                    'updated_at' => date('Y-m-d H:i:s')
                ]);
            }

             if($appUsage['call_count'] > 85){ // Si se ha usado más del 85% de los llamados disponibles, deja de realizar pedidos
                $this->info('Se ha llegado al límite de pedidos permitidos por facebook (pedidos fanpages).');
                return null;
            }
        } catch (\Facebook\Exceptions\FacebookResponseException $e){
            $this->error('No se encuentra la fanpage '.$fanpage->brand->name.' '.$fanpage->name);
        }catch (\Exception $e){
            $this->error('Error en '.$fanpage->brand->name.' '.$fanpage->name);
        }

        $fanpage->update([
            'updated_at' => date('Y-m-d H:i:s')
        ]);
    }
}


protected function getAllVideosYoutube(YoutubeChannel $channel, YoutubeController $yt, YoutubeVideo $video)
{

    $allChannels = YoutubeChannel::all();    

    
    foreach ($allChannels as $channel) {
        try {

            $scrap_data = YoutubeScrap::where('youtube_channel_id', $channel->id)->first();

            (is_null($scrap_data))? $last_cron = (new \DateTime(date("Y")."-01-01")): $last_cron = $scrap_data->updated_at; 


            
            //Scrap desde
            $since_validation = new DateTime($last_cron->format('Y-m-d'));
            $since = date_format($since_validation, "Y-m-d\TH:i:sP"); 
            //Scrap hasta
            $until_validation = new DateTime(date('Y-m-d H:i:s'));
            $until_validation->modify("23:59:59");
            $until = date_format($until_validation, "Y-m-d\TH:i:sP");
            
            //dd('Fecha inicial y final de scrap: ',  $since, $until);
             dd('Fechas de validación: ', $since_validation, $until_validation);

            $youtube = $yt->getYoutubeService();
            $channel_id = $channel-> youtube_id;

            $pageToken="";
            $allVideoIds= array();
            do{
                $searchResponse = $youtube->search->listSearch('snippet', array(
                    'channelId' => $channel_id,
                    'type' => 'video',
                    'order' => 'date',
                    'publishedAfter' => $since,
                    'publishedBefore' => $until,
                    'maxResults' => 50,
                    'fields' => 'items(id(videoId)),nextPageToken',
                    'pageToken' => $pageToken
                ));
                $videoResults = array();
                $pageToken=$searchResponse['nextPageToken'];

                foreach ($searchResponse['items'] as $searchResult) {
                    array_push($videoResults, $searchResult['id']['videoId']);
                }
                $videoIds = join(',', $videoResults);
                array_push($allVideoIds, $videoIds);
            }while ($pageToken != "");
            
            foreach ($allVideoIds as $videoIds) {
                $videosResponse = $youtube->videos->listVideos('snippet, statistics', array(
                    'id' => $videoIds,
                    'fields' => 'items(id,snippet(description,publishedAt,thumbnails/high/url,title),statistics(commentCount,dislikeCount,likeCount,viewCount))'
                ));

                foreach ($videosResponse['items'] as $videoResult) {
                    $com = $videoResult['statistics']['commentCount'];
                    if(!$com){
                        $com=0;
                    }
                    $fecha_format = new DateTime($videoResult['snippet']['publishedAt']);
                    $fecha = date_format($fecha_format, 'Y/m/d H:i:s');
                   
                    
                    $rep = array(
                        'youtube_channel_id' => $channel_id,
                        'youtube_id' => $videoResult['id'],
                        'title' => $videoResult['snippet']['title'],
                        'content' => $videoResult['snippet']['description'],
                        'type' => "Video",
                        'permalink_url' => "https://www.youtube.com/watch?v=".$videoResult['id'],
                        'attachment' => $videoResult['snippet']['thumbnails']['high']['url'],          
                        'published' =>  $fecha,            
                        'likes' => $videoResult['statistics']['likeCount'],
                        'dislikes' => $videoResult['statistics']['dislikeCount'],
                        'comments' => $com,
                        'views' => $videoResult['statistics']['viewCount']
                    );
                    $video->add($rep, $channel->id);           
                }
            }
            if(is_null($scrap_data)){
                YoutubeScrap::create([
                    'youtube_channel_id' => $channel->id,
                    'updated_at' => date('Y-m-d H:i:s'),
                ]);
            }else{
                $scrap_data->update([
                    'updated_at' => date('Y-m-d H:i:s')
                ]);
            }
            
        } catch (\Google_Service_Exception $e) {
            $this->error($e->getMessage());
        } catch (\Google_Exception $e) {
            $this->error($e->getMessage());
        }   
    }
}
}
