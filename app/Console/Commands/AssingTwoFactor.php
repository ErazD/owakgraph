<?php

/*-------------------------------------------------------*
 * Comando para enviar posts recientes de tipo video para hacer scrap la metrica de vistas
*-------------------------------------------------------*/

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DateTime;
use Config;
use App\Http\Controllers\TwoFactorAuthenticationController;

class AssingTwoFactor extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'AssingTwoFactor:assing {social_network=Facebook}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Assigns post from a social networ to TwoFactorProfiles with TwoFactorAuthentication.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(TwoFactorAuthenticationController $two)
    {
        //indicador a los posts para saber cuales redes sociales se van a scrapear
        $social_network = $this->argument('social_network');
        if($social_network == 'all'){
            $socials_n = Config::get('constants.social_networks');
            foreach ($socials_n as $key => $value) {
                $this -> sendScrapRequest($two, $value['name']);    
            }
        }else{
            $this -> sendScrapRequest($two, $social_network);
        }
    }


    /**
     * @param App\TwoFactorAuthenticationController $two: Controlador para llamar al método addPostsToTwoFactor.
     * @param String $social_network: Nombre de la red social.
     *
    */
    protected function sendScrapRequest(TwoFactorAuthenticationController $two, $social_network)
    {
        //$result = "";
        $result= $two->addPostsToTwoFactor($social_network);
        $this -> info(date("Y-m-d H:i:s") . ' Se han asignado los posts de '.$social_network.'.');
        $this -> info($result);
    }
}
