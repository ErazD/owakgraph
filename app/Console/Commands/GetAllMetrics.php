<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Fanpage;
use App\Post;
use App\Token;
use App\Metric;
use \SammyK\LaravelFacebookSdk\LaravelFacebookSdk as Facebook;
use App\YoutubeChannel;
use App\YoutubeMetrics;
use App\YoutubeVideo;
use App\YoutubeScrap;
use DateTime;
use DB;
use Carbon\Carbon;
use App\Http\Controllers\YoutubeController;
use App\Http\Controllers\MethodsController;

class GetAllMetrics extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'GetAllMetrics:getmetrics {scrap_type=0}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get Metric by post saved.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(Fanpage $fanpages, Metric $metric, Facebook $fb, YoutubeChannel $channel, YoutubeMetrics $youtubemetric, YoutubeController $yt)
    {

     // scrap_type: indicador en los posts para saber cada cuanto se hace scrap, 0:hora, 1:diario, 2:semanal, 3:mensual(publicaciones de más de un año), 4:anual, -1:no se hace
        $scrap_type = $this->argument('scrap_type');
        $this->getAllYoutubeMetrics($channel, $youtubemetric, $yt, $scrap_type);
        $this->getAllFacebookMetrics($fanpages, $metric, $fb, $scrap_type);
    }


    /**
     * Se recorren todos los post de todas las fanpages para actualizar sus métricas.
     *
     * @param {Fanpage} Modelo para controlar los objetos fanpage.
     * @param {Metric} Modelo para crear las métricas.
     * @param {Facebook} Clase con los métodos para conectar con facebook.
     * @param {int} indicador para filtrar los post que se actualian según el valor dado. 0:hora, 1:diario, 2:semanal, 3:mensual(publicaciones de más de un año), 4:anual, -1:no se actualiza
     */
    protected function getAllFacebookMetrics(Fanpage $fanpages, Metric $metric, Facebook $fb, $scrap_type)
    {
        //$token = env('FACEBOOK_TOKEN');
        $token = Token::first()->token;

        $appUsage = [];
        // $cant = 0;
        // try {
        //     $stat = $fb->get('156938611533378?fields= daily_active_users', $token);
        //     $appUsage = $stat->getHeaders()['x-app-usage'];
        //     $this->info('Uso de la app: '.$appUsage);
        //     $appUsage = json_decode($appUsage, true);   
        //     $stat = $stat->getGraphObject();
        //     $cant = $stat['daily_active_users'];
        // } catch (\Facebook\Exceptions\FacebookSDKException $e) {
        //         // dd($e->getMessage());
        //     $this->error("Error al consultar uso de llamados: ".$e->getMessage());       
        // }
        // $this->info(date("Y-m-d H:i:s").' Usuarios Activos: '.$cant);

        // if($cant==0 || $appUsage['call_count'] > 85) {// aproximadamente 650 posts por usuario
        //     $this->info('Se ha llegado al límite de pedidos permitidos por facebook.');
        //     return null;
        //  }  


        // $fanpages = Fanpage::where('active', 1)->get();
        // $postsArray = array();
        // foreach ($fanpages as $fanpage) {
        $postsArray =  Post::whereHas('fanpage', function($query){
            $query->where('fanpages.active', 1);
        })
        ->where('scrap_type',  $scrap_type)
        ->orderBy('posts.updated_at')
        ->get();
            // $posts = ($fanpage->posts()->where('scrap_type',  $scrap_type)->get());
            // array_push($postsArray, $posts);
        // }


           // $this->info(date("Y-m-d H:i:s").'   -  ' .sizeof($postsArray).'   '.$cant. ' Mensaje.'.$scrap_type);
           // return null;


        foreach ($postsArray as $detail) {
            // foreach ($post as $detail) {
            try {
             $stat = $fb->get($detail['facebook_id'].'?fields=shares.summary(true),comments.limit(0).summary(true),reactions.limit(0).summary(true)', $token);
             $appUsage = $stat->getHeaders()['x-app-usage'];
             $this->info('Uso de la app: '.$appUsage);
             $appUsage = json_decode($appUsage, true);
             $this->info('Actualización post: '.$detail['id'].': (FB: '.$detail['facebook_id'].')');               
             $stat = $stat->getGraphObject();
             $metric->addMetric($stat, $detail['id']);
             $detail->update([
                'updated_at' => date('Y-m-d H:i:s')
            ]);
             MethodsController::setScrapState($detail);

         } catch (\Facebook\Exceptions\FacebookSDKException $e) {
            $appUsage = $e->getResponse()->getHeaders()['x-app-usage'];
            $this->info('[Error] '.date("Y-m-d H:i:s").': uso de la app: '.$appUsage);
            $appUsage = json_decode($appUsage, true);
            //dd($e, $e->getErrorType(), $e->getCode(), $e->getSubErrorCode());
            $newInfo = [
                'updated_at' => date('Y-m-d H:i:s')
            ];            
            $ecode = $e->getCode();
            $esubcode = $e->getSubErrorCode();
            if($ecode == 100 && $esubcode = 33){
                $newInfo['scrap_type'] = -2;
            }
            $detail->update($newInfo);
            $this->error($e->getMessage());

        }
        // }

        if($appUsage['call_count'] > 85){ // Si se ha usado más del 85% de los llamados disponibles, deja de realizar pedidos
            $this->info('Se ha llegado al límite de pedidos permitidos por facebook (pedidos posts).');
            return null;
        }
    }
}


    /**
     * Se recorren todos los post de todas las fanpages para actualizar sus métricas.
     *
     * @param {YoutubeChannel} Modelo para controlar los canales de YouTube.
     * @param {YoutubeMetrics} Modelo para crear las métricas.
     * @param {YoutubeController} Clase que instancia el objeto para conectar con YouTube.
     * @param {int} indicador para filtrar los post que se actualian según el varlo dado. 0:hora, 1:diario, 2:semanal, 3:mensual(publicaciones de más de un año), 4:anual, -1:no se actualiza
     */
    protected function getAllYoutubeMetrics(YoutubeChannel $channels, YoutubeMetrics $metric, YoutubeController $yt, $scrap_type)
    {

        $channel_videos = YoutubeVideo::where('scrap_type',$scrap_type)->get();

        $cant = 0;
        $youtube = $yt->getYoutubeService();
        $vd_ids = array();
        $all_vd_ids = array();
        foreach ($channel_videos as $post) {
            // if($post->scrap_type == $scrap_type){ 
            array_push($vd_ids, $post->youtube_id);
            $cant++;
            if($cant >= 50){
                $videoIds = join(',', $vd_ids);
                array_push($all_vd_ids, $videoIds);
                $vd_ids= array();
                $cant = 0;
            }
        // }
        }       

        
        foreach ($all_vd_ids as $videoIds) {
         try{
            $videosResponse = $youtube->videos->listVideos('statistics', array(
                'id' => $videoIds,
                'fields' => 'items(id,statistics(commentCount,dislikeCount,likeCount,viewCount))'
            ));
            foreach ($videosResponse['items'] as $videoResult) {
                $youtube_id = $videoResult['id'];
                $com = $videoResult['statistics']['commentCount'];
                if(!$com){
                    $com=0;
                }
                $stat = array(
                    'likes' => $videoResult['statistics']['likeCount'],
                    'dislikes' => $videoResult['statistics']['dislikeCount'],
                    'comments' => $com,
                    'views' => $videoResult['statistics']['viewCount']
                );
                $vide=$channel_videos->where('youtube_id',$youtube_id)->first();
                $new_scrap = $metric->addMetric($stat, $vide['id']); 
                MethodsController::setScrapState($vide);
                // if($scrap_type != $new_scrap){
                //     $vide->$scrap_type = $new_scrap;
                //     $vide->save();
                // }     
            }

        } catch (\Google_Service_Exception $e) {
            dd($e);
        } catch (\Google_Exception $e) {
            dd($e);
        }
    }
    
}








}
