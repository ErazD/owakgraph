<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Report;
use App\YoutubeReport;

class DeleteAllReports extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'DeleteAllReports:deletereports';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete All old reports from Facebook and YouTube';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Report::truncate();
        YoutubeReport::truncate();
    }
}
