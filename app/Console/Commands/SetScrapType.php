<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class SetScrapType extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'setScrapType:set';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command to set the scrap frequency of posts';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(Post $post)
    {
        $this->setAllScrapTypes($post);
    }

    protected function setAllScrapTypes(Post $post){

      $posts = Post::get();
      $extinto = 0;
      $anual = 0;
      $mensual = 0;
      $semanal = 0;
      $diario = 0;
      $renovado = 0;

      foreach ($posts as $key => $post) {
             // $infoFrequency = MethodsController::setScrapState($post);
         $lastDate = $post->latestMetric;
         $date = strtotime($lastDate->created_at);

         if( $date < strtotime('-1 year')){
            $post->scrap_type = -1;
            $extinto ++;
        } else if($date < strtotime('-6 months')){
            $post->scrap_type = 4;
            $anual ++;
        } else if($date < strtotime('-2 months')){
         $post->scrap_type = 3;
         $mensual ++;
     }else if($date < strtotime('-3 weeks')){
        $post->scrap_type = 2;
        $semanal++;
    }else if($date < strtotime('-2 days')){
        $post->scrap_type = 1;
        $diario++;
    }else{
        $post->scrap_type = 0;
        $renovado++;
    }

    $post->save();

}
$this->info('Resultado de las frecuencias: ' . ' diario ' . $diario . ' semanal ' . $semanal . ' mensual ' . $mensual . ' anual ' . $anual . ' renovado ' . $renovado .' extinto  ' . $extinto);     
}
}