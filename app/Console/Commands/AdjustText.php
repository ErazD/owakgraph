<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Post;
use App\Fanpage;
use App\Brand;

class AdjustText extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'text:correct';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Removes unwanted characters from string';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(Fanpage $fanpages)
    {
        $this->correctStrings($fanpages);
    }
    protected function correctStrings(Fanpage $fanpages)
    {  
       $convertido = 0; 
       $fanpages = Fanpage::all();
       foreach($fanpages as $fanpage){
      
        $posts = $fanpage->posts()->get();
         foreach($posts as $post){
            
              if (!is_null($post->content)) {
                if(strpos($post->content, '??') !== false || strpos($post->content, '???') !== false || strpos($post->content, '????') !== false || strpos($post->content, '????????????????') !== false || strpos($post->content, '????????') !== false){ 
                  $this->info('Texto original:'. $post->content);
                  $adjustText = str_replace("????", " ", $post->content); 
                  // $adjustText = str_replace("???", " ", $post->content); 
                  // $adjustText = str_replace("????", " ", $post->content); 
                  // $adjustText = str_replace("????????????????", "", $post->content); 
                  // $adjustText = str_replace("????????", " ", $post->content); 
                  
                  $this->info('Texto corregido: ' . $adjustText);      
                  $post->content = $adjustText;
                  $post->save();
                  $convertido++;
              }
            }   
        }
       }
        $this->info('Posts ajustados:' . $convertido);
    }
}
