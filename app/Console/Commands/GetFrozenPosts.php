<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DateTime;
use App\Http\Controllers\PostsController;

class GetFrozenPosts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'FrozenPosts:getfrozen';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Retrieves blocked data from the scraper';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(PostsController $check)
    {
        $this->info(date('Y-m-d H:i:s') . 'Verificacion de posts bloqueados o congelados');
        $this->checkFrozenData($check);
    }
    protected function checkFrozenData(PostsController $check){
        $check->freezeData();
        $this->info(date("Y-m-d H:i:s") . 'Verificacion de datos bloqueados Exitosa');
    }
}
