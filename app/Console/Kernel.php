<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
    \App\Console\Commands\DeleteAllReports::class,
    \App\Console\Commands\GetAllPosts::class,
    \App\Console\Commands\GetAllMetrics::class,
    \App\Console\Commands\SetAttachPosts::class,
    \App\Console\Commands\SetFacebookID::class,
    \App\Console\Commands\ScrapMetrics::class,
    \App\Console\Commands\GetScrapMetrics::class,
    \App\Console\Commands\GetFrozenPosts::class,
    \App\Console\Commands\AssingTwoFactor::class,
    \App\Console\Commands\GetFansCount::class,
    \App\Console\Commands\SetScrapType::class,
    \App\Console\Commands\AdjustText::class,
     \App\Console\Commands\SendInstaProfiles::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();


        $schedule->command('GetAllPosts:getposts')->cron('0 */3 * * *')->appendOutputTo(storage_path('logs/posts.log'));
        $schedule->command('GetAllMetrics:getmetrics')->cron('0 */2 * * *')->appendOutputTo(storage_path('logs/metrics.log'));
        $schedule->command('GetAllMetrics:getmetrics 1')->daily()->appendOutputTo(storage_path('logs/metrics.log'));
        $schedule->command('GetAllMetrics:getmetrics 2')->weekly()->appendOutputTo(storage_path('logs/metrics.log'));
        $schedule->command('GetAllMetrics:getmetrics 3')->monthly()->appendOutputTo(storage_path('logs/metrics.log'));
        $schedule->command('GetAllMetrics:getmetrics 4')->yearly()->appendOutputTo(storage_path('logs/metrics.log'));
        $schedule->command('fancount:getfans')->cron('0 */2 * * *');
        //Scrap
        // $schedule->command('ScrapMetrics::getmetrics')->daily();
        $schedule->command('ScrapMetrics:getmetrics')->cron('0 */4 * * *')->appendOutputTo(storage_path('logs/scraplog.log'));
        $schedule->command('ScrapMetrics:getmetrics 1')->dailyAt('1:00')->appendOutputTo(storage_path('logs/scraplog.log'));
        $schedule->command('ScrapMetrics:getmetrics 2')->weekly()->mondays()->at('1:00')->appendOutputTo(storage_path('logs/scraplog.log'));
        $schedule->command('ScrapMetrics:getmetrics 3')->monthlyOn(1, '1:00')->appendOutputTo(storage_path('logs/scraplog.log'));
        $schedule->command('ScrapMetrics:getmetrics 4')->yearly()->appendOutputTo(storage_path('logs/scraplog.log'));
        $schedule->command('GetScrapMetrics:getmetrics')->cron('0 */6 * * *')->appendOutputTo(storage_path('logs/sendlog.log'));
        $schedule->command('getFrozenPosts:getfrozen')->cron('0 */6 * * *')->appendOutputTo(storage_path('logs/sendlog.log'));
        $schedule->command('AssingTwoFactor:assing')
        ->daily();
        
        $schedule->command('DeleteAllReports:deletereports')
        ->weekly();

         $schedule->command('profiles:send')->daily()->appendOutputTo(storage_path('logs/scraplog.log'));
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
