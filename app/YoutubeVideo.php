<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\YoutubeMetrics;

class YoutubeVideo extends Model
{
    
	protected $guarded = [];

	 /*-------------------------------------------------------*
     * Obtener relación entre un Video y su parent channel.
     * @return Object
     *-------------------------------------------------------*/
	 public function channel()
	 {
	 	return $this->belongsTo(YoutubeChannel::class, 'youtube_channel_id');
	 }
	 public function brand(){
	 	return $this->channel->brand;
	 }
	 public function social_network(){
	 	return $this->channel->social_network;
	 }

	 /**
     * Obtiene la Relación con los permisos que han sido asociados a su marca.
     * @return Object
     */
     public function industryRoles(){
        return $this->channel->industryRoles(); 
     }

	 /**
	 * Retorna el límite asignado para el scrap de los posts
	 * @return {int} límite seleccionado del post o límite en el elemento que lo contiene.
	 */
	 public function selectedScrap(){
	 	if($this->selected_scrap_type !== null)
	 		return $this->selected_scrap_type;
	 	else 
	 		return $this->channel->selectedScrap();
	 }
	/*-------------------------------------------------------*
     * Obtener relación entre un video y todas sus métricas.
     * @return Collection
     *-------------------------------------------------------*/
	public function metrics()
	{
		return $this->hasMany(YoutubeMetrics::class);
	}


	/*-------------------------------------------------------*
     * Obtener relación entre un Video y su última métrica.
     * @return Object
     *-------------------------------------------------------*/
	public function latestMetric()
	{
		return $this->hasOne(YoutubeMetrics::class)->latest();
	}


	/*-------------------------------------------------------*
     * Obtener relación entre un Video y sus métricas el día de hoy.
     * @return Collection
     *-------------------------------------------------------*/

	public function metrics_per_day()
	{
		return $this->hasMany(YoutubeMetrics::class)->where('created_at', '>=', \Carbon\Carbon::today());
	}


	/*-------------------------------------------------------*
     * Obtener relación entre un Video y sus métricas en la semana.
     * @return Collection
     *-------------------------------------------------------*/

	public function metrics_per_week()
	{
		return $this->hasMany(YoutubeMetrics::class)->whereBetween('created_at', [
			\Carbon\Carbon::parse('last monday')->startOfDay(),
			\Carbon\Carbon::parse('next friday')->endOfDay(),
		])->latest();

	}

	/*-------------------------------------------------------*
     * Obtener relación entre un Video y sus métricas en el mes.
     * @return Collection
     *-------------------------------------------------------*/

	public function metrics_per_month()
	{
		return $this->hasMany(YoutubeMetrics::class)->whereBetween('created_at', [
			\Carbon\Carbon::now()->startOfMonth(),
			\Carbon\Carbon::now()->endOfMonth(),
		])->latest();

	}

	/*-------------------------------------------------------*
     * Función que actualiza el attachment del Video.
     *-------------------------------------------------------*/

	public function setAttachmentField($src)
	{
		$this->update([
			'attachment' => $src,
		]);
	}

 	/*-------------------------------------------------------*
	 * Obtener relación entre el posts de facebook y los concurso a los que pertenece.
	 * @return collection
	 *-------------------------------------------------------*/
 	public function contests()
 	{
 		return $this->belongsToMany(Contest::class,'contest_post','post_id','contest_id')->where('social_network', 'YouTube');
 	}

    /*-------------------------------------------------------*
     * Obtener relación entre el post de facebook y el concurso.
     * @return collection
     *-------------------------------------------------------*/
    public function contest()
    {
    	return $this->belongsToMany(Contest::class,'contest_post','post_id','contest_id')->where('social_network', 'YouTube')->first();
    }


	/*-------------------------------------------------------*
     * Obtener relación entre un Post y sus elementos de two factor authentication.
     * @return Collection
     *-------------------------------------------------------*/
	 public function twofactors()
	 {
	 	return $this->hasMany(TwoFactorAuthentication::class, 'post_id')->where('social_network', 'YouTube');
	 }

	 /*-------------------------------------------------------*
     * Verifica el estado de los valores del comms, si están todos los campos obligatorios, si tiene twofactor en proceso, o si no tiene nada asignado. este resultado lo guarda en el triibuto local  'twoFactorStatus'.
     * @return 2 si tiene los campos obligatorios, 1 si tiene al menos dos twofactors iniciados, 0 si no tiene suficientes twoFactors para un match.
     *-------------------------------------------------------*/
	 public function getTwoFactorStatusAttribute()
	 {
	 	$twoFactorStatus = 0;

	 	if(
	 		$this->type != null && 
	 		$this->balance != null && 
	 		$this->comunication != null && 
	 		$this->theme != null && 
	 		($this->contest() != null || $this->theme != 'concurso') && 
	 		$this->product_id != null &&
	 		$this->canasta_id != null &&
	 		$this->micro_moment != null &&
	 		$this->business_objective != null
	 	){
	 		$twoFactorStatus = 2;
	 	}else {
	 		$twfMax = 0;
	 		foreach ($this->twofactors as $key => $twf) {
	 			$twfMax = max($twfMax, $twf->status);
	 		}
	 		if($twfMax >= 2){
	 		 		$twoFactorStatus = 1;
	 		 	}
	 	}
	 	return $twoFactorStatus;
	 }

	  /*-------------------------------------------------------*
     * Consulta si el usuario ha sido asignado para clasificar los comms del post.
     * @param $user: usuario para realiar la consulta.
     * @return true si el usuario existe, false en caso contrario.
     *-------------------------------------------------------*/
	 public function hasUser($user = false){
	 	if(!$user && Auth::check()){
    	  	$user = Auth::user();
    	}
	 	$has = false;
	 	if($user && $user->twoFactorProfile){
	 		$ele = $this->twofactors()->where('profile_id', $user->twoFactorProfile->id)->first();
	 		if($ele){
	 			$has=true;
	 		}
	 	}
	 	return $has;
	 }


	/*-------------------------------------------------------*
     * Añadir Video
     *-------------------------------------------------------*/

	public function add($video, $channel_id, $scrap_type=0)
	{	
		//Preguntar si este post ya ha sido añadido
		$saved = $this->where('youtube_id', $video['youtube_id'])->first();

		//Si no está
		if(is_null($saved)){

			//Se crea un post
			$video_created = $this->create([
				'youtube_channel_id' => $channel_id,
				'youtube_id' => $video['youtube_id'],
				'title' => $video['title'],
				'content' => $video['content'],
				'type' => $video['type'],
				'permalink_url' => $video['permalink_url'],
				'attachment' => $video['attachment'],
				'published' =>$video['published'],
				'scrap_type' => $scrap_type
			]);

			//Se crea la métrica inicial de ese post
			YoutubeMetrics::create([
				'youtube_video_id' => $video_created->id,
				'likes' => $video['likes'],
				'dislikes' => $video['dislikes'],
				'comments' => $video['comments'],
				'views' => $video['views'],
			]);

		}
	}


	/*-------------------------------------------------------*
     * Añadir Post Manualmente, existen DARK Posts, que no son visibles
     * por el scrap de OWAK Graph.
     *-------------------------------------------------------*/

	public function manualAdd($video, $channel_id, $scrap_type=0)
	{

		$video_created = $this->create([
			'youtube_channel_id' => $channel_id,
			'youtube_id' => $video['youtube_id'],
			'title' => $video['title'],
			'content' => $video['content'],
			'type' => $video['type'],
			'permalink_url' => $video['permalink_url'],
			'attachment' => $video['attachment'],
			'published' => $video['published'],
			'scrap_type' => $scrap_type
		]);

		YoutubeMetrics::create([
			'youtube_video_id' => $video_created->id,
				'likes' => $video['likes'],
				'dislikes' => $video['dislikes'],
				'comments' => $video['comments'],
				'views' => $video['views'],
		]);

		return $video_created;

	}

}
