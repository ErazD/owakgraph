<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fanpage extends Model
{
	protected $guarded = [];

       public function messyPosts()
     {
          return $this->hasMany(Post::class); 
     }

     public function snPosts($country="all", $socialNetwork="all", $filterObj=null){
      $snPosts = collect();
      if (($socialNetwork == 'all' || strtolower($socialNetwork) == strtolower($this->social_network)) &&
          ($country == "all" || strtolower($country) == strtolower($this->name)))
          $snPosts = ($filterObj)? $filterObj->filterPosts($this->messyPosts(), 'posts')->get(): $this->messyPosts()->get();
      return $snPosts;
     }
     
     /*-------------------------------------------------------*
     * Obtener relación entre Fanpage y sus posts
     * @return collection
     *-------------------------------------------------------*/
     public function posts()
     {
     	return $this->hasMany(Post::class)->orderBy('published', 'DESC');
     }

     /**
     * Obtiene el query de los posts dentro de un arreglo.
     * @return {Array<Object>}: arreglo de un objeto que contiene el query de posts.
     */
     public function postsArrayQueries()
     {
      return [$this->posts()];
    }

    /**
     * Obtiene la Relación entre una canasta y los permisos que han sido asociados a sus marcas.
     * @return Object
     */
     public function industryRoles(){
        return $this->brand->industryRoles(); 
     }

    public  function getFanCount()
    {
      return FanCount::where('table', 'fanpages')->where('element_id', $this->id);
    }

    public function getLastFanCount()
    {
      return FanCount::where('table', 'fanpages')->where('element_id', $this->id)->latest();
    }

     /*-------------------------------------------------------*
     * Obtener relación entre Fanpage y su parent brand.
     * @return Object
     *-------------------------------------------------------*/
     public function brand(){
     	return $this->belongsTo(Brand::class);
     }


      /**
      * Retorna el límite asignado para el scrap de los posts
      * @return {int} límite seleccionado del post o límite en el elemento que lo contiene.
      */
      public function selectedScrap(){
        if($this->scrap_type !== null)
         return $this->scrap_type;
       else 
         return $this->brand->selectedScrap();
     }
   }
