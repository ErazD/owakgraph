<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use DateTime;

class YoutubeReport extends Model
{
	protected $guarded = [];
	protected $dates = ['published'];

/*-------------------------------------------------------*
     * Guardar Reporte Youtube en la base de datos.
 *-------------------------------------------------------*/

public function saveReport($report, $id)
{
	$d=str_replace("T"," ",$report['published']);
		// $d=str_replace("-","/",$d);
	$d = explode('.', $d,2)[0];
		//dd($d);
	$date = new DateTime($d);

	$this->create([
		'report_id' => $id,
		'title' => $report['title'],
		'content' => $report['content'],
		'likes' => $report['likes'],
		'dislikes' => $report['dislikes'],
		'comments' => $report['comments'],
		'views' => $report['views'],
		'type' => $report['type'],
		'url' => $report['url'],			
		'published' => $d
		]);
    }
}