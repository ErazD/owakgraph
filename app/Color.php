<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Color extends Model
{
    protected $guarded = [];
    /**
    * @return {Object}: Obtiene el elemento al que pertenece el color.
    */
    public function element(){
    	return DB::table($this->table)->where('id', $this->element_id)->first();
    }

    
}
