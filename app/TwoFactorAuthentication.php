<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Config;

class TwoFactorAuthentication extends Model
{
     protected $guarded = [];
     protected $appends = ['status'];

     /*-------------------------------------------------------*
     * Obtener relación del usuario al que pertenece.
     * @return Object
     *-------------------------------------------------------*/
     public function user(){
          return $this->profile->user();
     }

     /*-------------------------------------------------------*
     * Obtener relación del perfil al que pertenece.
     * @return Object
     *-------------------------------------------------------*/
     public function profile(){
          return $this->belongsTo(TwoFactorProfile::class);
     }

     /*-------------------------------------------------------*
     * Obtener relación del post al que pertenece.
     * @return Object
     *-------------------------------------------------------*/
     public function post(){
          $consts = Config::get('constants.social_networks');
          $snSlug = strtolower($this->social_network);
          $snInfo = $consts[$snSlug];
          if($snInfo){
               $snInfo = $snInfo['models'];
               return $this->belongsTo($snInfo['post'], $snInfo['post_id']);
          }
          return null;
          // switch ($this->social_network) {
          //      case $consts['facebook']['name']:
          //           return $this->belongsTo(Post::class);
          //           break;
          //      case $consts['youtube']['name']:
          //           return $this->belongsTo(YoutubeVideo::class, 'post_id');
          //           break;               
          //      default:
          //           return null;
          //           break;
          // }
     }

     /*-------------------------------------------------------*
     * Obtener relación del producto que se asignará al post.
     * @return Object
     *-------------------------------------------------------*/
     public function product(){
          return $this->belongsTo(Product::class);
     }

     /*-------------------------------------------------------*
     * Obtener relación del producto que se asignará al post.
     * @return Object
     *-------------------------------------------------------*/
     public function canasta(){
          return $this->belongsTo(Canasta::class);
     }

     /*-------------------------------------------------------*
     * Obtener relación del concurso que se asignará al post.
     * @return Object
     *-------------------------------------------------------*/
     public function contest(){
          return $this->belongsTo(Contest::class);
     }

     /*-------------------------------------------------------*
     * Verifica el estado de los valores del comms, si están todos los campos obligatorios, si tiene alguno, o si no tiene nada asignado. este resultado lo guarda en el atributo local  'status'.
     * @return 2 si tiene los campos obligatorios, 1 si tiene alguno asignado, 0 si no tiene.
     *-------------------------------------------------------*/
     public function getStatusAttribute(){
          $status = 0;
          if(
               $this->type != null && 
               $this->balance != null && 
               $this->communication_task != null && 
               $this->communication_theme != null && 
                ($this->contests_id != null || $this->communication_theme != 'concurso') && 
               $this->product_id != null &&
               $this->canasta_id != null &&
               $this->micro_moment != null &&
               $this->business_objective != null
          ){
               $status=2;
          }else if(
               $this->balance != null || 
               $this->communication_task != null || 
               $this->communication_theme != null || 
               $this->product_id != null ||
               $this->canasta_id != null ||
               $this->micro_moment != null ||
               $this->business_objective != null
          ){
               $status=1;
          }
          return $status;
     }


}
