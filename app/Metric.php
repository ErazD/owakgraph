<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Metric extends Model
{
	protected $guarded = [];

	 /*-------------------------------------------------------*
     * Obtener relación entre las métricas y su parent post.
     * @return Object
     *-------------------------------------------------------*/
	 public function post(){
	 	return $this->belongsTo(Post::class);
	 }



	 /*-------------------------------------------------------*
     * Añadir Métrica.
     *-------------------------------------------------------*/
	 public function addMetric($post, $parent_post)
	 {

	 	//Obtener la ultima métrica del post.
	 	$last_metric = $this->where('post_id', $parent_post)->latest()->first();

	 	//Si el resultado es diferente a nulo
	 	if(!is_null($last_metric)){

	 		//Y los datos almacenados en la BD son diferentes a los actuales
	 		if($last_metric->reactions != $post['reactions']->getTotalCount() || $last_metric->comments != $post['comments']->getTotalCount() || $last_metric->shares != isset($post['shares']['count'])){

	 			//Se crea un nuevo record
	 			$this->create([
	 				'post_id' => $parent_post,
	 				'reactions' => $post['reactions']->getTotalCount(),
	 				'comments' => $post['comments']->getTotalCount(),
	 				'shares' => isset($post['shares']['count']) ? $post['shares']['count'] : 0,
	 			]);

	 		}

	 	}else{
	 		//Si era nulo, se crea la primera métrica para ese post.
	 		$this->create([
	 			'post_id' => $parent_post,
	 			'reactions' => $post['reactions']->getTotalCount(),
	 			'comments' => $post['comments']->getTotalCount(),
	 			'shares' => isset($post['shares']['count']) ? $post['shares']['count'] : 0,
	 		]);
	 	}

	 }
	}
