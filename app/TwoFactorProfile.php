<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TwoFactorProfile extends Model
{
	protected $guarded = [];
    
     /*-------------------------------------------------------*
     * Obtener relación del usuario al que pertenece.
     * @return Object
     *-------------------------------------------------------*/
	 public function user(){
	 	return $this->belongsTo(User::class);
	 }

	 /*-------------------------------------------------------*
     * Obtener relación entre un perfil y sus elementos de two factor authentication.
     * @return Collection
     *-------------------------------------------------------*/
	public function twofactors()
	{
		return $this->hasMany(TwoFactorAuthentication::class, 'profile_id');
	}
  
}
