<?php

namespace App\Http\Middleware;

use Closure;
use Redirect;
use App\User;
use App\UserRole;
use Auth;
use Session;

class CheckUserPermission
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = User::where('id',11)->first(); 
        $isAdmin = (UserRole::userHasPermissions($user, 'admin') == 1)? true : false;
        $isOwakean = (UserRole::userHasPermissions($user, 'owakean') == 1)? true : false;
       
        if($isAdmin || $isOwakean){
              return $next($request);
          }
        Session::flash('message', "Parece que no se encontro la ruta deseada");
        return redirect()->to('/');
    }
}
