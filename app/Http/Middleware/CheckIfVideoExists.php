<?php

namespace App\Http\Middleware;

use Closure;
use Session;
use Redirect;
use App\YoutubeVideo;

class CheckIfVideoExists
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $videoId = $request->video_url;
        $vId = explode('v=', $videoId);
        $videoResponse = '';
       
        if(strpos($vId[1], '&')){
         $vIdB = explode('&', vId[1]);
         $videoResponse = YoutubeVideo::where('youtube_id', $vIdB[0])->first(); 
        }else{
         $videoResponse = YoutubeVideo::where('youtube_id', $vId[1])->first();
        }
        if(is_null($videoResponse)){
            return $next($request);
        }else{
            Session::flash('message', "Este video ya fue añadido");
            return Redirect::back();
        }
  
    }
}
