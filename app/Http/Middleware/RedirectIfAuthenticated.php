<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use App\User;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        $user = (Auth::check())? Auth::user(): false;

        // $check = ($user)? $user : App\User::where('id',11)->first();
        // if (Auth::guard($guard)->check {
        //     return redirect('/home');
        // }
        if($user){
          return  redirect('/brands');   
        }else{

        return $next($request);
    }
  }
}
