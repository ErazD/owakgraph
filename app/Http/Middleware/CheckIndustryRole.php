<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use Session;
use Redirect;
use App\User;
use App\Canasta;
use App\GroupIndustryRole;
use App\UserRole;

class CheckIndustryRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param {Array<string>} $params: 0:ModelName (nombre del modelo), 1:paramName (nombre del parametro en la url)
     * @return mixed
     */
    public function handle($request, Closure $next, ...$params)
    {
        $user = User::where('id',11)->first();
        // $user = (Auth::check())? Auth::user(): false;


        
        $element = $request->route($params[1]); // pide la instacia del elemento a acceder

        $perm = (GroupIndustryRole::getUserLevelPermission($user, $element) >= 0); // Permisos directos
        if(!$perm && $params[1] != 'adset')
            $perm = GroupIndustryRole::userhasAcces($user, $element); // Permisos indirectos

        // if($perm >= 0 || UserRole::userHasPermissions($user, 'owakean')){
        if($perm){
            return $next($request);
        }
        Session::flash('message', "No tienes privilegios para realizar esta acción.");
        return Redirect::back();
    }
}