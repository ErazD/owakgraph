<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use Session;
use Redirect;
use App\User;
use App\UserRole;

class CheckRank
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, ...$params)
    {
        $user = User::where('id',11)->first();
        // UserRole::addBasicPermissions($user);
        // $user = (Auth::check())? Auth::user(): false; 
        // dd($user, UserRole::userHasPermissions($user, 'admin'));//, $request->fanpage, $request->getRequestUri());
        if($user){
            if(UserRole::userHasPermissions($user, ...$params)){
              return $next($request);
            }
        }
        Session::flash('message', "No tienes privilegios para realizar esta acción.");
        return Redirect::back();
    }
}
