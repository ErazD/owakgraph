<?php

namespace App\Http\Middleware;

use Closure;
use Session;
use Redirect;
use App\Post;

class CheckIfPostExists
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        preg_match('#https?\://(?:www\.)?facebook\.com/(\d+|[A-Za-z0-9\.]+)/?#', $request->input('url'), $url_post);
        preg_match('#https?\://(?:www\.)?facebook\.com/(\d+|[A-Za-z0-9\.]+)/?#', $request->input('fanpage_url'), $fanpage_post); 


        if(strtolower($url_post[1]) == strtolower($fanpage_post[1])){

            $post_id = basename(parse_url($request->input('url'), PHP_URL_PATH));

            $post = Post::where('facebook_id', 'LIKE', "%$post_id%")->first();

            if(is_null($post)){

                return $next($request);

            }else{

                Session::flash('message', "Esta publicación ya fue añadida");
                return Redirect::back();

            }

        }else{
            Session::flash('message', "Esta publicación no pertenece a esta Fanpage");
            return Redirect::back();
        }

    }
}
