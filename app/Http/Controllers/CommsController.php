<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\GroupIndustryRole;
use App\Canasta;
use App\Brand;
use App\Fanpage;
use App\AdSet;
use App\Post;
use App\Product;
use App\Contest;
use App\TwoFactorAuthentication;
use App\TwoFactorProfile;
use App\User;
use App\UserRole;
use App\YoutubeChannel;
use App\YoutubeVideo;
use App\InstaProfile;
use App\InstaPost;
use Config;
use DateTime;
use DateInterval;
use App\Http\Controllers\MethodsController;

class CommsController extends Controller
{

	public function index(){
		$user = User::where('id', 11)->first();
		$allElements = array(
			'fanpages' => GroupIndustryRole::getUserPermissionsElement($user, 'Fanpage')->get(),
			'channels' => GroupIndustryRole::getUserPermissionsElement($user, 'YoutubeChannel')->get(),
			'profiles' => GroupIndustryRole::getUserPermissionsElement($user, 'InstaProfile')->get(), 
			'brands' => GroupIndustryRole::getUserPermissionsElement($user, 'Brand')->get(), 
			'baskets' => GroupIndustryRole::getUserPermissionsElement($user, 'Canasta')->get(), 
			'adsets' => AdSet::getAdsetsFromUserId($user->id, 0)->get()
		);
		// return view('comms.comms',compact('allElements'));
		return view('comms.consults.comms',compact('allElements'));
	}

	public function adminCommsRegister($social_network, $post_id){
		$class = Config::get('constants.social_networks')[strtolower($social_network)]['models']['post'];
		$post = $class::where('id', $post_id)->first();
		return view('comms.form-comms',compact('post'));	
	}

	/**
	 * Este método agrega campos del comms panorama a un publicacion directamente.
	 * @param Request $request
	 * (Input: 'post_id', 'type', 'balance', 'micro_moment', 'business_objective', 'comunication_task', 'comunication_theme', 'contest', 'product')
	 * @return String para retornar cuando el proceso haya acabado.
	 */
	public function registerCommsToPost(Request $request){
		$social_network = $request->input('social_network');
		$post_id = $request->input('post_id');
		$class = Config::get('constants.social_networks')[strtolower($social_network)]['models']['post'];
		$post = $class::where('id', $post_id)->first();
		
		$type = $request->input('type');
		if($type && $type != ""){
			$post->type=$type;
		}

		$balance = $request->input('balance');
		if($balance && $balance != ""){
			$post->balance=$balance;
		}

		$micro_moment = $request->input('micro_moment');
		if($micro_moment && $micro_moment != ""){
			$post->micro_moment = $micro_moment;
		}

		$business_objective = $request->input('business_objective');
		if($business_objective && $business_objective != ""){
			$post->business_objective = $business_objective;
		}

		$comunication_task = $request->input('comunication_task');
		if($comunication_task && $comunication_task != ""){
			$post->comunication = $comunication_task;
		}
		$comunication_theme = $request->input('comunication_theme');
		if($comunication_theme && $comunication_theme != ""){
			$post->theme = $comunication_theme;			
		}

		$contest = $request->input('contest');
		if($contest && $contest!=""){
			$post->contests()->detach();
			if($contest != "no-contest")
				$post->contests()->attach($contest, ['social_network'=> $post->social_network()]);
		}		

		$product = $request->input('product');
		if($product && $product!=""){
			$post->product_id=$product;
		}

		$canasta = $request->input('canasta');
		if($canasta && $canasta!=""){
			$post->canasta_id=$canasta;
		}
		
		$influencer = $request->input('influencer');
		if($influencer && $influencer!=""){
			$post->influencer=$influencer;
		}
		$pauta = $request->input('pauta');
		if($pauta && $pauta!=""){
			$post->pauta= ($pauta=='PAUTA');
		}
		// dd($post);
		$post->save();
		$resp= array('result' => "El post Se ha actualizado con éxito");
		return json_encode($resp);

	}

	/**
	 * Este método agrega campos del comms panorama a un post luego del twoFactor authentication.
	 * @param woFactorAthentification $two
	 * ('post_id', 'type', 'balance', 'comunication_task', 'comunication_theme', 'contest', 'product')
	 * @return String para retornar cuando el proceso haya acabado.
	 */
	public function registerCommsFromTwoFactorToPost(TwoFactorAuthentication $two){
		//$post_id = $two->post_id;
		$post = $two->post;
		$type = $two->type;
		if($type && $type!=""){
			$post->type=$type;
		}
		$balance = $two->balance;
		if($balance && $balance!=""){
			$post->balance=$balance;
		}
		$micro = $two->micro_moment;
		if($micro && $micro!=""){
			$post->micro_moment=$micro;
		}
		$business = $two->business_objective;
		if($business && $business!=""){
			$post->business_objective=$business;
		}
		$comunication_task = $two->communication_task;
		if($comunication_task && $comunication_task!=""){
			$post->comunication=$comunication_task;
		}
		$comunication_theme = $two->communication_theme;
		if($comunication_theme && $comunication_theme!=""){
			$post->theme=$comunication_theme;			
			if($comunication_theme == 'concurso' ){
				$contest = $two->contest;				
				if($contest && $contest!=""){
					$post->contests()->detach();
					$post->contests()->attach($contest);
				}	
			}
		}	

		$product = $two->product_id;
		if($product && $product!=""){
			$post->product_id=$product;
		}

		$canasta = $two->canasta_id;
		if($canasta && $canasta!=""){
			$post->canasta_id=$canasta;
		}

		$influencer = $two->influencer;
		if($influencer && $influencer!=""){
			$post->influencer=$influencer;
		}
		$pauta = $two->pauta;
		if($pauta && $pauta!=""){
			$post->pauta= ($pauta=='PAUTA');
		}
		$post->save();
		return "El post Se ha actualizado con éxito";
	}

	/**
	 * Este método agrega campos del comms panorama que hagan match a un post con del twoFactor authentication.
	 * @param List<TwoFactorAthentification> $twos
	 * ('post_id', 'type', 'balance', 'comunication_task', 'comunication_theme', 'contest', 'product')
	 * @return String para retornar cuando el proceso haya acabado.
	 */
	public function registerCommsFromTwoFactorListToPost($twos, $post){

		$incomplete = false;
		foreach ($twos as $k => $tw) {
			$incomplete |= $tw->status == 0;
		}
		if($incomplete || count($twos)<2){ // si no hay suficientes valores para hacer match retorna un mensaje.
			return "Se han creado valores del comms para revisión.";
		}
		$tw_type = 'none';
		$tw_balance = 'none';
		$tw_micro='none';
		$tw_business='none';
		$tw_task = 'none';
		$tw_theme = 'none';
		$tw_contest = 'none';
		$tw_product = 'none';
		$tw_canasta = 'none';
		$tw_influencer = 'none';
		$tw_pauta = 'none';

		foreach ($twos as $key => $tw) {
			$tw_type = $this->compareTwoFactorCommsList($tw_type, $tw->type);
			$tw_balance = $this->compareTwoFactorCommsList($tw_balance, $tw->balance);
			$tw_micro = $this->compareTwoFactorCommsList($tw_micro, $tw->micro_moment);
			$tw_business = $this->compareTwoFactorCommsList($tw_business, $tw->business_objective);
			$tw_task = $this->compareTwoFactorCommsList($tw_task, $tw->communication_task);
			$tw_theme = $this->compareTwoFactorCommsList($tw_theme, $tw->communication_theme);
			if($tw_theme == 'concurso'){
				$tw_contest = $this->compareTwoFactorCommsList($tw_contest, $tw->contest);
			}else{
				$tw_contest = false;
			}
			$tw_product = $this->compareTwoFactorCommsList($tw_product, $tw->product);
			$tw_canasta = $this->compareTwoFactorCommsList($tw_canasta, $tw->canasta);
			$tw_influencer = $this->compareTwoFactorCommsList($tw_influencer, $tw->influencer);
			$tw_pauta = $this->compareTwoFactorCommsList($tw_pauta, $tw->pauta);
		}

		if($tw_type && $tw_type != "")
			$post->type = $tw_type;
		
		if($tw_balance && $tw_balance!="")
			$post->balance = $tw_balance;

		if($tw_micro && $tw_micro!="")
		    $post->micro_moment = $tw_micro;

		if($tw_business && $tw_business!="")
			$post->business_objective = $tw_business;
		
		if($tw_task && $tw_task != ""){
			$post->comunication = $tw_task;
		}
		
		if($tw_theme && $tw_theme != "")
			$post->theme = $tw_theme;	

		if($tw_contest && $tw_contest != ""){
			$post->contests()->detach();
			$post->contests()->attach($tw_contest);
		}	

		if($tw_product && $tw_product != "")
			$post->product_id = $tw_product->id;

		if($tw_canasta && $tw_canasta != "")
			$post->canasta_id = $tw_canasta->id;

		if($tw_influencer)
			$post->influencer=$tw_influencer;

		if($tw_pauta && $tw_pauta!="")
			$post->pauta= ($tw_pauta=='PAUTA');

		$post->save();

		$p_status = $post->getTwoFactorStatusAttribute();
		if($p_status == 2){
			return "El post Se ha actualizado con éxito";
		}else{
			// if(sizeof($twos)==0){
				// $profiles = TwoFactorProfile::where('rank', 1)->get();
				$activesJudge = UserRole::getUsersByPermisson('two_factor_principal', true, 1, '=')->get();
				if(sizeof($activesJudge) > 0){
					$p = $activesJudge->shuffle()->shift()->twoFactorProfile;
					app('App\Http\Controllers\TwoFactorAuthenticationController')->asingProfileToTwoFactor($p, $post->id, $post->social_network(), true);
				}
			// }
			return "Se han actualiado algunos campos del post";
		}
	}

	/**
	* Compara dentro de una iteración si un todos los elementos tienen el mismo valor.
	* @param {string} $tw_comms: Valor donde se tiene el valor actual de la comparación. 'none': valor inicial, false: hubo discrepancias, {string}: valor que ha sido igual.
	* @param {string} $twoFactorObject_comms: Valor a comparar.
	* @return {string/bool}: Si el valor actual es diferente al valor a comparar retorna false, en caso contrario retorna el mismo valor que tiene $twoFactorObject_comms.
	* Precondition: Este método es llamado dentro de un ciclo.
	*/
	private function compareTwoFactorCommsList($tw_comms = 'none', $twoFactorObject_comms){
		if($tw_comms == 'none'){
			$tw_comms = ($twoFactorObject_comms)? $twoFactorObject_comms: false;
		}else if($tw_comms != false && $tw_comms != $twoFactorObject_comms){
			$tw_comms = false;
		}
		return $tw_comms;
	}

	/**
	 * Método para crear un nuevo producto.
	 * @param Request $request 
	 * (Input: 'name', 'brand_id')
	 * @return redireccionamiento a la página anterior con mensaje de error o de succes.
	 */
	public function createProduct(Request $request){
		$this->validate($request,[
			'name' => 'required|string'
		]);

		$name = $request->input('name');
		$brand_id = $request->input('brand_id');

		$brand = Brand::where('id', $brand_id)->first();
		
		if($brand){
			$check_product = $brand->products()->where('name', $name)->first();
            if($check_product){
               return redirect()->back()->withError('El producto '. $name.' ya existe');
            }
		}
		$group = str_slug($name, '-');
		$segment = $request->input('segment');
        
		$product = Product::create([
			'name' => $name,
			'brand_id' => $brand_id,
			'segment' => $segment,
			'group_product' => $group
		]);
       
        // $product->save();
		// $segment = $request->input('segment');
		// if($segment){
		// 	$product->segment = $segment;
			
		// }
		return redirect()->back()->withSuccess('El producto '.$product->name.' ha sido creado');
	}

	/**
	* Agrega los productos por defecto a una marca
	* @param {Brand\brand} $brand: marca que tendrá los productos por defecto.
	* Postcondition: se han creados 2 productos para la marca.
	*/
    public function addDefaultProducts(Brand $brand){
    	Product::create([
    		'name' => 'Publicación Sin Productos',
    		'group_product' => 'na',
    		'brand_id' => $brand->id
    	]);
    	Product::create([
             'name' => 'Todos',
             'group_product' => 'todos',
             'brand_id' => $brand->id
    	]);        
    }

    /**
    * Elimina un producto de la base de datos.
    * @param {int} $id: id del elemento a eliminar.
    * @return \Illuminate\Http\Response
    */
    public function deleteProduct($id){
		Product::where('id', $id)->delete();
		return redirect()->back()->withSuccess('El producto ha sido eliminado correctamente');
	}

	/**
	* Actualiza la información de un producto
	* @param  \Illuminate\Http\Request  $request
	* @return \Illuminate\Http\Response
	*/
	public function editProduct(Request $request){
       $newName = $request->input('name');
       $pId = $request->input('product_id');
       $editProduct = Product::where('id', $pId)->first();
       $editProduct->name = $newName;
       $editProduct->group_product = str_slug($newName, '-');
       if($request->input('segmento')){
       $editProduct->segment =  $request->input('segmento');
       }
       $editProduct->save();

       return redirect()->back()->withSuccess('El producto '.$newName.' ha sido editado con éxito.');
	}


	/**
	* Retorna la vista para la sección discovery.
	* @param {string/int} $fanpage: id de la fanpage, o 'all' si se quieren pedir los posts e todas las fanpages.
	* @param {string} $since: fecha inicial para mostrar los posts.
	* @param {string} $since: fecha final para mostrar los posts.
	* @return \Illuminate\Http\Response
	*/
	public function discovery($social_network="Facebook", $fanpage = "all", $since=null, $until=null){ 
		$since = ($since != null)? $since: "";
		$until = ($until != null)? $until: "";
		$snInfo = Config::get('constants.social_networks')[strtolower($social_network)];
		$postClass = $snInfo['models']['post'];
		$pageClass = $snInfo['models']['page'];
		$allPosts = ($fanpage=="all")? $postClass::orderBy('published', 'desc') : $pageClass::where('id',$fanpage)->first()->posts()->orderBy('published', 'desc');
		$allPosts = MethodsController::filterByDate($allPosts, $since, $until);
		$allPosts = $this->filterByUser($allPosts);
		$posts = $this->getDiscoverySectionsPosts($allPosts);
		$title = "Todos los posts";
		$user = (Auth::check())? Auth::user(): false;
		if($fanpage != "all"){
			$fanpage = $pageClass::where('id',$fanpage)->first();
			$title =  $fanpage->brand->name ." ". $fanpage->name;
		}
		if(true || $user->twoFactorProfile->rank == 1){
			$allElements = array('fanpages'=>Fanpage::all(), 'brands'=>Brand::all(), 'baskets'=>Canasta::all(), 'adsets'=>AdSet::all());
			$userPoints = $this->getProfilePoints();
			
			return view('comms.discovery',compact('allElements', 'social_network' ,'posts', 'title', 'userPoints' ,'fanpage', 'since','until'));
		}else{
          return view('comms.discovery',compact('posts', 'title', 'social_network' ,'fanpage', 'since','until'));
		}
	}

	/**
	* Obtiene las listas de posts calificados y por calificar.
	* @param {Object} $queryPost: query con la lista de posts que se quiere separar entre calificados y sin calificar.
	* @return {array}: ['comms' => Query de posts calificados, 'no_comms' => Query de comms sin calificar]
	*/
	public function getDiscoverySectionsPosts($queryPosts){
		$noComms = clone $queryPosts;
        
		$noComms = $noComms->where(function ($query) {
			$query->orWhereNull('type')->orWhereNull('balance')->orWhereNull('comunication')->orWhereNull('theme')->orWhereNull('product_id')->orWhereNull('micro_moment')->orWhereNull('business_objective');
		})->paginate(50 ,["*"],"pendientes");
		$comms = clone $queryPosts;
		$comms = $comms->whereNotNull('type')->whereNotNull('balance')->whereNotNull('comunication')->whereNotNull('theme')->whereNotNull('product_id')->whereNotNull('micro_moment')->whereNotNull('business_objective')->paginate(20,["*"],"completos");
		$twoFactor = clone $queryPosts;
		//$twoFactor = $comms;
		$twoFactor = "";
		return array('comms' => $comms, 'no_comms' => $noComms, 'two_factor_athentification'=> $twoFactor);
	}
    
	/**
	 * Busca los usuarios que no tengan puntuaciones perfectas y retorna su información
	 */
	public function getProfilePoints(){
		$userPoints= array();
		//select permite aclarar explicitamente que tipo de informacion quiere que se conserve al realizar el join
		$users = TwoFactorProfile::select('users.name', 'two_factor_profiles.*')->leftJoin('users', 'users.id', '=', 'two_factor_profiles.user_id')->where('ratings_type', '!=' , 10000)->orWhere('ratings_communication_task', '!=', 10000)->orWhere('ratings_balance', '!=', 10000)->orWhere('ratings_product', '!=',  10000)->orWhere('ratings_communication_theme', '!=', 10000)->orWhere('ratings_contest', '!=', 10000)->orWhere('ratings_influencer', '!=', 10000)->get();
      
        foreach($users as $user){
        	$cant = $user->twofactors()->whereNotNull('type')->whereNotNull('balance')->whereNotNull('communication_task')->whereNotNull('communication_theme')->whereNotNull('product_id')->whereNotNull('micro_moment')->whereNotNull('business_objective')->count();

        	$total = $user->twofactors()->where('profile_id', '=', $user->id)->count();

        	 
            
            $userInfo = array(
              'name' => $user->name,
              'ratings_type' => $user->ratings_type,
              'ratings_balance' => $user->ratings_balance,
              'ratings_product' => $user->ratings_product,
              'ratings_communication_theme' => $user->ratings_communication_theme,
              'ratings_communication_task' => $user->ratings_communication_task,
              'ratings_contest' => $user->ratings_contest,
              'ratings_influencer' => $user->ratings_influencer,
              'complete_posts' => round( ($cant/$total) * 100 , 2, PHP_ROUND_HALF_UP),
              'total' => $user->ratings_type + $user->ratings_communication_task + $user->ratings_communication_theme + $user->ratings_balance + $user->ratings_product + $user->ratings_contest + $user->ratings_influencer,
            );
            array_push($userPoints, $userInfo);
        }
       return $userPoints;
	}

	/**
	 * Este método redirige a los métodos que obtienen los valores del comms panorama según el tipo dado.
	 * Los resultados los filtra por un rango de tiempo, si no existe rango de tiempo, calcula todos los datos.
	 * @param string $type: tipo de elemento en la vertical del que se pediran los valores del comms.
	 * ('basket', 'brand', 'fanpage', 'channel')
	 * @param number $id: id del elemento del que se conseguirán los comms
	 * @param string $country: País por el que se quiere realizar la filtración (default="all")
	 * @param string $since: fecha inicial en formato Y-m-d (default=null)
	 * @param string $until: fecha final en formato Y-m-d (default=null)
	 * @return diccionario con los comms de la categoría dada.
	 */
	public function getCategory($social_network='all', $type, $id, $country="all", $activity = "published", $since=null, $until=null){
		// Creación del filtro
		$filterObj = MethodsController::getFilterPostsObject();
		// ESTOY AQUI
		//Setea los atributos de filterObj dependiendo del tipo de actividad que vaya a consultarse 
		($activity == 'published')? $filterObj->setDate($since, $until) : $filterObj->setDate($since, $until, 'metrics.created_at');
		//$filterObj->setDate($since, $until, 'metrics.created_at');	
		if($_GET && $_GET['filtro_comms']){
			$filterObj->setupFilters($_GET);
			// dd($filterObj);
		}
		// Fin del filtro
		$error = 'La categoría "'.$type.'" no se encuentra.';
		switch ($type) {
			case 'basket':
			case 'Canasta':
			$basket = Canasta::where('id',$id)->first();
			if($basket)
				return $this->getCommsBasket($basket, $country, $social_network, $activity, $filterObj);
			else
				$error = 'No se encuentra ninguna canasta con id: '.$id;
			break;
			case 'brand':
			case 'Brand':
			$brand = Brand::where('id',$id)->first();

			if($brand)
				return $this->getCommsBrand($brand, $country, $social_network, $activity, $filterObj);
			else
				$error = 'No se encuentra ninguna marca con id: '.$id;
			break;

			case 'Page_List':
			case 'page_list':
				return $this->getCommsPageList($id, $filterObj);
			break;

			case 'fanpage':
			case 'Fanpage':
			$fanpage = Fanpage::where('id',$id)->first();
			if($fanpage)
				return $this->getCommsFacebookPage($fanpage, $activity, $filterObj);
			else
				$error = 'No se encuentra ninguna fanpage con id: '.$id;
			break;

		    case 'youtubechannel':
			case 'YoutubeChannel':
			$channel = YoutubeChannel::where('id',$id)->first();
			if($channel)
				return $this->getCommsYoutubeChannel($channel, $activity ,$filterObj);
			else
				$error = 'No se encuentra ningun canal con id: '.$id;
			break;

			case 'instaprofile':
			case 'InstaProfile':
			$profile = InstaProfile::where('id',$id)->first();
			if($profile)
				return $this->getCommsInstagramProfile($profile, $activity ,$filterObj);
			else
				$error = 'No se encuentra ningun canal con id: '.$id;
			break;

				case 'adset':
				case 'AdSet':
			$adset = AdSet::where('id',$id)->first();
			if($adset)
				return $this->getPostsAdSet($adset, $filterObj);
			else
				$error = 'No se encuentra ningún adset con id: '.$id;
			break;

		}
		return ['error' => $error];
	}
	/**
	 * Este método obtiene los valores del comms panorama para una canasta.
	 * Los resultados los filtra por un rango de tiempo, si no existe rango de tiempo, calcula todos los datos.
	 * @param Canasta $canasta: Canasta de la que se conseguirán los comms
	 * @param string $country: País por el que se quiere realizar la filtración (default="all")
	 * @param string $since: fecha inicial en formato Y-m-d (default=null)
	 * @param string $until: fecha final en formato Y-m-d (default=null)
	 * @return diccionario con los comms de la canasta.
	 */
	public function getCommsBasket(Canasta $canasta, $country="all", $social_network='all', $activity ,$filterObj=null){
		$comms = array();
 

		foreach ($canasta->brands as $brand) {
			array_push($comms, $this->getCommsBrand($brand,$country, $social_network, $activity ,$filterObj));
		 
		}
		$info_brand = array(
			'name' => $canasta->name,
			'type' => 'basket',
			'basket_id' => $canasta->id,
			'brands' => $comms,
			'comms' => $this->getSumComms($comms),
		);
		//dd($info_brand);
		return $info_brand;
	}
	/**
	 * Este método obtiene los valores del comms panorama para una marca.
	 * Los resultados los filtra por un rango de tiempo, si no existe rango de tiempo, calcula todos los datos.
	 * @param string $country: País por el que se quiere realizar la filtración (default="all")
	 * @param Brand $brand: Marca de la que se conseguirán los comms
	 * @param string $since: fecha inicial en formato Y-m-d (default=null)
	 * @param string $until: fecha final en formato Y-m-d (default=null)
	 * @return diccionario con los comms de la marca.
	 */
	public function getCommsBrand(Brand $brand, $country="all", $social_network='all', $activity, $filterObj=null){
		//$social_network = 'Facebook';
		$fb_comms = array();
		$yt_comms = array();
		$insta_comms = array();
		$comms = array();

		if($social_network == 'all' || $social_network == 'Facebook' ){
			$fanpages = ($country=="all")? $brand->fanpages : $brand->fanpages()->where('name', 'LIKE', '%'.$country.'%')->get();
			foreach ($fanpages as $fpg) {
				array_push($fb_comms, $this->getCommsFacebookPage($fpg, $activity ,$filterObj));	
			}
	 
			$comms = array_merge($comms, $fb_comms);
		}
		if($social_network == 'all' || $social_network == 'YouTube' ){
			$channels = ($country=="all")? $brand->channels : $brand->channels()->where('name', 'LIKE', '%'.$country.'%')->get();
			foreach ($channels as $chn) {
				array_push($yt_comms, $this->getCommsYoutubeChannel($chn, $activity ,$filterObj));	
			}
			$comms = array_merge($comms, $yt_comms);
		}

		if($social_network == 'all' || $social_network == 'Instagram' ){
			$profiles = ($country=="all")? $brand->profiles : $brand->profiles()->where('name', 'LIKE', '%'.$country.'%')->get();
			foreach ($profiles as $profile) {
				array_push($insta_comms, $this->getCommsInstagramProfile($profile, $activity ,$filterObj));	
			}
			$comms = array_merge($comms, $insta_comms);
		}  
 

		$info_brand = array(
			'name' => $brand->name,
			'type' => 'brand',
			'brand_id' => $brand->id,
			'fanpages' => $fb_comms,
			'channels' => $yt_comms,
			'profiles' => $insta_comms,
			'comms' => $this->getSumComms($comms),
		);

	//dd($info_brand);
		return $info_brand;
	}
    //Obtener cantidad de posts registrados
 	

 	 /**
 	 * Este método obtiene los valores del comms panorama para una lista de páginas.
	 * Los resultados los filtra por un rango de tiempo, si no existe rango de tiempo, calcula todos los datos.
	 * @param {string} $pagesID: Lista de las ids a filtrar "{SlugRedSocial}_{idpage_1},{idPage_2},...,{idPage_n}"
	 * @param {Array} $filterObj: Diccionario con las propiedades para filtrar los posts.
	 * @return diccionario con los comms de las páginas.
 	 */
 	public function getCommsPageList($pagesIDs, $filterObj=null){
		$comms = array();
		$social_network = explode('_', $pagesIDs);
 		$pagesIDs = $social_network[1];
 		$social_network = strtolower($social_network[0]);
		$snInfo = Config::get('constants.social_networks');
		// Indica el método a llamar según el tipo de página
		$methodComms = false;
		// if($social_network == $snInfo['facebook']['slug']) $methodComms = "getCommsFacebookPage";
		// else if($social_network == $snInfo['youtube']['slug']) $methodComms = "getCommsYoutubeChannel";
		if($social_network == $snInfo['facebook']['slug']) $methodComms = "getCommsFacebookPage";
		else if($social_network == $snInfo['youtube']['slug']) $methodComms = "getCommsYoutubeChannel";
        else if($social_network == $snInfo['instagram']['slug']) $methodComms = "getCommsInstagramProfile";
		// nombre del modelo de la página de la red social seleccionada
		$type = explode('\\', $snInfo[$social_network]['models']['page'])[1];
		$pageLists = MethodsController::getCategory($type, $pagesIDs);
		// Si no se obtiene una lista de páginas, convierte la página en una lista de un elemento.
		if(!$pageLists instanceof \Illuminate\Support\Collection && $pageLists!=null) $pageLists = [$pageLists];
		if($methodComms){
			foreach ($pageLists as $fpg) {
				array_push($comms, $this->$methodComms($fpg, $filterObj));
			}
		}
		
 		$info_pages = array(
			'name' => 'Lista de '.$type,
			'type' => $type,
			'pages' => $comms,
			'comms' => $this->getSumComms($comms),
		);
 		return $info_pages;
 	}

	/**
	 * Este método obtiene los valores del comms panorama para un adset.
	 * Los resultados los filtra por un rango de tiempo, si no existe rango de tiempo, calcula todos los datos.
	 * @param AdSet $adset: adset del que se conseguirán los comms
	 * @param string $since: fecha inicial en formato Y-m-d (default=null)
	 * @param string $until: fecha final en formato Y-m-d (default=null)
	 * @return diccionario con los comms de la adset.
	 */
	public function getPostsAdSet(AdSet $adset, $filterObj=null){
		$postsList = $adset->posts();
		if($filterObj)
			$postsList = $filterObj->filterPosts($postsList);

		$info = array(
			'type' => 'adset',
			'name' => $adset->name,
			'adset_id' => $adset->id,
			'comms' => $this->getSumComms($postsList->get(),1)
		);
		return $info;
	}


	/**
	 * Este método obtiene los valores del comms panorama para una fanpage.
	 * Los resultados los filtra por un rango de tiempo, si no existe rango de tiempo, calcula todos los datos.
	 * @param Fanpage $fanpage: fanpage de la que se conseguirán los comms
	 * @param string $since: fecha inicial en formato Y-m-d (default=null)
	 * @param string $until: fecha final en formato Y-m-d (default=null)
	 * @return diccionario con los comms de la fanpage.
	 */
	public function getCommsFacebookPage(Fanpage $fanpage, $activity ,$filterObj=null){
		//Query que permite crear una lista de publicaciones de la fanpage deseada
	 	$postsList = ($activity == 'published')? $fanpage->posts() : $fanpage->posts()->select('posts.*')->join('metrics as metrics', 'posts.id' , '=', 'metrics.post_id')->groupBy('posts.id');
		//Query para omitir posts que no esten calificados en tipo, balance, o tarea de comunicacion 
		//$postsList = $postsList->whereNotNull('type')->whereNotNull('balance')->whereNotNull('comunication');
		//Query que permite obtener las publicaciones de acuerdo a sus metricas
        //$postsList = $fanpage->posts()->select('posts.*')->join('metrics as metrics', 'posts.id' , '=', 'metrics.post_id')->groupBy('posts.id');

		if($filterObj)
			$postsList = $filterObj->filterPosts($postsList);
		$info = array(
			'type' => 'fanpage',
			'name' => 'FB '.$fanpage->name.' '.$fanpage->brand->name,
			'fanpage_id' => $fanpage->id,
			'facebook_id' => $fanpage->facebook_id,
			'comms' => $this->getSumComms($postsList->get(),1)
		);

		return $info;
	}

	/**
	 * Este método obtiene los valores del comms panorama para una Canal de youtube.
	 * Los resultados los filtra por un rango de tiempo, si no existe rango de tiempo, calcula todos los datos.
	 * @param YoutubeChannel $channel: canal de youtube del que se conseguirán los comms
	 * @param Object $filterObj: Objeto con los filtros a aplicar a las publicaciones.
	 * @return diccionario con los comms del canal de YouTube.
	 */
	public function getCommsYoutubeChannel(YoutubeChannel $channel, $activity ,$filterObj=null){
		
		// $postsList = $channel->messyPosts();
		$postsList = ($activity == 'published')? $channel->messyPosts() : $channel->posts()->select('youtube_videos.*')->join('youtube_metrics as metrics', 'youtube_videos.id' , '=', 'metrics.youtube_video_id')->groupBy('youtube_videos.id');

		// $postsList = $postsList->whereNotNull('type')->whereNotNull('balance')->whereNotNull('comunication');

		if($filterObj)
			$postsList = $filterObj->filterPosts($postsList);

		$info = array(
			'type' => 'channel',
			'name' => 'YT '.$channel->name.' '.$channel->brand->name,
			'channel_id' => $channel->id,
			'youtube_id' => $channel->youtube_id,
			'comms' => $this->getSumComms($postsList->get(),2)
		);
		return $info;
	}

	/**
	 * Este método obtiene los valores del comms panorama para una Perfil de Instagram.
	 * Los resultados los filtra por un rango de tiempo, si no existe rango de tiempo, calcula todos los datos.
	 * @param InstaProfile $profile: perfil de youtube del que se conseguirán los comms
	 * @param Object $filterObj: Objeto con los filtros a aplicar a las publicaciones.
	 * @return diccionario con los comms del canal de Instagram.
	 */
	public function getCommsInstagramProfile(InstaProfile $profile, $activity ,$filterObj=null){
		// $postsList = $profile->messyPosts();
		// $postsList = $postsList->whereNotNull('type')->whereNotNull('balance')->whereNotNull('comunication');
        
        $postsList = ($activity == 'published')? $profile->messyPosts() : $profile->posts()->select('insta_posts.*')->join('insta_metrics as metrics', 'insta_posts.id' , '=', 'metrics.insta_post_id')->groupBy('insta_posts.id'); 

		if($filterObj)
			$postsList = $filterObj->filterPosts($postsList);

		$info = array(
			'type' => 'profile',
			'name' => 'INSTGR '.$profile->name.' '.$profile->brand->name,
			'profile_id' => $profile->id,
			'instagram_id' => $profile->instagram_id,
			'comms' => $this->getSumComms($postsList->get(),2)
		);
		return $info;
	}

	/**
	* Filtra un query de posts por su usuario asignado en two factor.
	* @param {Object} $list: query de la lista de posts.
	* @param {App\User or bool} $user: usuario con el que se realizará la filtración.
	* @return {Object}: query de la lista de post luego de haber aplicado el filtro por usuario.
	*/
    public function filterByUser($list, $user=false){
    	if(!$user && Auth::check()){
    		$user = Auth::user();
    	}
    	if($user && $user->twoFactorProfile){
    		$list = $list->whereHas('twofactors', function($query) use ($user){
    			$query->where('profile_id', $user->twoFactorProfile->id);
    		});
    	}

    	return $list;
    }

	/**
	* Calcula las sumas de los comms en los posts existenten.
	* @param {Array} lista con los elementos que contienen los valores a sumar.
	* @param {number} identificador para el tipo de post, si pertenece a una red social o es un modelo que lo contiene. 0:diccionario, 1:Facebook, 2:YouTube.
	* @return diccionario de comms.
	*/
	public function getSumComms($list, $type=0){
		$comms= array(
			'type' => array(
				'count' => 0,
			),
			'balance' => array(
				'count' => 0,
			),
			'comunication_task' => array(
				'count' => 0,
			),
			'comunication_theme' => array(
				'count' => 0,
			),
			'contest' => array(
				'count' => 0,
				'No definido' => 0
			),
			'product' => array(
				'count' => 0,
				'No definido' => 0
			),
			'micro_moment' => array(
				'count' => 0,
			),
			'business_objective' => array(
				'count' => 0,
			),
			'canasta' =>array(
				'count'=>0,
			)
		);
		//,
			// 'posts' => array()
		if($type==0){
				foreach ($list as $ele) { // recorre lista de comms

					foreach ($ele['comms'] as $kyComm => $lComm) { // recorre cada elemento de los comms (type, balance, ...)
						// if(!array_key_exists($kyComm,$comms['posts'])){$comms['posts'][$kyComm]=array();}
						// if($kyComm != 'posts')
						foreach ($lComm as $nom => $cant) { // recorre elementos específicos 
							$comms[$kyComm][$nom]=(array_key_exists($nom, $comms[$kyComm]))? $comms[$kyComm][$nom]+$cant: $cant;
							// if(!array_key_exists($nom,$comms['posts'][$kyComm])){$comms['posts'][$kyComm][$nom]=array();}
							// array_push($comms['posts'][$kyComm][$nom], $lComm['posts'][$kyComm][$nom]);
						}
					}
				}
			}else if($type==1 || $type==2){
				foreach ($list as $post) {
					 
					$postComms = $this->getCommsFacebookPost($post);	
					foreach ($postComms as $key => $c) {				
						$comms[$key][$c]=(array_key_exists($c, $comms[$key]))? $comms[$key][$c]+1: 1;	
						$comms[$key]['count']++;
					}
					//$this->addMeticsInfoFBPost($comms['posts'], $postComms, $post);
				}
			}
			// Arreglo global para la sección de concursos
			if(array_key_exists('concurso', $comms['comunication_theme'])){
				$cant_contest = $comms['comunication_theme']['concurso'];
				$cant_not_participants = $comms['contest']['count'] - $cant_contest;
				$comms['contest']['count'] = $cant_contest;
				$comms['contest']['No definido'] -= $cant_not_participants;
			}else{
				$comms['contest']['count'] = 0; 
				$comms['contest']['No definido']=0;
			}
			return $comms;
		}
		// private function addMeticsInfoFBPost($list, $comms, $post){
		// 	foreach ($comms as $key => $c) {
		// 		$list[$key] = (array_key_exists($key, $list))? $list[$key] : array();
		// 		if(!array_key_exists($c, $list[$key])){
		// 			$list[$key][$c]=array();
		// 		}
		// 		array_push($list[$key][$c],$post->id);
		// 	}
		// }
		/**
		 * Este método obtiene los comms de un post
		 * @param  Post $post: post del que se obtendran los comms
		 * @return diccionario con los Comms del post
		 */
		public function getCommsFacebookPost($post){
			$undefined = "No definido";
			$type = $post->type;
			$type = ($type && $type != "")? $type : $undefined;
			$balance = $post->balance;
			$balance = ($balance && $balance != "")? $balance : $undefined;
			$comunication_task = $post->comunication;
			$comunication_task = ($comunication_task && $comunication_task != "")? $comunication_task : $undefined;
			$comunication_theme = $post->theme;
			$comunication_theme = ($comunication_theme && $comunication_theme != "")? $comunication_theme : $undefined;
			$contest = $post->contests->first();
			$contest = ($contest)?$contest->title:$undefined;
			$product = $post->product_id;
			$product = ($product && $product != "")? Product::where('id', $product)->first()->getSegementProductname() : $undefined;
			$canasta = $post->canasta;
			$canasta = ($canasta)?$canasta->name:$undefined;
			$micro = $post->micro_moment;
			$micro = ($micro && $micro != "")? $micro : $undefined;
			$bussiness = $post->business_objective;
			$bussiness = ($bussiness && $bussiness != "")? $bussiness : $undefined;
			$comms = array(
				'type' => $type,
				'balance' => $balance,
				'comunication_task' => $comunication_task,
				'comunication_theme' => $comunication_theme,
				'contest' => $contest,
				'product' => $product,
				'canasta' => $canasta,
				'micro_moment' => $micro,
				'business_objective' => $bussiness,
			);
			return $comms;
		}






		// PRUEBAS  /////////////////////////
		/**
		* Método para correr querys y corregir errores en la base de datos
		*/
		public function debug(){
			// Correción de errores: juez two factor no asignado a posts calificados
			$result = [];
			if(isset($_GET) && isset($_GET['since'])){
				$since = $_GET['since'];

				$posts = Post::whereHas('twofactors', function($query) use ($since){
					$query->where('created_at', '>=', $since);
				});
				dd($posts);
				foreach ($posts as $key => $post) {
		            $twos = $post->twofactors()->where('principal',false)->get();
		            $resp = $this->registerCommsFromTwoFactorListToPost($twos, $post);
				}
			}else{
				array_push($result, "No se ha especificado el parámetro 'since'.");
			}
			dd($result);
			return 'finalizado';
		}
	}