<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;
use Config;
use App\Vertical;
use App\Categoria;
use App\Canasta;
use App\Brand;
use App\Fanpage;
use App\AdSet;
use App\GroupIndustryRole;
use App\User;
use App\YoutubeChannel;
use App\YoutubeVideo;
use App\YoutubeMetrics;
use App\Fanpages;
use App\Report;
use App\Post;
use App\Metric;
use App\InstaPost;
use App\InstaProfile;
use DateTime;
use DateInterval;
use DatePeriod;
 

class ConsultActivityController extends Controller
{
	/** Intervalo entre horas para realizar el reporte */
	const HOUR_INTERVAL = 2;
	/** Cantidad de horas por día */
	const CANT_HOURS = 24;
	/** Nombres de los días de la semana
 	* Carbon -> dayOfWeek return number betten  0 (sunday) and 6 (saturday)
	*/
	const DAYS = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"];
	/** Prefijo usado para identificar los datos de las diferente redes sociales */
    const SOCIAL_NETWORK_PRE = ['Facebook' => 'fb_', 'YouTube' => 'yt_', 'Instagram' => 'insta_'];
    /** Nombre de las interacciones de las diferentes sociales para las que se registran métricas */
    const METRICS_BY_PRE = ['fb_' => ['reactions', 'comments', 'shares', 'views'], 'yt_' => ['views', 'likes', 'dislikes', 'comments'], 'insta_' => ['likes', 'comments', 'views']];


    public function index(){
		$user = User::where('id', 11)->first();
		$allElements = array(
			'fanpages' => GroupIndustryRole::getUserPermissionsElement($user, 'Fanpage')->get(), 
			'brands' => GroupIndustryRole::getUserPermissionsElement($user, 'Brand')->get(), 
		    'channels' => GroupIndustryRole::getUserPermissionsElement($user, 'YoutubeChannel')->get(),
		    'profiles' => GroupIndustryRole::getUserPermissionsElement($user, 'InstaProfile')->get(),
			'baskets' => GroupIndustryRole::getUserPermissionsElement($user, 'Canasta')->get(), 
			'adsets' => AdSet::getAdsetsFromUserId($user->id, 0)->get()
		);
		 return  view('comms.consults.intensity-rate', compact('allElements'));
	}

	public function filter(){
		$allElements = array('basket' => Canasta::all(), 'brands'=>Brand::all());
		return view('comms.tryout', compact('allElements'));
	}

	/**
	* Realiza una consulta de la variación de métricas por horas en los posts por días pertenecientes al tipo dado (Vertical, categoría, canasta, marca).
	*Solo sirve para un elemento, no está habilitado pararecorrer múltiples valores.
	* @param {string} $type: nombre de la categoría en la que se realizará la búsqueda. (vertical, categoría, canasta ó marca)
	* @param {string} $slug: del elemento perteneciente a la categoría donde se realizará la búsqueda.
	* @param {string} $country: nombre del país al que pertenece la fanpage.
	* @param {string} (Y-m-d) $since: fecha inicial para el filtro
	* @param {string} (Y-m-d) $until: fecha final para el filtro
	* @return  Diccionario com los datos de la consulta.
	*/
	public function consultMetrics($social_network='all', $type, $slug, $country="all", $activity ,$since=null, $until=null){
		$firstDate = new DateTime($since);
		$lastDate = new DateTime($until);		

		//  INICIO FILTRO POR COMMS
		$filterObj = MethodsController::getFilterPostsObject();
		if($_GET && $_GET['filtro_comms']){
			$filterObj->setupFilters($_GET);
		}
		//  SIN FILTRO COMMS
        $acts = ConsultActivityController::getEmptyArrayActivity();

		$models = MethodsController::getCategory($type, $slug);
		if(!$models instanceof \Illuminate\Support\Collection) $models = [$models];
		$allPosts = collect();
		$cantSN = [];
		// dd($models);
		$snInfo = Config::get('constants')['social_networks'];

		// $posts = $model->posts($country);
		// $postsNumber = 0;        
		// if($posts != null){		
		// 		$posts = $postsList = $filterObj->filterPosts($posts);
		// 		$posts = $posts->whereHas('metrics', function($query) use ($since, $until){
		// 		MethodsController::filterByDate($query, $since, $until,'created_at');
		// });
		
		foreach ($models as $km => $model) {
			$this->addPostsFromSocialNetwork($allPosts, $cantSN, $model, $filterObj, $snInfo['facebook'], $social_network, $country, $activity, $since, $until);
		     $this->addPostsFromSocialNetwork($allPosts, $cantSN, $model, $filterObj, $snInfo['youtube'], $social_network, $country, $activity ,$since, $until);
		     $this->addPostsFromSocialNetwork($allPosts, $cantSN, $model, $filterObj, $snInfo['instagram'], $social_network, $country, $activity ,$since, $until);	
		}
        $postsNumber = sizeof($allPosts);
        // dd($allPosts);
		foreach ($allPosts as $key => $post) {
			$acts = $this->addActivityFromPost($post, $acts, $since, $until);
		}	
		$info_consulta = array('info' => $acts, 'cant' => $postsNumber, 'cant_sn'=>$cantSN);
		return $info_consulta;
	}

	/**
	* Realiza el pedido de los posts que han tenido métricas por red social y lo une a la lista global de publicaciones.
	* @param {Collection} &$allPosts: Referencia a la lista que contiene los posts de todas las redes sociales.
	* @param {Array} &$cantSN: referencia al arreglo que tiene la cantidad de posts por red social.
	* @param {Mixed} $model: Instancia del modelo al que se pedirán los posts.
	* @param {Object} $filterObj: Es obtenido de MethodController::getFilterPostsObject, y es usuado para los filtros que se aplicarán a los posts.
	* @param {Array} $socialNetworkInfo: Arreglo obtenido de Config::get('constants')['social_networks'] con la información de la red social a comparar.
	* @param {string} $social_network: Nombre de la red social seleccionada para filtrar.
	* @param {string} $country: nombre del país a filtrar.
	* @param {string} (Y-m-d) $since: fecha inicial para el filtro
	* @param {string} (Y-m-d) $until: fecha final para el filtro
	* PostCondition: El objeto de la referencia a $allPosts ha sido actualizado.
	* PostCondition: El objeto de la referencia a $cantSN ha sido actualizado.
	*/
	private function addPostsFromSocialNetwork(&$allPosts, &$cantSN, $model, $filterObj, $socialNetworkInfo, $social_network='all', $country="all", $activity ,$since=null, $until=null){
		
		$snName = $socialNetworkInfo['name'];
		if($social_network == 'all' || $social_network == $snName ){
			 
			$metricsTable = (new $socialNetworkInfo['models']['metric']())->getTable();
			// dd('hehehe', $metricsTable, $socialNetworkInfo);
			if($activity == 'active'){

			$filterObj->extraFilter = function($q) use($since, $until, $metricsTable){
				$q = $q->whereHas('metrics', function($query) use ($since, $until, $metricsTable){
					MethodsController::filterByDate($query, $since, $until, $metricsTable.'.created_at');
				});

				return $q;
			};
		}else{
		 
			$filterObj->extraFilter = function($q) use($since, $until){
				$q = $q->whereHas('metrics', function($query) use ($since, $until){
					MethodsController::filterByDate($query, $since, $until, 'published');
				});
				
				return $q;
			};
		}
			$posts = $model->snPosts($country, $snName, $filterObj);
			if($posts){
				$allPosts = $allPosts->merge($posts);
				$cantSN[$snName] = sizeof($posts);
			}
		}
	}


	/**
	* Crea el arreglo donde se llenará la actividad de los posts, el arreglo unidimensional con el largo del día de la semana y la hora (reducida con sus intérvalos), el arreglo contiene los diccionarios retornados por el método getActivityInfo.
	*
	* @return arreglo con la información inicial para agregar la actividad.
	*/
	protected static function getEmptyArrayActivity(){
		$emptyArray = [];
		$interval = ConsultActivityController::HOUR_INTERVAL;
		$hours = ConsultActivityController::CANT_HOURS / $interval;
		foreach (ConsultActivityController::DAYS as $k => $d) {
			for ($i=0; $i < $hours; $i++) { 
				array_push($emptyArray, ConsultActivityController::getActivityInfo($i * $interval, $d));
			}
		}
		return $emptyArray;
	}
	/**
	* Retorna un diccionario con el formato inicial para guardar la actividad del día y hora dado.
	* @param {string} $hour: Hora del día
	* @param {string} $day: Nombre del día
	* @return Diccionario con el día y la hora seteados, y el resto de datos en 0.
	*/
	protected static function getActivityInfo($hour, $day){
		return [
			'hour' => $hour,
			'day' => $day,
			'fb_reactions' => 0,
			'fb_comments' => 0,
			'fb_shares' => 0,
			'fb_views' => 0,
			'yt_views' => 0,
			'yt_likes' => 0,
			'yt_dislikes' => 0,
			'yt_comments' => 0,
			'insta_views' => 0,
			'insta_likes' => 0,
			'insta_comments' => 0
		];
	}

	/**
	* Este método agrega a un arreglo de con la actividad acumulada, la actividad de las métricas pertenecientes a un post que se encuentran dentro del rango de tiempo dado.
	* @param {App/Post} $post: post del que se obtendrán las metricas a agregar.
	* @param {array} $activityArray: Arreglo con la actividad acumulada.
	* @param {string} (Y-m-d) $since: fecha inicial para la búsqueda.
	* @param {string} (Y-m-d) $until: fecha final para la búsqueda.
	* @return  El diccionario con la actividad acumulada ($activityArray), luego de agregarle los nuevos datos.
	*/
	public function addActivityFromPost($post, $activityArray, $since=null, $until=null){
		$cantDays = sizeof(ConsultActivityController::DAYS);
		$interval = ConsultActivityController::HOUR_INTERVAL;
		$cantHours = ConsultActivityController::CANT_HOURS / $interval;
		$pref = ConsultActivityController::SOCIAL_NETWORK_PRE[$post->social_network()];
		$metrics = MethodsController::filterByDate($post->metrics(), $since, $until, 'created_at')->get();		
		if(sizeof($metrics) == 0)
			return $activityArray;
		$ant = ($since != null)? $post->metrics()->where('created_at', '<', $since)->latest()->first(): null;		
		$metricsByPre = ConsultActivityController::METRICS_BY_PRE[$pref];
		foreach ($metrics as $k => $actual) {
			$date = $actual->created_at;
			$actualOrder = $date->dayOfWeek - 1;
			if($actualOrder < 0){
				$actualOrder=6;
			}
			$dayOfWeek = $actualOrder;
			$hour = $date->hour;
			$hour = $hour/ $interval;
			$ind = $dayOfWeek * $cantHours + $hour; 

			$newInfo = $this->sumMetric($activityArray[$ind], $ant, $actual, $pref, ...$metricsByPre);
			$activityArray[$ind] = $newInfo;
			$ant = $actual;
		}
		return $activityArray;
	}
	/**
	* Suma la variación entre 2 métricas a un diccionario con la información acumulada de un día (Día y hora).
	* @param {array} $info: diccionario con el formato dado por la función "getActivityInfo".
	* @param {App/Metric} $ant: metrica anterior o null.
	* @param {App/Metric} $act: metrica actual.
	* @param {string} $pref: prefijo que representa la red social (pertenece a los valores de SOCIAL_NETWORK_PRE).
	* @param {... string} $metrics: arreglo con los nombres de las interacciones que se van a sumar (pertenece a los valores de METRICS_BY_PRE).
	* @return diccionario luego de sumar los valores resultantes de la diferencia.
	*/
	protected function sumMetric($info, $ant, $act, $pref, ...$metrics){
		foreach ($metrics as $k => $m) {
			$metric = $act->$m - (($ant!=null)?$ant->$m: 0);
			//if($m == 'views' || $m == 'reactions' || $m == 'comments' || $m == 'shares' && $metric < 0)
			if($m=='views' && $metric < 0)	
		    $metric = 0;
		  
			$info[$pref.$m] += $metric;

		}
		return $info;
	}
}
