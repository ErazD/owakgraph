<?php

namespace App\Http\Controllers;

use App\Segmento;
use App\Product;
use App\Canasta;
use Illuminate\Http\Request;

class SegmentoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $route = 'segmento.store';
        $titulo = 'Segmentación';
        $parents = Canasta::all();
        // dd($parents);
        $segmento_producto = [];
        $segmento_producto['segments'] = Segmento::all();
        $segmento_producto['products'] = Product::all();

        return view('verticals.form', compact('route', 'titulo', 'parents', 'segmento_producto'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name' => 'required|string',
            'parent' => 'required|numeric',
        ]);
        $name = $request->input('name');
        $description = $request->input('description');
        $canasta_id = $request->input('parent');

        $vertical = Segmento::create([
            'name' => $name,
            'slug' => str_slug($name, '-'),
            'canasta_id' => $canasta_id,
            'description' => $description,
        ]);
        return redirect()->back()->withSuccess('La Segmentación '.$name.' ha sido creada');
    }

    /**
    * Cambia el segmento al que pertenece un producto.
    * @param {App\Segmento} $segmento: Segmento al que se agregará el producto.
    * @param {App\Product} $product: Producto a agregar.
    * @return \Illuminate\Http\Response
    */
    public function addproductToSegment(Segmento $segmento, Product $product){
        // dd($product);
        $product->segmento_id = $segmento->id;
        $product->save();
        return redirect()->back()->withSuccess('Se ha añadido '.$product->name.' a '.$segmento->name);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Segmento  $segmento
     * @return \Illuminate\Http\Response
     */
    public function show(Segmento $segmento)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Segmento  $segmento
     * @return \Illuminate\Http\Response
     */
    public function edit(Segmento $segmento)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Segmento  $segmento
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Segmento $segmento)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Segmento  $segmento
     * @return \Illuminate\Http\Response
     */
    public function destroy(Segmento $segmento)
    {
        //
    }
}
