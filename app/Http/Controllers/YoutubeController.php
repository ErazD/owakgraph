<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Google_Client;
use Google_Service_YouTube;
use DateTime;
use App\YoutubeReport;

class YoutubeController extends Controller{
  protected $youtubeService;

  public function getYoutubeService(){
    if(!$this->youtubeService){
$DEVELOPER_KEY = 'AIzaSyB-4J7JpEHeB3npz_jzBMpM8chk0ftYQes';#'AIzaSyCi77guwu17_KHNXJrE9mU1z57-Lp9u_s4'; #'AIzaSyB-4J7JpEHeB3npz_jzBMpM8chk0ftYQes';
$client = new Google_Client();
$client->setDeveloperKey($DEVELOPER_KEY);
// Define an object that will be used to make all API requests.
$this->youtubeService = new Google_Service_YouTube($client);
}
return $this->youtubeService;
}


public function nuevo(){
  return view('youtube.y_create');
}
/**
* Obtiene el id de un canal desde su URL.
* nombreUrl: ListerineCO https://www.youtube.com/user/ListerineCO/
* channelId: UCmSeeUXdOcxLZGw9xXUb8dQ https://www.youtube.com/channel/UCmSeeUXdOcxLZGw9xXUb8dQ
* @param {string} $ch_url: url del canal de youtube
* @return {string} id de youtube del canal.
*/
public function getChannelIdFromURL($ch_url){
  $channel_url = $ch_url;
  $channel_url = str_replace("https://", "", $channel_url);
  $channel_url = str_replace("http://", "", $channel_url);
$type="id"; // determina si se usa el nombre de usuario o la id el canal.
$id_channel="";  
if(strpos($channel_url,'/user/'))
{
  $type="forUsername";
  $uid=explode('/user/', $channel_url,2)[1];
  if(strpos($uid , '/')){
    $uid = explode('/', $uid,2)[0];
  }
  $id_channel=$uid;
} else if(strpos($channel_url , '/channel/')){
  $type="id";
  $cid=explode('/channel/', $channel_url,2)[1];
  if(strpos($cid , '/') ){
    $cid = explode('/', $cid,2)[0];
  }
  $id_channel=$cid;
}

$youtube = $this->getYoutubeService();
try {
  $channelsResponse = $youtube->channels->listChannels('id', array(
    $type => $id_channel,
    'fields' => 'items(id)'
  ));
  $channel_id='';
  foreach ($channelsResponse['items'] as $ch) {
    $channel_id= $ch['id'];
  }

  return $channel_id;

} catch (Google_Service_Exception $e) {
  return $e->getMessage();
} catch (Google_Exception $e) {
  return $e->getMessage();
}
}

/**
 * Crea un nuevo canal desde una url
 *
 *
 */
public function createChannelFromURL(Request $request){ // $channel_url){
  $this->validate($request,[
//  'name' => 'required|string',
    'channel_url' => 'required|string',
  ]);

  $channel_url = $request->channel_url;
  $channel_id = $this->getChannelIdFromURL($channel_url);
  $youtube = $this->getYoutubeService();

  try {

    $searchResponse = $youtube->search->listSearch('snippet', array(
      'channelId' => $channel_id,
      'type' => 'video',
      'order' => 'date',
      'maxResults' => 50,
      'fields' => 'items(id(videoId)),nextPageToken'
    ));

    $videoResults = array();

    foreach ($searchResponse['items'] as $searchResult) {
      array_push($videoResults, $searchResult['id']['videoId']);
    }
    $videoIds = join(',', $videoResults);
//$videoIds .= ',CgJShaqpcks'; //dd($videoIds);

// Call the videos.list method to retrieve location details for each video.
    $videosResponse = $youtube->videos->listVideos('snippet, statistics', array(
      'id' => $videoIds,
      'fields' => 'items(id,snippet(description,publishedAt,thumbnails/standard/url,title),statistics(commentCount,dislikeCount,likeCount,viewCount))'
    ));

    $videos = '';
    $cont=1;
    foreach ($videosResponse['items'] as $videoResult) {

      $videos .= sprintf('<li><h1> %s) Published: %s</h1> <h4>%s</h4> <b>Content:</b>%s, <a href="https://www.youtube.com/watch?v=%s"> <img src="%s"> </a> <br> <h4>Métricas</h4> <b>vistas: </b> %s, <b>Comentarios: </b> %s, <b>likes: </b> %s - <b>dislikes: </b> %s <br><hr><br><br></li>',
        $cont,
        $videoResult['snippet']['publishedAt'],
        $videoResult['snippet']['title'],
        $videoResult['snippet']['description'],
        $videoResult['id'],
        $videoResult['snippet']['thumbnails']['standard']['url'],          
        $videoResult['statistics']['viewCount'],
        $videoResult['statistics']['commentCount'],
        $videoResult['statistics']['likeCount'],
        $videoResult['statistics']['dislikeCount']

      );
      $cont++;
    }

    return $videos;

  } catch (Google_Service_Exception $e) {
    $htmlBody .= sprintf('<p>A service error occurred: <code>%s</code></p>',
      htmlspecialchars($e->getMessage()));
  } catch (Google_Exception $e) {
    $htmlBody .= sprintf('<p>An client error occurred: <code>%s</code></p>',
      htmlspecialchars($e->getMessage()));
  }
}

public function updateAllVideoMetrics(){

}


//--------------------------------------------------------------------------------------------
/*-------------------------------------------------------*
* Obtiene todas las publicaciones de una fanpage según el
* rango de fechas seleccionado
* @return array
*-------------------------------------------------------*/
public function YoutubeChannelReport(Request $request){

//Scrap desde
  $since_validation = new DateTime($request->input('date_since'));
  $since_format = date_format($since_validation, "Y-m-d\TH:i:sP"); 
//Scrap hasta
  $until_validation = new DateTime($request->input('date_to'));
  $until_format = date_format($until_validation, "Y-m-d\TH:i:sP");

  $interval = $until_validation->diff($since_validation);
  $days = $interval->format('%a');
// if($days <= 62){
//Validar si el request está completo

  $this->validate($request,
    ['url' => 'required',
    'date_since' => 'required',
    'date_to' => 'required'],
    ['url.required' => 'El campo URL no debe estar vacío',
    'date_since.required' => 'El campo Desde no debe estar vacío',
    'date_to.required' => 'El campo Hasta no debe estar vacío',
  ]);

  $ch_url =  $request->input('url');        
  $youtube = $this->getYoutubeService();
  $channel_id = $this->getChannelIdFromURL($ch_url);
  try {  
    $pageToken="";
    $allVideoIds= array();
    do{
      $searchResponse = $youtube->search->listSearch('snippet', array(
        'channelId' => $channel_id,
        'type' => 'video',
        'order' => 'date',
        'publishedAfter' => $since_format,
        'publishedBefore' => $until_format,
        'maxResults' => 50,
        'fields' => 'items(id(videoId)),nextPageToken',
        'pageToken' => $pageToken
      ));

      $videoResults = array();
      $pageToken = $searchResponse['nextPageToken'];

      foreach ($searchResponse['items'] as $searchResult) {
        array_push($videoResults, $searchResult['id']['videoId']);
      }
      $videoIds = join(',', $videoResults);
      array_push($allVideoIds, $videoIds);

    }while($pageToken != "");

    $totalPosts = array();
    $pageCount = 0;
    $unique_id = hexdec(uniqid());

    foreach ($allVideoIds as $videoIds) {
      $videosResponse = $youtube->videos->listVideos('snippet, statistics', array(
        'id' => $videoIds,
        'fields' => 'items(id,snippet(description,publishedAt,thumbnails/standard/url,title),statistics(commentCount,dislikeCount,likeCount,viewCount))'
      ));
# Call the videos.list method to retrieve location details for each video.      //do{
      foreach ($videosResponse['items'] as $videoResult) {

        $rep = array(
          'title' => $videoResult['snippet']['title'],
          'content' => $videoResult['snippet']['description'],
          'likes' => $videoResult['statistics']['likeCount'],
          'dislikes' => $videoResult['statistics']['dislikeCount'],
          'comments' => $videoResult['statistics']['commentCount'],
          'views' => $videoResult['statistics']['viewCount'],
          'type' => "Video",
          'url' => "https://www.youtube.com/watch?v=".$videoResult['id'],
          'published' => $videoResult['snippet']['publishedAt']
        );
        $report = new YoutubeReport();
        $report->saveReport($rep, $unique_id); 

        array_push($totalPosts, $rep);               
      }
      $pageCount++;
    }

    return view('pages.results.video_posts', compact('totalPosts', 'request', 'fanpage_name', 'unique_id'));

  } catch (Google_Service_Exception $e) {
    $htmlBody .= sprintf('<p>A service error occurred: <code>%s</code></p>',
      htmlspecialchars($e->getMessage()));
  } catch (Google_Exception $e) {
    $htmlBody .= sprintf('<p>An client error occurred: <code>%s</code></p>',
      htmlspecialchars($e->getMessage()));
  }

// }else{
//   return redirect()->back()->withInput()->withErrors('el rango de fechas seleccionado supera los 60 días. ('.$days.')');
// }
}
public function YoutubeCommentsReport(Request $request){
// $filter_option = (!is_null($request->input('date'))) ? true : false;

// if($filter_option){
//   $this->validate($request,
//     ['id' => 'required',
//     'date_since' => 'required',
//     'date_to' => 'required'],
//     ['id.required' => 'El campo YouTube ID no debe estar vacío',
//     'date_since.required' => 'El campo Desde no debe estar vacío',
//     'date_to.required' => 'El campo Hasta no debe estar vacío',
//   ]);
// }else{
  $this->validate($request,
    ['id' => 'required'],
    ['id.required' => 'El campo YouTube ID no debe estar vacío']);
// }

  $video_id = $request->input('id');

//    //Scrap desde
// $since_validation = new DateTime($request->input('date_since'));
// $since_format = date_format($since_validation, "Y-m-d\TH:i:sP"); 
//   //Scrap hasta
// $until_validation = new DateTime($request->input('date_to'));
// $until_format = date_format($until_validation, "Y-m-d\TH:i:sP");

  $youtube = $this->getYoutubeService();

  $totalComments = array();
  try {  
    $pageToken="";
    do{
      $searchResponse = $youtube->commentThreads->listCommentThreads('snippet', array(
        'videoId' => $video_id,
        'maxResults' => 100,
        'fields' => 'items(snippet(topLevelComment(snippet(authorChannelUrl,authorDisplayName,likeCount,publishedAt,textDisplay)))),nextPageToken',
        'pageToken' => $pageToken
      ));

      $pageToken = $searchResponse['nextPageToken'];

      foreach ($searchResponse['items'] as $commentResult) {

        $fecha_format = new DateTime($commentResult['snippet']['topLevelComment']['snippet']['publishedAt']);
        $fecha = date_format($fecha_format, 'Y/m/d H:i:s');

        $vid = array(
          'id' => $video_id,
          'username' => $commentResult['snippet']['topLevelComment']['snippet']['authorDisplayName'],
          'userchannel' => $commentResult['snippet']['topLevelComment']['snippet']['authorChannelUrl'],
          'url' => "https://www.youtube.com/watch?v=".$video_id,
          'message' => $commentResult['snippet']['topLevelComment']['snippet']['textDisplay'],
          'likes' => $commentResult['snippet']['topLevelComment']['snippet']['likeCount'],
          'published' => $fecha
        );
        array_push($totalComments, $vid);
      }
    }while($pageToken != "");
    return view('pages.results.video_comments', compact('totalComments', 'request')); 
  } catch (Google_Service_Exception $e) {
    $htmlBody .= sprintf('<p>A service error occurred: <code>%s</code></p>',
      htmlspecialchars($e->getMessage()));
  } catch (Google_Exception $e) {
    $htmlBody .= sprintf('<p>An client error occurred: <code>%s</code></p>',
      htmlspecialchars($e->getMessage()));
  }
}
}
