<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\YoutubeChannel;
use App\YoutubeVideo;
use App\YoutubeMetrics;
use App\YoutubeScrap;
use App\Brand;
use DateTime;
use DB;
use Carbon\Carbon;
use App\FanCount;

class YoutubeChannelController extends Controller
{
  public function __construct(){
    $this->middleware('industry_role:YoutubeChannelController,channel', ['only' => ['show']]);
  } 

    /*-------------------------------------------------------*
     * YoutubeChannel. Muestra todos los videos de ese canal
     * @return videos collection
     *-------------------------------------------------------*/

    public function show(YoutubeChannel $channel, $since=null, $until=null)
    {
     $posts = MethodsController::filterByDate($channel->posts(), $since, $until)->paginate(20);
     $since = ($since!=null)?$since:"";
     $until = ($until!=null)?$until:"";
     return view('youtube.channels.channel', compact('channel', 'posts', 'since','until'));
   }


    /*-------------------------------------------------------*
     * YoutubeChannel. Muestra todos los videos que han tenido actividad en el día
     * @return videos collection
     *-------------------------------------------------------*/

    public function hot(YoutubeChannel $channel)
    {

    	$hot_videos = YoutubeMetrics::join('youtube_videos', 'youtube_metrics.youtube_video_id', '=', 'youtube_videos.id')
    	->select(array(DB::Raw('COUNT(youtube_metrics.youtube_video_id) as count'),
    		'youtube_metrics.*', 'youtube_videos.*'))
    	->where('youtube_metrics.created_at', '>=', Carbon::today())
    	->where('youtube_videos.youtube_channel_id', $channel->id)
    	->orderBy('count', 'DESC')
    	->groupBy('youtube_videos.id')
    	->paginate(20);    	
      return view('youtube.channels.hot-posts', compact('channel','hot_videos'));

    }

    /*-------------------------------------------------------*
     * Consulta la cantidad de subscriptores que tiene el canal de youtube
     * @return posts collection
     *-------------------------------------------------------*/
    public function subsCount(Brand $brand, YoutubeChannel $channel, YoutubeController $yt, Fancount $subsCount)
    {      
      $channels = YoutubeChannel::where('brand_id', $brand->id)->get();
      $youtube = $yt->getYoutubeService();
      $subs = 0;
      foreach($channels as $channel){
        $channelsResponse = $youtube->channels->listChannels('statistics' ,array(
          'id' => $channel->youtube_id,
          'fields'=>'items(id, statistics/subscriberCount)',
        ));    
        foreach($channelsResponse['items'] as $channelResult){
         $subs = $channelResult['statistics']['subscriberCount'];
         if(!$subs){
          $subs=0;
        }
        $subsCount->addYoutubeFollowerMetrics($subs, $channel->id);  
      }  
    }
  }

  
  public function getAllSubMetricsByDay(YoutubeChannel $channel)
  {
    $values = array();
    $val=false;
    
    $orderbydate = DB::table('fan_counts')->where('element_id', $channel->id)->where('table', 'youtube_channels')->get();
    $fechas = array();

    foreach ($orderbydate as $key => $date) {
      array_push($fechas, ['date' => "$date->created_at", 'followers' => "$date->fan_count"]);
      $val=true;
    }
    if($val){
      $d = array('fanpage_id' => $channel->id, 'name'=> 'Subscriptores ' . $channel->brand->name .' '. $channel->name, 'data' => $fechas,  'color' => 'undefined');
      array_push($values, $d);
    }
    if(!$val){
      $null_val = array();
      array_push($null_val, ['date' => date("Y/m/d") ,'followers' => 'undefined']);
      $d = array('fanpage_id' => $channel->id, 'name'=> 'Seguidores ' . $channel->brand->name .' '. $channel->name, 'data' => $null_val);
      array_push($values, $d);
    }
    return response()->json($values);
  }
    /*-------------------------------------------------------*
     * Buscar en la lista de posts del canal según su contenido
     * @return posts collection
     *-------------------------------------------------------*/

    public function search(Request $request, YoutubeChannel $channel)
    {   
    	$channel_id = $channel->id;
    	$query = $request->input('query');

    	$posts = YoutubeVideo::where('youtube_channel_id', $channel_id)
    	->where('title', 'LIKE', "%$query%")
      ->orwhere('content', 'LIKE', "%$query%")
      ->orderBy('published', 'DESC')
      ->paginate(20);

      return view('youtube.channels.channel', compact('channel', 'posts'));
    }


    /*-------------------------------------------------------*
     * Crear Fanpage
     *-------------------------------------------------------*/

    public function create(Request $request, YoutubeController $yt)
    {
            //Validar que el request esté completo
    	$this->validate($request, [
    		'name' => 'required',
    		'url' => 'required',
     ]);
    	$country = YoutubeChannel::firstOrNew([
    		'brand_id' => $request->input('brand_id'),
    		'name' => $request->input('name'),
     ]);
    	if ($country->exists) {
    		return redirect('/brand/'.$request->input('brand_id'))->withSuccess($request->input('name').' ya fue creada.');
    	}else{
    		$yt_id = $yt->getChannelIdFromURL($request->input('url'));
                    //Si no está, se guarda la fanpage en nuestra BD.
    		$fanpage = YoutubeChannel::create([
    			'name' => $request->input('name'),
    			'url' => $request->input('url'),
    			'brand_id' => $request->input('brand_id'),
    			'social_network' => 'YouTube',
    			'youtube_id' => $yt_id,
        ]);
    		return redirect('/brand/'.$fanpage->brand_id)->withSuccess('El canal  de YouTube creado '.$fanpage->name.' ha sido añadido');
    	}

    }



    /*-------------------------------------------------------*
     * Obtener las publicaciones de una Canal específico
     *-------------------------------------------------------*/

    public function getYoutubeVideos(YoutubeChannel $channel, YoutubeController $yt, YoutubeVideo $video)
    {

    	$scrap_data = YoutubeScrap::where('youtube_channel_id', $channel->id)->first();

    	(is_null($scrap_data))? $last_cron = (new \DateTime(date("Y")."-01-01")): $last_cron = $scrap_data->updated_at; 

          //Scrap desde
    	$since_validation = new DateTime($last_cron->format('Y-m-d'));
    	$since = date_format($since_validation, "Y-m-d\TH:i:sP"); 
		   //Scrap hasta
        //$unt=
    	$until_validation = new DateTime(date('Y-m-d H:i:s'));
      $until_validation->modify("23:59:59");
      $until = date_format($until_validation, "Y-m-d\TH:i:sP");

      $youtube = $yt->getYoutubeService();
      $channel_id = $channel-> youtube_id;


      try {
        $pageToken="";
        $allVideoIds= array();
        do{
         $searchResponse = $youtube->search->listSearch('snippet', array(
          'channelId' => $channel_id,
          'type' => 'video',
          'order' => 'date',
          'maxResults' => 50,
          'fields' => 'items(id(videoId)),nextPageToken',
          'pageToken' => $pageToken
        ));
         $videoResults = array();
         $pageToken=$searchResponse['nextPageToken'];

         foreach ($searchResponse['items'] as $searchResult) {
          array_push($videoResults, $searchResult['id']['videoId']);
        }
        $videoIds = join(',', $videoResults);
        array_push($allVideoIds, $videoIds);
      }while ($pageToken != "");

      foreach ($allVideoIds as $videoIds) {
       $videosResponse = $youtube->videos->listVideos('snippet, statistics', array(
        'id' => $videoIds,
        'fields' => 'items(id,snippet(description,publishedAt,thumbnails/high/url,title),statistics(commentCount,dislikeCount,likeCount,viewCount))'
      ));
                //dd($videosResponse);
       foreach ($videosResponse['items'] as $videoResult) {
        $com = $videoResult['statistics']['commentCount'];
        if(!$com){
          $com=0;
        }
        $fecha_format = new DateTime($videoResult['snippet']['publishedAt']);
        $fecha = date_format($fecha_format, 'Y/m/d H:i:s');
                   // dd($fecha,$videoResult['snippet']['publishedAt']);
        $rep = array(
         'youtube_channel_id' => $channel_id,
         'youtube_id' => $videoResult['id'],
         'title' => $videoResult['snippet']['title'],
         'content' => $videoResult['snippet']['description'],
         'type' => "Video",
         'permalink_url' => "https://www.youtube.com/watch?v=".$videoResult['id'],
         'attachment' => $videoResult['snippet']['thumbnails']['high']['url'],          
         'published' =>  $fecha,            

         'likes' => $videoResult['statistics']['likeCount'],
         'dislikes' => $videoResult['statistics']['dislikeCount'],
         'comments' => $com,
         'views' => $videoResult['statistics']['viewCount']
       );
        $video->add($rep, $channel->id);           
      }

    }

  } catch (Google_Service_Exception $e) {
    dd($e);
  } catch (Google_Exception $e) {
    dd($e);
  }
  if(is_null($scrap_data)){
    YoutubeScrap::create([
     'youtube_channel_id' => $channel->id,
     'updated_at' => date('Y-m-d H:i:s'),
   ]);
  }else{
    $scrap_data->update([
     'updated_at' => date('Y-m-d H:i:s')
   ]);
  }

  return redirect('/channel/'.$channel->id);

}




    /*-------------------------------------------------------*
     * Obtener las métricas de los videos de un canal específico
     *-------------------------------------------------------*/

    public function getPostMetrics(YoutubeChannel $channel, YoutubeMetrics $metric, YoutubeController $yt)
    {
    	$channel_videos = $channel->posts;
    	$cant = 0;
    	$youtube = $yt->getYoutubeService();
    	$vd_ids = array();
    	$all_vd_ids = array();
    	foreach ($channel_videos as $post) {
    		array_push($vd_ids, $post->youtube_id);
    		$cant++;
    		if($cant >= 50){
    			$videoIds = join(',', $vd_ids);
    			array_push($all_vd_ids, $videoIds);
    			$vd_ids= array();
    			$cant = 0;
    		}
    	}    	

    	try{
    		foreach ($all_vd_ids as $videoIds) {
    			$videosResponse = $youtube->videos->listVideos('snippet, statistics', array(
    				'id' => $videoIds,
    				'fields' => 'items(id,statistics(commentCount,dislikeCount,likeCount,viewCount))'
         ));
    			foreach ($videosResponse['items'] as $videoResult) {
    				$youtube_id = $videoResult['id'];
            $com = $videoResult['statistics']['commentCount'];
            if(!$com){
              $com=0;
            }
            $stat = array(
             'likes' => $videoResult['statistics']['likeCount'],
             'dislikes' => $videoResult['statistics']['dislikeCount'],
             'comments' => $com,
             'views' => $videoResult['statistics']['viewCount']
           );
            $vide=$channel_videos->where('youtube_id',$youtube_id)->first();
            $new_scrap = $metric->addMetric($stat, $vide['id']); 
            if($vide->scrap_type != $new_scrap){
              $vide->$scrap_type = $new_scrap;
              $vide->save();
            }
          }
        }
      } catch (Google_Service_Exception $e) {
        dd($e);
      } catch (Google_Exception $e) {
        dd($e);
      }
      return redirect('/channel/'.$channel->id);
    }

  }