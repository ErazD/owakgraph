<?php

namespace App\Http\Controllers;

use Auth;
use App\User;
use App\Vertical;
use App\Categoria;
use App\Brand;
use Illuminate\Http\Request;

class CategoriaController extends Controller
{
    public function __construct(){
        $this->middleware('industry_role:Categoria,categorium', ['only' => ['show']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       return redirect()->action('VerticalController@index');
   }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       $route='categoria.store';
       $titulo='Categoría';
       $parents = Vertical::all();
       return view('verticals.form', compact( 'route', 'titulo', 'parents'));
   }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $this->validate($request,[
        'name' => 'required|string',
        'parent' => 'required|numeric',
    ]);
      $name = $request->input('name');
      $description = $request->input('description');
      $vertical_id = $request->input('parent');
      $vertical = Categoria::create([
        'name' => $name,
        'slug' => str_slug($name, '-'),
        'description' => $description,
        'vertical_id' => $vertical_id,

    ]);
      return redirect()->action('VerticalController@index')->withSuccess('La vertical '.$name.' ha sido creada');
  }

    /**
     * Display the specified resource.
     *
     * @param  \App\Categoria  $categorium
     * @return \Illuminate\Http\Response
     */
    public function show(Categoria $categorium)
    {
        $user = Auth::user();
        $user = User::where('id', 11)->first();        
        $brands = null;      
        foreach ($categorium->usercanastas($user)->get() as $canasta) {
           if(sizeof($canasta->userbrands($user)->get())>0){ 
            if($brands==null)
                $brands = $canasta->userbrands($user);
            else        
                $brands = $brands->union($canasta->userbrands($user));
        }
    }
    if($brands==null)
        $brands=array();
    else
        $brands = $brands->get();
    return view('verticals.categoria', compact( 'brands', 'categorium', 'user'));
}

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Categoria  $categorium
     * @return \Illuminate\Http\Response
     */
    public function edit(Categoria $categorium)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Categoria  $categorium
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Categoria $categorium)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Categoria  $categorium
     * @return \Illuminate\Http\Response
     */
    public function destroy(Categoria $categorium)
    {
        //
    }
}
