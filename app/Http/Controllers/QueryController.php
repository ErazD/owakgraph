<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Vertical;
use App\Categoria;
use App\Canasta;
use App\Brand;
use App\YoutubeChannel;
use App\YoutubeVideo;
use App\YoutubeMetrics;
use App\Fanpages;
use App\Report;
use App\Post;
use App\Metric;
use DateTime;
use DateInterval;
use DatePeriod;


class QueryController extends Controller
{

	public function index(){
		return view('consults.consult');
	}

	/**
	* Realiza una consulta de la variación de métricas en los posts por días pertenecientes al tipo dado (Vertical, categoría, canasta, marca).
	*Solo sirve para un elemento, no está habilitado pararecorrer múltiples valores.
	* @param {date-format} (Y-m-d) fecha inicial para el filtro
	* @param {date-format} (Y-m-d) fecha final para el filtro
	* @param {string} nombre de la categoría en la que se realizará la búsqueda. (vertical, categoría, canasta ó marca)
	* @param {string} slug del elemento perteneciente a la categoría donde se realizará la búsqueda.
	* @param {string} intervalo válido para crear un DateInterval. "1 day", "1 week", "1 month"
	* @return  Diccionario com los datos de la consulta.
	*/
	public function consultMetrics($since, $until,$type,$slug, $interval ="1 day"){

		dd($since, $until, $type, $slug, $interval);
		
		$firstDate=new DateTime($since);
		$lastDate=new DateTime($until);
		$intervalTime = /*M*/DateInterval::createFromDateString($interval);
		date_add($lastDate, $intervalTime);
		$period = new DatePeriod($firstDate, $intervalTime, $lastDate);
		$daysValues=array();
		
		$vertical = $this->getCategory($type,$slug);

		foreach ($period as $dt){
			
			$dtf = $dt->format('Y-m-d');
			$d = $this->getCategoryPublications($type, $vertical, $dt, $interval);		
			$dti = $dt->format('Y-m-d');
			$info_dia = array(
				'date_f' => $dtf,
				'date_i' => $dti,
				'type' => 'day',
				'subtype' => $type,
				$type => $d,
				'total' => $d['total']
				);			
			array_push($daysValues, $info_dia);
		}

		$info_consulta = array(
			'type'=>'days_group', 
			'subtype'=>'day', 
			'category' => $type,
			'days'=>$daysValues,
			'total' => $this->getTotalMetrics($daysValues),
			'max_dates' => $this->getDatesMaxMetrics($daysValues)
			);


		//dd('Consulta', $info_consulta);
		return $info_consulta;
	}

	/**
	* Obtiene una categoría según su tipo y su id (vertical, categoría, canasta ó marca)
	*
	* @param {string} $type: nombre de la categoría en la que se realizará la búsqueda. (vertical, categoría, canasta ó marca)
	* @param {string} $slug: slug del elemento perteneciente a la categoría donde se realizará la búsqueda.
	* @return Categoría obtenida
	*/
	protected function getCategory($type,$slug){
		$cate=null;
		switch ($type) {
			case 'vertical':
			$cate = Vertical::where('slug',$slug)->first();
			break;
			case 'categoria':
			$cate = Categoria::where('slug',$slug)->first();
			break;
			case 'canasta':
			$cate = Canasta::where('slug',$slug)->first();
			break;
			case 'marca':
			$cate = Brand::where('slug',$slug)->first();
			break;			
			default:
				# code...
			break;
		}
		return $cate;
	}

	/**
	 * Obtiene un diccionario con la varicación en las métricas de los posts de un elemento perteneciente a una vertical (vertical, categoría, canasta ó marca) en un periodo de tiempo dado.
	 * @param {string} $type: nombre de la categoría en la que se realizará la búsqueda. (vertical, categoría, canasta ó marca)
	 * @param $vertical: Elemento perteneciente a una categoría de la vertical;
	 * @param {DateTime} $dt: Fecha final del periodo de tiempo.
	 * @param {DateInterval} $interval: tamaño del periodo de tiempo.
	 * @return Diccionario con la variación de las métricas para el elemento perteneciente a la vertical.
	 */
	protected function getCategoryPublications($type, $vertical, $dt, $interval){
		$cate=null;
		switch ($type) {
			case 'vertical':
			$cate = $this->getFacebookPublicationsVertical($vertical, $dt, $interval);	 
			break;
			case 'categoria':
			$cate = $this->getFacebookPublicationsCategory($vertical, $dt, $interval);	 
			break;
			case 'canasta':
			$cate = $this->getFacebookPublicationsCanasta($vertical, $dt, $interval);	 
			break;
			case 'marca':
			$cate = $this->getFacebookPublicationsBrand($vertical, $dt, $interval);	 
			break;			
			default:
			break;
		}
		return $cate;
	}

	/**
	 * Obtiene las fechas en las que la varicación de métricas tiene el valor más alto para cada métrica.
	 * @param $days: lista de variacion en las métricas organizadas por días.
	 * @return Diccionario con las fechas con valores más altos para cada métrica.
	 */
	protected function getDatesMaxMetrics($days){
		$max_dates= array();
		$fb_r = 0;
		$fb_c = 0;
		$fb_s = 0;

		$yt_v = 0;
		$yt_c = 0;
		$yt_l = 0;
		$yt_d = 0;

		foreach ($days as $d) {
			$fbr = $d['total']['facebook_reactions'];
			if($fbr >= $fb_r){
				$fb_r = $fbr;
				$max_dates['max_facebook_reactions'] = array('facebook_reactions' => $fb_r, 'date_i' => $d['date_i'], 'date_f' => $d['date_f']);
			}
			$fbc = $d['total']['facebook_comments'];
			if($fbc >= $fb_c){
				$fb_c = $fbc;
				$max_dates['max_facebook_comments'] = array('facebook_comments' => $fb_c, 'date_i' => $d['date_i'], 'date_f' => $d['date_f']);
			}
			$fbs = $d['total']['facebook_shares'];
			if($fbs >= $fb_s){
				$fb_s = $fbs;
				$max_dates['max_facebook_shares'] = array('facebook_shares' => $fb_s, 'date_i' => $d['date_i'], 'date_f' => $d['date_f']);
			}
			$ytv = $d['total']['youtube_views'];
			if($ytv >= $yt_v){
				$yt_v = $ytv;
				$max_dates['max_youtube_views'] = array('youtube_views' => $yt_v, 'date_i' => $d['date_i'], 'date_f' => $d['date_f']);
			}
			$ytc = $d['total']['youtube_comments'];
			if($ytc >= $yt_c){
				$yt_c = $ytc;
				$max_dates['max_youtube_comments'] = array('youtube_comments' => $yt_c, 'date_i' => $d['date_i'], 'date_f' => $d['date_f']);
			}
			$ytl = $d['total']['youtube_likes'];
			if($ytl >= $yt_l){
				$yt_l = $ytl;
				$max_dates['max_youtube_likes'] = array('youtube_likes' => $yt_l, 'date_i' => $d['date_i'], 'date_f' => $d['date_f']);
			}
			$ytd = $d['total']['youtube_dislikes'];
			if($ytd >= $yt_d){
				$yt_d = $ytd;
				$max_dates['max_youtube_dislikes'] = array('youtube_dislikes' => $yt_d, 'date_i' => $d['date_i'], 'date_f' => $d['date_f']);
			}
		}
		return $max_dates;
	}


	/**
	 * Obtiene un diccionario con la varicación en las métricas de los posts de una vertical en un periodo de tiempo dado.
	 * @param {App/Vertical} $vertical: Vertical a la que se realiza la consulta
	 * @param {DateTime} $dt: Fecha final del periodo de tiempo.
	 * @param {DateInterval} $interval: tamaño del periodo de tiempo.
	 * @return Diccionario con la variación de las métricas para el elemento perteneciente a la vertical.
	 */
	public function getFacebookPublicationsVertical(Vertical $vertical, $dt, $interval){
		$publications = array();
		foreach ($vertical->categories as $category) {
			array_push($publications,  $this->getFacebookPublicationsCategory($category, $dt, $interval));
		}
		$info_vertical = array(
			'name' => $vertical->name,
			'type' => 'vertical',
			'vertical_id' => $vertical->id,
			'categories' => $publications,
			'total' => $this->getTotalMetrics($publications)	
			);
		//dd($vertical->name,$info_vertical);
		return $info_vertical;
	}

	/**
	 * Obtiene un diccionario con la varicación en las métricas de los posts de una Categoría en un periodo de tiempo dado.
	 * @param {App/Categoria} $categoria: Categoría a la que se realiza la consulta
	 * @param {DateTime} $dt: Fecha final del periodo de tiempo.
	 * @param {DateInterval} $interval: tamaño del periodo de tiempo.
	 * @return Diccionario con la variación de las métricas para el elemento perteneciente a la categoría.
	 */
	public function getFacebookPublicationsCategory(Categoria $categoria, $dt, $interval){
		$publications = array();
		foreach ($categoria->canastas as $canasta) {
			array_push($publications,  $this->getFacebookPublicationsCanasta($canasta, $dt, $interval));			
		}
		$info_categoy = array(
			'name' => $categoria->name,
			'type' => 'category',
			'categoria_id' => $categoria->id,
			'baskets' => $publications,
			'total' => $this->getTotalMetrics($publications)	
			);
		//dd($categoria->name,$info_categoy);
		return $info_categoy;
	}

	/**
	 * Obtiene un diccionario con la varicación en las métricas de los posts de una Canasta en un periodo de tiempo dado.
	 * @param {App/Canasta} $canasta: Canasta a la que se realiza la consulta
	 * @param {DateTime} $dt: Fecha final del periodo de tiempo.
	 * @param {DateInterval} $interval: tamaño del periodo de tiempo.
	 * @return Diccionario con la variación de las métricas para el elemento perteneciente a la canasta.
	 */
	public function getFacebookPublicationsCanasta(Canasta $canasta, $dt, $interval){
		$publications = array();
		foreach ($canasta->brands as $brand) {
			array_push($publications,$this->getFacebookPublicationsBrand($brand, $dt, $interval));
		}
		$info_canasta = array(
			'name' => $canasta->name,
			'type' => 'basket',
			'canasta_id' => $canasta->id,
			'brands' => $publications,
			'total' => $this->getTotalMetrics($publications)	
			);
		//dd($canasta->name,$info_canasta);
		return $info_canasta;
	}

	/**
	 * Obtiene un diccionario con la varicación en las métricas de los posts de una Marca en un periodo de tiempo dado.
	 * @param {App/Brand} $brand: Marca a la que se realiza la consulta
	 * @param {DateTime} $dtt: Fecha final del periodo de tiempo.
	 * @param {DateInterval} $interval: tamaño del periodo de tiempo.
	 * @return Diccionario con la variación de las métricas para el elemento perteneciente a la marca.
	 */
	public function getFacebookPublicationsBrand(Brand $brand, $dtt, $interval){
//		dd($dtt);
		$dt = date('Y-m-d', $dtt->getTimestamp());

		$dtInicial = date('Y-m-d', $dtt->modify('-'.$interval)->getTimestamp());
		//dd($dtt, $dt, $dtInicial);
		//dd($dt, $dtInicial);
		//$dt = $dt->getTimestamp();
		$fb_publications = array();
		$yt_publications = array();
		$publications = array();
		////--- Inicio de pedido de totales para las fanpages de Facebbok ----/////
		foreach ($brand->fanpages as $fpg) {
			$info_fb_post=  $this-> getFacebookListPostMetrics($dt, $dtInicial, $fpg->posts);
			$info = array(
				'type' => 'fanpage',
				'name' => $fpg->name,
				'fanpage_id' => $fpg->id,
				'facebook_id' => $fpg->facebook_id,
				'posts' => $info_fb_post,
				'total' => $this->getTotalMetrics($info_fb_post, 1)
				);
			array_push($fb_publications, $info);	

		}
		$publications = array_merge($publications, $fb_publications);	
		////--- Fin de pedido de totales para las fanpages de Facebbok ----/////
		////--- Inicio de pedido de totales para los canales de YouTube ----/////
		foreach ($brand->channels as $chn) {		
			$info_fb_post=  $this-> getYoutubeListPostMetrics($dt, $dtInicial, $chn->posts);
			$info = array(
				'type' => 'channel',
				'name' => $chn->name,
				'channel_id' => $chn->id,
				'youtube_id' => $chn->youtube_id,
				'posts' => $info_fb_post,
				'total' => $this->getTotalMetrics($info_fb_post, 2)
				);
			array_push($yt_publications, $info);		
		}
		
		$publications = array_merge($publications, $yt_publications);
		//dd('Publications ',$publications); 
		////--- Fin de pedido de totales para los canales de YouTube ----/////
		//dd($publications);
		$info_brand = array(
			'name' => $brand->name,
			'type' => 'brand',
			'brand_id' => $brand->id,
			'fanpages' => $fb_publications,
			'channels' => $yt_publications,
			'total' => $this->getTotalMetrics($publications)
			);
		//dd($brand->name,$info_brand);
		return $info_brand;
	}	

	/**
	* Calcula las métricas totales en base a los valores de la lista.
	* @param {Array} lista con los elementos que contienen las métricas a sumar.
	* @param {number} identificador para el tipo de post, si pertenece a una red social o es del modelo de verticales. 0:Vertical, 1:Facebook, 2:YouTube.
	* @return Diccionario con los valores totales para cada métrica.
	*/
	protected function getTotalMetrics($list, $typePost = 0){		
		$fb_r = 0;
		$fb_c = 0;
		$fb_s = 0;
		$yt_v = 0;
		$yt_c = 0;
		$yt_l = 0;
		$yt_d = 0;
		if($typePost == 0){ // 
			//dd($list);
			foreach ($list as $ele) {
				$total = $ele['total'];
				if(array_key_exists('facebook_reactions', $total)){
					$fb_r = $fb_r + $total['facebook_reactions'];
					$fb_c = $fb_c + $total['facebook_comments'];
					$fb_s = $fb_s + $total['facebook_shares'];
				}
				if(array_key_exists('youtube_views', $total)){
					$yt_v = $yt_v + $total['youtube_views'];
					$yt_c = $yt_c + $total['youtube_comments'];
					$yt_l = $yt_l + $total['youtube_likes'];
					$yt_d = $yt_d + $total['youtube_dislikes'];
				}
			}
		}else if($typePost == 1){ // facebook
			foreach ($list as $ele) {
				$fb_r = $fb_r + $ele['reactions'];
				$fb_c = $fb_c + $ele['comments'];
				$fb_s = $fb_s + $ele['shares'];
			}
		}else if($typePost == 2){ // youtube
			foreach ($list as $ele) {
				$yt_v = $yt_v + $ele['views'];
				$yt_c = $yt_c + $ele['comments'];
				$yt_l = $yt_l + $ele['likes'];
				$yt_d = $yt_d + $ele['dislikes'];
			}
		}		
		// Se asignan las métricas totales
		$total = array();
		if($typePost == 0 || $typePost == 1){
			$total['facebook_reactions'] = $fb_r;
			$total['facebook_comments'] = $fb_c;
			$total['facebook_shares'] = $fb_s;
		}
		if($typePost == 0 || $typePost == 2){
			$total['youtube_views'] = $yt_v;
			$total['youtube_comments'] = $yt_c;
			$total['youtube_likes'] = $yt_l;
			$total['youtube_dislikes'] = $yt_d;
		}
		return $total;
	}

	/**
	 * Obtiene la lista de métricas de una lista de posts para un periodo de tiempo dado.
	 * @param {DateTime} $dt: Fecha final del periodo de tiempo.
	 * @param {DateInterval} $interval: tamaño del periodo de tiempo.
	 * @param $posts: Lista de posts para hallar las métricas.
	 * @return lista de métricas
	 */
	public function getFacebookListPostMetrics($dt, $dtInicial, $posts){
		$list = array();
		foreach ($posts as $post) {
			array_push($list, $this->getMetricsFacebookPosts($dt, $dtInicial, $post->id, $post->facebook_id));
		}
		return $list;
	}

	/**
	 * Obtiene la variación en las métricas de un post.
	 * @param {DateTime} $dt: Fecha final del periodo de tiempo.
	 * @param {DateInterval} $interval: tamaño del periodo de tiempo.
	 * @param $id: id del post del que se buscarán las métricas.
	 * @param $fb_id: id de Facebook del post del que se buscarán las métricas.
	 * @return Diccionario con las métricas
	 */
	public function getMetricsFacebookPosts($dt, $dtInicial, $id, $fb_id){
		$r = 0;
		$c = 0;
		$s = 0;
		$begin = DB::table('metrics as w')->where('w.post_id', $id)->whereDate('created_at','<', $dtInicial)->latest()->first();
		$end = DB::table('metrics as w')->where('w.post_id', $id)->whereBetween('created_at', [$dtInicial, $dt])->latest()->first();
		$begin = ($begin)?$begin:$end;
		if($end){			
			$r = ($end->reactions != $begin->reactions)? $end->reactions - $begin->reactions:$end->reactions;
			$c = ($end->comments != $begin->comments)? $end->comments - $begin->comments:$end->comments;
			$s = ($end->shares != $begin->shares)? $end->shares - $begin->shares:$end->shares;
		}
		//$d = array('post_id' => $id, 'date_f' => $dt->format('Y-m-d'), 'reactions' => $r, 'comments' => $c, 'shares' => $s);
		$d = array('post_id' => $id, 'date_i' => $dtInicial, 'date_f' => $dt, 'reactions' => $r, 'comments' => $c, 'shares' => $s, 'facebook_id'=>$fb_id);
		return $d;
	}

	/**
	 * Obtiene la lista de métricas de una lista de videos de youtube para un periodo de tiempo dado.
	 * @param {DateTime} $dt: Fecha final del periodo de tiempo.
	 * @param {DateInterval} $interval: tamaño del periodo de tiempo.
	 * @param $posts: Lista de videos de YouTube para hallar las métricas.
	 * @return lista de métricas
	 */
	public function getYoutubeListPostMetrics($dt, $dtInicial, $posts){
		$list = array();
		foreach ($posts as $post) {
			array_push($list, $this->getMetricsYoutubePosts($dt, $dtInicial,$post->id, $post->youtube_id));
		}
		return $list;
	}

	/**
	 * Obtiene la variación en las métricas de un video de YouTube.
	 * @param {DateTime} $dt: Fecha final del periodo de tiempo.
	 * @param {DateInterval} $interval: tamaño del periodo de tiempo.
	 * @param $id: id del video del que se buscarán las métricas.
	 * @param $yt_id: id de Youtube del video del que se buscarán las métricas.
	 * @return Diccionario con las métricas
	 */
	public function getMetricsYoutubePosts($dt, $dtInicial, $id, $yt_id){		
		$yt_v = 0;
		$yt_c = 0;
		$yt_l = 0;
		$yt_d = 0;		
		$begin = DB::table('youtube_metrics as w')->where('w.youtube_video_id', $id)->whereDate('created_at','<', $dtInicial)->latest()->first();
		$end = DB::table('youtube_metrics as w')->where('w.youtube_video_id', $id)->whereBetween('created_at', [$dtInicial, $dt])->latest()->first();
		$begin = ($begin)?$begin:$end;
		if($end){			
			$yt_v = ($end->views != $begin->views)? $end->views - $begin->views:$end->views;
			$yt_c = ($end->comments != $begin->comments)? $end->comments - $begin->comments:$end->comments;
			$yt_l = ($end->likes != $begin->likes)? $end->likes - $begin->likes:$end->likes;
			$yt_d = ($end->dislikes != $begin->dislikes)? $end->dislikes - $begin->dislikes:$end->dislikes;
		}
		//$d = array('post_id' => $id, 'date_f' => $dt->format('Y-m-d'), 'reactions' => $r, 'comments' => $c, 'shares' => $s);
		$d = array('post_id' => $id, 'date_i' => $dtInicial, 'date_f' => $dt, 'views' => $yt_v, 'comments' => $yt_c, 'likes' => $yt_l, 'dislikes'=>$yt_d, 'youtube_id'=>$yt_id);
		return $d;
	}

}
