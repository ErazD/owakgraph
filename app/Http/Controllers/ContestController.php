<?php

namespace App\Http\Controllers;

use App\Contest;
use Illuminate\Http\Request;

class ContestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
           $this->validate($request,[
            'name' => 'required|string',
            'mechanics' => 'required',
            'prize' => 'required',
            'from-date' => 'required',
            'to-date' => 'required',
            'brand_id' => 'required',
        ]);
           $contest = Contest::create([
                'title' => $request->input('name'),
                'content' => $request->input('mechanics'),
                'prize' => $request->input('prize'),
                'since' => $request->input('from-date'),
                'until' => $request->input('to-date'),
                'brand_id' => $request->input('brand_id'),
           ]);

           return redirect()->back()->withSuccess('El concurso "'.$contest->title.'" ha sido creado');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Contest  $contest
     * @return \Illuminate\Http\Response
     */
    public function show(Contest $contest)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Contest  $contest
     * @return \Illuminate\Http\Response
     */
    public function edit(Contest $contest)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Contest  $contest
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Contest $contest)
    {
        $title = $request->input('name');
        if($title && $title!="")
            $contest->title = $title;
        $content = $request->input('mechanics');
    if($content && $content!="")
        $contest->content = $content;
        $prize = $request->input('prize');
    if($prize && $prize!="")
        $contest->prize = $prize;
        $since = $request->input('from-date');
    if($since && $since!="")
        $contest->since = $since;
        $until = $request->input('to-date');
    if($until && $until!="")
        $contest->until = $until;
    $contest->save();
    return redirect()->back()->withSuccess('El concurso "'.$contest->title.'" ha sido actualizado');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Contest  $contest
     * @return \Illuminate\Http\Response
     */
    public function destroy(Contest $contest)
    {
        $contest->delete();
        return redirect()->back()->withSuccess('El concurso ha sido eliminado correctamente');
    }
}
