<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \SammyK\LaravelFacebookSdk\LaravelFacebookSdk as Facebook;
use Session;
use App\Report;
use App\Post;
use App\Metric;
use DateTime;
use App\Token;
use DB;
//Metodos Youtube

class PagesController extends Controller
{
    /*-------------------------------------------------------*
     * Reporte Rápido - Publicaciones.
     * Ejecuta a fanpageReport()
     * @return view
     *-------------------------------------------------------*/
    public function fanpage()
    {
    	return view('pages.fanpages');
    }

     

	 /*-------------------------------------------------------*
     * Obtiene todas las publicaciones de una fanpage según el
     * rango de fechas seleccionado
     * @return array
     *-------------------------------------------------------*/

	 public function fanpageReport(Request $request, Facebook $fb)
	 {  
	 	//Scrap desde
	 	$since_validation = new DateTime($request->input('date_since'));
	 	//Scrap hasta
	 	$until_validation = new DateTime($request->input('date_to'));
	 	$interval = $until_validation->diff($since_validation);
	 	$report = new Report();
	 	$days = $interval->format('%a');

	 	if($days <= 62){
	 		//Validar si el request está completo
	 		$this->validate($request,
	 			['url' => 'required',
	 			'date_since' => 'required',
	 			'date_to' => 'required'],
	 			['url.required' => 'El campo URL no debe estar vacío',
	 			'date_since.required' => 'El campo Desde no debe estar vacío',
	 			'date_to.required' => 'El campo Hasta no debe estar vacío',
	 		]);
	 		//Obtener el token guardado en sesión para hacer pedidos en FB
	 		$token = Session::get('fb_user_access_token');

	 		//Convertir los strings en formato tiempo
	 		$since = strtotime($request->input('date_since'));
	 		//Sumarle un día más, para que se obtengan las publicaciones del último día seleccionado
	 		$until = strtotime($request->input('date_to')) + 23 * 60 * 60;

	 		//Obtener el nombre de la fanpage y alojarla en $fanpage_name
	 		preg_match('#https?\://(?:www\.)?facebook\.com/(\d+|[A-Za-z0-9\.]+)/?#', $request->input('url'), $fanpage_name);
	 		try{
	 			$dau = $fb->get('156938611533378?fields=daily_active_users', $token);
	 			$appUsage = $dau->getHeaders()['x-app-usage'];
	 			$appUsage = json_decode($appUsage, true);
	 			if($appUsage['call_count'] >= 95)
	 				return redirect()->back()->withInput()->withErrors('Se ha llegado al límite de llamados a facebook.'); 
		 		//guardar la data de FB en $posts
	 			$posts = $fb->get($fanpage_name[1].'/posts?fields=shares.summary(true),comments.limit(0).summary(true),reactions.limit(0).summary(true),message,type,permalink_url,created_time&since='.$since.'&until='.$until.'', $token);

	 			$maxPages = $request->input('limit') ? $request->input('limit') : INF;
	 			$pageCount = 0;
	 			$graphEdge = $posts->getGraphEdge();

	 			$unique_id = hexdec(uniqid());

	 			$totalPosts = array();

		 		//Realizar este do hasta que se acaben los posts
	 			do{

	 				foreach ($graphEdge as $page) {
	 					array_push($totalPosts, $page);
	 					$report->saveReport($page, $unique_id);

	 				}

	 				$pageCount++;

	 			} while ($pageCount < $maxPages && $graphEdge = $fb->next($graphEdge));

	 			return view('pages.results.posts', compact('totalPosts', 'request', 'fanpage_name', 'unique_id'));
	 		} catch (\Facebook\Exceptions\FacebookSDKException $e) {
	 			$appUsage = $e->getResponse()->getHeaders()['x-app-usage'];
	 			$appUsage = json_decode($appUsage, true);
	 			$ecode = $e->getCode();
	 			$esubcode = $e->getSubErrorCode();
	 			if($ecode == 100 || $ecode == 803){
	 				return redirect()->back()->withInput()->withErrors('No se encuentra la Fanpage Buscada.'); 
	 			}else if($ecode == 4){
	 				return redirect()->back()->withInput()->withErrors('Se ha sobrepasado el límite de pedidos a facebook.'); 
	 			}
	 		}
	 	}else{
	 		return redirect()->back()->withInput()->withErrors('el rango de fechas seleccionado supera los 60 días. ('.$days.')');
	 	}
	 	return redirect()->back();
	 }




	 /*-------------------------------------------------------*
     * Reporte Rápido - Comentarios.
     * Ejecuta a postReport()
     * @return view
     *-------------------------------------------------------*/

	 public function comments()
	 {
	 	return view('pages.posts');
	 }

	/*-------------------------------------------------------*
     * Obtiene los comentarios de una publicaciones con sus respectivos
     * datos
     * @return array
     *-------------------------------------------------------*/

	public function postReport(Request $request, Facebook $fb)
	{	

		$filter_option = (!is_null($request->input('date'))) ? true : false;
		$post_id = $request->input('id');

		$since = strtotime($request->input('date_since'));
		$until = strtotime($request->input('date_to')) + 23 * 60 * 60;

		if($filter_option){
			$this->validate($request,
				['id' => 'required',
				'date_since' => 'required',
				'date_to' => 'required'],
				['id.required' => 'El campo Facebook ID no debe estar vacío',
				// 'id.integer' => 'El campo Facebook ID debe ser un valor numérico',
				'date_since.required' => 'El campo Desde no debe estar vacío',
				'date_to.required' => 'El campo Hasta no debe estar vacío',
			]);
		}else{
			$this->validate($request,
				['id' => 'required'],
				['id.required' => 'El campo Facebook ID no debe estar vacío']);
				//'id.integer' => 'El campo Facebook ID debe ser un valor numérico']);
		}

		$attach_option = (!is_null($request->input('attach'))) ? true : false;
		
		$token = Session::get('fb_user_access_token');

		$dau = $fb->get('156938611533378?fields=daily_active_users', $token);
 			$appUsage = $dau->getHeaders()['x-app-usage'];
 			$appUsage = json_decode($appUsage, true);
 			if($appUsage['call_count'] >= 95)
 				return redirect()->back()->withInput()->withErrors('Se ha llegado al límite de llamados a facebook.'); 
	 			
		
		$fpg_id = $fb->get($post_id.'?fields=from',$token)->getDecodedBody()['from']['id'];
		$helper = $fb->get('/'.$fpg_id.'?fields=access_token',$token)->getDecodedBody();
		if(!empty($helper['access_token']))
			$token = $helper['access_token'];
		// dd($token);

		if($filter_option){
		// Query para obtener comentarios con fechas limites
			$comments = $fb->get($post_id.'/comments?fields=likes.limit(0).summary(true),message,comment_count,attachment,created_time,from,permalink_url&summary=1&filter=stream&since='.$since.'&until='.$until.'', $token);
		}else{
		// Query para obtener todos los mensajes
			$comments = $fb->get($post_id.'/comments?fields=likes.limit(0).summary(true),message,comment_count,attachment,created_time,from,permalink_url&summary=1&filter=stream', $token);
		}

		$graphEdge = $comments->getGraphEdge();
		$maxPages = $request->input('limit') ? $request->input('limit') : INF;
		$pageCount = 0;

		$totalComments = array();

		$post_type = $fb->get($post_id.'?metadata=true', $token)->getGraphObject();

		do{

			foreach ($graphEdge as $comment) {
				array_push($totalComments, $comment);
			}

			$pageCount++;

		} while ($pageCount < $maxPages && $graphEdge = $fb->next($graphEdge));

		return view('pages.results.comments', compact('totalComments', 'request'));

	}


	public function tokens()
	{	
		$token = Token::first();
		return view('pages.token', compact('token'));
	}

}
