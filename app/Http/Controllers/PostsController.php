<?php

namespace App\Http\Controllers;

use App\Http\Requests\ManualPostCreateRequest;
use Illuminate\Http\Request;
use Illuminate\Routing\Route;
use App\Post;
use App\Fanpage;
use Session;
use Excel;
use DB;
use App\Metric;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request as GuzzRequest;
use \SammyK\LaravelFacebookSdk\LaravelFacebookSdk as Facebook;

class PostsController extends Controller
{
	public function __construct(){
		$this->middleware('industry_role:Post,post', ['only' => ['show']]);
	}
	
    /*-------------------------------------------------------*
     * Post. Muestra la información de un post
     * @return Object
     *-------------------------------------------------------*/

    public function show(Post $post){
    	$post->load('metrics');

    	return view('posts.post', compact('post'));
    }

   /*-------------------------------------------------------*
     * Post. Guarda la imagen de las publicaciones en la base de datos.
     * @return string
     *-------------------------------------------------------*/
    public function saveImg(Post $post, $imgPath){
         //$url = url('/');
    	$url = 'imgUpload/' . 'fb_img_' .$post->id.'.jpg';
    	if(!empty($imgPath)){
    		file_put_contents(public_path($url), file_get_contents($imgPath));
    	}
    	return $url;
    }

	 /*-------------------------------------------------------*
     * Post. Añadir post manualmente mediante su ID
     * @return Object
     *-------------------------------------------------------*/

	 public function add(ManualPostCreateRequest $request, Facebook $fb, Post $pt)
	 {	

	 	$token = Session::get('fb_user_access_token');

	 	$dau = $fb->get('156938611533378?fields=daily_active_users', $token);
	 	$appUsage = $dau->getHeaders()['x-app-usage'];
	 	$appUsage = json_decode($appUsage, true);
	 	if($appUsage['call_count'] >= 95)
	 		return redirect()->back()->withErrors('Se ha llegado al límite de llamados a facebook.');

	 	$post_id = basename(parse_url($request->input('url'), PHP_URL_PATH));

	 	$fanpage = Fanpage::where('url', $request->input('fanpage_url'))->first();

	 	preg_match('#https?\://(?:www\.)?facebook\.com/(\d+|[A-Za-z0-9\.]+)/?#', $fanpage->url, $fanpage_name);

	 	$fanpage_id = $fb->get($fanpage_name[1].'?fields=id', $token);
	 	$fanpage_id = $fanpage_id->getDecodedBody();

	 	$post = $fb->get($fanpage_id['id'].'_'.$post_id.'?fields=shares.summary(true),comments.limit(0).summary(true),reactions.limit(0).summary(true),message,type,permalink_url,created_time,attachments',$token);

	 	$post = $post->getDecodedBody();

	 	$saved = $pt->manualAdd($post, $fanpage->id);

	 	return redirect('posts/'.$saved->id);

	 }
    
     /*-------------------------------------------------------*
     * Elimina una publicación
     * @return Collection
     *-------------------------------------------------------*/
     public function delete(Post $post){
         if($post){ 
         $post->delete();
         if(!empty($post->twofactors)){
         $twoFactors = $post->twofactors;
         foreach($twoFactors as $twoFactor){
         	$twoFactor->delete();
         }
         }
         }
        return redirect()->back()->withSuccess('El post ha sido eliminado con éxito.');
     }


	 /*-------------------------------------------------------*
     * Otener los posts con más interacciones
     * @return Collection
     *-------------------------------------------------------*/

	 public function hot()
	 {

	 	$hot_posts = Metric::with('post')
	 	->select(array(DB::Raw('count(metrics.post_id) as count'),
	 		'metrics.*'))
	 	->groupBy('metrics.post_id')
	 	->orderBy('count', 'desc')
	 	->take(10)
	 	->get();

	 	return view('posts.hot', compact('hot_posts'));

	 }

     /*-------------------------------------------------------*
     * Otener los posts de video con metricas de video 
     * @return Collection
     *-------------------------------------------------------*/
     public function frozen(){
     	$frozen = Post::where('scrap_state', -7)->paginate(20);
     	return view('posts.frozen', compact('frozen'));
     }
	/*-------------------------------------------------------*
     * Otener los posts más recientes
     * @return Collection
     *-------------------------------------------------------*/

	public function latest()
	{
		$latest_posts = Post::take(10)->orderBy('published', 'DESC')->get();
		return view('posts.latest', compact('latest_posts'));
	}



	/*-------------------------------------------------------*
     * Obtener las métricas de un posts en el día
     * Se llama por JS
     * @return array
     *-------------------------------------------------------

	public function metricsByDay(Post $post, $filter)
	{
		$orderbydate = DB::table('metrics as w')
		->where('w.post_id', $post->id)
		->select(array(DB::Raw('max(w.'.$filter.') as reactions'),
			DB::Raw('DATE(w.created_at) day')))
		->groupBy('day')
		->orderBy('w.reactions')
		->get();
		$json = array();
		$dates = array();
		$metrica = array();
		foreach ($orderbydate as $key => $date) {
			array_push($dates, $date->day);
			array_push($metrica, $date->reactions);
		}
		$filter = [ucfirst($filter)];
		array_push($json, $dates, $filter, $metrica);

		return response()->json($json);
	}
	*/

	public function getAllMetricsByDay(Post $post)
	{

       //$views_date = date_create_from_format('Y-m-d' , '2017-11-01');
       // $views_date = new DateTime('2017-11-01');
		$orderbydate = DB::table('metrics as w')
		->where('w.post_id', $post->id)
		->select(array(DB::Raw('max(w.reactions) as reactions, max(w.comments) as comments, max(w.shares) as shares, max(w.views) as views'),
			DB::Raw('DATE(w.created_at) day')))
		->groupBy('day')
		->get();


		$values = array();
		$val=false;
		foreach ($orderbydate as $key => $date) {
			$d = array('date' => "$date->day", 'reactions' => "$date->reactions",'comments' => "$date->comments",'shares' => "$date->shares" , 'views' => "$date->views");
			array_push($values, $d);
			$val=true;
		}
		if(!$val){
			$d = array('date' => date("Y/m/d") ,'reactions' => "0",'comments' => "0",'shares' => "0", 'views' => "0");
			array_push($values, $d);
		}
		return response()->json($values);
	}



	/*-------------------------------------------------------*
     * Obtener las métricas de un posts por hora
     * Se llama por JS
     * @return array
     *-------------------------------------------------------

	public function metricsByHour(Post $post, $filter)
	{	
		$metrics = $post->metrics_per_day;

		$json = array();
		$dates = array();
		$metrica = array();

		foreach ($metrics as $metric) {
			array_push($dates, $metric->created_at->format('Y/m/d H:i:s'));
			array_push($metrica, $metric->$filter);
		}

		$filter = [ucfirst($filter)];
		array_push($json, $dates, $filter, $metrica);

		return response()->json($json);
	}
	*/

	public function getAllMetricsByHour(Post $post){
		$metrics = $post->metrics_per_day;
		$json = array(); 
		$val=false;
		foreach ($metrics as $metric) {
			$d = array('date' => "$metric->created_at" ,'reactions' => "$metric->reactions", 'comments' => "$metric->comments", 'shares' => "$metric->shares", 'views' => "$metric->views");
			array_push($json, $d);
			$val=true;
		}
		if(!$val){
			$d = array('date' => date("Y/m/d H:i:s") ,'reactions' => "$0", 'comments' => "0", 'shares' => "0");
			array_push($json, $d);
		}
		return response()->json($json);
	}

	 /*-------------------------------------------------------*
     * Añadir la metrica de videos para los posts que la requieran
     *-------------------------------------------------------*/

	 public function getVideoMetrics($scrap_type=0){
	 	$posts = Post::where('type', 'video')->where('scrap_state', '>=' , 0)->get();
      	//  $posts = Post::where('type', 'video')->where('scrap_type', $scrap_type)->get();

	 	$container_bundle = [];
	 	$index=0;
	 	$container_bundle[$index] = [];
	 	foreach($posts as $post){
	 		$metrics = DB::table("metrics as w")->where('w.post_id', $post->id)->latest()->first();
	 		if($metrics->views == 0){
	 			if(count($container_bundle[$index])>100){
	 				$index++; 
	 			}
	 			if(!array_key_exists($index, $container_bundle)){
	 				$container_bundle[$index] = [];
	 			}
	 			$info_bundle = [
	 				'post_id' => $post->id,
	 				'facebook_id' => $post->facebook_id,
	 				'metrics_id' => $metrics->id,
	 				'scrap_state' => $post->scrap_state
	 			];
	 			array_push($container_bundle[$index], $info_bundle);  
	 		}	
	 	}
	 	$url = 'fbscrapper.test:8888/views-scrapper';
		// $url = 'http://owak.co/scraper/public/index.php/views-scrapper';
	 	$client = new Client();
	 	$return ='';
	 	foreach ($container_bundle as $bundle) {
	 		$final = json_encode($bundle);
	 		$response = $client->post($url, [
	 			'json' => $final
	 		]); 
	 		$return = $response->getBody()->getContents(); 
	 	}
	 	return count($container_bundle[$i]);
	 	
	 }


	/*-------------------------------------------------------*
     * Exportar todas las métricas de un post y el estado del los scraps
     * @return xls file
     *-------------------------------------------------------*/
	public function getData(){       
		$url = 'fbscrapper.test:8888/get-metrics';

		$client = new Client();
		$return =[];
		$response = $client->get($url); 
		$returns = array_merge($return, json_decode($response->getBody()->getContents()));
		foreach ($returns as $return) {
			$newMetrics = Metric::where('id', $return->metrics_id)->first();
			$metrics = DB::table("metrics")->where('post_id', $return->post_id)->where('views', 0)->whereDate('created_at', '<', $newMetrics->created_at)->update(['views' => $return->views]);
			$modify_state = Post::where('id', $return->post_id)->update(['scrap_state' => 10]);
		}
		return count($returns);
	}


	/*-------------------------------------------------------*
     * Recupera los datos de un post que la plataforma ha calificado como inescrapeable
     * @return xls file
     *-------------------------------------------------------*/
	public function freezeData(){
		$url = 'fbscrapper.test:8888/get-frozen';
		$client = new Client();
		$return =[];
		$response = $client->get($url); 
		$returns = array_merge($return, json_decode($response->getBody()->getContents()));
		foreach ($returns as $return) {
			$freezePost = POST::where('id', $return->post_id)->first();
			$frozenPost = DB::table("posts")->where('id', $return->post_id)->update(['scrap_state' => $return->scrap_state]);
		}
	}

	/*-------------------------------------------------------*
     * Exportar todas las métricas de un post
     * @return xls file
     *-------------------------------------------------------*/

	public function export(Post $post)
	{
		$metrics = $post->metrics->toArray();

		$without = ['id', 'post_id', 'updated_at'];

		foreach($metrics as $key => &$metric) {
			foreach($metric as $k => &$v){
				if(in_array($k, $without))
					unset($metrics[$key][$k]);
			}
			$data[] = $metrics[$key];
		}

		$metrics = $data;

		$array = array();
		$post_detail = [
			'Post ID' => $post->facebook_id,
			'Permalink' => $post->permalink_url,
			'Content' => $post->content,
			'Published' => $post->published,
			'Type' => $post->type
		];

		array_push($array, $post_detail);
		$post_detail = $array;

		return Excel::create('post-'.$post->facebook_id, function($excel) use ($metrics, $post_detail){

			$excel->sheet('Post', function($sheet) use ($post_detail){

				$sheet->setWidth(array(
					'A'     =>  16,
					'B'     =>  30,
					'C'     =>  70,
					'D'     =>  25
				));

				$sheet->setHeight(1, 25);


				$sheet->cells('A1:K1', function($data) {
					$data->setBackground('#EFF0F1');
					$data->setFontSize(13);
					$data->setFontWeight();
				});

				$sheet->getStyle('A2:E2')->getAlignment()->setWrapText(true)->applyFromArray(array('horizontal' => 'center', 'vertical' => 'center'));

				$sheet->fromArray($post_detail);

			});

			$excel->sheet('Metrics', function($sheet) use ($metrics){

				$sheet->setHeight(1, 25);

				$sheet->cells('A1:D1', function($data) {
					$data->setBackground('#EFF0F1');
					$data->setFontSize(13);
					$data->setFontWeight();
				});

				$sheet->fromArray($metrics, -1);

			});


		})->download('xls');
	}


}
