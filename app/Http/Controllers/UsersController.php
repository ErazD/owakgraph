<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\GroupController;
use App\User;
use App\Token;
use Session;
use Redirect;
use Auth;

class UsersController extends Controller
{
	public function login(\SammyK\LaravelFacebookSdk\LaravelFacebookSdk $fb, User $user)
	{
		// Obtain an access token.
        try {
            $token = $fb->getAccessTokenFromRedirect();
        } catch (Facebook\Exceptions\FacebookSDKException $e) {
            dd($e->getMessage());
        }

    // Access token will be null if the user denied the request
    // or if someone just hit this URL outside of the OAuth flow.
        if (! $token) {
        // Get the redirect helper
            $helper = $fb->getRedirectLoginHelper();

            if (! $helper->getError()) {
                abort(403, 'Unauthorized action.');
            }

        // User denied the request
            dd(
                $helper->getError(),
                $helper->getErrorCode(),
                $helper->getErrorReason(),
                $helper->getErrorDescription()
                );
        }

        if (! $token->isLongLived()) {
        // OAuth 2.0 client handler
            $oauth_client = $fb->getOAuth2Client();

        // Extend the access token.
            try {
                $token = $oauth_client->getLongLivedAccessToken($token);
            } catch (Facebook\Exceptions\FacebookSDKException $e) {
                dd($e->getMessage());
            }
        }

        $fb->setDefaultAccessToken($token);

    // Save for later
        Session::put('fb_user_access_token', (string) $token);


        Token::updateOrCreate(
            ['id' => 1],
            ['token' => (string) $token]
            );


    // Get basic info on the user from Facebook.
        try {
            $response = $fb->get('/me?fields=id,name,email');
        } catch (Facebook\Exceptions\FacebookSDKException $e) {
            dd($e->getMessage());
        }

    // Convert the response to a `Facebook/GraphNodes/GraphUser` collection
        $facebookUserData = $response->getGraphUser();



        // dd($facebookUserData);
        // $user = $user->getUser($facebookUserData['email']);

       $user = (!empty($facebookUserData['email']))? $user->getUser($facebookUserData['email']):$user->getUser($facebookUserData['id']);

        if(!is_null($user))
        {
         auth()->login($user);
     }else{
         $new_user = new User;
         $new_user = $new_user->createUser($facebookUserData);
         app('App\Http\Controllers\GroupController')->addElement("user,0", 11, $new_user->id, "agregar", true);
         auth()->login($new_user);
     }

     return redirect('/');

 }
}

