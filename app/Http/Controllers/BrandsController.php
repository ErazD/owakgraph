<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\GroupIndustryRole;
use App\User;
use App\Brand;
use App\Post;
use App\YoutubeVideo;
use App\YoutubeChannel;
use App\YoutubeMetrics;
use App\YoutubeScrap;
use App\Color;
use App\Segmento;
use App\Fanpage;
use App\Metric;
use App\Scrap;
use App\Product;
use App\Contest;
use DB;

class BrandsController extends Controller
{

    public function __construct(){
        $this->middleware('industry_role:Brand,brand', ['only' => ['show']]);
    }

    /*-------------------------------------------------------*
     * Homepage. Muestra todas las marcas añadidas
     * @return collection
     *-------------------------------------------------------*/
    public function index()
    {   
        // $brands = Brand::all();
        $user = User::where('id', 11)->first();
        $brands = GroupIndustryRole::getUserPermissionsElement($user, 'Brand')->orderBy('name')->get();
        // $brands->load('fanpages');
        return view('brands.brands', compact('brands'));
    }

    /*-------------------------------------------------------*
     * Brand. Muestra la información de u3na Marca
     * @return collection
     *-------------------------------------------------------*/

    public function show(Brand $brand)
    {
        $segments = array();
        // foreach ($brand->canastas as $canasta) {
        //     foreach ($canasta->segments as $s) {
        //         $segments[$s->id] = $s->name;
        //     }
        // }

        $segmentos = Segmento::all();
        $color= DB::table('colors')->where('element_id', $brand->id)->where('table', 'brands')->first();

        foreach ( $segmentos as $s) {
           $segments[$s->id] = [
            'id' => $s->id,
            'segmento' => $s->name
           ];
       }
       //asort($segments);
       $products = Product::where('brand_id',$brand->id)->orderBy('name')->get();
       
       return view('brands.brand', compact('brand', 'segments', 'products', 'color'));
   }

    /*-------------------------------------------------------*
     * Cambia la frecuencia de scrap base para los post de una marca
     * @param {string} $model: Nombre de la clase del modelo
     * @param {int} $element_id: id del elemento a modificar
     * @param {int} $scrap_type: indicador para filtrar los post que se actualian según el varlo dado. 0:hora, 1:diario, 2:semanal, 3:mensual(publicaciones de más de un año), 4:anual, -1:no se actualiza
     *-------------------------------------------------------*/
    public function setScrapType($model, $element_id, $scrap_type){
        if($scrap_type == 'null') $scrap_type = null;
        $scrap_type_nullable = ['Fanpage', 'YoutubeChannel']; // lista de modelos que pueden recibir null como scrap_type
        $element = ('App\\'.$model)::where('id', $element_id)->first();
        // si $scrap_type no es nulo o el elemento puede recibir un valor nulo, se actualiza su valor.
        if ($scrap_type != null || in_array($model, $scrap_type_nullable))
            $element->update([
                'scrap_type' => $scrap_type
            ]);
        // Se actualiza el valor de scrap_type para los posts
        switch ($model) {
            case 'Brand':
                if($scrap_type != null)
                    $this->setBrandScraptTypePosts($element, $scrap_type); 
                break;
            case 'Fanpage':
            case 'YoutubeChannel':
                if($scrap_type != null)
                    $element->posts()
                        ->whereNull('selected_scrap_type')
                        ->where('scrap_type', '>=', -1)
                        ->update([
                            'scrap_type' => $scrap_type
                        ]);
                break;
            case 'Post':
            case 'YoutubeVideo':
                $element->update([
                    'selected_scrap_type' => $scrap_type
                ]);
                break;
            default:
            break;
        }
        return redirect()->back()->withSuccess('Se ha Actualizado la frecuencia de scrap.');
    }   

    /**
    * Actualiza el valor de scrap_type para los posts de todas las redes sociales pertenecientes a una marca.
    * @param {App\Brand} $brand: Marca a actualizar.
    * @param {int} $scrap_type: nuevo valor a asignar.
    * Postcondition: Se ha actualizado el valor scrap_type para los posts pertenecientes ala marca a los que no se les haya asignado un valor manual, y no estén bloqueados por no poder recoger valores.
    */
    private function setBrandScraptTypePosts($brand, $scrap_type){
        Post::whereHas('fanpage', function($queryfpg) use ($brand){
            $queryfpg->where('brand_id', $brand->id);
        })
        ->whereNull('selected_scrap_type')
        ->where('scrap_type', '>=', -1)
        ->update([
            'scrap_type' => $scrap_type
        ]);
        YoutubeVideo::whereHas('channel', function($queryfpg) use ($brand){
            $queryfpg->where('brand_id', $brand->id);
        })
        // ->whereNull('selected_scrap_type')
        ->where('scrap_type', '>=', -1)
        ->update([
            'scrap_type' => $scrap_type
        ]);
    }

    /*-------------------------------------------------------*
     * Añadir marca
     *-------------------------------------------------------*/

    public function create(Request $request){
        $name = $request->input('name');

        $brand = Brand::create([
            'name' => $name,
            'slug' => str_slug($name, '-'),
        ]);
        
        app('App\Http\Controllers\CommsController')->addDefaultProducts($brand);
        return redirect()->action('BrandsController@index')->withSuccess('La marca '.$name.' ha sido creada');
    }


     /*-------------------------------------------------------*
     * Buscar Marca.
     * @return collection
     *-------------------------------------------------------*/

     public function search(Request $request)
     {
        $query = $request->input('query');
        $brands = Brand::where('name', 'LIKE', "%$query%")->get();

        return view('brands.brands', compact('brands'));
    }


    /*-------------------------------------------------------*
     * Eliminar Marca. Si se elimina la marca, también la fanpage,
     * sus posts y sus métricas.
     *-------------------------------------------------------*/

    public function delete(Request $request, Brand $brand)
    {   
        $name = $brand->name;
        $fanpages = $brand->fanpages;
        $channels = $brand->channels;
        
        Product::where('brand_id', $brand->id)->delete(); 
        
        if(! is_null($fanpages)){
        foreach ($fanpages as $fanpage) {
            $posts = Post::where('fanpage_id', $fanpage->id)->get();
            foreach ($posts as $key => $post) {
                Metric::where('post_id', $post->id)->delete();
            }
            Post::where('fanpage_id', $fanpage->id)->delete();
            Scrap::where('fanpage_id', $fanpage->id)->delete();
        }
        }
        if(! is_null($channels)){
        foreach ($channels as $channel){
           $videos = YoutubeVideo::where('youtube_channel_id', $channel->id)->get();
           foreach($videos as $key => $video){
              YoutubeMetrics::where('youtube_video_id', $video->id)->delete(); 
           }
           YoutubeVideo::where('youtube_channel_id', $channel->id)->delete();
           YoutubeScrap::where('fanpage_id', $channel->id)->delete();   
        }
        }

        $brand->delete();

        return redirect()->action('BrandsController@index')->withSuccess('La marca '.$name.' ha sido eliminada');

    }
}
