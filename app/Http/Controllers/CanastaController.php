<?php

namespace App\Http\Controllers;

use Auth;
use App\User;
use App\Categoria;
use App\Canasta;
use App\Brand;
use Illuminate\Http\Request;

class CanastaController extends Controller
{
    public function __construct(){
        $this->middleware('industry_role:Canasta,canastum', ['only' => ['show']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return redirect()->action('VerticalController@index');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       $route='canasta.store';

       $titulo='Categoría';
       $parents = Categoria::all();
       $canasta_marca = [];
       $canasta_marca['basket'] = Canasta::all();
       $canasta_marca['brand'] = Brand::all();
       return view('verticals.form', compact( 'route', 'titulo', 'parents', 'canasta_marca'));
   }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name' => 'required|string',
            'parent' => 'required|numeric',
        ]);
        $name = $request->input('name');
        $description = $request->input('description');
        $categoria_id = $request->input('parent');

        $canastum = Canasta::create([
            'name' => $name,
            'slug' => str_slug($name, '-'),
            'categoria_id' => $categoria_id,
            'description' => $description,
        ]);
        return redirect()->back()->withSuccess('La Canasta ' . $name . ' ha sido creada');
        // return redirect()->action('VerticalController@index')->withSuccess('La Canasta '.$name.' ha sido creada');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Canasta  $canastum
     * @return \Illuminate\Http\Response
     */
    public function show(Canasta $canastum)
    {
        $user = Auth::user();
        $user = User::where('id', 11)->first();
        $brands = $canastum->userbrands($user)->get();
        return view('verticals.canasta', compact( 'brands', 'canastum', 'user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Canasta  $canastum
     * @return \Illuminate\Http\Response
     */
    public function edit(Canasta $canastum)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Canasta  $canastum
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Canasta $canastum)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Canasta  $canastum
     * @return \Illuminate\Http\Response
     */
    public function destroy(Canasta $canastum)
    {
        //
    }
    /**
    * Agrega una marca a una canasta.
    * @param {App\Canasta} $canasta: canasta a la que se agregará la marca.
    * @param {App\Brand} $brand: marca a agregar.
    * @return \Illuminate\Http\Response
    */
    public function attachBrandToCanasta(Canasta $canasta, Brand $brand){
        $bra = $canasta->brands()->where('brand_id',$brand->id)->first();
        if(!$bra){
            $canasta->brands()->attach($brand);   
        }
        // dd($canasta->brands()->get());
        return redirect()->action('CanastaController@create')->withSuccess('La marca '.$brand->name.' ha sido asociada con la canasta ' . $canasta->name );
    }
}
