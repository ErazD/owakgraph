<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\YoutubeVideo;
use App\YoutubeMetrics;
use Session;
use Excel;
use DB;

class YoutubeVideoController extends Controller
{
	public function __construct(){
		$this->middleware('industry_role:YoutubeVideo,video', ['only' => ['show']]);
	}

    /*-------------------------------------------------------*
     * YoutubeVideo. Muestra la información de un Video
     * @return Object
     *-------------------------------------------------------*/

    public function show(YoutubeVideo $video){
    	$video->load('metrics');
    	return view('youtube.videos.video', compact('video'));
    }


	 /*-------------------------------------------------------*
     * YoutubeVideo. Añadir post manualmente mediante su ID
     * @return Object
     *-------------------------------------------------------*/

	 public function add(Request $request, YoutubeController $yt, YoutubeVideo $video)
	 {	
	 	$this->validate($request,[
	 		'video_url' => 'required|string',
	 	]);
	 	
	 	$videoId=$request->video_url;
	 	$vId = explode('v=', $videoId);
	 	$saved=null;
	 	$youtube = $yt->getYoutubeService();
	 	$videosResponse = '';

	 	if(strpos($vId[1], '&')){
	 		$vIdB = explode('&', $vId[1]); 
	 		$videosResponse = $youtube->videos->listVideos('snippet, statistics', array(
	 			'id' => $vIdB[0],
	 			'fields' => 'items(id,snippet(description,publishedAt,thumbnails/high/url,title,channelId),statistics(commentCount,dislikeCount,likeCount,viewCount))'
	 		));
	 	}else{
	 		$videosResponse = $youtube->videos->listVideos('snippet, statistics', array(
	 			'id' => $vId[1],
	 			'fields' => 'items(id,snippet(description,publishedAt,thumbnails/high/url,title,channelId),statistics(commentCount,dislikeCount,likeCount,viewCount))'
	 		));
	 	}

	 	foreach ($videosResponse['items'] as $videoResult) {
	 		//dd($videosResponse['items']);
	 		$rep = array(
	 			'youtube_channel_id' => $request->channel_id,
	 			'youtube_id' => $videoResult['id'],
	 			'title' => $videoResult['snippet']['title'],
	 			'content' => $videoResult['snippet']['description'],
	 			'type' => "video",
	 			'permalink_url' => "https://www.youtube.com/watch?v=".$videoResult['id'],
	 			'attachment' => $videoResult['snippet']['thumbnails']['high']['url'],          
	 			'published' => $videoResult['snippet']['publishedAt'],            

	 			'likes' => $videoResult['statistics']['likeCount'],
	 			'dislikes' => $videoResult['statistics']['dislikeCount'],
	 			'comments' => $videoResult['statistics']['commentCount'],
	 			'views' => $videoResult['statistics']['viewCount']

	 		);

	 		$saved = $video->manualAdd($rep, $request->channel_id); 
	 	}

	 	return redirect('youtube-video/'.$saved->id);

	 }



	 /*-------------------------------------------------------*
     * Otener los videos con más interacciones
     * @return Collection
     *-------------------------------------------------------*/

	 public function hot()
	 {

	 	$hot_videos = YoutubeMetric::with('youtube_videos')
	 	->select(array(DB::Raw('count(youtube_metrics.youtube_video_id) as count'),
	 		'youtube_metrics.*'))
	 	->groupBy('youtube_metrics.youtube_video_id')
	 	->orderBy('count', 'desc')
	 	->take(10)
	 	->get();

	 	return view('posts.hot', compact('hot_videos'));

	 }


	/*-------------------------------------------------------*
     * Otener los videos más recientes
     * @return Collection
     *-------------------------------------------------------*/

	public function latest()
	{
		$latest_videos = YoutubeVideo::take(10)->orderBy('published', 'DESC')->get();
		return view('posts.latest', compact('latest_videos'));
	}


	/*-------------------------------------------------------*
     * Obtener las métricas de un video en el día
     * Se llama por JS
     * @return array
     *-------------------------------------------------------*/

	public function getAllMetricsByDay(YoutubeVideo $video){

		$orderbydate = DB::table('youtube_metrics as w')
		->where('w.youtube_video_id', $video->id)
		->select(array(DB::Raw('max(w.likes) as likes, max(w.dislikes) as dislikes, max(w.comments) as comments, max(w.views) as views'),
			DB::Raw('DATE(w.created_at) day')))
		->groupBy('day')
		->orderBy('w.views')
		->get();


		$values = array();
		$val=false;
		foreach ($orderbydate as $key => $date) {
			$d = array('date' => "$date->day", 'likes' => "$date->likes", 'dislikes' => "$date->dislikes", 'comments' => "$date->comments",'views' => "$date->views");
			array_push($values, $d);
			$val=true;
		}
		if(!$val){
			$d = array('date' => date("Y/m/d"),'likes' => "0", 'dislikes' => "0", 'comments' => "0",'views' => "0");
			array_push($values, $d);
		}
		return response()->json($values);
	}



	/*-------------------------------------------------------*
     * Obtener las métricas de un video por hora
     * Se llama por JS
     * @return array
     *-------------------------------------------------------*/
	public function getAllMetricsByHour(YoutubeVideo $video){
		$metrics = $video->metrics_per_day;
		$json = array(); 
		$val=false;
		foreach ($metrics as $metric) {
			$d = array('date' => "$metric->created_at" ,'likes' => "$metric->likes",'dislikes' => "$metric->dislikes", 'comments' => "$metric->comments", 'views' => "$metric->views");
			array_push($json, $d);
			$val=true;
		}
		if(!$val){
			$d = array('date' => date("Y/m/d H:i:s") , 'likes' => "0", 'dislikes' => "0", 'comments' => "0",'views' => "0");
			array_push($json, $d);
		}
		return response()->json($json);
	}




	/*-------------------------------------------------------*
     * Exportar todas las métricas de un video
     * @return xls file
     *-------------------------------------------------------*/

	public function export(YoutubeVideo $video)
	{
		$metrics = $video->metrics->toArray();

		$without = ['id', 'youtube_video_id', 'updated_at'];

		foreach($metrics as $key => &$metric) {
			foreach($metric as $k => &$v){
				if(in_array($k, $without))
					unset($metrics[$key][$k]);
			}
			$data[] = $metrics[$key];
		}

		$metrics = $data;

		$array = array();
		$video_detail = [
			'Video ID' => $video->youtube_id,
			'Permalink' => $video->permalink_url,
			'Title' => $video->title,
			'Content' => $video->content,
			'Published' => $video->published,
			'Type' => $video->type
		];

		array_push($array, $video_detail);
		$video_detail = $array;

		return Excel::create('Video-'.$video->facebook_id, function($excel) use ($metrics, $video_detail){

			$excel->sheet('Video', function($sheet) use ($video_detail){

				$sheet->setWidth(array(
					'A'     =>  16,
					'B'     =>  30,
					'C'     =>  40,
					'D'     =>  70,
					'E'     =>  25
				));

				$sheet->setHeight(1, 25);


				$sheet->cells('A1:F1', function($data) {
					$data->setBackground('#EFF0F1');
					$data->setFontSize(13);
					$data->setFontWeight();
				});

				$sheet->getStyle('A2:F2')->getAlignment()->setWrapText(true)->applyFromArray(array('horizontal' => 'center', 'vertical' => 'center'));

				$sheet->fromArray($video_detail);

			});

			$excel->sheet('Metrics', function($sheet) use ($metrics){

				$sheet->setHeight(1, 25);

				$sheet->cells('A1:E1', function($data) {
					$data->setBackground('#EFF0F1');
					$data->setFontSize(13);
					$data->setFontWeight();
				});

				$sheet->fromArray($metrics, -1);

			});


		})->download('xls');
	}


}
