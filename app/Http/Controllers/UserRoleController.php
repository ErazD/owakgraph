<?php

namespace App\Http\Controllers;

use Config;
use App\UserRole;
use App\User;
use App\Group;
use Illuminate\Http\Request;

class UserRoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {


        $user = User::where('id',11)->first();
        // $user = (Auth::check())? Auth::user(): false; 
        $roles = Config::get('constants.user_roles');
        $isAdmin = UserRole::userHasPermissions($user, 'admin');
        return view('manage_sections.permissions.user-permissions', compact('user', 'roles', 'isAdmin'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'element_model' => 'required',
            'element_id' => 'required',
            'permission' => 'required'
        ]);

        $userRole = UserRole::firstOrCreate([
            'element_model' => $request->input('element_model'),
            'element_id' => $request->input('element_id'),
            'permission' => $request->input('permission'),
        ]);
        $val = $request->input('permission_value');
        if($val)
            $userRole->permission_value = $val;
        $userRole->save();

        return redirect()->back()->withSuccess('El Rol ha sido asignado');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\UserRole  $userRole
     * @return \Illuminate\Http\Response
     */
    public function show(UserRole $userRole)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\UserRole  $userRole
     * @return \Illuminate\Http\Response
     */
    public function edit(UserRole $userRole)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\UserRole  $userRole
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UserRole $userRole)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\UserRole  $userRole
     * @return \Illuminate\Http\Response
     */
    public function destroy(UserRole $userRole)
    {
        if($userRole)
            $userRole->delete();

    }
}
