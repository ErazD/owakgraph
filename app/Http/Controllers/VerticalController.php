<?php

namespace App\Http\Controllers;

use App\GroupIndustryRole;
use App\User;
use App\Vertical;
use App\Brand;
use Illuminate\Http\Request;
use DB;
use Auth;

class VerticalController extends Controller
{
    public function __construct(){
        $this->middleware('industry_role:Vertical,vertical', ['only' => ['show']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $user = User::where('id', 11)->first();
       // $verticals = Vertical::all();
        $verticals = GroupIndustryRole::getUserPermissionsElement($user, 'Vertical')->get();
       // $brands = Brand::all();
        $brands = GroupIndustryRole::getUserPermissionsElement($user, 'Brand')->get();
        return view('verticals.verticals', compact( 'brands', 'verticals', 'user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $route='vertical.store';
        $titulo='Industria';
        return view('verticals.form', compact( 'route', 'titulo'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request,[
            'name' => 'required|string',
        ]);
        $name = $request->input('name');
        $description = $request->input('description');
        $vertical = Vertical::create([
            'name' => $name,
            'slug' => str_slug($name, '-'),
            'description' => $description,

        ]);
        return redirect()->action('VerticalController@index')->withSuccess('La vertical '.$name.' ha sido creada');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Vertical  $vertical
     * @return \Illuminate\Http\Response
     */
    public function show(Vertical $vertical)
    {        
        $user = Auth::user();
        $user = User::where('id', 11)->first();
        $cates=array();
        //$brands = array();
        $brands = null;
        foreach ($vertical->usercategories($user)->get() as  $cate) {
            array_push($cates, $cate);
            foreach ($cate->usercanastas($user)->get() as $canasta) {
                // $brands [$cate->name] = $canasta->brands()->get();  
                if($canasta->userbrands($user)->count() > 0){ 
                    if($brands==null)
                        $brands = $canasta->userbrands($user);
                    else
                        $brands = $brands->union($canasta->userbrands($user));
                }
            }
        }
        if($brands==null)
            $brands=array();
        else
            $brands = $brands->get();
        return view('verticals.vertical', compact( 'brands', 'vertical', 'user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Vertical  $vertical
     * @return \Illuminate\Http\Response
     */
    public function edit(Vertical $vertical)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Vertical  $vertical
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Vertical $vertical)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Vertical  $vertical
     * @return \Illuminate\Http\Response
     */
    public function destroy(Vertical $vertical)
    {
        //
    }
}
