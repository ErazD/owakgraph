<?php

namespace App\Http\Controllers;

use Config;
use App\Group;
use App\User;
use App\UserRole;
use Illuminate\Http\Request;

class GroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($type='all')
    {
        $groups = null;
        if($type == 'all'){
            $groups = Group::all();
        }else{
            $condition = ($type == 'automatic')? 'LIKE' :'NOT LIKE';
            $groups = Group::where('description', $condition, 'Grupo automático %')->get();
        }
        $user = User::where('id',11)->first();
        // $user = (Auth::check())? Auth::user(): false; 
        $roles = Config::get('constants.user_roles');
        $isAdmin = UserRole::userHasPermissions($user, 'admin');
        return view('manage_sections.permissions.group-permissions', compact('user', 'groups', 'roles', 'isAdmin'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $this->validate($request, ['name' => 'required', 'description' => 'required']);
       $g = Group::create([
            'name' => $request->input('name'), 
            'description' => $request->input('description')
        ]);

       $nextUrl = $request->input('nexturl');
       if($nextUrl != ''){
            $nextUrl = str_replace('<id>', $g->id, $nextUrl);
            $nextUrl = str_replace('<lvl>', $request->input('add-element-level'), $nextUrl);

            return redirect($nextUrl);
       }
       return redirect()->back()->withSuccess('Se ha creado el Grupo');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Group  $group
     * @return \Illuminate\Http\Response
     */
    public function show(Group $group)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Group  $group
     * @return \Illuminate\Http\Response
     */
    public function edit(Group $group)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Group  $group
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Group $group)
    {
        //
    }

    /**
     * Elimina el grupo, sus usuarios, sus permisos y sus marcas.
     * Remove the specified resource from storage.
     *
     * @param  \App\Group  $group
     * @return \Illuminate\Http\Response
     */
    public function destroy(Group $group)
    {
        $result = "Se ha eliminado el Grupo \"".$group->name."\".";
        $group->users()->detach();
        $group->industryPremissions()->delete();
        $group->permissions()->delete();
        $group->delete();
        return redirect()->back()->withSuccess($result); 
    }

    /**
    * Agrega o elimina elementos al grupo.
    * @param {string} $type: tipo de elemento a agregar. user, permission, brand, group.
    * @param {int} $group: ID del grupo al que se agregarán los permisos si $type != 'group', ID de marca a agregar en caso contrario.
    * @param {string} $e_id: ID o lista de IDs separadas por comas(','). IDs de elementos a agregar si $type != 'group', IDs de los grupos deonde se agregan en caso contrario.
    * @return {string}: Mensaje del resultado del proceso.
    */
    public function addElement($type, $group, $e_id, $action, $is_new_user = false){
        
        $level = 0;
        if(strpos($type, ',') > 0){
            $type = explode(',', $type);
            $level = $type[1];
            $type = $type[0];
        }
        
 
        if(strpos($e_id, ',') > 0){
            $e_id = explode(',', $e_id);
        }

        $arr_result = -1;
        // dd($type, $group, $e_id, $action);
        // Si tipo es grupo, se invierte el funcionamiento normal, $group será la marca y 
        if(strtolower($type) == 'group'){
            if(!is_array($e_id)){
                $e_id = [$e_id];
            }
            $arr_result = [];
            foreach ($e_id as $key => $gr_id) {
                $gr = Group::where('id', $gr_id)->first();    
                if($gr)
                array_push($arr_result, $gr->addElement('Brand', $group, $action, $level));
            }

        }else{
            $group = Group::where('id', $group)->first();
            $arr_result = $group->addElement($type, $e_id, $action, $level);        
        }
        // busca el mensaje de respuesta perteneciente al resultado obtenido.
       
        if(is_array($arr_result)){
            $completo = true;
            foreach ($arr_result as $k => $v) {
                $completo &= $v >= 0;
            }
            $result = ($completo)? 1: -1;
        }else{
            $result = $arr_result;
        }
        $result = ($result > 0)? "Se ha completado la acción con éxito.": (($result == 0)?  "El elemento ya existe.": "No se pudo realiar la acción.");
         if($is_new_user == false){
              return redirect()->back()->withSuccess($result); 
         }else{
              return 'creado con éxito';
         }
      
    }
    // public function addElementsByBasicLogin($type, $group, $e_id, $action){
           
    //     $level = 0;
    //     if(strpos($type, ',') > 0){
    //         $type = explode(',', $type);
    //         $level = $type[1];
    //         $type = $type[0];
    //     }
        
    //     if(strpos($e_id, ',') > 0){
    //         $e_id = explode(',', $e_id);
    //     }

    //     $arr_result = -1;
    //     // dd($type, $group, $e_id, $action);
    //     // Si tipo es grupo, se invierte el funcionamiento normal, $group será la marca y 
    //     if(strtolower($type) == 'group'){
    //         if(!is_array($e_id)){
    //             $e_id = [$e_id];
    //         }
    //         $arr_result = [];
    //         foreach ($e_id as $key => $gr_id) {
    //             $gr = Group::where('id', $gr_id)->first();    
    //             if($gr)
    //             array_push($arr_result, $gr->addElement('Brand', $group, $action, $level));
    //         }

    //     }else{
    //         $group = Group::where('id', $group)->first();
    //         $arr_result = $group->addElement($type, $e_id, $action, $level);        
    //     }
    //     return 'Usuario creado con éxito';
    // }
}
