<?php

namespace App\Http\Controllers;

use App\User;
use App\AdSet;
use App\GroupIndustryRole;
use Illuminate\Http\Request;

class AdSetController extends Controller
{

    public function __construct(){
        $this->middleware('industry_role:AdSet,adset', ['only' => ['show']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = User::where('id',11)->first();
        // $user = (Auth::check())? Auth::user(): false; 
        $adsets = AdSet::getAdsetsFromUser($user);
        $adsets->load('posts');
        // dd($adsets);
        return view('manage_sections.adset.adsets', compact('adsets'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $issection = true;
        return view('manage_sections.adset.create-adset', compact('issection'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd('holi');
        $this->validate($request, ['user_id' => 'required', 'name' => 'required']);
        // $user = User::where('id', $request->input('user_id'))->first();
        $adset = AdSet::createAdset($request->input('user_id'),$request->input('name'),$request->input('description'));
        return redirect()->back()->withSuccess('Se ha creado el Adset');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AdSet  $adSet
     * @return \Illuminate\Http\Response
     */
    public function show(Adset $adset, $since=null, $until=null)
    {
         $posts = MethodsController::filterByDate($adset->posts(), $since, $until)->paginate(20);

          $since = ($since != null)? $since: "";
          $until = ($until != null)? $until: "";

          // $adset->add('post', 15438);
          // dd($adset->getAdsetsUserPost(11, 81));

        return view('manage_sections.adset.adset', compact('adset', 'posts', 'since','until'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AdSet  $adSet
     * @return \Illuminate\Http\Response
     */
    public function edit(AdSet $adset)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AdSet  $adSet
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AdSet $adset)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AdSet  $adSet
     * @return \Illuminate\Http\Response
     */
    public function destroy(AdSet $adset)
    {
        $result = "Se ha eliminado el AdSet \"".$adset->name."\".";
        GroupIndustryRole::where('element_model', 'AdSet')->where('element_id', $adset->id)->delete();
        $adset->delete();
        return redirect('/adset')->withSuccess($result); 
    }

    /**
    * Agrega un usuario o post a un adset
    * @param {App\AdSet} $adset: adset al que se agregará el elemento.
    * @param {string} $type: nombre del tipo del elemento a agregar.
    * @param {int} $id: id del elemento a agregar.
    * @param {string} $action: acción a ejecutar, ('agregar', 'eliminar', 'cambiar').
    * @return \Illuminate\Http\Response
    */
    public function add(AdSet $adset, $type, $id, $action="agregar"){
        $result = $adset->add($type, $id, $action);
       return redirect()->back()->withSuccess($result); 
    }
}
