<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\YoutubeReport;
use DB;
use Excel;

class YoutubeReportController extends Controller
{
	public function downloadReports($id)
	{
		$reports = YoutubeReport::where('report_id', $id)->get()->toArray();

		return Excel::create('report-'.$reports[0]['report_id'], function($excel) use ($reports){
			
			$excel->sheet('myReport', function($sheet) use ($reports){
				
				$sheet->cells('A1:M1', function ($data){
					$data->setBackground('#000000');
					$data->setFontColor('#FFFFFF');
					$data->setFontSize(14);
					$data->setFontWeight();
				});

				$sheet->fromArray($reports, -1);
			});        
		})->download('xls');
	} 
}
