<?php

namespace App\Http\Controllers;

use App\TwoFactorAuthentication;
use App\TwoFactorProfile;
use App\Post;
use App\User;
use App\Product;
use App\UserRole;
use App\GroupIndustryRole;
use App\Adset;
use App\YoutubeVideo;
use App\InstaPost;
use Illuminate\Http\Request;

use Config;

class TwoFactorAuthenticationController extends Controller
{
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
        $user = User::where('id',11)->first();
        // $user = (Auth::check())? Auth::user(): false;

        $allElements = array(
            'fanpages' => GroupIndustryRole::getUserPermissionsElement($user, 'Fanpage')->get(), 
            'brands' => GroupIndustryRole::getUserPermissionsElement($user, 'Brand')->get(), 
            'baskets' => GroupIndustryRole::getUserPermissionsElement($user, 'Canasta')->get(), 
            'adsets' => AdSet::getAdsetsFromUserId($user->id, 0)->get()
        );

        $twUsers = UserRole::getUserRolesPermissions('two_factor', 'User', 0)->get();
        $twGroups = UserRole::getUserRolesPermissions('two_factor', 'Group', 0)->get();

        $judgeUsers = UserRole::getUserRolesPermissions('two_factor_principal', 'User', 0)->get();
        $judgeGroups = UserRole::getUserRolesPermissions('two_factor_principal', 'Group', 0)->get();

        $activesTW = UserRole::getUsersByPermisson('two_factor', true, 1, '=')->get();
        $activesJudge = UserRole::getUsersByPermisson('two_factor_principal', true, 1, '=')->get();
        
        return view('comms.two-factor.assing-two-factor', compact('user', 'allElements','twUsers', 'twGroups', 'judgeUsers', 'judgeGroups', 'activesTW', 'activesJudge'));
    }

    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create($social_network, $post_id, $principal=false)
    {      
        $post = null;
        $consts = Config::get('constants.social_networks');
        switch (strtolower($social_network)) {
            case $consts['facebook']['slug']:
            $post = Post::where('id', $post_id)->first();
            break;
            case $consts['youtube']['slug']:
            $post = YoutubeVideo::where('id', $post_id)->first();
            break;
            case $consts['instagram']['slug']:
            $post = InstaPost::where('id', $post_id)->first();
            break;
        }
        // $social_network_name = $consts[$social_network]['name'];
        $social_network_name = $social_network;
        if($post){
            if($principal)
                return view('comms.two-factor.container-form', compact('social_network','social_network_name','post'));
            else
                return view('comms.two-factor.register', compact('social_network','social_network_name','post'));
        }else{
            return "No se encuentra un post de ".$social_network." con la id ".$post_id;
        }
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request, CommsController $comms)
    {   
        $resp = "Se han presentado problemas al crear los valores del comms.";  
        $two =  $this->createTwoFactor($request, false); 
        if($two){
            $post = $two->post;
            $twos = $post->twofactors()->where('principal',false)->get();
            $resp = $comms->registerCommsFromTwoFactorListToPost($twos, $post);
        }
        return $resp; 
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function storePrincipal(Request $request, CommsController $comms)
    {    
        $resp = "Se han presentado problemas al crear los valores del comms.";  
        $two = $this->createTwoFactor($request, true); 
        if($two){
         // $update = $request->input('update_post');
         // if($update && $update=="update"){
        $resp = $comms->registerCommsFromTwoFactorToPost($two);
         // }else{
         //     $resp = "Se han creado valores del comms.";
         // }
        }
       //return $resp;
     //return redirect('posts/'.$two->post->id)->with('status', $resp);

      return redirect('comms/discovery/'. $two->social_network . '/all')->with('status', $resp);
 }

    /**
    * Crea perfiles para twoFactor desde una lista que contiene las Ids de los usuarios.
    * @param Array<int> $listUsersID: arreglo que contiene las ids de los usuarios.
    */
    public function createTwoFactorProfilesFromList($listUsersID){
        foreach ($listUsersID as $i => $user_id) {
            $profile = TwoFactorProfile::firstOrCreate(['user_id' => $user_id]);
        }        
    }
    /**
    * Obtiene o crea un TwoFactorAuthentication para que el usuario pueda clasificar el un post.
    * @param App\TwoFactorProfile $profile: perfil del usuario a quién se le asignará el post.
    * @param $post_id: id del post a asignar.
    * @param String $social_network: nombre de la red social a la que será asignado.
    * @param boolean $isPrincipal: identifica si el usuario el calificador o juez para el post.
    * @return App\TwoFactorAuthentication creado.
    */
    public function asingProfileToTwoFactor(TwoFactorProfile $profile, $post_id, $social_network, $isPrincipal = false){
        $two = TwoFactorAuthentication::where('profile_id',$profile->id)->where('post_id',$post_id)->where('social_network',$social_network)->first();
        if($two){
            $two->principal = $isPrincipal;
            $two->save();
        }else{
            $two = TwoFactorAuthentication::create([
                'post_id' => $post_id,
                'social_network' =>  $social_network,
                'profile_id' => $profile->id,
                'principal' => $isPrincipal
            ]);
        }
        return $two;
    }

    /**
    * Asigna un nuevo TwoFactorAuthentication a un post y un usuario.
    * @param  \Illuminate\Http\Request  $request
    * @param {int} $request->input('post_id'): El ID del post a asignar.
    * @param {int} $request->input('user_id'): El ID del usuario a asignar.
    * @param {string} $request->input('social_network'): Nombre de la red social a la que pertenece el post.
    * @param {bool} $isPrincipal: true si el usuario es juez, false en caso contrario.
    * @return {App\TwoFactorAuthentication}: twofactorAuthentication encontrado o creado para que el usuario pueda calificar el post.
    */
    public function createTwoFactor(Request $request, $isPrincipal){
        $this->validate($request,[
            'post_id' => 'required',
            'social_network' => 'required',
            'user_id' => 'required',
        ],[
            'post_id.required' => 'Se debe seleccionar un post',
            'social_network.required' => 'Se debe indicar de a qué red social pertenece el post',
            'user_id.required' => 'Se requiere un usuario valido para registrar los comms',
        ]);
        $post_id = $request->input('post_id');
        $user_id = $request->input('user_id');
        $social_network = $request->input('social_network');
        $profile = TwoFactorProfile::firstOrCreate(['user_id' => $user_id]);
        $two = $this->asingProfileToTwoFactor($profile, $post_id, $social_network, $isPrincipal);
        if($isPrincipal)
            $this->modifyJudgeTwoFactor($request, $two);
        else
            $this->modifyTwoFactor($request, $two);

        return $two;
    }

    /**
    * Actualiza los datos del twoFactor cuando no es juez con la información dada en el request.
    * @param  \Illuminate\Http\Request  $request
    * @param {string} $request->input('type'): Tipo de post.
    * @param {string} $request->input('balance'): Balance E/F del post.
    * @param {string} $request->input('micro_moment'): Micro momento del post.
    * @param {string} $request->input('business_objective'): Objetivo de negocio del post.
    * @param {string} $request->input('comunication_task'): Tarea de comunicación del post.
    * @param {string} $request->input('comunication_theme'): Tema de comunicación del post.
    * @param {int} $request->input('concurso'): ID del concurso del post.
    * @param {int} $request->input('product'): ID del producto del post.
    * @param {int} $request->input('canasta'): ID de la canasta del post.
    * @param {string} $request->input('influencer'): Nombre del influencer del post.
    * @param {string} $request->input('pauta'): Indica si el post tiene o no pausa.
    * @param {App\TwoFactorAuthentication} $two: twofactor a modificar.
    * Postcondition: Se han actualizado los campos del twofactor.
    */
    public function modifyTwoFactor(Request $request, TwoFactorAuthentication $two){
        $type = $request->input('type');
        if($type && $type!=""){
            $two->type=$type;
        }
        $balance = $request->input('balance');
        if($balance && $balance!=""){
            $two->balance=$balance;
        }
        $micro_moment = $request->input('micro_moment');
        if($micro_moment && $micro_moment!=""){
            $two->micro_moment=$micro_moment;
        }
        $business_objective = $request->input('business_objective');
        if($business_objective && $business_objective!=""){
            $two->business_objective=$business_objective;
        }
        $comunication_task = $request->input('comunication_task');
        if($comunication_task && $comunication_task!=""){
            $two->communication_task=$comunication_task;
        }
        $comunication_theme = $request->input('comunication_theme');
        if($comunication_theme && $comunication_theme != ""){
            $two->communication_theme = $comunication_theme;           
            if($comunication_theme == 'concurso' ){
                $contest = $request->input('contest');                
                if($contest && $contest!=""){
                    $two->contest_id = $contest;
                }
            }
        }
        $product = $request->input('product');
        $brand = $request->input('brand_id');
        $catalogs = Product::where('brand_id', $brand)->get();
        foreach($catalogs as $catalog){
           if($catalog->name == $product){
             $two->product_id = $catalog->id;
             break;
           }
        }
        // if($product && $product != ""){
        //     $two->product_id = $product;
        // }
        
        $canasta = $request->input('canasta');
        if($canasta && $canasta != ""){
            $two->canasta_id = $canasta;
        }
        $influencer = $request->input('influencer');
        if($influencer && $influencer!=""){
            $two->influencer=$influencer;
        }
        $pauta = $request->input('pauta');
        if($pauta && $pauta!=""){
            $two->pauta=$pauta;
        }       
        $two->save();
    }

    /**
    * Actualiza los datos del twoFactor cuando es juez con la información dada en el request. y actualiza los puntajes de los otros usuarios que calificaron el post.
    * @param  \Illuminate\Http\Request  $request
    * @param {string} $request->input('type-check'): Tipo de post seleccionado por un usuario y elegido por el juez.
    * @param {string} $request->input('type'): Tipo de post definido por el juez, si no se ha seleccionado respuesta.
    * @param {string} $request->input('type-ef'): Balance E/F del post seleccionado por un usuario y elegido por el juez.
    * @param {string} $request->input('balance'): Balance E/F del post definido por el juez, si no se ha seleccionado respuesta.
    * @param {string} $request->input('type-micro'): Micro momento del post seleccionado por un usuario y elegido por el juez.
    * @param {string} $request->input('micro_moment'): Micro momento del post definido por el juez, si no se ha seleccionado respuesta.
    * @param {string} $request->input('type-business'): Objetivo de negocio del post seleccionado por un usuario y elegido por el juez.
    * @param {string} $request->input('business_objective'): Objetivo de negocio del post definido por el juez, si no se ha seleccionado respuesta.
    * @param {string} $request->input('type-task'): Tarea de comunicación del post seleccionado por un usuario y elegido por el juez.
    * @param {string} $request->input('comunication_task'): Tarea de comunicación del post definido por el juez, si no se ha seleccionado respuesta.
    * @param {string} $request->input('type-theme'): Tema de comunicación del post seleccionado por un usuario y elegido por el juez.
    * @param {string} $request->input('comunication_theme'): Tema de comunicación del post definido por el juez, si no se ha seleccionado respuesta.
    * @param {int} $request->input('type-contest'): ID del concurso del post seleccionado por un usuario y elegido por el juez.
    * @param {int} $request->input('concurso'): ID del concurso del post definido por el juez, si no se ha seleccionado respuesta.
    * @param {int} $request->input('type-product'): ID del producto del post seleccionado por un usuario y elegido por el juez.
    * @param {int} $request->input('product'): ID del producto del post definido por el juez, si no se ha seleccionado respuesta.
    * @param {int} $request->input('type-canasta'): ID de la canasta del post seleccionado por un usuario y elegido por el juez.
    * @param {int} $request->input('canasta'): ID de la canasta del post definido por el juez, si no se ha seleccionado respuesta.
    * @param {string} $request->input('type-influencer'): Nombre del influencer del post seleccionado por un usuario y elegido por el juez.
    * @param {string} $request->input('influencer'): Nombre del influencer del post definido por el juez, si no se ha seleccionado respuesta.
    * @param {string} $request->input('type-pauta'): Indica si el post tiene o no pausa seleccionado por un usuario y elegido por el juez.
    * @param {string} $request->input('pauta'): Indica si el post tiene o no pausa definido por el juez, si no se ha seleccionado respuesta.
    * @param {App\TwoFactorAuthentication} $two: twofactor a modificar.
    * Postcondition: Se han actualizado los campos del twofactor.
    * Postcondition: Se han actualizado los puntajes de los otros usuarion que han calificado el post.
    */
    public function modifyJudgeTwoFactor(Request $request, TwoFactorAuthentication $two){
        //dd($request->input('type-check'));
        
        // dd('informacion que llega: ', $request, $two); 

        $type =$request->input('type-check');
        $type =($type=="otro")? $request->input('type'):$type;
    
        if($type && $type!=""){
            $two->type=$type;
        }
        //dd($type);
        $balance = $request->input('type-ef');
        $balance =($balance=="otro")? $request->input('balance'):$balance;
        if($balance && $balance!=""){
            $two->balance=$balance;
        }
        
        $micro = $request->input('type-micro');
        $micro =($micro=="otro")? $request->input('micro_moment'):$micro;
        if($micro && $micro!=""){
            $two->micro_moment=$micro;
        }

        $business = $request->input('type-business');
        $business =($business=="otro")? $request->input('business_objective'):$business;
        if($business && $business!=""){
            $two->business_objective=$business;
        }

        $comunication_task = $request->input('type-task');
        $comunication_task =($comunication_task=="otro")? $request->input('comunication_task'):$comunication_task;
        if($comunication_task && $comunication_task!=""){
            $two->communication_task=$comunication_task;
        }

        $comunication_theme = $request->input('type-theme');
        $comunication_theme =($comunication_theme=="otro")? $request->input('comunication_theme'):$comunication_theme;
        $contest = "";
        if($comunication_theme && $comunication_theme != ""){
            $two->communication_theme = $comunication_theme;           
            if($comunication_theme == 'concurso' ){
                $contest = $request->input('type-contest');   
                $contest =($contest=="otro")? $request->input('contest'):$contest;             
                if($contest && $contest!=""){
                    $two->contest_id = $contest;
                }
            }
        }

        $product = $request->input('type-product');
        $product =($product=="otro")? $request->input('product'):$product;
        if($product && $product != ""){
            $two->product_id = $product;
        }

        $canasta = $request->input('type-canasta');
        $canasta =($canasta=="otro")? $request->input('canasta'):$canasta;
        if($canasta && $canasta != ""){
            $two->canasta_id = $canasta;
        }

        $influencer = $request->input('type-influencer');
        $influencer =($influencer=="otro")? $request->input('influencer'):$influencer;
        if($influencer){
            $two->influencer=$influencer;
        }
        $pauta = $request->input('type-pauta');
        $pauta =($pauta=="otro")? $request->input('pauta'):$pauta;
        if($pauta && $pauta!=""){
            $two->pauta=$pauta;
        }
        // dd($two);
        $two->save();

        $twos = $two->post->twofactors()->where('principal',false)->get();
        foreach ($twos as $k => $tw) {
            $p = $this->setScoreTwoFactor('type', $type, $tw);
            $this->setScoreTwoFactor('balance', $balance, $tw);
            $this->setScoreTwoFactor('communication_task', $comunication_task, $tw);
            $this->setScoreTwoFactor('communication_theme', $comunication_theme, $tw);
            $this->setScoreTwoFactor('contest_id', $contest, $tw, 'contest');
            $this->setScoreTwoFactor('product_id', $product, $tw, 'product');
            $this->setScoreTwoFactor('canasta_id', $canasta, $tw, 'canasta');
            $this->setScoreTwoFactor('influencer', $influencer, $tw);
            $this->setScoreTwoFactor('pauta', $pauta, $tw);
            $p->save();
        }
    }

/**
* Compara un valor de two factor con un valor elegido en el two factor de un usuario, y si son diferentes resta puntaje al usuario.
* @param {string} $key: nombre del atributo en el TwoFactorAuthentication
* @param {string} $value: valor seleccionado por el juez.
* @param {App\TwoFactorAuthentication} $tw: two factor calificado por un usuario que no es juez.
* @param {string} $p_key: Nombre del atributo para el TwoFactorProfile cuando se llama difrernte en TwoFactorAuthentication (contest, product, canasta).
* @return {App\TwoFactorProfile}: Perfil que realizó la calificación al TwoFactorAuthentication.
*/
private function setScoreTwoFactor($key, $value, $tw, $p_key = null){
    $p_key = ($p_key == null)? 'ratings_'.$key: 'ratings_'.$p_key;
    $profile = $tw->profile;
    if($value && $value != "" && $tw->$key != $value){
        $profile->$p_key -= 1;
    }
    return $profile;
}

    /**
    * Display the specified resource.
    *
    * @param  \App\TwoFactorAuthentication  $twoFactorAuthentication
    * @return \Illuminate\Http\Response
    */
    public function show(TwoFactorAuthentication $twoFactorAuthentication)
    {
    //
    }

    /**
    * Show the form for editing the specified resource.
    *
    * @param  \App\TwoFactorAuthentication  $twoFactorAuthentication
    * @return \Illuminate\Http\Response
    */
    public function edit(TwoFactorAuthentication $twoFactor)
    {
    //
    }

    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  \App\TwoFactorAuthentication  $twoFactorAuthentication
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, TwoFactorAuthentication $twoFactorAuthentication)
    {
        $this->modifyTwoFactor($request, $twoFactorAuthentication);
        return "Se han modificado los valores del comms para revisión.";
    }

    /**
    * Remove the specified resource from storage.
    *
    * @param  \App\TwoFactorAuthentication  $twoFactorAuthentication
    * @return \Illuminate\Http\Response
    */
    public function destroy(TwoFactorAuthentication $twoFactorAuthentication)
    {
    //
    }

    /**
    * Busca los posts que no tengan twofactors asignados y los usuarios que pueden calificar y reparte los posts entre los usuarios disponibles.
    * @param {string} $social_network: Red social de la que se calificarán los posts.
    * @return {string}: mensaje de información del resultado del proceso.
    */
    public function addPostsToTwoFactor($social_network = 'Facebook', $twosProfiles=null, $posts=null){
        // $ids = User::select('id')->where('rank',1)->get();
        // $ids_arr=[];
        // foreach ($ids as $key => $value) {
        //    array_push($ids_arr, $value->id);
        // }
        // $this->createTwoFactorProfilesFromList($ids_arr);
        // return "asignados";

        if(!$twosProfiles){
            $twosProfiles = TwoFactorProfile::where('rank',0)->get();    
        }
        $cant = sizeof($twosProfiles);

        if(!$posts){
            // $posts = Post::doesntHave('twofactors')->orderBy('published', 'desc');
            $posts = Post::doesntHave('twofactors')->orderBy('published', 'desc')->select('posts.*')
            ->whereHas('fanpage', function($query){
                $query->where('name', 'Colombia')
                ->whereIn('fanpages.brand_id', [29, 39, 40, 26, 34, 53, 8, 37]);
            });
            //post incompletos
            $posts = $posts->where(function ($query) {
                $query->orWhereNull('type')->orWhereNull('balance')->orWhereNull('comunication')->orWhereNull('theme')->orWhereNull('product_id')->orWhereNull('micro_moment')->orWhereNull('business_objective');
            });
            //filtro por fecha
            $posts = MethodsController::filterByDate($posts, '2018-01-01','2018-01-31');
            //dd(sizeof($posts->get()));
            $posts = $posts->get();
        }        

        // crea un arrglo para guardar la cantidad de posts asignados
        $profile_cant = [];
        foreach ($twosProfiles as $key => $value) {
            $profile_cant[$value->user->name] = 0;
        }
        // recorre los post y les asigna los usuarios, re ordena los usuarios cada que ha asinado posts a todos los usuarios.
        foreach ($posts as $key => $post) {
         if($key % $cant==0){
            $twosProfiles = $twosProfiles->shuffle();
        }
        $usu_a = $twosProfiles->shift();
        $usu_b = $twosProfiles->shift();
        $two = $this->asingProfileToTwoFactor($usu_a, $post->id, $post->social_network(), false);
        $twosProfiles->push($usu_a);
        $profile_cant[$usu_a->user->name] += 1;
        if($usu_b){
            $two = $this->asingProfileToTwoFactor($usu_b, $post->id, $post->social_network(), false);
            $twosProfiles->push($usu_b);
            $profile_cant[$usu_b->user->name] += 1;
        }
    }

    $resp = sizeof($posts)." posts asignados";
    foreach ($twosProfiles as $key => $value) {
        $n = $value->user->name;
        $resp .= ", ".$n.": ". $profile_cant[$n];
    }
       // $two = $this->asingProfileToTwoFactor($profile, $post_id, $social_network, $isPrincipal);
       //dd($twosProfiles, $posts);
    return $resp;
}
/**
* Busca los posts a los que no se han asignado perfiles de two factor que cumplan con las características dadas, y los asigna a los usuarios dados.
* @param {string} $users: IDs de los usuarios separados por comas (',').
* @param {string} $social_network: Nombre de la red social a la que pertenece.
* @param {string} $type: nombre de la clase del elemento de industria de donde se buscarán los posts.
* @param {string/int} $slug: ID o slug del elemento donde se buscarán los posts.
* @param {string} $country: nombre del país para filtrar los posts.
* @param {string} $since: fecha inicial para filtrar los posts.
* @param {string} $until: fecha final para filtrar los posts.
* @return {array}: Arreglo de respuesta para mostrar el resultado de la operación. ['message' => Mensaje cuando se completa exitosamente, 'error'=> mensaje de error cuando se presenta alguno].
*/
public function assingTwoFactors($users, $social_network, $type, $slug, $country='all', $since='none', $until='none'){
    if(strpos($users, ',') > 0)
        $users = explode(',', $users);
    else
        $users = [$users];
    $this->createTwoFactorProfilesFromList($users);
    $twosProfiles = TwoFactorProfile::whereIn('user_id', $users)->get();
    $posts = MethodsController::getpostList($social_network, $type, $slug, $country, $since, $until, -1);
    $mensaje = $this->addPostsToTwoFactor($social_network, $twosProfiles, $posts);    
    return array(        
        // 'posts' => $posts->toArray(),
        'message' => $mensaje,
    );
}


}
