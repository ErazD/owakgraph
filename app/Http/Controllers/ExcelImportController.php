<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Excel;
use App\Post;
use App\Metric;
use App\Product;
use App\Brand;
use App\Canasta;
use App\Fanpage;
use Config;
use App\ExcelImport;
use App\Contest;
use DateTime;
use DB;



class ExcelImportController extends Controller
{


	function updateExcelImports($estado = 'all'){
		$imports = null;
		$str_estado= 'Todos';

		if(is_numeric($estado)){
			$imports = ExcelImport::where('estado', $estado)->paginate(100);	
			$str_estado= array('Posts no encontrados', 'Datos incompletos', 'Posts con datos básicos', 'Post con información adicional', 'Posts Pauta vs No pauta')[$estado];
		}else{
			$imports = ExcelImport::orderBy('estado')->paginate(100);	
		}	

		foreach ($imports as $key => $import) {
			$this->updatePost($import);
		}
		return view('comms.excel-import',compact('imports', 'estado', 'str_estado'));		
	}

	function ScrapExcel($filename = 'Comms_Abril'){
    	// $currentPath= url('/');
		echo '
		<style type="text/css">
		body{
			background-color:#fcfcfc;
		}
		table, th, td {
			border: 1px solid black;
			border-collapse: collapse;
			padding: 5px;
			text-align:center;
		}
		.tabla{
			overflow-x: scroll;
		}
		th{
			padding: 20px;
			text-align: center;
			background-color: #707070;
			color: white;
			font-family: sans-serif;
			font-weight: 300;
		}
		td.estado{
			color: white;
			font-weigth: bolder;
		}
		td.estado0{
			background-color:#e53935;
		}
		td.estado1{
			background-color:#fb8c00;
		}
		td.estado2{
			background-color:#00897b;
		}
		td.estado3{
			background-color:#00acc1;
		}
		td.estado4{
			background-color:#039be5;
		}
		</style>
		';
		echo ' 
		<table>
		<tr>
		<th>ESTADO</th> <th>FB_id</th><th>Post_id</th>
		<th>Producto</th><th>Type</th> <th>Balance</th><th>Comunication Task</th><th>Comunication Theme</th>
		<th>Nombre Concurso</th><th>Mecánica</th><th>Premio</th><th>Duración</th>
		<th>Influencer</th> <th>Pauta</th> <th>URL</th>
		</tr>
		';

		$result = Excel::filter('chunk')->load('public/uploads/'.$filename.'.xlsx')->chunk(100, function($reader){
			$dataExcel = $reader->toArray();
            //$explodeExcel = explode('/', $dataExcel[0]['url']);
            // var_dump($explodeExcel[6]);
			foreach($dataExcel as $data){
				$getId = explode('/', $data['url']);
				$getCount = count($getId) - 1;
                //dd($getCount);
				$commsId = $getId[$getCount];
				if(strstr($commsId, '?') || $commsId == ''){   
					$commsId = $getId[$getCount - 1];
                  //  echo $commsId;    
				}

				$match = Post::where('facebook_id', 'LIKE', '%'.$commsId)->first();
                   //else{
                // 	$id = $getId[$getCount];
				$import = $this->createImport($match, $data);
				$this->updatePost($import);  
				
				//echo "<tr><td>aaaaaaa</td></tr>";
				echo '
				<tr>
				<td class="estado estado'.$import->estado.'">'.$import->estado.'</td> <td>'.$import->facebook_id.'</td><td>'.$import->post_id.'</td>
				<td>'.(($import->product_id!= NULL)?$import->product->name : "-").'</td><td>'.$import->type.'</td> <td>'.$import->balance.'</td><td>'.$import->communication_task.'</td><td>'.$import->communication_theme.'</td>
				<td>'. (($import->nombre_del_concurso!= NULL)?$import->nombre_del_concurso : "-") .'</td>
				<td>'. (($import->mecanica_del_concurso!= NULL)?$import->mecanica_del_concurso : "-") .'</td>
				<td>'. (($import->premio!= NULL)?$import->premio : "-") .'</td>
				<td>'. (($import->duracion!= NULL)?$import->duracion : "-") .'</td>

				<td>'. (($import->influencer > 0)?$import->nombre_del_influencer : "-") .'</td>
				<td>'.$import->pauta_vs_no_pauta.'</td>   
				<td><a href="'.$import->url.'">url</a></td>
				</tr>
				' ;
                // } 

			}
			//dd($imports_results);
		}); 
		echo '</table>';
		//dd($imports_results, $result);
	//		return view('comms.excel-import',compact('imports_results'));		
	}

	public function products(Post $post){
		return Product::where('brand_id',$post->fanpage->brand->id)->get();
	}

	public function createImport($post, $info){
		$const_type = Config::get('constants.comms.type.facebook');
		$const_comunication_task = Config::get('constants.comms.comunication_task');
		$const_balance = Config::get('constants.comms.balance');
		$const_comunication_theme = Config::get('constants.comms.comunication_theme');

		$const_concurso = [
			"nombre_concurso",
			"mecanica_del_concurso",
			"premio",
			"duracion"
		];

		$const_influencer = [
			"influencer",
			"nombre_influencer",
		];

		$const_pauta_vs_no_pauta = "pauta_vs_no_pauta";
		$const_reproducciones = "reproducciones";

		$type =  $this->match($const_type ,$info);
		$balance = $this->match($const_balance ,$info);
		$comunication_task = $this->match($const_comunication_task ,$info);
		$comunication_theme = $this->match($const_comunication_theme ,$info);
		
		$url = $info["url"];
		if(!$url || $url=="" || $url=="0"){
			return null;	
		}else{
			if(strpos($url, 'business.')){
				$url = str_replace("business.","",$url); 
			}			
		}


		$import = false;
		if($post){
			$import = ExcelImport::where('post_id',$post->id)->where('facebook_id', $post->facebook_id)->first();
		}else{
			$import = ExcelImport::where('url', $url)->first();
		}
		if(!$import){
			$import = ExcelImport::create([
				'type' => $type,
				'balance' => $balance,
				'communication_task' => $comunication_task,
				'communication_theme' => $comunication_theme,
				'url' => $url,
				'estado' => 0,
			]);
		}else{
			$import->type = $type;
			$import->balance = $balance;
			$import->communication_task = $comunication_task;
			$import->communication_theme = $comunication_theme;
				//$import->url = $url;
				//$import->save();
		}
		if($post){
			$product = $this->getPostProductID($post, $info);
			$import->facebook_id = $post->facebook_id;
			$import->post_id = $post->id;
			$import->product_id = $product;
			$import->estado = 1;
		}
		if($comunication_theme == "concurso"){
			$import->nombre_del_concurso = $info["nombre_concurso"];
			$import->mecanica_del_concurso = $info["mecanica_del_concurso"];
			$import->premio = $info["premio"];
			$import->duracion = $info["duracion"];
		}
		$influencer = $info["influencer"] > 0;
		if($influencer){
			$import->influencer =true;
			$import->nombre_del_influencer =$info["nombre_influencer"];
		}

		$import->reproducciones = $info[$const_reproducciones];
		$import->pauta_vs_no_pauta = $info[$const_pauta_vs_no_pauta];
		$import->save();

		return $import;
	}

	public function match($array, $info){
		$final_value = null;
		foreach ($array as $key => $value) {
			if(array_key_exists($key, $info)
				&& $info[$key]>0){
				$final_value = $key;
		}		
	}
	return $final_value;
}

public function getPostProductID($post, $info){
	$final_product = null;
	$products = $this->products($post);
	// echo $post->facebook_id;
	// echo '<br>';
	foreach ($products as $value) {
		
		$p = $value->group_product;
		// echo $p;
		// echo ' ';
		// echo $info[$p];
		// echo '<br>';
		if(array_key_exists($p, $info) && $info[$p]>0){
			$final_product = $value->id;

		}
	}
	return $final_product;
}
public function updatePost($import){
	if($import == null)
		return null;

	$commsPostId = $import->post_id;
	$postExist = $commsPostId != null;

	if(!$postExist){
		$getId = explode('/', $import->url);
		$getCount = count($getId) - 1;
		$commsId = $getId[$getCount];
		if(strstr($commsId, '?') || $commsId == ''){   
			$commsId = $getId[$getCount - 1];
		}
		$match = Post::where('facebook_id', 'LIKE', '%'.$commsId)->first();
		if($match) {
			$postExist = true;
			$commsPostId = $match->id;

			//$product = $this->getPostProductID($match, $info);
			$import->facebook_id = $match->facebook_id;
			$import->post_id = $match->id;
			//$import->product_id = $product;
			$import->estado = 1;
			$import->save();
		}
	}

	if($postExist){
		$post=Post::where('id',$commsPostId)->first();
			// Actualizar la información referente al post
		$actu = true;

		$type = $import->type;
		if($type!=null)
			$post->type = $type;
		$actu &= $type!=null;

		$balance = $import->balance;
		if($balance!=null)
			$post->balance = $balance;
		$actu &= $balance!=null;

		$communication_task = $import->communication_task;
		if($communication_task!=null)
			$post->comunication = $communication_task;
		$actu &= $communication_task!=null;

		$communication_theme = $import->communication_theme;
		if($communication_theme!=null)
			$post->theme = $communication_theme;
		$actu &= $communication_theme!=null;

		$product = $import->product_id;
		if($product!=null)
			$post->product_id = $product;
		$actu &= $product!=null;

		$post->save();
		if($actu){
			$import->estado = 2;
			$import->save();
		}
		$actu = true;
		if($import->communication_theme == "concurso"){
			$title = $import->nombre_del_concurso;
			$brand = $post->fanpage->brand;
			$contest = Contest::where('brand_id', $brand->id)->where('title', $title)->first();

			
			try { 
				if(!$contest && strpos(' ', $import->duracion)){
					$duration = explode(' ', $import->duracion);
					$since = new DateTime($duration[0]);
					$until = new DateTime($duration[1]);
					$contest = Contest::create([
						'title' => $title,
						'content' => $import->mecanica_del_concurso,
						'prize' => $import->premio,
						'since' => $since,
						'until' => $until,
						'brand_id' => $brand->id,
					]);
				}
			} catch (Exception $e) { 
				$actu = false;
			}
			
			if($contest){
				$post->contests()->detach($contest->id);
				$post->contests()->attach($contest);
			}

		}
		// Influencer y reproducciones
		$influencer = $import->nombre_del_influencer;
		if($influencer!=null)
			$post->influencer = $influencer;
		$pauta = ($import->pauta_vs_no_pauta == 'PAUTA');
		$post->pauta = $pauta;
		$post->save();
		if($actu && $import->estado == 2){
			$import->estado = 3;
			$import->save();
		}
		// Fin información del post
	}
}

public function getCategoryObject($type, $id){
	$cate = null;
	switch ($type) {
		case 'fanpages':
		$cate = Fanpage::where('id', $id)->first();
			break;
			case 'brands':
		$cate = Brand::where('id', $id)->first();
			break;
			case 'canastas':
			case 'baskets':
		$cate = Canasta::where('id', $id)->first();
			break;
			case 'categorias':
		$cate = Categorias::where('id', $id)->first();
			break;
			case 'vericals':
		$cate = Vertical::where('id', $id)->first();
			break;
	}
	return $cate;
}

public function exportsPosts($type, $id, $country='all', $since='2018-01-01', $until='2018-01-31'){

$hours= [['0:00:01','12:00:00' ], ['12:00:01', '18:00:00'], ['18:00:01', '0:00:00']];

echo "<table>";
for ($i=1; $i <= 31 ; $i++) { 
	foreach ($hours as $key => $hs) {
		echo "<tr>";
		echo "<td>".$i."/3/2018 ".$hs[0]."</td><td>".(($key<2)?$i:$i+1)."/3/2018 ".$hs[1]."</td>";
		echo "</tr>";
	}
}
echo "</table>";

// dd("");

	$type = (substr($type,-1) == 's')? $type: $type.'s';
	$excInfo = [];
	// dd($type);
	$firstDate = date('Y-m-d H:i:s', (new DateTime($since."00:00:00"))->getTimestamp());
	$lastDate = date('Y-m-d H:i:s', (new DateTime($until."23:59:59"))->getTimestamp());

	// $cate =DB::table($type)->where('id', $id)->first();
	$cate = $this->getCategoryObject($type, $id);
	 // dd( $cate->posts);

	// $info = DB::table('posts')
	$inf = $cate->postsArrayQueries();
	// dd($inf);

foreach ($inf as $k => $info) {
		// dd($info);
	// $info = DB::table('posts')
	// ->leftJoin('metrics','posts.id','=','metrics.post_id')	
	$info = $info->leftJoin('fanpages','fanpages.id','=','posts.fanpage_id')
	->leftJoin('brands','brands.id','=','fanpages.brand_id')
	// ->leftJoin('canasta_brand','brands.id','=','canasta_brand.brand_id')
	// ->leftJoin('canastas','canastas.id','=','canasta_brand.canastas_id')
	->leftJoin('products','products.id','=','posts.product_id')
	->leftJoin('contest_post','posts.id','=','contest_post.post_id')
	->leftJoin('contests','contest_post.contest_id','=','contests.id')
	// ->where('metrics.created_at', '<=', $lastDate)
	// ->where($type, $id)
	->select(
		array(
			DB::Raw('posts.id as post_id, posts.published as published, posts.permalink_url as url, brands.name as marca, fanpages.name as pais, posts.content as message, posts.type, posts.comunication as tarea_de_comunicacion, posts.balance, products.name as producto, posts.theme as tema_de_comunicacion, contests.title as nombre_concurso, contests.content as mecanica_del_concurso, contests.prize as premio, contests.since as inicio_concurso, contests.until as fin_concurso, posts.influencer as influencer, posts.pauta as pauta_vs_no_pauta')
		)
	)
	->groupBy('posts.id');

	if($until != null && $since != null){
		$info = $info->whereBetween('posts.published', [$firstDate, $lastDate]);
	}
	$info = $info->get();
 	// dd($info);
	foreach ($info as $k => $i) {
		$r = $c = $s = $v = 0;
		$lm = Metric::where('post_id',$i->post_id)->where('created_at', '<=', $lastDate)->orderBy('created_at', 'desc')->first();
		if($lm){
			$r = $lm->reactions;
			$c = $lm->comments;
			$s = $lm->shares;
			$v = $lm->views;			 
		}
		$i->reacciones = $r;
		$i->comentarios = $c;
		$i->compartidos = $s;
		$i->reproducciones = $v;
		// unset($i['twofactors']);
		$i->twofactors = -1;
}
	$info = $info->toArray();
	$info= json_decode( json_encode($info), true);

	$excInfo = array_merge($excInfo, $info);
	}
	// unset($info['twofactors']);

 // dd($excInfo);
	
	return Excel::create("cooms_" . $type . "_" . $id . "_" . $since . "_" . $until, function($excel) use ($excInfo){

		$excel->sheet('comms', function($sheet) use ($excInfo){
				//dd($excInfo);		
			$sheet->cells('A1:V1', function ($data){
				$data->setBackground('#000000');
				$data->setFontColor('#FFFFFF');
				$data->setFontSize(14);
				$data->setFontWeight();
			});

			$sheet->fromArray($excInfo, -1);
		});        
	})->download('xls');


}




}
