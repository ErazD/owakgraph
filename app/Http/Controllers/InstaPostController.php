<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\InstaPost;
use App\InstaMetrics;
use App\InstaProfile;
use Excel;
use Session;
use GuzzleHttp\Client;
use DateTime;
use Carbon\Carbon as Carbon;
use GuzzleHttp\Psr7\Request as GuzzRequest;
use DB;
use App\Http\Controllers\MethodsController;

class InstaPostController extends Controller
{
    //
	public function show(InstaPost $post){
		$post->load('metrics');
		return view('instagram.posts.post', compact('post'));
	}
	
	/**
     * Manually requests the scrap of requested instagram post to add to the main database
     * @param {Request} $request -> contains profile_id from instagram profile and the request post url
     * @param {Instapost} $pt -> adds the retrieved data from scraper to database
     * @return {redirect} new instagram-post page   
    */
    public function manualAdd(Request $request, InstaPost $pt)
    {

    	$url = 'scraper.test:8888/manual-post';
    	$client = new Client();
    	$return = '';
    	$post_info = [
    	'profile_id' => $request->profile_id,
    	'post_url' => $request->post_url
    	];
    	$post = json_encode($post_info);
    	$response = $client->post($url, [
    		'json' => $post
    		]);
    	$return = $response->getBody()->getContents();
    	$post_info = json_decode($return);
      // dd($post_info);
    	$result = $pt->manualAdd($post_info, $request->profile_id);

    	return redirect('instagram-post/'.$result->id);
    }

	/**
     * Manually requests the scrap of requested instagram post to add to the main database
     * @param {number} $scrap_type -> scrap frequency set for posts
     * @return {number} count($container_bundle) -> number of data bundles (100 posts each) sended to scraper
    */
	 public function sendPostsData($scrap_type = 0){
	 	$posts = InstaPost::where('scrap_state', '>=', 0)->where('scrap_type', $scrap_type)->get();
	 	$container_bundle = [];
	 	$index = 0;
	 	$container_bundle[$index] = [];

	 	foreach($posts as $post){
	 		if(count($container_bundle[$index])>100){
	 			$index++;
	 		}
	 		if(!array_key_exists($index, $container_bundle)){
	 			$container_bundle[$index]= [];
	 		} 
	 		$post_info = [
	 		'post_id' => $post->post_id,
	 		'type' => $post->type,
	 		'scrap_state' => $post->scrap_state
	 		]; 
	 		array_push($container_bundle[$index], $post_info);
	 	}
	 	$url = 'scraper.test:8888/save-posts-info';
	 	$client = new Client();
	 	$return = '';
	 	foreach ($container_bundle as $bundle) {

	 		$final = json_encode($bundle);
	 		$response = $client->post($url, [
	 			'json' => $final
	 			]); 
	 		$return = $response->getBody()->getContents(); 
	 	}
	 	return count($container_bundle);
	 }

   public function sendDatabasePosts(){
     $posts = InstaPost::where('insta_profile_id', '=', 1)->get();
     $container_bundle = [];
     $index = 0;
     $container_bundle[$index] = [];

     foreach($posts as $post){
      if(count($container_bundle[$index])>100){
        $index++;
      }
      if(!array_key_exists($index, $container_bundle)){
        $container_bundle[$index]= [];
      } 
      $post_info = [
      'post_id' => $post->post_id,
      'profile_id' => $post->insta_profile_id, 
      'attachment' => $post->attachment,
      'type' => $post->type,
      'content' => $post->content,
      'published' => $post->published,
      'permalink_url' => $post->permalink_url
      ]; 
      array_push($container_bundle[$index], $post_info);
    }
    $url = 'scraper.test:8888/save-data-posts';
    $client = new Client();
    $return = '';
    foreach ($container_bundle as $bundle) {

      $final = json_encode($bundle);
      $response = $client->post($url, [
        'json' => $final
        ]); 
      $return = $response->getBody()->getContents(); 
    }
    return count($container_bundle);
   }

   public function getInstaDates(){
      $url = 'scraper.test:8888/get-insta-posts';
      $dateUpdate = 0;
      $client = new Client();
      $return =[];
      $response = $client->get($url); 
      $returns = array_merge($return, json_decode($response->getBody()->getContents()));
      foreach ($returns as $return) {
            $refPost = InstaPost::where('post_id', $return->post_id)->first();
            $refPost->published = $return->published;
            $refPost->save();
            $dateUpdate++;
      }
      return array($dateUpdate, count($returns));
     }
 /**
     * Retrieves new metrics from scraper to save on the main database
     * @return {number} count($returns) -> number of new data bundles retrieved from scraper
    */
     public function getInstaMetrics(){
     	$url = 'scraper.test:8888/get-insta-metrics';
     	$client = new Client();
     	$return =[];
     	$response = $client->get($url); 
     	$returns = array_merge($return, json_decode($response->getBody()->getContents()));
     	foreach ($returns as $return) {
            $refPost = InstaPost::where('post_id', $return->post_id)->first();
  
            if($refPost->latestMetric->views != $return->views || $refPost->latestMetric->likes != $return->likes || $refPost->latestMetric->comments != $return->comments){
               
               $metric = new InstaMetrics();
               $metric->addMetric($return);
               $refPost->update(['scrap_state' => 10]);
               $refPost->update(['scrap_type' => 0]);
            
            }else{
                MethodsController::setScrapState($refPost);
            }	
     	}
     	return count($returns);
     }
     
     /**
     * Retrieves new posts from scraper to save on the main database
     * @return {number} count($returns) -> number of new data bundles retrieved from scraper
    */

     public function retrieveInstagramPosts(){
      $url = 'scraper.test:8888/get-insta-posts';
      $client = new Client();
      $return =[];
      $response = $client->get($url); 
      $addedPosts = 0;
      $total = 0;
      $returns = array_merge($return, json_decode($response->getBody()->getContents()));
      foreach ($returns as $return) {
      	   $ip_check = InstaPost::where('post_id', $return->post_id)->first();
      	   if(is_null($ip_check)){
           $ip_new = new InstaPost(); 
           $ip_new->add($return, $return->profile_id);
           $addedPosts++; 
           }
           $total++;
      }
      return [$total, $addedPosts];
  
  }

   
	/*-------------------------------------------------------*
     * Obtener las métricas de un posts en el día
     * Se llama por JS
     * @return array
     *-------------------------------------------------------*/
	
	public function getAllMetricsByDay(InstaPost $post)
	{

    //$views_date = date_create_from_format('Y-m-d' , '2017-11-01');
    // $views_date = new DateTime('2017-11-01');
		$orderbydate = DB::table('insta_metrics as w')
		->where('w.insta_post_id', $post->id)
		->select(array(DB::Raw('max(w.likes) as likes, max(w.views) as views ,max(w.comments) as comments'),
			DB::Raw('DATE(w.created_at) day')))
		->groupBy('day')
		->get();


		$values = array();
		$val=false;
		foreach ($orderbydate as $key => $date) {
			$d = array('date' => "$date->day", 'likes' => "$date->likes",'comments' => "$date->comments", 'views' => "$date->views");
			array_push($values, $d);
			$val=true;
		}
		if(!$val){
			$d = array('date' => date("Y/m/d") ,'likes' => "0",'comments' => "0", 'views' => "0");
			array_push($values, $d);
		}
		return response()->json($values);
	}
	/*-------------------------------------------------------*
     * Obtener las métricas de un posts por hora
     * Se llama por JS
     * @return array
     *-------------------------------------------------------*/
	public function getAllMetricsByHour(InstaPost $post){
		$metrics = $post->metrics_per_day;
		$json = array(); 
		$val=false;
		foreach ($metrics as $metric) {
			$d = array('date' => "$metric->created_at" ,'likes' => "$metric->likes", 'comments' => "$metric->comments", 'views' => "$metric->views");
			array_push($json, $d);
			$val=true;
		}
		if(!$val){
			$d = array('date' => date("Y/m/d H:i:s") ,'likes' => "0", 'comments' => "0", 'views' => "0");
			array_push($json, $d);
		}
		return response()->json($json);
	}



	public function export(InstaPost $post){
		
	}
}
