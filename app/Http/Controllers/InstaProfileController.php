<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\InstaProfile;
use App\InstaMetrics;
use App\InstaPost;
use App\InstaScrap;
use App\Contest;
use App\Product;
use App\FanCount;
use App\Http\Controllers\MethodsController;
use Session;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request as GuzzRequest;
use DB;
use DateTime;
use Carbon\Carbon;

class InstaProfileController extends Controller
{

  public function show(InstaProfile $profile, $since=null, $until=null, MethodsController $checkDate)
  {
 $posts = $checkDate->filterByDate($profile->posts(), $since, $until)->paginate(20);
   $contests = Contest::where('brand_id',$profile->brand->id)->get();
   $products = Product::where('brand_id',$profile->brand->id)->where(
    function($query){
      $query->orwhere('created_at', '>', '2018-02-12')->orwhere('group_product', 'na')->orwhere('group_product', 'todos');
    })->get();
   $since = ($since!=null)?$since:"";
   $until = ($until!=null)?$until:"";
   return view('instagram.profiles.profile', compact('profile', 'posts', 'contests', 'products', 'since','until'));
 }

 public function hot(InstaProfile $profile){
  $hot_posts = InstaMetrics::join('insta_posts', 'insta_metrics.insta_post_id', '=', 'insta_posts.id')
  ->select(array(DB::Raw('COUNT(insta_metrics.insta_post_id) as count'),
    'insta_metrics.*', 'insta_posts.*'))
  ->where('insta_metrics.created_at', '>=', Carbon::today())
  ->where('insta_posts.insta_profile_id', $profile->id)
  ->orderBy('count', 'DESC')
  ->groupBy('insta_posts.id')
  ->paginate(20);
  return view('instagram.profiles.hot-posts', compact('profile','hot_posts'));
}

    //Crear Perfil de Instagram
public function create(Request $request)
{

      //Validar que el request esté completo
  $this->validate($request, [
    'name' => 'required',
    'url' => 'required',
    ]);

            //Verificar si ya hay una fanpage asociada a ese país
  $country = InstaProfile::firstOrNew([
    'brand_id' => $request->input('brand_id'),
    'name' => $request->input('name'),
    ]);

  $profile_url = $request->input('url');
  $profile_id = explode('/', $profile_url);

        //Pero ya existe el registro en OWAK Graph
  if ($country->exists) {
    return redirect('/brand/'.$request->input('brand_id'))->withSuccess($request->input('name').' ya fue creada.');
  }else{
                    //Si no está, se guarda la fanpage en nuestra BD.
    $profile = InstaProfile::create([
      'name' => $request->input('name'),
      'url' => $request->input('url'),
      'brand_id' => $request->input('brand_id'),
      'social_network' => 'Instagram',
      'instagram_id' => $profile_id[3],
      ]);

    return redirect('/brand/'.$profile->brand_id)->withSuccess('El perfil'.$profile->name.' ha sido añadido con éxito');
  }
}

public function sendProfileInfo(){
    
    $profiles = InstaProfile::all();
    $index = 0;
    $profile_bundle = [];
    $profile_bundle[$index] = [];
    $scrapResult = '';

    foreach($profiles as $profile){
       
       if(count($profile_bundle[$index]) > 100){
          $index++;
       }
       if(!array_key_exists($index, $profile_bundle)){
        $profile_bundle[$index]= [];
       } 

       $profile_info = [
          'profile_id' => $profile->id,
          'profile_url' => $profile->instagram_id
       ];  

       array_push($profile_bundle[$index], $profile_info);
    }

    $url = 'scraper.test:8888/save-insta-profile';
    $client = new Client(); 
    $return = '';
    foreach($profile_bundle as $bundle){
       $profile_sent = json_encode($bundle);
         $response = $client->post($url, [
          'json' => $profile_sent
       ]);  
       $return = $response->getBody()->getContents();
       $scrapResult = json_decode($return);
    }
    
    return ' Numero de paquetes enviados: '. count($profile_bundle) . ' Perfiles nuevos agregados: ' . $scrapResult[0] . ' Perfiles agregados anteriormente: ' . $scrapResult[1];   
}

public function getProfileFollowers(){
    
    $url = 'scraper.test:8888/get-profile-followers';
    $followers = 0;
    $client = new Client();
    $return = [];
    $response = $client->get($url); 
    $returns = array_merge($return, json_decode($response->getBody()->getContents()));
     
    foreach ($returns as $return) {
      $followerCount =  new FanCount();
      $followerCount->addInstagramFollowerMetrics($return->metric, $return->profile_id);
      $followers++;
    }
    return $followers;
}

public function getAllSubMetricsByDay(InstaProfile $profile)
  {
    $values = array();
    $val=false;
    
    $orderbydate = DB::table('fan_counts')->where('element_id', $profile->id)->where('table', 'insta_profiles')->get();
    $fechas = array();

    foreach ($orderbydate as $key => $date) {
      array_push($fechas, ['date' => "$date->created_at", 'followers' => "$date->fan_count"]);
      $val=true;
    }
    if($val){
      $d = array('fanpage_id' => $profile->id, 'name'=> 'Followers ' . $profile->brand->name .' '. $profile->name, 'data' => $fechas,  'color' => 'undefined');
      array_push($values, $d);
    }
    if(!$val){
      $null_val = array();
      array_push($null_val, ['date' => date("Y/m/d") ,'followers' => 'undefined']);
      $d = array('fanpage_id' => $profile->id, 'name'=> 'Seguidores ' . $profile->brand->name .' '. $profile->name, 'data' => $null_val);
      array_push($values, $d);
    }
    return response()->json($values);
  }


public function getInstagramPosts(InstaProfile $profile, InstaPost $pt)
{

  $scrap_data = InstaScrap::where('insta_profile_id', $profile->id)->first();

  (!is_null($scrap_data)) ? $last_cron = $scrap_data->updated_at : $last_cron = (new \DateTime(date("Y")."-01-01")); 

  $since = strtotime($last_cron->format('Y-m-d'));
  $until = strtotime(date('Y-m-d H:i:s')) + 23 * 60 * 60;

  $url = 'scraper.test:8888/get-posts-bundle';
  $client = new Client();
  $return = [];
  $profile_used = $profile->id;
  $profile_info = [
  'profile_id' => $profile->id,
  'profile_url' => $profile->instagram_id
  ];
  $profile_send = json_encode($profile_info);
  $response = $client->post($url, [
    'json' => $profile_send 
    ]);
  $return = $response->getBody()->getContents();
  $profile_posts = json_decode($return);
  
  foreach($profile_posts as $profile_post){

     $pt->add($profile_post, $profile_used);

  }

  if(is_null($scrap_data)){
    InstaScrap::create([
      'insta_profile_id' => $profile_used,
      'updated_at' => date('Y-m-d H:i:s'),
      ]);
  }else{
    $scrap_data->update([
      'updated_at' => date('Y-m-d H:i:s')
      ]);
  }
  return redirect('/profile/'.$profile_used);
}


public function search(Request $request, InstaProfile $profile)
{   
  $profile_id = $profile->id;
  $query = $request->input('query');

  $posts = InstaPost::where('insta_profile_id', $profile_id)
  ->where('content', 'LIKE', "%$query%")
  ->orderBy('published', 'DESC')
  ->paginate(20);

  return view('instagram.profile.profiles', compact('profile', 'posts'));
}
}
