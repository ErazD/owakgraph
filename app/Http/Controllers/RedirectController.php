<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \SammyK\LaravelFacebookSdk\LaravelFacebookSdk as Facebook;
// use PagesController;
// use YoutubeController;

class RedirectController extends Controller
{
    /**
     * Redirige a las secciones de Reporte rápido de una fanpage o canal según la url ingresada en el formulario.
     * @param {Illuminate\Http\Request} $request
     * @param {App/Http/Controllers/PagesController} $pg
     * @param {App/Http/Controllers/YoutubeController} $yc
     * @param {SammyK\LaravelFacebookSdk\LaravelFacebookSdk} $fb
     * @return \Illuminate\Http\Response
     */
    public function redirectReport(Request $request, PagesController $pg, YoutubeController $yc, Facebook $fb)
    {
      $this->validate($request, [
        'url' => 'required',
      ]);
      $the_url = $request->input('url');
      if(strpos($the_url, 'facebook.com')){
        return $pg->fanpageReport($request, $fb);
      }else if(strpos($the_url, 'youtube.com')){
        return $yc->YoutubeChannelReport($request);
      }
    }

    /**
     * Redirige a las secciones de Reporte rápido de los comentarios de un post según la url ingresada en el formulario.
     * @param {Illuminate\Http\Request} $request
     * @param {App/Http/Controllers/PagesController} $pg
     * @param {App/Http/Controllers/YoutubeController} $yc
     * @param {SammyK\LaravelFacebookSdk\LaravelFacebookSdk} $fb
     * @return \Illuminate\Http\Response
     */
    public function redirectComments(Request $request, PagesController $pg, YoutubeController $yc, Facebook $fb){
      $this->validate($request,
        ['id' => 'required'],
        ['id.required' => 'El campo YouTube ID no debe estar vacío']);
      $social_network=$request->input('social_network');
      if($social_network == "Facebook"){
        return $pg->postReport($request, $fb);
      }  if($social_network == "YouTube"){
        return $yc->YoutubeCommentsReport($request);
      }

    }

    /**
     * Redirige a las secciones de crear una fanpage o canal segun la url ingresada.
     * @param {Illuminate\Http\Request} $request
     * @param {App/Http/Controllers/PagesController} $pg
     * @param {App/Http/Controllers/YoutubeChannelController} $ycc
     * @param {SammyK\LaravelFacebookSdk\LaravelFacebookSdk} $fb
     * @param {App/Http/Controllers/YoutubeController} $yc
     * @return \Illuminate\Http\Response
     */
    public function redirectCreateFanpage(Request $request, FanpagesController $fc, YoutubeChannelController $ycc, Facebook $fb, YoutubeController $yc, InstaProfileController $ipc){

      $this->validate($request, [
        'url' => 'required',
      ]);
      $the_url = $request->input('url');
      if(strpos($the_url, 'facebook.com')){
        return $fc->create($request, $fb);

      }else if(strpos($the_url, 'youtube.com')){
        return $ycc->create($request, $yc);
      
      }else if(strpos($the_url, 'instagram.com')){
          return $ipc->create($request);
      }
    }    
  }
