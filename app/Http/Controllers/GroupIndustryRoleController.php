<?php

namespace App\Http\Controllers;

use App\GroupIndustryRole;
use Illuminate\Http\Request;

use App\Brand;
use App\User;
use App\UserRole;
use Config;

class GroupIndustryRoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $brands = Brand::all();
        $user = User::where('id',11)->first();
        // $user = (Auth::check())? Auth::user(): false; 
        $roles = Config::get('constants.user_roles');
        $isAdmin = UserRole::userHasPermissions($user, 'admin');
        return view('manage_sections.permissions.industries-permissions', compact('user', 'brands', 'roles', 'isAdmin'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\GroupIndustryRole  $groupIndustryRole
     * @return \Illuminate\Http\Response
     */
    public function show(GroupIndustryRole $groupIndustryRole)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\GroupIndustryRole  $groupIndustryRole
     * @return \Illuminate\Http\Response
     */
    public function edit(GroupIndustryRole $groupIndustryRole)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\GroupIndustryRole  $groupIndustryRole
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, GroupIndustryRole $groupIndustryRole)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\GroupIndustryRole  $groupIndustryRole
     * @return \Illuminate\Http\Response
     */
    public function destroy(GroupIndustryRole $groupIndustryRole)
    {
        //
    }
}
