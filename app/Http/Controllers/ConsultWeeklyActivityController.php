<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Vertical;
use App\Categoria;
use App\Canasta;
use App\Brand;
use App\Fanpage;
use App\Report;
use App\AdSet;
use App\Post;
use App\Metric;
use DateTime;
use DateInterval;
use DatePeriod;

class ConsultWeeklyActivityController extends Controller
{
//
public function index(){
$allElements = array('fanpages'=>Fanpage::all(), 'brands'=>Brand::all(), 'baskets'=>Canasta::all(), 'adsets'=>Adset::all());
return view('comms.consults.comms-weekly-rates', compact('allElements'));
}

public function getWeekDates($date, $date_i, $date_f){
$week =  date('W', strtotime($date));
$year =  date('Y', strtotime($date));
$from = date("Y-m-d", strtotime("{$year}-W{$week}+1")); //Returns the date of monday in week
if($from < $date_i) $from = $date_i;
$to = date("Y-m-d", strtotime("{$year}-W{$week}-7"));   //Returns the date of sunday in week
if($to > $date_f) $to = $date_f;

$week = array(
'start' => $from,
'end' => $to
);

return $week;	 
} 

public function consultMetrics($type, $slug, $country, $since, $until, $interval = '1 day'){
$intervalNumber = 0;
$times = array();
$ranges = array();
$rangeType = explode(" ", $interval);
$timeValues = array();
$i = 0;
$model = $this->getModel($type, $slug);
$start = new DateTime($since);
$end = new DateTime($until);
$intervalDate = "";

switch($interval){
case '1 day':
$intervalDate = new DateInterval('PT1H');
$dateRange = new DatePeriod($start, $intervalDate, $end);
foreach ($dateRange as $date) {
$times[$intervalNumber][] = $date->format('Y-m-d');
if ($date->format('G') == 23) {
$intervalNumber++;
}
}
break;
case '1 week':
$intervalDate = new DateInterval('P1D');
$dateRange = new DatePeriod($start, $intervalDate, $end);
foreach ($dateRange as $date) {
$times[$intervalNumber][] = $date->format('Y-m-d');
if ($date->format('w') == 6) {
$intervalNumber++;
}
}  
break;
case '1 month':
$intervalDate = new DateInterval('P1D');
$dateRange = new DatePeriod($start, $intervalDate, $end);
//$monthDigit = 0;
foreach ($dateRange as $date) {
$times[$intervalNumber][] = $date->format('Y-m-d');
if ($date->format('t') == $date->format('j')) {
$intervalNumber++;
}
}
break;
case '1 year':
$intervalDate = new DateInterval('P1D');
$dateRange = new DatePeriod($start, $intervalDate, $end);
$monthDigit = 0;
foreach ($dateRange as $date) {
$times[$intervalNumber][] = $date->format('Y-m-d');
if ($date->format('t') == $date->format('j')) {
$monthDigit++;
}
if($monthDigit == 12){
$intervalNumber++;
$monthDigit = 0;
}
}
break;
}

$ranges = array_map(function($times){
$start = array_shift($times);
$end = array_pop($times);
$times = array(
'start' => $start,
'end' => $end
);  
return $times; }, $times);

foreach($ranges as $date){
$i++;
$date_initial = '';
$date_final = '';
if($date['end'] != null){
$date_initial = $date['start'];
$date_final = $date['end'];
}
$publications = $this->getCategoryPublications($type, $model, $date_initial, $date_final, $country); 

$info_intervalo = array(
'date_i' => $date['start'],
'date_f' => $date['end'],
'type' => $rangeType[1] . " " . $i,
'subtype' => $type,
$type => $publications,
'metrics' => $publications['metrics']
);
array_push($timeValues, $info_intervalo);
}

$info_consulta = array(
'type' => $rangeType[1].'_group',
'subtype' => $rangeType[1],
'category' => $type,
'intervals' => $timeValues,
'metrics' => $this->getTotalMetrics($timeValues),
);
return $info_consulta;
}

public function getModel($type, $slug){
if(is_numeric($slug))
return ("App\\".$type)::where('id',$slug)->first();
else				
return ("App\\".$type)::where('slug',$slug)->first();
}

protected function getCategoryPublications($type, $model, $date_i, $date_f, $country){
$category = null;

switch($type){
case 'Categoria':
$category = $this->getPublicationsCategory($model, $date_i, $date_f, $country);
break;
case 'Canasta':
$category = $this->getPublicationsCanasta($model, $date_i, $date_f, $country);
break;
case 'Brand':
$category = $this->getPublicationsBrand($model, $date_i, $date_f, $country);

break;
case 'Adset':
$category = $this->getPublicationsAdset($model, $date_i, $date_f, $country);
break;
default:
dd("How'd I get here?");
break;
}
return $category; 
}

public function getPublicationsCategory(Categoria $categoria, $date_i, $date_f, $country){
$publications = array();
foreach($categoria->canastas as $canasta){
array_push($publications, $this->getPublicationsCanasta($canasta, $date_i, $date_f, $country));
}
$info_category = array(
'name' => $categoria->name,
'type' => 'category',
'categoria_id' => $categoria->id,
'baskets' => $publications,
'metrics' => $this->getTotalMetrics($publications, 0)
);
return $info_category;
} 
public function getPublicationsCanasta(Canasta $canasta, $date_i, $date_f, $country){
$publications = array();
foreach($canasta->brands as $brand){
array_push($publications, $this->getPublicationsBrand($brand, $date_i, $date_f, $country));
}
$info_canasta = array(
'name' => $canasta->name,
'type' => 'basket',
'canasta_id' => $canasta->id,
'brands' => $publications,
'metrics' => $this->getTotalMetrics($publications, 0)
);

return $info_canasta;
}
public function getPublicationsBrand(Brand $brand, $date_i, $date_f, $country){
$fb_publications = array();
$publications = array();
if($country != 'all'){
foreach ($brand->fanpages->where('name', '=', $country) as $fpg){
// dd('items recuperados', $fpg_posts->get()); 
// dd($date_i, $date_f, $fpg_posts->get());
$fpg_posts = $fpg->posts()->select('posts.*')->join('metrics', 'posts.id', '=', 'metrics.post_id')->groupBy('posts.id');
//dd($fpg_posts->select('metrics.created_at')->get()->toArray());

$fpg_posts = MethodsController::filterByDate($fpg_posts, $date_i, $date_f, 'metrics.created_at');

$fb_post = $this->getFacebookListPostMetrics($date_i, $date_f, $fpg_posts->get());

$info_fpg = array(
'type' => 'fanpage',
'name' => 'FB ' . $fpg->name .' '.$fpg->brand->name,
'fanpage_id' => $fpg->id,
'facebook_id' => $fpg->facebook_id,
'posts' => $fb_post,
'metrics' => $this->getTotalMetrics($fb_post, 1)
);
array_push($fb_publications, $info_fpg);
}

}else{
//recorre las fanpages y obtiene las metricas de los posts dentro de un rango de tiempo dado 
foreach($brand->fanpages as $fpg){
$fb_post = $this->getFacebookListPostMetrics($date_i, $date_f, $fpg->posts);
$info_fpg = array(
'type' => 'fanpage',
'name' => 'FB ' . $fpg->name .' '. $fpg->brand->name,
'fanpage_id' => $fpg->id,
'facebook_id' => $fpg->facebook_id,
'posts' => $fb_post,
'metrics' => $this->getTotalMetrics($fb_post, 1)
);
array_push($fb_publications, $info_fpg);
}
}
//Devuelve un objeto con los valores generales recopilados de las diferentes redes sociales
$publications = array_merge($publications, $fb_publications);

$info_brand = array(
'name' => $brand->name,
'type' => 'brand',
'brand_id' => $brand->id,
'fanpages'=> $fb_publications,
'metrics' => $this->getTotalMetrics($fb_publications, 0)
);

return $info_brand;
}

public function getFacebookListPostMetrics($date_i, $date_f, $posts){
$list = array();

foreach($posts as $post){

array_push($list, $this->getMetricsFacebookPosts($date_i, $date_f, $post->id, $post->facebook_id));
}
return $list;

}

public function getTotalMetrics($list, $typePost = 0){
$fb_reactions = 0;
$fb_comments = 0;
$fb_shares = 0;
$fb_views = 0;

if($typePost == 0){ //Asignacion general cuando se piden posts de un alto nivel
foreach($list as $ele){
$total = $ele['metrics'];
if(array_key_exists('reactions', $total)){
$fb_reactions =  $fb_reactions + $total['reactions'];
$fb_comments = $fb_comments + $total['comments'];
$fb_shares = $fb_shares + $total['shares'];
$fb_views = $fb_views + $total['views'];
}
}
}else if ($typePost == 1){ //Asignacion metricas facebook
foreach($list as $ele){
$fb_reactions =  $fb_reactions + $ele['reactions'];
$fb_comments = $fb_comments + $ele['comments'];
$fb_shares = $fb_shares + $ele['shares'];
$fb_views = $fb_views + $ele['views'];
}
}
//Asignacion de metricas totales
$total = array();
if($typePost == 0 || $typePost == 1){
$total['reactions'] = $fb_reactions;
$total['comments'] = $fb_comments;
$total['shares'] = $fb_shares;
$total['views'] = $fb_views;
}

return $total;
} 

public function getMetricsFacebookPosts($date_i, $date_f, $id, $fb_id){
$reactions = 0;
$comments = 0;
$shares = 0;
$views = 0;   	 

$begin = DB::table('metrics as w')->where('w.post_id', $id)->whereDate('created_at','<', $date_i)->latest()->first();
$end = DB::table('metrics as w')->where('w.post_id', $id)->whereBetween('created_at', [$date_i, $date_f . ' 23:59:59'])->latest()->first();

$begin = ($begin)?$begin:$end;

if($end){
$reactions = ($end->reactions != $begin->reactions)? $end->reactions - $begin->reactions:$end->reactions;
$comments = ($end->comments != $begin->comments)? $end->comments - $begin->comments:$end->comments;
$shares = ($end->shares != $begin->shares)? $end->shares - $begin->shares:$end->shares;
$views = ($end->views != $begin->views)? $end->views - $begin->views:$end->views;
}
$content = array('post_id' => $id, 'date_i' => $date_i, 'date_f' => $date_f, 'reactions' =>$reactions, 'comments' => $comments, 'shares' => $shares, 'views' => $views, 'id' => $fb_id);

// dd($date_i, $date_f, $begin, $end, $content);

return $content;
}

public function getPublicationsAdsets(AdSet $adset, $date_i, $date_f, $country){



}
}
