<?php

namespace App\Http\Controllers;

use Config;
use App\Metric;
use Illuminate\Http\Request;
use DateTime;
use App\Post;
use App\Fanpage;
use App\YoutubeVideo;
use App\User;

class MethodsController extends Controller
{

    /**
    * Este método recibe un querie para realizar una filtración por rango de tiempo, si no existe rango de tiempo, no realiza fitración.
    * @param querie $list: querie que representa la lista de elementos a filtrar
    * @param string $since: fecha inicial en formato Y-m-d (default=null)
    * @param string $until: fecha final en formato Y-m-d (default=null)
    * @return querie con los elementos luego de filtrar por fecha
    */
    public static function filterByDate($list, $since=null, $until=null, $filter='published'){

        if($until != null && $since != null){
            if($since == $until){
                $list = $list->whereDate($filter, '=', date('Y-m-d', (new DateTime($since))->getTimestamp()));
            }else{
                $firstDate = date('Y-m-d H:i:s', (new DateTime($since."00:00:00"))->getTimestamp());
                $lastDate = date('Y-m-d H:i:s', (new DateTime($until."23:59:00"))->getTimestamp());
                $list = $list->whereBetween($filter, [$firstDate, $lastDate]);
                 
            }
        }
        return $list;
    }

    /*-------------------------------------*
    * Asignar una frecuencia de scrap al post
    *-------------------------------------*/

    public static function setScrapState($post){
        $extinto = 0;
        $anual = 0;
        $mensual = 0;
        $semanal = 0;
        $diario = 0;
        $renovado = 0;

        $antScrap = $post->scrap_type;
        $parentScrap = $post->selectedScrap();
        $lastDate = $post->latestMetric;
        $date = strtotime($lastDate->created_at);

        if($date < strtotime('-1 year')){
            $post->scrap_type = -1;
            $extinto ++;
        }else if($date < strtotime('-6 months')){
            $post->scrap_type = 4;
            $anual ++;
        }else if($date < strtotime('-2 months')){
            $post->scrap_type = 3;
            $mensual ++;
        }else if($date < strtotime('-3 weeks')){
            $post->scrap_type = 2;
            $semanal++;
        }else if($date < strtotime('-2 days')){
            $post->scrap_type = 1;
            $diario++;
        }else{
            $post->scrap_type = 0;
            $renovado++;
        }
        // Compara que el post se encuentre activo, 
        // Compara que el valor inicial del post sea mayor al definido por la marca (para que en un scrap dado específico para el post, no lo afecte)
        // Compara que el nuevo valor del post sea menor al de la marca 
        // Para establecer como límite el valor de la marca
        if($post->scrap_type!= -1 && $parentScrap <= $antScrap && $post->scrap_type < $parentScrap)
            $post->scrap_type = $parentScrap;
        $post->save();

        return "Publicaciones" . ' diarias ' . $diario . ' semanal ' . $semanal . ' mensual ' . $mensual . ' anual ' . $anual . ' renovado ' . $renovado .' extinto  ' . $extinto; 
    }  

    /**
    * Retorna una instancia de una clase para filtrar una lista de posts
    */
    public static function getFilterPostsObject(){
        /** Clase para realizar filtros a listas de posts */
        return new class { 
            const CONJUNCTION = 'where';
            const DISJUNCTION = 'orWhere';
            const SLGUS_BD_POSTS = [
                'type' => 'type',
                'balance' => 'balance',
                'comunication_task' =>'comunication',
                'comunication_theme' => 'theme',
                'google_micro_moment' => 'micro_moment',
                'business-objective' => 'business_objective',
                'product' => 'product_id',
                'canasta' => 'canasta_id',
                'influencer' => 'influencer',
                'pauta' => 'pauta'
            ];
            const CONTEST = 'contest';
            const SEGMENTS = 'segment';

            public $date;
            public $comms;
            /* Operador para el filtro. 'conjunction' or 'disjunction'. */
            public $operator;
            public $idsArrays;


        /**
        * Variable que contiene una función que recibe el query para aplicar filtros adicionales
        * @param {Object} $query: query al actual donde se aplicarán los filtros extras.
        * @return retorna el query luego de haberle aplicado los nuevos filtros.
        */
        public $extraFilter;

    /**
    * Constructor de la clase, inicaliza sus atributos
    */
    public function __construct(){
        $this->date = [
            'since' => null,
            'until' => null,
            'filter' => 'published'
        ];
        $this->comms = false;
        $this->idsArrays = false;
        $this->operator = self::CONJUNCTION;
        $this->extraFilter = null;
    // $comms = {};
    // foreach (Config::get('constants.comms')['comms'] as $k => $c) {
    //      $this->comms->$k = false;
    // }
    }

    /**
    * Obtiene los filtros delcarados en la url y los inicializa.
    * @param {Array<string>} $get: parámetros pasados en url.
    * @param {string} $get[].filtro_comms: cadena de texto con los filtros separados con ';', separa su tipo y valor con ':' y sus valores separados por ','. 'filtro1:val1,val2;filtro2:val1,val2'
    * @param {string} $get[].filtro_comms_operation: indica el tipo de operación. ('conjunction' o 'disjunction')
    * Postondition: se han agregado valores a la variable $comms.
    */
    public function setupFilters($get){
        $allFilter = $get['filtro_comms'];
        $arrFilter = (strpos($allFilter, ';'))? explode(';', $allFilter) : [$allFilter];
        foreach ($arrFilter as $index => $values) {
            $infFilter = explode(':', $values);
            $typeFilter = $infFilter[0];
            $values =  $infFilter[1];
            $values = (strpos($values, ','))? explode(',', $values) : [$values];
            foreach ($values as $k => $value) {
                $this->addComms($typeFilter, $value);
            }
        }
        $isConjunction = !(isset($get['filtro_comms_operation']) && $get['filtro_comms_operation'] == 'disjunction');
        $this->setOperator($isConjunction);
    }

    /**
    * asigna nuevos valores para el filtro de fecha
    * @param {string} $since: fecha inicial del filtro. 'yyyy-mm-dd'.
    * @param {string} $until: fecha final del filtro. 'yyyy-mm-dd'.
    * @param {string} $filter: nombre del valor a filtrar (fecha publicación, de creción o de actualizacion).
    * Postcondition: los valores de fechas se han actualizado.
    */
    public function setDate($since='none', $until='none', $filter='none'){
        if($since!='none')
            $this->date['since'] = $since;
        if($until!='none')
            $this->date['until'] = $until;
        if($filter!='none')
            $this->date['filter'] = $filter;
    }
    /**
    * Define el tipo de operación que se va a utilizar para la consulta (conjunción o disjunción)
    * @param {bool} $isConjunction: indica si la operación es una conjunción.
    * Postcondition: se ha actualizado la operación.
    */
    public function setOperator($isConjunction=false){
        $this->operator = ($isConjunction)? self::CONJUNCTION : self::DISJUNCTION;
    }

    /**
    * Este método agrega un filtro por comms a la lista de filtros.
    * @param {string} $keyComms: slug del tipo de comms por el que se quiere filtrar
    * @param {string} $keyValue: slug del valor del comms por el cual se quiere filtrar
    */
    public function addComms($keyComms, $keyValue){
        $otr = false;
        if(!$this->comms)
            $this->comms = [];
        $comms = $this->comms;
        if(empty($this->comms[$keyComms]))
            $this->comms[$keyComms] =[];
        array_push($this->comms[$keyComms], $keyValue);
    }

    /*Propiedad que aún no está lista para el filtro*/
    public function addIdParent($categorySlug, $idsValue){
        if(!$this->idsArrays)
            $this->idsArrays = [];
        $this->idsArrays->$categorySlug = $idsValue;
    }

    /**
    * Realiza la filtración de la lista usando los filtros definidos.
    * @param {array} $posts: lista a filtrar.
    * @param {string} $table: nombre de la tabla de la publicación (post, youtubeVideo)
    * @return {Object}: Query de las publicaciones luego de ser filtradas.
    */
    public function filterPosts($posts, $table=null){

        // dd('Filtros qu se piden en la fecha', $this->date['since'], $this->date['until'], $this->date['filter']);
        $posts = MethodsController::filterByDate($posts, $this->date['since'], $this->date['until'], $this->date['filter']);
        
        if( $this->extraFilter != null){
            $fil = $this->extraFilter;
            $posts =  $fil($posts);
        }
        if($this->comms){
            $operation = $this->operator;
            $posts = $posts->where(function ($query) use ($operation, $table){
                foreach ($this->comms as $kcomm => $commsArray) {
                    if($kcomm == self::CONTEST){
                        // Si se busca un concurso, se hace una búsqueda de many to many
                        $query->$operation(function ($queryComms) use ($commsArray){
                            $queryComms->whereHas('contests', function($queryContests) use ($commsArray){
                                $queryContests->whereIn('contests.id', $commsArray);
                            });
                        });
                    }else if($kcomm == self::SEGMENTS){
                        // Si se busca un segmento, se buscan los productos que pertenezcan al segmento
                        $query->$operation(function ($queryComms) use ($commsArray){
                            $queryComms->whereHas('product', function($queryContests) use ($commsArray){
                                $queryContests->whereIn('products.segmento_id', $commsArray);
                            });
                        });
                    }else{
                        // si no es un concurso, se busca en un atributo de post
                        $slug = self::SLGUS_BD_POSTS[$kcomm];
                        if($table)
                            $slug = $table.'.'.$slug;
                        $query->$operation(function ($queryComms) use ($slug, $commsArray){
                            $queryComms->whereIn($slug, $commsArray);
                            if(in_array( "NULL", $commsArray) !== False || in_array("null", $commsArray) !== False)
                                $queryComms->orWhere(function ($queryNull) use ($slug){
                                   $queryNull->whereNull($slug);
                                });
                            

                        });
                    }
                }
            });
        }
     
        return $posts;
    }
};
}
    //  fin del método getFilterPostsObject


    /**
    * Actualiza las métricas de una publicación para un rango de tiempo, o crea una nueva metrica en la fecha dada.
    * la información se obtiene a travéz del formulario 'utils.posts.update-metrics'.
    * @param Request $request
    */
    public function setMetricsFromPost(Request $request){
        $requiredValues = [
            'post_id' => 'required',
            'social_network' => 'required',
            'until' => 'required',
        ];
        $requiredMessages = [
            'until.required' => 'Se debe seleccionar la fecha',
        ];
        $type = $request->input('info_metrics');
        if($type != null && $type == 'update'){
            $requiredValues['since'] = 'required'; 
            $requiredMessages['since.required'] = 'Se debe seleccionar la fecha';
        }

        $this->validate($request, $requiredValues, $requiredMessages);
        // dd($request->input('until'), $request->input('since'));
        $post_id = $request->input('post_id');
        $social_network = strtolower($request->input('social_network'));
        $since = $request->input('since');
        $until = $request->input('until');
        $const = Config::get('constants');
        // Infromación con las clases relacionadas a la red social (fanpage, post, metricas, y nombres para id)
        $socialNetworkInfo = $const['social_networks'][$social_network]['models'];
        $metricsName = $const['metrics'][$social_network];
        $metrics = $socialNetworkInfo['metric']::where($socialNetworkInfo['post_id'], $post_id);

        if($since && $since!=""){
        // si existe un rango de fecha, se actualizan las métricas pertenecientes al periodo.
            $updates = array();
            foreach ($metricsName as $k => $mName) {
                $m = $request->input($mName);
                if($m && $m != "")
                    $updates[$mName] = $m;
            }
            if(sizeof($updates) > 0){
                $metrics = MethodsController::filterByDate($metrics, $since, $until, 'created_at');
                $metrics->update($updates);
            }else{
                return redirect()->back()->withError('No se han seleccionado valores para actualizar.');
            }
        }else{
        // Si no existe un rango de tiempo, se crea una métrica nueva, los valores de las métricas que no se hayan especificado (vistas, reacciones, comentarios, ...), se tomarán de la métrica anterior a la fecha.
            $lastMetric = $metrics->where('created_at', '<', $until)->latest()->first();
            $metric = [
                $socialNetworkInfo['post_id'] => $post_id,
                'created_at' => $until." 23:59:59"
            ];
            foreach ($metricsName as $k => $mName) {
                $m = $request->input($mName);
                $metric[$mName] = ($m && $m != "")? $m: (($lastMetric)? $lastMetric->$mName: 0);
            }
            Metric::create($metric);
        }
        return redirect()->back()->withSuccess('Se han modificado las métricas.');
    }

    /**
    * Vista para mostrar una lista de posts.
    * @param {string} $social_network: Nombre de la redSocial de donde provienen los posts.
    * @param {string} $type: Modelo del elemento del que se buscarán sus posts.
    * @param {int/slug} $slug: id o slug del elemento a buscar.
    * @param {string} $country: nombre del pais a filtrar
    * @param {string} $since: fecha inicial a filtrar, si no hay fecha, $since='none'
    * @param {string} $until: fecha final a filtrar, si no hay fecha, $until='none'
    *
    */
    public function postsList($social_network, $type, $slug, $country='all', $since='none', $until='none'){
        $user = User::where('id',11)->first();
        // $user = (Auth::check())? Auth::user(): false;
        $hasTwProfiles = ($_GET && isset($_GET['tw_profiles']))? $_GET['tw_profiles']: 0;
        $posts = $this->getpostList($social_network, $type, $slug, $country, $since, $until, $hasTwProfiles);
     
        $count = $posts->count();
        // dd($posts);
        $posts = $posts->sortByDesc('published');
        // $path = '?';
        // foreach($_GET as $key => $value)
        // {
        //     if($key != 'page'){
        //         if($path != '?')
        //             $path .= '&';
        //         $path .= $key.'='.$value;
        //         }
        // }
        $posts = $this->paginateCollection($posts, ['path'=>'']);
        // $posts = $this->paginateCollection($posts);
        return view('utils.posts.posts-list', compact('posts', 'user', 'count'));
    }

    /**
    * NO ES QUERY
    * POR ACTUALIZAR MÉTODO PARA RECIBIR REDES SOCIALES
    * Obtiene una lista NO ORDENADA de posts pertenecientes a un elemento.
    * @param {string} $social_network: Nombre de la redSocial a consultar.
    * @param {string} $type: Modelo del elemento del que se buscarán sus posts.
    * @param {int/slug} $slug: id o slug del elemento a buscar.
    * @param {string} $country: nombre del pais a filtrar
    * @param {string} $since: fecha inicial a filtrar, si no hay fecha, $since='none'
    * @param {string} $until: fecha final a filtrar, si no hay fecha, $until='none'
    * @param {int} $hasTwProfiles: identifica si se debe filtrar por usuarios asignados al twofactor. 1:usuarios que tienen asignados, 0:no se filtra por asignación de twofactor, -1: usuarios que no tienen asignados.
    * @return {Collection}: lista que contiene los posts filtrados, no es Query.
    */
    public static function getpostList($social_network, $type, $slug, $country='all', $since='none', $until='none', $hasTwProfiles=0){
        // $since = ($since=='none')? null: $since;
        // $until = ($until=='none')? null: $until;
        $filterObj = MethodsController::getFilterPostsObject();
        $filterObj->setDate($since, $until);
        if($_GET && isset($_GET['filtro_comms'])){
            $filterObj->setupFilters($_GET);
        }
        if($hasTwProfiles != 0){
            $filterObj->extraFilter = function($query) use($hasTwProfiles){
                if(!$query){
                    $query = Post::where('id', -1);
                }else{
                    if($hasTwProfiles > 0){
                        $query = $query->has('twofactors');
                    }else if($hasTwProfiles < 0){
                        $query = $query->doesntHave('twofactors');
                    }
                }
                return $query;
            };
        }
         //dd('Valores de categorias', $type, $slug, $country, $social_network);
        // dd('Controlador de metodos', MethodsController::getCategory($type, $slug));
        $posts = MethodsController::getCategory($type, $slug)->snPosts($country, $social_network, $filterObj);
      
        // $posts = MethodsController::filterByDate($posts, $since, $until);
        return $posts;
    }

    /**
    * Retorna la url para acceder la la sección que muestra el elemento seleccionado.
    * @param {Model} $element: elemento del cual se quiere hayar la url.
    * @param {string} $model: nombre de la clase a la cual pertenece $element.
    * @return {string}: url para acceder a la visualización de $element.
    */
    public static function getSectionUrl($element, $model){
        $url = url('/').'/'.MethodsController::urlModelSlug($model).'/'.$element->id;
        return $url;
    }
    /**
    * usado en (getSectionUrl)
    * Retorna en slug la parte de la url perteneciente a un modelo.
    * @param {string} $model: nombre de la clase del modelo
    * @return slug parte de la url
    */
    private static function urlModelSlug($model){
        $slug = $model;
        switch ($model) {
            case 'YoutubeChannel':
            $slug = 'channel';
            break;
            case 'YoutubeVideo':
            $slug = 'youtube-video';
            break;

            default:
            $slug = strtolower($model);
            break;
        }
        return $slug;
    }

    /**
    * Obtiene la paginación para los elementos de una colección.
    * @param {\Illuminate\Support\Collection} $items: lista de elementos a paginar.
    * @param {array} $options: arreglo con la informacín de la paginación.
    * @param {string} $options.path: url de la ruta de la sección a paginar.
    * @param {int} $perpage: cantidad de elementos por página.
    * @param {int} $page: página actual.
    * @return {\Illuminate\Pagination\LengthAwarePaginator}: paginación de la lista.
    */
    public static function paginateCollection($items, $options = [], $perPage = 20, $page = null){
         $path =(isset($options['path']))? $options['path']: '?';
        foreach($_GET as $key => $value)
        {
            if($key != 'page'){
                $path .= (strpos($path, '?') === false )? '?': '&';
                $path .= $key.'='.$value;
            }
        }
        if($path != '?')
            $options['path'] = $path;
        $page = $page ?: (\Illuminate\Pagination\Paginator::resolveCurrentPage() ?: 1);
        $items = $items instanceof \Illuminate\Support\Collection ? $items : \Illuminate\Support\Collection::make($items);
        return new \Illuminate\Pagination\LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
    }

    /**
    * Obtiene una categoría según su tipo y su slug (vertical, categoría, canasta ó marca)
    *
    * @param {string} $type: nombre del Modelo categoría en la que se realizará la búsqueda. (Vertical, Categoría, Canasta ó Brand)
    * @param {string} $slug: slug del elemento perteneciente a la categoría donde se realizará la búsqueda.
    * @return Categoría obtenida
    */
    public static function getCategory($type, $slug){
        $class = "App\\".$type;
        if(!strpos($slug, ',')){
            if(is_numeric($slug))
                return $class::where('id', $slug)->first();
            else
                return $class::where('slug', $slug)->first();
        }else{
            return $class::whereIn('id', explode(',', $slug))->get();
        }
    }
}
