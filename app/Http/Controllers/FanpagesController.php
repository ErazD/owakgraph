<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Fanpage;
use App\Post;
use App\Product;
use App\Scrap;
use App\Contest;
use App\Metric;
use App\Brand;

//Quitar despues
use App\Token;
use App\FanCount;
//

use Session;
use DB;
use DateTime;
use Carbon\Carbon;
use \SammyK\LaravelFacebookSdk\LaravelFacebookSdk as Facebook;
use App\Http\Controllers\MethodsController;

class FanpagesController extends Controller
{

 public function __construct(){
  $this->middleware('industry_role:Fanpage,fanpage', ['only' => ['show']]);
}

    /*-------------------------------------------------------*
     * Fanpage. Muestra todos los posts de esa fanpage
     * @return posts collection
     *-------------------------------------------------------*/
    public function show(Fanpage $fanpage, $since=null, $until=null, MethodsController $checkDate) 
    {
        // $posts = $this->filterByDate($fanpage->posts(), $since, $until)->paginate(20);
      $posts = $checkDate->filterByDate($fanpage->posts(), $since, $until)->paginate(20);
      $contests = Contest::where('brand_id',$fanpage->brand->id)->get();
      $products = Product::where('brand_id',$fanpage->brand->id)->where(
        function($query){
          $query->orwhere('created_at', '>', '2018-02-12')->orwhere('group_product', 'na')->orwhere('group_product', 'todos');
        })->get();
      $since = ($since!=null)?$since:"";
      $until = ($until!=null)?$until:"";
      return view('fanpages.fanpage', compact('fanpage', 'posts', 'contests', 'products', 'since','until'));
    }


    /*-------------------------------------------------------*
     * Fanpage. Muestra todos los posts que han tenido actividad en el día
     * @return posts collection
     *-------------------------------------------------------*/

    public function hot(Fanpage $fanpage){
      $hot_posts = Metric::join('posts', 'metrics.post_id', '=', 'posts.id')
      ->select(array(DB::Raw('COUNT(metrics.post_id) as count'),
        'metrics.*'))
      ->where('metrics.created_at', '>=', Carbon::today())
      ->where('posts.fanpage_id', $fanpage->id)
      ->orderBy('count', 'DESC')
      ->groupBy('posts.id')
      ->paginate(20);
      return view('fanpages.hot-posts', compact('fanpage','hot_posts'));
    }


   /*-------------------------------------------------------*
     * Añadir manualmente los seguidores a una  fanpage
     *-------------------------------------------------------*/
   public function fanCount(Brand $brand, Facebook $fb)
   {  
    $fanpages = Fanpage::where('brand_id', $brand->id)->get();
    // $token = Session::get('fb_user_access_token');
    // $token = env('FACEBOOK_TOKEN');
    $token = Token::first()->token;
    foreach($fanpages  as $fanpage){
      preg_match('#https?\://(?:www\.)?facebook\.com/(\d+|[A-Za-z0-9\.]+)/?#', $fanpage->url, $fanpage_name);
      $fan_count = $fb->get($fanpage_name[1].'/?fields=fan_count', $token);
      $fans = $fan_count->getGraphObject(); 
      FanCount::addFacebookFanMetric($fans, $fanpage->id);
    }
  }
  /*-------------------------------------------------------*
     * Recupera todos los datos de metricas de una fanpage especifica
     *-------------------------------------------------------*/

  public function getAllFanMetricsByDay(Fanpage $fanpage)
  {
   $values = array();
   $val=false;

   $orderbydate = DB::table('fan_counts')->where('element_id', $fanpage->id)->where('table', 'fanpages')->get();
   $fechas = array();

   foreach ($orderbydate as $key => $date) {
    array_push($fechas, ['date' => "$date->created_at", 'followers' => "$date->fan_count"]);
    $val=true;
  }
  if($val){
    $d = array('fanpage_id' => $fanpage->id, 'name'=> 'Seguidores ' . $fanpage->brand->name .' '. $fanpage->name, 'data' => $fechas,  'color' => 'undefined');
    array_push($values, $d);
  }
  if(!$val){
    $null_val = array();
    array_push($null_val, ['date' => date("Y/m/d") ,'followers' => 'undefined']);
    $d = array('fanpage_id' => $fanpage->id, 'name'=> 'Seguidores ' . $fanpage->brand->name .' '. $fanpage->name, 'data' => $null_val);
    array_push($values, $d);
  }
  return response()->json($values);
}

//   public function getAllFanMetricsByDay(Brand $brand)
//   {

//     $fanpages= Fanpage::where('brand_id', $brand->id)->get();
//     $values = array();
//     $val=false;
//     foreach($fanpages as $fanpage){

//      $orderbydate = DB::table('fan_counts')->where('element_id', $fanpage->id)->get();
//      $fechas = array();

//      foreach ($orderbydate as $key => $date) {
//       array_push($fechas, ['date' => "$date->created_at", 'followers' => "$date->fan_count"]);
//       $val=true;
//    }
//    if($val){
//     $d = array('fanpage_id' => $fanpage->id, 'name'=> $fanpage->brand->name .' '. $fanpage->name , 'data' => $fechas,  'color' => 'undefined');
//     array_push($values, $d);
//    }
//     if(!$val){
//       $null_val = array();
//       array_push($null_val, ['date' => date("Y/m/d") ,'followers' => 'undefined']);
//       $d = array('fanpage_id' => $fanpage->id, 'name'=> $fanpage->brand->name .' '. $fanpage->name, 'data' => $null_val);
//       array_push($values, $d);
//     }
//   }
//   return response()->json($values);
// }
    /*-------------------------------------------------------*
     * Buscar en la lista de posts de esa fanpage según su contenido
     * @return posts collection
     *-------------------------------------------------------*/

    public function search(Request $request, Fanpage $fanpage)
    {   
      $fanpage_id = $fanpage->id;
      $query = $request->input('query');

      $posts = Post::where('fanpage_id', $fanpage_id)
      ->where('content', 'LIKE', "%$query%")
      ->orderBy('published', 'DESC')
      ->paginate(20);


      return view('fanpages.fanpage', compact('fanpage', 'posts'));
    }

    /*-------------------------------------------------------*
     * Crear Fanpage
     *-------------------------------------------------------*/

    public function create(Request $request, Facebook $fb)
    {

      $dau = $fb->get('156938611533378?fields=daily_active_users', $token);
      $appUsage = $dau->getHeaders()['x-app-usage'];
      $appUsage = json_decode($appUsage, true);
      if($appUsage['call_count'] >= 95)
        return redirect()->back()->withErrors('Se ha llegado al límite de llamados a facebook.');

            //Validar que el request esté completo
      $this->validate($request, [
        'name' => 'required',
        'url' => 'required',
      ]);

            //Obtener Token para generar petición a FB
      $token = Session::get('fb_user_access_token');
            //Verificar si la fanpage existe
      $exists_on_fb = $fb->get($request->input('url'), $token)->getDecodedBody();

            //Verificar si ya hay una fanpage asociada a ese país
      $country = Fanpage::firstOrNew([
        'brand_id' => $request->input('brand_id'),
        'name' => $request->input('name'),
      ]);

            //Si la fanpage existe en FB
      if(count($exists_on_fb) > 1)
      {
                //Pero ya existe el registro en OWAK Graph
        if ($country->exists) {
          return redirect('/brand/'.$request->input('brand_id'))->withSuccess($request->input('name').' ya fue creada.');
        }else{
                    //Si no está, se guarda la fanpage en nuestra BD.
          $fanpage = Fanpage::create([
            'name' => $request->input('name'),
            'url' => $request->input('url'),
            'brand_id' => $request->input('brand_id'),
            'social_network' => 'Facebook',
            'facebook_id' => $exists_on_fb['id'],
          ]);

          return redirect('/brand/'.$fanpage->brand_id)->withSuccess('La Fanpage creada '.$fanpage->name.' ha sido añadida');
        }

      }else{
                //Si la fanpage no existe o no es accesible por nosotros
        return redirect('/brand/'.$request->input('brand_id'))->withSuccess('Esta fanpage no está disponible en Facebook');
      }

    }
    


    /*-------------------------------------------------------*
     * Obtener las publicaciones de una Fanpage específica
     *-------------------------------------------------------*/

    public function getFacebookPosts(Fanpage $fanpage, Facebook $fb, Post $pt)
    {
      $token = Session::get('fb_user_access_token');

      $dau = $fb->get('156938611533378?fields=daily_active_users', $token);
      $appUsage = $dau->getHeaders()['x-app-usage'];
      $appUsage = json_decode($appUsage, true);
      if($appUsage['call_count'] >= 95)
        return redirect()->back()->withErrors('Se ha llegado al límite de llamados a facebook.');

      $scrap_data = Scrap::where('fanpage_id', $fanpage->id)->first();

      (!is_null($scrap_data)) ? $last_cron = $scrap_data->updated_at : $last_cron = (new \DateTime(date("Y")."-01-01")); 

      $since = strtotime($last_cron->format('Y-m-d'));
      $until = strtotime(date('Y-m-d H:i:s')) + 23 * 60 * 60;

      
      preg_match('#https?\://(?:www\.)?facebook\.com/(\d+|[A-Za-z0-9\.]+)/?#', $fanpage->url, $fanpage_name);

      $posts = $fb->get($fanpage_name[1].'/posts?fields=shares.summary(true),comments.limit(0).summary(true),reactions.limit(0).summary(true),message,type,permalink_url,created_time,attachments&since='.$since.'&until='.$until.'', $token);

      $graphEdge = $posts->getGraphEdge();

      do {
        foreach ($graphEdge as $post) {
         $pt->add($post, $fanpage->id);
       }
     }while($graphEdge = $fb->next($graphEdge));

     if(is_null($scrap_data)){
      Scrap::create([
        'fanpage_id' => $fanpage->id,
        'updated_at' => date('Y-m-d H:i:s'),
      ]);
    }else{
      $scrap_data->update([
        'updated_at' => date('Y-m-d H:i:s')
      ]);
    }

    return redirect('/fanpage/'.$fanpage->id);

  }




    /*-------------------------------------------------------*
     * Obtener las métricas de un post específico
     *-------------------------------------------------------*/

    public function getPostMetrics(Fanpage $fanpage, Metric $metric, Facebook $fb)
    {
      $fanpage_posts = $fanpage->posts;
      $token = Session::get('fb_user_access_token');

      $dau = $fb->get('156938611533378?fields=daily_active_users', $token);
      $appUsage = $dau->getHeaders()['x-app-usage'];
      $appUsage = json_decode($appUsage, true);
      if($appUsage['call_count'] >= 95)
        return redirect()->back()->withErrors('Se ha llegado al límite de llamados a facebook.');

    // $token = env('FACEBOOK_TOKEN');
    // dd($fanpage_posts);

      foreach ($fanpage_posts as $post) {

        $stat = $fb->get($post['facebook_id'].'?fields=shares.summary(true),comments.limit(0).summary(true),reactions.limit(0).summary(true)', $token);
        $stat = $stat->getGraphObject();

        $metric->addMetric($stat, $post['id']);

      }

      return redirect('/fanpage/'.$fanpage->id);

    }



    /*METODO REPETIDO*
     * Este método recibe un querie para realizar una filtración por rango de tiempo, si no existe rango de tiempo, no realiza fitración.
     * @param querie $list: querie que representa la lista de elementos a filtrar
     * @param string $since: fecha inicial en formato Y-m-d (default=null)
     * @param string $until: fecha final en formato Y-m-d (default=null)
     * @return querie con los elementos luego de filtrar por fecha
     */
    // public function filterByDate($list, $since=null, $until=null){
    //     if($until != null && $since != null){
    //         if($since == $until){
    //             $list = $list->whereDate('published', '=', date('Y-m-d', (new DateTime($since))->getTimestamp()));
    //         }else{
    //             $firstDate = date('Y-m-d H:i:s', (new DateTime($since."00:00:00"))->getTimestamp());
    //             $lastDate = date('Y-m-d H:i:s', (new DateTime($until."23:59:00"))->getTimestamp());
    //             $list = $list->whereBetween('published', [$firstDate, $lastDate]);
    //         }
    //     }
    //     return $list;
    // }



  }
