<?php 

namespace App\Http\Controllers;

use App\Http\Controllers\ColorController as Color; 
use Illuminate\Support\Facades\Auth;
use App\User;
use Illuminate\Http\Request;
use App\Post;
use App\Canasta;
use App\Brand;
use App\Fanpage;
use App\YoutubeChannel;
use App\YoutubeVideo;
use App\YoutubeMetrics;
use App\InstaProfile;
use App\InstaPost;
use App\InstaMetrics;
use App\AdSet;
use App\GroupIndustryRole;
use App\Metric;
use App\Product;
use App\Contest;
use Config;
use DateTime;
use DateInterval;
use DatePeriod;
use App\Token;
use DB;


class ConsultController extends Controller
{

	const SOCIAL_NETWORK_PRE = ['Facebook' => 'fb_', 'YouTube' => 'yt_', 'Instagram' => 'insta_'];
    /** Nombre de las interacciones de las diferentes sociales para las que se registran métricas */
    const METRICS_BY_PRE = ['fb_' => ['reactions', 'comments', 'shares', 'views'], 'yt_' => ['views', 'likes', 'dislikes', 'comments'], 'insta_' => ['likes', 'views', 'comments']];


	public function index(){
		$user = User::where('id', 11)->first();
		$allElements = array(
			'fanpages' => GroupIndustryRole::getUserPermissionsElement($user, 'Fanpage')->get(), 
			'channels' => GroupIndustryRole::getUserPermissionsElement($user, 'YoutubeChannel')->get(),
			'profiles' => GroupIndustryRole::getUserPermissionsElement($user, 'InstaProfile')->get(),  
			'brands' => GroupIndustryRole::getUserPermissionsElement($user, 'Brand')->get(), 
			'baskets' => GroupIndustryRole::getUserPermissionsElement($user, 'Canasta')->get(), 
			'adsets' => AdSet::getAdsetsFromUserId($user->id, 0)->get()
		);

		return view('comms.consults.comms-rates',compact('allElements'));
	}

	// public function testing(){
 //       return view('pages.test');
	// }

	public function sharesof(){
		$user = User::where('id', 11)->first();
		$allElements = array(
			'fanpages' => GroupIndustryRole::getUserPermissionsElement($user, 'Fanpage')->get(), 
			'channels' => GroupIndustryRole::getUserPermissionsElement($user, 'YoutubeChannel')->get(),
			'profiles' => GroupIndustryRole::getUserPermissionsElement($user, 'InstaProfile')->get(),  
			'brands' => GroupIndustryRole::getUserPermissionsElement($user, 'Brand')->get(), 
			'baskets' => GroupIndustryRole::getUserPermissionsElement($user, 'Canasta')->get(), 
			'adsets' => AdSet::getAdsetsFromUserId($user->id, 0)->get()
		);
		 
		return view('comms.consults.sharesof',compact('allElements'));
	}

	public function postsRates(){
		$user = User::where('id', 11)->first();
		$allElements = array(
			'fanpages' => GroupIndustryRole::getUserPermissionsElement($user, 'Fanpage')->get(), 
			'channels' => GroupIndustryRole::getUserPermissionsElement($user, 'YoutubeChannel')->get(),
			'profiles' => GroupIndustryRole::getUserPermissionsElement($user, 'InstaProfile')->get(),  
			'brands' => GroupIndustryRole::getUserPermissionsElement($user, 'Brand')->get(), 
			'baskets' => GroupIndustryRole::getUserPermissionsElement($user, 'Canasta')->get(), 
			'adsets' => AdSet::getAdsetsFromUserId($user->id, 0)->get()
		);
		return view('comms.consults.comms-rates-by-post',compact('allElements'));
	}

//2017/10/27 14:54:38
// public function consultPost( $since, $until,$type,$ids){
// 	$firstDate=new DateTime($since); //->format("Y/M/D H:i:s")
// 	$lastDate=new DateTime($until);
// 	$interval = DateInterval::createFromDateString('1 day');
// 	$period = new DatePeriod($firstDate, $interval, $lastDate);
// 	$postValues=array();

// 	foreach ( $period as $dt ){
// 		$consult = DB::table('metrics as w')->where('w.post_id', $ids)->where('created_at', $dt);
// 		$begin = $consult->first();
// 		$end = $consult->latest()->first();

// 		$r = $this->restValues($end->reactions , $begin->reactions);
// 		$c = 0;//$end->comments - $begin->comments;
// 		$s = 0;//$end->shares - $begin->shares;
// 		$d = array('post_id' => $ids, 'date' => $dt->format('Y-m-d'), 'fb_reactions' => $r, 'fb_comments' => $c, 'fb_shares' => $s);
// 		array_push($postValues,$d);
// 	}
// 	//dd($postValues);
// 	return $consult;
// }

// public function restValues($end, $begin){
// 	return $end - $begin;
// }


	/**
	 * Metodo principal
	 *
	 * Este método redirige a los métodos que obtienen los valores del comms panorama según el tipo dado.
	 * Los resultados los filtra por un rango de tiempo, si no existe rango de tiempo, calcula todos los datos.
	 * @param string $type: tipo de elemento en la vertical del que se pediran los valores del comms.
	 * ('basket', 'brand', 'fanpage', 'channel')
	 * @param number $id: id del elemento del que se conseguirán los comms
	 * @param string $country: País por el que se quiere realizar la filtración (default="all")
	 * @param string $since: fecha inicial en formato Y-m-d (default=null)
	 * @param string $until: fecha final en formato Y-m-d (default=null)
	 * @return diccionario con los comms de la categoría dada.
	 */
	public function getPostsCategory($social_network='all', $type, $id, $country="all", $activity = "published" ,$since=null, $until=null){
		// $filterObj = null;
		$filterObj = MethodsController::getFilterPostsObject();
		($activity == 'published')? $filterObj->setDate($since, $until) : $filterObj->setDate($since, $until, 'metrics.created_at');
		// $filterObj->setDate($since, $until, 'metrics.created_at');
		if($_GET && $_GET['filtro_comms'])
			$filterObj->setupFilters($_GET);

		// 	búsqueda de filtros adicionales
		// $filterObj->addComms('type', 'video');
		// $filterObj->addComms('type', 'link');
		// $filterObj->addComms('balance', 'emocional');
		// $filterObj->setOperator(false);

		$error = 'La categoría "'.$type.'" no se encuentra.';
		switch ($type) {
			case 'basket':
			case 'Canasta':
			$basket = Canasta::where('id',$id)->first();
			if($basket)
				return $this->getPostsBasket($basket, $country, $social_network, $activity ,$filterObj);
			else
				$error = 'No se encuentra ninguna canasta con id: '.$id;
			break;
			case 'brand':
			case 'Brand':
			$brand = Brand::where('id',$id)->first();
			if($brand)
				return $this->getPostsBrand($brand, $country, $social_network, $activity ,$filterObj);
			else
				$error = 'No se encuentra ninguna marca con id: '.$id;
			break;

			case 'Page_List':
			case 'page_list':
				return $this->getPostsPageList($id, $filterObj);
			break;

			case 'fanpage':
			case 'Fanpage':
			$fanpage = Fanpage::where('id',$id)->first();
			if($fanpage)
				return $this->getPostsFacebookPage($fanpage, $activity ,$filterObj);
			else
				$error = 'No se encuentra ninguna fanpage con id: '.$id;
			break;

			case 'youtubechannel':
			case 'YoutubeChannel':
			$channel = YoutubeChannel::where('id',$id)->first();
			if($channel)
				return $this->getPostsYoutubePage($channel, $activity ,$filterObj);
			else
				$error = 'No se encuentra ningun canal con id: '.$id;
			break;

			case 'instaprofile':
			case 'InstaProfile':
			$profile = InstaProfile::where('id',$id)->first();
			if($profile)
				return $this->getPostsInstagramProfile($profile, $activity ,$filterObj);
			else
				$error = 'No se encuentra ningun perfil con id: '.$id;
			break;

			case 'adset':
			case 'AdSet':
			$adset = AdSet::where('id',$id)->first();
			if($adset)
				return $this->getPostsAdSet($adset, $filterObj);
			else
				$error = 'No se encuentra ningún adset con id: '.$id;
			break;
		}
		return ['error' => $error];
	}



	/**
	 * Este método obtiene los valores del comms panorama para una canasta.
	 * Los resultados los filtra por un rango de tiempo, si no existe rango de tiempo, calcula todos los datos.
	 * @param Canasta $canasta: Canasta de la que se conseguirán los comms
	 * @param string $country: País por el que se quiere realizar la filtración (default="all")
	 * @param string $since: fecha inicial en formato Y-m-d (default=null)
	 * @param string $until: fecha final en formato Y-m-d (default=null)
	 * @return diccionario con los comms de la canasta.
	 */
	public function getPostsBasket(Canasta $canasta, $country="all", $social_network = 'all', $activity ,$filterObj=null){
		$infometrics = array();
		
		foreach ($canasta->brands as $brand) {
			array_push($infometrics, $this->getPostsBrand($brand, $country, $social_network, $activity ,$filterObj));	
		}
		$info_brand = array(
			'name' => $canasta->name,
			'type' => 'basket',
			'basket_id' => $canasta->id,
			'brands' => $infometrics,
			'metrics' => $this->getInfoMetrics($infometrics, $social_network),
			'color' => Color::getColor('canastas', $canasta->id)
		);
		//dd('Canasta data', $info_brand);
		return $info_brand;
	}
	/**
	 * Este método obtiene los valores del comms panorama para una marca.
	 * Los resultados los filtra por un rango de tiempo, si no existe rango de tiempo, calcula todos los datos.
	 * @param string $country: País por el que se quiere realizar la filtración (default="all")
	 * @param Brand $brand: Marca de la que se conseguirán los comms
	 * @param string $since: fecha inicial en formato Y-m-d (default=null)
	 * @param string $until: fecha final en formato Y-m-d (default=null)
	 * @return diccionario con los comms de la marca.
	 */
	public function getPostsBrand(Brand $brand, $country="all", $social_network = 'all', $activity ,$filterObj=null){
		$fb_infometrics = array();
		$yt_infometrics = array();
		$insta_infometrics = array();
		$infometrics = array();
       
		if($social_network == 'all' || $social_network == 'Facebook' ){
			$fanpages = ($country=="all")? $brand->fanpages : $brand->fanpages()->where('name', 'LIKE', '%'.$country.'%')->get();
			foreach ($fanpages as $fpg) {
				array_push($fb_infometrics, $this->getPostsFacebookPage($fpg, $activity ,$filterObj));	
			}
			$infometrics = array_merge($infometrics, $fb_infometrics);
		}
		if($social_network == 'all' || $social_network == 'YouTube' ){
			$channels = ($country=="all")? $brand->channels : $brand->channels()->where('name', 'LIKE', '%'.$country.'%')->get();
			foreach ($channels as $chn) {
				array_push($yt_infometrics, $this->getPostsYoutubePage($chn, $activity ,$filterObj));	
			}
			$infometrics = array_merge($infometrics, $yt_infometrics);	
		}
		if($social_network == 'all' || $social_network == 'Instagram'){
			$profiles = ($country=="all")? $brand->profiles : $brand->profiles()->where('name', 'LIKE', '%'.$country.'%')->get();
			foreach ($profiles as $profile) {
				array_push($insta_infometrics, $this->getPostsInstagramProfile($profile, $activity ,$filterObj));	
			}
			$infometrics = array_merge($infometrics, $insta_infometrics);	
		}

		$info_brand = array(
			'name' => $brand->name,
			'type' => 'brand',
			'brand_id' => $brand->id,
			'fanpages' => $fb_infometrics,
			'channels' => $yt_infometrics,
			'profiles' => $insta_infometrics,
			'metrics' => $this->getInfoMetrics($infometrics, $social_network),
			'color' => Color::getColor('brands', $brand->id)
		);

		//dd('Marca data', $info_brand);
		return $info_brand;
	}

	 /**
 	 * Este método obtiene los posts y sus métricas para una lista de páginas.
	 * Los resultados los filtra por un rango de tiempo, si no existe rango de tiempo, calcula todos los datos.
	 * @param {string} $pagesID: Lista de las ids a filtrar "{SlugRedSocial}_{idpage_1},{idPage_2},...,{idPage_n}"
	 * @param {Array} $filterObj: Diccionario con las propiedades para filtrar los posts.
	 * @return diccionario con los comms de las páginas.
 	 */
	public function getPostsPageList($pagesIDs, $filterObj=null){
		$infometrics = array();
		$social_network = explode('_', $pagesIDs);
 		$pagesIDs = $social_network[1];
 		$social_network = strtolower($social_network[0]);
		$snInfo = Config::get('constants.social_networks');
		// Indica el método a llamar según el tipo de página
		$methodComms = false;
		if($social_network == $snInfo['facebook']['slug']) $methodComms = "getCommsFacebookPage";
		else if($social_network == $snInfo['youtube']['slug']) $methodComms = "getCommsYoutubeChannel";
        else if($social_network == $snInfo['instagram']['slug']) $methodComms = "getCommsInstagramProfile";
		// nombre del modelo de la página de la red social seleccionada
		$type = explode('\\', $snInfo[$social_network]['models']['page'])[1];
		$pageLists = MethodsController::getCategory($type, $pagesIDs);
		// Si no se obtiene una lista de páginas, convierte la página en una lista de un elemento.
		if(!$pageLists instanceof \Illuminate\Support\Collection && $pageLists!=null) $pageLists = [$pageLists];
		if($methodComms){
			foreach ($pageLists as $pg) {
				array_push($infometrics, $this->$methodComms($pg, $filterObj));
			}
		}

		$info_pages = array(
			'name' => 'Lista de '.$type,
			'type' => $type,
			'pages' => $infometrics,
			'metrics' => $this->getInfoMetrics($infometrics, $snInfo[$social_network]['name']),
			'color' => null
		);
		return $info_pages;

	}

	/**
	 * Este método obtiene los valores del comms panorama para un adset.
	 * Los resultados los filtra por un rango de tiempo, si no existe rango de tiempo, calcula todos los datos.
	 * @param AdSet $adset: Adset del que se conseguirán los comms
	 * @param string $since: fecha inicial en formato Y-m-d (default=null)
	 * @param string $until: fecha final en formato Y-m-d (default=null)
	 * @return diccionario con los comms de la fanpage.
	 */
	public function getPostsAdSet(AdSet $adset, $filterObj=null){

		$posts = $adset->posts()->leftJoin('metrics', 'posts.id' , '=', 'metrics.post_id')->groupBy('posts.id');

		if($filterObj)
			$posts = $filterObj->filterPosts($posts);
		$posts = $posts->get();
// dd($posts);
		$list = array();
		$sumMetrics = array(
			'fb_reactions' => 0,
			'fb_comments' => 0,
			'fb_shares' => 0,
			'fb_views' => 0,
			'cant' => 0,
			'fb_reactions_n' => 0,
			'fb_comments_n' => 0,
			'fb_shares_n' => 0,
			'fb_views_n' => 0,
		);

		foreach ($posts as $key => $p) {
			$res = $this->getMetricsFacebookPosts($p, $filterObj, $list, $sumMetrics);
			$list = $res['list'];
			$sumMetrics = $res['metrics'];
		}

		foreach ($list as $key => $ele) {
			$list[$key] = $this->addRatesTodInfoMetricsFB($ele, $sumMetrics['fb_reactions'], $sumMetrics['fb_views']);
		}
		//dd($p, $list, $sumMetrics, $posts);
		$info = array(
			'type' => 'adset',
			'name' => $adset->name,
			'adset_id' => $adset->id,
			'metrics' => $sumMetrics,
			'posts' => $list,
			'color' => Color::getColor('ad_sets', $adset->id)
		);
		// dd($info);
		return $info;
	}


	/**
	 * Este método obtiene los valores del comms panorama para una fanpage.
	 * Los resultados los filtra por un rango de tiempo, si no existe rango de tiempo, calcula todos los datos.
	 * @param Fanpage $fanpage: fanpage de la que se conseguirán los comms
	 * @param string $since: fecha inicial en formato Y-m-d (default=null)
	 * @param string $until: fecha final en formato Y-m-d (default=null)
	 * @return diccionario con los comms de la fanpage.
	 */
	public function getPostsFacebookPage(Fanpage $fanpage, $activity ,$filterObj=null){
		$posts = ($activity == 'published')? $fanpage->posts() : $fanpage->posts()->select('posts.*')->join('metrics as metrics', 'posts.id' , '=', 'metrics.post_id')->groupBy('posts.id');
		// $posts = $fanpage->posts()->select('posts.*')->join('metrics as metrics', 'posts.id' , '=', 'metrics.post_id')->groupBy('posts.id');
		if($filterObj)
		$posts = $filterObj->filterPosts($posts);
		$posts = $posts->get();
        
			// Filtros anteriores
			// $posts = $this->filterByDate($posts, $since, $until,'metrics.created_at')->get();
			// $posts = $this->filterByDate($fanpage->posts(), $since, $until)->get();

		$list = array();
		$sumMetrics = array(
			'fb_reactions' => 0,
			'fb_comments' => 0,
			'fb_shares' => 0,
			'fb_views' => 0,
			'cant' => 0,
			'fb_reactions_n' => 0,
			'fb_comments_n' => 0,
			'fb_shares_n' => 0,
			'fb_views_n' => 0,
		);

		foreach ($posts as $key => $p) {
			$res = $this->getMetricsFacebookPosts($p, $filterObj, $list, $sumMetrics);
			$list = $res['list'];
			$sumMetrics = $res['metrics'];
		}
		foreach ($list as $key => $ele) {
			$list[$key] = $this->addRatesTodInfoMetricsFB($ele, $sumMetrics['fb_reactions'], $sumMetrics['fb_views']);
		}
		//dd($p, $list, $sumMetrics, $posts);
		$info = array(
			'type' => 'fanpage',
			'name' => 'FB '.$fanpage->name.' '.$fanpage->brand->name,
			'fanpage_id' => $fanpage->id,
			'facebook_id' => $fanpage->facebook_id, 
			'metrics' => $sumMetrics,
			'posts' => $list,
			'color' => Color::getColor('fanpages', $fanpage->id)
		);
		// dd($info);
		return $info;
	}


	/**
	 * Este método obtiene los valores del comms panorama para una fanpage.
	 * Los resultados los filtra por un rango de tiempo, si no existe rango de tiempo, calcula todos los datos.
	 * @param Fanpage $fanpage: fanpage de la que se conseguirán los comms
	 * @param string $since: fecha inicial en formato Y-m-d (default=null)
	 * @param string $until: fecha final en formato Y-m-d (default=null)
	 * @return diccionario con los comms de la fanpage.
	 */
	public function getPostsYoutubePage(YoutubeChannel $channel, $activity ,$filterObj=null){
		$posts = ($activity == 'published')? $channel->messyPosts() : $channel->posts()->select('youtube_videos.*')->join('youtube_metrics as metrics', 'youtube_videos.id' , '=', 'metrics.youtube_video_id')->groupBy('youtube_videos.id');
		// $posts = $channel->posts()->select('youtube_videos.*')->join('youtube_metrics as metrics', 'youtube_videos.id' , '=', 'metrics.youtube_video_id')->groupBy('youtube_videos.id');
		if($filterObj)
			$posts = $filterObj->filterPosts($posts);
		$posts = $posts->get();

		$list = array();
		$sumMetrics = array(
			'yt_likes' => 0,
			'yt_dislikes' => 0,
			'yt_comments' => 0,
			'yt_views' => 0,
			'cant' => 0,
			'yt_likes_n' => 0,
			'yt_dislikes_n' => 0,
			'yt_comments_n' => 0,
			'yt_views_n' => 0,
		);

		foreach ($posts as $key => $p) {
			$res = $this->getMetricsYoutubeVideo($p, $filterObj, $list, $sumMetrics);
			$list = $res['list'];
			$sumMetrics = $res['metrics'];
		}
		foreach ($list as $key => $ele) {
			$list[$key] = $this->addRatesTodInfoMetricsYT($ele, $sumMetrics['yt_views']);
		}
		//dd($p, $list, $sumMetrics, $posts);
		$info = array(
			'type' => 'channel',
			'name' => 'YT '.$channel->name.' '.$channel->brand->name,
			'channel_id' => $channel->id,
			'youtube_id' => $channel->youtube_id, 
			'metrics' => $sumMetrics,
			'posts' => $list,
			'color' => Color::getColor('channels', $channel->id)
		);
		// dd($info);
		return $info;
	}

	/**
	 * Este método obtiene los valores del comms panorama para un Perfil.
	 * Los resultados los filtra por un rango de tiempo, si no existe rango de tiempo, calcula todos los datos.
	 * @param Fanpage $fanpage: fanpage de la que se conseguirán los comms
	 * @param string $since: fecha inicial en formato Y-m-d (default=null)
	 * @param string $until: fecha final en formato Y-m-d (default=null)
	 * @return diccionario con los comms del Perfil.
	 */
	public function getPostsInstagramProfile(InstaProfile $profile, $activity ,$filterObj=null){
		 
		$posts = ($activity == 'published')? $profile->messyPosts() : $profile->posts()->select('insta_posts.*')->join('insta_metrics as metrics', 'insta_posts.id' , '=', 'metrics.insta_post_id')->groupBy('insta_posts.id'); 
		// $posts = $profile->posts()->select('insta_posts.*')->join('insta_metrics as metrics', 'insta_posts.id' , '=', 'metrics.insta_post_id')->groupBy('insta_posts.id');
		
		if($filterObj)
		$posts = $filterObj->filterPosts($posts);
		$posts = $posts->get();

		$list = array();
		$sumMetrics = array(
			'insta_likes' => 0,
			'insta_comments' => 0,
			'insta_views' => 0,
			'cant' => 0,
			'insta_likes_n' => 0,
			'insta_comments_n' => 0,
			'insta_views_n' => 0,
		);

		foreach ($posts as $key => $p) {
			$res = $this->getMetricsInstagramPost($p, $filterObj, $list, $sumMetrics);
			$list = $res['list'];
			$sumMetrics = $res['metrics'];
		}
		foreach ($list as $key => $ele) {
			$list[$key] = $this->addRatesTodInfoMetricsINSTA($ele, $sumMetrics['insta_views']);
		}
		//dd($p, $list, $sumMetrics, $posts);
		$info = array(
			'type' => 'profile',
			'name' => 'IN '.$profile->name.' '.$profile->brand->name,
			'profile_id' => $profile->id,
			'instagram_id' => $profile->instagram_id, 
			'metrics' => $sumMetrics,
			'posts' => $list,
			'color' => Color::getColor('profiles', $profile->id)
		);
		// dd($info);
		return $info;
	}


	/**
	 * Obtiene la variación en las métricas de un post.
	 * @param $post: post del que se buscarán las métricas.
	 * @param string $since: fecha inicial en formato Y-m-d (default=null)
	 * @param string $until: fecha final en formato Y-m-d (default=null)
	 * @param array $list: Lista con la información de los posts (default=null)
	 * @param array $sumMetrics: diccionario con la suma de las métricas (default=null)
	 * @return Diccionario con los atributos del post
	 */
	public function getMetricsFacebookPosts($post, $filterObj=null, $list=null, $sumMetrics=null){		
		if(!$post instanceof Post)
			$post = Post::where('id', $post->post_id)->first();

		$first =null;
		$end = DB::table('metrics as w')->where('w.post_id', $post->id);
		if($filterObj->date['since'] != null  && $filterObj->date['until'] != null){
			$first = DB::table('metrics as w')->where('w.post_id', $post->id);
			$first = $first->where('w.created_at', '<', $filterObj->date['since'])->latest()->first();         	
			$end = $end->where('w.created_at', '<=', $filterObj->date['until'].' 23:59:59')->latest()->first();
		}else{

			$end = $end->latest()->first();
		}

		// dd($first, $end, $since, $until);
		$r = 0;
		$c = 0;
		$s = 0;
		$v = 0;
		if($end && $first){
			$r = $end->reactions - $first->reactions;
			$c = $end->comments - $first->comments;
			$s = $end->shares - $first->shares;
			$v = max($end->views - $first->views, 0);	
		}else
		if($end){
			$r = $end->reactions;
			$c = $end->comments;
			$s = $end->shares;
			$v = $end->views;
		}
		
		$d = array('fb_reactions' => $r, 'fb_comments' => $c, 'fb_shares' => $s, 'fb_views'=>$v ,'facebook_id'=>$post->facebook_id, 'social_network'=>$post->social_network());
		$d = $this->addPublicationInfoToArray($post, $d);

		if($v > 0){
			$d['own_amplification_rate'] = 100 * $s / $v;
			$d['own_participation_rate'] = 100 * $c / $v;
			$d['own_reaction_rate'] = 100 * $r / $v;
		}else if($r > 0){
			$d['own_amplification_rate'] = 100 * $s / $r;
			$d['own_participation_rate'] = 100 * $c / $r;
			$d['own_reaction_rate'] = 'N/A';
		}else{
			$d['own_amplification_rate'] = 0;
			$d['own_participation_rate'] = 0;
			$d['own_reaction_rate'] = 0;
		}
		if($list !== null){
			array_push($list, $d);
		}

		if($sumMetrics != null){
			foreach (ConsultController::METRICS_BY_PRE['fb_'] as $key => $metricsName) {
				$snmn = 'fb_'.$metricsName;
				$m = $d[$snmn];
				if($m >= 0)
					$sumMetrics[$snmn] += $m;
				else
					$sumMetrics[$snmn.'_n'] += $m;
			}
			$sumMetrics['cant']++;
		}
		return array('list'=>$list, 'metrics' => $sumMetrics);
	}


	/**
	 * Obtiene la variación en las métricas de un post.
	 * @param $post: post del que se buscarán las métricas.
	 * @param string $since: fecha inicial en formato Y-m-d (default=null)
	 * @param string $until: fecha final en formato Y-m-d (default=null)
	 * @param array $list: Lista con la información de los posts (default=null)
	 * @param array $sumMetrics: diccionario con la suma de las métricas (default=null)
	 * @return Diccionario con los atributos del post
	 */
	public function getMetricsYoutubeVideo($post, $filterObj=null, $list=null, $sumMetrics=null){		
		if(!$post instanceof YoutubeVideo)
			$post = YoutubeVideo::where('id', $post->youtube_video_id)->first();

		$first =null;
		$end = DB::table('youtube_metrics as w')->where('w.youtube_video_id', $post->id);
		if($filterObj->date['since'] != null  && $filterObj->date['until'] != null){
			$first = DB::table('youtube_metrics as w')->where('w.youtube_video_id', $post->id);
			$first = $first->where('w.created_at', '<', $filterObj->date['since'])->latest()->first();         	
			$end = $end->where('w.created_at', '<=', $filterObj->date['until'].' 23:59:59')->latest()->first();
		}else{

			$end = $end->latest()->first();
		}

		// dd($first, $end, $since, $until);
		$l = 0;
		$c = 0;
		$dl = 0;
		$v = 0;
		if($end && $first){
			$l = $end->likes - $first->likes;
			$c = $end->comments - $first->comments;
			$dl = $end->dislikes - $first->dislikes;
			$v = $end->views - $first->views;	
		}else
		if($end){
			$l = $end->likes;
			$c = $end->comments;
			$dl = $end->dislikes;
			$v = $end->views;
		}
		
		$d = array('yt_likes' => $l, 'yt_comments' => $c, 'yt_dislikes' => $dl, 'yt_views'=>$v ,'youtube_id'=>$post->youtube_id, 'social_network'=>$post->social_network());
		$d = $this->addPublicationInfoToArray($post, $d);

		if($v > 0){
			$d['own_amplification_rate'] = 'N/A';
			$d['own_participation_rate'] = 100 * $c / $v;
			$d['own_reaction_rate'] = 100 * $l / $v;
		}else{
			$d['own_amplification_rate'] = 'N/A';
			$d['own_participation_rate'] = 0;
			$d['own_reaction_rate'] = 0;
		}
		if($list !== null){
			array_push($list, $d);
		}

		if($sumMetrics != null){
			foreach (ConsultController::METRICS_BY_PRE['yt_'] as $key => $metricsName) {
				$snmn = 'yt_'.$metricsName;
				$m = $d[$snmn];
				if($m >= 0)
					$sumMetrics[$snmn] += $m;
				else
					$sumMetrics[$snmn.'_n'] += $m;
			}
			$sumMetrics['cant']++;
		}
		return array('list'=>$list, 'metrics' => $sumMetrics);
	}

	/**
	 * Obtiene la variación en las métricas de un post de instagram.
	 * @param $post: post del que se buscarán las métricas.
	 * @param string $since: fecha inicial en formato Y-m-d (default=null)
	 * @param string $until: fecha final en formato Y-m-d (default=null)
	 * @param array $list: Lista con la información de los posts (default=null)
	 * @param array $sumMetrics: diccionario con la suma de las métricas (default=null)
	 * @return Diccionario con los atributos del post
	 */
	public function getMetricsInstagramPost($post, $filterObj=null, $list=null, $sumMetrics=null){		
		if(!$post instanceof InstaPost)
			$post = InstaPost::where('id', $post->post_id)->first();
            
		$first =null;
		$end = DB::table('insta_metrics as w')->where('w.insta_post_id', $post->id);
		if($filterObj->date['since'] != null  && $filterObj->date['until'] != null){
			$first = DB::table('insta_metrics as w')->where('w.insta_post_id', $post->id);
			$first = $first->where('w.created_at', '<', $filterObj->date['since'])->latest()->first();         	
			$end = $end->where('w.created_at', '<=', $filterObj->date['until'].' 23:59:59')->latest()->first();
		}else{
			$end = $end->latest()->first();
		}

		// dd($first, $end, $since, $until);
		$l = 0;
		$c = 0;
		$v = 0;
		if($end && $first){
			$l = $end->likes - $first->likes;
			$c = $end->comments - $first->comments;
			$v = max($end->views - $first->views, 0);	
		}else
		if($end){
			$l = $end->likes;
			$c = $end->comments;
			$v = $end->views;
		}
 		
		$d = array('insta_likes' => $l, 'insta_comments' => $c, 'insta_views' => $v, 'instagram_id'=>$post->post_id, 'social_network'=>$post->social_network());
		$d = $this->addPublicationInfoToArray($post, $d);
        // Revisar calculo para Instagram
		if($v > 0){
			$d['own_amplification_rate'] = 'N/A';
			$d['own_participation_rate'] = 100 * $c / $v;
			$d['own_reaction_rate'] = 100 * $l / $v;
		}else{
			$d['own_amplification_rate'] = 'N/A';
			$d['own_participation_rate'] = 0;
			$d['own_reaction_rate'] = 0;
		}
		if($list !== null){
			array_push($list, $d);
		}

		if($sumMetrics != null){
			foreach (ConsultController::METRICS_BY_PRE['insta_'] as $key => $metricsName) {
				$snmn = 'insta_'.$metricsName;
				$m = $d[$snmn];
				if($m >= 0)
					$sumMetrics[$snmn] += $m;
				else
					$sumMetrics[$snmn.'_n'] += $m;
			}
			$sumMetrics['cant']++;
		}
		return array('list'=>$list, 'metrics' => $sumMetrics);
	}



	private function addPublicationInfoToArray($post, $infoPost){
		$undefined = "No definido";
		$type = $post->type;
		$type = ($type && $type != "")? $type : $undefined;
		$balance = $post->balance;
		$balance = ($balance && $balance != "")? $balance : $undefined;
		$comunication_task = $post->comunication;
		$comunication_task = ($comunication_task && $comunication_task != "")? $comunication_task : $undefined;
		$comunication_theme = $post->theme;
		$comunication_theme = ($comunication_theme && $comunication_theme != "")? $comunication_theme : $undefined;
		$contest = $post->contests->first();
		$contest = ($contest)?$contest->title:$undefined;
		$product = $post->product_id;
		$p = Product::where('id', $product)->first();
		$product = ($product && $product != "")? $p->getSegementProductname() : $undefined;
		$productName = ($p)? $p->name: $undefined;
		$segment = ($product != $undefined && $p->segmento)? $p->segmento->name : $undefined;
		$influencer = $post->influencer;
		$influencer = ($influencer && $influencer != "")? $influencer : 'Sin influencer';
		$pauta = $post->pauta;
		$d = array( 
			'id'=>$post->id,
			'type' => $type,
			'balance' => $balance,
			'comunication_task' => $comunication_task,
			'comunication_theme' => $comunication_theme,
			'contest' => $contest,
			'product' => $product,
			'product_name' => $productName,
			'segment' => $segment,
			'influencer' => $influencer,
			'pauta' => $pauta,
			'published' => $post->published
		);
		return array_merge($infoPost, $d);
	}


	public function addRatesTodInfoMetricsFB($infoMetrics, $totalReactions, $totalViews){
		if($infoMetrics['fb_views'] > 0 && $totalViews > 0){
			$infoMetrics['fb_amplification_rate'] = 100 * $infoMetrics['fb_shares']/$totalViews;
			$infoMetrics['fb_participation_rate'] = 100 * $infoMetrics['fb_comments']/$totalViews;
			$infoMetrics['fb_reaction_rate'] = 100 * $infoMetrics['fb_reactions']/$totalViews;
		}else if($totalReactions>0){
				// $totalReactions = ($totalReactions == 0)? 1: $totalReactions;
			$infoMetrics['fb_amplification_rate'] = 100 * $infoMetrics['fb_shares']/$totalReactions;
			$infoMetrics['fb_participation_rate'] = 100 * $infoMetrics['fb_comments']/$totalReactions;
			$infoMetrics['fb_reaction_rate'] = 'N/A';
		}else{
			$infoMetrics['fb_amplification_rate'] = 0;
			$infoMetrics['fb_participation_rate'] = 0;
			$infoMetrics['fb_reaction_rate'] = 0;
		}		
		return $infoMetrics;
	}

	public function addRatesTodInfoMetricsYT($infoMetrics, $totalViews){
		if($infoMetrics['yt_views'] > 0 && $totalViews > 0){
			$infoMetrics['yt_amplification_rate'] = 'N/A';
			$infoMetrics['yt_participation_rate'] = 100 * $infoMetrics['yt_comments']/$totalViews;
			$infoMetrics['yt_reaction_rate'] = 100 * $infoMetrics['yt_likes']/$totalViews;
		}else{
			$infoMetrics['yt_amplification_rate'] = 'N/A';
			$infoMetrics['yt_participation_rate'] = 0;
			$infoMetrics['yt_reaction_rate'] = 0;
		}		
		return $infoMetrics;
	}

	public function addRatesTodInfoMetricsINSTA($infoMetrics, $totalViews){
		if($infoMetrics['insta_views'] > 0 && $totalViews > 0){
			$infoMetrics['insta_amplification_rate'] = 'N/A';
			$infoMetrics['insta_participation_rate'] = 100 * $infoMetrics['insta_comments']/$totalViews;
			$infoMetrics['insta_reaction_rate'] = 100 * $infoMetrics['insta_likes']/$totalViews;
		}else{
			$infoMetrics['insta_amplification_rate'] = 'N/A';
			$infoMetrics['insta_participation_rate'] = 0;
			$infoMetrics['insta_reaction_rate'] = 0;
		}		
		return $infoMetrics;
	}

	public function getInfoMetrics($list, $social_network="all"){

		$sns = ($social_network == 'all')? ['fb_', 'yt_', 'insta_'] : [ConsultController::SOCIAL_NETWORK_PRE[$social_network]];
		$sumMetrics = array(
			'cant' => 0,
		);

		foreach ($sns as $key => $snp) {
			foreach (ConsultController::METRICS_BY_PRE[$snp] as $key => $metricsName) {
				$sumMetrics[$snp.$metricsName] = 0;
				$sumMetrics[$snp.$metricsName.'_n'] = 0;
			}
		}
		foreach ($list as $key => $ele) {
			$metrics = $ele['metrics'];
			foreach ($sns as $key => $snp) {
				foreach (ConsultController::METRICS_BY_PRE[$snp] as $key => $metricsName) {
					$snmName = $snp.$metricsName;
					if(isset($metrics[$snmName])){
						$sumMetrics[$snmName] += $metrics[$snmName];
						$sumMetrics[$snmName.'_n'] += $metrics[$snmName.'_n'];
					}
				}
			}
			$sumMetrics['cant'] += $metrics['cant'];
		}
		return $sumMetrics;
	}

}