<?php

namespace App\Http\Controllers;

use App\Color;
use App\Vertical;
use App\Categoria;
use App\Canasta;
use App\Brand;
use App\Fanpage;
use App\YoutubeChanel;
use App\Basket;
use App\Product;
use App\Segmento;
use Illuminate\Http\Request;
use DB;

class ColorController extends Controller
{
    const STRUCTURE = [
    'verticals' =>[
    'name' => 'verticals',
    'class' => 'App\Vertical',
    'ant' => false,
    'sig' => ['categorias']
    ],

    'categorias' =>[
    'name' => 'categorias',
    'class' => 'App\Categoria',
    'ant' => 'verticals',
    'sig' => ['canastas']
    ],

    'canastas' =>[
    'name' => 'canastas',
    'class' => 'App\Canasta',
    'ant' => 'categorias',
    'sig' => ['brands', 'segmentos']
    ],

    'brands' =>[
    'name' => 'brands',
    'class' => 'App\Brand',
    'ant' => 'canastas',
    'sig' => ['fanpages', 'channels', 'products']
    ],

    'segmentos' =>[
    'name' => 'segmentos',
    'class' => 'App\Segmento',
    'ant' => 'canastas',
    'sig' => ['products']
    ],

    'fanpages' =>[
    'name' => 'fanpages',
    'class' => 'App\Fanpage',
    'ant' => 'brands',
    'sig' => ['posts']
    ],

    'channels' =>[
    'name' => 'channels',
    'class' => 'App\YoutubeChanel',
    'ant' => 'brands',
    'sig' => ['videos']
    ],

    'posts' =>[
    'name' => 'posts',
    'class' => 'App\Post',
    'ant' => 'brands',
    'sig' => false
    ],

    'videos' =>[
    'name' => 'videos',
    'class' => 'App\YoutubeVideo',
    'ant' => 'brands',
    'sig' => false
    ],

    'products' =>[
    'name' => 'products',
    'class' => 'App\Product',
    'ant' => 'segmentos',
    'sig' => false
    ]

    ];



    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $allOptions = array(
            'verticals'=> ['slug' => 'verticals', 'name' => 'Industrias', 'list' => Vertical::all()],
            'categorias'=> ['slug' => 'categorias', 'name' => 'Sectores', 'list' => Categoria::all()],
            'canastas'=> ['slug' => 'canastas', 'name' => 'Categoría', 'list' => Canasta::all()],
            'brands'=> ['slug' => 'brands', 'name' => 'Marcas', 'list' => Brand::all()],
            'fanpages'=> ['slug' => 'fanpages', 'name' => 'Fanpages', 'list' => Fanpage::all()], 
            'segmentos'=> ['slug' => 'segmentos', 'name' => 'Segmentos', 'list' => Segmento::all()],
            'products'=> ['slug' => 'products', 'name' => 'Productos', 'list' => Product::all()]
            );
        
        $multiple = false;
        $elements = [];
        $table = false;
        $parent = false;
        $type = '';

        $get = $_GET;
        if($_GET){
            $multiple = !empty($_GET['multiple']) && $_GET['multiple']==true;
            $type = $_GET['type'];
            switch ($type) {
                case 'table':
                $table = $_GET['to'];
                $parent = $_GET['from'];
                    // dd($this->getRelationRoute($parent, $table));
                $elements = $this->getRelation($parent, $_GET['element'], $table);

                break;
                case 'row':


                break;
                default:
                
                break;
            }


        }
        
        return view('utils.color.create', compact('allOptions', 'multiple', 'type', 'table', 'parent','elements'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $multiple = $request['multiple'];
        if($multiple){
            $modified = explode(' ', $request['modified-colors']);
            foreach ($modified as $k => $cName) {
                $c = $request[$cName]; // 
                if($c){
                    $cInfo = explode('-', $cName); // 'color' + type + table + id
                    
                    if($cInfo[1]=='table'){
                        $color = Color::firstOrCreate([
                            'table' => $cInfo[2] ,
                            'element_id' => $cInfo[3],
                            ]);                        
                        $color->color = $this->getColorFormat($c);
                        $color->save();
                    }
                }
            }
        }else{

            $this->validate($request,[
                'table' => 'required|string',
                'color' => 'required|string',
                ]);

            $color = Color::firstOrCreate([
                'table' => $request['table'],
                'element_id' => $request['id'],
                ]);
        // dd($request['color'],'a',$this->getColorFormat($request['color']));
            $color->color = $this->getColorFormat($request['color']);
            $color->save();
        // dd($color, $request);
        }
        return redirect()->back()->withSuccess('El color se ha actualizado.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Color  $color
     * @return \Illuminate\Http\Response
     */
    public function show(Color $color)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Color  $color
     * @return \Illuminate\Http\Response
     */
    public function edit(Color $color)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Color  $color
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Color $color)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Color  $color
     * @return \Illuminate\Http\Response
     */
    public function destroy(Color $color)
    {
        //
    }
    
    /**
    *
    * @param String $fType From type
    * @param String $fId From id
    * @param String $tType to type
    */
    public function getRelation($fType,$fId, $tType){
        $from =false;
        $elements = [];
        if($fType == 'all'){
            $elements= DB::table($tType);
            if($fId != 'none' && $fId!='')
                $elements = $elements->where('id', $fId);
            $elements = $elements->get();
        }else{
            $struct = ColorController::STRUCTURE;
            if(array_key_exists($fType, $struct)){
                $from = $struct[$fType]['class']::where('id', $fId)->first();    
                $relationRoute = $this->getRelationRoute($fType, $tType);
                $elements = $this->getListElements($from, $relationRoute);
                 // dd($relationRoute, $elements);
                // $elements = $from->$tType;
            }else{
                echo 'No se encuentra '.$tType.' en '.$fType.'\n';
            }
        }
        return $elements;
    }

    /**
    * @param Model $from elemento dónde se realizará la búsqueda.
    * @param Array<String> $relationRoute ruta para llegar a la búsqueda.
    */
    public function getListElements($from, $relationRoute){
        if(!$from || !$relationRoute) 
            return [];
        $first = array_shift($relationRoute);

        $actualData = [$from];
        while (sizeof($relationRoute) > 0) {
            $actual = array_shift($relationRoute);            
            
            $newData = null;
            // echo '<br> <br> <br> <br> <br> <br>';
            foreach ($actualData as $key => $data) {
                if($actual == 'brands')
                    $newData = ($newData)? $newData->union($data->$actual()->select(array('brands.*', 'canasta_brand.canasta_id as pivot_canasta_id', 'canasta_brand.brand_id as pivot_brand_id'))): $data->$actual();
                else
                    $newData = ($newData)? $newData->union($data->$actual()): $data->$actual();
            }
            $actualData = $newData->get();

        }

        return $actualData;
    }

    /**
    * Busca las relaciones intermedias entre dos modelos pertenecientes a la constante ColorController::STRUCTURE y retorna el camino en un arreglo de strings.
    * @param String $fType From type
    * @param String $tType to type
    * @return Array<String> ruta de $fType hasta $tType.
    */
    public static function getRelationRoute($fType, $tType){
        $struct = ColorController::STRUCTURE[$fType];
        $sig = $struct['sig'];
        // echo $fType.' -> '.$tType.'<br>';
        if($sig){
            // si existe al menos un camino para continuar.
            if(in_array($tType, $sig)){
                // comienza buscando el destino en el siguiente nivel. Si encuentra el destino, finaliza la búsqueda.
                return [$fType, $tType];
            }else{
                // si no encuentra  el destino, baja un nivel.                
                $route = false;
                foreach ($sig as $k => $siguiente) {
                    $rt = ColorController::getRelationRoute($siguiente, $tType);
                    if($rt)
                        $route = $rt;
                }
                if($route){
                    // si encuentra un camino que llegue al destino, lo retorna desde su posición inicial. 
                    array_unshift($route, $fType);
                    return $route;
                }else{
                    // si ha recorrido los caminos existentes y no encuentra ek destino, retorna no encontrado y finaliza la búsqueda.
                    return false;
                }
            }  
        }else{
            // si no existen más caminos a seguir, retorna que no lo encontró y finaliza la búsqueda.
            return false;
        }
    }

    private function getColorFormat($color){
        $color = ($color[0] == '#')? $color: '#'.$color;
        return $color;
    }

    /**
    * @param {String} $table: nombre de la tabla del elemento del color, o   
    *
    *
    *
    */
    public static function getColor($table, $id){
        $fr = false;
        if(strpos($table, " ")){
            $infoColor = explode(' ', $table);
            switch ($infoColor[0]) {
                case 'slug': // slug metrics-youtube-likes
                $fr = Color::where('table','like', '%'.$infoColor[1])->first();        
                break;
                case 'color': // nombre de color
                $fr = Color::where('table', $infoColor[1])->first();        
                break;
                default:
                return 'undefined';
                break;
            }
        }else{
            $fr = Color::where('table', $table)->where('element_id', $id)->first();
        }
        return (($fr)? $fr->color: 'undefined');
    }

// $newName = $request->input('name');
//        $pId = $request->input('product_id');
//        $editProduct = Product::where('id', $pId)->first();
//        $editProduct->name = $newName;
//        $editProduct->group_product = str_slug($newName, '-');
//        if($request->input('segmento')){
//        $editProduct->segmento_id =  $request->input('segmento');
//        }
//        $editProduct->save();

    public function assignColor(Request $request){
        $colorExists = Color::where('element_id', $request->input('element_id'))->where('table',  $request->input('table'))->first();
     if($colorExists){
         // $colorExists->update(['color' => $request->input('color')]);
        if($colorExists->color == $request->input('color')){
        return redirect()->back()->withSuccess('El color es el mismo');
        }else{
        $colorExists->color = "#" . $request->input('color');
        $colorExists->save();   
        return redirect()->back()->withSuccess('El color fue editado exitosamente');
        }
    }else{
        $colorExists = Color::create([
            'table' => $request->input('table'),
            'element_id' => $request->input('element_id'),
            'color' => "#" . $request->input('color')
            ]); 
          return redirect()->back()->withSuccess('Color asignado con éxito');
    }
}

public static function getColorList(...$tables){
    $allColors = array('default' => 'undefined');
    foreach ($tables as $kTable => $table) {
        $colors = [];        
        if(strpos($table, " ")){
        }else{
            $cls = Color::leftJoin($table, 'colors.element_id', '=', $table.'.id')->where('table', $table)->get();
            foreach ($cls as $key => $c) {
                $colors[$c->name] = $c->color;
            }                
        }
        $allColors[$table] = $colors;
    }
    return $allColors;
}

}
