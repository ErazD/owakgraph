<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];


    /*-------------------------------------------------------*
     * Obtener usuario mediante su correo electrónico.
     * @return Object
     *-------------------------------------------------------*/
    public function getUser($email)
    {
        return $this->where('email', $email)->first();
    }



    /*-------------------------------------------------------*
     * Crear usuario
     * @return Object
     *-------------------------------------------------------*/
    public function createUser($facebook_user) {
        if(!empty($facebook_user['email'])){
            return $this->create([
                'name' => $facebook_user['name'],
                'email' => $facebook_user['email'],
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ]);
        }else{
            return $this->create([
                'name' => $facebook_user['name'],
                'email' => $facebook_user['id'],
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ]);
        }
    }



    /*-------------------------------------------------------*
     * Verificar Rango del usuario
     * @return var
     *-------------------------------------------------------*/
    public function isAdmin()
    {
        return $this->rank;
    }


    public function twoFactorProfile(){
        return $this->hasOne(twoFactorProfile::class);
    }

     /*-------------------------------------------------------*
     * Obtener relación entre un usuario y sus grupos.
     * @return Object
     *-------------------------------------------------------*/
     public function groups(){
        return $this->belongsToMany(Group::class, 'group_user', 'user_id', 'group_id');
    }

    public function permissions(){
        return $this->hasMany(UserRole::class, 'element_id')->where('element_model', 'User');
        // return UserRole::where('element_model', 'User')->where('element_id', $this->id);
     }

}
