<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
|--------------------------------------------------------------------------
| Cron Routes
|--------------------------------------------------------------------------
|
| app/Console/Commands/GetAllPosts
| app/Console/Commands/GetAllMetrics
| app/Console/Commands/DeleteAllReports
| 
*/
Route::get('/test/{brand}/{country}/{since}/{until}' , 'ConsultController@getPostsBrand');

Route::get('/pruebas/{type}/{id}/', 'ExcelImportController@exportsPosts');
Route::get ('/pruebas', 'RedirectController@mockComments');

Route::get('/facebook/callback', 'UsersController@login');

Auth::routes();

Route::get('/', 'BrandsController@index')->name('brands')->middleware('guest');
// Route::get('/', function () {
//     return view('auth.login');
// });

//Get post metrics by fanpage
Route::get('/fanpage/{fanpage}/metrics', 'FanpagesController@getPostMetrics');

//Get all post metrics
Route::get('/fanpages/metrics', 'FanpagesController@getAllMetrics');

//Para poder acceder a estas rutas es necesiario iniciar sesión en FB y el usuario debe tener RANK 1
 // Route::group(['middleware' => ['auth', 'rank']], function(){
Route::group(['middleware' => ['rank:user']], function(){

	Route::get('/token', 'PagesController@tokens')->name('token');



	// Quickly Report
	Route::get('/quickly-report/fanpage', 'PagesController@fanpage');
	Route::get('/quickly-report/posts', 'PagesController@comments');
	Route::get('/download/report/{id}', 'ReportsController@downloadReports');
	Route::post('/quickly-report/fanpage/get-report', 'PagesController@fanpageReport')->name('fanpage_report');
	Route::post('/quickly-report/post/get-report', 'PagesController@postReport')->name('post_report');
	Route::get('consult/post/{since}/{until}/{type}/{ids}', 'ConsultController@consultPost')->name('consult_post');
	//Reportes Youtube
	Route::post('/quickly-report/fanpage/all', 'RedirectController@redirectReport')->name('redirect_report');
	Route::post('/quickly-report/comments/all', 'RedirectController@redirectComments')->name('redirect_comments');

	Route::post('youtube/report/result','YoutubeController@YoutubeChannelReport')->name('channel_report');
	Route::get('/download/youtube-report/{id}', 'YoutubeReportController@downloadReports');


	// Brands
	Route::get('/brand/{brand}', 'BrandsController@show');
	Route::get('/brands/search', 'BrandsController@search')->name('search_brand');
	
	Route::post('/brands/create', 'BrandsController@create')->name('create_brand');
	Route::post('/brand/{brand}/delete', 'BrandsController@delete')->name('delete_brand');

	Route::get('/brand/scrap_type/{model}/{element_id}/{scrap_type}', 'BrandsController@setScrapType');

	// Secciones de posts
	Route::get('/latest-posts', 'PostsController@latest');
	Route::get('/hot-posts', 'PostsController@hot');
	Route::get('/frozen-posts', 'PostsController@frozen');

	Route::post('/create/fanpage/all', 'RedirectController@redirectCreateFanpage')->name('redirect_create_fanpage');
	// Facebook
	//Route::post('/fanpage/create', 'FanpagesController@create')->name('create_fanpage');
	
	Route::get('/fanpage/{fanpage}', 'FanpagesController@show');
	Route::get('/fanpage/{fanpage}/{since}/{until}/', 'FanpagesController@show');
	Route::get('/fanpage/{fanpage}/search', 'FanpagesController@search')->name('search_posts');
	Route::get('/fanpage/{fanpage}/hot-posts', 'FanpagesController@hot');
	Route::get('/fanpage/{fanpage}/scrap', 'FanpagesController@getFacebookPosts');

	Route::get('/posts/{post}', 'PostsController@show');
	
	Route::get('/posts/{post}/metrics', 'PostsController@metricsByWeek');
	Route::get('/posts/{post}/export', 'PostsController@export');
	Route::post('/posts/add', 'PostsController@add')->name('add_post')->middleware('post_exits');
    Route::get('/posts/delete/{post}', 'PostsController@delete')->name('delete_post');


	Route::get('/posts/{post}/allmetrics/hours', 'PostsController@getAllMetricsByHour');
	Route::get('/posts/{post}/allmetrics/day', 'PostsController@getAllMetricsByDay');
	
	// YouTube
	Route::get('/channel/{channel}/search', 'YoutubeChannelController@search')->name('search_video');
	Route::get('/channel/{channel}', 'YoutubeChannelController@show');
	Route::get('/channel/{channel}/{since}/{until}/', 'YoutubeChannelController@show');
	Route::get('/channel/{channel}/hot-posts', 'YoutubeChannelController@hot');
	Route::get('/channel/{channel}/scrap', 'YoutubeChannelController@getYoutubeVideos');
	Route::get('/channel/{channel}/metrics', 'YoutubeChannelController@getPostMetrics');

	Route::get('/youtube-video/{video}', 'YoutubeVideoController@show');
	Route::get('/youtube-video/{video}/metrics', 'YoutubeVideoController@metricsByWeek');
	Route::get('/youtube-video/{video}/export', 'YoutubeVideoController@export');
	Route::post('/youtube-video/add', 'YoutubeVideoController@add')->name('add_video')->middleware('video_exists');

	Route::get('/youtube-video/{video}/allmetrics/hours', 'YoutubeVideoController@getAllMetricsByHour');
	Route::get('/youtube-video/{video}/allmetrics/day', 'YoutubeVideoController@getAllMetricsByDay');
    
    //INSTAGRAM
    Route::group(['middleware' => ['permission']], function () {
    //

    Route::get('/profile/{profile}/search', 'InstaProfileController@search')->name('search_posts');
	Route::get('/profile/{profile}', 'InstaProfileController@show');
	Route::get('/profile/{profile}/scrap', 'InstaProfileController@getInstagramPosts');
	Route::get('/profile/{profile}/hot-posts', 'InstaProfileController@hot');
	Route::get('/profile/{profile}/{since}/{until}/', 'InstaProfileController@show');

	Route::get('/send-instagram-profiles', 'InstaProfileController@sendProfileInfo');

	Route::get('/instagram-post/{post}', 'InstaPostController@show');
	Route::get('/instagram-post/{post}/metrics', 'InstaPostController@metricsByWeek');
	Route::get('/instagram-post/{post}/export', 'InstaPostController@export');
	Route::post('/instagram-post/add', 'InstaPostController@manualAdd')->name('add_instagram_post')->middleware('instagram_post_exists'); 
	Route::get('/instagram-post/{post}/allmetrics/hours', 'InstaPostController@getAllMetricsByHour');
	Route::get('/instagram-post/{post}/allmetrics/day', 'InstaPostController@getAllMetricsByDay');
    Route::get('/retrieve-posts', 'InstaPostController@retrieveInstagramPosts');
    Route::get('/instagram-post-send', 'InstaPostController@sendPostsData');
   
    /////////////////////RUTAS PARA MODIFICAR FECHAS EN POSTS INSTAGRAM/////////////////////
    Route::get('/instagram-send-dates', 'InstaPostController@sendDatabasePosts');
    Route::get('/retrieve-instagram-dates', 'InstaPostController@getInstaDates');
	//////////////////////////////////////////////////
});

	
	// Clasificacion Industria (verticales, segmentos, categorias y canastas)
	Route::resource('vertical','VerticalController');
	Route::resource('segmento','SegmentoController');
	Route::resource('categoria','CategoriaController');
	Route::resource('canasta','CanastaController');
	Route::get('canasta-add-brand/{canasta}/{brand}/','CanastaController@attachBrandToCanasta');
	Route::get('segment-add-product/{segmento}/{product}/','SegmentoController@addproductToSegment');


	// Producto
	Route::post('product/register','CommsController@createProduct')->name('create_product');
	Route::post('product/edit','CommsController@editProduct')->name('edit_product');
	Route::get('product/delete/{product}','CommsController@deleteProduct')->name('delete_product');
	Route::get('product/default/{brand}', 'CommsController@addDefaultProducts')->name('add_default');
	// Concursos
	Route::resource('contest','ContestController');
	// Colores
	Route::resource('color','ColorController');
    Route::post('/color/assign','ColorController@assignColor')->name('assign_brand_color');

	// registro de comms (deprecated) -> transform in admin section
	Route::post('comms/register','CommsController@registerCommsToPost')->name('comms_register');
	

	// CONSULTAS
	//Publicaciones de marcas y consultas
	Route::get('brand-publications/{brand}/','QueryController@getFacebookPublicationsBrand');
	Route::get('canasta-publications/{canasta}/','QueryController@getFacebookPublicationsCanasta');
	Route::get('category-publications/{categoria}/','QueryController@getFacebookPublicationsCategory');
	Route::get('vertical-publications/{vertical}/','QueryController@getFacebookPublicationsVertical');
	Route::get('consulta/{since}/{until}/{type}/{slug}/{interval}/','QueryController@consultMetrics');
	Route::get('consulta/','QueryController@index');

	// SECCIONES DE CONSULTA
	// Consulta por comms
	Route::get('comms/','CommsController@index');
	// Consulta por métricas
	Route::get('comms/consult', 'ConsultController@index');
	Route::get('comms/consult/posts', 'ConsultController@postsRates');
	Route::get('comms/sharesof', 'ConsultController@sharesof');
	Route::get('comms/activity', 'ConsultController@postsRates');
	

    //Consulta por actividad semanal
    Route::get('comms/weekly', 'ConsultWeeklyActivityController@index');
    Route::get('comms/weekly/{type}/{slug}/{country}/', 'ConsultWeeklyActivityController@consultMetrics');
    Route::get('comms/weekly/{type}/{slug}/{country}/{since}/{until}/{interval}', 'ConsultWeeklyActivityController@consultMetrics');

	// Consulta por actividad diaria
	Route::get('comms/heatmap', 'consultActivityController@index');
	
	// RESULTADOS DE CONSULTA 
	//Consulta por comms
	Route::get('comms/{social_network}/{type}/{fanpage}/{country}/{activity}','CommsController@getCategory');
	Route::get('comms/{social_network}/{type}/{fanpage}/{country}/{activity}/{since}/{until}/','CommsController@getCategory');
	//Consulta por métricas
	Route::get('comms/consult/{social_network}/{type}/{fanpage}/{country}/{activity}', 'ConsultController@getPostsCategory');
	Route::get('comms/consult/{social_network}/{type}/{fanpage}/{country}/{activity}/{since}/{until}', 'ConsultController@getPostsCategory');
	// Consulta de actividad

	Route::get('comms/heatmap/{social_network}/{type}/{slug}/{country}/{activity}', 'consultActivityController@consultMetrics');
	Route::get('comms/heatmap/{social_network}/{type}/{slug}/{country}/{activity}/{since}/{until}', 'consultActivityController@consultMetrics');

	// (SCRAP) Agregar vistas de reproduccion a videos de facebook
	Route::get('send/', 'PostsController@getVideoMetrics');
	Route::get('get-data/', 'PostsController@getData');


	//IMPORTACION DE EXCELS
	Route::get('/scrap-excel', 'ExcelImportController@ScrapExcel');
	Route::get('/scrap-excel/upload/{filename}', 'ExcelImportController@ScrapExcel');
	Route::get('/scrap-excel/update/{estado}', 'ExcelImportController@updateExcelImports');
	Route::get('excel-comms/download/{type}/{country}/{id}/{since}/{until}', 'ExcelImportController@exportsPosts');

	// USUARIOS TWO FACTOR
	Route::group(['middleware' => ['rank:two_factor']], function(){
		Route::get('comms/discovery/{social_network}/{fanpage}/', 'CommsController@discovery');
		Route::get('comms/discovery/{social_network}/{fanpage}/{since}/{until}/', 'CommsController@discovery');

		Route::get('two-factor/create/{social_network}/{post_id}/', 'TwoFactorAuthenticationController@create')->name('two-factor.create');
		Route::get('two-factor/create/{social_network}/{post_id}/{principal}/', 'TwoFactorAuthenticationController@create')->name('two-factor.create.p');
		Route::resource('two-factor', 'TwoFactorAuthenticationController', ['except' => 'create']);
		Route::post('two-factor/store-principal/', 'TwoFactorAuthenticationController@storePrincipal')->name('two-factor.store.p');
		Route::post('two-factor/update-post/{two}', 'TwoFactorAuthenticationController@updatePost');
	});	

	Route::group(['middleware' => ['rank:admin']], function(){
		Route::get('post-update/{social_network}/{post_id}/', 'CommsController@adminCommsRegister');
	});
});

Route::get('permissions/users', 'UserRoleController@index')->middleware('rank:admin');
Route::get('permissions/industries', 'GroupIndustryRoleController@index'); //->middleware('rank:admin');
Route::get('permissions/groups/{type}', 'GroupController@index'); //->middleware('rank:admin');

Route::get('group/add/{type}/{group}/{e_id}/{action}', 'GroupController@addElement');
Route::resource('group', 'GroupController');

//Filtros repeditos
Route::get('get-date/{fanpage}', 'MethodsController@discovery');
Route::get('get-date/{fanpage}/{since}/{until}/', 'CommsController@discovery');
Route::get('/scrap-frequency', 'MethodsController@setScrapState');

//Filtros de fan_count
Route::get('/get-fans/{brand}', 'FanpagesController@fanCount');

Route::get('/get-subs/{brand}', 'YoutubeChannelController@subsCount');
Route::get('/get-fanscount/{fanpage}/allmetrics/day', 'FanpagesController@getAllFanMetricsByDay');
Route::get('/get-subscount/{channel}/allmetrics/day', 'YoutubeChannelController@getAllSubMetricsByDay');
Route::get('/get-followers-count/{profile}/allmetrics/day', 'InstaProfileController@getAllSubMetricsByDay');

// Route::get('/get-fanscount/{brand}/allmetrics/day', 'FanpagesController@getAllFanMetricsByDay');
Route::resource('adset', 'AdSetController');
Route::get('adset/add/{adset}/{type}/{id}/{action}', 'AdSetController@add');

Route::post('/metrics/set-metrics/', 'MethodsController@setMetricsFromPost')->name('update_post_metrics');

Route::get('posts-list/{social_network}/{type}/{slug}/{country}/{since}/{until}', 'MethodsController@postsList');
Route::get('assing-tw/{users}/{social_network}/{type}/{slug}/{country}/{since}/{until}', 'TwoFactorAuthenticationController@assingTwoFactors');