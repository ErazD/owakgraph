# README #

This is the main project of Owak Graph the main app of the company

### What is this repository for? ###

* This repository is aimed to develop new features and sections for the main project of owak graph located on the L4i server.
* Version 3.0

### How do I get set up? ###

* Modify the .env file located on the project with your local database information and your facebook graph api key, import the database migrations with "php artisan migrate", or import the test database to your site located on the "test" folder inside this project (You may need to enlarge your local database capacity because the test database is quite large)  

* To run the local graph file, just create a route for it on your local server the current route is "graph.test:8888" if that's not possible, just run "php artisan serve" on the command line

* The functions of this project are different from the ones on the main server, so everytime a new feature is created you'll need to copy and paste the new or modified files on the server and change the routes and other components to make sure they run on live (mainly routes or file locations)

* Some functions of the server like the Comms features or the facebook api features require the user to be logged in with facebook, since that is not available on the local server you may need to work with a pre selected user or work on the live version to change this features.

* If you have a more standarized way to handle this project development, please feel free to do so.

### Contribution guidelines ###

* For testing work mainly on the local project then upload the new features and files on the live server to deploy. Don't make the route of the new function public until you're sure it works.
* No code review guidelines at the moment, feel free to include some
* This project is quite complex but with a little documentation on Laravel you'll be able to learn your way around it in no time!

### Who do I talk to? ###

* Admins: Juanjo Gonazlez, David Erazo, Juan Esteban Lopez
* (If you're the current admin of this project feel free to include yourself on this list)
* (If you need help with contacting the previous admins ask your boss for the info)